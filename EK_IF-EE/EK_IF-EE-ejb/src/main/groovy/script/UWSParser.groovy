import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xml = new MarkupBuilder(writer)

def ULD_NO_REGEX = ".*-(\\w*)/.*";
def UWS_DEST_REGEX = ".*/(\\w{3})/.*";
def WT_INFO_REGEX = ".*/((\\d{1,5})([A|P]))/.*";

//load category 1
def LOAD_1CAT_WITH_SHC_REGEX = ".*/\\d{1,5}[A|P]/([a-zA-Z])\\..*"; // 572A/M.XXX
def LOAD_1CAT_NO_SHC_REGEX = ".*/\\d{1,5}[A|P]/([a-zA-Z])"; // 572A/M

//load category 2
def LOAD_2CAT_WITH_SHC_REGEX = ".*/\\d{1,5}[A|P]/([a-zA-Z])([a-zA-Z])\\..*"; // 572A/MQ.XXX
def LOAD_2CAT_NO_SHC_REGEX = ".*/\\d{1,5}[A|P]/([a-zA-Z])([a-zA-Z])"; // 572A/MQ

def COTOUR_REGEX = ".*\\.(((?i)[a-z]{1,3})/(\\w{1,2}))\\.?.*"; //shc conflict 
def SHC_REGEX_PREFIX1 = ".*/((\\d{1,5})([A|P]))";
def SHC_REGEX_PREFIX2 = "/(((?i)[a-z]?)((?i)[a-z]?))\\.?"; //572A/MQ.
def SHC_REGEX_PREFIX3 = "(((?i)[a-z]{1,3})/(\\w{1,2}))?\\.?"; // PMD/Q7
def SHC_REGEX = SHC_REGEX_PREFIX1 + SHC_REGEX_PREFIX2 + SHC_REGEX_PREFIX3 + "(((?i)[a-z]{1,3})+(/.*{1,6})?).*"; //cotour conflict

//shc remarks
//def SHC_REMARKS_COTOUR_REGEX = LOAD_1CAT_WITH_SHC_REGEX + COTOUR_REGEX

def NEW_LINE_REGEX = "(\n\r|\r\n|\n|\r)"

def ULD_REGEX = "\\s*-[a-zA-Z]{3}.*";

//Bulk
def BULK_REGEX = "\\s*-.*";
def BULK_DEST_REGEX = "\\s*-(\\w{3})/.*";

String nsRegex = "(?ms).*<\\?xml.*\\?>.*<(.*):Cargo\\s.*";

def matcher;

if(rawMessage.matches(nsRegex)) {
	
	def ns = rawMessage.replaceAll(nsRegex, "\$1")
	//println "Xml NameSpace $ns"
	LOG.info("Xml NameSpace {}", ns);
			
	def uwsOnlyBlockRegex = "(?ms)<$ns:UWSInfo>(.*)</$ns:UWSInfo>"
	
	def uwsInfoTag = new XmlSlurper().parseText(rawMessage)
	def extractedText = uwsInfoTag."UWSInfo".text()
	rawMessage = rawMessage.replaceAll(uwsOnlyBlockRegex, String.format("<$ns:UWSInfo>%s</$ns:UWSInfo>", extractedText)).trim()
	
			
	def uwsTagName = "$ns:UWSULDDetails"
	def bulkTagName = "$ns:BULKULDDetails"
	def siTagName = "$ns:SIULDDetails"
	
	def uldTagName = "$ns:ULDNumber"
	def destTagName = "$ns:Dest3"
	def wtTagName = "$ns:Weight"
	def wtStatusTagName = "$ns:WeightStatus"
	def cat1TagName = "$ns:LoadCat1"
	def cat2TagName = "$ns:LoadCat2"
	def contourTagName = "$ns:LoadContourCode"
	def contourNoTagName = "$ns:LoadContourNumber"
	def shcTagName = "$ns:ShcList"
	def shcRmksTagName = "$ns:ShcRemarks"
	
	def rampTansferFlnoTagName = "$ns:RampTransferFlno"
	
	def flightInfoTagName = "$ns:FlightInfo"
	def flightNumberTagName = "$ns:FlightNumber"
	def dateOfMonthTagName = "$ns:DateOfMonth"
	def airportCodeTagName = "$ns:AirportCode"
	def finalMessageIndicatorTagName = "$ns:FinalMessageIndicator"
	
	xml."$ns:UWSInfo"() {
		
		//	FLIGHT_REGEX
		matcher = rawMessage =~ /(?ms).*((\w{6})\/(\d{2})\.([a-zA-Z]{3})\.([a-zA-Z]{1,5}))$.*/ ///EK0001/05.DXB.FINAL
				def flightInfo = matcher.replaceAll("\$1").trim()
				//TODO: to fix greedy match \w{1,7}
				//println "Flight info ==> $flightInfo"
				LOG.info("Flight info ==> {}", flightInfo);
				
				xml."$flightInfoTagName"() {
			"$flightNumberTagName"(matcher.replaceAll("\$2").trim()) //EK001
			"$dateOfMonthTagName"(matcher.replaceAll("\$3").trim()) //05
			"$airportCodeTagName"(matcher.replaceAll("\$4").trim()) //DXB
			"$finalMessageIndicatorTagName"(matcher.replaceAll("\$5").trim()) //FINAL
		}
		
		//	UWS_BLK_REGEX
		matcher = rawMessage =~ /(?ms).*UWS$(.*)BULK$.*/ ///(?ms)UWS$(.*)BULK$/
				def uwsInfo = matcher.replaceAll("\$1").trim()
				def uwsRecords = uwsInfo.split(NEW_LINE_REGEX)
				
				if(matcher.matches()) {
					//println "========> UWS - " + uwsRecords.length
					LOG.info("========> UWS - {}", uwsRecords.length);
					uwsRecords.each{ 
					input ->
						def isWt = input.matches(WT_INFO_REGEX);
						def isCotour = input.matches(COTOUR_REGEX) //PMD/Q7
						
						def cat1 = ""
						def cat2 = ""
						//Load Category 1
						if(input.matches(LOAD_1CAT_WITH_SHC_REGEX)) {
							cat1 = input.replaceAll(LOAD_1CAT_WITH_SHC_REGEX, "\$1")
						} else if(input.matches(LOAD_1CAT_NO_SHC_REGEX)) {
							cat1 = input.replaceAll(LOAD_1CAT_NO_SHC_REGEX, "\$1")
						} else if (input.matches(LOAD_2CAT_WITH_SHC_REGEX)) {
							cat1 = input.replaceAll(LOAD_2CAT_WITH_SHC_REGEX, "\$1")
							cat2 = input.replaceAll(LOAD_2CAT_WITH_SHC_REGEX, "\$2")
						} else if (input.matches(LOAD_2CAT_NO_SHC_REGEX)) {
							cat1 = input.replaceAll(LOAD_2CAT_NO_SHC_REGEX, "\$1")
							cat2 = input.replaceAll(LOAD_2CAT_NO_SHC_REGEX, "\$2")
						}
						
						
						if(input.matches(ULD_REGEX)) {
							
							//FIXME: temp fix to retrieve shc list => to use regex
							def inputArray = input.split("/");
							def last = inputArray[inputArray.length-1]
									int firstSeparatorOccurance = last.indexOf(".");
							def shcList = ""
									if(firstSeparatorOccurance != -1) {
										shcList = last.substring(firstSeparatorOccurance+1)
									}
							
							
							"$uwsTagName"() {
								"$ns:Raw"(input)
								"$uldTagName"(input.matches(ULD_NO_REGEX) ? input.replaceAll(ULD_NO_REGEX, "\$1") : "")
								"$destTagName"(input.matches(UWS_DEST_REGEX) ? input.replaceAll(UWS_DEST_REGEX, "\$1") : "")
								"$wtTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$2") : "")
								"$wtStatusTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$3") : "")
								"$cat1TagName"(cat1)
								"$cat2TagName"(cat2)
								"$contourTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$2") : "")
								"$contourNoTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$3") : "")
								"$shcTagName"(shcList)
								"$shcRmksTagName"("")
							}
						}
					}
					
				}
		
		//  BULK_BLK_REGEX
		matcher = rawMessage =~ /(?ms).*BULK$(.*)SI$.*/ ///(?ms)BULK$(.*)SI$/
				def bulkInfo = matcher.replaceAll("\$1").trim()
				def bulkRecords = bulkInfo.split(NEW_LINE_REGEX)
//		println "========> BULK Info - " + bulkInfo
				
				if(matcher.matches()) {
					//println "========> BULK - " + bulkRecords.length
					LOG.info("========> BULK - {}", bulkRecords.length);
					
					bulkRecords.eachWithIndex{
						input, index ->
						
						if(input.matches(BULK_REGEX)) {
							def isWt = input.matches(WT_INFO_REGEX);
							def isCotour = input.matches(COTOUR_REGEX) //PMD/Q7
							
							def cat1 = ""
							def cat2 = ""
							//Load Category 1
							if(input.matches(LOAD_1CAT_WITH_SHC_REGEX)) {
								cat1 = input.replaceAll(LOAD_1CAT_WITH_SHC_REGEX, "\$1")
							} else if(input.matches(LOAD_1CAT_NO_SHC_REGEX)) {
								cat1 = input.replaceAll(LOAD_1CAT_NO_SHC_REGEX, "\$1")
							} else if (input.matches(LOAD_2CAT_WITH_SHC_REGEX)) {
								cat1 = input.replaceAll(LOAD_2CAT_WITH_SHC_REGEX, "\$1")
								cat2 = input.replaceAll(LOAD_2CAT_WITH_SHC_REGEX, "\$2")
							} else if (input.matches(LOAD_2CAT_NO_SHC_REGEX)) {
								cat1 = input.replaceAll(LOAD_2CAT_NO_SHC_REGEX, "\$1")
								cat2 = input.replaceAll(LOAD_2CAT_NO_SHC_REGEX, "\$2")
							}
							
							//FIXME: temp fix to retrieve shc list => to use regex
							def inputArray = input.split("/");
							def last = inputArray[inputArray.length-1]
									int firstSeparatorOccurance = last.indexOf(".");
							def shcList = "";
							if(firstSeparatorOccurance != -1) {
								shcList = last.substring(firstSeparatorOccurance+1)
							}
							
							"$bulkTagName"() {
								"$ns:Raw"(input)
								"$uldTagName"("BULK")
								"$destTagName"(input.matches(BULK_DEST_REGEX) ? input.replaceAll(BULK_DEST_REGEX, "\$1") : "")
								"$wtTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$2") : "")
								"$wtStatusTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$3") : "")
								"$cat1TagName"(cat1)
								"$cat2TagName"(cat2)
								"$contourTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$2") : "")
								"$contourNoTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$3") : "")
								"$shcTagName"(shcList)
								"$shcRmksTagName"("")
							}
						}
					}
				}
		
//  SI_BLK_REGEX
//		matcher = rawMessage =~ /(?ms).*SI$(.*)<\/$ns:UWSInfo>$.*/ 
				def siBlockRegex = "(?ms).*SI\$(.*)</$ns:UWSInfo>\$.*"
				def siInfo = rawMessage.replaceAll(siBlockRegex, "\$1").trim()
				def siRecords = siInfo.split(NEW_LINE_REGEX)
				
				if(rawMessage.matches(siBlockRegex)) {
					//println "========> SI - " + siRecords.length
					LOG.info("========> SI - {}", siRecords.length);
					
					siRecords.eachWithIndex{
						input, index ->
						
						def siMatcher = input =~ /\s*ULD\sNO\s(.*)\sRMK\sQRTX(.*\d*[a-zA-Z]?)\s?.*/ //"\\s*ULD\\sNO\\s(.*)\\sRMK\\sQRTX(.*)\\s?.*"
								if(siMatcher.matches()) {
									def uldNo = siMatcher.replaceAll('$1')
											def transferFlight = siMatcher.replaceAll('$2').split(" ")[0] //TODO: temp fix to workaround as it's very bad style
													"$siTagName"() {
										"$uldTagName"(uldNo)
										"$rampTansferFlnoTagName"(transferFlight)
									}
								}
						
						//TODO: code refactor
						def siMatcherNoQRTX = input =~ /\s*ULD\sNO\s(.*)\sRMK.*/ 
								if(siMatcherNoQRTX.matches()) {
									if(!input.contains("QRTX")) {
										def uldNo = siMatcherNoQRTX.replaceAll('$1')
												"$siTagName"() {
											"$uldTagName"(uldNo)
											"$rampTansferFlnoTagName"("")
										}
									}
								}
					}
				}
		
	}
	
	def convertedXml = rawMessage.replaceAll(uwsOnlyBlockRegex, writer.toString()).trim()
	convertedXml
}

