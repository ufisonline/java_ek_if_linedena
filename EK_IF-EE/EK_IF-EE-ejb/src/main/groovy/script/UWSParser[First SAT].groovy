import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xml = new MarkupBuilder(writer)

def ULD_NO_REGEX = ".*-(\\w*)/.*";
def UWS_DEST_REGEX = ".*/(\\w{3})/.*";
def WT_INFO_REGEX = ".*/((\\d{1,5})([A|P]))/.*";
def LOAD_CAT_REGEX = ".*/(((?i)[a-z]?)((?i)[a-z]?))\\.?.*"; //572A/MQ.
def COTOUR_REGEX = ".*\\.(((?i)[a-z]{1,3})/(\\w{1,2}))\\.?.*"; //shc conflict 
//def SHC_REGEX = ".*(\\.((?i)[a-z]{1,3})+(/.*{1,6})?).*"; //cotour conflict
def SHC_REGEX_PREFIX1 = ".*/((\\d{1,5})([A|P]))";
def SHC_REGEX_PREFIX2 = "/(((?i)[a-z]?)((?i)[a-z]?))\\.?"; //572A/MQ.
def SHC_REGEX_PREFIX3 = "(((?i)[a-z]{1,3})/(\\w{1,2}))?\\.?"; // PMD/Q7
def SHC_REGEX = SHC_REGEX_PREFIX1 + SHC_REGEX_PREFIX2 + SHC_REGEX_PREFIX3 + "(((?i)[a-z]{1,3})+(/.*{1,6})?).*"; //cotour conflict

def NEW_LINE_REGEX = "(\n\r|\r\n|\n)"

def ULD_REGEX = "\\s*-[a-zA-Z]{3}.*";

//Bulk
def BULK_DEST_REGEX = "\\s*-(\\w{3})/.*";

def uwsTagName = "p:UWSULDDetails"
def bulkTagName = "p:BULKULDDetails"
def siTagName = "p:SIULDDetails"

def uldTagName = "p:ULDNumber"
def destTagName = "p:Dest3"
def wtTagName = "p:Weight"
def wtStatusTagName = "p:WeightStatus"
def cat1TagName = "p:LoadCat1"
def cat2TagName = "p:LoadCat2"
def contourTagName = "p:LoadContourCode"
def contourNoTagName = "p:LoadContourNumber"
def shcTagName = "p:ShcList"
def shcRmksTagName = "p:ShcRemarks"

def rampTansferFlnoTagName = "p:RampTransferFlno"

def flightInfoTagName = "p:FlightInfo"
def flightNumberTagName = "p:FlightNumber"
def dateOfMonthTagName = "p:DateOfMonth"
def airportCodeTagName = "p:AirportCode"
def finalMessageIndicatorTagName = "p:FinalMessageIndicator"

def matcher;

xml."p:UWSInfo"() {
	
	//	FLIGHT_REGEX
	matcher = rawMessage =~ /(?ms).*((\w{6})\/(\d{2})\.([a-zA-Z]{3})\.([a-zA-Z]{1,5}))$.*/ ///EK0001/05.DXB.FINAL
	def flightInfo = matcher.replaceAll("\$1").trim()
	//TODO: to fix greedy match \w{1,7}
	println "Flight info ==> $flightInfo"
	
	xml."$flightInfoTagName"() {
		"$flightNumberTagName"(matcher.replaceAll("\$2").trim()) //EK001
		"$dateOfMonthTagName"(matcher.replaceAll("\$3").trim()) //05
		"$airportCodeTagName"(matcher.replaceAll("\$4").trim()) //DXB
		"$finalMessageIndicatorTagName"(matcher.replaceAll("\$5").trim()) //FINAL
	}
	
	//	UWS_BLK_REGEX
		matcher = rawMessage =~ /(?ms).*UWS$(.*)BULK$.*/ ///(?ms)UWS$(.*)BULK$/
		def uwsInfo = matcher.replaceAll("\$1").trim()
		def uwsRecords = uwsInfo.split(NEW_LINE_REGEX)
		println "========> UWS - " + uwsRecords.length
		
		if(matcher.matches()) {
			uwsRecords.each{ 
				input ->
				def isWt = input.matches(WT_INFO_REGEX);
				def isCotour = input.matches(COTOUR_REGEX) //PMD/Q7
				def isLoadCategory = input.matches(LOAD_CAT_REGEX)
				
				if(input.matches(ULD_REGEX)) {
					
					//FIXME: temp fix to retrieve shc list => to use regex
					def inputArray = input.split("/");
					def last = inputArray[inputArray.length-1]
							int firstSeparatorOccurance = last.indexOf(".");
					def shcList = ""
							if(firstSeparatorOccurance != -1) {
								shcList = last.substring(firstSeparatorOccurance+1)
							}
					
					
					"$uwsTagName"() {
						"p:Raw"(input)
						"$uldTagName"(input.matches(ULD_NO_REGEX) ? input.replaceAll(ULD_NO_REGEX, "\$1") : "")
						"$destTagName"(input.matches(UWS_DEST_REGEX) ? input.replaceAll(UWS_DEST_REGEX, "\$1") : "")
						"$wtTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$2") : "")
						"$wtStatusTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$3") : "")
						"$cat1TagName"(isLoadCategory ? input.replaceAll(LOAD_CAT_REGEX, "\$2") : "")
						"$cat2TagName"(isLoadCategory ? input.replaceAll(LOAD_CAT_REGEX, "\$3") : "")
						"$contourTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$2") : "")
						"$contourNoTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$3") : "")
						"$shcTagName"(shcList)
						"$shcRmksTagName"(input.matches(SHC_REGEX) ? input.replaceAll(SHC_REGEX, "\$12") : "")
					}
				}
			}
			
		}
	
	//  BULK_BLK_REGEX
		matcher = rawMessage =~ /(?ms).*BULK$(.*)SI$.*/ ///(?ms)BULK$(.*)SI$/
		def bulkInfo = matcher.replaceAll("\$1").trim()
		def bulkRecords = bulkInfo.split(NEW_LINE_REGEX)
//		println "========> BULK Info - " + bulkInfo
		println "========> BULK - " + bulkRecords.length
		
		if(matcher.matches()) {
			bulkRecords.eachWithIndex{
				input, index ->
				def isWt = input.matches(WT_INFO_REGEX);
				def isCotour = input.matches(COTOUR_REGEX) //PMD/Q7
						def isLoadCategory = input.matches(LOAD_CAT_REGEX)
						
						//FIXME: temp fix to retrieve shc list => to use regex
						def inputArray = input.split("/");
				def last = inputArray[inputArray.length-1]
						int firstSeparatorOccurance = last.indexOf(".");
				def shcList = "";
				if(firstSeparatorOccurance != -1) {
					shcList = last.substring(firstSeparatorOccurance+1)
				}
				
				"$bulkTagName"() {
					"p:Raw"(input)
					"$uldTagName"("BULK")
					"$destTagName"(input.matches(BULK_DEST_REGEX) ? input.replaceAll(BULK_DEST_REGEX, "\$1") : "")
					"$wtTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$2") : "")
					"$wtStatusTagName"(isWt ? input.replaceAll(WT_INFO_REGEX, "\$3") : "")
					"$cat1TagName"(isLoadCategory ? input.replaceAll(LOAD_CAT_REGEX, "\$2") : "")
					"$cat2TagName"(isLoadCategory ? input.replaceAll(LOAD_CAT_REGEX, "\$3") : "")
					"$contourTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$2") : "")
					"$contourNoTagName"(isCotour ? input.replaceAll(COTOUR_REGEX, "\$3") : "")
					"$shcTagName"(shcList)
					"$shcRmksTagName"(input.matches(SHC_REGEX) ? input.replaceAll(SHC_REGEX, "\$12") : "")
				}
			}
		}
		
//  SI_BLK_REGEX
	matcher = rawMessage =~ /(?ms).*SI$(.*)<\/p:UWSInfo>$.*/ 
	def siInfo = matcher.replaceAll("\$1").trim()
	def siRecords = siInfo.split(NEW_LINE_REGEX)
	println "========> SI - " + siRecords.length
	
	if(matcher.matches()) {
		siRecords.eachWithIndex{
			input, index ->
			
			def siMatcher = input =~ /\s*ULD\sNO\s(.*)\sRMK\sQRTX(.*\d*[a-zA-Z]?)\s?.*/ //"\\s*ULD\\sNO\\s(.*)\\sRMK\\sQRTX(.*)\\s?.*"
					if(siMatcher.matches()) {
						def uldNo = siMatcher.replaceAll('$1')
								def transferFlight = siMatcher.replaceAll('$2').split(" ")[0] //TODO: temp fix to workaround as it's very bad style
										"$siTagName"() {
							"$uldTagName"(uldNo)
							"$rampTansferFlnoTagName"(transferFlight)
						}
					}
			
			//TODO: code refactor
			def siMatcherNoQRTX = input =~ /\s*ULD\sNO\s(.*)\sRMK.*/ 
			if(siMatcherNoQRTX.matches()) {
				if(!input.contains("QRTX")) {
					def uldNo = siMatcherNoQRTX.replaceAll('$1')
							"$siTagName"() {
						"$uldTagName"(uldNo)
						"$rampTansferFlnoTagName"("")
					}
				}
			}
		}
	}
	
}


matcher = rawMessage =~ /(?ms)<p:UWSInfo>(.*)<\/p:UWSInfo>/ 
def convertedXml = matcher.replaceAll(writer.toString()).trim()
convertedXml
