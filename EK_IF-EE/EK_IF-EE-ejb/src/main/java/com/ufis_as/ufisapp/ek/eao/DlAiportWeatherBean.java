package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.lido.entities.EntDbAirportWeather;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author btr
 * Session Bean implementation class DlAiportWeatherBean 
 * */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlAiportWeatherBean extends DlAbstractBean<EntDbAirportWeather>{

	public static final Logger LOG = LoggerFactory.getLogger(DlAiportWeatherBean.class);
	  
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DlAiportWeatherBean() {
    	super(EntDbAirportWeather.class);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public EntDbAirportWeather merge(EntDbAirportWeather entity){
    	EntDbAirportWeather weather = null;
    	try{
    		weather = em.merge(entity);
    		//em.flush();
    	} catch (OptimisticLockException Oexc) {
    		LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
    	}catch(Exception ex){
    		LOG.error("ERROR to create new airpot weather!!! {}", ex.toString());
    	}
    	return weather;
    }
    
    public void persist(EntDbAirportWeather entity){
    	try{
    		em.persist(entity);
    		//em.flush();
    	} catch (OptimisticLockException Oexc) {
    		LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
    	}catch(Exception ex){
    		LOG.error("ERROR to create new airpot weather!!! {}", ex.toString());
    	}
    }
    
    public EntDbAirportWeather findExisting(String icao, String iata){
    	EntDbAirportWeather result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbAirportWeather.findByKey");
    	try{
    		query.setParameter("iata", iata);
    		query.setParameter("icao", icao);
	    	List<EntDbAirportWeather> resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Input data will be updated to ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Input Airpot Weather record will be added as new record.");
		}
		catch(Exception ex){
			LOG.debug("ERROR when retrieving existing Airpot Weather records : {}", ex.toString());
		}
    	return result;
    }
    
    public EntDbAirportWeather findExistingByIcaoWeatherIndent(String icao, String weatherIndent){
    	EntDbAirportWeather result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbAirportWeather.findByIcaoWeatherIndent");
    	try{
    		query.setParameter("icao", icao);
    		query.setParameter("weatherIndent", weatherIndent);
	    	List<EntDbAirportWeather> resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Input weather will be updated to ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Input weather will be added as new record.");
		}
		catch(Exception ex){
			LOG.debug("ERROR when retrieving existing Airpot Weather records : {}", ex.toString());
		}
    	return result;
    }
}
