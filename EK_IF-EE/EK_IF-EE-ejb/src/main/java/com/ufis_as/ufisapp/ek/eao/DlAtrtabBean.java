package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.lido.entities.EntDbAtrtab;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author btr
 * Session Bean implementation class DlAtrtabBean 
 * */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlAtrtabBean extends DlAbstractBean<EntDbAtrtab>{

	public static final Logger LOG = LoggerFactory.getLogger(DlAtrtabBean.class);
	  
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DlAtrtabBean() {
    	super(EntDbAtrtab.class);
    }

    public EntDbAtrtab findByMsid(String msid){
    	EntDbAtrtab result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbAtrtab.findByRestAndMsid");
    	try{
    		query.setParameter("rest", HpEKConstants.EK_LIDO_NOTAMS);
    		query.setParameter("msid", msid);
	    	List<EntDbAtrtab> resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Record is found for NOTAMS MSID : {}.", msid);
	    	}
	    	else
	    		LOG.debug("Record is not found for NOTAMS MSID : {}.", msid);
		}
		catch(Exception ex){
			LOG.debug("ERROR when retrieving existing NOTAMS records in ATRTAB: {}", ex.toString());
		}
    	return result;
    }
}
