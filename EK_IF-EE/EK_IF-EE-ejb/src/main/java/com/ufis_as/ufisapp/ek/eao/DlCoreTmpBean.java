package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.core.ICoreTmpBean;
import com.ufis_as.ek_if.core.entities.EntDbCoreTmp;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.core.flightevent.FlightEvent.FlightId;

/**
 * Session Bean implementation class DlCoreTmpBean
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class DlCoreTmpBean 
		extends DlAbstractBean<EntDbCoreTmp> 
		implements ICoreTmpBean {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DlCoreTmpBean.class);
    
	/**
	 * CDI
	 */
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;
    @Resource
    private EJBContext _context;
    
    private HpUfisCalendar ufisCalendar = new HpUfisCalendar();

	public DlCoreTmpBean() {
		super(EntDbCoreTmp.class);
	}
    
	@Override
	protected EntityManager getEntityManager() {
		return _em;
	}
    
    /**
     * Find temp message by id from db temp table
     * @param id (FlightId)
     * @return
     */
	@Override
    public List<EntDbCoreTmp> getMsgById(FlightId id) {
    	List<EntDbCoreTmp> result = null;
    	try {
    		StringBuilder sb = new StringBuilder();
    		sb.append("SELECT e ")
    			.append("FROM EntDbCoreTmp e ")
    			.append("WHERE e.cxcd    = :cxcd ")
    			.append("AND e.fltNum    = :fltNum ")
    			.append("AND e.fltDate   = :fltDate ")
    			.append("AND e.arrStn    = :arrStn ")
    			.append("AND e.depStn    = :depStn ");
    		if (HpUfisUtils.isNotEmptyStr(id.getFltSuffix())) {
    			sb.append("AND e.fltSuffix = :fltSuffix ");
    		}
	    	Query query = _em.createQuery(sb.toString());
	    	query.setParameter("cxcd", id.getCxCd());
	    	query.setParameter("fltNum", id.getFltNum());
	    	if (id.getFltDate().getValue() != null) {
	    		ufisCalendar.setTime(id.getFltDate().getValue());
	    		query.setParameter("fltDate", ufisCalendar.getCedaDateString());
	    	}
	    	query.setParameter("arrStn", id.getArrStn());
	    	query.setParameter("depStn", id.getDepStn());
	    	if (HpUfisUtils.isNotEmptyStr(id.getFltSuffix())) {
	    		query.setParameter("fltSuffix", id.getFltSuffix());
	    	}
	    	result = query.getResultList();
	    	if (result != null && result.size() > 0) {
	    		LOG.debug("{} record found for flight [{}, {}, {}] from temp table", 
	    				new Object[]{
	    					result.size(),
	    					id.getCxCd(),
	    					id.getFltNum() + id.getFltSuffix(),
	    					id.getFltDate().getValue()});
	    	}
    	} catch (Exception e) {
    		LOG.error("Cxcd={}, fltNum={}, fltSuffix={}, fltDate={}, arrStn={}, depStn={}",
    					new Object[] {
    						id.getCxCd(), 
    						id.getFltNum(), 
    						id.getFltSuffix(), 
    						id.getFltDate().getValue(), 
    						id.getArrStn(), 
    						id.getDepStn()});
    		LOG.error("Cannot find message from temp table: {}", e);
    	}
    	return result;
    }

	@Override
	public List<EntDbCoreTmp> getMsgChainById(FlightId id) {
		List<EntDbCoreTmp> results = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT e ")
				.append("FROM EntDbCoreTmp e ")
				.append("WHERE e.cxcd    = :cxcd ")
				.append("AND e.fltNum    = :fltNum ")
				.append("AND e.fltDate   = :fltDate ")
				.append("AND (e.arrStn  != :hopo ")
				.append("OR e.depStn    != :hopo) ")
				.append("AND e.rec_status != :status ");
			if (HpUfisUtils.isNotEmptyStr(id.getFltSuffix())) {
    			sb.append("AND e.fltSuffix = :fltSuffix ");
    		}
			sb.append("ORDER BY e.legNum, e.depNum desc");
	    	Query query = _em.createQuery(sb.toString());
	    	query.setParameter("cxcd", id.getCxCd());
	    	query.setParameter("fltNum", id.getFltNum());
	    	if (id.getFltDate().getValue() != null) {
	    		ufisCalendar.setTime(id.getFltDate().getValue());
	    		query.setParameter("fltDate", ufisCalendar.getCedaDateString());
	    	}
	    	query.setParameter("status", 'X');
	    	query.setParameter("hopo", HpEKConstants.HOPO);
	    	if (HpUfisUtils.isNotEmptyStr(id.getFltSuffix())) {
	    		query.setParameter("fltSuffix", id.getFltSuffix());
	    	}
	    	results = query.getResultList();
	    	if (results != null && results.size() > 0) {
	    		LOG.debug("{} record found for flight chain[{}, {}, {}] from temp table", 
	    				new Object[]{
	    					results.size(),
	    					id.getCxCd(),
	    					id.getFltNum() + id.getFltSuffix(),
	    					id.getFltDate().getValue()});
	    	}
    	} catch (Exception e) {
    		LOG.error("Cxcd={}, fltNum={}, fltSuffix={}, fltDate={}",
    					new Object[] {
    						id.getCxCd(), 
    						id.getFltNum(), 
    						id.getFltSuffix(), 
    						id.getFltDate().getValue()});
    		LOG.error("Cannot find flight chains message from temp table: {}", e);
    	}
    	return results;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updMsgStatus(FlightId criteria, boolean isChainUpd, Character status) {
		StringBuilder strBld = new StringBuilder();
		strBld.append("UPDATE EntDbCoreTmp ");
		strBld.append("SET rec_status = :rec_status ");
		strBld.append("WHERE cxcd = :cxcd ");
		strBld.append("AND fltNum = :fltNum ");
		strBld.append("AND fltDate = :fltDate ");
		if (HpUfisUtils.isNotEmptyStr(criteria.getFltSuffix())) {
			strBld.append("AND fltSuffix = :fltSuffix ");
		}
		if (!isChainUpd) {
			strBld.append("AND depStn = :depStn ");
			strBld.append("AND arrStn = :arrStn ");
		}
		Query query = _em.createQuery(strBld.toString());
		query.setParameter("rec_status", status);
    	query.setParameter("cxcd", criteria.getCxCd());
    	query.setParameter("fltNum", criteria.getFltNum());
    	if (HpUfisUtils.isNotEmptyStr(criteria.getFltSuffix())) {
    		query.setParameter("fltSuffix", criteria.getFltSuffix());
    	}
    	if (criteria.getFltDate().getValue() != null) {
    		ufisCalendar.setTime(criteria.getFltDate().getValue());
    		query.setParameter("fltDate", ufisCalendar.getCedaDateString());
    	}
    	if (!isChainUpd) {
    		query.setParameter("depStn", criteria.getDepStn());
    		query.setParameter("arrStn", criteria.getArrStn());
    	}
//    	UserTransaction tx = _context.getUserTransaction();
    	try {
//			tx.begin();
			query.executeUpdate();
//			tx.commit();
		} catch (Exception e) {
//			try {
//				tx.rollback();
//				LOG.error("Exception: {}", e);
//			} catch (Exception ex) {
				LOG.error("Exception: {}", e);
//			}
		}
	}


}
