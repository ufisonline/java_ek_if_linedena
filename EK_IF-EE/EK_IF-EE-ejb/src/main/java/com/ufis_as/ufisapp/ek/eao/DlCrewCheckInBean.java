package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.bms.entities.EntDbCrewCheckin;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author btr
 * Session Bean implementation class DlCrewCheckInBean 
 * */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlCrewCheckInBean extends DlAbstractBean<EntDbCrewCheckin>{

	public static final Logger LOG = LoggerFactory.getLogger(EntDbCrewCheckin.class);
	  
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DlCrewCheckInBean() {
    	super(EntDbCrewCheckin.class);
    }
    
    public EntDbCrewCheckin merge(EntDbCrewCheckin entity){
    	EntDbCrewCheckin crewImmigration = null;
    	try{
    		crewImmigration = em.merge(entity);
    		//em.flush();
    	} catch (OptimisticLockException Oexc) {
    		LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
    	}catch(Exception ex){
    		LOG.error("ERROR to create new crew check in!!! {}", ex.toString());
    	}
    	return crewImmigration;
    }
    
    public EntDbCrewCheckin getExistingCrewCheckIn(String flNum, String legNum, String depNum,
    		String staffNum, Date flightDate){
    	EntDbCrewCheckin result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewCheckin.findByKey");
    	try{
    		query.setParameter("flNum", flNum);
    		query.setParameter("flightDate", flightDate);
	    	query.setParameter("depNum", depNum);
	    	query.setParameter("legNum", legNum);
	    	query.setParameter("staffNum", staffNum);
	    	List<EntDbCrewCheckin> resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Input data will be updated to existing Crew CheckIn record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew CheckIn record is not existing. Input data will be added as a new record.");
		}
		catch(Exception ex){
			LOG.debug("ERROR when retrieving existing crew check in records : {}", ex.toString());
		}
    	return result;
    }
    
    public EntDbCrewCheckin getExistingByIdFlight(BigDecimal idFlight, String staffNumber,
    		String staffCode, String idFltJobAssign){
    	List<EntDbCrewCheckin> resultList = null;
    	EntDbCrewCheckin result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewCheckin.findByIdFlightAndStaffNum");
    	try{
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("staffType", staffCode);
	    	query.setParameter("staffNumber", staffNumber);
	    	query.setParameter("idFltJobAssign", idFltJobAssign);
	    	query.setParameter("recStatus", "X");
	    	
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Data will be updated to existing Crew check in record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew check in record is not existing.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving existing crew check in records : {}", ex.toString());
    	}
    	return result;
    }
    
    public EntDbCrewCheckin getExistingByIdJobAssign(BigDecimal idFlight, String idFltJobAssign){
    	List<EntDbCrewCheckin> resultList = null;
    	EntDbCrewCheckin result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewCheckin.findByIdFlightAndIdFltJobAssign");
    	try{
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("idFltJobAssign", idFltJobAssign);
	    	query.setParameter("recStatus", "X");
	    	
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Data will be updated to existing Crew checkin record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew check in record is not existing to delete.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving existing crew check in records : {}", ex.toString());
    	}
    	return result;
    }
}
