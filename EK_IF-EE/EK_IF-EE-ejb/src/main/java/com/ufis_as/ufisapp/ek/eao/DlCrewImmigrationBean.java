package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.iams.entities.EntDbCrewImmigration;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author btr
 * Session Bean implementation class DlCrewImmigrationBean */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlCrewImmigrationBean extends DlAbstractBean<EntDbCrewImmigration>{

	public static final Logger LOG = LoggerFactory.getLogger(EntDbCrewImmigration.class);
	  
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DlCrewImmigrationBean() {
    	super(EntDbCrewImmigration.class);
    }
    
    public EntDbCrewImmigration merge(EntDbCrewImmigration entity){
    	EntDbCrewImmigration crewImmigration = null;
    	try{
    		crewImmigration = em.merge(entity);
    		//em.flush();
    	} catch (OptimisticLockException Oexc) {
    		LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
    	}catch(Exception ex){
    		LOG.error("ERROR to create new crew immigration!!! {}", ex.toString());
    	}
    	return crewImmigration;
    }
    
    public EntDbCrewImmigration getExistingCrewImmigration(String flNum, Date flightDate, String depNum, 
    		String legNum, String staffNum, String gateType){
    	List<EntDbCrewImmigration> resultList = null;
    	EntDbCrewImmigration result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewImmigration.findByPk");
    	try{
	    	query.setParameter("flNum", flNum);
	    	query.setParameter("flightDate", flightDate);
	    	query.setParameter("depNum", depNum);
	    	query.setParameter("legNum", legNum);
	    	query.setParameter("staffNum", staffNum);
	    	query.setParameter("gateType", gateType);
	    	query.setParameter("recStatus", "X");
	    	
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Input data will be updated to existing Crew immigration record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew immigration record is not existing. Input data will be added as a new record.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving existing crew immigration records : {}", ex.toString());
    	}
    	return result;
    }
    
    public EntDbCrewImmigration getExistingByIdFlight(BigDecimal idFlight, String staffNumber,
    		String staffCode, String idFltJobAssign){
    	List<EntDbCrewImmigration> resultList = null;
    	EntDbCrewImmigration result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewImmigration.findByIdFlightAndStaffNum");
    	try{
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("crewTypeCode", staffCode);
	    	query.setParameter("staffNumber", staffNumber);
	    	query.setParameter("idFltJobAssign", idFltJobAssign);
	    	query.setParameter("recStatus", "X");
	    	
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Data will be updated to existing Crew immigration record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew immigration record is not existing.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving existing crew immigration records : {}", ex.toString());
    	}
    	return result;
    }
    
    public EntDbCrewImmigration getExistingByIdJobAssign(BigDecimal idFlight, String idFltJobAssign){
    	List<EntDbCrewImmigration> resultList = null;
    	EntDbCrewImmigration result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbCrewImmigration.findByIdFlightAndIdFltJobAssign");
    	try{
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("idFltJobAssign", idFltJobAssign);
	    	query.setParameter("recStatus", "X");
	    	
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Data will be updated to existing Crew immigration record's ID : {}.", result.getId());
	    	}
	    	else
	    		LOG.debug("Crew immigration record is not existing to delete.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving existing crew immigration records : {}", ex.toString());
    	}
    	return result;
    }
    
    //@NamedQuery(name = "EntDbCrewImmigration.updateIdFltJobAssignDeleted", query = "UPDATE EntDbCrewImmigration e SET  e.recStatus = :recStatus WHERE e.idFltJobAssign = :idFltJobAssign")
    
    public void updateIdFltJobAssignDeleted(String idFltJobAssign){
    	
    	Query query = getEntityManager().createNamedQuery("EntDbCrewImmigration.updateIdFltJobAssignDeleted");
    	try{
	    	query.setParameter("idFltJobAssign", idFltJobAssign);
	    	query.setParameter("recStatus", "X");
	    	query.executeUpdate();
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when updating the deleted id_flt_job_assign : {}", ex.toString());
    	}
    }
}
