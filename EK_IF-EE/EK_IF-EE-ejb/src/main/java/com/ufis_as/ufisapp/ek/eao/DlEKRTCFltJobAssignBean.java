package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlEKRTCFltJobAssignBean extends DlAbstractBean<EntDbFltJobAssign>{
	private static final Logger LOG = LoggerFactory.getLogger(DlEKRTCFltJobAssignBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;
    
    public DlEKRTCFltJobAssignBean() {
        super(EntDbFltJobAssign.class);
    }

    public DlEKRTCFltJobAssignBean(Class<EntDbFltJobAssign> entityClass) {
		super(EntDbFltJobAssign.class);
	}

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public EntDbFltJobAssign getExsitRecord(EntDbFltJobAssign entDbFltJobAssign){
    	EntDbFltJobAssign existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobAssign.findRecord");
		query.setParameter("idFltJobTask", entDbFltJobAssign.getIdFltJobTask());
		query.setParameter("idStaffShift", entDbFltJobAssign.getIdStaffShift());
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbFltJobAssign)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    public EntDbFltJobAssign getExsitRecord(BigDecimal idFltJobTask, BigDecimal idStaffShift){
    	EntDbFltJobAssign existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobAssign.findRecord");
		query.setParameter("idFltJobTask", idFltJobTask);
		query.setParameter("idStaffShift", idStaffShift);
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbFltJobAssign)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    
    public EntDbFltJobAssign getExsitRecord(String idFltJobTask, BigDecimal shiftId){
    	EntDbFltJobAssign existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobAssign.findRecordByIdFltJobTaskAndShiftId");
		query.setParameter("idFltJobTask", idFltJobTask); 
		query.setParameter("shiftId", shiftId);
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbFltJobAssign)resultList.get(0);
		}

    	return existRecord;
    }
    
    public List<EntDbFltJobAssign> getExsitRecord(String idFltJobTask){
    	List<EntDbFltJobAssign> existRecordList = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobAssign.findRecordByIdFltJobTask");
		query.setParameter("idFltJobTask", idFltJobTask); 
		
		existRecordList = query.getResultList();

    	return existRecordList;
    }
    
    public List<EntDbFltJobAssign> getExsitRecordX(String idFltJobTask){
	    List<EntDbFltJobAssign> resultList = null;
    	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbFltJobAssign> cq = cb.createQuery(EntDbFltJobAssign.class);
		Root<EntDbFltJobAssign> r = cq.from(EntDbFltJobAssign.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("idFltJobTask"), idFltJobTask);
//		cq.where(predicate);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("idFltJobTask"), idFltJobTask));
		  filters.add(cb.notEqual(r.get("recStatus"),"X"));
//		  filters.add(cb.not(cb.in(r.get("recStatus")).value('X')));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
//		  List<Predicate> filters = new LinkedList<>();
//		  filters.add(cb.equal(r.get("shiftId"), shiftId));
//		  filters.add(cb.equal(r.get("idFltJobTask"), idFltJobTask));	
//		cq.where(cb.and(filters.toArray(new Predicate[0])));
		 resultList = em.createQuery(cq).getResultList();
		
//		if (resultList != null && resultList.size() > 0){
//			result = resultList.get(0);;
//		}
		
		return resultList;
    }
    
    public List<EntDbFltJobAssign> getExsitRecordByShiftIdX(BigDecimal shiftId){
    	List<EntDbFltJobAssign> resultList = null;
    	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbFltJobAssign> cq = cb.createQuery(EntDbFltJobAssign.class);
		Root<EntDbFltJobAssign> r = cq.from(EntDbFltJobAssign.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("shiftId"), shiftId);
//		cq.where(predicate);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("shiftId"), shiftId));
//		  filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		
//		  List<Predicate> filters = new LinkedList<>();
//		  filters.add(cb.equal(r.get("shiftId"), shiftId));
////		  filters.add(cb.equal(r.get("intRefNumber"), paxRefNum));	
//		cq.where(cb.and(filters.toArray(new Predicate[0])));
		resultList = em.createQuery(cq).getResultList();
		
//		if (resultList != null && resultList.size() > 0){
//			result = resultList.get(0);;
//		}
		
		return resultList;
    }
    
    


    public void Persist(EntDbFltJobAssign entDbFltJobAssign){
    	if (entDbFltJobAssign != null){
    		try{
    			entDbFltJobAssign.setDataSource("EK-RTC");
    			entDbFltJobAssign.setCreatedUser("EK-RTC");
    			entDbFltJobAssign.setRecStatus(" ");
    			entDbFltJobAssign.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
    		em.persist(entDbFltJobAssign); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbFltJobAssign, e);
    		}
    	}
    }

 
    public void Update(EntDbFltJobAssign entDbFltJobAssign){
    	if (entDbFltJobAssign != null){
    		try{
    		em.merge(entDbFltJobAssign); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbFltJobAssign, e);
    		}
    	}
    }
}
