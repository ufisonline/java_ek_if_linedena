package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlEKRTCFltJobTaskBean extends DlAbstractBean<EntDbFltJobTask>{

	private static final Logger LOG = LoggerFactory.getLogger(DlEKRTCFltJobTaskBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;
    
    public DlEKRTCFltJobTaskBean() {
        super(EntDbFltJobTask.class);
    }

    public DlEKRTCFltJobTaskBean(Class<EntDbFltJobTask> entityClass) {
		super(EntDbFltJobTask.class);
	}

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public EntDbFltJobTask getExsitRecordByJobAssignShiftId(BigDecimal shiftId){
    	EntDbFltJobTask existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobTask.findRecordByJobAssignShiftID");
		query.setParameter("shiftId", shiftId);
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbFltJobTask)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    
    public EntDbFltJobTask getExsitRecord(EntDbFltJobTask entDbFltJobTask){
    	EntDbFltJobTask existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbFltJobTask.findRecord");
		query.setParameter("taskId", entDbFltJobTask.getTaskId());
		query.setParameter("staffType", entDbFltJobTask.getStaffType());
		query.setParameter("idFlight", entDbFltJobTask.getIdFlight());
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbFltJobTask)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    public EntDbFltJobTask getExsitRecordX(BigDecimal taskId){
    	EntDbFltJobTask result = null;
    	
    		CriteriaBuilder cb = em.getCriteriaBuilder();
    		 CriteriaQuery<EntDbFltJobTask> cq = cb.createQuery(EntDbFltJobTask.class);
    		Root<EntDbFltJobTask> r = cq.from(EntDbFltJobTask.class);
    		cq.select(r);
//    		Predicate predicate = cb.equal(r.get("taskId"), taskId);
//    		cq.where(predicate);
    		
    		  List<Predicate> filters = new LinkedList<>();
    		  filters.add(cb.equal(r.get("taskId"), taskId));
//    		  filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
    		  filters.add(cb.notEqual(r.get("recStatus"),"X"));
    		cq.where(cb.and(filters.toArray(new Predicate[0])));
    		List<EntDbFltJobTask> resultList = em.createQuery(cq).getResultList();
    		
    		if (resultList != null && resultList.size() > 0){
    			result = resultList.get(0);
    		}
    		
    		return result;
    }


    public void Persist(EntDbFltJobTask entDbFltJobTask){
    	if (entDbFltJobTask != null){
    		try{
    			entDbFltJobTask.setDataSource("EK-RTC");
    			entDbFltJobTask.setCreatedUser("EK-RTC");
    			entDbFltJobTask.setRecStatus(" ");
    			entDbFltJobTask.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
    		em.persist(entDbFltJobTask); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbFltJobTask, e);
    		}
    	}
    }

 
    public void Update(EntDbFltJobTask entDbFltJobTask){
    	if (entDbFltJobTask != null){
    		try{
    		em.merge(entDbFltJobTask); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbFltJobTask, e);
    		}
    	}
    }

}
