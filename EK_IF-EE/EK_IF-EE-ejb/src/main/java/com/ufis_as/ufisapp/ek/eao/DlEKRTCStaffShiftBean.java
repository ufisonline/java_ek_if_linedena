package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlEKRTCStaffShiftBean extends DlAbstractBean<EntDbStaffShift>{

	private static final Logger LOG = LoggerFactory.getLogger(DlEKRTCStaffShiftBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;
    
    public DlEKRTCStaffShiftBean() {
        super(EntDbStaffShift.class);
    }

    public DlEKRTCStaffShiftBean(Class<EntDbStaffShift> entityClass) {
		super(EntDbStaffShift.class);
	}

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public EntDbStaffShift getExsitRecord(EntDbStaffShift entDbStaffShift){
    	EntDbStaffShift existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbStaffShift.findRecord");
		query.setParameter("shiftId", entDbStaffShift.getShiftId());
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbStaffShift)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    public EntDbStaffShift getExsitRecordByShiftIdX(BigDecimal shiftId){
    	EntDbStaffShift result = null;
    	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbStaffShift> cq = cb.createQuery(EntDbStaffShift.class);
		Root<EntDbStaffShift> r = cq.from(EntDbStaffShift.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("shiftId"), shiftId);
//		cq.where(predicate);
		
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("shiftId"), shiftId));
//		  filters.add(cb.not(cb.in(r.get("recStatus")).value('X')));
		  filters.add(cb.notEqual(r.get("recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));

		List<EntDbStaffShift> resultList = em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
		}
		
		return result;
    }
    
    public List<EntDbStaffShift> getExsitRecordByidFltJboTaskX(String idFltJobTask){
    	List<EntDbStaffShift> resultList = null;
    	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbStaffShift> cq = cb.createQuery(EntDbStaffShift.class);
		Root<EntDbStaffShift> r = cq.from(EntDbStaffShift.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("idFltJobTask"), idFltJobTask);
//		cq.where(predicate);	
		
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("idFltJobTask"), idFltJobTask));
//		  filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));

		resultList = em.createQuery(cq).getResultList();
		
//		if (resultList != null && resultList.size() > 0){
//			result = resultList.get(0);;
//		}
		
		return resultList;
    }
    
    public EntDbStaffShift getExsitRecord(BigDecimal shiftId){
    	EntDbStaffShift existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbStaffShift.findRecord");
		query.setParameter("shiftId", shiftId);
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbStaffShift)resultList.get(0);
		}
    	
    	return existRecord;
    }
    
    public EntDbStaffShift getExsitRecord(String idFltJobTask, BigDecimal shiftId){
    	EntDbStaffShift existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbStaffShift.findRecordByIdFltJobTaskAndShiftIdAndShiftId");
		query.setParameter("idFltJobTask", idFltJobTask);
		query.setParameter("shiftId", shiftId);
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbStaffShift)resultList.get(0);
		}
	
    	return existRecord;
    }
    
    public List<EntDbStaffShift> getExsitRecord(String idFltJobTask){
    	List<EntDbStaffShift> existRecordList = null;
    	
    	Query query = em.createNamedQuery("EntDbStaffShift.findRecordByIdFltJobTask");
		query.setParameter("idFltJobTask", idFltJobTask);
		
		existRecordList = query.getResultList();
	
    	return existRecordList;
    }


    public void Persist(EntDbStaffShift entDbStaffShift){
    	if (entDbStaffShift != null){
    		try{
    			entDbStaffShift.setDataSource("EK-RTC");
    			entDbStaffShift.setCreatedUser("EK-RTC");
    			entDbStaffShift.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
    			entDbStaffShift.setRecStatus(" ");
    		em.persist(entDbStaffShift); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbStaffShift, e);
    		}
    	}
    }

 
    public void Update(EntDbStaffShift entDbStaffShift){
    	if (entDbStaffShift != null){
    		try{
    		em.merge(entDbStaffShift); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbStaffShift, e);
    		}
    	}
    }
    

}
