package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @author $Author: jgo $
 * @version $Revision: 
 */
@Stateless(name = "DlFlightIdMappingBean")
@LocalBean
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DlFlightIdMappingBean {

    private static final Logger LOG = LoggerFactory.getLogger(DlFlightIdMappingBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;

	public EntDbFlightIdMapping getFlightIdMapping(String intFltId) {
		EntDbFlightIdMapping mapping = null;
		Query query = _em.createNamedQuery("EntDbFlightIdMapping.findByFlid");
		query.setParameter("intFlId", intFltId);
		
		List<EntDbFlightIdMapping> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}", result.size(), intFltId);
			mapping = result.get(0);
		}
		return mapping;
	}
	
	public EntDbFlightIdMapping getFlightIdMappingX(String intFltId) {
		EntDbFlightIdMapping result = null;
		
		CriteriaBuilder cb = _em.getCriteriaBuilder();
		CriteriaQuery<EntDbFlightIdMapping> cq = cb.createQuery(EntDbFlightIdMapping.class);
		Root<EntDbFlightIdMapping> r = cq.from(EntDbFlightIdMapping.class);
		cq.select(r);
//		List<Predicate> filters = new LinkedList<>();
//		filters.add(cb.equal(r.get("intFltId"), intFltId));
//		filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		Predicate predicate = cb.equal(r.get("intFltId"), intFltId);
		cq.where(predicate);
//		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbFlightIdMapping> resultList = _em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = (EntDbFlightIdMapping)resultList.get(0);
			
			if (resultList.size() >1){
				LOG.warn("Attention ! {} duplicate records found for FlightIdMapping, intFltId {}", resultList.size(), intFltId);
			}
		}
		
		return result;
	}
	
	public EntDbFlightIdMapping getFlightIdMappingX(String airlineCode, String flightNumber, String flightNumberSuffice,  Date scheduledFlightDatetime) {
		EntDbFlightIdMapping result = null;
		Timestamp scheduledFlightDatetimeTs =new Timestamp(scheduledFlightDatetime.getTime());
		CriteriaBuilder cb = _em.getCriteriaBuilder();
		 CriteriaQuery<EntDbFlightIdMapping> cq = cb.createQuery(EntDbFlightIdMapping.class);
		Root<EntDbFlightIdMapping> r = cq.from(EntDbFlightIdMapping.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("airlineCode2"), airlineCode));
		  filters.add(cb.equal(r.get("flightNumber"), flightNumber));
//		  filters.add(cb.equal(r.get("flightNumberSuffix"), flightNumberSuffice));
		  filters.add(cb.equal(r.get("scheduledflightdatetime"), scheduledFlightDatetimeTs));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbFlightIdMapping> resultList = _em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = (EntDbFlightIdMapping)resultList.get(0);
		}
		
		return result;
	}
	
	public EntDbFlightIdMapping getFlightIdMappingX(String fltNumber,  Date scheduledFlightDatetimeLocal, Character arrDepFlag) {
		EntDbFlightIdMapping result = null;
		Timestamp scheduledFlightDatetimeTs =new Timestamp(scheduledFlightDatetimeLocal.getTime());
		CriteriaBuilder cb = _em.getCriteriaBuilder();
		 CriteriaQuery<EntDbFlightIdMapping> cq = cb.createQuery(EntDbFlightIdMapping.class);
		Root<EntDbFlightIdMapping> r = cq.from(EntDbFlightIdMapping.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("fltNumber"), fltNumber));
		  filters.add(cb.equal(r.get("arrDepFlag"), arrDepFlag));
//		  filters.add(cb.equal(r.get("flightNumber"), flightNumber));
//		  filters.add(cb.equal(r.get("flightNumberSuffix"), flightNumberSuffice));
		  filters.add(cb.equal(r.get("fltDateLocal"), scheduledFlightDatetimeTs));
		cq.where(cb.and(filters.toArray(new Predicate[0])));


		List<EntDbFlightIdMapping> resultList = _em.createQuery(cq).getResultList();

		if (resultList != null && resultList.size() > 0){
			result = (EntDbFlightIdMapping)resultList.get(0);
			if (resultList.size() >1){
				LOG.warn("Attention ! {} duplicate records found for FlightIdMapping, fltNumber {}, scheduledFlightDatetime {}", resultList.size(), fltNumber, scheduledFlightDatetimeTs);
			}
		}
		
		return result;
	}
	
	public EntDbFlightIdMapping getMappingByFlightInfo(EntDbAfttab queryCriteria) {
		EntDbFlightIdMapping mapping = null;
		Query query = _em.createNamedQuery("EntDbFlightIdMapping.findByAftCriteria");
		//query.setParameter("flightId", String.valueOf(criteria.getUrno()));
		//query.setParameter("alc2", criteria.getAlc2());
		//query.setParameter("alc3", criteria.getAlc3());
		query.setParameter("adid", queryCriteria.getAdid());
		query.setParameter("flno", queryCriteria.getFlno());
		//query.setParameter("fltn", criteria.getFltn());
		//query.setParameter("flns", criteria.getFlns());
		HpUfisCalendar schedule = null;
		try {
			if ('A' == queryCriteria.getAdid()) {
				if (HpUfisUtils.isNotEmptyStr(queryCriteria.getStoa())) {
					schedule = new HpUfisCalendar(queryCriteria.getStoa());
				}
			} else {
				if (HpUfisUtils.isNotEmptyStr(queryCriteria.getStod())) {
					schedule = new HpUfisCalendar(queryCriteria.getStod());
				}
			}
		} catch (Exception e) {
			LOG.error("Cannot parse schedule time: {}", e);
		}
		
		if (schedule != null) {
			query.setParameter("schDate", schedule.getTime());
		} else {
			query.setParameter("schDate", null);
		}
		
		List<EntDbFlightIdMapping> result = query.getResultList();
		if (result != null && result.size() > 0) {
			mapping = result.get(0);
		}
		return mapping;
	}
	
	public String getIntFlightIdByFlightId(String idFlight){
		String intFliId = null;
		Query query = _em.createNamedQuery("EntDbFlightIdMapping.getMappingByFlightId");
		query.setParameter("idFlight", new BigDecimal(idFlight));
		
		List<String> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}", result.size(), idFlight);
			if (result.size() != 1){
				LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
			}
			intFliId = result.get(0);
		}
		return intFliId;
	}
	
	public EntDbFlightIdMapping getMappingByFlightIdX(BigDecimal idFlight){
		EntDbFlightIdMapping result = null;
		
		CriteriaBuilder cb = _em.getCriteriaBuilder();
		 CriteriaQuery<EntDbFlightIdMapping> cq = cb.createQuery(EntDbFlightIdMapping.class);
		Root<EntDbFlightIdMapping> r = cq.from(EntDbFlightIdMapping.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("idFlight"), idFlight));
//		  filters.add(cb.equal(r.get("intRefNumber"), paxRefNum));	
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbFlightIdMapping> resultList = _em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
		}
		
		return result;
	}
	
	public List<EntDbFlightIdMapping> getMappingByFlightId(){
		List<EntDbFlightIdMapping> result = null;
		Query query = _em.createQuery("SELECT e FROM EntDbFlightIdMapping e WHERE e.fltDate <= :fltEndDate AND e.fltDate >= :fltStartDate");
		Date currentDate = new Date();
		query.setParameter("fltStartDate", DateUtils.addHours(currentDate, -6));
		query.setParameter("fltEndDate", DateUtils.addHours(currentDate, -2));

//		LOG.info("Query:" + query.toString());

		result = query.getResultList();
		LOG.info("Total {} flights ", result.size());
		LOG.info("between {} and {} found for pax summarizing", DateUtils.addHours(currentDate, -6), DateUtils.addHours(currentDate, -2));
		
		return result;

	}
	
	public String getUfisFlightId(String intFlId){
		String flightId = null;
		Query query = _em.createNamedQuery("EntDbFlightIdMapping.getFlightIdByIntFlId");
		query.setParameter("intFlId", intFlId);
		
		List<BigDecimal> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}", result.size(), flightId);
			if (result.size() != 1){
				LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
			}
			flightId = String.valueOf(result.get(0));
		}
		return flightId;
	}
	
	public String getUfisFlightId(String airlineCode, String flightNumber,
			String flightNumberSuffice, Date scheduledFlightDatetime){
		String flightId = null;
//		Query query = _em.createNamedQuery("EntDbFlightIdMapping.getFlightIdByOtherInfo");
//		query.setParameter("airlineCode2", airlineCode);
//		query.setParameter("flightNumber", flightNumber);
//		query.setParameter("flightNumberSuffix", flightNumberSuffice);
//		query.setParameter("scheduledflightdatetime", scheduledFlightDatetime);
//		
//		List<String> result = query.getResultList();
//		if (result != null && result.size() > 0) {
//			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}", result.size(), flightId);
//			if (result.size() != 1){
//				LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
//			}
//			flightId = result.get(0);
//		}
		return flightId;
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(EntDbFlightIdMapping entity){
    	_em.persist(entity); 	
    }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EntDbFlightIdMapping update(EntDbFlightIdMapping entity) {
        try {
        	entity = _em.merge(entity);
        } catch (OptimisticLockException Oexc) {
            //getEntityManager().refresh(entity);
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
        return entity;
    }
}
