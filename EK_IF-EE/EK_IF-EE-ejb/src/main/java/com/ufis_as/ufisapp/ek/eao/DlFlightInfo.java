/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbFlightInfo;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author $Author$
 * @version $Revision$
 */
@Stateless(name = "DlFlightInfo")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFlightInfo extends DlAbstractBean<EntDbFlightInfo> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlFlightInfo.class);
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;

    public DlFlightInfo() {
        super(EntDbFlightInfo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return _em;
    }

    public EntDbFlightInfo getFlightInfo(String intFlId) {
        EntDbFlightInfo result = null;
        Query query = getEntityManager().createNamedQuery("EntDbFlightInfo.findByFlid");
        query.setParameter("intFlId", intFlId);

        List<EntDbFlightInfo> list = query.getResultList();
        if (list != null && list.size() > 0) {
            LOG.debug("{} record found for EntDbFlightInfo  by intFlId={}", list.size(), intFlId);
            result = list.get(0);
        }
        return result;
    }
}
