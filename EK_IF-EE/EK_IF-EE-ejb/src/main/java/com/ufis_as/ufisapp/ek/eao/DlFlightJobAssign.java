package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlFlightJobAssign")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFlightJobAssign extends DlAbstractBean<Object> implements IDlFlighttJobAssignLocal {

	public DlFlightJobAssign() {
		super(Object.class);
	}

	private static final Logger LOG = LoggerFactory.getLogger(DlFlightJobAssign.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbFltJobAssign merge(EntDbFltJobAssign fltJobAssign) {
		EntDbFltJobAssign result = null;
		try {
			result = em.merge(fltJobAssign);
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}",
					fltJobAssign, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", fltJobAssign, exc);
		}
		return result;
	}

	@Override
	public List<EntDbFltJobAssign> findByIdFltJobTask(String idFltJobTask) {
		List<EntDbFltJobAssign> entFltJobAssign = null;
		//String[] staffTypeList = new String[]{"ROP", "SPHL"};
	//	String staffType = "'ROP', 'SPHL'";
		try{
			Query query = em.createNamedQuery("EntDbFltJobAssign.findByIdFltJobTask");
			query.setParameter("idFltJobTask", idFltJobTask);
			query.setParameter("recStatus", " ");
			//query.setParameter("staffType", staffType);
			
			entFltJobAssign = query.getResultList();
			if(entFltJobAssign.size()>0){
				LOG.debug("Total Flight job Assign <{}> is found.", entFltJobAssign.size());
			}
			else
				LOG.debug("Flight job Assign are not found for idFltJobTask <{}>.", idFltJobTask);
		}catch(Exception ex){
	    		LOG.debug("ERROR when retrieving existing flt_job_Assign: {}", ex.toString());
		}
		return entFltJobAssign;
	}
	
	@Override
	public EntDbFltJobAssign getAssignedCrewIdByFlight(BigDecimal idFlight, String staffNum, String crewType) {

		EntDbFltJobAssign entResult = null;
		try {
			Query query = em.createNamedQuery("EntDbFltJobAssign.findAssignedCrewByFlidAndStaffNum");
			query.setParameter("idFlight", idFlight);
			query.setParameter("staffNumber", staffNum);
			query.setParameter("staffTypeCode", HpEKConstants.CREW_STAFF_TYPE_CODE);
			query.setParameter("staffType", crewType);
			query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
			query.setParameter("recStatus", "X");
			List<EntDbFltJobAssign> resultList = query.getResultList();
			if (resultList.size() > 0) {
				entResult = resultList.get(0);
				// LOG.debug("Assigned crew for flight record's ID : <{}>",
				// entResult.getId());
			} else
				LOG.debug("Flight does not have any assigned crew.");
		} catch (Exception ex) {
			LOG.error("ERROR when retrieving assigned crews by flight. {}", ex);
		}
		return entResult;
	}
	@Override
	public List<EntDbFltJobAssign> getAssignedCrewsByFltId(long fltId) {
		BigDecimal flightId=new BigDecimal(fltId);
		Query query = em.createNamedQuery("EntDbFltJobAssign.findByFltId");
		query.setParameter("idFlight", flightId);
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		
		try {
			List<EntDbFltJobAssign> result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobAssign ");
			if(!result.isEmpty()){
				LOG.info(" {} Records found in FLT_JOB_ASSIGN." , result.size());
				return result;
			}
			else
			{
				LOG.info("No record exist in FLT_JOB_ASSIGN for Flight ID: {}" , fltId);
			}

		} catch (Exception e) {
			LOG.error("Error on retrieving entities from FLT_JOB_ASSIGN for Flight ID: " + fltId + " error:{}", e.getMessage());
		}
		return null;
	}
	
	@Override
	public List<EntDbFltJobAssign> getAssignedCrewsByFltIdCrewType(long fltId,String crewType) {
		BigDecimal flightId=new BigDecimal(fltId);
		Query query = em.createNamedQuery("EntDbFltJobAssign.findByFltIdCrewType");
		query.setParameter("idFlight", flightId);
		query.setParameter("staffType", crewType);
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		
		try {
			List<EntDbFltJobAssign> result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobAssign ");
			if(!result.isEmpty()){
				LOG.info(" {} Records found in FLT_JOB_ASSIGN." , result.size());
				return result;
			}
			else
			{
				LOG.info("No record exist in FLT_JOB_ASSIGN for Flight ID: {} and StaffType: {}" , fltId,crewType);
			}

		} catch (Exception e) {
			LOG.error("Error on retrieving entities from FLT_JOB_ASSIGN for Flight ID: " + fltId + " error:{}", e.getMessage());
		}
		return null;
	}	
	
//	@Override
//	public EntDbFltJobAssign findCrewFlight(String fltNum, long fltId, Date fltDate, String staffNum) {
//		EntDbFltJobAssign entity = null;
//		Query query = em
//				.createQuery("SELECT e FROM EntDbFltJobAssign e WHERE e.flightNumber = :fltNum AND e.idFlight=:fltId AND e.fltDate=:fltDate AND e.staffNumber=:staffNumber AND e.dataSource=:dataSource AND e.recStatus <> :recStatus");
//		query.setParameter("fltNum", fltNum);
//		query.setParameter("fltId", fltId);
//		query.setParameter("fltDate", fltDate);
//		query.setParameter("staffNumber", staffNum);
//		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
//		query.setParameter("recStatus", "X");
//
//		try {
//			entity = (EntDbFltJobAssign) query.getSingleResult();
//		} catch (Exception e) {
//			LOG.debug("Cannot find crew info By using StaffNum and FlightNum on FltDate: {}", staffNum + "  " + fltNum + " " + fltDate);
//		}
//		return entity;
//	}
}
