package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @author btr
 */

@Stateless(name = "DlFltConxSummaryBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFltConxSummaryBean {

    private static final Logger LOG = LoggerFactory.getLogger(DlFltConxSummaryBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;
	
	protected EntityManager getEntityManager() {
		return _em;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void persist(EntDbFltConnectSummary entity){
    	_em.persist(entity); 	
    }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public EntDbFltConnectSummary merge(EntDbFltConnectSummary entity) {
        try {
        	entity = _em.merge(entity);
        } catch (OptimisticLockException Oexc) {
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
        return entity;
    }
    
    public EntDbFltConnectSummary findExisting(BigDecimal idArrFlight, BigDecimal idDepFlight){
    	EntDbFltConnectSummary ent = null;
    	try{
    		Query query = _em.createNamedQuery("EntDbFltConnectSummary.findExisting");
    		query.setParameter("idArrFlight", idArrFlight);
    		query.setParameter("idDepFlight", idDepFlight);
    		List<EntDbFltConnectSummary> resultList = query.getResultList();
			if (resultList.size() > 0) {
				ent = resultList.get(0);
			} else {
				LOG.debug("FltConnectSummary record not found");
			}
    	}
    	catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return ent;
    }
    
    public List<EntDbFltConnectSummary> findExistingForFlights(BigDecimal idFlight){
    	try{
    		Query query = _em.createNamedQuery("EntDbFltConnectSummary.findExistingForFlights");
    		query.setParameter("idFlight", idFlight);
    		return query.getResultList();
    	} catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
    
    public List<FltConnectionDTO> findExistingForArrival(BigDecimal idFlight){
    	try{
    		Query query = _em.createNamedQuery("EntDbFltConnectSummary.findExistingForArrival");
    		query.setParameter("idFlight", idFlight);
    		List<EntDbFltConnectSummary> summaryList = query.getResultList();
    		List<FltConnectionDTO> list = new ArrayList<>();
    		for (EntDbFltConnectSummary summary : summaryList) {
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdConxFlight(summary.getIdDepFlight());
				list.add(dto);
			}
    		return list;
    	} catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
    
    public List<FltConnectionDTO> findExistingForDeparture(BigDecimal idFlight){
    	try{
    		Query query = _em.createNamedQuery("EntDbFltConnectSummary.findExistingForDeparture");
    		query.setParameter("idFlight", idFlight);
    		List<EntDbFltConnectSummary> summaryList =  query.getResultList();
    		List<FltConnectionDTO> list = new ArrayList<>();
    		for (EntDbFltConnectSummary summary : summaryList) {
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdConxFlight(summary.getIdDepFlight());
				list.add(dto);
			}
    		return list;
    	} catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
    
    public List<EntDbFltConnectSummary> findExistingWithNoConxFlights(boolean isUld, boolean isBag, boolean isPax, BigDecimal idFlight, BigDecimal idConxFlight){
    	try{
    		if(!isUld && !isBag && !isPax) {
    			LOG.error("Summary Type must be either one of ULD, Bag or Pax. Wrong Type.");
    			return Collections.EMPTY_LIST;
    		}
    		
    		String namedQueryName = "";
    		if(isUld) {
    			namedQueryName = "EntDbFltConnectSummary.findExistingWithNoConxFlightsForULD";
    		} else if(isBag) {
    			namedQueryName = "EntDbFltConnectSummary.findExistingWithNoConxFlightsForBag";
    		} else if(isPax) {
    			namedQueryName = "EntDbFltConnectSummary.findExistingWithNoConxFlightsForPax";
    		}
    		
    		Query query = _em.createNamedQuery(namedQueryName);
    		query.setParameter("idArrFlight", idFlight);
    		query.setParameter("idDepFlight", idConxFlight);
    		return query.getResultList();
    	} catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
    
    public List<EntDbFltConnectSummary> findExistingForAllFlights(BigDecimal idFlight, BigDecimal idConxFlight){
    	try{
    		Query query = _em.createNamedQuery("EntDbFltConnectSummary.findExistingForAllFlights");
    		query.setParameter("idArrFlight", idFlight);
    		query.setParameter("idDepFlight", idConxFlight);
    		return query.getResultList();
    	} catch(Exception ex){
    		LOG.error("Error when retrieving existing flight connected summary : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
}