package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.core.entities.EntDbFlightDaily;
import com.ufis_as.ek_if.core.entities.EntFlightDailyDTO;

/**
 * Session Bean implementation class DlFltDailyBean
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DlFltDailyBean {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(DlFltDailyBean.class);

	/**
	 * CDI
	 */
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	/**
	 * Constructor
	 */
	public DlFltDailyBean() {

	}

	public EntDbFlightDaily getFltDaily(EntFlightDailyDTO dto) {
		EntDbFlightDaily entity = null;
		try {
			Query query = em
					.createNamedQuery("EntDbFlightDaily.findByFlightInfo");
			query.setParameter("flno", dto.getFlightNumber());
			query.setParameter("org3", dto.getFltOrg3());
			query.setParameter("des3", dto.getFltDes3());
			query.setParameter("legNum", dto.getLegNum());
			query.setParameter("flut", dto.getFlut());

			List<EntDbFlightDaily> list = query.getResultList();
			if (list != null && list.size() > 0) {
				if (list.size() == 1) {
					entity = list.get(0);
				} else {
					LOG.warn("No unique records found by: {}", dto);
				}
			} else {
				LOG.debug("No record found by: {}", dto);
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return entity;
	}

	public List<String> getFltDailyIds(EntFlightDailyDTO dto) {
		List<String> list = new ArrayList<>();
		try {
			Query query = em
					.createNamedQuery("EntDbFlightDaily.findIdsByFlightInfo");
			query.setParameter("flno", dto.getFlightNumber());
			query.setParameter("org3", dto.getFltOrg3());
			query.setParameter("des3", dto.getFltDes3());
			query.setParameter("flut", dto.getFlut());
			list = query.getResultList();
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return list;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void persist(EntDbFlightDaily entity) {
		em.persist(entity);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EntDbFlightDaily update(EntDbFlightDaily entity) {
		try {
			entity = em.merge(entity);
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", entity,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", entity, exc);
		}
		return entity;
	}

	// ACTS flight search
	public String getFltDailyId(EntFlightDailyDTO dto) {
		List<BigDecimal> list = new ArrayList<>();
		String flId = null;
		try {
			Query query = em
					.createNamedQuery("EntDbFlightDaily.findIdByFltInfoSobtLoc");
			query.setParameter("flno", dto.getFlightNumber());
			query.setParameter("org3", dto.getFltOrg3());
			query.setParameter("des3", dto.getFltDes3());
			query.setParameter("sobtLoc",dto.getFlda());
			list = query.getResultList();
			//LOG.info("Result size: " + list.size());
			//LOG.info("Result: " + (list.size() != 0 ? list.get(0) : null));
			if (list != null && !list.isEmpty() && list.get(0)!=null) {
				flId = list.get(0).toString();
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return flId;
	}

}
