package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.egds.entities.EntDbFlightFuelStatus;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class DlFltFuelStatusBean
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class DlFltFuelStatusBean {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DlFltFuelStatusBean.class);
    
	/**
	 * CDI
	 */
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;
	
	public DlFltFuelStatusBean() {

    }

	public EntDbFlightFuelStatus findByIdFlight(BigDecimal idFlight) {
		EntDbFlightFuelStatus  entity = null;
		try {
			Query query = _em.createNamedQuery("EntDbFlightFuelStatus.findByIdFlight");
			query.setParameter("idFlight", idFlight);
			
			List<EntDbFlightFuelStatus> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entity = list.get(0);
			} else {
				LOG.debug("Flight fuel status is not found for idFlight : <{}>", idFlight);
			}
		} catch (Exception e) {
			LOG.error("Exception idFlight:{} , Error:{}", idFlight, e);
		}
		return entity;
	}
	
	public EntDbFlightFuelStatus findByIdRFlight(BigDecimal idRFlight) {
		EntDbFlightFuelStatus  entity = null;
		try {
			Query query = _em.createNamedQuery("EntDbFlightFuelStatus.findByIdRFlight");
			query.setParameter("idRFlight", idRFlight);
			
			List<EntDbFlightFuelStatus> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entity = list.get(0);
			} else {
				LOG.debug("Flight fuel status is not found for id_RFlight : <{}>", idRFlight);
			}
		} catch (Exception e) {
			LOG.error("Exception id_RFlight:{} , Error:{}", idRFlight, e);
		}
		return entity;
	}
	
	public EntDbFlightFuelStatus findByFlight(String alc2, String fltn, String flns, String flda) {
		EntDbFlightFuelStatus entity = null;
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("SELECT a FROM EntDbFlightFuelStatus a WHERE a.airlineCode2 =:alc2 ")
				.append(" AND a.flightNumber =:fltn ")
				.append(" AND a.fltDate =:flda ");
			if (HpUfisUtils.isNotEmptyStr(flns)) {
				sb.append(" AND a.fltSuffix =:flns");
			}
			Query query = _em.createQuery(sb.toString());
			query.setParameter("alc2", alc2);
			query.setParameter("fltn", fltn);
			query.setParameter("flda", flda);
//			if (HpUfisUtils.isNotEmptyStr(flns)) {
//				query.setParameter("flns", flns.charAt(0));
//			}
			List<EntDbFlightFuelStatus> list = query.getResultList();
			if (list != null && list.size() >0) {
				entity = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}
		return entity;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EntDbFlightFuelStatus persist(EntDbFlightFuelStatus entity){
    	try {
    		_em.persist(entity);
        } catch (OptimisticLockException Oexc) {
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
    	return entity;
    }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EntDbFlightFuelStatus update(EntDbFlightFuelStatus entity) {
        try {
        	entity = _em.merge(entity);
        } catch (OptimisticLockException Oexc) {
            //getEntityManager().refresh(entity);
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
        return entity;
    }

}
