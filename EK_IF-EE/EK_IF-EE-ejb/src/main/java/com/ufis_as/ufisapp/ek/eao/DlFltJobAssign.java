package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlFltJobAssign")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFltJobAssign extends DlAbstractBean<Object> implements IDlFltJobAssignLocal {

	public DlFltJobAssign() {
		super(Object.class);
	}

	private static final Logger LOG = LoggerFactory.getLogger(DlFltJobAssign.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	public List<EntDbFltJobAssign> getAssignedCrews(String fltNum, long fltId, Date fltDate) {
		LOG.debug("retrieving flight Assignments from FltJobAssign.");
		Query query = em
				.createQuery("SELECT e FROM EntDbFltJobAssign e WHERE e.flightNumber = :fltNum AND e.idFlight=:fltId AND e.fltDate=:fltDate AND e.dataSource=:dataSource AND e.recStatus <> :recStatus");
		query.setParameter("fltNum", fltNum);
		query.setParameter("fltId", fltId);
		query.setParameter("fltDate", fltDate);
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");
		LOG.info("Query:" + query.toString());
		List<EntDbFltJobAssign> result = null;
		try {
			result = query.getResultList();
			LOG.info("found " + result.size() + " EntDbFltJobAssign ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from FLT_JOB_ASSIGN for Flight ID: " + fltId + " error:", e.getMessage());
		}
		return null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean saveCrewFlightAssign(EntDbFltJobAssign fltJobAssign) {

		em.persist(fltJobAssign);

		return true;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateCrewFlightAssign(EntDbFltJobAssign fltJobAssign) {

		try {
			// LOG.debug("Merging the chnages:"+fltJobAssign.getId()+"Staff Number:"+fltJobAssign.getStaffNumber());
			em.merge(fltJobAssign);

			// LOG.debug("Merge Successfull");

		} catch (OptimisticLockException Oexc) {
			// em.flush();
			LOG.error("OptimisticLockException Entity:{} , Error:{}", fltJobAssign, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", fltJobAssign, exc);
		}
		// return upFltJobAssign;
	}

	@Override
	public EntDbFltJobAssign findCrewFlight(String fltNum, long fltId, Date fltDate, String staffNum) {
		EntDbFltJobAssign entity = null;
		Query query = em
				.createQuery("SELECT e FROM EntDbFltJobAssign e WHERE e.flightNumber = :fltNum AND e.idFlight=:fltId AND e.fltDate=:fltDate AND e.staffNumber=:staffNumber AND e.dataSource=:dataSource AND e.recStatus <> :recStatus");
		query.setParameter("fltNum", fltNum);
		query.setParameter("fltId", fltId);
		query.setParameter("fltDate", fltDate);
		query.setParameter("staffNumber", staffNum);
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");

		try {
			entity = (EntDbFltJobAssign) query.getSingleResult();
		} catch (Exception e) {
			LOG.debug("Cannot find crew info By using StaffNum and FlightNum on FltDate: {}", staffNum + "  " + fltNum + " " + fltDate);
		}
		return entity;
	}

	@Override
	public EntDbFltJobAssign getAssignedCrewIdByFlight(BigDecimal idFlight, String staffNum) {

		EntDbFltJobAssign entResult = null;
		try {
			Query query = em.createNamedQuery("EntDbFltJobAssign.findAssignedCrewByFlidAndStaffNum");
			query.setParameter("idFlight", idFlight.longValue());
			query.setParameter("staffNumber", staffNum);
			query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
			query.setParameter("recStatus", "X");
			List<EntDbFltJobAssign> resultList = query.getResultList();
			if (resultList.size() > 0) {
				entResult = resultList.get(0);
				// LOG.debug("Assigned crew for flight record's ID : <{}>",
				// entResult.getId());
			} else
				LOG.debug("Flight does not have any assigned crew.");
		} catch (Exception ex) {
			LOG.error("ERROR when retrieving assigned crews by flight. {}", ex);
		}
		return entResult;
	}

	@Override
	public List<EntDbFltJobAssign> getAssignedCrewsByFltIdDesOrg(String flightNumber, Date fltDate, String org3, String des3) {
		Query query = em.createNamedQuery("EntDbFltJobAssign.findByFltIdDetails");
		query.setParameter("fltNum", flightNumber);
		query.setParameter("fltDate", fltDate);
		query.setParameter("org3", org3);
		query.setParameter("des3", des3);
		query.setParameter("idFlight", new Long(0).longValue());
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		List<EntDbFltJobAssign> result = null;
		try {
			result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobAssign ");
			return result;

		} catch (Exception e) {
			LOG.error("Error on retrieving entities from FLT_JOB_ASSIGN for Flight ID: " + flightNumber + " " + fltDate + " error:", e.getMessage());
		}
		return null;
	}

	@Override
	public List<EntDbFltJobAssign> getAssignedCrewsByFltId(long fltId) {
		Query query = em.createNamedQuery("EntDbFltJobAssign.findByFltId");
		query.setParameter("idFlight", fltId);
		query.setParameter("dataSource", HpEKConstants.ACTS_DATA_SOURCE);
		query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		List<EntDbFltJobAssign> result = null;
		try {
			result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobAssign ");
			return result;

		} catch (Exception e) {
			LOG.error("Error on retrieving entities from FLT_JOB_ASSIGN for Flight ID: " + fltId + " error:", e.getMessage());
		}
		return null;
	}

}
