package com.ufis_as.ufisapp.ek.eao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobSummary;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFltJobSummary extends DlAbstractBean<Object> implements
		IDlFltJobSummaryLocal {

	public DlFltJobSummary(Class<Object> entityClass) {
		super(entityClass);
		// TODO Auto-generated constructor stub
	}

	public DlFltJobSummary() {
		super(Object.class);
	}

	private static final Logger LOG = LoggerFactory
			.getLogger(DlFltJobSummary.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbFltJobSummary saveFltJobSummary(EntDbFltJobSummary fltJobSummary) {
		EntDbFltJobSummary summryRec = null;
		try {
			summryRec = em.merge(fltJobSummary);
			// em.flush();
		} catch (OptimisticLockException Oexc) {
			// em.flush();
			LOG.error("OptimisticLockException Entity:{} , Error:{}",
					fltJobSummary, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", fltJobSummary, exc);
		}
		return summryRec;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbFltJobSummary updateFlightJobSummary(
			EntDbFltJobSummary fltJobSummary) {
		EntDbFltJobSummary summryRec = null;
		try {
			// LOG.debug("Merging the chnages:"+fltJobSummary.getId());
			summryRec = em.merge(fltJobSummary);
			// em.flush();
			// LOG.debug("Merge Successfull");
		} catch (OptimisticLockException Oexc) {
			// em.flush();
			LOG.error("OptimisticLockException Entity:{} , Error:{}",
					fltJobSummary, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", fltJobSummary, exc);
		}
		return summryRec;
	}

	@Override
	public List<EntDbFltJobSummary> getFltJobSummary(long id_flight) {
		Query query = em.createNamedQuery("EntDbFltJobSummary.findByFlId");
		query.setParameter("fltId", id_flight);
		//LOG.info("Query:" + query.toString());
		List<EntDbFltJobSummary> result = null;
		try {
			result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobSummary ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving records from FLT_JOB_SUMMARY for Flight ID:"
					+ id_flight + " error:", e.getMessage());
		}
		return result;
	}
	
	@Override
	public List<EntDbFltJobSummary> getFltJobSummaryByCrewType(long id_flight,String crewType) {
		Query query = em.createNamedQuery("EntDbFltJobSummary.findByFlIdCrewType");
		query.setParameter("fltId", id_flight);
		query.setParameter("resourceTypeCode",crewType);
		//LOG.info("Query:" + query.toString());
		List<EntDbFltJobSummary> result = null;
		try {
			result = query.getResultList();
			// LOG.trace("found " + result.size() + " EntDbFltJobSummary ");
			return result;

		} catch (Exception e) {
			LOG.error("Error on retrieving records from FLT_JOB_SUMMARY for Flight ID:"
					+ id_flight + " , CrewType: "+crewType+ ". Error:", e.getMessage());
		}
		return result;
	}
	
	@Override
	public EntDbFltJobSummary getFltJobSummaryRecByCrewType(long id_flight) {
		List<EntDbFltJobSummary> fltSummary =new ArrayList<EntDbFltJobSummary>();
		Query query = em.createNamedQuery("EntDbFltJobSummary.findCrewSumByFlIdCrewType");
		query.setParameter("fltId", id_flight);
		//query.setParameter("resourceTypeCode",null);
		//LOG.info("Query:" + query.toString());
		EntDbFltJobSummary result = null;
		try {
			fltSummary=query.getResultList();
			if(fltSummary!=null && !fltSummary.isEmpty()){
			result =fltSummary .get(0);
			return result;
			}
			// LOG.trace("found " + result.size() + " EntDbFltJobSummary ");			

		} catch (Exception e) {
			LOG.error("Error on retrieving record from FLT_JOB_SUMMARY for Flight ID:"
					+ id_flight + " . Error:", e.getMessage());
		}
		return result;
	}

	@Override
	public boolean deleteFltJobSummaryByCrewType(long fltId,String crewType) {
		List<EntDbFltJobSummary> fltSummary = getFltJobSummaryByCrewType(fltId,crewType);
		if (fltSummary != null && !fltSummary.isEmpty()) {
			for (EntDbFltJobSummary fltSumRec : fltSummary) {
				// fltSumRec.setRecStatus("X");
				// updateFlightJobSummary(fltSumRec);
				em.remove(fltSumRec);
			}
		} else {
			LOG.info("Summary records doesn't exist with Flight ID:" + fltId);
		}
		return true;
	}
	
	@Override
	public boolean deleteFltJobSummary(long fltId) {
		List<EntDbFltJobSummary> fltSummary = getFltJobSummary(fltId);
		if (fltSummary != null && !fltSummary.isEmpty()) {
			for (EntDbFltJobSummary fltSumRec : fltSummary) {
				// fltSumRec.setRecStatus("X");
				// updateFlightJobSummary(fltSumRec);
				em.remove(fltSumRec);
			}
		} else {
			LOG.info("Summary records doesn't exist with Flight ID:" + fltId);
		}
		return true;
	}
	
	
}
