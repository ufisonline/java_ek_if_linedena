package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlFltJobTask")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlFltJobTask extends DlAbstractBean<Object> implements IDlFltJobTaskLocal {

	public DlFltJobTask() {
		super(Object.class);
	}

	private static final Logger LOG = LoggerFactory.getLogger(DlFltJobTask.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	public EntDbFltJobTask merge(EntDbFltJobTask fltJobTask) {
		EntDbFltJobTask result = null;
		try {
			result = em.merge(fltJobTask);
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}",
					fltJobTask, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", fltJobTask, exc);
		}
		return result;
	}

	@Override
	public EntDbFltJobTask findExistingFltJobTask(BigDecimal idFlight,
			BigDecimal taskId, String staffType) {
		EntDbFltJobTask entFltJobTask = null;
		try{
			Query query = em.createNamedQuery("EntDbFltJobTask.findUniqueFltJobTask");
			query.setParameter("idFlight", idFlight);
			query.setParameter("staffType", staffType);
			query.setParameter("taskId", taskId);
			query.setParameter("recStatus", " ");
			
			List<EntDbFltJobTask> resultList = query.getResultList();
			if(resultList.size()>0){
				entFltJobTask = resultList.get(0);
				LOG.debug("Flight job task is found. ID <{}>", entFltJobTask.getId());
			}
			else
				LOG.debug("Flight job task is not found for idFlight <{}>.", idFlight);
		}catch(Exception ex){
	    		LOG.debug("ERROR when retrieving existing flt_job_task : {}", ex.toString());
		}
		return entFltJobTask;
	}
}
