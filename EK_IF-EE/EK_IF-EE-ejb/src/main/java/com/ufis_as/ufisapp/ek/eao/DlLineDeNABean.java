package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.linedena.entities.EntDbAircraftOpsMsg;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * 
 * @author SCH
 *
 */
@Stateless(name = "DlLineDeNABean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLineDeNABean extends DlAbstractBean<EntDbAircraftOpsMsg> {

	private static final Logger LOG = LoggerFactory.getLogger(DlLineDeNABean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;
    
    public DlLineDeNABean() {
        super(EntDbAircraftOpsMsg.class);
    }

    public DlLineDeNABean(Class<EntDbAircraftOpsMsg> entityClass) {
		super(EntDbAircraftOpsMsg.class);
	}

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public EntDbAircraftOpsMsg getExsitRecord(EntDbAircraftOpsMsg entDbAircraftOpsMsg, BigDecimal sequenceNumber){
    	EntDbAircraftOpsMsg existRecord = null;
    	
    	Query query = em.createNamedQuery("EntDbAircraftOpsMsg.findRecord");
		query.setParameter("idFlight", entDbAircraftOpsMsg.getIdFlight());
		query.setParameter("msgSeqnNum", sequenceNumber);
//		query.setParameter("closeMsgFlag", closeMsgFlag);
//		query.setParameter("fltDate", entDbAircraftOpsMsg.getFltDate());
//		query.setParameter("arrDepFlag", entDbAircraftOpsMsg.getArrDepFlag());
//		query.setParameter("fltRegn", entDbAircraftOpsMsg.getFltRegn());
		
		List<?> resultList = query.getResultList();
		if (resultList != null && resultList.size() > 0){
			existRecord =  (EntDbAircraftOpsMsg)resultList.get(0);
		}
    	
    	return existRecord;
    }


    public void Persist(EntDbAircraftOpsMsg entDbAircraftOpsMsg){
    	if (entDbAircraftOpsMsg != null){
    		try{
    		em.persist(entDbAircraftOpsMsg); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbAircraftOpsMsg, e);
    		}
    	}
    }

 
    public void Update(EntDbAircraftOpsMsg entDbAircraftOpsMsg){
    	if (entDbAircraftOpsMsg != null){
    		try{
    		em.merge(entDbAircraftOpsMsg); 	
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbAircraftOpsMsg, e);
    		}
    	}
    }
    
    public void UpdateSameRegnRecords(EntDbAircraftOpsMsg entDbAircraftOpsMsg){
    	Date aldt = entDbAircraftOpsMsg.getAldt();
    	Date atot = entDbAircraftOpsMsg.getAtot();
    	BigDecimal idFlight = entDbAircraftOpsMsg.getIdFlight();
    	StringBuilder queryStr = new StringBuilder();
    	String adid = entDbAircraftOpsMsg.getArrDepFlag();
//    	if ("A".equalsIgnoreCase(adid)){
//    		queryStr.append("SELECT a FROM EntDbAircraftOpsMsg a WHERE a.fltRegn = :fltRegn AND ( trim(a.aldt) ='' OR a.aldt is NULL) AND a.recStatus NOT IN ('X') ");
//    	}else{
    	queryStr.append("SELECT a FROM EntDbAircraftOpsMsg a WHERE a.fltRegn = :fltRegn AND ( trim(a.atot) ='' OR a.atot is NULL) AND a.recStatus NOT IN ('X') ");
//    	}
    	Query query = em.createQuery(queryStr.toString());
    	query.setParameter("fltRegn", entDbAircraftOpsMsg.getFltRegn());
    	List<EntDbAircraftOpsMsg> opsMsgList = query.getResultList();
    	if (opsMsgList != null && opsMsgList.size() >0){
    		Iterator<EntDbAircraftOpsMsg> it = opsMsgList.iterator();
    		while (it.hasNext()){
    			EntDbAircraftOpsMsg existOpsMsg = it.next();
    			existOpsMsg.setAtot(atot);
    			existOpsMsg.setAldt(aldt);
    			if (adid.equalsIgnoreCase(existOpsMsg.getArrDepFlag())){
    				existOpsMsg.setIdFlight(idFlight);
    			}else{
    				existOpsMsg.setIdRflight(idFlight);
    			}
//    			try {
					existOpsMsg.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
//				} catch (ParseException e) {
//					LOG.error("ERROR when getting current UTC time : {}", e.toString());
//				}
    			existOpsMsg.setUpdatedUser(HpEKConstants.DeNA_DATA_SOURCE);
    			em.merge(existOpsMsg); 	
    		}
    	}
    }

}
