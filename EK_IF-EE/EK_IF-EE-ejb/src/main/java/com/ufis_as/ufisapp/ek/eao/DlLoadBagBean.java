package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlLoadBagBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadBagBean extends DlAbstractBean<Object> implements IDlLoadBagUpdateLocal {

	private static final Logger LOG = LoggerFactory.getLogger(DlLoadBagBean.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public DlLoadBagBean() {
		super(Object.class);
		// super(entityClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	// @Override
	// public List<EntDbLoadBag> getBagMoveDetails(long idFlight,String
	// idLoadPax) {
	// Query query = em
	// .createNamedQuery("EntDbLoadBag.findByFlIdPax");
	// query.setParameter("idFlight", idFlight);
	// query.setParameter("idLoadPax", idLoadPax);
	// //query.setParameter("recStatus", "X");
	// //LOG.info("Query:" + query.toString());
	// List<EntDbLoadBag> result = Collections.EMPTY_LIST;
	// try {
	// result = query.getResultList();
	// LOG.info("Rec found from LOAD_BAG" + result.size() + " EntDbLoadBag ");
	// return result;
	//
	// } catch (Exception e) {
	// LOG.error("retrieving entities from LOAD_BAG ERROR:",
	// e.getMessage());
	// }
	// return null;
	// }
	//

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByBagTag(long idFlight, String bagTag) {
		EntDbLoadBag result = null;
		Query query = em.createNamedQuery("EntDbLoadBag.findByFlIdBagTag");
		query.setParameter("idFlight", new BigDecimal(idFlight));
		query.setParameter("bagTag", bagTag);
		// query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		List<EntDbLoadBag> results = Collections.EMPTY_LIST;
		try {
			results = query.getResultList();
			/*
			 * LOG.debug(
			 * "{} record found for EntDbLoadBag  by IdFlight={} and BagTag={}",
			 * results.size(), idFlight, bagTag);
			 */
		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG ERROR:", e.getMessage());
		}
		return results;
	}

	@Override
	public EntDbLoadBag getValidBagMoveDetailsByBagTag(long idFlight, String bagTag) {
		EntDbLoadBag result = null;
		Query query = em.createNamedQuery("EntDbLoadBag.findByValidFlIdBagTag");
		query.setParameter("idFlight", new BigDecimal(idFlight));
		query.setParameter("bagTag", bagTag);
		// query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		List<EntDbLoadBag> results = Collections.EMPTY_LIST;
		try {
			results = query.getResultList();
			if (results.size() > 0) {
				result = results.get(0);
				/*
				 * LOG.debug(
				 * "{} record found for EntDbLoadBag  by IdFlight={} and BagTag={}"
				 * , results.size(), idFlight, bagTag);
				 */
			}
		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG ERROR:", e.getMessage());
		}
		return result;
	}

	// EntDbLoadBag.findByBagPaxDeptFltInfo
	@Override
	public List<EntDbLoadBag> getBagMoveDetails(String bagTag, String paxRefNum, String fltNumber, String fltDate, String fltDest3) {
		Query query = em.createNamedQuery("EntDbLoadBag.findByBagPaxDeptFltInfo");
		query.setParameter("bagTag", bagTag);
		query.setParameter("paxRefNum", paxRefNum);
		query.setParameter("fltNumber", fltNumber);
		query.setParameter("fltDate", fltDate);
		query.setParameter("fltDest3", fltDest3);
		// query.setParameter("recStatus", "X");
		// LOG.info("Query:" + query.toString());
		List<EntDbLoadBag> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			LOG.info("Rec found from LOAD_BAG" + result.size() + " EntDbLoadBag ");
		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG ERROR:", e.getMessage());
		}
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean saveLoadBagMove(EntDbLoadBag loadBag) {
		try {
			em.persist(loadBag);
			LOG.debug("Record Insert Successfull");
			return true;
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", loadBag, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", loadBag, exc);
		}
		return false;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbLoadBag updateLoadedBag(EntDbLoadBag loadBag) {
		EntDbLoadBag loadBagStat = null;
		try {
			loadBagStat = em.merge(loadBag);
			LOG.debug("Merge Successfull");

		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", loadBag, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", loadBag, exc);
		}
		return loadBagStat;
	}

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByBagtagPax(long idFlight, String bagTag, String paxRefNum) {
		Query query = em.createNamedQuery("EntDbLoadBag.findByBagTagFliIdPaxRef");
		query.setParameter("idFlight", new BigDecimal(idFlight));
		query.setParameter("bagTag", bagTag);
		query.setParameter("paxRefNum", paxRefNum);

		List<EntDbLoadBag> result = Collections.EMPTY_LIST;

		try {
			result = query.getResultList();
			LOG.info("Rec found from LOAD_BAG" + result.size() + " EntDbLoadBag ");

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG ERROR:", e.getMessage());
		}

		return result;
	}

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByBagtagPaxX(long idFlight, String bagTagStr, String paxRefNum) {
		boolean result = false;
		List<EntDbLoadBag> resultList;

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<EntDbLoadBag> cq = cb.createQuery(EntDbLoadBag.class);
		Root<EntDbLoadBag> r = cq.from(EntDbLoadBag.class);
		cq.select(r);
		List<Predicate> filters = new LinkedList<>();
		filters.add(cb.equal(r.get("paxRefNum"), paxRefNum));
		filters.add(cb.equal(r.get("id_Flight"), new BigDecimal(idFlight)));
		// filters.add(cb.equal(r.get("bagTagStr"), bagTagStr));
		filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		// filters.add(cb.not(cb.in(r.get("recStatus")).value(" ").value("R")));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		resultList = em.createQuery(cq).getResultList();

		return resultList;
	}

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByPax(long idFlight, String paxRefNum) {
		Query query = em.createNamedQuery("EntDbLoadBag.findByFliIdPaxRef");
		query.setParameter("idFlight", new BigDecimal(idFlight));
		query.setParameter("paxRefNum", paxRefNum);

		List<EntDbLoadBag> result = Collections.EMPTY_LIST;

		try {
			result = query.getResultList();
			LOG.info("Rec found from LOAD_BAG" + result.size() + " EntDbLoadBag ");

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG ERROR:", e.getMessage());
		}

		return result;
	}

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByPaxX(String idLoadPax) {

//		LOG.debug("idFlight: {}, paxRefNum: {}, adid: {}", idFlight, paxRefNum, adid);
//		LOG.debug("idFlight: " + idFlight + ", paxRefNum: " + paxRefNum + ", adid: " + adid);
		List<EntDbLoadBag> resultList;

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<EntDbLoadBag> cq = cb.createQuery(EntDbLoadBag.class);
		Root<EntDbLoadBag> r = cq.from(EntDbLoadBag.class);
		cq.select(r);
		List<Predicate> filters = new LinkedList<>();
//		filters.add(cb.equal(r.get("paxRefNum"), paxRefNum));
		filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));
//		if ("a".equalsIgnoreCase(adid)) {
//			filters.add(cb.equal(r.get("idArrFlight"), idFlight));
//		} else {
//			filters.add(cb.equal(r.get("idFlight"), idFlight));

//		}
		// filters.add(cb.not(cb.in(r.get("recStatus")).value(" ").value("R")));
		filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		resultList = em.createQuery(cq).getResultList();



		return resultList;
	}

	@Override
	public List<EntDbLoadBag> getBagMoveDetailsByIdLoadPax(String idLoadPax) {

		List<EntDbLoadBag> resultList;

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<EntDbLoadBag> cq = cb.createQuery(EntDbLoadBag.class);
		Root<EntDbLoadBag> r = cq.from(EntDbLoadBag.class);
		cq.select(r);
		List<Predicate> filters = new LinkedList<>();

		filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));

		filters.add(cb.not(cb.in(r.get("recStatus")).value("X")));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		resultList = em.createQuery(cq).getResultList();

		return resultList;
	}
}
