package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagMove;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlLoadBagMoveBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadBagMoveBean extends DlAbstractBean<Object> implements IDlLoadBagMoveUpdateLocal{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlLoadBagMoveBean.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlLoadBagMoveBean() {
		super(Object.class);
		
	}
	
	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}
	
	@Override
	public List<EntDbLoadBagMove> getBagMoveDetailsByPax(long idFlight,String loadBagTag,String paxRefNum) {
		Query query = em
				.createNamedQuery("EntDbLoadBagMove.findBagMovesByIdFltBagTagPax");
		query.setParameter("idFlight", idFlight);
		query.setParameter("bagTag", loadBagTag);
		query.setParameter("paxRefNum",paxRefNum);
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<EntDbLoadBagMove> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from LOAD_BAG_MOVE" + result.size() + " EntDbLoadBagMove ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG_MOVE ERROR:",
					e.getMessage());
		}
		return null;
	}	
	
	@Override
	public List<EntDbLoadBagMove> getBagMoveDetails(long idFlight,String loadBagTag) {
		Query query = em
				.createNamedQuery("EntDbLoadBagMove.findBagMovesByIdFltBagTag");
		query.setParameter("idFlight", idFlight);
		query.setParameter("bagTag", loadBagTag);		
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<EntDbLoadBagMove> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from LOAD_BAG_MOVE" + result.size() + " EntDbLoadBagMove ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG_MOVE ERROR:",
					e.getMessage());
		}
		return null;
	}	
	
	@Override
	public List<EntDbLoadBagMove> getBagMoveDetailsById(BigDecimal idFlight,String loadBagTag,String idLoadBag) {
		Query query = em
				.createNamedQuery("EntDbLoadBagMove.findBagMovesByIdFltIdBagTag");
		query.setParameter("idFlight", idFlight);
		query.setParameter("bagTag", loadBagTag);
		query.setParameter("idLoadBag", idLoadBag);
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<EntDbLoadBagMove> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from LOAD_BAG_MOVE" + result.size() + " EntDbLoadBagMove ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG_MOVE ERROR:",
					e.getMessage());
		}
		return null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean saveLoadBagMove(EntDbLoadBagMove loadBagMove) {
		try {
			em.persist(loadBagMove);

			return true;
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", loadBagMove, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", loadBagMove, exc);
		}
		return false;
	}

	@Override	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbLoadBagMove updateLoadedBagMove(
			EntDbLoadBagMove loadBagMove) {		
		EntDbLoadBagMove entLoadBagMoveObj = null;
		try {
			entLoadBagMoveObj = em.merge(loadBagMove);
			// em.flush();
			// LOG.debug("Merge Successfull");

		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", loadBagMove, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", loadBagMove, exc);
		}
		return entLoadBagMoveObj;
	}

	@Override
	public List<EntDbLoadBagMove> getBagMoveByIdLoadBag(String idLoadBag) {
		Query query = em
				.createNamedQuery("EntDbLoadBagMove.findByidLoadBag");				
		query.setParameter("idLoadBag", idLoadBag);		
		//LOG.info("Query:" + query.toString());
		List<EntDbLoadBagMove> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from LOAD_BAG_MOVE" + result.size() + " EntDbLoadBagMove ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from LOAD_BAG_MOVE ERROR:",
					e.getMessage());
		}
		return null;		
	}
	
}
