package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * @author JGO
 * @version 1.0: 2013-11-14 JGO - new created
 * 
 */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadBagSummaryBean extends DlAbstractBean<EntDbLoadBagSummary> {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DlLoadBagSummaryBean.class);

	/**
	 * CDI
	 */
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	/**
	 * Constructor
	 */
	public DlLoadBagSummaryBean() {
		super(EntDbLoadBagSummary.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Object> findByTimeRange(HpUfisCalendar startDate, HpUfisCalendar endDate) {
		List<Object> list = new ArrayList<>();
		try {
			LOG.debug("Find all idFligt from Load_Bag_Summary between <{}> and <{}> in UTC", 
					startDate.getCedaString(), 
					endDate.getCedaString());
			Query query = getEntityManager().createNamedQuery("EntDbLoadBagSummary.findIdFlightByRange");
			query.setParameter("starDate", startDate.getTime());
    		query.setParameter("endDate", endDate.getTime());
    		query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			list = query.getResultList();
			LOG.debug("{} records found", list.size());
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return list;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FltConnectionDTO> findIdConxFlight(BigDecimal id) {
		List<FltConnectionDTO> list = new ArrayList<>();
		try {
			Query query = getEntityManager().createNamedQuery("EntDbLoadBagSummary.findConxFlightById");
			query.setParameter("idFlight", id);
			query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			List<EntDbLoadBagSummary> res = query.getResultList();
			if (res != null) {
				for (EntDbLoadBagSummary entity : res) {
					FltConnectionDTO dto = new FltConnectionDTO();
					dto.setBagPcs(entity.getBagPcs().intValue());
					dto.setIdFlight(entity.getIdFlight());
					dto.setIdConxFlight(entity.getIdConxFlight());
					list.add(dto);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return list;
	}

}
