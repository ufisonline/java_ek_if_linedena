/*
 * $Id: DlLoadPaxBean.java 8988 2013-09-23 07:33:24Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.LoadPaxPK;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author $Author: sch $
 * @version $Revision: 8988 $
 */
@Stateless(name = "DlLoadPaxBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadPaxBean extends DlAbstractBean<EntDbLoadPax> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlLoadPaxBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    public DlLoadPaxBean() {
        super(EntDbLoadPax.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EntDbLoadPax getPax(String intId) {
        EntDbLoadPax result = null;
        Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByid");
        query.setParameter("intId", intId);

        List<EntDbLoadPax> list = query.getResultList();
        if (list != null && list.size() > 0) {
            LOG.debug("{} record found for EntDbPax  by intId={}", list.size(), intId);
            result = list.get(0);
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
	public List<EntDbLoadPax> getPaxListByFlightId(String intFlId){
    	List<EntDbLoadPax> rawPaxList = null; 
//     	Retrieve raw data from database
    	Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByFlid");
    	query.setParameter("intFlId", intFlId);
    	rawPaxList = (List<EntDbLoadPax>) query.getResultList();
    	
    	if (rawPaxList != null){
    		 LOG.debug("{} record found for EntDbPax  by intFlId={}", rawPaxList.size(), intFlId);
    	}
    	return rawPaxList;
    }
    
	public List<EntDbLoadPax> getPaxListByIdFlightX(int idFlight){
		List<EntDbLoadPax> result = null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPax> r = cq.from(EntDbLoadPax.class);
//		cq.select(r.get("intId"));
		cq.select(r);
		Predicate predicate = cb.equal(r.get("idFlight"), idFlight);
		cq.where(predicate);
		result= em.createQuery(cq).getResultList();
		
		return result;
    }
    
    public EntDbLoadPax findByPkId(LoadPaxPK pk){
    	EntDbLoadPax entLoadPax = null; 
		try {
			Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByPk");
			query.setParameter("pKId", pk);
			
			List<EntDbLoadPax> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entLoadPax = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", entLoadPax, e);
		}
		return entLoadPax;
    }
    
    public  EntDbLoadPax findByPkIdX(String intId){
    	EntDbLoadPax result = null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPax> r = cq.from(EntDbLoadPax.class);
//		cq.select(r.get("intId"));
		cq.select(r);
		Predicate predicate = cb.equal(r.get("intId"), intId);
		cq.where(predicate);
		List<EntDbLoadPax> resultList = em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
		}
		
		return result;
    }
    
    public EntDbLoadPax findByPkIdX(LoadPaxPK pKId){
    	EntDbLoadPax result = null;
	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPax> cq = cb.createQuery(EntDbLoadPax.class);
		Root<EntDbLoadPax> r = cq.from(EntDbLoadPax.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("pKId").get("intFlId"), pKId.getIntflid()));
		  filters.add(cb.equal(r.get("pKId").get("intRefNumber"), pKId.getIntrefnumber()));
//		  filters.add(cb.equal(r.get("intRefNumber"), paxRefNum));	
//		  filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbLoadPax> resultList = em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxDetails, intFltId {}, paxRefnum {}, dataSource{}", resultList.size(), pKId.getIntflid(), pKId.getIntrefnumber());
			}
		}
		
		return result;
    }
    
    public List<EntDbLoadPax> findByIntFltId(String intFlId){
    	List<EntDbLoadPax> result = null;
	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPax> cq = cb.createQuery(EntDbLoadPax.class);
		Root<EntDbLoadPax> r = cq.from(EntDbLoadPax.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("pKId").get("intFlId"), intFlId));

//		  filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		result = em.createQuery(cq).getResultList();
		
		return result;
    }
  
    public EntDbLoadPax getPaxForPaxLounge(String paxRefNum, String intFlId){
    	EntDbLoadPax result = null;
		List<EntDbLoadPax> resultList = null;
		Query query = em.createNamedQuery("EntDbLoadPax.findForPaxLounge");
		try {
			query.setParameter("paxRefNum", paxRefNum);
			query.setParameter("intFlId", intFlId);
			resultList = query.getResultList();
			if (resultList.size() > 0) {
				result = resultList.get(0);
				LOG.debug("Pax record found for refNum : <{}>", paxRefNum);
			} else
				LOG.debug("Pax record is not found.");
		} catch (Exception ex) {
			LOG.error("ERROR finding pax for PaxLounge : {}", ex.toString());
		}
		return result;
    }
    
    public List<EntDbLoadPax> findPaxByFlightAndName(BigDecimal urno, String paxName) {
    	List<EntDbLoadPax> paxes = new ArrayList<>();
    	try {
    		Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByFlightAndName");
    		query.setParameter("idFlight", urno.intValue());
    		query.setParameter("paxName", paxName);
    		paxes = query.getResultList();
    	} catch (Exception e) {
    		LOG.error("Exception: {}", e);
    	}
    	return paxes;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Persist(EntDbLoadPax entDbPax) {
		if (entDbPax != null) {
			try {
				entDbPax.set_recStatus(" ");
				em.persist(entDbPax);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}", entDbPax, e);
			}
		}
	}

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Update(EntDbLoadPax entDbPax) {
		if (entDbPax != null) {
			try {
				em.merge(entDbPax);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}", entDbPax, e);
			}
		}
	}
    
}
