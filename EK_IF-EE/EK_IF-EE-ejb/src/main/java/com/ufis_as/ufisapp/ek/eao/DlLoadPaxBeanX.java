/*
 * $Id: DlLoadPaxBean.java 8988 2013-09-23 07:33:24Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxX;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author $Author: sch $
 * @version $Revision: 8988 $
 */
@Stateless(name = "DlLoadPaxBeanX")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadPaxBeanX extends DlAbstractBean<EntDbLoadPaxX> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlLoadPaxBeanX.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    public DlLoadPaxBeanX() {
        super(EntDbLoadPaxX.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public EntDbLoadPaxX findByPkIdX(String intFlId, String intRefNumber){
    	EntDbLoadPaxX result = null;
	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPaxX> cq = cb.createQuery(EntDbLoadPaxX.class);
		Root<EntDbLoadPaxX> r = cq.from(EntDbLoadPaxX.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("intFlId"), intFlId));
		  filters.add(cb.equal(r.get("intRefNumber"), intRefNumber));
//		  filters.add(cb.equal(r.get("intRefNumber"), paxRefNum));	
//		  filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbLoadPaxX> resultList = em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxDetails, intFltId {}, paxRefnum {}, dataSource{}", resultList.size(), intFlId, intRefNumber);
			}
		}
		
		return result;
    }
    
    public EntDbLoadPaxX findByIdLoadPax(String uuid){
    	EntDbLoadPaxX result = null;
    	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPaxX> cq = cb.createQuery(EntDbLoadPaxX.class);
		 Root<EntDbLoadPaxX> r = cq.from(EntDbLoadPaxX.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("uuid"),uuid));

		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbLoadPaxX> resultList = em.createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxDetails, uuid {}", resultList.size(), uuid);
			}
		}
		
		return result;
    }
    
    public List<EntDbLoadPaxX> findByIntFltId(String intFlId){
    	List<EntDbLoadPaxX> result = null;
	
		CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPaxX> cq = cb.createQuery(EntDbLoadPaxX.class);
		Root<EntDbLoadPaxX> r = cq.from(EntDbLoadPaxX.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("intFlId"), intFlId));

//		  filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		result = em.createQuery(cq).getResultList();
		
		return result;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Persist(EntDbLoadPaxX entDbPax) {
		if (entDbPax != null) {
			try {
				entDbPax.set_recStatus(" ");
				em.persist(entDbPax);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}", entDbPax, e);
			}
		}
	}

    
}
