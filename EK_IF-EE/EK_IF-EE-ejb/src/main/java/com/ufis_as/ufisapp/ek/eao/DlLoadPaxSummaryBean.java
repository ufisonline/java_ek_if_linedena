package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.configuration.HpPaxAlertTimeRangeConfig;

import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;



/**
 * Session Bean implementation class DlLoadPaxSummaryBean
 * 2014-01-13		BTR		- changed findConxFlightById() to find the conx flights depends on (id_flight or id_conx_flight) 
 * 								if the (id_arr_flight or id_dep_flight) is not exist.   
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class DlLoadPaxSummaryBean extends DlAbstractBean<EntDbLoadPaxSummary>
		implements IDlLoadPaxSummaryBeanLocal {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(DlLoadPaxSummaryBean.class);

	/**
	 * CDI
	 */
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager _em;

	public DlLoadPaxSummaryBean() {
		super(EntDbLoadPaxSummary.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return _em;
	}

	public EntDbLoadPaxSummary findByPk(String id) {
		EntDbLoadPaxSummary entDbLoadPaxSummary = null;
		try {
			Query query = getEntityManager().createNamedQuery(
					"EntDbLoadPaxSummary.findByPk");
			query.setParameter("id", id);

			List<EntDbLoadPaxSummary> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entDbLoadPaxSummary = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}",
					entDbLoadPaxSummary, e);
		}
		return entDbLoadPaxSummary;

	}
	
	public EntDbLoadPaxSummary findByintIdNtype(EntDbLoadPaxSummary loadPaxSum) {
		EntDbLoadPaxSummary entDbLoadPaxSummary = null;
		try {
			Query query = getEntityManager().createNamedQuery(
					"EntDbLoadPaxSummary.findByintIdNtype");
			query.setParameter("interfaceFltId", loadPaxSum.getInterfaceFltId());
			query.setParameter("infoType", loadPaxSum.getInfoType());

			List<EntDbLoadPaxSummary> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entDbLoadPaxSummary = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}",
					entDbLoadPaxSummary, e);
		}
		return entDbLoadPaxSummary;

	}

	@Override
	public EntDbLoadPaxSummary getLoadPaxSummaryByMfid(BigDecimal mfid,
			String dataSource) {
		EntDbLoadPaxSummary entity = null;
		try {
			Query query = getEntityManager().createNamedQuery(
					"EntDbLoadPaxSummary.findByMfid");
			query.setParameter("mfid", mfid);
			query.setParameter("infoType", HpEKConstants.LOAD_TYPE_CONFIG);
			List<EntDbLoadPaxSummary> result = query.getResultList();
			if (result != null && result.size() > 0) {
				entity = result.get(0);
			}
		} catch (Exception e) {
			LOG.error("LOAD_PAX_SUMMARY: cannot find record by mfid={}", mfid);
			LOG.error("Exception: \n{}", e);
		}
		return entity;
	}

	@Override
    public List<EntDbLoadPaxSummary> findByArrFlId(BigDecimal arrFlId){
    	List<EntDbLoadPaxSummary> entList = null;
    	try{
    		Query query = _em.createNamedQuery("EntDbLoadPaxSummary.findByArrFlId");
    		query.setParameter("idArrFlight", arrFlId);
    		query.setParameter("infoType", HpEKConstants.INFO_ID_PAX_ALERT);
    		
    		 entList = query.getResultList();
    		if(entList.size()<=0)
    			LOG.debug("No Connected flights for arrFlid : <{}>", arrFlId);
    		else
    			LOG.debug("Total Connected <{}> flights for arrFlid : <{}>", entList.size(), arrFlId);
		} catch (Exception ex) {
			LOG.error("Load summary for arrFlid : <{}> is not found. {}", arrFlId, ex);
		}
    	return entList;
    }
    
	@Override
    public List<EntDbLoadPaxSummary> findByDepFlId(BigDecimal depFlId){
		List<EntDbLoadPaxSummary> entList = null;
    	try{
    		Query query = _em.createNamedQuery("EntDbLoadPaxSummary.findByDepFlId");
    		query.setParameter("idDepFlight", depFlId);
    		query.setParameter("infoType", HpEKConstants.INFO_ID_PAX_ALERT);
    		 entList = query.getResultList();
     		if(entList.size()<=0)
     			LOG.debug("No Connected flights for depFlid : <{}>", depFlId);
     		else
     			LOG.debug("Total connected <{}> flights for depFlid : <{}>", entList.size(), depFlId);
		} catch (Exception ex) {
			LOG.error("ERROR when retrieving Load pax summary for depFlid : <{}>. {}", depFlId, ex);
		}
    	return entList;
    }

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void persist(EntDbLoadPaxSummary entity) {
		_em.persist(entity);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void update(EntDbLoadPaxSummary entity) {
		try {
			entity = _em.merge(entity);
		} catch (OptimisticLockException Oexc) {
			// getEntityManager().refresh(entity);
			LOG.error("OptimisticLockException Entity:{} , Error:{}", entity,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", entity, exc);
		}
	}

	@Override
	public List<EntDbLoadPaxSummary> findByTimeRange() {
		List<EntDbLoadPaxSummary> res = new ArrayList<>();

		HpUfisCalendar startDate = new HpUfisCalendar();
		HpUfisCalendar endDate = new HpUfisCalendar();
		startDate.setTimeZone(HpEKConstants.utcTz);
		endDate.setTimeZone(HpEKConstants.utcTz);

		startDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertFromOffset(), EnumTimeInterval.Hours);
		endDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertToOffset(), EnumTimeInterval.Hours);
		LOG.debug("Timer FromOffset : <{}>  ToOffset : <{}>", startDate.getCedaString(), endDate.getCedaString());
    	try{
    		Query query = _em.createNamedQuery("EntDbLoadPaxSummary.findByTimeRange");
    		query.setParameter("starDate", startDate.getTime());
    		query.setParameter("endDate", endDate.getTime());
    		query.setParameter("infoType", HpEKConstants.INFO_ID_PAX_ALERT);
    		
			res = query.getResultList();
			LOG.debug("Total pax connected flights within current UTC : <{}>", res.size());
		} catch (Exception ex) {
			LOG.error("ERROR when retrieving connected flights : {}", ex);
		}
    	return res;
	}
	
	@Override
	//public List<EntDbLoadPaxSummary> findByTimeRange(HpUfisCalendar startDate, HpUfisCalendar endDate) {
	public List<Object> findByTimeRange(HpUfisCalendar startDate, HpUfisCalendar endDate) {
		//List<EntDbLoadPaxSummary> res = new ArrayList<>();
		List<Object> res = new ArrayList<>();
		LOG.debug("Find all idArrFlight from Load_Pax_Summary between <{}> and <{}> in UTC", 
				startDate.getCedaString(), 
				endDate.getCedaString());
    	try{
    		//Query query = _em.createNamedQuery("EntDbLoadPaxSummary.findByTimeRange");
    		Query query = _em.createNamedQuery("EntDbLoadPaxSummary.findIdArrFlightByTimeRange");
    		query.setParameter("starDate", startDate.getTime());
    		query.setParameter("endDate", endDate.getTime());
    		query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			res = query.getResultList();
			LOG.debug("{} records found", res.size());
		} catch (Exception ex) {
			LOG.error("ERROR when retrieving connected flights : {}", ex);
		}
    	return res;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FltConnectionDTO> findConxFlightById(BigDecimal idFlight, Character adid) {
		
		//long recordStartBeforeRetrieve = System.currentTimeMillis();
		
		List<FltConnectionDTO> list = new ArrayList<>();
		try {
			StringBuilder sb = new StringBuilder();
			String mainFlight = null;
			// build search criterias and set parameters
			if (HpEKConstants.ADID_A == adid) {
				mainFlight = "idArrFlight";
			} else if (HpEKConstants.ADID_D == adid) {
				mainFlight = "idDepFlight";
			} else {
				LOG.warn("Main flight ADID: {}, it must be either A or D", adid);
				return list;
			}
			sb.append(" SELECT a ");
			//sb.append(" SELECT NEW com.ufis_as.ek_if.connection.FltConnectionDTO(a.totalPax, a.idDepFlight)");
			sb.append(" FROM EntDbLoadPaxSummary a");
			sb.append(" WHERE a.infoType = :infoType ");
			sb.append(" AND " + mainFlight + " = :idFlight");
			sb.append(" AND a.recStatus <> 'X' ");
			
			Query query = getEntityManager().createQuery(sb.toString());
			query.setParameter("idFlight", idFlight);
			query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			
			//list = query.getResultList();
			
    		List<EntDbLoadPaxSummary> res = query.getResultList();
    		
    		if(res == null || res.size() == 0){
    			//find the conx flights from id_flight and id_conx_flight
    			StringBuilder strB = new StringBuilder();
    			strB.append(" SELECT a ");
    			strB.append(" FROM EntDbLoadPaxSummary a");
    			strB.append(" WHERE a.infoType = :infoType ");
    			strB.append(" AND (idConxFlight = :idFlight OR idFlight = :idFlight)");
    			strB.append(" AND a.recStatus <> 'X' ");
    			
    			Query query2 = getEntityManager().createQuery(strB.toString());
        		query2.setParameter("idFlight", idFlight);
        		query2.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
        		res = query2.getResultList();// second query to get conx flights on id_flight or id_conx_flight
        		
        		if (res != null) {
        			LOG.debug("<{}> Conx Flights are found by (id_flight or id_conx_flight)", res.size());
        			for (EntDbLoadPaxSummary entity : res) {
        				FltConnectionDTO dto = new FltConnectionDTO();
        				dto.setTotalPax(entity.getTotalPax().intValue());
    					dto.setIdFlight(entity.getIdFlight());
    					dto.setIdConxFlight(entity.getIdConxFlight());
        				list.add(dto);
        			}
        		}
    		} else if (res != null) {
    			LOG.debug("<{}> Conx Flights are found by (id_arr_flight or id_dep_flight)", res.size());
    			for (EntDbLoadPaxSummary entity : res) {
    				FltConnectionDTO dto = new FltConnectionDTO();
    				dto.setTotalPax(entity.getTotalPax().intValue());
    				if (HpEKConstants.ADID_A == adid) {
    					dto.setIdConxFlight(entity.getIdDepFlight());
    				} else {
    					dto.setIdConxFlight(entity.getIdArrFlight());
    				}
    				list.add(dto);
    			}
    		} 
    			
		} catch (Exception e) {
			LOG.error("ERROR when retrieving connected flights: {}", e.getMessage());
		}
		//LOG.info("Total record found : "+list.size()+" %%%%%%%%%% Testing Total time taken to query : <"+(System.currentTimeMillis()-recordStartBeforeRetrieve) + ">ms");
		return list;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FltConnectionDTO> findConxFlightById(BigDecimal idFlight) {
		List<FltConnectionDTO> list = new ArrayList<>();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT a ");
			sb.append(" FROM EntDbLoadPaxSummary a ");
			sb.append(" WHERE a.infoType = :infoType ");
			sb.append(" AND a.idFlight = :idFlight ");
			sb.append(" AND a.recStatus <> 'X' ");
			
			Query query = getEntityManager().createQuery(sb.toString());
			query.setParameter("idFlight", idFlight);
			query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
    		List<EntDbLoadPaxSummary> res = query.getResultList();
    		if (res != null) {
    			for (EntDbLoadPaxSummary entity : res) {
    				FltConnectionDTO dto = new FltConnectionDTO();
    				dto.setTotalPax(entity.getTotalPax().intValue());
					dto.setIdConxFlight(entity.getIdArrFlight());
    				list.add(dto);
    			}
    		}
		} catch (Exception e) {
			LOG.error("ERROR when retrieving connected flights: {}", e.getMessage());
		}
		return list;
	}

}
