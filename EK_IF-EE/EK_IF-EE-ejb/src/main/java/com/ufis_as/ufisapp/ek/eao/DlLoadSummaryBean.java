//package com.ufis_as.ufisapp.ek.eao;
//
//import java.util.Iterator;
//import java.util.List;
//
//import javax.ejb.Stateless;
//import javax.ejb.TransactionAttribute;
//import javax.ejb.TransactionAttributeType;
//import javax.persistence.EntityManager;
//import javax.persistence.OptimisticLockException;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ufis_as.configuration.HpEKConstants;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadSummary;
//import com.ufis_as.ek_if.macs.entities.LoadPaxPK;
//import com.ufis_as.ek_if.macs.entities.LoadSummaryPK;
//import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
//
///**
// * 
// * @author SCH
// *
// */
//
//@Stateless(name = "DlLoadSummaryBean")
//@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
//public class DlLoadSummaryBean extends DlAbstractBean<EntDbLoadSummary> {
//
//    /**
//     * Logger
//     */
//    private static final Logger LOG = LoggerFactory.getLogger(DlLoadSummaryBean.class);
//    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
//    private EntityManager _em;
//	
//	public DlLoadSummaryBean() {
//		super(EntDbLoadSummary.class);
//	}
//
//	@Override
//	protected EntityManager getEntityManager() {
//		return _em;
//	}
//	
//	@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
//	public void persistLoadSummaryList(List<EntDbLoadSummary> loadSummaryList){
//		Iterator<EntDbLoadSummary> it = loadSummaryList.iterator();
//		while(it.hasNext()){
//			EntDbLoadSummary loa= (EntDbLoadSummary)it.next();
//			getEntityManager().merge(loa);
//		}
//	}
//	
//	public EntDbLoadSummary getLoadSummaryByPkey(LoadSummaryPK loadKey) {
//		EntDbLoadSummary load = null;
//		if (loadKey != null) {
//			Query query = getEntityManager().createNamedQuery("EntDbLoadSummary.findByPkey");
//			query.setParameter("intFlId", loadKey.getIntFlId());
//			query.setParameter("infoId", loadKey.getInfoId());
//			query.setParameter("airlineCode", loadKey.getAirlineCode());
//			query.setParameter("flightNumber", loadKey.getFlightNumber());
//			query.setParameter("flightNumberSuffix", loadKey.getFlightNumberSuffix());
//			query.setParameter("scheduledFlightDateTime", loadKey.getScheduledFlightDateTime());
//			try {
//				load = (EntDbLoadSummary) query.getSingleResult();
//			} catch (Exception e) {
//				LOG.debug("Cannot find load summary info By using: {}", loadKey);
//			}
//		} else {
//			LOG.debug("LoadSummary primary key is null");
//		}
//		return load;
//	}
//	
//	
//    public EntDbLoadSummary findByPk(LoadSummaryPK pk){
//    	EntDbLoadSummary entDbLoadSummary = null; 
//		try {
//			Query query = getEntityManager().createNamedQuery("EntDbLoadSummary.findByPk");
//			query.setParameter("pKId", pk);
//			
//			List<EntDbLoadSummary> list = query.getResultList();
//			if (list != null && list.size() > 0) {
//				entDbLoadSummary = list.get(0);
//			}
//		} catch (Exception e) {
//			LOG.error("Exception entLoadPax:{} , Error:{}", entDbLoadSummary, e);
//		}
//		return entDbLoadSummary;
//    	
//    }
//	
//	
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void persist(EntDbLoadSummary entity){
//    	_em.persist(entity); 	
//    }
//	
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public EntDbLoadSummary update(EntDbLoadSummary entity) {
//        try {
//        	entity = _em.merge(entity);
//        } catch (OptimisticLockException Oexc) {
//            //getEntityManager().refresh(entity);
//            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
//        } catch (Exception exc) {
//            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
//        }
//        return entity;
//    }
//	
//	
//
//}
