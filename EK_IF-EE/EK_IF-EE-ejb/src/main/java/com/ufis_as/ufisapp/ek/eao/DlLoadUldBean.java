package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadUldBean extends DlAbstractBean<EntDbLoadUld> {

	  /** Logger **/
    private static final Logger LOG = LoggerFactory.getLogger(DlLoadUldBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    public DlLoadUldBean() {
    	super(EntDbLoadUld.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(EntDbLoadUld entity){
    	em.persist(entity); 	
    }
    
    public EntDbLoadUld merge(EntDbLoadUld entity){
    	EntDbLoadUld loadUld = null;
    	try{
    		loadUld = em.merge(entity);
    	} catch (OptimisticLockException opEx) {
    		LOG.error("Optimistics Lock Exception for LOAD ULD !!! {}", opEx.toString());
    	} catch(Exception ex){
    		LOG.error("ERROR to create new/update existing LOAD ULD !!! {}", ex.toString());
    	}
    	return loadUld;
    }

    /**
     * - get all ULDs by IdFlight 
     * @param idFlight
     */
    public List<EntDbLoadUld> getUldsByIdFlight(BigDecimal idFlight){
    	List<EntDbLoadUld> resultList  = new ArrayList<>();
    	Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUldsByIdFlight");
    	query.setParameter("idFlight", idFlight);
    	query.setParameter("recStatus", " ");
    	try{
    		resultList =  query.getResultList();
			if (resultList.size() > 0) {
    			LOG.debug("Total <{}> ulds found for idFlight <{}>", resultList.size(), idFlight);
    		}
			else
				LOG.debug("No ulds found for idFlight <{}>", idFlight);
		} catch (Exception nur) {
			LOG.error("ERROR when retriving Ulds by idFlight : "+ nur.getMessage());
		}
    	return resultList;
    }
    
    public List<BigDecimal> getUniqueConxFlightByIdFlight(BigDecimal idFlight){
    	List<BigDecimal> resultList  = null;
    	Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUniqueConxFlightByIdFlight");
    	query.setParameter("idFlight", idFlight);
    	query.setParameter("recStatus", " ");
    	try{
    		resultList =  query.getResultList();
			if (resultList.size() > 0) {
    			LOG.debug("Total <{}> conx flights found for idFlight <{}>", resultList.size(), idFlight);
    		}
			else
				LOG.debug("No conx flight found for idFlight <{}>", idFlight);
		} catch (Exception nur) {
			LOG.error("ERROR when retriving Conx flights by idFlight: "+ nur.getMessage());
		}
    	return resultList;
    }
    
    public EntDbLoadUld getUldNum(BigDecimal idFlight, String uld_number){
    	//LOG.debug("==== Searching ULD for Movement ====");
    	List<EntDbLoadUld> resultList  = null;
    	EntDbLoadUld result = null;
    	Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUld");
    	query.setParameter("idFlight", idFlight);
    	query.setParameter("uldNumber", uld_number);
    	query.setParameter("recStatus", "X");
    	try{
    		resultList =  query.getResultList();
			if (resultList.size() > 0) {
				result = resultList.get(0);
    			LOG.debug("Uld record is found. ID : {}", result.getId());
    		}
			else
				LOG.debug("Uld record is not found");
		} catch (Exception nur) {
			LOG.error("ERROR when retriving uld number : "+ nur.getMessage());
		}
    	return result;
    }
    
    public EntDbLoadUld getUldByPk(Date fldate, String flno, String uldno){
    	//LOG.debug("==== Searching ULD ====");
    	EntDbLoadUld result  = null;
    	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    	try{
	    	Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUldByPk");
	    	query.setParameter("uldCurrFltno", flno);
	    	query.setParameter("uldNumber", uldno);
	    	query.setParameter("fltDate", formatter.format(fldate));
	    	query.setParameter("recStatus", "X");
	    	
    		List<EntDbLoadUld> resultList = query.getResultList();
    		if(resultList.size()>0){
    			result = resultList.get(0);
    			LOG.debug("Uld record found. ID : {}", result.getId());
    		}
    		else
    			LOG.debug("ULD is not found");
		} catch (Exception nur) {
			LOG.error("ERROR finding uld record ", nur.toString());
		}
    	return result;
    }
    
    public EntDbLoadUld findUldByFlightAndULDNo(String flno, String uldno){
    	//LOG.debug("==== Searching ULD ====");
    	EntDbLoadUld result  = null;
    	try{
	    	Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUldByFlightAndULDNo");
	    	query.setParameter("uldCurrFltno", flno);
	    	query.setParameter("uldNumber", uldno);
	    	query.setParameter("recStatus", "X");
	    	
    		List<EntDbLoadUld> resultList = query.getResultList();
    		if(resultList.size()>0){
    			result = resultList.get(0);
    			LOG.debug("Uld record found. ID : {}", result.getId());
    		}
    		else
    			LOG.debug("ULD is not found");
		} catch (Exception nur) {
			nur.printStackTrace();
			LOG.error("ERROR finding uld record ", nur.toString());
		}
    	return result;
    }

    public List<EntDbLoadUld> findUldsByFlight(BigDecimal flId) {
    	//LOG.debug("==== Searching ULDs ====");
    	try{
    		Query query = getEntityManager().createNamedQuery("EntDbLoadUld.findUldsByFlight");
    		query.setParameter("idFlight", flId);
    		query.setParameter("recStatus", "X");
    		
    		List<EntDbLoadUld> resultList = query.getResultList();
			if (resultList.size() > 0) {
				LOG.debug("Uld records found. size : {}", resultList.size());
			} else {
				LOG.debug("ULD is not found");
			}
			
			return resultList;
    	} catch (Exception nur) {
    		nur.printStackTrace();
    		LOG.error("ERROR finding uld record ", nur.toString());
    	}
    	return Collections.EMPTY_LIST;
    }
    
    public void deleteBulkUldsByFlight(BigDecimal flId) {
    	try {
    		Query query = getEntityManager().createNativeQuery("DELETE FROM LOAD_ULDS WHERE ULD_NUMBER LIKE 'BULK%' AND ID_FLIGHT = :flId");
    		query.setParameter("flId", flId);
    		int updated = query.executeUpdate();
    		LOG.debug("{} of BULK ULDs regarding the flight {} are deleted.", updated, flId);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		LOG.error("Deleting BULK uld records regarding the flight {}, {}", flId, ex.getMessage());
    	}
    }
}