package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUldMove;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadUldMoveBean extends DlAbstractBean<EntDbLoadUldMove> {

	  /** Logger **/
    private static final Logger LOG = LoggerFactory.getLogger(DlLoadUldMoveBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    public DlLoadUldMoveBean() {
    	super(EntDbLoadUldMove.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(EntDbLoadUldMove entity){
    	em.persist(entity); 	
    }
    
    public EntDbLoadUldMove merge(EntDbLoadUldMove entity){
    	EntDbLoadUldMove loadUld = null;
    	try{
    		loadUld = em.merge(entity);
    	}
    	catch(Exception ex){
    		LOG.error("ERROR to create new LOAD ULD !!! {}", ex.toString());
    	}
    	return loadUld;
    }
    
    public EntDbLoadUldMove getUldMoveByIdFlightUldNum(BigDecimal idFlight, String uldNumber){
    	EntDbLoadUldMove result  = null;
    	try{
	    	Query query = getEntityManager().createNamedQuery("EntDbLoadUldMove.findUldMoveByidFlightUldNum");
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("uldNumber", uldNumber);
	    	query.setParameter("recStatus", "X");
	    	
    		List<EntDbLoadUldMove> resultList = query.getResultList();
    		if(resultList.size()>0){
    			//get the uld latest movement
    			result = resultList.get(0);
    			LOG.info("Uld Move record found. ID : {}", result.getId());
    		}
    		else
    			LOG.debug("ULD Move is not found");
		} catch (Exception nur) {
			LOG.error("ULD Move Record not found "+ nur.getMessage());
		}
    	return result;
    }
    
    public EntDbLoadUldMove getUldMoveByPk(Date flDate, String flno, String uldNo){
    	//LOG.debug("==== Searching ULD Move ====");
    	EntDbLoadUldMove result  = null;
    	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    	try{
	    	Query query = getEntityManager().createNamedQuery("EntDbLoadUldMove.findUldMoveByPk");
	    	query.setParameter("flightNumber", flno);
	    	query.setParameter("uldNumber", uldNo);
	    	query.setParameter("flightDate", formatter.format(flDate));
	    	query.setParameter("recStatus", "X");
    		List<EntDbLoadUldMove> resultList = query.getResultList();
    		if(resultList.size()>0){
    			//get the uld latest movement
    			result = resultList.get(0);
    			LOG.info("Uld Move record found. ID : {}", result.getId());
    		}
    		else
    			LOG.debug("ULD Move is not found");
		} catch (Exception nur) {
			LOG.error("ULD Move Record not found "+ nur.getMessage());
		}
    	return result;
    }

    public void updateDeletedUld(String IdUldList){
    	Query query = getEntityManager().createNamedQuery("EntDbLoadUldMove.updateDeletedUlds");
    	query.setParameter("recStatus", "I");
    	query.setParameter("idLoadUlds", IdUldList);
    	query.setParameter("curStatus", "X");
    	try{
    		query.executeUpdate();
    	}
    	catch(Exception e){
    		LOG.error("ERROR updating deleted records in uldMove tab : {}", e.toString());
    	}
    }
    
}
