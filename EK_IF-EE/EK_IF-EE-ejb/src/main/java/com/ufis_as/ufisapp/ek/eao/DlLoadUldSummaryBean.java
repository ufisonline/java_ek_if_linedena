package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUldSummary;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlLoadUldSummaryBean extends DlAbstractBean<EntDbLoadUldSummary> {

	  /** Logger **/
    private static final Logger LOG = LoggerFactory.getLogger(DlLoadUldSummaryBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    public DlLoadUldSummaryBean() {
    	super(EntDbLoadUldSummary.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(EntDbLoadUldSummary entity){
    	em.persist(entity); 	
    }
    
    public EntDbLoadUldSummary merge(EntDbLoadUldSummary entity){
    	EntDbLoadUldSummary loadUld = null;
    	try{
    		loadUld = em.merge(entity);
    	}
    	catch(Exception ex){
    		LOG.error("ERROR to create new LOAD ULD !!! {}", ex.toString());
    	}
    	return loadUld;
    }
    
    public EntDbLoadUldSummary getUldMoveByIdFlightUldNum(BigDecimal idFlight, BigDecimal idConxFlight, String infoType){
    	EntDbLoadUldSummary result  = null;
    	try{
	    	Query query = getEntityManager().createNamedQuery("EntDbLoadUldSummary.getExisting");
	    	query.setParameter("idFlight", idFlight);
	    	query.setParameter("idConxFlight", idConxFlight);
	    	query.setParameter("infoType", infoType);
	    	query.setParameter("recStatus", " ");
	    	
    		List<EntDbLoadUldSummary> resultList = query.getResultList();
			if (resultList.size() > 0) {
    			result = resultList.get(0);
    			//LOG.info("Uld Summary record found. ID : {}", result.getId());
    		}
    		/*else
    			LOG.debug("ULD Summary is not found");*/
		} catch (Exception nur) {
			LOG.error("ULD Summary Record not found "+ nur.getMessage());
		}
    	return result;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Object> findByTimeRange(HpUfisCalendar startDate, HpUfisCalendar endDate) {
    	List<Object> list = new ArrayList<>();
    	try {
    		LOG.debug("Find all idFligt from Load_Uld_Summary between <{}> and <{}> in UTC", 
					startDate.getCedaString(), 
					endDate.getCedaString());
    		Query query = getEntityManager().createNamedQuery("EntDbLoadUldSummary.findIdFlightByRange");
    		query.setParameter("starDate", startDate.getTime());
    		query.setParameter("endDate", endDate.getTime());
    		query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			list = query.getResultList();
			LOG.debug("{} records found", list.size());
    	} catch (Exception e) {
    		LOG.error("Exception: {}", e.getMessage());
    	}
    	return list;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FltConnectionDTO> findIdConxFlight(BigDecimal id) {
		List<FltConnectionDTO> list = new ArrayList<>();
		try {
			Query query = getEntityManager().createNamedQuery("EntDbLoadUldSummary.findConxFlightById");
			query.setParameter("idFlight", id);
			query.setParameter("infoType", HpUfisAppConstants.INFO_TYPE_TRANSFER);
			List<EntDbLoadUldSummary> res = query.getResultList();
			if (res != null) {
				for (EntDbLoadUldSummary entity : res) {
					FltConnectionDTO dto = new FltConnectionDTO();
					Float f = entity.getUldPcs();
					dto.setUldPcs(f.intValue());
					dto.setIdFlight(entity.getIdFlight());
					dto.setIdConxFlight(entity.getIdConxFlight());
					list.add(dto);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return list;
	}
}
