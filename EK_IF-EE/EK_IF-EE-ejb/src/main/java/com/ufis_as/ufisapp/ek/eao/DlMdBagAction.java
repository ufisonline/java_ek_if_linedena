package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagAction;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdBagAction")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdBagAction extends DlAbstractBean<Object> implements IDlMdBagAction{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdBagAction.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdBagAction() {
		super(Object.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}
	
	@Override
	public List<EntDbMdBagAction> getAllBagActions() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagAction e");	
		Query query = em.createNamedQuery("EntDbMdBagAction.findAllBagAction");
		//LOG.info("Query:" + query.toString());
		List<EntDbMdBagAction> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_ACTION" + result.size() + " EntDbMdBagAction ");
			

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_ACTION ERROR:",
					e.getMessage());
		}
		return result;
	}
	
	@Override
	public List<String> getBagActions() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagClassify e");	
		Query query = em.createNamedQuery("EntDbMdBagAction.findBagAction");
		//LOG.info("Query:" + query.toString());
		List<String> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_ACTION" + result.size() + " EntDbMdBagAction ");
			
		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_ACTION ERROR:",
					e.getMessage());
		}
		return result;
	}

}
