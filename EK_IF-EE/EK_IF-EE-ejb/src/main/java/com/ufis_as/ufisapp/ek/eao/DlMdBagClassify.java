package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagClassify;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdBagClassify")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdBagClassify extends DlAbstractBean<Object> implements IDlMdBagClassify{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdBagClassify.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdBagClassify() {
		super(Object.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}
	@Override
	public List<EntDbMdBagClassify> getAllBagClassifies() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagClassify e");	
		Query query = em.createNamedQuery("EntDbMdBagClassify.findAllBagClassify");
		//LOG.info("Query:" + query.toString());
		List<EntDbMdBagClassify> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_CLASSIFY" + result.size() + " EntDbMdBagClassify ");
			

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_CLASSIFY ERROR:",
					e.getMessage());
		}
		return result;
	}
	@Override
	public List<String> getBagClassifies() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagClassify e");	
		Query query = em.createNamedQuery("EntDbMdBagClassify.findBagClassify");
		//LOG.info("Query:" + query.toString());
		List<String> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_CLASSIFY" + result.size() + " EntDbMdBagClassify ");
			
		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_CLASSIFY ERROR:",
					e.getMessage());
		}
		return result;
	}
	
}
