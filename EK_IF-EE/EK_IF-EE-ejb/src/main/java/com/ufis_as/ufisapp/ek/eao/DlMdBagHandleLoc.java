package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagAction;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagHandleLoc;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdBagHandleLoc")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdBagHandleLoc extends DlAbstractBean<Object> implements IDlMdBagHandleLoc{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdBagHandleLoc.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdBagHandleLoc() {
		super(Object.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}
	
	@Override
	public List<EntDbMdBagHandleLoc> getAllBagHandleLocs() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagHandleLoc e");	
		Query query = em.createNamedQuery("EntDbMdBagHandleLoc.findAllBagHandleLoc");
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<EntDbMdBagHandleLoc> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_HANDLE_LOC" + result.size() + " EntDbMdBagHandleLoc ");
			

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_HANDLE_LOC ERROR:",
					e.getMessage());
		}
		return result;
	}

}
