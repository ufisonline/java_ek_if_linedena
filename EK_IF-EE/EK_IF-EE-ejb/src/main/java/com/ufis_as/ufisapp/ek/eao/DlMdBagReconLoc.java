package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagReconLoc;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdBagReconLoc")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdBagReconLoc extends DlAbstractBean<Object> implements IDlMdBagReconLoc{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdBagReconLoc.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdBagReconLoc() {
		super(Object.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}
	
	@Override
	public List<EntDbMdBagReconLoc> getBagReconLocs() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagReconLoc e");	
		Query query = em.createNamedQuery("EntDbMdBagReconLoc.findAllBagReconLoc");
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<EntDbMdBagReconLoc> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_RECON_LOC" + result.size() + " EntDbMdBagReconLoc ");
			

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_RECON_LOC ERROR:",
					e.getMessage());
		}
		return result;
	}

}
