package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdBagType")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdBagType extends DlAbstractBean<Object> implements IDlMdBagType{

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdBagType.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdBagType() {
		super(Object.class);
		
	}

	@Override
	protected EntityManager getEntityManager() {
		//LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	public List<EntDbMdBagType> getAllBagTypes() {
//		Query query = em
//				.createQuery("SELECT e FROM EntDbMdBagType e");	
		Query query = em.createNamedQuery("EntDbMdBagType.findallBagTypes");
		//LOG.info("Query:" + query.toString());
		List<EntDbMdBagType> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_TYPE" + result.size() + " EntDbMdBagType ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_TYPE ERROR:",
					e.getMessage());
		}
		return null;
	}
	@Override
	public List<String> getBagTypes() {						
		Query query = em.createNamedQuery("EntDbMdBagType.findBagTypes");	
		query.setParameter("recStatus", "X");
		//LOG.info("Query:" + query.toString());
		List<String> result = Collections.EMPTY_LIST;
		try {
			//LOG.debug("Result list:"+query.getResultList());
			result = query.getResultList();
			//LOG.info("Rec found from MD_BAG_TYPE" + result.size() + " EntDbMdBagType ");
			

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_BAG_TYPE ERROR:",
					e.getMessage());
		}
		return result;
	}
	
}
