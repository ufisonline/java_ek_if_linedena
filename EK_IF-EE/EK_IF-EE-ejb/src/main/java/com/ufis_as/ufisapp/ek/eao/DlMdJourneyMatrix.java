package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.journeymatrix.EntDbMdJourneyMatrix;

/**
 * @author lma
 */

@Stateless(name = "DlMdJourneyMatrix")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdJourneyMatrix {

    private static final Logger LOG = LoggerFactory.getLogger(DlMdJourneyMatrix.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;
	
	protected EntityManager getEntityManager() {
		return _em;
	}
	
    public List<EntDbMdJourneyMatrix> findMatrixBy(String fromLoc, String toLoc){
    	try{
    		Query query = _em.createNamedQuery("EntDbMdJourneyMatrix.findMatrixBy");
    		query.setParameter("opType", "CONN");
    		query.setParameter("fromLocType", "PST");
    		query.setParameter("fromLoc", fromLoc);
    		query.setParameter("toLocType", "PST");
    		query.setParameter("toLoc", toLoc);
    		query.setParameter("moveObjType", "PAX");
    		
    		List<EntDbMdJourneyMatrix> resultList = query.getResultList();
			if (resultList.size() > 0) {
				return resultList;
			} else {
				LOG.debug("JourneyMatrix record not found");
			}
    	}
    	catch(Exception ex){
    		LOG.error("Error when retrieving existing JourneyMatrix : {}", ex);
    	}
    	return Collections.EMPTY_LIST;
    }
}