package com.ufis_as.ufisapp.ek.eao;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.acts_ods.entities.EntDbMdStaffType;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMdStaffType")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMdStaffType extends DlAbstractBean<Object> implements IDlMdStaffType {

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMdStaffType.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	public DlMdStaffType() {
		super(Object.class);
		// super(entityClass);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<EntDbMdStaffType> getStaffTypeDetails() {
		Query query = em
				.createQuery("SELECT e FROM EntDbMdStaffType e");		
		//LOG.info("Query:" + query.toString());
		List<EntDbMdStaffType> result = Collections.EMPTY_LIST;
		try {
			result = query.getResultList();
			LOG.info("Rec found from MD_STAFF_TYPE" + result.size() + " EntDbFltJobAssign ");
			return result;

		} catch (Exception e) {
			LOG.error("retrieving entities from MD_STAFF_TYPE ERROR:",
					e.getMessage());
		}
		return null;
	}

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

}
