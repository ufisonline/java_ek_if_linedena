package com.ufis_as.ufisapp.ek.eao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.lms.entities.EntDbMsgTelex;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlMsgTelexBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlMsgTelexBean extends DlAbstractBean<Object> {

	private static final Logger LOG = LoggerFactory
			.getLogger(DlMsgTelexBean.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public DlMsgTelexBean() {
		super(Object.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	// @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean saveMsgTelex(EntDbMsgTelex msgTelex) {
		try {
			em.persist(msgTelex);
			LOG.debug("Record Insert Successfull");
			return true;
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", msgTelex,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", msgTelex, exc);
		}
		return false;
	}

	// @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbMsgTelex updateMsgTelex(EntDbMsgTelex msgTelex) {
		EntDbMsgTelex msgTelexStat = null;
		try {
			msgTelexStat = em.merge(msgTelex);
			LOG.debug("Merge Successfull");

		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", msgTelex,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", msgTelex, exc);
		}
		return msgTelexStat;
	}

}
