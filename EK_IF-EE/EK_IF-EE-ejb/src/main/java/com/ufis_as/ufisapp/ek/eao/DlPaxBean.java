/*
 * $Id$ Copyright 2012 UFIS
 * Airport Solutions, Inc. All rights reserved. UFIS PROPRIETARY/CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.LoadPaxPK;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author $Author$
 * @version $Revision$
 */
@Stateless(name = "DlPaxBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxBean extends DlAbstractBean<EntDbLoadPax> {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DlPaxBean.class);

	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public DlPaxBean() {
		super(EntDbLoadPax.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public EntDbLoadPax getPax(String intId) {
		EntDbLoadPax result = null;
		Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByid");
		query.setParameter("intId", intId);

		List<EntDbLoadPax> list = query.getResultList();
		if (list != null && list.size() > 0) {
			LOG.debug("{} record found for EntDbPax  by intId={}", list.size(), intId);
			result = list.get(0);
		}
		return result;
	}

	public String getPaxByIdFlight(int idFlight) {
		String intFlid = null;
		Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByIdFlight");
		query.setParameter("idFlight", idFlight);

		List<String> list = query.getResultList();
		if (list.size() > 0) {
			LOG.debug("{} record found for EntDbPax  by idFlight {}", list.size(), idFlight);
			intFlid = list.get(0);
		}
		else
			LOG.debug("No pax record is found for idFlight {}", idFlight);
		return intFlid;
	}
	
	@SuppressWarnings("unchecked")
	public List<EntDbLoadPax> getPaxListByFlightId(String intFlId) {
		List<EntDbLoadPax> rawPaxList = null;
		// Retrieve raw data from database
		Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByFlid");
		query.setParameter("intFlId", intFlId);
		rawPaxList = (List<EntDbLoadPax>) query.getResultList();

		if (rawPaxList != null) {
			LOG.debug("{} record found for EntDbPax  by intFlId={}", rawPaxList.size(), intFlId);
		}
		return rawPaxList;
	}

	public EntDbLoadPax findByPkId(LoadPaxPK pk) {
		EntDbLoadPax entLoadPax = null;
		try {
			Query query = getEntityManager().createNamedQuery("EntDbLoadPax.findByPk");
			query.setParameter("pKId", pk);

			List<EntDbLoadPax> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entLoadPax = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", entLoadPax, e);
		}
		return entLoadPax;
	}

	/*public EntDbLoadPax getPaxForPaxLounge(String paxRefNum, String intFlId) {
		EntDbLoadPax result = null;
		List<EntDbLoadPax> resultList = null;
		Query query = em.createNamedQuery("EntDbLoadPax.findForPaxLounge");
		try {
			query.setParameter("paxRefNum", paxRefNum);
			query.setParameter("intFlId", intFlId);
			resultList = query.getResultList();
			if (resultList.size() > 0) {
				result = resultList.get(0);
				LOG.debug("Pax record found for refNum : <{}>", paxRefNum);
			} else
				LOG.debug("Pax record is not found.");
		} catch (Exception ex) {
			LOG.error("ERROR finding pax for PaxLounge : {}", ex.toString());
		}
		return result;
	}*/

	// BELT, OPERA
	public EntDbLoadPax getPaxWithPaxRefNum(String paxRefNum, String urno) {
		EntDbLoadPax result = null;
		List<EntDbLoadPax> resultList = null;					
		Query query = em.createNamedQuery("EntDbLoadPax.findByPaxIdFlight");
		try {
			int idFlight=Integer.parseInt(urno);
			query.setParameter("paxRefNum", paxRefNum);
			query.setParameter("idFlight", idFlight);
			resultList = query.getResultList();
			if (resultList.size() > 0) {
				result = resultList.get(0);
				LOG.debug("Pax record found for refNum : <{}>", paxRefNum);
			} else
				LOG.debug("Pax record is not found.");
		} catch (NumberFormatException ex) {
			LOG.error("ERROR parsing the String(urno) to int : {}", ex);
		}
		catch (Exception ex) {
			LOG.error("ERROR finding pax for PaxLounge : {}", ex.toString());
		}
		return result;
	}

	public void Persist(EntDbLoadPax entDbPax) {
		if (entDbPax != null) {
			try {
				em.persist(entDbPax);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}", entDbPax, e);
			}
		}
	}

	public void Update(EntDbLoadPax entDbPax) {
		if (entDbPax != null) {
			try {
				em.merge(entDbPax);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}", entDbPax, e);
			}
		}
	}

}
