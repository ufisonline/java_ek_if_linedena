/*
 * $Id: DlPaxConBean.java 8742 2013-09-09 10:07:45Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.ek_if.macs.entities.EnumLoadPaxConnType;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;






/**
 * @author $Author: sch $
 * @version $Revision: 8742 $
 */
@Stateless(name = "DlPaxConBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxConBean extends DlAbstractBean<EntDbLoadPaxConn> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlPaxConBean.class);
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;

    public DlPaxConBean() {
        super(EntDbLoadPaxConn.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return _em;
    }

    public EntDbLoadPaxConn getPaxConn(String intId, EnumLoadPaxConnType connType) {
        EntDbLoadPaxConn result = null;
        Query query = getEntityManager().createNamedQuery("EntDbLoadPaxConn.findByIdAndType");
        query.setParameter("intId", intId);
        query.setParameter("connType", connType);
        List<EntDbLoadPaxConn> list = query.getResultList();
        if (list != null && list.size() > 0) {
        //    LOG.debug("{} record found for EntDbPaxConn  by intId={} and connType={}", list.size(), intId, connType);
            result = list.get(0);
        }
        return result;
    }
    
    // testing use
    public EntDbLoadPaxConn getPaxConnByFliId(String FliId, String system) {
        EntDbLoadPaxConn result = null;
        Query query = getEntityManager().createNamedQuery("EntDbLoadPaxConn.findByFlid");
        query.setParameter("intFlId", FliId);
        query.setParameter("intSystem", system);
        List<EntDbLoadPaxConn> list = query.getResultList();
        if (list != null && list.size() > 0) {
//            LOG.debug("{} record found for EntDbPaxConn  by intId={} and connType={}", list.size(), intId, connType);
            result = list.get(0);
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
	public List<EntDbLoadPaxConn> getPaxConnListByIntflidAndFlightnumber(String intFlId, String airlineCode, String flightNumber, String flightNumberSuffice,  Date scheduledFlightDatetime){
    	List<EntDbLoadPaxConn> paxConnList = null;
    	Timestamp scheduledFlightDatetimeTs =new Timestamp(scheduledFlightDatetime.getTime());
    	
    	Query query = getEntityManager().createNamedQuery("EntDbLoadPaxConn.findByIntflidAndOtherInfo");
		query.setParameter("intFlId", intFlId);
		query.setParameter("airlineCode", airlineCode);
		query.setParameter("flightNumber", flightNumber);
		query.setParameter("flightNumberSuffice", flightNumberSuffice);
		query.setParameter("scheduledFlightDateTime", scheduledFlightDatetimeTs);
		paxConnList = (List<EntDbLoadPaxConn>) query.getResultList();
	       if (paxConnList != null ) {
	            LOG.debug("{} record found for EntDbPaxConn  by intFlId={}", paxConnList.size(), intFlId);
	        }
    	
    	return paxConnList;
    }
    
public List<EntDbLoadPaxConn> getPaxConnListByIntflidAndFlightnumberX(String intFlId, String airlineCode, String flightNumber, String flightNumberSuffice,  Date scheduledFlightDateTime){
		List<EntDbLoadPaxConn> result = null;
		
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
		  filters.add(cb.equal(r.get("intFlId"), intFlId));
		  filters.add(cb.equal(r.get("airlineCode"), airlineCode));
		  filters.add(cb.equal(r.get("flightNumber"), flightNumber));
		  filters.add(cb.equal(r.get("flightNumberSuffice"), flightNumberSuffice));
		  filters.add(cb.equal(r.get("scheduledFlightDateTime"), scheduledFlightDateTime));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		result = getEntityManager().createQuery(cq).getResultList();
			
		return result;	
    }
    
    @SuppressWarnings("unchecked")
	public List<EntDbLoadPaxConn> getPaxConnListByIntFlid(String intFlId){
    	List<EntDbLoadPaxConn> paxConnList = null;
    	Query query = getEntityManager().createNamedQuery("EntDbLoadPaxConn.findByIntFlid");
    	query.setParameter("intFlId", intFlId);
    	paxConnList = (List<EntDbLoadPaxConn>) query.getResultList();
	       if (paxConnList != null ) {
	            LOG.debug("{} record found for EntDbPaxConn  by intFlId={}", paxConnList.size(), intFlId);
	        }
 	
    	return paxConnList;
    }
    
	public List<EntDbLoadPaxConn> getPaxConnListByIntFlidX(String idFlight){
		List<EntDbLoadPaxConn> result = null;
		
			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
			cq.select(r);
			Predicate predicate = cb.equal(r.get("idFlight"), idFlight);
			cq.where(predicate);
			result = getEntityManager().createQuery(cq).getResultList();
			
//			if (resultList != null && resultList.size() > 0){
//				result = resultList.get(0);
//			}
			
			return result;	
    }
    
    
    public EntDbLoadPaxConn findByPkId(String intId, String system){
    	EntDbLoadPaxConn entLoadPaxConn = null; 
		try {
			Query query = getEntityManager().createNamedQuery("EntDbLoadPaxConn.findByPk");
			query.setParameter("intId", intId);
			query.setParameter("intSystem", system);
			
			List<EntDbLoadPaxConn> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entLoadPaxConn = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", entLoadPaxConn, e);
		}
		return entLoadPaxConn;
    	
    }
    
    public EntDbLoadPaxConn findByPkIdX(String intId){
		EntDbLoadPaxConn result = null;
    	
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		 CriteriaQuery<EntDbLoadPaxConn> cq = cb.createQuery(EntDbLoadPaxConn.class);
		Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
		cq.select(r);
		  List<Predicate> filters = new LinkedList<>();
			 filters.add(cb.equal(r.get("intId"), intId));
//		  filters.add(cb.equal(r.get("intRefNumber"), paxRefNum));	
//		  filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		cq.where(cb.and(filters.toArray(new Predicate[0])));
		List<EntDbLoadPaxConn> resultList = getEntityManager().createQuery(cq).getResultList();
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxConn, intId {}", resultList.size(), intId);
			}
		}
		
		return result;
    }
    
    public EntDbLoadPaxConn findByintFltRefX(String intFlId, String intRefNumber){
    	EntDbLoadPaxConn result = null;


		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("intId"), intId);
//		cq.where(predicate);

		
		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("paxConnPK").get("interfaceFltid"), intFlId));
		 filters.add(cb.equal(r.get("paxConnPK").get("intRefNumber"),intRefNumber));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
		  List<EntDbLoadPaxConn> resultList = null;
		  try {
			  resultList = getEntityManager().createQuery(cq).getResultList();
		  }catch(Exception e){
			 LOG.warn("{}",e);
		  }
		
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxConn, intFlId {}, intRefNumber{}", resultList.size(), intFlId, intRefNumber);
			}
		}
		
		return result;	
    }
    
    
    public EntDbLoadPaxConn findByIdLoadPax(String idLoadPax){
    	EntDbLoadPaxConn result = null;

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
		cq.select(r);

		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
		  List<EntDbLoadPaxConn> resultList = null;
		  try {
			  resultList = getEntityManager().createQuery(cq).getResultList();
		  }catch(Exception e){
			 LOG.warn("{}",e);
		  }
		
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxConn, idLoadPax", resultList.size(), idLoadPax);
			}
		}
		
		return result;	
    }
    
    
    public List<EntDbLoadPaxConn> findByFlnoFltDate(String paxConxFlno, Date conxFltDate){
    	List<EntDbLoadPaxConn> result = null;

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConn> r = cq.from(EntDbLoadPaxConn.class);
		cq.select(r);

		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("paxConxFlno"), paxConxFlno));
		 filters.add(cb.equal(r.get("conxFltDate"), conxFltDate));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
			  result = getEntityManager().createQuery(cq).getResultList();
		
		
		return result;	
    }
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void Persist(EntDbLoadPaxConn entDbPaxConn){
    	if (entDbPaxConn != null){
    		try{
    			entDbPaxConn.set_recStatus(" ");
    		_em.persist(entDbPaxConn); 	
    		LOG.debug("entDbPaxConn persisted , intId {}. timeStamp {}",entDbPaxConn.getIntId(), System.currentTimeMillis());
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbPaxConn, e);
    		}
    	}
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void Update(EntDbLoadPaxConn entDbPaxConn){
    	if (entDbPaxConn != null){
    		try{
    			_em.merge(entDbPaxConn); 		
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbPaxConn, e);
    		}
    	}
    }
    
}
