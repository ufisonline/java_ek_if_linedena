/*
 * $Id: DlPaxConBean.java 8742 2013-09-09 10:07:45Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConnX;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;






/**
 * @author $Author: sch $
 * @version $Revision: 8742 $
 */
@Stateless(name = "DlPaxConBeanX")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxConBeanX extends DlAbstractBean<EntDbLoadPaxConnX> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlPaxConBeanX.class);
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;

    public DlPaxConBeanX() {
        super(EntDbLoadPaxConnX.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return _em;
    }
    
    public EntDbLoadPaxConnX findByintFltRefX(String intFlId, String intRefNumber){
    	EntDbLoadPaxConnX result = null;


		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConnX> r = cq.from(EntDbLoadPaxConnX.class);
		cq.select(r);
//		Predicate predicate = cb.equal(r.get("intId"), intId);
//		cq.where(predicate);

		
		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("interfaceFltid"), intFlId));
		 filters.add(cb.equal(r.get("intRefNumber"),intRefNumber));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
		  List<EntDbLoadPaxConnX> resultList = null;
		  try {
			  resultList = getEntityManager().createQuery(cq).getResultList();
		  }catch(Exception e){
			 LOG.warn("{}",e);
		  }		
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxConn, intFlId {}, intRefNumber{}", resultList.size(), intFlId, intRefNumber);
			}
		}
		
		return result;	
    }
    
    public List<EntDbLoadPaxConnX> findByFlnoFltDate(String paxConxFlno, Date conxFltDate){
    	List<EntDbLoadPaxConnX> result = null;

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConnX> r = cq.from(EntDbLoadPaxConnX.class);
		cq.select(r);

		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("paxConxFlno"), paxConxFlno));
		 filters.add(cb.equal(r.get("conxFltDate"), conxFltDate));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
			  result = getEntityManager().createQuery(cq).getResultList();
		
		
		return result;	
    }
    
    public EntDbLoadPaxConnX findByIdLoadPax(String idLoadPax){
    	EntDbLoadPaxConnX result = null;

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<EntDbLoadPaxConnX> r = cq.from(EntDbLoadPaxConnX.class);
		cq.select(r);

		 List<Predicate> filters = new LinkedList<>();
		 filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));
//		 filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
		  cq.where(cb.and(filters.toArray(new Predicate[0])));
		
		  List<EntDbLoadPaxConnX> resultList = null;
		  try {
			  resultList = getEntityManager().createQuery(cq).getResultList();
		  }catch(Exception e){
			 LOG.warn("{}",e);
		  }
		
		
		if (resultList != null && resultList.size() > 0){
			result = resultList.get(0);
			if (resultList.size() > 1){
				LOG.warn("Attention ! {} duplicate records found for paxConn, idLoadPax", resultList.size(), idLoadPax);
			}
		}
		
		return result;	
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void Persist(EntDbLoadPaxConnX entDbPaxConn){
    	if (entDbPaxConn != null){
    		try{
    			entDbPaxConn.set_recStatus(" ");
    		_em.persist(entDbPaxConn); 	
    		LOG.debug("entDbPaxConn persisted , intId {}. timeStamp {}",entDbPaxConn.getIntId(), System.currentTimeMillis());
    		}catch(Exception e){
    			LOG.error("Exception Entity:{} , Error:{}", entDbPaxConn, e);
    		}
    	}
    }

  
    
}
