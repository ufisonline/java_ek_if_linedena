package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.opera.entities.EntDbPaxLoungeMove;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author btr
 * Session Bean implementation class DlPaxLoungeMoveBean */
@Stateless
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxLoungeMoveBean extends DlAbstractBean<EntDbPaxLoungeMove>{

	public static final Logger LOG = LoggerFactory.getLogger(EntDbPaxLoungeMove.class);
	  
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DlPaxLoungeMoveBean() {
    	super(EntDbPaxLoungeMove.class);
    }
    
    public EntDbPaxLoungeMove merge(EntDbPaxLoungeMove entity){
    	EntDbPaxLoungeMove loungeMove = null;
    	try{
    		loungeMove = em.merge(entity);
    	}
    	catch(Exception ex){
    		LOG.error("ERROR to create new pax lounge move!!! {}", ex.toString());
    	}
    	return loungeMove;
    }
    
    public EntDbPaxLoungeMove getPaxLoungeMove(String paxRefNum, BigDecimal id_Flight){
    	List<EntDbPaxLoungeMove> resultList = null;
    	EntDbPaxLoungeMove result = null;
    	
    	Query query = getEntityManager().createNamedQuery("EntDbPaxLoungeMove.getByPaxRegnKey");
    	try{
	    	query.setParameter("paxRegnKey", paxRefNum);
	    	query.setParameter("id_Flight", id_Flight);
	    	resultList = query.getResultList();
	    	if(resultList.size()>0){
	    		result = resultList.get(0);
	    		LOG.debug("Pax lounge move is found. ID : {}", result.getId());
	    	}
	    	else
	    		LOG.debug("Pax lounge move is not found.");
    	}
    	catch(Exception ex){
    		LOG.debug("ERROR when retrieving pax lounge move : {}", ex);
    	}
    	return result;
    }
}
