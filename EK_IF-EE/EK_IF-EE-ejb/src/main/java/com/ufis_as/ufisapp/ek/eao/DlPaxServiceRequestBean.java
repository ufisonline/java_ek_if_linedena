/*
 * $Id: DlPaxServiceRequestBean.java 8992 2013-09-23 07:40:10Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequest;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @author $Author: sch $
 * @version $Revision: 8992 $
 */
@Stateless(name = "DlPaxServiceRequestBean")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxServiceRequestBean extends DlAbstractBean<EntDbServiceRequest> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlPaxServiceRequestBean.class);
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;


    public DlPaxServiceRequestBean() {
        super(EntDbServiceRequest.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return _em;
    }
    
    
    public EntDbServiceRequest findByPkId(String intId, String system){
    	EntDbServiceRequest entDbPaxServiceRequest = null; 
		try {
			Query query = getEntityManager().createNamedQuery("EntDbServiceRequest.findByPk");
			query.setParameter("intId", intId);
			query.setParameter("intSystem", system);
			
			List<EntDbServiceRequest> list = query.getResultList();
			if (list != null && list.size() > 0) {
				entDbPaxServiceRequest = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", entDbPaxServiceRequest, e);
		}
		return entDbPaxServiceRequest;
    }
    
    public EntDbServiceRequest findByPkIdX(String intFlId, String intRefNumber){
        EntDbServiceRequest result = null;
     
     CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
     CriteriaQuery cq = cb.createQuery();
     Root<EntDbServiceRequest> r = cq.from(EntDbServiceRequest.class);
   //  cq.select(r.get("intId"));
     cq.select(r);
       List<Predicate> filters = new LinkedList<>();
       
//       filters.add(cb.equal(r.get("intId"), intId));
       filters.add(cb.equal(r.get("serviceRequestPK").get("intFlId"), intFlId));
       filters.add(cb.equal(r.get("serviceRequestPK").get("intRefNumber"), intRefNumber));
//       filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
     cq.where(cb.and(filters.toArray(new Predicate[0])));
     List<EntDbServiceRequest> resultList = getEntityManager().createQuery(cq).getResultList();
     
     if (resultList != null && resultList.size() > 0){
      result = resultList.get(0);
      	if (resultList.size() > 1){
      		LOG.warn("Attention ! {} duplicate records found for ServiceREquest, intFlId {}, intRefNumber {}", resultList.size(), intFlId, intRefNumber );
      	}
     }
     
     return result;
       }
    
    
    public List<EntDbServiceRequest> findByidLoadPax(String idLoadPax){
        List<EntDbServiceRequest> result = null;
     
     CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
     CriteriaQuery cq = cb.createQuery();
     Root<EntDbServiceRequest> r = cq.from(EntDbServiceRequest.class);
   //  cq.select(r.get("intId"));
     cq.select(r);
       List<Predicate> filters = new LinkedList<>();
       filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));
//       filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
     cq.where(cb.and(filters.toArray(new Predicate[0])));
     result = getEntityManager().createQuery(cq).getResultList();
     
     
     return result;
       }
    
    public String findByPkId2(String intId, String system){
    	String uuid = null; 
		try {
			Query query = getEntityManager().createNamedQuery("EntDbServiceRequest.findByPk2");
			query.setParameter("intId", intId);
			query.setParameter("intSystem", system);
			
			List<String> list = query.getResultList();
			if (list != null && list.size() > 0) {
				uuid = list.get(0);
			}
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", uuid, e);
		}
		return uuid;
    }
    
    public EntDbServiceRequest findByIdFlightAndPaxRefNum(int idFlight, String intRefNumber){
    	EntDbServiceRequest entDbPaxServiceRequest = null; 
		try {
			Query query = getEntityManager().createNamedQuery("EntDbServiceRequest.findByIdFlightAndPaxRefNum");
			query.setParameter("idFlight", idFlight);
			query.setParameter("intRefNumber", intRefNumber);
			
			List<EntDbServiceRequest> list = query.getResultList();
			if (list.size() > 0) {
				entDbPaxServiceRequest = list.get(0);
				LOG.debug("Service Request is found for idFlight <{}> and paxRefNumber <{}>", idFlight, intRefNumber);
			}
			else
				LOG.debug("Service Request is not found for idFlight <{}> and paxRefNumber <{}>", idFlight, intRefNumber);
		} catch (Exception e) {
			LOG.error("Exception entLoadPax:{} , Error:{}", entDbPaxServiceRequest, e);
		}
		return entDbPaxServiceRequest;
    	
    }
    
	public List<EntDbServiceRequest> findPaxRequest(BigDecimal idFlight,
			String idLoadPax, String serviceCode, String interRefNum) {
		List<EntDbServiceRequest> requests = new ArrayList<>();
		try {
    		StringBuilder sb = new StringBuilder();
    		sb.append(" SELECT a FROM EntDbServiceRequest a ");
    		sb.append(" WHERE a.serviceCode = :serviceCode ");
    		if (HpUfisUtils.isNotEmptyStr(idLoadPax)) {
    			sb.append(" AND a.idLoadPax = :idLoadPax");
    		} else {
    			sb.append(" AND a.idFlight = :idFlight ");
    			sb.append(" AND a.intRefNumber = :interRefNum");
    		}
    		Query query = getEntityManager().createQuery(sb.toString());
    		query.setParameter("serviceCode", serviceCode);
    		if (HpUfisUtils.isNotEmptyStr(idLoadPax)) {
    			query.setParameter("idLoadPax", idLoadPax);
    		} else {
    			query.setParameter("idFlight", idFlight.intValue());
        		query.setParameter("interRefNum", interRefNum);
    		}
    		requests = query.getResultList();
    	} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}
    	return requests;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Persist(EntDbServiceRequest entDbPaxServiceRequest) {
		if (entDbPaxServiceRequest != null) {
			try {
				entDbPaxServiceRequest.set_recStatus(" ");
				_em.persist(entDbPaxServiceRequest);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}",
						entDbPaxServiceRequest, e);
			}
		}
	}

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Update(EntDbServiceRequest entDbPaxServiceRequest) {
		if (entDbPaxServiceRequest != null) {
			try {
				_em.merge(entDbPaxServiceRequest);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}",
						entDbPaxServiceRequest, e);
			}
		}
	}
	
}
