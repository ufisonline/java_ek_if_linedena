/*
 * $Id: DlPaxServiceRequestBean.java 8992 2013-09-23 07:40:10Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.eao;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequestX;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

/**
 * @author $Author: sch $
 * @version $Revision: 8992 $
 */
@Stateless(name = "DlPaxServiceRequestBeanX")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlPaxServiceRequestBeanX extends DlAbstractBean<EntDbServiceRequestX> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlPaxServiceRequestBeanX.class);
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;


    public DlPaxServiceRequestBeanX() {
        super(EntDbServiceRequestX.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return _em;
    }
    
    public EntDbServiceRequestX findByPkIdX(String intFlId, String intRefNumber){
        EntDbServiceRequestX result = null;
     
     CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
     CriteriaQuery cq = cb.createQuery();
     Root<EntDbServiceRequestX> r = cq.from(EntDbServiceRequestX.class);
   //  cq.select(r.get("intId"));
     cq.select(r);
       List<Predicate> filters = new LinkedList<>();
       
//       filters.add(cb.equal(r.get("intId"), intId));
       filters.add(cb.equal(r.get("intFlId"), intFlId));
       filters.add(cb.equal(r.get("intRefNumber"), intRefNumber));
//       filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
     cq.where(cb.and(filters.toArray(new Predicate[0])));
     List<EntDbServiceRequestX> resultList = getEntityManager().createQuery(cq).getResultList();
     
     if (resultList != null && resultList.size() > 0){
      result = resultList.get(0);
      	if (resultList.size() > 1){
      		LOG.warn("Attention ! {} duplicate records found for ServiceREquest, intFlId {}, intRefNumber {}", resultList.size(), intFlId, intRefNumber );
      	}
     }
     
     return result;
       }
    
    
    public List<EntDbServiceRequestX> findByidLoadPax(String idLoadPax){
        List<EntDbServiceRequestX> result = null;
     
     CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
     CriteriaQuery cq = cb.createQuery();
     Root<EntDbServiceRequestX> r = cq.from(EntDbServiceRequestX.class);
   //  cq.select(r.get("intId"));
     cq.select(r);
       List<Predicate> filters = new LinkedList<>();
       filters.add(cb.equal(r.get("idLoadPax"), idLoadPax));
//       filters.add(cb.not(cb.in(r.get("_recStatus")).value("X")));
		  filters.add(cb.notEqual(r.get("_recStatus"),"X"));
     cq.where(cb.and(filters.toArray(new Predicate[0])));
     result = getEntityManager().createQuery(cq).getResultList();
     
     
     return result;
       }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void Persist(EntDbServiceRequestX entDbPaxServiceRequest) {
		if (entDbPaxServiceRequest != null) {
			try {
				entDbPaxServiceRequest.set_recStatus(" ");
				_em.persist(entDbPaxServiceRequest);
			} catch (Exception e) {
				LOG.error("Exception Entity:{} , Error:{}",
						entDbPaxServiceRequest, e);
			}
		}
	}
    
    
	
}
