package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlStaffShift")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlStaffShift extends DlAbstractBean<Object> implements IDlStaffShiftLocal {

	public DlStaffShift() {
		super(Object.class);
	}

	private static final Logger LOG = LoggerFactory.getLogger(DlStaffShift.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	public EntDbStaffShift merge(EntDbStaffShift staffShift) {
		EntDbStaffShift result = null;
		try {
			result = em.merge(staffShift);
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}",
					staffShift, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", staffShift, exc);
		}
		return result;
	}

	@Override
	public EntDbStaffShift findById(String idStaffShift) {
		EntDbStaffShift entStaffShift = null;
		//String[] staffTypeList = new String[]{"ROP", "SPHL"};
		//String staffType = "'ROP', 'SPHL'";
		try {
			Query query = em.createNamedQuery("EntDbStaffShift.findById");
			query.setParameter("id", idStaffShift);
			query.setParameter("recStatus", " ");
			//query.setParameter("staffType", staffType);

			List<EntDbStaffShift> entStaffShiftList = query.getResultList();
			if (entStaffShiftList.size() > 0) {
				entStaffShift = entStaffShiftList.get(0);
				LOG.debug("Staff shift <{}> is found.", idStaffShift);
			} else
				LOG.debug("Staff shift is not found..");
		} catch (Exception ex) {
			LOG.debug("ERROR when retrieving existing flt_job_Assign: {}",
					ex.toString());
		}
		return entStaffShift;
	}

}
