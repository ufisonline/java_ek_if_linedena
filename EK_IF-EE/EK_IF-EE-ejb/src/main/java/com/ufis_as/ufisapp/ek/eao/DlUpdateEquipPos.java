package com.ufis_as.ufisapp.ek.eao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.proveo.entities.EntDbEquipPos;
import com.ufis_as.ufisapp.ek.eao.generic.DlAbstractBean;

@Stateless(mappedName = "DlUpdateEquipPos")
@TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
public class DlUpdateEquipPos extends DlAbstractBean<Object> implements IDlUpdateEquipPosLocal {

	private static final Logger LOG = LoggerFactory
			.getLogger(DlUpdateEquipPos.class);
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public DlUpdateEquipPos() {
		super(Object.class);		
	}

	@Override
	protected EntityManager getEntityManager() {
		LOG.debug("EntityManager. {}", em);
		return em;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EntDbEquipPos saveEqiupPosUpdate(EntDbEquipPos euipPosUpdate) {

		EntDbEquipPos equipPos = null;
		try {
			equipPos=em.merge(euipPosUpdate);
			LOG.info("Record Inserted Successfully.");
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", euipPosUpdate, exc);
		}
		return equipPos;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateEqiupPos(EntDbEquipPos euipPosUpdate) {
		try {
			em.merge(euipPosUpdate);
			LOG.debug("Merge Successfull");

		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", euipPosUpdate,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", euipPosUpdate, exc);
		}

	}


}
