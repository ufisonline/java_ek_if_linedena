package com.ufis_as.ufisapp.ek.eao.generic;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class BaseDTO {
	
	/**
	 * DTFL and Current Proccssing message urno(irmtab)
	 */
	public String dtfl = "";
	public String tableRef = "";
	public long irmtabUrno = 0;
	
	/**
	 * JAXB 
	 */
	public JAXBContext _cnxJaxb;
	public JAXBContext _cnxJaxbM;
	public Unmarshaller _um;
	public Marshaller _ms;
	
	/**
	 * Error/warning DataStore
	 */
	public List<Object> data = null;
	public List<String> dataItem = null;
	public String exptCode = null;
	public String exptData = null;
	public String exptCateg = null; 
	
	public BaseDTO() {
		data = new ArrayList<>();
		dataItem = new ArrayList<>();
	}

	public String getDtfl() {
		return dtfl;
	}

	public void setDtfl(String dtfl) {
		this.dtfl = dtfl;
	}

	public long getIrmtabUrno() {
		return irmtabUrno;
	}

	public void setIrmtabUrno(long irmtabUrno) {
		this.irmtabUrno = irmtabUrno;
	}

	public JAXBContext getCnxJaxb() {
		return _cnxJaxb;
	}

	public void setCnxJaxb(JAXBContext _cnxJaxb) {
		this._cnxJaxb = _cnxJaxb;
	}

	public JAXBContext getCnxJaxbM() {
		return _cnxJaxbM;
	}

	public void setCnxJaxbM(JAXBContext _cnxJaxbM) {
		this._cnxJaxbM = _cnxJaxbM;
	}

	public Unmarshaller getUm() {
		return _um;
	}

	public void setUm(Unmarshaller _um) {
		this._um = _um;
	}

	public Marshaller getMs() {
		return _ms;
	}

	public void setMs(Marshaller _ms) {
		this._ms = _ms;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public List<String> getDataItem() {
		return dataItem;
	}

	public void setDataItem(List<String> dataItem) {
		this.dataItem = dataItem;
	}

	public String getExptCode() {
		return exptCode;
	}

	public void setExptCode(String exptCode) {
		this.exptCode = exptCode;
	}

	public String getExptData() {
		return exptData;
	}

	public void setExptData(String exptData) {
		this.exptData = exptData;
	}
	
	public String getTableRef() {
		return tableRef;
	}

	public void setTableRef(String tableRef) {
		this.tableRef = tableRef;
	}

	public String getExptCateg() {
		return exptCateg;
	}

	public void setExptCateg(String exptCateg) {
		this.exptCateg = exptCateg;
	}

	public void addExptInfo(String code, String data) {
		this.dataItem = new ArrayList<>();
		this.dataItem.add(String.valueOf(irmtabUrno));
		this.dataItem.add(tableRef);
		this.dataItem.add(code);
		this.dataItem.add(exptCateg);
		this.dataItem.add(data);
		this.dataItem.add(dtfl);
		this.data.add(dataItem);
	}
}
