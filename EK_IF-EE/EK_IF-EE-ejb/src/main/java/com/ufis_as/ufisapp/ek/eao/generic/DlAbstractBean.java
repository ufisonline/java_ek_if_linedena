/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ufisapp.ek.eao.generic;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.server.eao.generic.IAbstractBeanRemote;

/**
 *
 * @author fou
 */
public abstract class DlAbstractBean<T> implements IAbstractBeanRemote<T> {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlAbstractBean.class);
    private Class<T> _entityClass;

    public DlAbstractBean(Class<T> entityClass) {
        this._entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    @Override
    public T create(T entity) {
        try {
            getEntityManager().persist(entity);
        } catch (OptimisticLockException Oexc) {
            //getEntityManager().refresh(entity);
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
        return entity;
    }

    @Override
    public T edit(T entity) {
        try {
            getEntityManager().merge(entity);
        } catch (OptimisticLockException Oexc) {
            //getEntityManager().refresh(entity);
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
        return entity;
    }

    @Override
    public void remove(T entity) {
        try {
            getEntityManager().remove(getEntityManager().merge(entity));
        } catch (OptimisticLockException Oexc) {
            //getEntityManager().refresh(entity);
            LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
        } catch (Exception exc) {
            LOG.error("Exception Entity:{} , Error:{}", entity, exc);
        }
    }

    @Override
    public T find(Object id) {
        return getEntityManager().find(_entityClass, id);
    }

    @Override
    public List<T> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(_entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findAll(String namedQuery) {
        LOG.debug("retrieving all " + _entityClass.getName());

        Query query = getEntityManager().createNamedQuery(namedQuery);
        List<T> result = query.getResultList();

        if (LOG.isDebugEnabled()) {
            LOG.trace("found " + result.size() + _entityClass.getName());
        }

        return result;
    }

    public List<Object> findBasicDataAll(Class entityClass, String fetchGroup) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(_entityClass));
        return getEntityManager().createQuery(cq).getResultList();

//        ReadAllQuery query = new ReadAllQuery(entityClass);
//        query.setFetchGroupName(fetchGroup);
//        query.setIsReadOnly(true);
//        Session session = JpaHelper.getEntityManagerFactory(getEntityManager()).getServerSession();
//        return (List) session.executeQuery(query);
    }

    @Override
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(_entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @Override
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(_entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
