package com.ufis_as.ufisapp.ek.hp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;

/**
 * @author btr
 */
public class HpUfisFlightUtils {

	private static final Logger LOG = LoggerFactory
			.getLogger(HpUfisFlightUtils.class);

	public HpUfisFlightUtils() {

	}

	public static EntDbAfttab searchDestCodeInVial(EntDbAfttab obj,
			String destCode) {
		EntDbAfttab result = null;
		int index = 1; // first char is space
		long vianValue = Long.parseLong(obj.getVian().trim());
		if (vianValue > 0) {
			for (int i = 0; i < vianValue; i++) {
				if (obj.getVial().substring(index, index + 3)
						.equalsIgnoreCase(destCode)) {
					LOG.debug("Destination matched with VIAL at index: {}", index);
					result = obj; break;
				}
				index += 120;
			}
		} else {
			LOG.debug("Destination code <{}> is not found in VIAL.", destCode);
		}
		return result;
	}
}
