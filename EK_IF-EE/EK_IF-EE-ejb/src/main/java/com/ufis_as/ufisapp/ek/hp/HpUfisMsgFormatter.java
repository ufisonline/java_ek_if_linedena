package com.ufis_as.ufisapp.ek.hp;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.configuration.HpEKConstants;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;

/**
 * @author btr
 */
public class HpUfisMsgFormatter {

	private static final Logger LOG = LoggerFactory
			.getLogger(HpUfisMsgFormatter.class);
	
	public HpUfisMsgFormatter() {
		
	}

	public static String formExceptionData(List<Object> msgExpEntity,
			String cmd, Long irmtabRef, boolean isLast, String dtflString) {
		String msg = null;
		if (msgExpEntity == null) {
			LOG.info("No error identified to communicate.");
		} else {
			// get the HEAD info
			EntUfisMsgHead header = new EntUfisMsgHead();
			header.setHop(HpEKConstants.HOPO);
			header.setOrig(dtflString);
			header.setApp(dtflString);
			header.setUsr(dtflString);
			if (isLast) {
				header.setBcnum(-1);
			}
			
			List<String> idFlightList = new ArrayList<String>();
			List<String> idList = new ArrayList<>();
			header.setIdFlight(idFlightList);

			List<String> fldList = new ArrayList<>();
			fldList.add("ID_REF_TABLE");
			fldList.add("REF_TABLE");
			fldList.add("EXCEPTION_CODE");
			fldList.add("EXCEPTION_CATEGORY");
			fldList.add("EXCEPTION_DATA");
			fldList.add("DATA_SOURCE");
			
			List<EntUfisMsgACT> listAction = new ArrayList<>();
			EntUfisMsgACT action = new EntUfisMsgACT();
			action.setCmd(cmd);
			action.setTab("MSG_EXCEPTION");
			action.setFld(fldList);
			action.setData(msgExpEntity);
			action.setId(idList);
			listAction.add(action);

			msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		}
		return msg;
	}
	
	public static String formNotifyMsg(
			String cmd, 
			boolean isLast, 
			String dtfl,
			String table,
			List<String> fields,
			List<Object> data) {
		String msg = null;
		if (data == null) {
			LOG.info("No error identified to communicate.");
		} else {
			// get the HEAD info
			EntUfisMsgHead header = new EntUfisMsgHead();
			header.setHop(HpEKConstants.HOPO);
			header.setOrig(dtfl);
			header.setApp(dtfl);
			header.setUsr(dtfl);
			if (isLast) {
				header.setBcnum(-1);
			}
			
			List<String> idFlightList = new ArrayList<String>();
			List<String> idList = new ArrayList<>();
			header.setIdFlight(idFlightList);

			List<EntUfisMsgACT> listAction = new ArrayList<>();
			EntUfisMsgACT action = new EntUfisMsgACT();
			action.setCmd(cmd);
			action.setTab(table);
			action.setFld(fields);
			action.setData(data);
			action.setId(idList);
			listAction.add(action);

			msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		}
		return msg;
	}
}

