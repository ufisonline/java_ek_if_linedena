package com.ufis_as.ufisapp.ek.hp;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConnX;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxX;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequestX;
import com.ufis_as.ufisapp.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @version 1.0 2013-10-21 JGO - Create notification message formatter
 * @version 1.1 2013-10-24 JGO - Fix the nullpointer issue with no column annotation for field
 * @version 1.2 2013-10-30 JGO - Fix the IRT message size issue(size not match with fld)
 * @version 1.3 2013-11-01 JGO - Adding id_flight and id holder support
 *                               Change fld, odat, dat from instance variable to normal
 * @author JGO
 *
 */
public class HpUfisNotifyFormatter {

	private static final Logger LOG = LoggerFactory.getLogger(HpUfisNotifyFormatter.class);
	private static final HpUfisNotifyFormatter instance = new HpUfisNotifyFormatter();
	
	private String dtfl = null;
	private EntUfisMsgHead header = null;
//	private List<EntUfisMsgACT> actions = null;
//	
//	private List<String> fld = null;
//	private List<Object> odat = null;
//	private List<Object> dat = null;
	
	private HpUfisNotifyFormatter() {
//		actions = new ArrayList<>();
//		fld = new ArrayList<>();
//		dat = new ArrayList<>();
//		odat = new ArrayList<>();
		dtfl = HpCommonConfig.dtfl;
		
		header = new EntUfisMsgHead();
		header.setApp(dtfl);
		header.setOrig(dtfl);
		header.setUsr(dtfl);
		header.setHop(HpEKConstants.HOPO);
		
	}
	
	public static HpUfisNotifyFormatter getInstance() {
		return instance;
	}
	
	
	/**
	 * Format notification message and convert to json. According to
	 * configuration make correspond verify and filter or exclude.
	 * @param isLast
	 * @param cmd
	 * @param idFlight(if not provide, auto-fill from entity data)
	 * @param oldData
	 * @param data
	 * @return
	 */
	public String transform(boolean isLast, UfisASCommands cmd,
			String idFlight, Object oldData, Object data) {
		String notificationMsg = null;
		try {
//			fld.clear();
//			odat.clear();
//			dat.clear();
//			actions.clear();
			header.getIdFlight().clear();
			
			List<EntUfisMsgACT> actions = new ArrayList<>();
			List<String> fld = new ArrayList<>();
			List<Object> odat = new ArrayList<>();
			List<Object> dat = new ArrayList<>();
			
			if (cmd != null) {
				// Add idFlight
				if (HpUfisUtils.isNotEmptyStr(idFlight)) {
					header.getIdFlight().add(idFlight);
				}
				
				// Compare two object are from same class
				if (data != null || oldData != null) {
					// Get the class
					Class<?> clazz = (data == null) ? oldData.getClass() : data.getClass();
					Object temp = (data == null) ? oldData : data;
					
					// Table
					boolean isMatched = false;
					String tabName = "";
					for (Annotation ann : clazz.getAnnotations()) {
						if (ann instanceof Table) {
							Table tab = (Table) ann;
							tabName = tab.name();
							// check table whether in config list
							if (tabName != null) {
								if (HpCommonConfig.tables.contains(tabName.toUpperCase())) {
									isMatched = true;
								} else {
									LOG.warn("Table={} is not found in the notify configuration list", tabName);
								}
							} else {
								LOG.warn("Table name not found in Entity={} annotation", clazz.getSimpleName());
							}
						}
					}
					
					if (isMatched) {
						String idValue = null;
						String flightUrno = null;
						boolean isFlightUrno = false;
						
						long startTime = System.currentTimeMillis();
						
						// Fields
						boolean isPkeyFound = false;
						List<String> pkeyColumn = new ArrayList<>();
						List<Object> pkeyValue = new ArrayList<>();
						for (Field f : clazz.getDeclaredFields()) {
							// --------------------- PKey Field Handle ------------------------
							// check whether is primary key field
							Id id = f.getAnnotation(Id.class);
							if (id != null) {
								Column idCol = f.getAnnotation(Column.class);
								if (idCol != null && idCol.name() != null) {
									pkeyColumn.add(idCol.name());
									// check modifier if private set accessible
									if (f.getModifiers() != Modifier.PUBLIC) {
										f.setAccessible(true);
									}
									pkeyValue.add(f.get(temp));
									isPkeyFound = true;
									// adding ID column to ID holder(
									if (HpUfisAppConstants.CON_ID.equals(idCol.name())) {
										idValue = (f.get(temp) == null ? "" : f.get(temp).toString());
									}
									
									// adding id_flight to id_flight holder
									if (HpUfisAppConstants.CON_AFTTAB.equals(tabName)){
										flightUrno = (f.get(temp) == null ? "" : f.get(temp).toString());
										isFlightUrno = true;
									}
									continue;
								} else {
									LOG.warn("Notification: Name of Column annotation is required for field={}", f.getName());
								}
							}
							
							// check whether is composite key field
							EmbeddedId cpzKey = f.getAnnotation(EmbeddedId.class);
							if (cpzKey != null) {
								Object cpzValue = f.get(temp);
								// get composite key columns
								Class<?> cpzClazz = f.getType();
								//LOG.debug("check 5");
								for (Field cpzField : cpzClazz.getDeclaredFields()) {
									Column cpzCol = cpzField.getAnnotation(Column.class);
									if (cpzCol != null && cpzCol.name() != null) {
										pkeyColumn.add(cpzCol.name());
										// check modifier if private set accessible
										if (cpzField.getModifiers() != Modifier.PUBLIC) {
											cpzField.setAccessible(true);
										}					//	LOG.debug("check 7");
										pkeyValue.add(cpzField.get(cpzValue));
										//LOG.debug("check 8");										
										isPkeyFound = true;
									} else {
										LOG.warn("Notification: Name of Column annotation is required for field={}", cpzField.getName());
									}
								}
								continue;
							}
							// --------------------- Non-PKey Field Handle ------------------------
							PropertyDescriptor pd = null;
							try {
								pd = new PropertyDescriptor(f.getName(), clazz);
							} catch (IntrospectionException e) {
								LOG.debug("No property found for field={}", f.getName());
								continue;
							}
							Method getterMethod = pd.getReadMethod();
	
							// According to command to build fields, odat, data
							// DRT: notify id only(DRT mark as deleted rec_status)
							// IRT: notify all
							// URT: notify all or change only
							Column col = f.getAnnotation(Column.class);
							if (col == null || col.name() == null) {
								LOG.warn("Column name not found in annotation for {} field={}", tabName, f.getName());
								continue;
							}
							
							// if idFlights empty or null, check id_flight from field to auto-fill
							if (header.getIdFlight().size() == 0) {
								// whether is flight urno(UAFT, URNO of AFTTAB, ID_FLIGHT)
								if (HpUfisAppConstants.CON_UAFT.equals(col.name())
										|| HpUfisAppConstants.CON_ID_FLIGHT.equals(col.name())) {
									isFlightUrno =  true;
								}
							}
							
							Object tempOldValue = null;
							Object tempNewValue = null;
							switch (cmd) {
							case DRT:
								//tempNewValue = getterMethod.invoke(data);
								//fld.add(col.name().toUpperCase());
								//dat.add(tempOldValue);
								break;
							case IRT:
								if (data != null) {
									tempNewValue = getterMethod.invoke(data);
									fld.add(col.name().toUpperCase());
									dat.add(tempNewValue);
									if (isFlightUrno && tempNewValue != null) {
										flightUrno = tempNewValue.toString();
										//LOG.debug("check 12");
										}
								} else {
									LOG.warn("Data parameter is required for IRT notification");
								}
								break;
							case URT:
								if (data != null && oldData != null) {
									tempNewValue = getterMethod.invoke(data);
									tempOldValue = getterMethod.invoke(oldData);
									String fldsConfig = HpCommonConfig.fields.get(tabName);
									// flight urno 
									if (isFlightUrno && tempNewValue != null) {
										flightUrno = tempNewValue.toString();
									}
									//LOG.debug("check 14");
									if (HpUfisUtils.isNotEmptyStr(fldsConfig)) {
										// check whether only send changed fields
										if (fldsConfig.equals(HpUfisAppConstants.FLD_CHANGED)) {
											if (tempNewValue != null && !tempNewValue.equals(tempOldValue)) {
												fld.add(col.name().toUpperCase());
												odat.add(tempOldValue);
												dat.add(tempNewValue);
											}
										// check whether send all fields
										} else if (fldsConfig.equals(HpUfisAppConstants.FLD_ALL)) {
											fld.add(col.name().toUpperCase());
											odat.add(tempOldValue);
											dat.add(tempNewValue);
										// check whether send selected fields only
										} else if (fldsConfig.contains(col.name().toUpperCase())) {
												fld.add(col.name().toUpperCase());
												odat.add(tempOldValue);
												dat.add(tempNewValue);
										} else {
											LOG.warn("Column={} is not contained in the notify fields list", col.name().toUpperCase());
										}
									} else {
										LOG.warn("Notify Field Configuration cannot be null or empty");
									}
								} else {
									LOG.warn("Both old and new data are required for URT notification");
								}
								break;
							default:
								LOG.warn("Unsupported notification command: {}", cmd);
								break;
							}
							// Add id of the flight
							if (isFlightUrno && header.getIdFlight().size() == 0) {
								header.getIdFlight().add(flightUrno);
							}
						}
						
						LOG.debug("process fields, takes {}",System.currentTimeMillis()-startTime);
						
						// verify fld, data and old data size
						boolean isSizeMatched = true;
						if (UfisASCommands.IRT == cmd) {
							if (fld.size() != dat.size()) {
								isSizeMatched = false;
							}
						} else if (UfisASCommands.URT == cmd) {
							if (fld.size() != dat.size() || fld.size() != odat.size()) {
								isSizeMatched = false;
							}
						}
						if (isSizeMatched) {
							// exclusion check
							String exclusion = HpCommonConfig.exclusions.get(tabName);
							if ((UfisASCommands.DRT != cmd) && HpUfisUtils.isNotEmptyStr(exclusion)) {
								String[] excFields = exclusion.split(",");
								for (String s : excFields) {
									int index = fld.indexOf(s.trim());
									if (index != -1) {
										fld.remove(index);
										dat.remove(index);
										if (UfisASCommands.URT == cmd) {
											odat.remove(index);
										}
									}
								}
							}
							// check whether the ID column is defined in parent class
							if (!isPkeyFound) {
								Class<?> superClazz = clazz.getSuperclass();
								do {
									for (Field f : superClazz.getDeclaredFields()) {
										// check whether is primary key field
										Id id = f.getAnnotation(Id.class);
										if (id != null) {
											Column idCol = f.getAnnotation(Column.class);
											if (idCol != null && idCol.name() != null) {
												pkeyColumn.add(idCol.name());
												// check modifier if private set accessible
												if (f.getModifiers() != Modifier.PUBLIC) {
													f.setAccessible(true);
												}
												pkeyValue.add(f.get(temp));
												isPkeyFound = true;
												// add ID column to ID holder
												if (HpUfisAppConstants.CON_ID.equals(idCol.name())) {
													idValue = (f.get(temp) == null ? "" : f.get(temp).toString());
												}
												break;
											} else {
												LOG.warn("Notification: Name of Column annotation is required for field={}", f.getName());
											}
										}
									}
								} while ((superClazz = superClazz.getSuperclass()) != null);
							} 
							
							// Build the selection tag: where clause
							StringBuilder sb = new StringBuilder();
							if (!isPkeyFound) {
								LOG.warn("PKey not found for notitciation message");
							} else {
								if (pkeyColumn.size() <= pkeyValue.size()) {
									sb.append(" WHERE ");
									for (int i = 0; i < pkeyColumn.size(); i++) {
										if (i != 0) {
											sb.append(" AND ");
										}
										sb.append(" ").append(pkeyColumn.get(i)).append(" = ");
										Object value = pkeyValue.get(i);
										if ((value instanceof Short)
												|| (value instanceof Integer)
												|| (value instanceof Long)
												|| (value instanceof Double)
												|| (value instanceof Float)) {
											sb.append(value);
										} else {
											sb.append("'").append(value).append("'");
										}
									}
								}
								
								// Format notification message
								header.setReqt(HpUfisCalendar.getCurrentUTCTimeString());
								if (isLast) {
									header.setBcnum(-1);
								} else {
									header.setBcnum(1);
								}
								
								// TODO may need to support multiple action
								EntUfisMsgACT act = new EntUfisMsgACT();
								act.setCmd(cmd.toString());
								act.setTab(tabName);
								act.setFld(fld);
								act.setData(dat);
								act.setOdat(odat);
								act.setSel(sb.toString());
								if (idValue != null) {
									act.getId().add(idValue);
								}
								actions.add(act);
								LOG.debug("Before 'notificationMsg = HpUfisJsonMsgFormatter.formDataInJson(actions, header);'");
								notificationMsg = HpUfisJsonMsgFormatter.formDataInJson(actions, header);
								LOG.debug("Notification FLD size: {}", fld.size());
								LOG.debug("Notification DAT size: {}", dat.size());
								LOG.debug("Notification ODAT size: {}", odat.size());
								LOG.debug("Notification Msg: {}", notificationMsg);
							}
						}
					}
				} else {
					LOG.warn("Invalid notification info: DATA and ODAT are null ");
				}
			}
		} catch (Exception e) {
			LOG.error("Cannot format notification message due to: {}", e);
		}
		return notificationMsg;
	}
	
	
	public String transformForPax(boolean isLast, UfisASCommands cmd,
			String idFlight, Object oldData, Object data) {
		
		String notificationMsg = null;
		
		header.getIdFlight().clear();
		
		List<EntUfisMsgACT> actions = new ArrayList<>();
		List<String> fld = new ArrayList<>();
		List<String> ld = new ArrayList<>();
		List<Object> odat = new ArrayList<>();
		List<Object> dat = new ArrayList<>();
		String tabName = "";
		String selectStr = "";
		String idFlightStr ="";
		
		try{
		if (data instanceof EntDbLoadPaxX){
			EntDbLoadPaxX loadPax = (EntDbLoadPaxX) data;
//			fld.add(Integer.toString(loadPax.getIdFlight()));
			tabName = "LOAD_PAX";
			selectStr = "WHERE ID = '"+loadPax.getUuid()+"'";
			idFlightStr = Integer.toString(loadPax.getIdFlight());
			ld.add(loadPax.getUuid());
			// add fields
//			fld.add("ID");
			 fld.add("INTFLID");
			 fld.add("INTREFNUMBER");
			 fld.add("DATA_SOURCE");
			 fld.add("ID_FLIGHT");
			 fld.add("ID_LOAD_PAX_CONNECT");
			 fld.add("BAGNOOFPIECES");
			 fld.add("BAGTAGPRINT");
			 fld.add("BAGWEIGHT");
			 fld.add("BOARDINGPASSPRINT");
			 fld.add("BOARDINGSTATUS");
			 fld.add("BOOKEDCLASS");
			 fld.add("CABINCLASS");
			 fld.add("UPGRADE_INDICATOR");
			 fld.add("TRANSIT_INDICATOR");
			 fld.add("CANCELLED");
			 fld.add("JTOP_PAX");
			 fld.add("TRANSIT_BAG_INDICATOR");
			 fld.add("E_TICKET_ID");
			 fld.add("TICKET_NUMBER");
			 fld.add("COUPON_NUMBER");
			 fld.add("APD_TYPE");
			 fld.add("DOCUMENT_TYPE");
			 fld.add("DOCUMENT_NUMBER");
			 fld.add("DOCUMENT_EXPIRY_DATE");
			 fld.add("DOCUMENT_ISSUED_DATE");
			 fld.add("DOCUMENT_ISSUED_COUNTRY");
			 fld.add("COUNTRY_OF_BIRTH");
			 fld.add("COUNTRY_OF_RESIDENCE");
			 fld.add("ITN_EMBARKATION");
			 fld.add("ITN_DISEMBARKATION");
			 fld.add("PAX_STATUS");
			 fld.add("CHECKINDATETIME");
			 fld.add("DESTINATION");
			 fld.add("DOB");
			 fld.add("ETKTYPE");
			 fld.add("GENDER");
			 fld.add("HANDICAPPED");
			 fld.add("INFANTINDICATOR");
			 fld.add("INTID");
			 fld.add("NATIONALITY");
			 fld.add("OFFLOADEDPAX");
			 fld.add("PAXBOOKINGSTATUS");
			 fld.add("PAXGROUPCODE");
			 fld.add("PAXNAME");
			 fld.add("PAXTYPE");
			 fld.add("PRIORITYPAX");
			 fld.add("STATUSONBOARD");
			 fld.add("TRAVELLEDCLASS");
			 fld.add("SEAT_NUMBER");
			 fld.add("UNACCOMPANIEDMINOR");
			 fld.add("BAGTAGINFO");
			 fld.add("SCANLOCATION");
			 fld.add("SCANDATETIME");
			 fld.add("CHECKINAGENTCODE");
			 fld.add("CHECKINHANDLINGAGENT");
			 fld.add("CHECKINSEQUENCE");
			 fld.add("CHECKINCITY");
			 fld.add("BOARDINGDATETIME");
			 fld.add("BOARDINGAGENTCODE");
			 fld.add("BOARDINGHANDLINGAGENT");
			 fld.add("ID_CONX_FLIGHT");  // ADDED BY 2013-12-23
			// populate data
//			dat.add(loadPax.getUuid());
			 dat.add(loadPax.getIntFlId());
			 dat.add(loadPax.getIntRefNumber());
			 dat.add(loadPax.getIntSystem());
			 dat.add(loadPax.getIdFlight());
			 dat.add(loadPax.getIdLoadPaxConnect());
			 dat.add(loadPax.getBagNoOfPieces());
			 dat.add(loadPax.getBagTagPrint());
			 dat.add(loadPax.getBagWeight());
			 dat.add(loadPax.getBoardingPassprint());
			 dat.add(loadPax.getBoardingStatus());
			 dat.add(loadPax.getBookedClass());
			 dat.add(loadPax.getCabinClass());
			 dat.add(loadPax.getUpgradeIndicator());
			 dat.add(loadPax.getTransitIndicator());
			 dat.add(loadPax.getCancelled());
			 dat.add(loadPax.getJtopPax());
			 dat.add(loadPax.getTransitBagIndicator());
			 dat.add(loadPax.getETickedId());
			 dat.add(loadPax.getTicketNumber());
			 dat.add(loadPax.getCouponNumber());
			 dat.add(loadPax.getApdType());
			 dat.add(loadPax.getDocumentType());
			 dat.add(loadPax.getDocumentNumber());
			 dat.add(loadPax.getDocumentExpiryDate());
			 dat.add(loadPax.getDocumentIssuedDate());
			 dat.add(loadPax.getDocumentIssuedCountry());
			 dat.add(loadPax.getCountryOfBirth());
			 dat.add(loadPax.getCountryOfResidence());
			 dat.add(loadPax.getItnEmbarkation());
			 dat.add(loadPax.getItnDisembarkation());
			 dat.add(loadPax.getPaxStatus());
			 dat.add(loadPax.getCheckInDateTime());
			 dat.add(loadPax.getDestination());
			 dat.add(loadPax.getDob());
			 dat.add(loadPax.getEtkType());
			 dat.add(loadPax.getGender());
			 dat.add(loadPax.getHandicapped());
			 dat.add(loadPax.getInfantIndicator());
			 dat.add(loadPax.getIntId());
			 dat.add(loadPax.getNationality());
			 dat.add(loadPax.getOffLoadedPax());
			 dat.add(loadPax.getPaxBookingStatus());
			 dat.add(loadPax.getPaxGroupCode());
			 dat.add(loadPax.getPaxName());
			 dat.add(loadPax.getPaxType());
			 dat.add(loadPax.getPriorityPax());
			 dat.add(loadPax.getStatusOnboard());
			 dat.add(loadPax.getTravelledClass());
			 dat.add(loadPax.getSeatNumber());
			 dat.add(loadPax.getUnAccompaniedMinor());
			 dat.add(loadPax.getBagTagInfo());
			 dat.add(loadPax.getScanLocation());
			 dat.add(loadPax.getScanDateTime());
			 dat.add(loadPax.getCheckInAgentCode());
			 dat.add(loadPax.getCheckInHandlingAgent());
			 dat.add(loadPax.getCheckInSequence());
			 dat.add(loadPax.getCheckInCity());
			 dat.add(loadPax.getBoardingDateTime());
			 dat.add(loadPax.getBoardingAgentCode());
			 dat.add(loadPax.getBoardingHandlingAgent());
			 dat.add(loadPax.getIdConxFlight()); // ADDED BY 2013-12-23
			 
			 if (oldData != null && oldData instanceof EntDbLoadPaxX){
				EntDbLoadPaxX oloadPax = (EntDbLoadPaxX) oldData;
//				 odat.add(oloadPax.getUuid());
				 odat.add(oloadPax.getIntFlId());
				 odat.add(oloadPax.getIntRefNumber());
				 odat.add(oloadPax.getIntSystem());
				 odat.add(oloadPax.getIdFlight());
				 odat.add(oloadPax.getIdLoadPaxConnect());
				 odat.add(oloadPax.getBagNoOfPieces());
				 odat.add(oloadPax.getBagTagPrint());
				 odat.add(oloadPax.getBagWeight());
				 odat.add(oloadPax.getBoardingPassprint());
				 odat.add(oloadPax.getBoardingStatus());
				 odat.add(oloadPax.getBookedClass());
				 odat.add(oloadPax.getCabinClass());
				 odat.add(oloadPax.getUpgradeIndicator());
				 odat.add(oloadPax.getTransitIndicator());
				 odat.add(oloadPax.getCancelled());
				 odat.add(oloadPax.getJtopPax());
				 odat.add(oloadPax.getTransitBagIndicator());
				 odat.add(oloadPax.getETickedId());
				 odat.add(oloadPax.getTicketNumber());
				 odat.add(oloadPax.getCouponNumber());
				 odat.add(oloadPax.getApdType());
				 odat.add(oloadPax.getDocumentType());
				 odat.add(oloadPax.getDocumentNumber());
				 odat.add(oloadPax.getDocumentExpiryDate());
				 odat.add(oloadPax.getDocumentIssuedDate());
				 odat.add(oloadPax.getDocumentIssuedCountry());
				 odat.add(oloadPax.getCountryOfBirth());
				 odat.add(oloadPax.getCountryOfResidence());
				 odat.add(oloadPax.getItnEmbarkation());
				 odat.add(oloadPax.getItnDisembarkation());
				 odat.add(oloadPax.getPaxStatus());
				 odat.add(oloadPax.getCheckInDateTime());
				 odat.add(oloadPax.getDestination());
				 odat.add(oloadPax.getDob());
				 odat.add(oloadPax.getEtkType());
				 odat.add(oloadPax.getGender());
				 odat.add(oloadPax.getHandicapped());
				 odat.add(oloadPax.getInfantIndicator());
				 odat.add(oloadPax.getIntId());
				 odat.add(oloadPax.getNationality());
				 odat.add(oloadPax.getOffLoadedPax());
				 odat.add(oloadPax.getPaxBookingStatus());
				 odat.add(oloadPax.getPaxGroupCode());
				 odat.add(oloadPax.getPaxName());
				 odat.add(oloadPax.getPaxType());
				 odat.add(oloadPax.getPriorityPax());
				 odat.add(oloadPax.getStatusOnboard());
				 odat.add(oloadPax.getTravelledClass());
				 odat.add(oloadPax.getSeatNumber());
				 odat.add(oloadPax.getUnAccompaniedMinor());
				 odat.add(oloadPax.getBagTagInfo());
				 odat.add(oloadPax.getScanLocation());
				 odat.add(oloadPax.getScanDateTime());
				 odat.add(oloadPax.getCheckInAgentCode());
				 odat.add(oloadPax.getCheckInHandlingAgent());
				 odat.add(oloadPax.getCheckInSequence());
				 odat.add(oloadPax.getCheckInCity());
				 odat.add(oloadPax.getBoardingDateTime());
				 odat.add(oloadPax.getBoardingAgentCode());
				 odat.add(oloadPax.getBoardingHandlingAgent());
				 odat.add(oloadPax.getIdConxFlight());    // ADDED BY 2013-12-23
			 }
			 
		}else if(data instanceof EntDbLoadPaxConnX){
			EntDbLoadPaxConnX loadPaxConn = (EntDbLoadPaxConnX) data;
//			fld.add(Integer.toString(loadPaxConn.getIdFlight()));
			tabName = "LOAD_PAX_CONNECT";
			selectStr = "WHERE ID = '"+loadPaxConn.getUuid()+"'";
			idFlightStr = Integer.toString(loadPaxConn.getIdFlight());
			ld.add(loadPaxConn.getUuid());
			// add fields
//			 fld.add("ID");
			 fld.add("INTERFACE_FLTID");
			 fld.add("INTREFNUMBER");
			 fld.add("ID_FLIGHT");
			 fld.add("ID_LOAD_PAX");
			 fld.add("BOARDPOINT");
			 fld.add("BOOKEDCLASS");
			 fld.add("CONNSTATUS");
			 fld.add("CONNTYPE");
			 fld.add("INTID");
			 fld.add("INTERFACE_CONX_FLTID");
			 fld.add("PAX_CONX_FLNO");
			 fld.add("CONX_FLT_DATE");
			 fld.add("ID_CONX_FLIGHT");
			 fld.add("DATA_SOURCE");
			 fld.add("OFFPOINT");
			 // populate data
//			 dat.add(loadPaxConn.getUuid());
			 dat.add(loadPaxConn.getInterfaceFltid());
			 dat.add(loadPaxConn.getIntRefNumber());
			 dat.add(loadPaxConn.getIdFlight());
			 dat.add(loadPaxConn.getIdLoadPax());
			 dat.add(loadPaxConn.getBoardPoint());
			 dat.add(loadPaxConn.getBookedClass());
			 dat.add(loadPaxConn.getConnStatus());
			 dat.add(loadPaxConn.getConnType());
			 dat.add(loadPaxConn.getIntId());
			 dat.add(loadPaxConn.getInterfaceConxFltid());
			 dat.add(loadPaxConn.getPaxConxFlno());
			 dat.add(loadPaxConn.getConxFltDate());
			 dat.add(loadPaxConn.getIdConxFlight());
			 dat.add(loadPaxConn.getIntSystem());
			 dat.add(loadPaxConn.getOffPoint());
			 
			 if (oldData != null &&  oldData instanceof EntDbLoadPaxConnX){
				 EntDbLoadPaxConnX oloadPaxConn = (EntDbLoadPaxConnX) oldData;
//				 odat.add(oloadPaxConn.getUuid());
				 odat.add(oloadPaxConn.getInterfaceFltid());
				 odat.add(oloadPaxConn.getIntRefNumber());
				 odat.add(oloadPaxConn.getIdFlight());
				 odat.add(oloadPaxConn.getIdLoadPax());
				 odat.add(oloadPaxConn.getBoardPoint());
				 odat.add(oloadPaxConn.getBookedClass());
				 odat.add(oloadPaxConn.getConnStatus());
				 odat.add(oloadPaxConn.getConnType());
				 odat.add(oloadPaxConn.getIntId());
				 odat.add(oloadPaxConn.getInterfaceConxFltid());
				 odat.add(oloadPaxConn.getPaxConxFlno());
				 odat.add(oloadPaxConn.getConxFltDate());
				 odat.add(oloadPaxConn.getIdConxFlight());
				 odat.add(oloadPaxConn.getIntSystem());
				 odat.add(oloadPaxConn.getOffPoint());
			 }


		}else if (data instanceof EntDbServiceRequestX){
			EntDbServiceRequestX serviceRequest = (EntDbServiceRequestX) data;
//			fld.add(Integer.toString(serviceRequest.getIdFlight()));
			tabName = "SERVICE_REQUEST";
			selectStr = "WHERE ID = '"+serviceRequest.getUuid()+"'";
			idFlightStr = Integer.toString(serviceRequest.getIdFlight());
			ld.add(serviceRequest.getUuid());
			// add fields
//			fld.add("ID");
			 fld.add("INTFLID");
			 fld.add("INTREFNUMBER");
			 fld.add("ID_FLIGHT");
			 fld.add("DATA_SOURCE");
			 fld.add("INTID");
			 fld.add("REQUESTTYPE");
			 fld.add("SERVICECODE");
			 fld.add("SERVICETYPE");
			 fld.add("EXTINFO");
			 fld.add("ID_LOAD_PAX");
			 // populate fields
//			dat.add(serviceRequest.getUuid());
			 dat.add(serviceRequest.getIntFlId());
			 dat.add(serviceRequest.getIntRefNumber());
			 dat.add(serviceRequest.getIdFlight());
			 dat.add(serviceRequest.getIntSystem());
			 dat.add(serviceRequest.getIntId());
			 dat.add(serviceRequest.getRequestType());
			 dat.add(serviceRequest.getServiceCode());
			 dat.add(serviceRequest.getServiceType());
			 dat.add(serviceRequest.getExtInfo());
			 dat.add(serviceRequest.getIdLoadPax());
			 
			 if (odat != null && odat instanceof EntDbServiceRequestX){
				 EntDbServiceRequestX oserviceRequest = (EntDbServiceRequestX) oldData;
//				dat.add(oserviceRequest.getUuid());
				 odat.add(oserviceRequest.getIntFlId());
				 odat.add(oserviceRequest.getIntRefNumber());
				 odat.add(oserviceRequest.getIdFlight());
				 odat.add(oserviceRequest.getIntSystem());
				 odat.add(oserviceRequest.getIntId());
				 odat.add(oserviceRequest.getRequestType());
				 odat.add(oserviceRequest.getServiceCode());
				 odat.add(oserviceRequest.getServiceType());
				 odat.add(oserviceRequest.getExtInfo());
				 odat.add(oserviceRequest.getIdLoadPax());
			 }
		}else if (data instanceof EntDbLoadBag){
			EntDbLoadBag loadBag = (EntDbLoadBag) data;
//			fld.add(Long.toString(loadBag.getIdFlight()));
			tabName = "LOAD_BAG";
			selectStr = "WHERE ID = '"+loadBag.getId()+"'";
			idFlightStr = Long.toString(loadBag.getIdFlight());
			ld.add(loadBag.getId());
			// add fields
//			fld.add("ID");
			 fld.add("ARR_FLIGHT_NUMBER");
			 fld.add("ARR_FLT_DATE");
			 fld.add("ARR_FLT_DEST3");
			 fld.add("ARR_FLT_ORIGIN3");
			 fld.add("BAG_CLASS");
			 fld.add("BAG_CLASSIFY");
			 fld.add("BAG_TAG");
			 fld.add("BAG_TYPE");
			 fld.add("CHANGE_DATE");
			 fld.add("DATA_SOURCE");
			 fld.add("DEP_FLIGHT_NUMBER");
			 fld.add("DEP_FLT_DATE");
			 fld.add("DEP_FLT_DEST3");
			 fld.add("DEP_FLT_ORIGIN3");
			 fld.add("ID_ARR_FLIGHT");
			 fld.add("ID_DEP_FLIGHT");
			 fld.add("ID_LOAD_PAX");
			 fld.add("MSG_SEND_DATE");
			 fld.add("PAX_NAME");
			 fld.add("PAX_REF_NUM");
			 fld.add("REC_STATUS");
			 fld.add("BAG_TAG_STR");
			 fld.add("BAGWEIGHT");
			 // added 2014-01-06
			 fld.add("ID_FLIGHT");
			 fld.add("ID_CONX_FLIGHT");
			// populate data
//			 dat.add(loadBag.getId());
			 dat.add(loadBag.getArrFlightNumber());
			 dat.add(loadBag.getArrFltDate());
			 dat.add(loadBag.getArrFltDest3());
			 dat.add(loadBag.getArrFltOrigin3());
			 dat.add(loadBag.getBagClass());
			 dat.add(loadBag.getBagClassify());
			 dat.add(loadBag.getBagTag());
			 dat.add(loadBag.getBagType());
			 dat.add(loadBag.getChangeDate());
			 dat.add(loadBag.getDataSource());
			 dat.add(loadBag.getFlightNumber());
			 dat.add(loadBag.getFltDate());
			 dat.add(loadBag.getFltDest3());
			 dat.add(loadBag.getFltOrigin3());
			 dat.add(loadBag.getIdArrFlight());
			 dat.add(loadBag.getIdFlight());
			 dat.add(loadBag.getIdLoadPax());
			 dat.add(loadBag.getMsgSendDate());
			 dat.add(loadBag.getPaxName());
			 dat.add(loadBag.getPaxRefNum());
			 dat.add(loadBag.getRecStatus());
			 dat.add(loadBag.getBagTagStr());
			 dat.add(loadBag.getBagWeight());
			 // added 2014-01-06
			 dat.add(loadBag.getId_Flight());
			 dat.add(loadBag.getIdConxFlight());
			 if (oldData != null && oldData instanceof EntDbLoadBag){
				 EntDbLoadBag oloadBag = (EntDbLoadBag) oldData;
//				 odat.add(oloadBag.getId());
				 odat.add(oloadBag.getArrFlightNumber());
				 odat.add(oloadBag.getArrFltDate());
				 odat.add(oloadBag.getArrFltDest3());
				 odat.add(oloadBag.getArrFltOrigin3());
				 odat.add(oloadBag.getBagClass());
				 odat.add(oloadBag.getBagClassify());
				 odat.add(oloadBag.getBagTag());
				 odat.add(oloadBag.getBagType());
				 odat.add(oloadBag.getChangeDate());
				 odat.add(oloadBag.getDataSource());
				 odat.add(oloadBag.getFlightNumber());
				 odat.add(oloadBag.getFltDate());
				 odat.add(oloadBag.getFltDest3());
				 odat.add(oloadBag.getFltOrigin3());
				 odat.add(oloadBag.getIdArrFlight());
				 odat.add(oloadBag.getIdFlight());
				 odat.add(oloadBag.getIdLoadPax());
				 odat.add(oloadBag.getMsgSendDate());
				 odat.add(oloadBag.getPaxName());
				 odat.add(oloadBag.getPaxRefNum());
				 odat.add(oloadBag.getRecStatus());
				 odat.add(oloadBag.getBagTagStr());
				 odat.add(oloadBag.getBagWeight());
				 // added 2014-01-06
				 odat.add(oloadBag.getId_Flight());
				 odat.add(oloadBag.getIdConxFlight());
			 }
			
		}
		
		header.getIdFlight().add(idFlightStr);
		header.setReqt(HpUfisCalendar.getCurrentUTCTimeString());
		if (isLast) {
			header.setBcnum(-1);
		} else {
			header.setBcnum(1);
		}
		
		EntUfisMsgACT act = new EntUfisMsgACT();
		act.setCmd(cmd.toString());
		act.setTab(tabName);
		act.setFld(fld);
		act.setData(dat);
		act.setOdat(odat);
		act.setSel(selectStr);
		act.setId(ld);
		
		actions.add(act);
		
		notificationMsg = HpUfisJsonMsgFormatter.formDataInJson(actions, header);
		}catch(Exception e){
			LOG.error("erro happen when transformForPax, {}",e);
		}
		
//		LOG.debug(notificationMsg);
		
		return notificationMsg;
		
	}
	
}
