package com.ufis_as.ufisapp.ek.hp;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConnX;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxX;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequestX;
import com.ufis_as.ufisapp.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;

/**
*
 * @author SCH
 *
 */
@Stateless
public class HpUfisNotifyFormatterForPax {

	private static final Logger LOG = LoggerFactory.getLogger(HpUfisNotifyFormatter.class);
//	private static final HpUfisNotifyFormatter instance = new HpUfisNotifyFormatter();
	
	private String dtfl = null;
//	private EntUfisMsgHead header = null;
//	private List<EntUfisMsgACT> actions = null;
//	
//	private List<String> fld = null;
//	private List<Object> odat = null;
//	private List<Object> dat = null;
	
	public HpUfisNotifyFormatterForPax() {
//		actions = new ArrayList<>();
//		fld = new ArrayList<>();
//		dat = new ArrayList<>();
//		odat = new ArrayList<>();
//		dtfl = HpCommonConfig.dtfl;
//		
//		header = new EntUfisMsgHead();
//		header.setApp(dtfl);
//		header.setOrig(dtfl);
//		header.setUsr(dtfl);
//		header.setHop(HpEKConstants.HOPO);
		
	}
	
//	public static HpUfisNotifyFormatter getInstance() {
//		return instance;
//	}
	
	
	/**
	 * Format notification message and convert to json. According to
	 * configuration make correspond verify and filter or exclude.
	 * @param isLast
	 * @param cmd
	 * @param idFlight(if not provide, auto-fill from entity data)
	 * @param oldData
	 * @param data
	 * @return
	 */
	
	
	public String transformForPax(boolean isLast, UfisASCommands cmd,
			String idFlight, Object oldData, Object data) {
		
		String notificationMsg = null;
		
	     dtfl = HpCommonConfig.dtfl;
		
	    EntUfisMsgHead header  = new EntUfisMsgHead();
		header.setApp(dtfl);
		header.setOrig(dtfl);
		header.setUsr(dtfl);
		header.setHop(HpEKConstants.HOPO);
		
		header.getIdFlight().clear();
		
		List<EntUfisMsgACT> actions = new ArrayList<>();
		List<String> fld = new ArrayList<>();
		List<String> ld = new ArrayList<>();
		List<Object> odat = new ArrayList<>();
		List<Object> dat = new ArrayList<>();
		String tabName = "";
		String selectStr = "";
		String idFlightStr ="";
		
		try{
		if (data instanceof EntDbLoadPaxX){
			EntDbLoadPaxX loadPax = (EntDbLoadPaxX) data;
//			fld.add(Integer.toString(loadPax.getIdFlight()));
			tabName = "LOAD_PAX";
			selectStr = "WHERE ID = '"+loadPax.getUuid()+"'";
			idFlightStr = Integer.toString(loadPax.getIdFlight());
			ld.add(loadPax.getUuid());
			// add fields
//			fld.add("ID");
			 fld.add("INTFLID");
			 fld.add("INTREFNUMBER");
			 fld.add("DATA_SOURCE");
			 fld.add("ID_FLIGHT");
			 fld.add("ID_LOAD_PAX_CONNECT");
			 fld.add("BAGNOOFPIECES");
			 fld.add("BAGTAGPRINT");
			 fld.add("BAGWEIGHT");
			 fld.add("BOARDINGPASSPRINT");
			 fld.add("BOARDINGSTATUS");
			 fld.add("BOOKEDCLASS");
			 fld.add("CABINCLASS");
			 fld.add("UPGRADE_INDICATOR");
			 fld.add("TRANSIT_INDICATOR");
			 fld.add("CANCELLED");
			 fld.add("JTOP_PAX");
			 fld.add("TRANSIT_BAG_INDICATOR");
			 fld.add("E_TICKET_ID");
			 fld.add("TICKET_NUMBER");
			 fld.add("COUPON_NUMBER");
			 fld.add("APD_TYPE");
			 fld.add("DOCUMENT_TYPE");
			 fld.add("DOCUMENT_NUMBER");
			 fld.add("DOCUMENT_EXPIRY_DATE");
			 fld.add("DOCUMENT_ISSUED_DATE");
			 fld.add("DOCUMENT_ISSUED_COUNTRY");
			 fld.add("COUNTRY_OF_BIRTH");
			 fld.add("COUNTRY_OF_RESIDENCE");
			 fld.add("ITN_EMBARKATION");
			 fld.add("ITN_DISEMBARKATION");
			 fld.add("PAX_STATUS");
			 fld.add("CHECKINDATETIME");
			 fld.add("DESTINATION");
			 fld.add("DOB");
			 fld.add("ETKTYPE");
			 fld.add("GENDER");
			 fld.add("HANDICAPPED");
			 fld.add("INFANTINDICATOR");
			 fld.add("INTID");
			 fld.add("NATIONALITY");
			 fld.add("OFFLOADEDPAX");
			 fld.add("PAXBOOKINGSTATUS");
			 fld.add("PAXGROUPCODE");
			 fld.add("PAXNAME");
			 fld.add("PAXTYPE");
			 fld.add("PRIORITYPAX");
			 fld.add("STATUSONBOARD");
			 fld.add("TRAVELLEDCLASS");
			 fld.add("SEAT_NUMBER");
			 fld.add("UNACCOMPANIEDMINOR");
			 fld.add("BAGTAGINFO");
			 fld.add("SCANLOCATION");
			 fld.add("SCANDATETIME");
			 fld.add("CHECKINAGENTCODE");
			 fld.add("CHECKINHANDLINGAGENT");
			 fld.add("CHECKINSEQUENCE");
			 fld.add("CHECKINCITY");
			 fld.add("BOARDINGDATETIME");
			 fld.add("BOARDINGAGENTCODE");
			 fld.add("BOARDINGHANDLINGAGENT");
			 fld.add("ID_CONX_FLIGHT");  // ADDED BY 2013-12-23
			// populate data
//			dat.add(loadPax.getUuid());
			 dat.add(loadPax.getIntFlId());
			 dat.add(loadPax.getIntRefNumber());
			 dat.add(loadPax.getIntSystem());
			 dat.add(loadPax.getIdFlight());
			 dat.add(loadPax.getIdLoadPaxConnect());
			 dat.add(loadPax.getBagNoOfPieces());
			 dat.add(loadPax.getBagTagPrint());
			 dat.add(loadPax.getBagWeight());
			 dat.add(loadPax.getBoardingPassprint());
			 dat.add(loadPax.getBoardingStatus());
			 dat.add(loadPax.getBookedClass());
			 dat.add(loadPax.getCabinClass());
			 dat.add(loadPax.getUpgradeIndicator());
			 dat.add(loadPax.getTransitIndicator());
			 dat.add(loadPax.getCancelled());
			 dat.add(loadPax.getJtopPax());
			 dat.add(loadPax.getTransitBagIndicator());
			 dat.add(loadPax.getETickedId());
			 dat.add(loadPax.getTicketNumber());
			 dat.add(loadPax.getCouponNumber());
			 dat.add(loadPax.getApdType());
			 dat.add(loadPax.getDocumentType());
			 dat.add(loadPax.getDocumentNumber());
			 dat.add(loadPax.getDocumentExpiryDate());
			 dat.add(loadPax.getDocumentIssuedDate());
			 dat.add(loadPax.getDocumentIssuedCountry());
			 dat.add(loadPax.getCountryOfBirth());
			 dat.add(loadPax.getCountryOfResidence());
			 dat.add(loadPax.getItnEmbarkation());
			 dat.add(loadPax.getItnDisembarkation());
			 dat.add(loadPax.getPaxStatus());
			 dat.add(loadPax.getCheckInDateTime());
			 dat.add(loadPax.getDestination());
			 dat.add(loadPax.getDob());
			 dat.add(loadPax.getEtkType());
			 dat.add(loadPax.getGender());
			 dat.add(loadPax.getHandicapped());
			 dat.add(loadPax.getInfantIndicator());
			 dat.add(loadPax.getIntId());
			 dat.add(loadPax.getNationality());
			 dat.add(loadPax.getOffLoadedPax());
			 dat.add(loadPax.getPaxBookingStatus());
			 dat.add(loadPax.getPaxGroupCode());
			 dat.add(loadPax.getPaxName());
			 dat.add(loadPax.getPaxType());
			 dat.add(loadPax.getPriorityPax());
			 dat.add(loadPax.getStatusOnboard());
			 dat.add(loadPax.getTravelledClass());
			 dat.add(loadPax.getSeatNumber());
			 dat.add(loadPax.getUnAccompaniedMinor());
			 dat.add(loadPax.getBagTagInfo());
			 dat.add(loadPax.getScanLocation());
			 dat.add(loadPax.getScanDateTime());
			 dat.add(loadPax.getCheckInAgentCode());
			 dat.add(loadPax.getCheckInHandlingAgent());
			 dat.add(loadPax.getCheckInSequence());
			 dat.add(loadPax.getCheckInCity());
			 dat.add(loadPax.getBoardingDateTime());
			 dat.add(loadPax.getBoardingAgentCode());
			 dat.add(loadPax.getBoardingHandlingAgent());
			 dat.add(loadPax.getIdConxFlight()); // ADDED BY 2013-12-23
			 
			 if (oldData != null && oldData instanceof EntDbLoadPaxX){
				EntDbLoadPaxX oloadPax = (EntDbLoadPaxX) oldData;
//				 odat.add(oloadPax.getUuid());
				 odat.add(oloadPax.getIntFlId());
				 odat.add(oloadPax.getIntRefNumber());
				 odat.add(oloadPax.getIntSystem());
				 odat.add(oloadPax.getIdFlight());
				 odat.add(oloadPax.getIdLoadPaxConnect());
				 odat.add(oloadPax.getBagNoOfPieces());
				 odat.add(oloadPax.getBagTagPrint());
				 odat.add(oloadPax.getBagWeight());
				 odat.add(oloadPax.getBoardingPassprint());
				 odat.add(oloadPax.getBoardingStatus());
				 odat.add(oloadPax.getBookedClass());
				 odat.add(oloadPax.getCabinClass());
				 odat.add(oloadPax.getUpgradeIndicator());
				 odat.add(oloadPax.getTransitIndicator());
				 odat.add(oloadPax.getCancelled());
				 odat.add(oloadPax.getJtopPax());
				 odat.add(oloadPax.getTransitBagIndicator());
				 odat.add(oloadPax.getETickedId());
				 odat.add(oloadPax.getTicketNumber());
				 odat.add(oloadPax.getCouponNumber());
				 odat.add(oloadPax.getApdType());
				 odat.add(oloadPax.getDocumentType());
				 odat.add(oloadPax.getDocumentNumber());
				 odat.add(oloadPax.getDocumentExpiryDate());
				 odat.add(oloadPax.getDocumentIssuedDate());
				 odat.add(oloadPax.getDocumentIssuedCountry());
				 odat.add(oloadPax.getCountryOfBirth());
				 odat.add(oloadPax.getCountryOfResidence());
				 odat.add(oloadPax.getItnEmbarkation());
				 odat.add(oloadPax.getItnDisembarkation());
				 odat.add(oloadPax.getPaxStatus());
				 odat.add(oloadPax.getCheckInDateTime());
				 odat.add(oloadPax.getDestination());
				 odat.add(oloadPax.getDob());
				 odat.add(oloadPax.getEtkType());
				 odat.add(oloadPax.getGender());
				 odat.add(oloadPax.getHandicapped());
				 odat.add(oloadPax.getInfantIndicator());
				 odat.add(oloadPax.getIntId());
				 odat.add(oloadPax.getNationality());
				 odat.add(oloadPax.getOffLoadedPax());
				 odat.add(oloadPax.getPaxBookingStatus());
				 odat.add(oloadPax.getPaxGroupCode());
				 odat.add(oloadPax.getPaxName());
				 odat.add(oloadPax.getPaxType());
				 odat.add(oloadPax.getPriorityPax());
				 odat.add(oloadPax.getStatusOnboard());
				 odat.add(oloadPax.getTravelledClass());
				 odat.add(oloadPax.getSeatNumber());
				 odat.add(oloadPax.getUnAccompaniedMinor());
				 odat.add(oloadPax.getBagTagInfo());
				 odat.add(oloadPax.getScanLocation());
				 odat.add(oloadPax.getScanDateTime());
				 odat.add(oloadPax.getCheckInAgentCode());
				 odat.add(oloadPax.getCheckInHandlingAgent());
				 odat.add(oloadPax.getCheckInSequence());
				 odat.add(oloadPax.getCheckInCity());
				 odat.add(oloadPax.getBoardingDateTime());
				 odat.add(oloadPax.getBoardingAgentCode());
				 odat.add(oloadPax.getBoardingHandlingAgent());
				 odat.add(oloadPax.getIdConxFlight());    // ADDED BY 2013-12-23
			 }
			 
		}else if(data instanceof EntDbLoadPaxConnX){
			EntDbLoadPaxConnX loadPaxConn = (EntDbLoadPaxConnX) data;
//			fld.add(Integer.toString(loadPaxConn.getIdFlight()));
			tabName = "LOAD_PAX_CONNECT";
			selectStr = "WHERE ID = '"+loadPaxConn.getUuid()+"'";
			idFlightStr = Integer.toString(loadPaxConn.getIdFlight());
			ld.add(loadPaxConn.getUuid());
			// add fields
//			 fld.add("ID");
			 fld.add("INTERFACE_FLTID");
			 fld.add("INTREFNUMBER");
			 fld.add("ID_FLIGHT");
			 fld.add("ID_LOAD_PAX");
			 fld.add("BOARDPOINT");
			 fld.add("BOOKEDCLASS");
			 fld.add("CONNSTATUS");
			 fld.add("CONNTYPE");
			 fld.add("INTID");
			 fld.add("INTERFACE_CONX_FLTID");
			 fld.add("PAX_CONX_FLNO");
			 fld.add("CONX_FLT_DATE");
			 fld.add("ID_CONX_FLIGHT");
			 fld.add("DATA_SOURCE");
			 fld.add("OFFPOINT");
			 // populate data
//			 dat.add(loadPaxConn.getUuid());
			 dat.add(loadPaxConn.getInterfaceFltid());
			 dat.add(loadPaxConn.getIntRefNumber());
			 dat.add(loadPaxConn.getIdFlight());
			 dat.add(loadPaxConn.getIdLoadPax());
			 dat.add(loadPaxConn.getBoardPoint());
			 dat.add(loadPaxConn.getBookedClass());
			 dat.add(loadPaxConn.getConnStatus());
			 dat.add(loadPaxConn.getConnType());
			 dat.add(loadPaxConn.getIntId());
			 dat.add(loadPaxConn.getInterfaceConxFltid());
			 dat.add(loadPaxConn.getPaxConxFlno());
			 dat.add(loadPaxConn.getConxFltDate());
			 dat.add(loadPaxConn.getIdConxFlight());
			 dat.add(loadPaxConn.getIntSystem());
			 dat.add(loadPaxConn.getOffPoint());
			 
			 if (oldData != null &&  oldData instanceof EntDbLoadPaxConnX){
				 EntDbLoadPaxConnX oloadPaxConn = (EntDbLoadPaxConnX) oldData;
//				 odat.add(oloadPaxConn.getUuid());
				 odat.add(oloadPaxConn.getInterfaceFltid());
				 odat.add(oloadPaxConn.getIntRefNumber());
				 odat.add(oloadPaxConn.getIdFlight());
				 odat.add(oloadPaxConn.getIdLoadPax());
				 odat.add(oloadPaxConn.getBoardPoint());
				 odat.add(oloadPaxConn.getBookedClass());
				 odat.add(oloadPaxConn.getConnStatus());
				 odat.add(oloadPaxConn.getConnType());
				 odat.add(oloadPaxConn.getIntId());
				 odat.add(oloadPaxConn.getInterfaceConxFltid());
				 odat.add(oloadPaxConn.getPaxConxFlno());
				 odat.add(oloadPaxConn.getConxFltDate());
				 odat.add(oloadPaxConn.getIdConxFlight());
				 odat.add(oloadPaxConn.getIntSystem());
				 odat.add(oloadPaxConn.getOffPoint());
			 }


		}else if (data instanceof EntDbServiceRequestX){
			EntDbServiceRequestX serviceRequest = (EntDbServiceRequestX) data;
//			fld.add(Integer.toString(serviceRequest.getIdFlight()));
			tabName = "SERVICE_REQUEST";
			selectStr = "WHERE ID = '"+serviceRequest.getUuid()+"'";
			idFlightStr = Integer.toString(serviceRequest.getIdFlight());
			ld.add(serviceRequest.getUuid());
			// add fields
//			fld.add("ID");
			 fld.add("INTFLID");
			 fld.add("INTREFNUMBER");
			 fld.add("ID_FLIGHT");
			 fld.add("DATA_SOURCE");
			 fld.add("INTID");
			 fld.add("REQUESTTYPE");
			 fld.add("SERVICECODE");
			 fld.add("SERVICETYPE");
			 fld.add("EXTINFO");
			 fld.add("ID_LOAD_PAX");
			 // populate fields
//			dat.add(serviceRequest.getUuid());
			 dat.add(serviceRequest.getIntFlId());
			 dat.add(serviceRequest.getIntRefNumber());
			 dat.add(serviceRequest.getIdFlight());
			 dat.add(serviceRequest.getIntSystem());
			 dat.add(serviceRequest.getIntId());
			 dat.add(serviceRequest.getRequestType());
			 dat.add(serviceRequest.getServiceCode());
			 dat.add(serviceRequest.getServiceType());
			 dat.add(serviceRequest.getExtInfo());
			 dat.add(serviceRequest.getIdLoadPax());
			 
			 if (odat != null && odat instanceof EntDbServiceRequestX){
				 EntDbServiceRequestX oserviceRequest = (EntDbServiceRequestX) oldData;
//				dat.add(oserviceRequest.getUuid());
				 odat.add(oserviceRequest.getIntFlId());
				 odat.add(oserviceRequest.getIntRefNumber());
				 odat.add(oserviceRequest.getIdFlight());
				 odat.add(oserviceRequest.getIntSystem());
				 odat.add(oserviceRequest.getIntId());
				 odat.add(oserviceRequest.getRequestType());
				 odat.add(oserviceRequest.getServiceCode());
				 odat.add(oserviceRequest.getServiceType());
				 odat.add(oserviceRequest.getExtInfo());
				 odat.add(oserviceRequest.getIdLoadPax());
			 }
		}else if (data instanceof EntDbLoadBag){
			EntDbLoadBag loadBag = (EntDbLoadBag) data;
//			fld.add(Long.toString(loadBag.getIdFlight()));
			tabName = "LOAD_BAG";
			selectStr = "WHERE ID = '"+loadBag.getId()+"'";
			idFlightStr = Long.toString(loadBag.getIdFlight());
			ld.add(loadBag.getId());
			// add fields
//			fld.add("ID");
			 fld.add("ARR_FLIGHT_NUMBER");
			 fld.add("ARR_FLT_DATE");
			 fld.add("ARR_FLT_DEST3");
			 fld.add("ARR_FLT_ORIGIN3");
			 fld.add("BAG_CLASS");
			 fld.add("BAG_CLASSIFY");
			 fld.add("BAG_TAG");
			 fld.add("BAG_TYPE");
			 fld.add("CHANGE_DATE");
			 fld.add("DATA_SOURCE");
			 fld.add("DEP_FLIGHT_NUMBER");
			 fld.add("DEP_FLT_DATE");
			 fld.add("DEP_FLT_DEST3");
			 fld.add("DEP_FLT_ORIGIN3");
			 fld.add("ID_ARR_FLIGHT");
			 fld.add("ID_DEP_FLIGHT");
			 fld.add("ID_LOAD_PAX");
			 fld.add("MSG_SEND_DATE");
			 fld.add("PAX_NAME");
			 fld.add("PAX_REF_NUM");
			 fld.add("REC_STATUS");
			 fld.add("BAG_TAG_STR");
			 fld.add("BAGWEIGHT");
			 // added 2014-01-06
			 fld.add("ID_FLIGHT");
			 fld.add("ID_CONX_FLIGHT");
			// populate data
//			 dat.add(loadBag.getId());
			 dat.add(loadBag.getArrFlightNumber());
			 dat.add(loadBag.getArrFltDate());
			 dat.add(loadBag.getArrFltDest3());
			 dat.add(loadBag.getArrFltOrigin3());
			 dat.add(loadBag.getBagClass());
			 dat.add(loadBag.getBagClassify());
			 dat.add(loadBag.getBagTag());
			 dat.add(loadBag.getBagType());
			 dat.add(loadBag.getChangeDate());
			 dat.add(loadBag.getDataSource());
			 dat.add(loadBag.getFlightNumber());
			 dat.add(loadBag.getFltDate());
			 dat.add(loadBag.getFltDest3());
			 dat.add(loadBag.getFltOrigin3());
			 dat.add(loadBag.getIdArrFlight());
			 dat.add(loadBag.getIdFlight());
			 dat.add(loadBag.getIdLoadPax());
			 dat.add(loadBag.getMsgSendDate());
			 dat.add(loadBag.getPaxName());
			 dat.add(loadBag.getPaxRefNum());
			 dat.add(loadBag.getRecStatus());
			 dat.add(loadBag.getBagTagStr());
			 dat.add(loadBag.getBagWeight());
			 // added 2014-01-06
			 dat.add(loadBag.getId_Flight());
			 dat.add(loadBag.getIdConxFlight());
			 if (oldData != null && oldData instanceof EntDbLoadBag){
				 EntDbLoadBag oloadBag = (EntDbLoadBag) oldData;
//				 odat.add(oloadBag.getId());
				 odat.add(oloadBag.getArrFlightNumber());
				 odat.add(oloadBag.getArrFltDate());
				 odat.add(oloadBag.getArrFltDest3());
				 odat.add(oloadBag.getArrFltOrigin3());
				 odat.add(oloadBag.getBagClass());
				 odat.add(oloadBag.getBagClassify());
				 odat.add(oloadBag.getBagTag());
				 odat.add(oloadBag.getBagType());
				 odat.add(oloadBag.getChangeDate());
				 odat.add(oloadBag.getDataSource());
				 odat.add(oloadBag.getFlightNumber());
				 odat.add(oloadBag.getFltDate());
				 odat.add(oloadBag.getFltDest3());
				 odat.add(oloadBag.getFltOrigin3());
				 odat.add(oloadBag.getIdArrFlight());
				 odat.add(oloadBag.getIdFlight());
				 odat.add(oloadBag.getIdLoadPax());
				 odat.add(oloadBag.getMsgSendDate());
				 odat.add(oloadBag.getPaxName());
				 odat.add(oloadBag.getPaxRefNum());
				 odat.add(oloadBag.getRecStatus());
				 odat.add(oloadBag.getBagTagStr());
				 odat.add(oloadBag.getBagWeight());
				 // added 2014-01-06
				 odat.add(oloadBag.getId_Flight());
				 odat.add(oloadBag.getIdConxFlight());
			 }
			
		}
		
		header.getIdFlight().add(idFlightStr);
		header.setReqt(HpUfisCalendar.getCurrentUTCTimeString());
		if (isLast) {
			header.setBcnum(-1);
		} else {
			header.setBcnum(1);
		}
		
		EntUfisMsgACT act = new EntUfisMsgACT();
		act.setCmd(cmd.toString());
		act.setTab(tabName);
		act.setFld(fld);
		act.setData(dat);
		act.setOdat(odat);
		act.setSel(selectStr);
		act.setId(ld);
		
		actions.add(act);
		
		notificationMsg = HpUfisJsonMsgFormatter.formDataInJson(actions, header);
		}catch(Exception e){
			LOG.error("erro happen when transformForPax, {}",e);
		}
		
//		LOG.debug(notificationMsg);
		
		return notificationMsg;
		
	}
	
}
