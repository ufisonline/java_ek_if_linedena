package com.ufis_as.ufisapp.ek.intf;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.ekrtc.BlHandleCedaEKRTC;
import com.ufis_as.ufisapp.ek.intf.ekrtc.BlHandleEKRTC;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;

import ek.ekrtc.resourceshift.ResourceShiftType;
import ek.ekrtc.rtcassignment.ResourceAssignmentType;


@Stateless
public class BIRouterEKRTC{
	private static final Logger LOG = LoggerFactory
			.getLogger(BIRouterEKRTC.class);

    @EJB
    private BlHandleCedaEKRTC blHandleCedaEKRTC;
    @EJB
    private BlHandleEKRTC bIHandleEKRTC;
    @EJB
    private EntStartupInitSingleton entStartupInitSingleton;

	private JAXBContext _cnxJaxb1;
	private Unmarshaller _um1;
	private JAXBContext _cnxJaxb2;
	private Unmarshaller _um2;


	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb1 = JAXBContext.newInstance(
                    ResourceAssignmentType.class
                    );
			_um1 = _cnxJaxb1.createUnmarshaller();
			
			_cnxJaxb2 = JAXBContext.newInstance(
                    ResourceShiftType.class
                    );
			_um2 = _cnxJaxb2.createUnmarshaller();

		} catch (JAXBException ex) {
			LOG.error("JAXBException when creating Unmarshaller: {}", ex);
		}
		
		

	}

	public void routeMessage(TextMessage textMsg) {
		
		String message;
		try {
			message = textMsg.getText();
		} catch (JMSException e) {
			LOG.error("ERRO when receving msg, {}",e);
			return;
		}
		
		bIHandleEKRTC.setRawMsg(textMsg);
		bIHandleEKRTC.setxmlMsg(message);
		
		if (message.contains("ResourceShift.xsd")){
			bIHandleEKRTC.init( _um2);
		}else if (message.contains("ResourceAssignment.xsd")){
			bIHandleEKRTC.init( _um1);
		}

		boolean isunMarshalSuccess;
		long startTime = System.currentTimeMillis();
		isunMarshalSuccess = bIHandleEKRTC.unMarshal();
		LOG.info("parsing the msg, takes {} ms", System.currentTimeMillis() - startTime);
		
		if (!isunMarshalSuccess){
			LOG.warn("unmarshall not successful, pls check");
			return;
		}
		
		if (message.contains("ResourceShift.xsd")){
			
			startTime = System.currentTimeMillis();
			if (bIHandleEKRTC.getEntDbStaffShift() != null) {
				blHandleCedaEKRTC.handleEKRTC(bIHandleEKRTC,
						bIHandleEKRTC.getEntDbStaffShift());
			} else {
				LOG.warn("bIHandleEKRTC.getEntDbStaffShift() is null for ResourceShift msg, pls check");
			}
			LOG.info("handle the staffShift msg, takes {} ms", System.currentTimeMillis()
					- startTime);
	
		}else if (message.contains("ResourceAssignment.xsd")){
			
			startTime = System.currentTimeMillis();
			if (bIHandleEKRTC.getEntDbFltJobTask() != null
					&& bIHandleEKRTC.getEntDbFltJobAssign() != null) {
				blHandleCedaEKRTC.handleEKRTC(bIHandleEKRTC,
						bIHandleEKRTC.getEntDbFltJobTask(),
						bIHandleEKRTC.getEntDbFltJobAssign());
			} else {
				LOG.warn("bIHandleEKRTC.getEntDbFltJobTask() or bIHandleEKRTC.getEntDbFltJobAssign() is null for ResourceAssignment msg, pls check");
			}
			LOG.info("handle the ResourceAssignment msg, takes {} ms", System.currentTimeMillis()
					- startTime);
			
		}else {
			LOG.warn("unknow msg, not containing 'ResourceShift.xsd' or 'ResourceAssignment.xsd' : {}", message);
		}

		LOG.info(" out time {}", new Date().getTime());
	}
}

