package com.ufis_as.ufisapp.ek.intf;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.intf.bms.BlHandleCrewCheckInBean;
import com.ufis_as.ufisapp.ek.intf.iams.BlHandleCrewImmigrationBean;
import com.ufis_as.ufisapp.ek.intf.belt.BlHandleBagScanUpdateBean;
import com.ufis_as.ufisapp.ek.intf.lido.BlHandleLidoBean;
import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleLineDeNA;
import com.ufis_as.ufisapp.ek.intf.macs.BlHandleCedaMacsPaxX;
import com.ufis_as.ufisapp.ek.intf.paxAlert.BlConnectionSummaryBean;
import com.ufis_as.ufisapp.ek.intf.paxAlert.BlConxHandler;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;

@Stateless
public class BlAMQMessageRouter {

	@EJB
	private BlHandleLidoBean clsBlHandleLidoBean;
	@EJB
	private BlHandleCrewImmigrationBean clsBlCrewImmigration;
	@EJB
	private BlHandleCrewCheckInBean clsBlCrewCheckIn;
	@EJB
	private BlConnectionSummaryBean conxSummaryBean;
	@EJB
	private ConnFactorySingleton clsConFactory;
	@EJB
	private BlHandleCedaMacsPaxX blHandleCedaMacsPax;
	@EJB
	private BlHandleLineDeNA blHandleLineDeNA;
	@EJB
	private BlConxHandler blConxHandler;
	@EJB
	private BlHandleBagScanUpdateBean blHandleBagUpdate;
	
	
	public static final Logger LOG = LoggerFactory
			.getLogger(BlAMQMessageRouter.class);

	public BlAMQMessageRouter() {
	}

	/**
	 * - check if the msg is for resetting (RST) TIBCO or AMQ connection -
	 * identify the data flow name and route to each business logic class.
	 * 
	 * @param inputMsgDTO
	 * @param dtflStr
	 */
	public void routeUfisAMQMessage(EntUfisMsgDTO ufisMsgDTO, String dtflStr) {
		EntUfisMsgACT msgBodyAct = ufisMsgDTO.getBody().getActs().get(0);
		if (UfisASCommands.RST.toString().equalsIgnoreCase(msgBodyAct.getCmd())) {
			routeToResetConxHanlder(msgBodyAct);
		} else if (ufisMsgDTO != null
				&& ufisMsgDTO.getBody().getActs() != null
				&& ufisMsgDTO.getBody().getActs().get(0) != null
				&& ufisMsgDTO.getBody().getActs().get(0).getTab() != null
				&& ufisMsgDTO.getBody().getActs().get(0).getTab()
						.equalsIgnoreCase("LOAD_BAG")) {
			blHandleBagUpdate.handleBagUpdate(ufisMsgDTO);
		}else {
			routeToBlHandler(ufisMsgDTO, dtflStr);
		}
	}

	private void routeToResetConxHanlder(EntUfisMsgACT msgBodyAct) {
		for (Object data : msgBodyAct.getData()) {
			switch ((String) data) {
			case "TIBCO":
				clsConFactory.reconnectTibco();
				break;
			case "AMQ":
				break;
			default:
				LOG.info("!!RST msg without DATA value. No process performed.");
				break;
			}
		}
	}

	private void routeToBlHandler(EntUfisMsgDTO ufisMsgDTO, String dtflStr) {
		switch (dtflStr) {
		case HpEKConstants.LIDO_SOURCE:
			clsBlHandleLidoBean.updateDeptFlightPlan(ufisMsgDTO);
			break;
		case HpEKConstants.IAMS_SOURCE:
			clsBlCrewImmigration.updateFltJobAssign(ufisMsgDTO);
			break;
		case HpEKConstants.BMS_SOURCE:
			clsBlCrewCheckIn.updateFltJobAssign(ufisMsgDTO);
			break;
		//case HpEKConstants.PAX_ALERT_SOURCE:
		case HpUfisAppConstants.FLT_CONX_SUMMARY:
//			conxSummaryBean.processConxMsg(ufisMsgDTO);
			blConxHandler.processConxMsg(ufisMsgDTO);
			break;
		case HpEKConstants.ULD_SOURCE:
			break;
		case HpEKConstants.MACS_PAX_DATA_SOURCE:
			long startTime = System.currentTimeMillis();
			blHandleCedaMacsPax.handleFltIdMappingNotification(ufisMsgDTO);
			LOG.debug("To handle fltIdMapping message from AMQ, takes {}",
					System.currentTimeMillis() - startTime);
			break;
		case HpEKConstants.DeNA_DATA_SOURCE:
			startTime = System.currentTimeMillis();
			blHandleLineDeNA.handleLineDeNAFromActiveMQ(ufisMsgDTO, "IF_LINEDENA");
			LOG.debug("To handle msg for LineDeNA from AMQ, takes {}",
					System.currentTimeMillis() - startTime);
			break;
		default:
			LOG.info("UNKNOWN message received from AMQ.");
			break;
		}
	}
}
