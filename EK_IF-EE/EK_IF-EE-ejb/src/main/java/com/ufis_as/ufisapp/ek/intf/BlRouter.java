/*
 * $Id: BlRouter.java 9161 2013-09-26 09:23:59Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.intf;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.BlHandleMACS;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.InterfaceMqConfig;
import com.ufis_as.exco.MSG;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.intf.aacs.BlHandleUldBean;
import com.ufis_as.ufisapp.ek.intf.aacs.BlHandleUldMoveBean;
import com.ufis_as.ufisapp.ek.intf.bms.BlHandleCrewCheckInBean;
import com.ufis_as.ufisapp.ek.intf.core.BlHandleCoreBean;
import com.ufis_as.ufisapp.ek.intf.dmis.BlHandleDmisBean;
import com.ufis_as.ufisapp.ek.intf.egds.BlHandleEgdsBean;
import com.ufis_as.ufisapp.ek.intf.iams.BlHandleCrewImmigrationBean;
import com.ufis_as.ufisapp.ek.intf.lido.BlHandleLidoBean;
import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleDeNa;
import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleLineDeNA;
import com.ufis_as.ufisapp.ek.intf.macs.BlHandleMacsFltBean;
import com.ufis_as.ufisapp.ek.intf.opera.BlHandlePaxLoungeBean;
import com.ufis_as.ufisapp.ek.intf.rms_rtc.BlRmsRtcRouterBean;
import com.ufis_as.ufisapp.ek.intf.skychain.BlHandleSkyChainULDBean;
import com.ufis_as.ufisapp.ek.intf.skychain.BlHandleSkyChainUWSBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.LineDeNA.OpsFlashMsg;
import ek.bms.CrewCheckIn;
import ek.core.flightevent.FlightEvent;
import ek.lido.LidoMsgInfo;
import ek.macs.flightdetails.FlightDetails;
import ek.macs.paxdetails.PaxDetails;
import ek.macs.paxfctdetails.FctDetails;
import ek.macs.paxinbdetails.InbDetails;
import ek.macs.paxonwdetails.OnwDetails;


/**
 * @author $Author: sch $
 * @VERSION $Revision: 9161 $
 */
@Stateless
public class BlRouter {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(BlRouter.class);

    private static GregorianCalendar xmlLastUpdate;
    private static AtomicInteger seqD = new AtomicInteger(0);
    private static AtomicInteger seqF = new AtomicInteger(0);

    private AtomicInteger _messagesCount = new AtomicInteger(0);
    private AtomicInteger _errorsCount = new AtomicInteger(0);
    
    
    /* TODO
     *  move the following to the Singleton class
     */
    //private List<FlightEvent> globalFlightEvent = new ArrayList<>();
    private List<EntDbLoadPaxConn> edpiods = new ArrayList<>();

    private JAXBContext _cnxJaxb;
    private JAXBContext _cnxJaxbM;
    private Unmarshaller _um;
    private Marshaller _ms;

    /* Classes for Handling the messages */
    //private BlHandleCore handleCore;
    private BlHandleMACS handleMACS;
    private BlHandleDeNa handleDeNA;
    //private BlHandleDMIS handleDMIS;
    //private BlHandleEdgs handleEGDS;

    private List<InterfaceConfig> interfaceConfigs = new ArrayList<>();
    private static XMLConfiguration config;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
//    @EJB
//    private BlHandleCedaMacs _blHandleCedaMacs;
    @EJB
    private BlHandleMacsFltBean _blHandleMacsFlt;
    @EJB
    private BlUfisQueue _bridgeMessage;
    @EJB
    private BlUfisExceptionQueue _ufisExptMessage;
    @EJB
    private IAfttabBeanLocal afttabBean;
//    @EJB
//    private ICoreHandler _coreHandler;
    @EJB
    private BlHandleCoreBean _coreHandler;
    @EJB
    private BlHandleDmisBean _dmisHandler;
    @EJB
    private BlHandleEgdsBean _egdsHandler;
    @EJB
    private BlHandlePaxLoungeBean clsBlPaxLoungeHanlder;
    @EJB
    private BlHandleCrewImmigrationBean clsBlCrewImmigrationHandler;
    @EJB
    private BlHandleCrewCheckInBean clsBlCrewCheckInBean;
    @EJB
    private BlHandleLineDeNA bIHandleLineDeNA;
    @EJB
    BlRmsRtcRouterBean clsBlHandleRmsRtcBean;
    @EJB
	private BlIrmtabFacade _irmtabFacade;
    @EJB
	private BlHandleUldBean clsHandleuldBean;
    @EJB
	private BlHandleUldMoveBean clsHandleUldMoveBean;
	@EJB
    private BlHandleLidoBean clsBlHandleLidoBean;
	@EJB
	private BlHandleSkyChainULDBean blSkyChainUld;
	@EJB
	private BlHandleSkyChainUWSBean blSkyChainUWS;
    
    @PostConstruct
    private void initialize(){
        try {
            config = new XMLConfiguration(HpUfisAppConstants.DEFAULT_CONFIGNAME);
            LOG.info("Loading configuration file {}", config.getBasePath());
        } catch (ConfigurationException cex) {
            LOG.error("Error Loading configuration file {}", cex.getMessage());
            LOG.error("Application is now Stopped");
        }
        try {
            _cnxJaxb = JAXBContext.newInstance(
            		FlightDetails.class,
                    PaxDetails.class,
                    InbDetails.class,
                    OnwDetails.class,
                    FctDetails.class,
                    CrewCheckIn.class,
                    LidoMsgInfo.class,
                    OpsFlashMsg.class);
            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
            _um = _cnxJaxb.createUnmarshaller();
            _ms = _cnxJaxbM.createMarshaller();
        } catch (JAXBException ex) {
            LOG.error("JAXBException when creating Unmarshaller");
        }
        //Get The Configuration
        //getInterfacesConfig();
    }

    public void routerCommand(String commandName, String queueName) {
        LOG.info("RouterCommand commandName {} , queueName {}", commandName, queueName);
    }
    
    @Deprecated
    public boolean routeMessage(String message, InterfaceConfig interfaceConfig) {
    	Boolean isTransferToAmq = false;
        String returnXML = "";
        if (interfaceConfig == null && !interfaceConfigs.isEmpty()) {
            interfaceConfig = interfaceConfigs.get(0);
        }
        
        /** Base on DTFL, route to Biz processes **/
        String dtfl = _startupInitSingleton.getDtflStr();
        if(dtfl != "UNKNOWN"){
        	switch(dtfl){
        	case "BMS" : //crew check in 
        		clsBlCrewCheckInBean.processCrewCheckIn(message, 0); break;
        	case "IAMS" : //crew immigration 
        		clsBlCrewImmigrationHandler.processCrewImmigration(message, 0); break;
        	case "OPERA" :	//pax lounge usage 
    			clsBlPaxLoungeHanlder.processPaxLoungeUsage(message, 0); break;
        	case "RMS-RTC" : //resource assignment and tracking
        		clsBlHandleRmsRtcBean.processRtcSourceRouter(message); break;
        	}
        	return true;
        }
        else{
        	LOG.debug("!!!! Dataflow DTFL <{}>.", dtfl);
        }
        
        if (message.contains("FlightEvent.xsd")) {
			/* CORE */
//		    FlightEvent flightEvent = _coreHandler.unMarshal(message);
//		    if (flightEvent != null) {
//		    	isTransferToAmq = _coreHandler.tranformFlight(flightEvent);
//		     	if (isTransferToAmq) {
//		     		returnXML = _coreHandler.getReturnXml();
//		     	}
//		    }
		} 
		else if (message.contains("FlightDetails")
				|| message.contains("OnwDetails")
				|| message.contains("InbDetails")
				|| message.contains("PaxDetails")
				|| message.contains("FctDetails")) {
			/* MACS */
			if (handleMACS == null) {
				handleMACS = new BlHandleMACS(_ms, _um, interfaceConfig);
			}
			//handleMACS.setEdpiods(edpiods);
			handleMACS.setFlightXml(message);
			//isTransferToAmq = handleMACS.unMarshal();
			handleMACS.unMarshal();
			returnXML = handleMACS.getReturnXML();
			if (message.contains("FlightDetails")) {
				if (handleMACS.getFlightData() != null 
						&& HpUfisUtils.isNotEmptyStr(handleMACS.getFlightData().getMflId())) {
					//_blHandleCedaMacs.handleMACSFLT(handleMACS.getFlightData(), message);
				} else {
					LOG.debug("MACS-FLT: flight details and mfid cannot be null or empty");
					LOG.debug("Message dropped: {}", message);
				}
			}
			if (message.contains("OnwDetails")
					|| message.contains("InbDetails")) {
//				if (handleMACS.getEntDbPaxInbOwnDetails() != null){
//				_blHandleCedaMacs.handleMACSPAX(handleMACS
//						.getEntDbPaxInbOwnDetails());
//				}
			}
			if (message.contains("PaxDetails")) {
//				if (handleMACS.get_entDbPax() != null){
//				_blHandleCedaMacs.handleMACSPAX(handleMACS.get_entDbPax());
//				}
			}
			if (message.contains("FctDetails")) {
//				if (handleMACS.get_entDbPaxServiceRequest() != null){
//				_blHandleCedaMacs.handleMACSPAX(handleMACS
//						.get_entDbPaxServiceRequest());
//				}
			}

		} else if (message.contains("BothFlightEvent")
        		|| message.contains("root")
                || message.contains("TowDetails")) {
            /* DMIS */
        	/*if (handleDMIS == null) {
                handleDMIS = new BlHandleDMIS(_ms, _um, interfaceConfig);
            }
            handleDMIS.setFlightXml(message);
            isTransferToAmq = handleDMIS.unMarshal();
            returnXML = handleDMIS.getReturnXML();*/
            isTransferToAmq = _dmisHandler.unMarshal(message, 0);
            returnXML = _dmisHandler.getReturnXML();
            
        } else if (message.contains("EGDS_MSG")){
        	/* EGDS */
//        	isTransferToAmq = _egdsHandler.unMarshal(message);
        	returnXML = _egdsHandler.getReturnXML();
        	
		}
//        else if (message.contains("OpsFlashMsg")) {
//        	/* OPFlashMsg */
//			if (handleDeNA == null){
//				handleDeNA = new BIHandleDeNa(_ms, _um, interfaceConfig);
//			}
////			handleDeNA.setEdpiods(edpiods);
//			handleDeNA.setFlightXml(message);
////			handleDeNA.setEntDbAircraftOpsMsg(entDbAircraftOpsMsg);
//			boolean isUnMarshalSucess = handleDeNA.unMarshal();
//			
//			if (isUnMarshalSucess){
//				EntDbAfttab deNAEntDbAfttab = null;
//				if (handleDeNA.getEntDbAircraftOpsMsg() != null){
//					String getFlightNumber =null;
//					String flDate =null;
//					if (handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber() != null){
//						getFlightNumber = handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber().substring(0,2)+" "+handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber().substring(2);
//					}
//					if (handleDeNA.getEntDbAircraftOpsMsg().getFltDate() != null){
//						flDate = HpEKConstants.DENA_DATE_FORMAT.format(handleDeNA.getEntDbAircraftOpsMsg().getFltDate());
//					}
//					
//					deNAEntDbAfttab =afttabBean.findFlightForLineDeNA(flDate, handleDeNA.getEntDbAircraftOpsMsg().getArrDepFlag(), getFlightNumber, handleDeNA.getEntDbAircraftOpsMsg().getFltRegn());
//				}
//				
//				EntDbAircraftOpsMsg entDbAircraftOpsMsg = handleDeNA.getEntDbAircraftOpsMsg();
//				
////				deNAEntDbAfttab = new EntDbAfttab();
//				if (deNAEntDbAfttab != null){		
//					// set ID_flight		
//					entDbAircraftOpsMsg.setIdFlight(deNAEntDbAfttab.getUrno());
//					try {
//						entDbAircraftOpsMsg.setAldt(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAldt()));
//						entDbAircraftOpsMsg.setAtot(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAtot()));
//					} catch (ParseException e) {
//						LOG.debug("String to Date parse erro {}", e);
//					}
//					bIHandleLineDeNA.handleLineDeNA(entDbAircraftOpsMsg, handleDeNA.getSubtype());
//				}else{
//					entDbAircraftOpsMsg.setIdFlight(new BigDecimal(0));
//					LOG.debug("flight not found , IdFlight = 0");
//					bIHandleLineDeNA.handleLineDeNA(entDbAircraftOpsMsg, handleDeNA.getSubtype());
////					LOG.debug("flight not found , drop message and insert into IRMTAB");
////					_irmtabFacade.updateIRMStatus("Invalid");
//				}			
//				
//			}else{
//				LOG.debug("not match with xsd , drop message and insert into IRMTAB");
//	        	// is not match with xsd, then mark as STAT "invalid"
//	        	_irmtabFacade.updateIRMStatus("Invalid");
//			}
//		} 
        
        if (isTransferToAmq) {
            _messagesCount.getAndIncrement();
            // if (interfaceConfig.getToMq().getEnabled().equalsIgnoreCase("TRUE") && !returnXML.isEmpty()) {
            _bridgeMessage.sendMessage(returnXML);
            //     }
        } else {
            //_errorsCount.getAndIncrement();
        }
        return isTransferToAmq;
        //LOG.info("xml prossesed {}" , watch.time(TimeUnit.MILLISECONDS));
    }
    
    public void routeMessageWithDTFL(TextMessage msg, String dtfl, long irmtabRef) throws JMSException {
        
    	String message = msg.getText();
    	String errorMsg = "";
    	boolean isSuccess = false;
    	boolean isExptNofity = false; 
    	
    	if(_startupInitSingleton.getIrmLogLev().equalsIgnoreCase(
				HpUfisAppConstants.IrmtabLogLev.LOG_ERR.name())){ //LOG_ERR?
			irmtabRef = _irmtabFacade.storeRawMsg(msg, dtfl);//save to irmtab
		}    	
    	
		if (!"UNKNOWN".equalsIgnoreCase(dtfl)) {
			switch (dtfl) {
			case "CORE-ODS":
				/* CORE */
			    FlightEvent flightEvent = _coreHandler.unMarshal(message, irmtabRef);
			    if (flightEvent != null) {
			    	isSuccess = _coreHandler.tranformFlight(flightEvent);
			     	if (isSuccess) {
			     		_bridgeMessage.sendMessage(_coreHandler.getReturnXml());
			     	}
			    } else {
			    	_coreHandler.addExptInfo(EnumExceptionCodes.EXSDF.name(),
							EnumExceptionCodes.EXSDF.toString());
			    }
			    
			    if (!_coreHandler.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
							_coreHandler.getData(), 
							UfisASCommands.IRT.toString(),
							irmtabRef, 
							true,
							dtfl);
					isExptNofity = true;
				}
				break;
				
			case "DMIS":
			case "DMIS-TOW":
				/* DMIS */
				isSuccess = _dmisHandler.unMarshal(message, irmtabRef);
//	            returnXML = _dmisHandler.getReturnXML();
				if (!_dmisHandler.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
							_dmisHandler.getData(), 
							UfisASCommands.IRT.toString(),
							irmtabRef, 
							true,
							dtfl);
					isExptNofity = true;
				}
				if (isSuccess) {
		     		_bridgeMessage.sendMessage(_dmisHandler.getReturnXML());
		     	}
				break;
				
			case "EGDS":
				/* EGDS */
				isSuccess = _egdsHandler.unMarshal(message, irmtabRef);
//	        	returnXML = _egdsHandler.getReturnXML();
				if (isSuccess) {
		     		_bridgeMessage.sendMessage(_egdsHandler.getReturnXML());
		     	}
				
				if (!_egdsHandler.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
							_egdsHandler.getData(), 
							UfisASCommands.IRT.toString(),
							irmtabRef, 
							true,
							dtfl);
					isExptNofity = true;
				}
				break;
			case "MACS-FLT":
				/* MACS */
				if (handleMACS == null) {
					handleMACS = new BlHandleMACS(_ms, _um, null);
				}
				handleMACS.setFlightXml(message);
				handleMACS.unMarshal();
				if (handleMACS.getFlightData() != null) {
					_blHandleMacsFlt.handleMACSFLT(handleMACS.getFlightData(), message, irmtabRef);
				} else {
					LOG.debug("MACS-FLT: flight details cannot be null");
					LOG.debug("Message dropped: {}", message);
					_blHandleMacsFlt.addExptInfo(EnumExceptionCodes.EXSDF.name(),
							EnumExceptionCodes.EXSDF.toString());
				}
				
				if (!_blHandleMacsFlt.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
							_blHandleMacsFlt.getData(), 
							UfisASCommands.IRT.toString(),
							irmtabRef, 
							true,
							dtfl);
					isExptNofity = true;
				}
				break;
			case "SkyChain" :
				if(message.contains("ULDInfo")) {
					LOG.info("SkyChain ULD info starts being processed...");
					isSuccess = blSkyChainUld.processULD(message, irmtabRef);
					if(!blSkyChainUld.getData().isEmpty()) {
						errorMsg = HpUfisMsgFormatter.formExceptionData(blSkyChainUld.getData(),
								UfisASCommands.IRT.name(),
								blSkyChainUld.getIrmtabUrno(),
								true,
								dtfl);
						isExptNofity = true;
					}
				} else if (message.contains("UWS")) {
					LOG.info("SkyChain UWS info starts being processed...");
					isSuccess = blSkyChainUWS.processUWS(message, irmtabRef);
					if(!blSkyChainUWS.getData().isEmpty()) {
						errorMsg = HpUfisMsgFormatter.formExceptionData(blSkyChainUWS.getData(),
								UfisASCommands.IRT.name(),
								blSkyChainUWS.getIrmtabUrno(),
								true,
								dtfl);
						isExptNofity = true;
					}
				} else {
					LOG.warn("Skychain input ULD/UWS info message type is invalid. Message will be dropped.");
				}
				
				break;
			case "AACS-ULD" :
				// AACS(Uld info and scanning)
					/*		logLevel = _BasicDataSingleton.getIrmLogLev();
							this.irmtabRef = irmtabRef;
							txtMsg = msg;
							isSend = false;
							msgWarnList = new ArrayList<>();*/
				String inputMsg = msg.getText();
				if (inputMsg.contains("ULD_INFO")) {
					isSuccess = clsHandleuldBean.processULD(inputMsg, irmtabRef);
					if (!clsHandleuldBean.getData().isEmpty()) {
						errorMsg = HpUfisMsgFormatter.formExceptionData(
							clsHandleuldBean.getData(), UfisASCommands.IRT.toString(),
							clsHandleuldBean.getIrmtabUrno(), true, dtfl);
						isExptNofity = true;
					}
				} else if (inputMsg.contains("ULD_SCANNING_DETAILS")) {
					isSuccess = clsHandleUldMoveBean.processULDScanning(inputMsg, irmtabRef);
					if (!clsHandleUldMoveBean.getData().isEmpty()) {
						errorMsg = HpUfisMsgFormatter.formExceptionData(
							clsHandleUldMoveBean.getData(), UfisASCommands.IRT.toString(),
							clsHandleUldMoveBean.getIrmtabUrno(), true, dtfl);
						isExptNofity = true;
					}
				} else
					LOG.warn("Input uld info message is incorrect. Message dropped. ");
				
				break;
			case "BMS":
				// BMS(crew Check In)
				isSuccess = clsBlCrewCheckInBean.processCrewCheckIn(message, irmtabRef);
				if (!clsBlCrewCheckInBean.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
						clsBlCrewCheckInBean.getData(), UfisASCommands.IRT.toString(),
						clsBlCrewCheckInBean.getIrmtabUrno(), true, dtfl);
					isExptNofity = true;
				}
				break;
			case "IAMS":
				// IAMS(crew immigration)
				isSuccess = clsBlCrewImmigrationHandler.processCrewImmigration(message, irmtabRef);
				if (!clsBlCrewImmigrationHandler.getData().isEmpty()) {
					errorMsg = HpUfisMsgFormatter.formExceptionData(
						clsBlCrewImmigrationHandler.getData(), UfisASCommands.IRT.toString(),
						clsBlCrewImmigrationHandler.getIrmtabUrno(), true, dtfl);
					isExptNofity = true;
				}
				break;
			case "OPERA":
				// OPERA(pax lounge usage)
				isSuccess = clsBlPaxLoungeHanlder.processPaxLoungeUsage(message, irmtabRef);
				if(!clsBlPaxLoungeHanlder.getData().isEmpty()){
					errorMsg = HpUfisMsgFormatter.formExceptionData(
						clsBlPaxLoungeHanlder.getData(), UfisASCommands.IRT.toString(),
						clsBlPaxLoungeHanlder.getIrmtabUrno(), true, dtfl);
					isExptNofity = true;
				}
				break;
			case "LIDO":
				clsBlHandleLidoBean.processLidoRouter(message, irmtabRef);
				if(!clsBlHandleLidoBean.getData().isEmpty()){
					errorMsg = HpUfisMsgFormatter.formExceptionData(
						clsBlHandleLidoBean.getData(), 
						UfisASCommands.IRT.toString(),
						irmtabRef, 
						true, 
						dtfl);
					isExptNofity = true;
				}
				break;
			case "RMS-RTC": // resource assignment and tracking
				clsBlHandleRmsRtcBean.processRtcSourceRouter(message);
				break;
			case "EK-RTC":
				if (message.contains("ResourceShift.xsd")) {

				} else if (message.contains("ResourceAssignment.xsd")) {

				}
				break;
//			case HpEKConstants.DeNA_DATA_SOURCE:
//				if (message.contains("OpsFlashMsg")) {
//		        	/* OPFlashMsg */
//					if (handleDeNA == null){
//						handleDeNA = new BIHandleDeNa(_ms, _um, null);
//					}
////					handleDeNA.setEdpiods(edpiods);
//					handleDeNA.setFlightXml(message);
////					handleDeNA.setEntDbAircraftOpsMsg(entDbAircraftOpsMsg);
//					long startTime = System.currentTimeMillis();
//					boolean isUnMarshalSucess = handleDeNA.unMarshal();
//					LOG.debug("2.1 unMarshal LineDeNA msg, takes {} ms", System.currentTimeMillis() - startTime);
//					
//					if (isUnMarshalSucess){
////						EntDbAfttab deNAEntDbAfttab = null;
//						Tuple deNAEntDbAfttab = null;
//						if (handleDeNA.getEntDbAircraftOpsMsg() != null){
//							String getFlightNumber =null;
//							String flDate =null;
//							if (handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber() != null){
//								getFlightNumber = handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber().substring(0,2)+" "+handleDeNA.getEntDbAircraftOpsMsg().getFlightNumber().substring(2);
//							}
//							if (handleDeNA.getEntDbAircraftOpsMsg().getFltDate() != null){
//								flDate = HpEKConstants.DENA_DATE_FORMAT.format(handleDeNA.getEntDbAircraftOpsMsg().getFltDate());
//							}
//							
//							startTime = System.currentTimeMillis();
//							deNAEntDbAfttab =afttabBean.findFlightForLineDeNAX(flDate, handleDeNA.getEntDbAircraftOpsMsg().getArrDepFlag(), getFlightNumber, handleDeNA.getEntDbAircraftOpsMsg().getFltRegn());
//							LOG.debug("2.2 search flight from afttab, takes {}", System.currentTimeMillis() - startTime);
//						}
//						
//						EntDbAircraftOpsMsg entDbAircraftOpsMsg = handleDeNA.getEntDbAircraftOpsMsg();
//						
////						deNAEntDbAfttab = new EntDbAfttab();
//						if (deNAEntDbAfttab != null){		
//							// set ID_flight		
//							entDbAircraftOpsMsg.setIdFlight(deNAEntDbAfttab.get("urno", BigDecimal.class));
////							entDbAircraftOpsMsg.setIdFlight(deNAEntDbAfttab.getUrno());
//							try {
//								entDbAircraftOpsMsg.setAldt(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.get("aldt", String.class)));
////								entDbAircraftOpsMsg.setAldt(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAldt()));
//								entDbAircraftOpsMsg.setAtot(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.get("atot", String.class)));
////								entDbAircraftOpsMsg.setAtot(HpEKConstants.sDENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAtot()));
//							} catch (ParseException e) {
//								LOG.debug("String to Date parse erro {}", e);
//							}
//							startTime = System.currentTimeMillis();
//							bIHandleLineDeNA.handleLineDeNA(entDbAircraftOpsMsg, handleDeNA.getSubtype());
//							LOG.debug("2.3 handleLineDeNA msg, db insert, update, takes {}", System.currentTimeMillis() - startTime);
//						}else{
//							entDbAircraftOpsMsg.setIdFlight(new BigDecimal(0));
//							LOG.debug("flight not found , IdFlight = 0");
//							startTime = System.currentTimeMillis();
//							bIHandleLineDeNA.handleLineDeNA(entDbAircraftOpsMsg, handleDeNA.getSubtype());
//							LOG.debug("2.3 handleLineDeNA msg, db insert, update, takes {}", System.currentTimeMillis() - startTime);
//						}			
//						
//					}else{
//						LOG.debug("not match with xsd , drop message and insert into IRMTAB");
//			        	// is not match with xsd, then mark as STAT "invalid"
//			        	_irmtabFacade.updateIRMStatus("Invalid");
//					}
//				} 
//				break;
			}
			if (isExptNofity && !_startupInitSingleton.getIrmLogLev().equalsIgnoreCase(
					HpUfisAppConstants.IrmtabLogLev.LOG_OFF.name())) {
				_ufisExptMessage.sendMessage(errorMsg);
			}
		} else {
			LOG.debug("!!!! Cannot route message due to Dataflow: {}", dtfl);
		}
    }
    
    @Deprecated
    private void getInterfacesConfig() {
        String isFilestr = config.getString("Global.isFile");
        String filePath = config.getString("Global.FilePath");

        if (isFilestr.equalsIgnoreCase("YES") || isFilestr.equalsIgnoreCase("TRUE")) {
            /* TODO
             * This is not availiable from EJB
             */
        }

        int infList = config.getList("Interfaces.Interface.name").size();
        if (infList > 0) {
            for (int i = 0; i < infList; i++) {
                InterfaceConfig interfaceConfig = new InterfaceConfig();
                InterfaceMqConfig fromMq = new InterfaceMqConfig();
                InterfaceMqConfig toMq = new InterfaceMqConfig();
                HierarchicalConfiguration fromMqConfig = config.configurationAt("Interfaces.Interface(" + i + ").FromMq");
                HierarchicalConfiguration toMqConfig = config.configurationAt("Interfaces.Interface(" + i + ").ToMq");
                String name = config.getString("Interfaces.Interface(" + i + ").name");
                String xsd = config.getString("Interfaces.Interface(" + i + ").xsd");
                String className = config.getString("Interfaces.Interface(" + i + ").classname");
                String regExp = config.getString("Interfaces.Interface(" + i + ").regexp");

                interfaceConfig.setName(name);
                interfaceConfig.setXsd(xsd);
                interfaceConfig.setClassName(className);
                interfaceConfig.setRegexp(regExp);

                fromMq.setUrl(fromMqConfig.getString("url"));
                fromMq.setUserName(fromMqConfig.getString("username"));
                fromMq.setPassword(fromMqConfig.getString("password"));
                fromMq.setQueueName(fromMqConfig.getString("queue"));
                fromMq.setEnabled(fromMqConfig.getString("enabled"));
                interfaceConfig.setFromMq(fromMq);

                toMq.setUrl(toMqConfig.getString("url"));
                toMq.setUserName(toMqConfig.getString("username"));
                toMq.setPassword(toMqConfig.getString("password"));
                toMq.setQueueName(toMqConfig.getString("queue"));
                toMq.setEnabled(toMqConfig.getString("enabled"));
                interfaceConfig.setToMq(toMq);
                interfaceConfigs.add(interfaceConfig);
            }
        }
    }//end getInterfaceConfig()
}