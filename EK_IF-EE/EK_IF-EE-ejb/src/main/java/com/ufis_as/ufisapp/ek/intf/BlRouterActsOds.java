package com.ufis_as.ufisapp.ek.intf;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.intf.acts_ods.BlHandleActsOdsBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.acts_ods.crewFlightAssignment.CrewOnFlight;

@Stateless
public class BlRouterActsOds {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlRouterActsOds.class);

	private JAXBContext _cnxJaxb;

	private Unmarshaller _um;

	private List<String> qFromMqList = null;

	/*
	 * TODO In the future we will use seperate config per Interface for the
	 * moment we will jsut take the first from the list
	 */
	private List<InterfaceConfig> interfaceConfigs = new ArrayList<>();
	/*
	 * TODO move XMLConfiguration config to the Singleton class
	 */
	private static XMLConfiguration config;

	@EJB
	private BlHandleActsOdsBean _actsOdsHandler;
	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;

	String dtflString = HpEKConstants.ACTS_DATA_SOURCE;
	String logLevel = null;
	// EntUfisMsgExpt msgExpData;
	Boolean msgLogged = Boolean.FALSE;

	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(CrewOnFlight.class);
			dtflString = HpCommonConfig.dtfl;
			_um = _cnxJaxb.createUnmarshaller();

		} catch (JAXBException ex) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}

	public void routerCommand(String commandName, String queueName) {
		LOG.info("RouterCommand commandName {} , queueName {}", commandName,
				queueName);

	}

	public boolean routeMessage(Message message,
			InterfaceConfig interfaceConfig, Long irmtabRef) {
		CrewOnFlight crewOnFlight;
		logLevel = HpCommonConfig.irmtab;
		msgLogged = Boolean.FALSE;
		TextMessage msg = (TextMessage) message;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		try {
			String inMessage = msg.getText();
			if (HpUfisUtils.isNullOrEmptyStr(inMessage)) {
				LOG.error("Incoming message is Empty/null.");
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, irmtabRef);
				}
				return false;
			}
			crewOnFlight = (CrewOnFlight) _um.unmarshal(new StreamSource(
					new StringReader(inMessage)));

			if (crewOnFlight == null) {

				LOG.warn("Message is not valid. Dropping the message :{}",
						inMessage);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, irmtabRef);
				}
				return false;
			}

			if (crewOnFlight.getFlightId() == null
					|| crewOnFlight.getFlightDetails() == null) {
				LOG.warn("Dropped Message details(Flight details are not specified): ");
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, irmtabRef);
				}
				return false;
			}
			if (HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getFlightId()
					.getFltNum())
					|| crewOnFlight.getFlightId().getFltDate() == null
					|| HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getFlightId()
							.getCxCd())) {
				LOG.warn(
						"Dropped Message details(Flight Number or Flight Date or CxCd are not specified): "
								+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND, irmtabRef);
				}
				return false;
			}
			if (!(crewOnFlight.getFlightId().getCxCd().equalsIgnoreCase("EK") || crewOnFlight
					.getFlightId().getCxCd().equalsIgnoreCase("QF"))) {
				LOG.warn("Dropped Message details(Not EK flight): "
						+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWALC, irmtabRef);
				}
				return false;
			}
			if (HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getFlightId()
					.getDepNum().toString())
					|| HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getFlightId()
							.getDepStn())
					|| HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getFlightId()
							.getArrStn())) {
				LOG.warn(
						"Either Departure Number or Departure Station or Arrival Station is not defined:: "
								+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND, irmtabRef);
				}
				return false;
			}
			if (HpUfisUtils.isNullOrEmptyStr(crewOnFlight.getOriginator())
					|| !crewOnFlight.getOriginator()
							.equalsIgnoreCase("ACTSODS")) {

				LOG.warn("Dropped Message details(Originator is not valid): "
						+ "FlightNum = {}, " + "Flight Date = {}, "
						+ " Originator= {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate(),
								crewOnFlight.getOriginator() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWSRC, irmtabRef);
				}
				return false;
			}
			if (crewOnFlight.getMessageType() == null
					|| crewOnFlight.getActionType() == null) {
				LOG.warn(
						"Dropped Message details(Message Type ,Action Type is not valid): "
								+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND, irmtabRef);
				}
				return false;
			}
			if (crewOnFlight.getCrewFlightAssignment() == null) {
				LOG.warn(
						"Flight Crew Assignments are not specified. Dropping the message Details:"
								+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND, irmtabRef);
				}
				return false;
			} else if (crewOnFlight.getCrewFlightAssignment()
					.getCrewAssignment().isEmpty()
					|| crewOnFlight.getCrewFlightAssignment()
							.getCrewAssignment() == null) {
				LOG.warn(
						"Crew Assignments are not specified. Dropping the message Details:"
								+ "FlightNum = {}, " + "Flight Date = {}, ",
						new Object[] { crewOnFlight.getFlightId().getFltNum(),
								crewOnFlight.getFlightId().getFltDate() });
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND, irmtabRef);
				}
				return false;
			}
			LOG.debug(" message is Valid flight message");
			_actsOdsHandler.tranformFlightCrews(crewOnFlight, irmtabRef,
					message);

			return true;

		} catch (JAXBException e) {
			LOG.error("!!!!ERROR:  " + e);
			return false;
		} catch (Exception e) {
			LOG.error("!!!!ERROR:  " + e);
			return false;
		}

	}

	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString());
		dataList.add(dtflString);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflString);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

	}

	public List<String> getqFromMqList() {
		return qFromMqList;
	}

	public void setqFromMqList(List<String> qFromMqList) {
		this.qFromMqList = qFromMqList;
	}

}
