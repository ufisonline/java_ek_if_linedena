package com.ufis_as.ufisapp.ek.intf;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConnX;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxX;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequestX;
import com.ufis_as.ufisapp.ek.intf.macs.BlHandleCedaMacsPaxX;
import com.ufis_as.ufisapp.ek.intf.macs.BlHandleMacsPaxX;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacadeForPax;

/**
 * 
 * @author SCH
 * 
 */


@Stateless
//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlRouterMacsPax {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlRouterMacsPax.class);

	@EJB
	private BlHandleCedaMacsPaxX _blHandleCedaMacsPax;

	@EJB
	private BlHandleMacsPaxX blHandleMacsPax;
	@EJB
	private BlUfisExceptionQueue _ufisExptMessage;
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	private BlIrmtabFacadeForPax _irmtabFacade;
	@EJB
	BlUfisBCTopic clsBlUfisBCTopic;

//	private JAXBContext _cnxJaxb;
//	private Unmarshaller _um;
	boolean isExptNofity = false;
	private long irmtabUrno = 0;


//	@PostConstruct
//	private void initialize() {
//		try {
//			_cnxJaxb = JAXBContext.newInstance(PaxDetails.class,
//					InbDetails.class, OnwDetails.class, FctDetails.class);
//			_um = _cnxJaxb.createUnmarshaller();
//		} catch (JAXBException ex) {
//			LOG.error("JAXBException when creating Unmarshaller");
//		}
//
//	}

//	 @Asynchronous
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void routeMessage(TextMessage message, 
			String name, long irmtabRef) {
		
		String msgString = "";
		try {
			msgString = message.getText();
		} catch (JMSException e1) {
			LOG.error("Erro while getting the text from JMS msg, {}",e1);
		}


//		blHandleMacsPax.setRawMsg(message);           // commented on 2014-01-22
//		_blHandleCedaMacsPax.setRawMsg(message); // commented on 2014-01-22
		// blHandleMacsPax.set_flightXml(message);
//		blHandleMacsPax.set_um(_um);
		// blHandleMacsPax.set

		switch (name) {
		case "paxDetails":
			try {

				if ( msgString.contains("PaxDetails")) {
					Object o = blHandleMacsPax.unMarshal(name, msgString, irmtabRef, message);
					EntDbLoadPaxX entDbLoadPax =null;
					if (o != null ){
						entDbLoadPax =  (EntDbLoadPaxX)o;
					}
					
//					long startTime = System.currentTimeMillis();
					
					if(entDbLoadPax != null){
					_blHandleCedaMacsPax.handleMACSPAX(entDbLoadPax, irmtabRef);
					}

//					LOG.info("1.2 handle paxDetails msg, takes {} ms",System.currentTimeMillis() - startTime);
				} else {
					LOG.warn(" msg from pax queue not contain 'PaxDetails', msg will not process ");
				}
			} catch (Exception e) {
				LOG.error("paxDetails - Exception: {}", e);
				LOG.error("Input msg : "+ msgString);
			}
			break;
		case "inb":
			try {
				if (msgString.contains("InbDetails")) {
 
					Object o = blHandleMacsPax.unMarshal(name, msgString, irmtabRef, message);
					EntDbLoadPaxConnX entDbLoadPaxConn =null;
					if (o != null ){
						 entDbLoadPaxConn =  (EntDbLoadPaxConnX)o;
					}
								
					if (entDbLoadPaxConn != null){		
					_blHandleCedaMacsPax.handleMACSPAX(entDbLoadPaxConn,HpEKConstants.ARR_DEP_FALG_A, irmtabRef);
					}

//					LOG.info("1.2 handle InbDetails msg, takes {} ms",HpClock.GetDuration(true));
				} else {
					LOG.warn("msg from inb queue not contain 'InbDetails' , msg will not process");
				}
			} catch (Exception e) {
				LOG.error("inWard - Exception: {}", e);
			}
			break;
		case "onw":
			try {
				if (msgString.contains("OnwDetails")) {
					Object o = blHandleMacsPax.unMarshal(name, msgString, irmtabRef, message);
					EntDbLoadPaxConnX entDbLoadPaxConn =null;
					if (o != null ){
						 entDbLoadPaxConn =  (EntDbLoadPaxConnX)o;
					}
					
					if (entDbLoadPaxConn != null){
					 _blHandleCedaMacsPax.handleMACSPAX(entDbLoadPaxConn,HpEKConstants.ARR_DEP_FALG_D, irmtabRef);
					}

//					LOG.info("1.2 handle OnwDetails msg, takes {} ms",HpClock.GetDuration(true));
				} else {
					LOG.warn("msg from onw queue not contain 'OnwDetails' , msg will not process");
				}
			} catch (Exception e) {
				LOG.error("onWard - Exception: {}", e);
				LOG.error("Input msg : "+ msgString);
			}
			break;
		case "fct":
			try {
				if ( msgString.contains("FctDetails")) {
					Object o = blHandleMacsPax.unMarshal(name, msgString, irmtabRef, message);
					EntDbServiceRequestX entDbServiceRequest =null;
					if (o != null){
						entDbServiceRequest =  (EntDbServiceRequestX)o;
					}

					if (entDbServiceRequest != null){
					_blHandleCedaMacsPax.handleMACSPAX(entDbServiceRequest, irmtabRef);
					}

//					LOG.info("1.2 handle FctDetails msg, takes {} ms",HpClock.GetDuration(true));
				} else {
					LOG.warn("msg from fct queue not contain 'FctDetails' , msg will not process");
				}
			} catch (Exception e) {
				LOG.error("Fct - Exception: {}", e);
				LOG.error("Input msg : "+ msgString);
			}
			break;

		}


	}

	public long getIrmtabUrno() {
		return irmtabUrno;
	}

	public void setIrmtabUrno(long irmtabUrno) {
		this.irmtabUrno = irmtabUrno;
	}

}
