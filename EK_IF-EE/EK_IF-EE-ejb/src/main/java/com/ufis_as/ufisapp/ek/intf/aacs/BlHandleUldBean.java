package com.ufis_as.ufisapp.ek.intf.aacs;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldItemType;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldLoc;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldPos;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldSubType;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldType;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldMoveBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.aacs.uldinfo.ULDINFO;
import ek.aacs.uldscan.ULDSCANNINGDETAILS;

/**
 * 	@author BTR
 *  @version 1:		initial implementation refers to design doc
 * 	@version 1.1.0	2013-10-18 	Modified for:	
 * 								1.) internal exception handling to Q_DATS
 * 								2.) Tibco reconnection
 *  							3.) Data changes Notification to Ufis AMQ topic UFIS_IF_NOTIFY_AACS
 *  @version 1.1.1	2014-01-08 	BTR - check master data for input ULD_POSITION and ULD_DISPATCH_TO_LOC_CODE value only when the value exists
 *  								- Used standardized version to send notification.
 *  								- Changed the BlIrmtabFacade from Singleton to Stateless bean.
 *  
 *  @version 1.1.2 2014-01-20 	BTR - check master data for input ULD_ITEMTYPE
 *  								- changed to use Password Decryption for tibco conx.
 */
@Stateless
public class BlHandleUldBean extends BaseDTO{
	
	public static final Logger LOG = LoggerFactory.getLogger(BlHandleUldBean.class);
	
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;
	
	@EJB
	DlLoadUldBean _dlLoadUldBean;
	@EJB
	DlLoadUldMoveBean _dlLoadUldMoveBean;
	@EJB
    IAfttabBeanLocal afttabBean;
	@EJB
	EntStartupInitSingleton _BasicDataSingleton;
	@EJB
    BlUfisBCTopic clsBlUfisBCTopic;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	BlIrmtabFacade _irmfacade;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	BlHandleUldMoveBean clsHandleUldMoveBean;
	
	String inputMsg = null, expData = null;
	
	@PostConstruct
	private void initialize()
	{
		try {
			_cnxJaxb = JAXBContext.newInstance(ULDINFO.class, ULDSCANNINGDETAILS.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = _BasicDataSingleton.getDtflStr();

		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... ");
		}
	}

	public BlHandleUldBean() {
	}
	
	public boolean processULD(String message, long irmtabRef) {
    	try {
    	 	// clear
        	data.clear();
        	
        	// set urno of irmtab
        	irmtabUrno = irmtabRef;
			ULDINFO _inputUldInfo = (ULDINFO) _um.unmarshal(new StreamSource(new StringReader(message)));
			if(checkForMandatory(_inputUldInfo)){
				LOG.error("Compulsory uld info are empty. Message dropped. ");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			//ignore if it's not EK flight
			if(_inputUldInfo.getFLIGHTNO().contains("EK")){
			
				String flno = null;
				String connectedFlno = "0";
				String flDate = null;
				String connectedFlDate = null;
				EntDbAfttab entFlight = null;
				String act3 = null;
				String act5 = null;
				BigDecimal urno = null;
				BigDecimal connectedUrno = null;
				
				//convert the string for flight "EK" to ufis ceda flight num format
				flno = _inputUldInfo.getFLIGHTNO().substring(0,2) + " " +
						HpUfisUtils.formatCedaFltn(_inputUldInfo.getFLIGHTNO().substring(2, _inputUldInfo.getFLIGHTNO().length()));
				if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getCONNFLIGHTNO()))
					connectedFlno = "0";
				else 
					connectedFlno = _inputUldInfo.getCONNFLIGHTNO().substring(0,2) + " "+ 
						 HpUfisUtils.formatCedaFltn(_inputUldInfo.getCONNFLIGHTNO().substring(2,_inputUldInfo.getCONNFLIGHTNO().length()));
				
				flDate = convertFlDateToUTC(_inputUldInfo.getFLIGHTDATE());
				if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getCONNFLIGHTDATE()))
					connectedFlDate = convertFlDateToUTC(_inputUldInfo.getCONNFLIGHTDATE());
				
				//get the flight urno for arrival
				if("A".equals(_inputUldInfo.getFLIGHTAD())){
					entFlight = afttabBean.findUrnoForArr(flno, flDate);
					if(connectedFlno != "0"){
						//get the connected flight urno for arr which is not arrival
						if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getCONNFLIGHTNO()))
							connectedUrno = afttabBean.findUrnoForNotArr(connectedFlno, connectedFlDate);
					}
				}
				//get the flight urno for dept
				else if("D".equals(_inputUldInfo.getFLIGHTAD())){
					entFlight = afttabBean.findUrnoForDept(flno, flDate);
					if(connectedFlno != "0"){
						//get the connected flight urno for dept which is not dept
						if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getCONNFLIGHTNO()))
							connectedUrno = afttabBean.findUrnoForNotDept(connectedFlno, connectedFlDate);
					}
				}
				if(entFlight == null){
					LOG.error("Message dropped.");
					addExptInfo(EnumExceptionCodes.ENOFL.name(), flno);
					return false;
				}else{
					urno = entFlight.getUrno();
					act3 = entFlight.getAct3().trim();
					act5 = entFlight.getAct5().trim();
				}
				connectedUrno = (connectedUrno == null) ? new BigDecimal(0) : connectedUrno;
				
				EntDbLoadUld loadUld = null;
				EntDbLoadUld oldLoadUld = null;
				String cmdForBroadcasting = null;
				
				//find existing record depending on IdFlight and UldNumber..
				loadUld = _dlLoadUldBean.getUldNum(urno, _inputUldInfo.getULDNO());
				
				if(loadUld == null){
					if("Y".equals(_inputUldInfo.getULDISDELETED())){
						LOG.error("ULD record does not exist to delete. Message dropped.");
						addExptInfo(EnumExceptionCodes.ENOUD.name(), _inputUldInfo.getULDISDELETED());
						return false;
					}
					loadUld = new EntDbLoadUld();
					loadUld.setFltDate(new HpUfisCalendar(flDate).getTime());
					loadUld.setUldCurrFltno(flno);
					loadUld.setUldNumber(_inputUldInfo.getULDNO());
					loadUld.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					loadUld.setCreatedUser(HpEKConstants.ULD_SOURCE);
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.IRT.name();
				}
				else{
					oldLoadUld = new EntDbLoadUld(loadUld);
					loadUld.setUpdatedUser(HpEKConstants.ULD_SOURCE);
					loadUld.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.URT.name();
				}
					
				loadUld.setIdFlight(urno);
				loadUld.setIdConxFlight(connectedUrno);
				
				// only when the value is exist, check against the md value
				if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDPOSITION())){
					//set the ULD Basic Data
					List<EntDbMdUldPos> uldPosList = _BasicDataSingleton.getUldPosList();
					boolean isFound = false;
					
					if(!uldPosList.isEmpty()){
						for(int i = 0; i < uldPosList.size(); i++){
							//get uld pos according to act3, act5 and position name
							if(entFlight != null){
								if(uldPosList.get(i).getAircraftType3().trim().equals(act3) &&
										uldPosList.get(i).getAircraftType5().trim().equals(act5) &&
										uldPosList.get(i).getUldPosition().trim().equals(_inputUldInfo.getULDPOSITION())){
									loadUld.setIdMdUldPos(uldPosList.get(i).getId());
									isFound = true;
									break;
								}
							}
						}
					}
					if(!isFound){
						LOG.warn("Master Data Uld Position is not match in MdUldPos.");
						addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("ULD_POSITION => %s", _inputUldInfo.getULDPOSITION()));
					}
				} else
					LOG.error("Input ULD_POSITION is empty/blank.");
				
				loadUld = checkUldInfoMaterdata(_inputUldInfo, loadUld);//call to check MD
				loadUld.setDataSource(HpEKConstants.ULD_SOURCE);
				
				loadUld.setArrDepFlag(_inputUldInfo.getFLIGHTAD());
				loadUld.setUldPosition(_inputUldInfo.getULDPOSITION());
				loadUld.setUldType(_inputUldInfo.getULDTYPE());
				loadUld.setUldSubtype(_inputUldInfo.getULDSUBTYPE());
				loadUld.setUldItemtype(_inputUldInfo.getULDITEMTYPE());
				loadUld.setUldWeight(_inputUldInfo.getULDWEIGHT());
				loadUld.setUldDispLocCode(_inputUldInfo.getULDDISPATCHTOLOCCODE());
				loadUld.setUldDispLocDesc(_inputUldInfo.getULDDISPATHCTOLOCDESC());
				String status = " ";
				if("Y".equals(_inputUldInfo.getULDISDELETED())){
					status = "X";
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.DRT.toString();	
				}
				
				loadUld.setRecFlag(status);
				loadUld.setUldConxFlno(connectedFlno);
				if(connectedFlDate != null)
					loadUld.setConxFltDate(new HpUfisCalendar(connectedFlDate).getTime());
				loadUld.setConxToUld(_inputUldInfo.getCONNULDNO());
				if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getRECORDSENDDATE()))
					loadUld.setUldSentDate(new HpUfisCalendar(convertFlDateToUTC(_inputUldInfo.getRECORDSENDDATE())).getTime());
				
				EntDbLoadUld resultUld = _dlLoadUldBean.merge(loadUld);
				
				if(resultUld != null){
					sendNotifyUldInfoToInJson(resultUld, oldLoadUld, cmdForBroadcasting);
				}
			}
			else {
				LOG.error("Wrong airline. Non EK flight. Message dropped");
				addExptInfo(EnumExceptionCodes.EWALC.name(), _inputUldInfo.getFLIGHTNO());
				return false;
			}
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		} catch (ParseException e) {
			LOG.error("DateTime parsing error {}", e.toString());
		}
    	catch(Exception e){
    		LOG.error("ERRROR : {}", e);
    	}
		return true;
	}

	private boolean checkForMandatory(ULDINFO _inputUldInfo) {
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getFLIGHTDATE().trim())){
				expData = _inputUldInfo.getFLIGHTDATE();
				return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getFLIGHTNO().trim())){
			expData = _inputUldInfo.getFLIGHTNO();
			return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getFLIGHTAD().trim())){
			expData = _inputUldInfo.getFLIGHTAD();
			return true;
		}
		if( HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDNO().trim())){
			expData = _inputUldInfo.getULDNO();
			return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDTYPE().trim())){
			expData = _inputUldInfo.getULDTYPE();
			return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDSUBTYPE().trim())){
			expData = _inputUldInfo.getULDSUBTYPE();
			return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDISDELETED().trim())){
			expData = _inputUldInfo.getULDISDELETED();
			return true;
		}
		if(HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getRECORDSENDDATE().trim())){
			expData = _inputUldInfo.getRECORDSENDDATE();
			return true;
		}
		return false;
	}

	private EntDbLoadUld checkUldInfoMaterdata(ULDINFO _inputUldInfo, EntDbLoadUld loadUld) {
		List<EntDbMdUldType> uldTypeList = _BasicDataSingleton.getUldTypeList();
		List<EntDbMdUldSubType> uldSubTypeList = _BasicDataSingleton.getUldSubtypeList();
		List<EntDbMdUldItemType> uldItemTypeList = _BasicDataSingleton.getUldItemTypeList();
		List<EntDbMdUldLoc> uldLocList = _BasicDataSingleton.getUldLocList();
		
		boolean isFound = false;
		if(!uldTypeList.isEmpty()){
			for(int i = 0; i < uldTypeList.size(); i++){
				if(uldTypeList.get(i).getUldTypeCode().equals(_inputUldInfo.getULDTYPE())){
					loadUld.setIdMdUldType(uldTypeList.get(i).getId());
					isFound = true;
					break;
				}
			}
		}
		if(!isFound){
			LOG.warn("Master Data ULDType is not match in MdUldType.");
			addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("ULD_TYPE => %s", _inputUldInfo.getULDTYPE()));
		}
		
		isFound = false;
		if(!uldSubTypeList.isEmpty()){
			for(int i = 0; i < uldSubTypeList.size(); i++){
				if(uldSubTypeList.get(i).getUldSubtypeCode().equals(_inputUldInfo.getULDSUBTYPE())){
					loadUld.setIdMdUldSubtype(uldSubTypeList.get(i).getId());
					isFound = true;
					break;
				}
			}
		}
		if(!isFound){
			LOG.warn("Master Data ULDSubType is not match in MdUldSubType.");
			addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("ULD_SUBTYPE => %s", _inputUldInfo.getULDSUBTYPE()));
		}
		
		// only when the value is exist, check against the md value
		if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDITEMTYPE())){
			isFound = false;
			if(!uldItemTypeList.isEmpty()){
				for(int i = 0; i < uldItemTypeList.size(); i++){
					if(uldItemTypeList.get(i).getUldItemTypeCode().equals(_inputUldInfo.getULDITEMTYPE())){
						loadUld.setUldItemtype(uldItemTypeList.get(i).getId());
						isFound = true;
						break;
					}
				}
			}
			if(!isFound){
				LOG.warn("Master Data ULDItemType is not match in MdUldItemType.");
				addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("ULD_ITEMTYPE => %s", _inputUldInfo.getULDITEMTYPE()));
			}
		} else
			LOG.error("Input ULD_ITEMTYPE is empty/blank.");
		
		// only when the value is exist, check against the md value
		if(!HpUfisUtils.isNullOrEmptyStr(_inputUldInfo.getULDDISPATCHTOLOCCODE())){
			isFound = false;
			if(!uldLocList.isEmpty()){
				for(int i = 0; i < uldLocList.size(); i++){
					if(uldLocList.get(i).getUldPlaceLoc().equals(_inputUldInfo.getULDDISPATCHTOLOCCODE())){
						loadUld.setIdMdUldLoc(uldLocList.get(i).getId());
						isFound = true;
						break;
					}
				}
			}
			if(!isFound){
				LOG.warn("Master Data ULDLoc is not match in MdUldLoc.");
				addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("ULD_DISPATCH_TO_LOC_CODE => %s",_inputUldInfo.getULDDISPATCHTOLOCCODE()));
			}
		} else
			LOG.error("Input ULD_DISPATCH_TO_LOC_CODE is empty/blank.");
		
		return loadUld;
	}
	
	private void sendNotifyUldInfoToInJson(EntDbLoadUld entUld, EntDbLoadUld oldLoadUld, String cmd)
			throws IOException, JsonGenerationException, JsonMappingException {
		String urno = entUld.getIdFlight().toString();
		/*// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setOrig(dtfl);
		header.setBcnum(-1);
		List<String> idFlightList = new ArrayList<>();
		idFlightList.add(urno);
		header.setIdFlight(idFlightList);

		// get the BODY ACTION info
		List<String> fldList = new ArrayList<>();
		fldList.add("ID_FLIGHT");
		fldList.add("ID_CONX_FLIGHT");
		fldList.add("ULD_NUMBER");
		fldList.add("ULD_CURR_FLNO");
		fldList.add("ULD_CONX_FLNO");
		fldList.add("REC_STATUS");

		List<Object> dataList = new ArrayList<>();
		dataList.add(urno);
		dataList.add(entUld.getIdConxFlight().toString());
		dataList.add(entUld.getUldNumber());
		dataList.add(entUld.getUldCurrFltno());
		dataList.add(entUld.getUldConxFlno());
		dataList.add(entUld.getRecFlag());
		
		List<Object> oldList = new ArrayList<>();
		if(UfisASCommands.URT.name().equals(cmd)){
			oldList.add(oldLoadUld.getIdFlight().toString());
			oldList.add(oldLoadUld.getIdConxFlight().toString());
			oldList.add(oldLoadUld.getUldNumber());
			oldList.add(oldLoadUld.getUldCurrFltno());
			oldList.add(oldLoadUld.getUldConxFlno());
			oldList.add(oldLoadUld.getRecFlag());
		}
			
		List<String> idList = new ArrayList<>();
		idList.add(entUld.getId());

		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		action.setCmd(cmd);
		action.setTab("LOAD_ULDS");
		action.setFld(fldList);
		action.setData(dataList);
		action.setOdat(oldList);
		action.setId(idList);
		action.setSel("WHERE ID = \""+entUld.getId()+"\"");
		listAction.add(action);

		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		clsBlUfisBCTopic.sendMessage(msg);*/
		
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), urno, oldLoadUld, entUld);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), urno, oldLoadUld, entUld);
		}
		LOG.debug("Sent Notify ULDs from AACS");
	}

	private String convertFlDateToUTC(String flightDate)
			throws ParseException {
		SimpleDateFormat dfm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date flDate =  dfm.parse(flightDate);
		HpUfisCalendar utcDate = new HpUfisCalendar(flDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();
	}
}