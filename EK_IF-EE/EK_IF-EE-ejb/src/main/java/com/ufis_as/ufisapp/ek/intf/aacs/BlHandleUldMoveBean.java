package com.ufis_as.ufisapp.ek.intf.aacs;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUldMove;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldLoc;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldScanAct;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldMoveBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.aacs.uldinfo.ULDINFO;
import ek.aacs.uldscan.ULDSCANNINGDETAILS;

/**
 * @author BTR
 *  @version 1:		initial implementation refers to design doc
 * 	@version 1.1.0	2013-10-18 	Modified for:	
 * 								1.) internal exception handling to Q_DATS
 * 								2.) Tibco reconnection
 *  							3.) Data changes Notification to Ufis AMQ topic UFIS_IF_NOTIFY_AACS
 */
@Stateless
public class BlHandleUldMoveBean extends BaseDTO{
	
	public static final Logger LOG = LoggerFactory.getLogger(BlHandleUldMoveBean.class);
	
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;
	
	@EJB
	DlLoadUldBean _dlLoadUldBean;
	@EJB
	DlLoadUldMoveBean _dlLoadUldMoveBean;
	@EJB
    IAfttabBeanLocal afttabBean;
	@EJB
	EntStartupInitSingleton _BasicDataSingleton;
	@EJB
    BlUfisBCTopic clsBlUfisBCTopic;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	BlIrmtabFacade _irmfacade;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	
	String inputMsg = null;
	
	@PostConstruct
	private void initialize()
	{
		try {
			_cnxJaxb = JAXBContext.newInstance(ULDINFO.class, ULDSCANNINGDETAILS.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = _BasicDataSingleton.getDtflStr();

		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... ");
		}
	}

	public BlHandleUldMoveBean() {
	}
	
	public boolean processULDScanning(String message, long irmtabRef){
    	try {
    	  	// clear
        	data.clear();
        	// set urno of irmtab
        	irmtabUrno = irmtabRef;
        	
			ULDSCANNINGDETAILS _inputUldScanInfo = (ULDSCANNINGDETAILS) _um.unmarshal(new StreamSource(new StringReader(message)));
			//check if the primary data for uld scan info are empty or null
			if(HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getFLIGHTDATE())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getFLIGHTNO())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getFLIGHTAD())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getULDNUMBER())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANACTION())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANLOCATIONCODE())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANLOCATIONDESC())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANDATETIME())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANBY())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getRECORDTYPE())
					|| HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getRECORDSENDDATE())){
				
				LOG.error(" Compulsory uld scan info are empty. Message dropped. ");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			//ignore if it's not EK flight
			if(_inputUldScanInfo.getFLIGHTNO().contains("EK")){
				String flno = null;
				String flDate = null;
				String idUld = null;
				EntDbAfttab entFlight = null;
				BigDecimal urno = null;
				//convert the string for flight "EK" to ufis ceda flight num format 
				flno = _inputUldScanInfo.getFLIGHTNO().substring(0, 2) + " " +
						HpUfisUtils.formatCedaFltn(_inputUldScanInfo.getFLIGHTNO().substring(2, _inputUldScanInfo.getFLIGHTNO().length()));
				flDate = convertFlDateToUTC(_inputUldScanInfo.getFLIGHTDATE());
				//get the flight urno for arrival
				if("A".equals(_inputUldScanInfo.getFLIGHTAD()))
					entFlight = afttabBean.findUrnoForArr(flno, flDate);

				//get the flight urno for dept
				else if("D".equals(_inputUldScanInfo.getFLIGHTAD()))
					entFlight = afttabBean.findUrnoForDept(flno, flDate);
				
				if(entFlight == null){
					LOG.error("Message dropped..");
					addExptInfo(EnumExceptionCodes.ENOFL.name(), flno);
					return false;
				}
				urno = entFlight.getUrno();
				String uldNumber = _inputUldScanInfo.getULDNUMBER().trim();
				EntDbLoadUld entUld = _dlLoadUldBean.getUldNum(urno, uldNumber);
				idUld = (entUld == null) ? "0" : entUld.getId();
				
				boolean flag = false;
				String cmdForBroadcasting =  null;
				EntDbLoadUldMove loadUldMove = null;
				EntDbLoadUldMove oldLoadUldMove = null;
				
				if("UNDO".equals(_inputUldScanInfo.getRECORDTYPE())){
					loadUldMove = _dlLoadUldMoveBean.getUldMoveByIdFlightUldNum(urno, uldNumber);
					flag = true;
				}
				if(loadUldMove == null){//null? create a new record
					if(flag){
						LOG.error(" ULD record does not exist to UNDO.  Message dropped.");
						addExptInfo(EnumExceptionCodes.ENOUD.name(), uldNumber);
						return false;
					}
					loadUldMove = new EntDbLoadUldMove();
					oldLoadUldMove = loadUldMove;
					loadUldMove.setFltDate(new HpUfisCalendar(flDate).getTime());
					loadUldMove.setUldFltno(flno);
					loadUldMove.setUldNumber(_inputUldScanInfo.getULDNUMBER());
					loadUldMove.setCreatedUser(_inputUldScanInfo.getSCANBY());
					loadUldMove.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.IRT.toString();
				}
				else{
					oldLoadUldMove = new EntDbLoadUldMove(loadUldMove);
					loadUldMove.setUpdatedUser(_inputUldScanInfo.getSCANBY());
					loadUldMove.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.URT.toString();
				}
				loadUldMove.setIdFlight(urno);
				loadUldMove.setIdLoadUlds(idUld);
				List<EntDbMdUldLoc> uldLocList = _BasicDataSingleton.getUldLocList();
				List<EntDbMdUldScanAct> uldScanActList = _BasicDataSingleton.getUldScanActList();
				
				boolean isFound = false;
				for(int i = 0; i < uldLocList.size(); i++){
					if(uldLocList.get(i).getUldPlaceLoc().trim().equals(_inputUldScanInfo.getSCANLOCATIONCODE().trim())){
						loadUldMove.setIdScanMdUldLoc(uldLocList.get(i).getId());
						isFound = true;
					}
					//if(!HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANDISLOCCODE())){ // only when the value is exist, check against the md value
						if(uldLocList.get(i).getUldPlaceLoc().trim().equals(_inputUldScanInfo.getSCANDISLOCCODE().trim())){
							loadUldMove.setIdDispMdUldLoc(uldLocList.get(i).getId());
							isFound = true;
						}
					//}
				}
				if (!isFound) {
					addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("SCAN_LOCATION_CODE/SCAN_DIS_LOC_CODE => %s/%s", _inputUldScanInfo.getSCANLOCATIONCODE().trim(),_inputUldScanInfo.getSCANDISLOCCODE().trim()));
					LOG.warn("Master Data scanLoc/scanDisLoc code is not match in MdUldLoc.");
				}
				
				isFound = false;
				for(int i = 0; i < uldScanActList.size(); i++){
					if(uldScanActList.get(i).getUldScanAction().trim().equals(_inputUldScanInfo.getSCANACTION().trim())){
						loadUldMove.setIdMdUldScanAct(uldScanActList.get(i).getId());
						isFound = true;
						break;
					}
				}
				if (!isFound) {
					addExptInfo(EnumExceptionCodes.WNOMD.name(), String.format("SCAN_ACTION => %s", _inputUldScanInfo.getSCANACTION().trim()));
					LOG.warn("Master Data uldScanAction is not match in MdUldScanAct.");
				}
				
				String status = " ";
				if("UNDO".equals(_inputUldScanInfo.getRECORDTYPE())){
					status = "X";
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.DRT.toString();
				}
				loadUldMove.setRecFlag(status);
				loadUldMove.setArrDepFlag(_inputUldScanInfo.getFLIGHTAD());
				loadUldMove.setScanAction(_inputUldScanInfo.getSCANACTION());
				loadUldMove.setScanLocCode(_inputUldScanInfo.getSCANLOCATIONCODE());
				loadUldMove.setScanLocDesc(_inputUldScanInfo.getSCANLOCATIONDESC());
				if(!HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getSCANDATETIME()))
						loadUldMove.setScanDate(new HpUfisCalendar(convertFlDateToUTC(_inputUldScanInfo.getSCANDATETIME())).getTime());
				loadUldMove.setScanUser(_inputUldScanInfo.getSCANBY());
				loadUldMove.setDataSource(HpEKConstants.ULD_SOURCE);
				loadUldMove.setDispLocCode(_inputUldScanInfo.getSCANDISLOCCODE());
				loadUldMove.setDispLocDesc(_inputUldScanInfo.getSCANDISLOCDESC());
				
				if(!HpUfisUtils.isNullOrEmptyStr(_inputUldScanInfo.getRECORDSENDDATE()))
					loadUldMove.setUldSentDate(new HpUfisCalendar(convertFlDateToUTC(_inputUldScanInfo.getRECORDSENDDATE())).getTime());
				EntDbLoadUldMove uldMove = _dlLoadUldMoveBean.merge(loadUldMove);
				if(uldMove != null){
					sendNotifyUldMoveInJson(uldMove, oldLoadUldMove, cmdForBroadcasting);
				}
			}
			else {
				LOG.error("Processing performs only for EK flight. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWALC.name(), _inputUldScanInfo.getFLIGHTNO());
				return false;
			}
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		} catch (ParseException e) {
			LOG.error("DateTime parsing error {}", e.toString());
		}catch(Exception e){
    		LOG.error("ERRROR : {}", e.toString());
    	}
		return true;
	}
	
	private void sendNotifyUldMoveInJson(EntDbLoadUldMove entUld, EntDbLoadUldMove oldUldMove, String cmd)
			throws IOException, JsonGenerationException, JsonMappingException {
		String urno = entUld.getIdFlight().toString();
/*
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setOrig(dtfl);
		header.setBcnum(-1);
		List<String> idFlightList = new ArrayList<>();
		idFlightList.add(urno);
		header.setIdFlight(idFlightList);

		// get the BODY ACTION info
		List<String> fldList = new ArrayList<>();
		fldList.add("ID_FLIGHT");
		fldList.add("ID_LOAD_ULDS");
		fldList.add("ULD_NUMBER");
		fldList.add("FLIGHT_NUMBER");
		fldList.add("REC_STATUS");

		List<Object> dataList = new ArrayList<>();
		dataList.add(urno);
		dataList.add(entUld.getIdLoadUlds());
		dataList.add(entUld.getUldNumber());
		dataList.add(entUld.getUldFltno());
		dataList.add(entUld.getRecFlag());

		List<String> oldList =  new ArrayList<>();
		if(UfisASCommands.URT.toString().equals(cmd)){
			oldList.add(oldUldMove.getIdFlight().toString());
			oldList.add(oldUldMove.getIdLoadUlds());
			oldList.add(oldUldMove.getUldNumber());
			oldList.add(oldUldMove.getUldFltno());
			oldList.add(oldUldMove.getRecFlag());
		}
		
		List<String> idList = new ArrayList<>();
		idList.add(entUld.getId());

		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		action.setCmd(cmd);
		action.setTab("LOAD_ULD_MOVE");
		action.setFld(fldList);
		action.setData(dataList);
		action.setId(idList);
		action.setSel("WHERE ID = \""+entUld.getId()+"\"");
		listAction.add(action);

		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		clsBlUfisBCTopic.sendMessage(msg);*/
		
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), urno, oldUldMove, entUld);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), urno, oldUldMove, entUld);
		}
		LOG.debug("Sent Notify ULD Movement from AACS");
	}

	private String convertFlDateToUTC(String flightDate)
			throws ParseException {
		SimpleDateFormat dfm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date flDate =  dfm.parse(flightDate);
		HpUfisCalendar utcDate = new HpUfisCalendar(flDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();
	}
}