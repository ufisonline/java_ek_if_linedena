package com.ufis_as.ufisapp.ek.intf.acts_ods;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.ek.eao.IBlFltJobAssignBean;
import com.ufis_as.ufisapp.ek.eao.IDlFltJobAssignLocal;

@Stateless(mappedName = "BlFltJobAssignBean")
public class BlFltJobAssignBean implements IBlFltJobAssignBean {

	@EJB
	IDlFltJobAssignLocal dlFltJobAssignLocal;

	private static final Logger LOG = LoggerFactory
			.getLogger(BlFltJobAssignBean.class);

	@Override
	public void storeCrewFltAssign(EntDbFltJobAssign fltJobAssign) {
		// TODO Auto-generated method stub
		boolean result = dlFltJobAssignLocal.saveCrewFlightAssign(fltJobAssign);
		// LOG.debug("Inserted the fltJobAssign:"+result);
	}

	@Override
	public void updateCrewFltAssign(EntDbFltJobAssign fltJobAssign) {
		// TODO Auto-generated method stub
		dlFltJobAssignLocal.updateCrewFlightAssign(fltJobAssign);

	}

	@Override
	public void deleteCrewFltAssign(EntDbFltJobAssign fltJobAssign) {
		EntDbFltJobAssign crewAssignment = dlFltJobAssignLocal.findCrewFlight(
				fltJobAssign.getFlightNumber(), fltJobAssign.getIdFlight(),
				fltJobAssign.getFltDate(), fltJobAssign.getStaffNumber());
		crewAssignment.setRecStatus("X");
		dlFltJobAssignLocal.updateCrewFlightAssign(crewAssignment);
	}

	@Override
	public List<EntDbFltJobAssign> retrieveCrews(String fltNum, long fltId,
			Date fltDate) {
		// TODO Auto-generated method stub
		if (fltId != 0) {
			return dlFltJobAssignLocal.getAssignedCrews(fltNum, fltId, fltDate);
		} else {
			return Collections.EMPTY_LIST;

		}
	}

	@Override
	public List<EntDbFltJobAssign> retrieveCrewsByFltIdDepOrg(
			String flightNumber, Date fltDate, String depStn, String arrStn) {

		return dlFltJobAssignLocal.getAssignedCrewsByFltIdDesOrg(flightNumber,
				fltDate, depStn, arrStn);
	}

	@Override
	public List<EntDbFltJobAssign> retrieveCrewsByFltId(long flightId) {

		return dlFltJobAssignLocal.getAssignedCrewsByFltId(flightId);
	}

}
