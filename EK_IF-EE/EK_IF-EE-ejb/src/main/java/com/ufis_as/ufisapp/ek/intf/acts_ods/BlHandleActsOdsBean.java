package com.ufis_as.ufisapp.ek.intf.acts_ods;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobSummary;
import com.ufis_as.ek_if.acts_ods.entities.EntDbMdStaffType;
import com.ufis_as.ek_if.core.entities.EntFlightDailyDTO;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlFltDailyBean;
import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.eao.IDlFltJobSummaryLocal;
import com.ufis_as.ufisapp.ek.eao.IDlMdStaffType;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.acts_ods.crewFlightAssignment.ActionType;
import ek.acts_ods.crewFlightAssignment.CrewOnFlight;
import ek.acts_ods.crewFlightAssignment.CrewOnFlight.CrewFlightAssignment;

@Stateless
public class BlHandleActsOdsBean {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleActsOdsBean.class);

	/**
	 * CDI
	 */
	/*
	 * @EJB private IAfttabBeanLocal _afttabBean;
	 */
	@EJB
	private DlFltDailyBean flitDailyBean;
	@EJB
	private BlIrmtabFacade _irmfacade;

	@EJB
	private IDlFlighttJobAssignLocal _fltJobAssign;

	@EJB
	private IDlFltJobSummaryLocal _fltJobSummary;

	@EJB
	private IDlMdStaffType _mdStaffType;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlUfisBCTopic clsBlUfisBCTopic;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;

	protected String _actionType = "";
	protected String _flightXml = "";
	protected String _messageType = "";
	protected ActionType _msgActionType;
	protected String _returnXML = "";
	private CrewOnFlight crewsOnFlight;
	String dtflString = HpEKConstants.ACTS_DATA_SOURCE;
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;
	List<String> fldList = new ArrayList<>();
	List<String> SummfldList = new ArrayList<>();
	HashMap<String, List<String>> staffTypeMap = new HashMap<String, List<String>>();
	DateFormat fldaDF = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	EntFlightDailyDTO fltDailyDto = null;

	@PreDestroy
	private void initialClear() {
		staffTypeMap.clear();
	}

	public BlHandleActsOdsBean() {

	}

	public void tranformFlightCrews(CrewOnFlight crewsOnFlightAssign,
			Long irmtabRef, Message message) {
		dtflString = HpCommonConfig.dtfl;
		logLevel = HpCommonConfig.irmtab;
		msgLogged = Boolean.FALSE;
		String crewType = null;
		Boolean isLast = Boolean.FALSE;
		EntDbFltJobAssign crewAssignment = null;
		EntDbFltJobAssign currCrewAssignment = null;
		EntDbFltJobAssign oldCrewAssignment = null;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		try {

			if (crewsOnFlightAssign.getFlightId() != null) {

				String urno = null;
				String flightNumber = crewsOnFlightAssign.getFlightId()
						.getCxCd().substring(0, 2)
						+ " "
						+ crewsOnFlightAssign.getFlightId().getFltNum()
						+ (crewsOnFlightAssign.getFlightId().getFltSuffix() == null ? ""
								: crewsOnFlightAssign.getFlightId()
										.getFltSuffix());
				String flda = chgXMLGregorianCalendarToDateString(crewsOnFlightAssign
						.getFlightId().getFltDate());
				if (HpUfisUtils.isNullOrEmptyStr(flda)) {
					LOG.error("Dropping the message .Incorrect flight date:"
							+ "flight Number = {}, " + "Flight Date= {}, ",
							new Object[] { flightNumber, flda });
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(message,
									dtflString);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.EWVAL, irmtabRef,
								isLast, flightNumber, flda);
					}
					return;
				}

				LOG.debug("flightNumber: " + flightNumber + "   FlightDate:"
						+ flda);
				/*
				 * LOG.debug("After Formating flight Date (yyyy-MM-dd):" +
				 * sdf.format(chgXMLGregorianCalendarToDate(crewsOnFlightAssign
				 * .getFlightId().getFltDate())));
				 */
				// Look into DB only if ORG or DES is 'DXB'
				// if (crewsOnFlightAssign.getFlightId().getArrStn()
				// .equalsIgnoreCase(HpEKConstants.EK_HOPO)
				// || crewsOnFlightAssign.getFlightId().getDepStn()
				// .equalsIgnoreCase(HpEKConstants.EK_HOPO)) {
				// EntDbAfttab criteriaParams = new EntDbAfttab();
				// criteriaParams.setFlno(flightNumber);
				// criteriaParams.setFlda(flda);
				/*
				 * urno = _afttabBean.getUrnoByFldaFlnoOrgDes(criteriaParams,
				 * crewsOnFlightAssign.getFlightId().getArrStn(),
				 * crewsOnFlightAssign.getFlightId().getDepStn());
				 */
				// Search for IdFlight from FLT_DAILY
				fltDailyDto = new EntFlightDailyDTO();
				fltDailyDto
						.setFlda(sdf
								.format(chgXMLGregorianCalendarToDate(crewsOnFlightAssign
										.getFlightId().getFltDate())));
				fltDailyDto.setFlightNumber(flightNumber);
				fltDailyDto.setFltOrg3(crewsOnFlightAssign.getFlightId()
						.getDepStn());
				fltDailyDto.setFltDes3(crewsOnFlightAssign.getFlightId()
						.getArrStn());
				urno = flitDailyBean.getFltDailyId(fltDailyDto);
				LOG.info("IdFlight:" + urno);
				if (HpUfisUtils.isNullOrEmptyStr(urno)) {
					LOG.error(
							"No flight is available in FLT_DAILY with the Flight details. Flight Number: {}, Flight Date: {}:Dropping the message.",
							new Object[] { flightNumber, flda });
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(message,
									dtflString);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.ENOFL, irmtabRef,
								isLast, flightNumber, flda);
					}
					return;
				}

				long idFlight = (urno != null && (String.valueOf(urno) != null) && (!urno
						.isEmpty())) ? Long.parseLong(urno) : 0;
				String recStatus = " ";

				// Retrieve all crews from DB for the flight and on FltDate

				if (!crewsOnFlightAssign.getCrewFlightAssignment()
						.getCrewAssignment().isEmpty()) {

					List<CrewFlightAssignment.CrewAssignment> crewsAssigned = crewsOnFlightAssign
							.getCrewFlightAssignment().getCrewAssignment();

					List<String> incomingCrewsList = new ArrayList<String>();

					// if (idFlight != 0 && !crewsAssigned.isEmpty()) {
					if (!crewsAssigned.isEmpty()) {

						/*
						 * LOGIC FOR SUMMARY
						 */
						if (staffTypeMap.isEmpty()) {
							List<EntDbMdStaffType> staffTypeList = _mdStaffType
									.getStaffTypeDetails();
							// Storing the Staff Type details in a list in order
							// Staff_SubType,Staff_Type
							List<String> details;
							if (!staffTypeList.isEmpty()) {
								for (EntDbMdStaffType staffTypeobj : staffTypeList) {
									details = new ArrayList<String>();
									// details.add(staffTypeobj.getStaffTypeCode());
									details.add(staffTypeobj.getStaffSubtype());
									details.add(staffTypeobj.getStaffTypeCode());
									staffTypeMap.put(
											staffTypeobj.getStaffType(),
											details);
								}
							}
						}
						HashMap<String, Long> summaryByResType = new HashMap<String, Long>();
						HashMap<String, Long> summaryByWorkType = new HashMap<String, Long>();
						HashMap<String, Long> resTypeMap = new HashMap<String, Long>();
						HashMap<String, CrewFlightAssignment.CrewAssignment> crewFilter = new HashMap<String, CrewFlightAssignment.CrewAssignment>();
						for (CrewFlightAssignment.CrewAssignment iCrew : crewsAssigned) {
							isLast = Boolean.FALSE;
							if (iCrew.equals(crewsAssigned.get(crewsAssigned
									.size() - 1))) {
								isLast = Boolean.TRUE;
							}
							if (HpUfisUtils.isNullOrEmptyStr(crewType)) {
								crewType = iCrew.getCrewType();
							}
							if (HpUfisUtils
									.isNullOrEmptyStr(iCrew.getStaffno())
							// || crews.getFirstName() == null
							// || crews.getLastName() == null
							) {
								LOG.error(
										"Saff Details (StaffNo) are not specified correctly. Dropping the Crew Assignment Details:"
												+ "FlightNum = {}, "
												+ "Flight Date = {}, "
												+ "Crew ={}, ",
										new Object[] { flightNumber, flda,
												iCrew.getStaffno() });
								if (logLevel
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflString);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(EnumExceptionCodes.EMAND,
											irmtabRef, isLast, flightNumber,
											flda);
								}
								continue;
							}
							// If similar staff occurs more than once in message
							if (crewFilter.isEmpty()) {
								crewFilter.put(iCrew.getStaffno(), iCrew);
							} else if (crewFilter.keySet().contains(
									iCrew.getStaffno())) {
								crewFilter.put(iCrew.getStaffno(), iCrew);
							} else {
								crewFilter.put(iCrew.getStaffno(), iCrew);
							}
							if (incomingCrewsList.isEmpty()) {
								incomingCrewsList.add(iCrew.getStaffno());
							} else if (!incomingCrewsList.contains(iCrew
									.getStaffno())) {
								incomingCrewsList.add(iCrew.getStaffno());
							}
						}
						List<CrewFlightAssignment.CrewAssignment> crewsAssignedFilter = new ArrayList<CrewFlightAssignment.CrewAssignment>(
								crewFilter.values());

						LOG.debug("flightNumber: "
								+ flightNumber
								+ "   idFlight:"
								+ idFlight
								+ "   FlightDate:"
								+ convertDateToUTC(crewsOnFlightAssign
										.getFlightId().getFltDate()));
						// If idFlight=0 no need to search in FLT_JOB_ASSIGN
						List<EntDbFltJobAssign> crewsFromDb = Collections.EMPTY_LIST;
						// Change on 29NOV2013
						if (idFlight != 0) {
							/*
							 * crewsFromDb = _fltJobAssign
							 * .getAssignedCrewsByFltId(idFlight);
							 */
							crewsFromDb = _fltJobAssign
									.getAssignedCrewsByFltIdCrewType(idFlight,
											crewType);
						}

						//
						// LOG.debug(" Crews from DB list size:"+
						// crewsFromDb.size());
						HashMap<String, EntDbFltJobAssign> staffFromDB = new HashMap<String, EntDbFltJobAssign>();
						if (crewsFromDb != null && !crewsFromDb.isEmpty()) {
							for (EntDbFltJobAssign rec : crewsFromDb) {
								isLast = Boolean.FALSE;
								if (rec.equals(crewsFromDb.get(crewsFromDb
										.size() - 1))) {
									isLast = Boolean.TRUE;
								}
								if (!incomingCrewsList.contains(rec
										.getStaffNumber())) {

									// rec.setUpdatedDate(HpUfisCalendar
									// .getCurrentUTCTime());

									// rec.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);
									// Change on 29NOV2013
									/*
									 * crewAssignment = _fltJobAssign
									 * .getAssignedCrewIdByFlight(
									 * rec.getIdFlight(), rec.getStaffNumber());
									 * if (crewAssignment != null) {
									 * oldCrewAssignment = crewAssignment;
									 * crewAssignment
									 * .setUpdatedDate(HpUfisCalendar
									 * .getCurrentUTCTime()); crewAssignment
									 * .setUpdatedUser
									 * (HpEKConstants.ACTS_DATA_SOURCE);
									 * crewAssignment.setRecStatus("X");
									 * currCrewAssignment = _fltJobAssign
									 * .merge(crewAssignment);
									 */
									oldCrewAssignment = rec.getClone();
									rec.setUpdatedDate(HpUfisCalendar
											.getCurrentUTCTime());
									rec.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);
									rec.setRecStatus("X");
									currCrewAssignment = _fltJobAssign
											.merge(rec);
									if (currCrewAssignment != null) {
										/*
										 * sendJobAssignUpdateToCedaInJson(
										 * currCrewAssignment,
										 * oldCrewAssignment, HpUfisAppConstants
										 * .UfisASCommands.URT .name(), isLast);
										 */

										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
											// send notification message to
											// topic
											clsBlUfisBCTopic
													.sendNotification(
															isLast.booleanValue(),
															HpUfisAppConstants.UfisASCommands.DRT,
															null,
															oldCrewAssignment,
															currCrewAssignment);
										}
										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
											// if topic not defined, send
											// notification to queue
											ufisQueueProducer
													.sendNotification(
															isLast,
															HpUfisAppConstants.UfisASCommands.DRT,
															null,
															oldCrewAssignment,
															currCrewAssignment);
										}
										LOG.debug("ACTS Job Assign Record change communicated to Ceda.");
									} else {
										LOG.error(
												"Failed on updating the record to DB. Crew Details(Staff Number): {}",
												crewAssignment.getStaffNumber());
									}
									// }

								} else {
									staffFromDB.put(rec.getStaffNumber(), rec);
								}
							}
						}
						for (CrewFlightAssignment.CrewAssignment crews : crewsAssignedFilter) {
							isLast = Boolean.FALSE;

							if (crews.equals(crewsAssignedFilter
									.get(crewsAssignedFilter.size() - 1))) {
								isLast = Boolean.TRUE;
							}
							EntDbFltJobAssign fltJobAssignObj = new EntDbFltJobAssign();
							fltJobAssignObj
									.setIdFlight(new BigDecimal(idFlight));
							if (crews.getAssignedOnDate() == null
									|| crews.getCrewTripEnd() == null
									|| crews.getCrewTripStart() == null
									|| crews.getActionType() == null) {
								LOG.error(
										"Crew details(Assigned On Date, Crew Trip Start, Crew Trip End and Action Type) not specified correctly. Dropping the Crew  Assignment:"
												+ "FlightNum = {}, "
												+ "Flight Date = {}, "
												+ "Crew ={}, ",
										new Object[] { flightNumber, flda,
												crews.getStaffno() });
								if (logLevel
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflString);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(EnumExceptionCodes.EMAND,
											irmtabRef, isLast, flightNumber,
											flda);
								}
								continue;
							}
							if (HpUfisUtils.isNullOrEmptyStr(crews
									.getCrewType())
									|| HpUfisUtils.isNullOrEmptyStr(crews
											.getOperatingGrade())
									|| HpUfisUtils.isNullOrEmptyStr(crews
											.getWorkType())) {
								LOG.error(
										"Crew Details are not specified correctly. Dropping the Crew Assignment Details:"
												+ "FlightNum = {}, "
												+ "Flight Date = {}, "
												+ "Crew ={}, ",
										new Object[] { flightNumber, flda,
												crews.getStaffno() });
								if (logLevel
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflString);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(EnumExceptionCodes.EMAND,
											irmtabRef, isLast, flightNumber,
											flda);
								}
								continue;
							}

							fltJobAssignObj = assignFlightData(fltJobAssignObj,
									crewsOnFlightAssign);

							if ((!staffFromDB.isEmpty())
									&& staffFromDB.keySet().contains(
											crews.getStaffno())
									&& (staffFromDB.get(crews.getStaffno())
											.getIdFlight() != BigDecimal.ZERO)) { // update
								// the
								// existing
								// recordstaffFromDB
								EntDbFltJobAssign recFromDB = staffFromDB
										.get(crews.getStaffno());
								oldCrewAssignment = recFromDB.getClone();
								recFromDB = assignFlightData(recFromDB,
										crewsOnFlightAssign);
								if (crews.getActionType().equals(
										ActionType.DELETE)) {
									LOG.warn(
											"Crew exist in FLT_JOB_ASSIGN. Deleting the Crew: "
													+ "FlightId = {}, "
													+ "Staff Number = {}, ",
											new Object[] {
													fltJobAssignObj
															.getIdFlight(),
													fltJobAssignObj
															.getStaffNumber() });
									recFromDB.setUpdatedDate(HpUfisCalendar
											.getCurrentUTCTime());
									recFromDB
											.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);
									// _fltJobAssign
									// .deleteCrewFltAssign(recFromDB);
									// Change on 29NOV2013
									/*
									 * crewAssignment = _fltJobAssign
									 * .getAssignedCrewIdByFlight(
									 * recFromDB.getIdFlight(),
									 * recFromDB.getStaffNumber()); if
									 * (crewAssignment != null) {
									 * crewAssignment.setRecStatus("X");
									 * currCrewAssignment = _fltJobAssign
									 * .merge(crewAssignment);
									 */
									recFromDB.setRecStatus("X");
									currCrewAssignment = _fltJobAssign
											.merge(recFromDB);
									if (currCrewAssignment != null) {
										/*
										 * sendJobAssignUpdateToCedaInJson(
										 * currCrewAssignment,
										 * oldCrewAssignment, HpUfisAppConstants
										 * .UfisASCommands.URT .name(), isLast);
										 * LOG.debug(
										 * "ACTS Job Assign Record change has beed communicated to Ceda."
										 * );
										 */
										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
											// send notification message to
											// topic
											clsBlUfisBCTopic
													.sendNotification(
															isLast.booleanValue(),
															HpUfisAppConstants.UfisASCommands.DRT,
															null,
															oldCrewAssignment,
															currCrewAssignment);
										}
										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
											// if topic not defined, send
											// notification to queue
											ufisQueueProducer
													.sendNotification(
															isLast,
															HpUfisAppConstants.UfisASCommands.DRT,
															null,
															oldCrewAssignment,
															currCrewAssignment);
										}
										LOG.debug("ACTS Job Assign Record change communicated to Ceda.");
									} else {
										LOG.error(
												"Failed on updating the record to DB. Crew Details(Staff Number): {}",
												crewAssignment.getStaffNumber());
									}
									// }
									continue;
								}

								recFromDB
										.setJobAssignDate(convertDateToUTC(crews
												.getAssignedOnDate()));
								recFromDB.setTripStart(crews.getCrewTripStart()
										.value().substring(0, 1));
								recFromDB.setTripEnd(crews.getCrewTripEnd()
										.value().substring(0, 1));
								// recFromDB.setStaffNumber(crews.getStaffno());
								recFromDB.setStaffFirstName(crews
										.getFirstName());
								recFromDB.setStaffLastName(crews.getLastName());
								if (HpUfisUtils.isNotEmptyStr(fltJobAssignObj
										.getStaffFirstName())
										&& HpUfisUtils
												.isNotEmptyStr(fltJobAssignObj
														.getStaffLastName())) {
									fltJobAssignObj
											.setStaffName(fltJobAssignObj
													.getStaffFirstName()
													.concat(" "
															+ fltJobAssignObj
																	.getStaffLastName()));
								}
								recFromDB.setStaffType(crews.getCrewType());
								recFromDB.setStaffOpGrade(crews
										.getOperatingGrade());
								recFromDB.setStaffWorkType(crews.getWorkType());
								recFromDB.setRecStatus(" ");
								recFromDB
										.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);

								recFromDB.setUpdatedDate(HpUfisCalendar
										.getCurrentUTCTime());

								currCrewAssignment = _fltJobAssign
										.merge(recFromDB);
								LOG.debug(
										"Updated the crew details into FLT_JOB_ASSIGN successfully.{}",
										(flightNumber + " " + crews
												.getStaffno()));
								if (currCrewAssignment != null) {
									/*
									 * sendJobAssignUpdateToCedaInJson(
									 * currCrewAssignment, null,
									 * HpUfisAppConstants.UfisASCommands.URT
									 * .name(), isLast); LOG.debug(
									 * "ACTS Job Assign Record update communicated to Ceda."
									 * );
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										clsBlUfisBCTopic
												.sendNotification(
														isLast.booleanValue(),
														HpUfisAppConstants.UfisASCommands.URT,
														null,
														oldCrewAssignment,
														currCrewAssignment);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														isLast,
														HpUfisAppConstants.UfisASCommands.URT,
														null,
														oldCrewAssignment,
														currCrewAssignment);
									}
									LOG.debug("ACTS Job A)ssign Record change communicated to Ceda.");
								} else {
									LOG.error(
											"Failed on updating the record to DB. Crew Details(Staff Number): {}",
											recFromDB.getStaffNumber());
								}

								if (idFlight != 0
										&& (recFromDB.getStaffWorkType()
												.equalsIgnoreCase("OP") || recFromDB
												.getStaffWorkType()
												.equalsIgnoreCase("HV"))) {
									String key = recFromDB.getStaffType() + ","
											+ recFromDB.getStaffWorkType();
									// LOG.debug("key:"+key);
									// LOG.debug("summaryByWorkType.containsKey(key):"+summaryByWorkType.containsKey(key));

									if (summaryByResType.isEmpty()
											|| !summaryByResType
													.containsKey(recFromDB
															.getStaffType())) {
										summaryByResType.put(
												recFromDB.getStaffType(),
												new Long(1));
										summaryByWorkType.put(key, new Long(1));
									} else if (summaryByResType
											.containsKey(recFromDB
													.getStaffType())
											&& !summaryByWorkType
													.containsKey(key)) {
										// LOG.debug("key:"+key);
										long count = summaryByResType.get(
												recFromDB.getStaffType())
												.longValue();
										summaryByResType.put(
												recFromDB.getStaffType(),
												new Long(count + 1));
										summaryByWorkType.put(key, new Long(1));
									} else {
										long count = summaryByResType.get(
												recFromDB.getStaffType())
												.longValue();
										summaryByResType.put(
												recFromDB.getStaffType(),
												new Long(count + 1));
										summaryByWorkType.put(key, new Long(
												summaryByWorkType.get(key)
														.longValue() + 1));
									}

								}

							} else {
								fltJobAssignObj.setId(UUID.randomUUID()
										.toString());
								fltJobAssignObj
										.setJobAssignDate(convertDateToUTC(crews
												.getAssignedOnDate()));
								fltJobAssignObj.setTripStart(crews
										.getCrewTripStart().value()
										.substring(0, 1));
								fltJobAssignObj.setTripEnd(crews
										.getCrewTripEnd().value()
										.substring(0, 1));
								fltJobAssignObj.setStaffNumber(crews
										.getStaffno());
								fltJobAssignObj.setStaffFirstName(crews
										.getFirstName());
								fltJobAssignObj.setStaffLastName(crews
										.getLastName());
								if (HpUfisUtils.isNotEmptyStr(fltJobAssignObj
										.getStaffFirstName())
										&& HpUfisUtils
												.isNotEmptyStr(fltJobAssignObj
														.getStaffLastName())) {
									fltJobAssignObj
											.setStaffName(fltJobAssignObj
													.getStaffFirstName()
													.concat(" "
															+ fltJobAssignObj
																	.getStaffLastName()));
								}
								fltJobAssignObj.setStaffType(crews
										.getCrewType());
								fltJobAssignObj.setStaffOpGrade(crews
										.getOperatingGrade());
								fltJobAssignObj.setStaffWorkType(crews
										.getWorkType());
								fltJobAssignObj.setRecStatus(" ");
								fltJobAssignObj.setCreatedDate(HpUfisCalendar
										.getCurrentUTCTime());
								fltJobAssignObj
										.setCreatedUser(HpEKConstants.ACTS_DATA_SOURCE);
								fltJobAssignObj
										.setDataSource(HpEKConstants.ACTS_DATA_SOURCE);

								if (crews.getActionType().equals(
										ActionType.DELETE)) {
									LOG.warn(
											"Crew doesn't exist in FLT_JOB_ASSIGN. Omitting the Crew: "
													+ "FlightNum = {}, "
													+ "Staff Number = {}, ",
											new Object[] {
													fltJobAssignObj
															.getIdFlight(),
													fltJobAssignObj
															.getStaffNumber() });
									if (logLevel
											.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
													.name())) {
										if (!msgLogged) {
											irmtabRef = _irmfacade.storeRawMsg(
													message, dtflString);
											msgLogged = Boolean.TRUE;
										}
									}
									if (irmtabRef > 0) {
										sendErrInfo(EnumExceptionCodes.ENREC,
												irmtabRef, isLast,
												flightNumber, flda);
									}
									continue;
								}
								currCrewAssignment = _fltJobAssign
										.merge(fltJobAssignObj);
								LOG.debug(
										"Inserted the crew details into FLT_JOB_ASSIGN successfully.{}",
										(flightNumber + " " + crews
												.getStaffno()));
								if (currCrewAssignment != null) {
									/*
									 * sendJobAssignUpdateToCedaInJson(
									 * currCrewAssignment, null,
									 * HpUfisAppConstants.UfisASCommands.IRT
									 * .name(), isLast); LOG.debug(
									 * "ACTS Job Assign Record insert communicated to Ceda."
									 * ); }
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										clsBlUfisBCTopic
												.sendNotification(
														isLast.booleanValue(),
														HpUfisAppConstants.UfisASCommands.IRT,
														null, null,
														currCrewAssignment);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														isLast,
														HpUfisAppConstants.UfisASCommands.IRT,
														null, null,
														currCrewAssignment);
									}
									LOG.debug("ACTS Job Assign Record change communicated to Ceda.");
								} else {
									LOG.error(
											"Failed on updating the record to DB. Crew Details(Staff Number): {}",
											fltJobAssignObj.getStaffNumber());
								}

								if (idFlight != 0
										&& (fltJobAssignObj.getStaffWorkType()
												.equalsIgnoreCase("OP") || fltJobAssignObj
												.getStaffWorkType()
												.equalsIgnoreCase("HV"))) {
									String key = fltJobAssignObj.getStaffType()
											+ ","
											+ fltJobAssignObj
													.getStaffWorkType();

									if (summaryByResType.isEmpty()
											|| !summaryByResType
													.containsKey(fltJobAssignObj
															.getStaffType())) {
										summaryByResType.put(
												fltJobAssignObj.getStaffType(),
												new Long(1));
										summaryByWorkType.put(key, new Long(1));
									} else if (summaryByResType
											.containsKey(fltJobAssignObj
													.getStaffType())
											&& !summaryByWorkType
													.containsKey(key)) {

										long count = summaryByResType.get(
												fltJobAssignObj.getStaffType())
												.longValue();
										summaryByResType.put(
												fltJobAssignObj.getStaffType(),
												new Long(count + 1));
										summaryByWorkType.put(key, new Long(1));
									} else {
										long count = summaryByResType.get(
												fltJobAssignObj.getStaffType())
												.longValue();
										summaryByResType.put(
												fltJobAssignObj.getStaffType(),
												new Long(count + 1));
										summaryByWorkType.put(key, new Long(
												summaryByWorkType.get(key)
														.longValue() + 1));
									}
								}
							}

						}
						if (idFlight != 0) {
							// Change on 29NOV2013
							List<EntDbFltJobSummary> fltJobSummaryList = null;
							// Change on 29NOV2013
							List<EntDbFltJobSummary> totalFltJobSummaryList = null;
							long crewTypeSum = 0;
							totalFltJobSummaryList = _fltJobSummary
									.getFltJobSummary(idFlight);
							// Delete Summary by Assigned Crew Type
							if (HpUfisUtils.isNotEmptyStr(crewType)) {
								fltJobSummaryList = _fltJobSummary
										.getFltJobSummaryByCrewType(idFlight,
												crewType);
							}// No crews Assigned Some error in Crew Format
							else {
								LOG.error("CrewType(Mandatory) is null . Not considered a scenario.Dropping message.");
								if (logLevel
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflString);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(EnumExceptionCodes.EMAND,
											irmtabRef, isLast, flightNumber,
											flda);
								}
								return;
							}
							if (fltJobSummaryList == null
									|| fltJobSummaryList.isEmpty()) {
								LOG.info(
										"No summary exist for ID_FLIGHT:{}and Crew Type:{} ",
										idFlight, crewType);
							}
							// LOG.info("fltJobSummaryList.size:"+fltJobSummaryList.size());
							if (fltJobSummaryList != null
									&& !fltJobSummaryList.isEmpty()) {
								// Change on 29NOV2013
								// delete Summary Record by IdFlight and
								// CrewType
								_fltJobSummary.deleteFltJobSummaryByCrewType(
										idFlight, crewType);

								LOG.info(
										"Deleting the existing summary records from FLT_JOB_SUMMARY for IdFlight  = {}, "
												+ "Crew Type = {}, ",
										new Object[] { idFlight, crewType });
								// Notification
								for (EntDbFltJobSummary delRec : fltJobSummaryList) {

									if (delRec != null) {
										if (HpUfisUtils.isNotEmptyStr(delRec
												.getResourceTypeCode())
												&& HpUfisUtils
														.isNullOrEmptyStr(delRec
																.getStaffWorkType())) {
											crewTypeSum = delRec
													.getTotalAssigned();
										}
										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
											// send notification message to
											// topic
											clsBlUfisBCTopic
													.sendNotification(
															isLast.booleanValue(),
															HpUfisAppConstants.UfisASCommands.DRT,
															null, delRec,
															delRec);
										}
										if (HpUfisUtils
												.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
											// send notification to queue
											ufisQueueProducer
													.sendNotification(
															isLast,
															HpUfisAppConstants.UfisASCommands.DRT,
															null, delRec,
															delRec);
										}
										LOG.debug("ACTS Job Summary Record delete communicated to Ceda.");
									} else {
										LOG.error(
												"Failed on deleting the summary record to DB. Details(Record can be null): {}",
												delRec);
									}
								}

							}

							// LOG.info("summaryByWorkType.size:"+summaryByWorkType.size());
							// LOG.info("summaryByResType.size:"+summaryByResType.size());
							String resurce_subType = null;
							String resource_type = null;

							if (!summaryByWorkType.isEmpty()) {
								StringTokenizer st;
								String resTypeCode;
								// int size = summaryByWorkType.keySet().size();
								for (String keystr : summaryByWorkType.keySet()) {
									// isLast = Boolean.FALSE;
									// if (--size == 0) {
									// isLast = Boolean.TRUE;
									// }
									st = new StringTokenizer(keystr, ",");
									resTypeCode = st.nextToken();
									if (!staffTypeMap.isEmpty()
											&& staffTypeMap
													.containsKey(resTypeCode)) {
										resurce_subType = staffTypeMap.get(
												resTypeCode).get(0);
										resource_type = staffTypeMap.get(
												resTypeCode).get(1);
									} else {
										LOG.warn(
												"Resource Type Code doesn't match with the Master Data(MD_STAFF_TYPE).Omiting this record in summary calc."
														+ "FlightNum = {}, "
														+ "Flight Date = {}, "
														+ "Resource Type Code = {} , ",
												new Object[] {
														crewsOnFlightAssign
																.getFlightId()
																.getFltNum(),
														crewsOnFlightAssign
																.getFlightId()
																.getFltDate(),
														resTypeCode });
										if (logLevel
												.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
														.name())) {
											if (!msgLogged) {
												irmtabRef = _irmfacade
														.storeRawMsg(message,
																dtflString);
												msgLogged = Boolean.TRUE;
											}
										}
										if (irmtabRef > 0) {
											sendErrInfo(
													EnumExceptionCodes.ENOMD,
													irmtabRef, isLast,
													flightNumber, flda);
										}
										// resurce_subType = "0";
										// resource_type = "0";
										continue;
									}
									populateFltJobSummary(idFlight,
											resTypeCode, resurce_subType,
											resource_type, st.nextToken(),
											summaryByWorkType.get(keystr)
													.longValue(), recStatus,
											isLast);
								}
							}

							if (!summaryByResType.isEmpty()) {
								// int size = summaryByResType.keySet().size();
								for (String keyStr : summaryByResType.keySet()) {
									// isLast = Boolean.FALSE;
									// if (--size == 0) {
									// isLast = Boolean.TRUE;
									// }
									if (!staffTypeMap.isEmpty()
											&& staffTypeMap.containsKey(keyStr)) {
										resurce_subType = staffTypeMap.get(
												keyStr).get(0);
										resource_type = staffTypeMap
												.get(keyStr).get(1);
									} else {
										LOG.warn(
												"Resource Type Code doesn't match with the Master Data(MD_STAFF_TYPE).Omiting this record in summary calc."
														+ "FlightNum = {}, "
														+ "Flight Date = {}, "
														+ "Resource Type Code = {} , ",
												new Object[] {
														crewsOnFlightAssign
																.getFlightId()
																.getFltNum(),
														crewsOnFlightAssign
																.getFlightId()
																.getFltDate(),
														keyStr });
										if (logLevel
												.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
														.name())) {
											if (!msgLogged) {
												irmtabRef = _irmfacade
														.storeRawMsg(message,
																dtflString);
												msgLogged = Boolean.TRUE;
											}
										}
										if (irmtabRef > 0) {
											sendErrInfo(
													EnumExceptionCodes.ENOMD,
													irmtabRef, isLast,
													flightNumber, flda);
										}
										continue;
										// resurce_subType = "0";
										// resource_type = "0";
									}
									populateFltJobSummary(idFlight, keyStr,
											resurce_subType, resource_type, "",
											summaryByResType.get(keyStr)
													.longValue(), recStatus,
											isLast);
									// totalCount=totalCount+summaryByResType.get(keyStr).longValue();
									if (resTypeMap.isEmpty()
											|| !resTypeMap
													.containsKey(resource_type)) {
										resTypeMap.put(resource_type, new Long(
												summaryByResType.get(keyStr)));
									} else {
										resTypeMap
												.put(resource_type,
														(resTypeMap.get(
																resource_type)
																.longValue() + summaryByResType
																.get(keyStr)
																.longValue()));
									}
								}
							}

							// Case when Summary exists for multiple Crew Types

							// Insert the Summary records and update Sum count

							if (totalFltJobSummaryList != null
									&& !totalFltJobSummaryList.isEmpty()) {
								for (String resType : resTypeMap.keySet()) {
									EntDbFltJobSummary oldSumRec = null;
									EntDbFltJobSummary summRec = _fltJobSummary
											.getFltJobSummaryRecByCrewType(idFlight);
									/*LOG.info("Record found : "
											+ summRec.getTotalAssigned());*/
									if(summRec!=null){
									oldSumRec = summRec.getClone();
									summRec.setTotalAssigned((summRec
											.getTotalAssigned() + (resTypeMap
											.get(resType) - crewTypeSum)));
									summRec.setUpdatedDate(HpUfisCalendar
											.getCurrentUTCTime());
									summRec.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);
									_fltJobSummary
											.updateFlightJobSummary(summRec);
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										clsBlUfisBCTopic
												.sendNotification(
														isLast.booleanValue(),
														HpUfisAppConstants.UfisASCommands.URT,
														null, oldSumRec,
														summRec);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														isLast,
														HpUfisAppConstants.UfisASCommands.URT,
														null, oldSumRec,
														summRec);
									}
									LOG.debug("ACTS Job Summary Record update communicated to Ceda.");
								}
								}
							} else {
								// LOG.info("resTypeMap.size:"+resTypeMap.size());
								if (!resTypeMap.isEmpty()) {
									// int size = resTypeMap.keySet().size();
									for (String resType : resTypeMap.keySet()) {
										// isLast = Boolean.FALSE;
										// if (--size == 0) {
										// isLast = Boolean.TRUE;
										// }
										populateFltJobSummary(idFlight, "", "",
												resType, "",
												resTypeMap.get(resType),
												recStatus, isLast);
									}

								}
							}
						}

					} else if (idFlight != 0) {
						LOG.warn("No Crews assigned to the flight"
								+ "FlightNum = {}, " + "Flight Date = {}, ",
								new Object[] {
										crewsOnFlightAssign.getFlightId()
												.getFltNum(),
										crewsOnFlightAssign.getFlightId()
												.getFltDate() });

						List<EntDbFltJobAssign> crewsFlight = _fltJobAssign
								.getAssignedCrewsByFltId(idFlight);
						if (crewsFlight != null && !crewsFlight.isEmpty()) {
							for (EntDbFltJobAssign crewFlight : crewsFlight) {
								/*
								 * isLast = Boolean.FALSE; if
								 * (crewFlight.equals(
								 * crewsFlight.get(crewsFlight .size() - 1))) {
								 * isLast = Boolean.TRUE; }
								 */
								// Change on 29NOV2013
								/*
								 * crewAssignment = _fltJobAssign
								 * .getAssignedCrewIdByFlight(
								 * crewFlight.getIdFlight(),
								 * crewFlight.getStaffNumber());
								 * oldCrewAssignment = crewAssignment; if
								 * (crewAssignment != null) {
								 * crewAssignment.setRecStatus("X");
								 * crewAssignment .setUpdatedDate(HpUfisCalendar
								 * .getCurrentUTCTime()); crewAssignment
								 * .setUpdatedUser
								 * (HpEKConstants.ACTS_DATA_SOURCE);
								 * currCrewAssignment = _fltJobAssign
								 * .merge(crewAssignment);
								 */
								oldCrewAssignment = crewFlight.getClone();
								crewFlight.setRecStatus("X");
								crewFlight.setUpdatedDate(HpUfisCalendar
										.getCurrentUTCTime());
								crewFlight
										.setUpdatedUser(HpEKConstants.ACTS_DATA_SOURCE);
								currCrewAssignment = _fltJobAssign
										.merge(crewFlight);
								if (currCrewAssignment != null) {
									/*
									 * sendJobAssignUpdateToCedaInJson(
									 * currCrewAssignment, oldCrewAssignment,
									 * HpUfisAppConstants.UfisASCommands.URT
									 * .name(), isLast); LOG.debug(
									 * "ACTS Job Assign Record update communicated to Ceda."
									 * ); }
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to
										// topic
										clsBlUfisBCTopic
												.sendNotification(
														isLast.booleanValue(),
														HpUfisAppConstants.UfisASCommands.DRT,
														null,
														oldCrewAssignment,
														currCrewAssignment);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														isLast,
														HpUfisAppConstants.UfisASCommands.DRT,
														null,
														oldCrewAssignment,
														currCrewAssignment);
									}
									LOG.debug("ACTS Job Assign Record change communicated to Ceda.");
								} else {
									LOG.error(
											"Failed on updating the record to DB. Crew Details(Staff Number): {}",
											crewAssignment.getStaffNumber());
								}

								// }
							}
						}

						// deleting summary records
						List<EntDbFltJobSummary> fltJobSummaryList = _fltJobSummary
								.getFltJobSummary(idFlight);
						if (fltJobSummaryList != null
								&& !fltJobSummaryList.isEmpty()) {
							_fltJobSummary.deleteFltJobSummary(idFlight);
							for (EntDbFltJobSummary delRec : fltJobSummaryList) {
								/*
								 * isLast = Boolean.FALSE; if
								 * (delRec.equals(fltJobSummaryList
								 * .get(fltJobSummaryList.size() - 1))) { isLast
								 * = Boolean.TRUE; }
								 */
								if (delRec != null) {
									/*
									 * sendJobAssignSummUpdateToCedaInJson(delRec
									 * , null,
									 * HpUfisAppConstants.UfisASCommands.DRT
									 * .name(), isLast); LOG.debug(
									 * "ACTS Job Summary Record delete communicated to Ceda."
									 * ); }
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										clsBlUfisBCTopic
												.sendNotification(
														isLast.booleanValue(),
														HpUfisAppConstants.UfisASCommands.DRT,
														null, delRec, delRec);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														isLast,
														HpUfisAppConstants.UfisASCommands.DRT,
														null, delRec, delRec);
									}
									LOG.debug("ACTS Job Summary Record delete communicated to Ceda.");
								} else {
									LOG.error(
											"Failed on deleting the summary record to DB. Details(The record can be null): {}",
											delRec);
								}
							}
						}
					} else {
						LOG.error(
								"Flight ID is not avilable from FLT_DAILY and Crews are not specified.Dropping the message Details:"
										+ "FlightNum = {}, "
										+ "Flight Date = {}, ", new Object[] {
										crewsOnFlightAssign.getFlightId()
												.getFltNum(),
										crewsOnFlightAssign.getFlightId()
												.getFltDate() });
						if (logLevel
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(message,
										dtflString);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(EnumExceptionCodes.ENOFL, irmtabRef,
									isLast, flightNumber, flda);
						}
						return;
					}
				}

			}
		} catch (Exception e) {
			LOG.error("Exception :{}", e);
		}

	}

	private void populateFltJobSummary(long idFlight, String resTypeCode,
			String resurce_subType, String resource_type, String workType,
			long count, String recStatus, Boolean isLast) {
		EntDbFltJobSummary jobSummary = null;
		try {
			EntDbFltJobSummary fltJobSumRec = new EntDbFltJobSummary();
			fltJobSumRec.setIdFlight(idFlight);
			fltJobSumRec.setResourceType(resource_type);
			fltJobSumRec.setAssignDate(HpUfisCalendar.getCurrentUTCTime()); // ?????
																			// need
																			// to
			// clarify...
			fltJobSumRec.setResourceSubtype(resurce_subType);
			fltJobSumRec.setResourceTypeCode(resTypeCode);
			if (!workType.isEmpty()) {
				fltJobSumRec.setStaffWorkType(workType);
			}
			fltJobSumRec.setTotalAssigned(count);
			fltJobSumRec.setRecStatus(" ");
			jobSummary = _fltJobSummary.saveFltJobSummary(fltJobSumRec);
			LOG.info("Inserted record into fltJobSummary successfully");
			if (jobSummary != null) {
				/*
				 * sendJobAssignSummUpdateToCedaInJson(jobSummary, null,
				 * HpUfisAppConstants.UfisASCommands.IRT.name(), isLast);
				 * LOG.debug
				 * ("ACTS Job Summary Record insert communicated to Ceda."); }
				 */
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
					// send notification message to topic
					clsBlUfisBCTopic.sendNotification(isLast.booleanValue(),
							HpUfisAppConstants.UfisASCommands.IRT, null,
							jobSummary, jobSummary);
				}
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
					// if topic not defined, send notification to queue
					ufisQueueProducer.sendNotification(isLast,
							HpUfisAppConstants.UfisASCommands.IRT, null,
							jobSummary, jobSummary);
				}
				LOG.debug("ACTS Job Summary Record insert communicated to Ceda.");
			} else {
				LOG.error(
						"Failed to insert the summary record to DB. Details(Summary record can be null): {}",
						jobSummary);
			}
		} catch (Exception e) {
			LOG.error("Exception :{}", e);
		}
	}

	private EntDbFltJobAssign assignFlightData(
			EntDbFltJobAssign fltJobAssignObj, CrewOnFlight crewsOnFlightAssign) {
		if (crewsOnFlightAssign.getAudit() == null) {
			LOG.debug("Audit Details are not defined. " + "FlightNum = {}, "
					+ "Flight Date = {}, ", new Object[] {
					crewsOnFlightAssign.getFlightId().getFltNum(),
					crewsOnFlightAssign.getFlightId().getFltDate() });
		} else {
			fltJobAssignObj.setChangeReason(crewsOnFlightAssign.getAudit()
					.getTransComment());
			fltJobAssignObj.setChangeReasonCode(crewsOnFlightAssign.getAudit()
					.getReasonCode());
			try {
				if (crewsOnFlightAssign.getAudit().getPostedDateTime() != null) {
					fltJobAssignObj
							.setMsgSendDate(convertDateToUTC(crewsOnFlightAssign
									.getAudit().getPostedDateTime()));
				}
				if (crewsOnFlightAssign.getAudit().getTransDateTime() != null) {
					fltJobAssignObj
							.setTransactDate(convertDateToUTC(crewsOnFlightAssign
									.getAudit().getTransDateTime()));
				}
			} catch (ParseException e) {
				LOG.error(
						"Error on parsing the getPostedDateTime and getTransDateTime. Exception: {}",
						e);
			}
			if (crewsOnFlightAssign.getAudit().getTransUserId() != null) {
				fltJobAssignObj.setTransactUser(crewsOnFlightAssign.getAudit()
						.getTransUserId());
			}

		}
		return fltJobAssignObj;
	}

	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef,
			Boolean isLast, String flightNumber, String flda)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString().concat(
				" Flight Number: "
						+ flightNumber.concat("  Flight Date:" + flda)));
		dataList.add(dtflString);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, isLast, dtflString);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader.");

	}

	private Date convertDateToUTC(XMLGregorianCalendar xmlDate)
			throws ParseException {

		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = xmlDate.toGregorianCalendar().getTime();
		return formatter.parse(df.format(utcDate));
	}

	private String chgXMLGregorianCalendarToDateString(
			XMLGregorianCalendar fltDate) {
		if (fltDate == null) {
			return null;
		}
		try {
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			// df.setTimeZone(TimeZone.getTimeZone("UTC"));
			ufisCalendar.setTime(fltDate);
			return fldaDF.format(ufisCalendar.getTime());
			// return ufisCalendar.getCedaDateString();
		} catch (Exception e) {
			LOG.error("ERROR!!!: {}" + e);
		}
		return null;
	}

	private Date chgXMLGregorianCalendarToDate(XMLGregorianCalendar fltDate) {
		if (fltDate == null) {
			return null;
		}
		try {
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			// df.setTimeZone(TimeZone.getTimeZone("UTC"));
			ufisCalendar.setTime(fltDate);
			return ufisCalendar.getTime();
			// return ufisCalendar.getCedaDateString();
		} catch (Exception e) {
			LOG.error("ERROR!!!: {}" + e);
		}
		return null;
	}

	/*
	 * private void sendJobAssignUpdateToCedaInJson( EntDbFltJobAssign
	 * flightJobAssign, EntDbFltJobAssign oldFltJobAssign, String cmd, Boolean
	 * isLast) throws IOException, JsonGenerationException, JsonMappingException
	 * { EntUfisMsgDTO msgDTO = new EntUfisMsgDTO(); String urno =
	 * flightJobAssign.getIdFlight().toString(); if (isLast) {
	 * header.setBcnum(-1); } else { header.setBcnum(0); } // get the HEAD info
	 * List<String> idFlightList = new ArrayList<>(); idFlightList.add(urno);
	 * header.setIdFlight(idFlightList);
	 * 
	 * // get the BODY info EntUfisMsgBody body = new EntUfisMsgBody();
	 * 
	 * List<Object> dataList = new ArrayList<>(); dataList.add(urno); if
	 * (flightJobAssign.getJobAssignDate() != null) {
	 * dataList.add(df.format(flightJobAssign.getJobAssignDate())); } else {
	 * dataList.add(flightJobAssign.getJobAssignDate()); }
	 * dataList.add(flightJobAssign.getTripStart());
	 * dataList.add(flightJobAssign.getTripEnd());
	 * dataList.add(flightJobAssign.getStaffNumber());
	 * dataList.add(flightJobAssign.getStaffFirstName());
	 * dataList.add(flightJobAssign.getStaffLastName());
	 * dataList.add(flightJobAssign.getStaffType());
	 * dataList.add(flightJobAssign.getStaffOpGrade());
	 * dataList.add(flightJobAssign.getStaffWorkType());
	 * dataList.add(flightJobAssign.getChangeReason());
	 * dataList.add(flightJobAssign.getChangeReasonCode()); if
	 * (flightJobAssign.getMsgSendDate() != null) {
	 * dataList.add(df.format(flightJobAssign.getMsgSendDate())); } else {
	 * dataList.add(flightJobAssign.getMsgSendDate()); } if
	 * (flightJobAssign.getTransactDate() != null) {
	 * dataList.add(df.format(flightJobAssign.getTransactDate())); } else {
	 * dataList.add(flightJobAssign.getTransactDate()); }
	 * dataList.add(flightJobAssign.getTransactUser());
	 * dataList.add(flightJobAssign.getRecStatus()); if
	 * (flightJobAssign.getUpdatedDate() != null) {
	 * dataList.add(df.format(flightJobAssign.getUpdatedDate())); } else {
	 * dataList.add(flightJobAssign.getUpdatedDate()); }
	 * dataList.add(flightJobAssign.getUpdatedUser());
	 * 
	 * List<String> oldList = new ArrayList<>(); if
	 * (UfisASCommands.URT.toString().equals(cmd) && oldFltJobAssign != null) {
	 * oldList.add(oldFltJobAssign.getIdFlight().toString()); if
	 * (oldFltJobAssign.getJobAssignDate() != null) {
	 * oldList.add(df.format(oldFltJobAssign.getJobAssignDate())); } else {
	 * dataList.add(oldFltJobAssign.getJobAssignDate()); }
	 * oldList.add(oldFltJobAssign.getTripStart());
	 * oldList.add(oldFltJobAssign.getTripEnd());
	 * oldList.add(oldFltJobAssign.getStaffNumber());
	 * oldList.add(oldFltJobAssign.getStaffFirstName());
	 * oldList.add(oldFltJobAssign.getStaffLastName());
	 * oldList.add(oldFltJobAssign.getStaffType());
	 * oldList.add(oldFltJobAssign.getStaffOpGrade());
	 * oldList.add(oldFltJobAssign.getStaffWorkType());
	 * oldList.add(oldFltJobAssign.getChangeReason());
	 * oldList.add(oldFltJobAssign.getChangeReasonCode()); if
	 * (oldFltJobAssign.getMsgSendDate() != null) {
	 * oldList.add(df.format(oldFltJobAssign.getMsgSendDate())); } else {
	 * dataList.add(oldFltJobAssign.getMsgSendDate()); } if
	 * (oldFltJobAssign.getTransactDate() != null) {
	 * oldList.add(df.format(oldFltJobAssign.getTransactDate())); } else {
	 * dataList.add(oldFltJobAssign.getTransactDate()); }
	 * oldList.add(oldFltJobAssign.getTransactUser());
	 * oldList.add(oldFltJobAssign.getRecStatus()); if
	 * (oldFltJobAssign.getUpdatedDate() != null) {
	 * oldList.add(df.format(oldFltJobAssign.getUpdatedDate())); } else {
	 * dataList.add(oldFltJobAssign.getUpdatedDate()); }
	 * oldList.add(oldFltJobAssign.getUpdatedUser()); }
	 * 
	 * List<String> idList = new ArrayList<>();
	 * idList.add(flightJobAssign.getId());
	 * 
	 * List<EntUfisMsgACT> listAction = new ArrayList<>(); EntUfisMsgACT action
	 * = new EntUfisMsgACT(); action.setCmd(cmd);
	 * action.setTab("FLT_JOB_ASSIGN"); action.setFld(fldList);
	 * action.setData(dataList); action.setId(idList); action.setSel(urno);
	 * 
	 * listAction.add(action); body.setActs(listAction);
	 * 
	 * msgDTO.setHead(header); msgDTO.setBody(body);
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); String msg =
	 * mapper.writeValueAsString(msgDTO); clsBlUfisBCTopic.sendMessage(msg);
	 * LOG.debug(
	 * "Sent Update JobAssign  Notification from ACTS Job Assign to UMD :\n{}",
	 * msg); }
	 */

	/*
	 * private void sendJobAssignSummUpdateToCedaInJson( EntDbFltJobSummary
	 * flightJobSumm, EntDbFltJobSummary oldFltJobSumm, String cmd, Boolean
	 * isLast) throws IOException, JsonGenerationException, JsonMappingException
	 * { EntUfisMsgDTO msgDTO = new EntUfisMsgDTO(); String urno = new
	 * Long(flightJobSumm.getIdFlight()).toString(); if (isLast) {
	 * header.setBcnum(-1); } else { header.setBcnum(0); } // get the HEAD info
	 * List<String> idFlightList = new ArrayList<>(); idFlightList.add(urno);
	 * header.setIdFlight(idFlightList);
	 * 
	 * // get the BODY info EntUfisMsgBody body = new EntUfisMsgBody();
	 * 
	 * List<Object> dataList = new ArrayList<>(); dataList.add(urno);
	 * dataList.add(flightJobSumm.getResourceType());
	 * dataList.add(flightJobSumm.getResourceSubtype());
	 * dataList.add(flightJobSumm.getResourceTypeCode());
	 * dataList.add(flightJobSumm.getStaffOpGrade());
	 * dataList.add(flightJobSumm.getStaffWorkType()); dataList.add(new
	 * Long(flightJobSumm.getTotalAssigned()).toString());
	 * dataList.add(df.format(flightJobSumm.getAssignDate()));
	 * dataList.add(flightJobSumm.getRecStatus());
	 * 
	 * List<String> oldList = new ArrayList<>(); if
	 * (UfisASCommands.URT.toString().equals(cmd) && oldFltJobSumm != null) {
	 * oldList.add(urno); oldList.add(oldFltJobSumm.getResourceType());
	 * oldList.add(oldFltJobSumm.getResourceSubtype());
	 * oldList.add(oldFltJobSumm.getResourceTypeCode());
	 * oldList.add(oldFltJobSumm.getStaffOpGrade());
	 * oldList.add(oldFltJobSumm.getStaffWorkType()); oldList.add(new
	 * Long(oldFltJobSumm.getTotalAssigned()).toString());
	 * oldList.add(df.format(oldFltJobSumm.getAssignDate()));
	 * oldList.add(oldFltJobSumm.getRecStatus()); }
	 * 
	 * List<String> idList = new ArrayList<>();
	 * idList.add(flightJobSumm.getId());
	 * 
	 * List<EntUfisMsgACT> listAction = new ArrayList<>(); EntUfisMsgACT action
	 * = new EntUfisMsgACT(); action.setCmd(cmd);
	 * action.setTab("FLT_JOB_SUMMARY"); action.setFld(fldList);
	 * action.setData(dataList); action.setId(idList); action.setSel(urno);
	 * 
	 * listAction.add(action); body.setActs(listAction);
	 * 
	 * msgDTO.setHead(header); msgDTO.setBody(body);
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); String msg =
	 * mapper.writeValueAsString(msgDTO); clsBlUfisBCTopic.sendMessage(msg);
	 * LOG.debug(
	 * "Sent Update JobAssignSummary  Notification from ACTS Flt Job Summary to UMD :\n{}"
	 * , msg); }
	 */

	public CrewOnFlight getCrewsOnFlightData() {
		return crewsOnFlight;
	}

	public void setCrewsOnFlightData(CrewOnFlight crewsOnFlight) {
		this.crewsOnFlight = crewsOnFlight;
	}

	public String getReturnXML() {
		return _returnXML;
	}

	public void setReturnXML(String _returnXML) {
		this._returnXML = _returnXML;
	}

	public String validateMessage(String msg) {
		String c = "N";

		if (msg.toUpperCase().equals("YES")) {
			c = "Y";
		} else if (msg.toUpperCase().equals("NO")) {
			c = "N";
		}
		return c;
	}

}
