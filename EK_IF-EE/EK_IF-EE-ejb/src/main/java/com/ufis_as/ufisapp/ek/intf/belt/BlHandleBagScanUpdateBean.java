package com.ufis_as.ufisapp.ek.intf.belt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagMove;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IDlLoadBagMoveUpdateLocal;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

@Stateless(mappedName = "BlHandleBagScanUpdateBean")
public class BlHandleBagScanUpdateBean {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleBagScanUpdateBean.class);

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private IDlLoadBagMoveUpdateLocal _loadBagMoveUpdate;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;

	List<String> fldList = new ArrayList<String>();
	// List<Object> oldDataList = new ArrayList<Object>();
	List<Object> dataList = new ArrayList<Object>();
	List<EntDbLoadBagMove> bagMovesList = new ArrayList<EntDbLoadBagMove>();
	EntDbLoadBagMove oldRec = null;
	EntDbLoadBagMove newRec = null;

	@PreDestroy
	private void initialClear() {
		bagMovesList.clear();
		dataList.clear();
		fldList.clear();
	}
	
	public void handleBagUpdate(EntUfisMsgDTO msgData) {
		String idLoadBag = null;
		String idFlight = null;
		String bagTag = null;
		String cmd = null;
		long start = HpUfisCalendar.getCurrentUTCTime().getTime();
		if(msgData.getHead().getIdFlight()!=null && !msgData.getHead().getIdFlight().isEmpty()){
		idFlight = msgData.getHead().getIdFlight().get(0);
		}
		if (idFlight == null || idFlight.isEmpty() || idFlight.trim().isEmpty()) {
			LOG.info("Flight ID is not specified.Invalid Message");
			return;
		}
		// assuming atleast only one ID_FLIGHT in the list
		if (idFlight.equalsIgnoreCase("0")) {
			LOG.info("Flight Id is either NULL or ZERO.");
			return;
		}
		if (msgData.getBody().getActs().get(0) == null) {
			LOG.info("ACTS section is NULL.INVALID MESSAGE.");
			return;
		}
		fldList = msgData.getBody().getActs().get(0).getFld();
		if (fldList == null || fldList.isEmpty()) {
			LOG.info("FIELD section is NULL/EMPTY.INVALID MESSAGE.");
			return;
		}
		try {
			// idFlight=new BigDecimal(idFlightList.get(0));
			dataList = msgData.getBody().getActs().get(0).getData();
			idLoadBag = msgData.getBody().getActs().get(0).getId().get(0);
			// get idFlight,bagTag and id
			cmd = msgData.getBody().getActs().get(0).getCmd();
			if (cmd.equalsIgnoreCase("URT")) {
				// Ignoring the URT messages.
				LOG.info(
						"Total time to process the Bag Update message: {} ms ",
						(HpUfisCalendar.getCurrentUTCTime().getTime() - start));
				return;
			}
			if (cmd.equalsIgnoreCase("DRT")) {
				if (idLoadBag != null) {
					start = HpUfisCalendar.getCurrentUTCTime().getTime();
					bagMovesList = _loadBagMoveUpdate
							.getBagMoveByIdLoadBag(idLoadBag);
					LOG.info(
							"Total time to read from LOAD_BAG_MOVE: {} ms ",
							(HpUfisCalendar.getCurrentUTCTime().getTime() - start));
					if (bagMovesList != null && !bagMovesList.isEmpty()) {
						for (EntDbLoadBagMove bagMoveRec : bagMovesList) {
							if (bagMoveRec != null) {
								oldRec = bagMoveRec.getClone();
								bagMoveRec.setRecStatus("X");
								bagMoveRec.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
								bagMoveRec.setUpdatedUser(HpEKConstants.BELT_SOURCE);
								newRec = _loadBagMoveUpdate
										.updateLoadedBagMove(bagMoveRec);
								if (newRec != null) {
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										ufisTopicProducer
												.sendNotification(
														true,
														HpUfisAppConstants.UfisASCommands.DRT,
														idFlight, oldRec,
														newRec);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer
												.sendNotification(
														true,
														HpUfisAppConstants.UfisASCommands.DRT,
														idFlight, oldRec,
														newRec);
									}
									LOG.debug("Bag Move Update has beed communicated to Ceda.");
								}
							}
						}
					}
				}
				LOG.info(
						"Total time to process the Bag Update message: {} ms ",
						(HpUfisCalendar.getCurrentUTCTime().getTime() - start));
				return;
			}
			for (String fld : fldList) {
				if (fld.equalsIgnoreCase("BAG_TAG")) {
					bagTag = dataList.get(fldList.indexOf(fld)) != null ? dataList
							.get(fldList.indexOf(fld)).toString() : null;
					break;
				}
			}
			LOG.info("idFlight:{} , bagTag:{} ,idLoadBag:{} ", idFlight,
					bagTag, idLoadBag);
			if (idFlight != null && bagTag != null && idLoadBag != null
					&& !idFlight.equalsIgnoreCase("0")
					&& !bagTag.equalsIgnoreCase("0")) {
				start = HpUfisCalendar.getCurrentUTCTime().getTime();
				bagMovesList = _loadBagMoveUpdate.getBagMoveDetailsById(
						new BigDecimal(idFlight), bagTag, idLoadBag);
				LOG.info("Total time to read from LOAD_BAG_MOVE: {} ms ",
						(HpUfisCalendar.getCurrentUTCTime().getTime() - start));
				if (bagMovesList != null && !bagMovesList.isEmpty()) {
					for (EntDbLoadBagMove bagMoveRec : bagMovesList) {
						if (bagMoveRec != null) {
							oldRec = bagMoveRec.getClone();
							bagMoveRec.setIdLoadBag(idLoadBag);
							bagMoveRec.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
							bagMoveRec.setUpdatedUser(HpEKConstants.BELT_SOURCE);
							newRec = _loadBagMoveUpdate
									.updateLoadedBagMove(bagMoveRec);
							if (newRec != null) {
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
									// send notification message to topic
									ufisTopicProducer
											.sendNotification(
													true,
													HpUfisAppConstants.UfisASCommands.URT,
													idFlight, oldRec, newRec);
								}
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
									// if topic not defined, send notification
									// to queue
									ufisQueueProducer
											.sendNotification(
													true,
													HpUfisAppConstants.UfisASCommands.URT,
													idFlight, oldRec, newRec);
								}
								LOG.debug("Bag Move Update has beed communicated to Ceda.");
							}
						}
					}
				}
				else
				{
					LOG.warn("No Record found from Load Bag Move for IdFlight: "+idFlight+" , bagTag: "+bagTag+"  , idLoadBag: "+idLoadBag);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception :{}", e);
		}

		LOG.info("Total time to process the Bag Update message: {} ms ",
				(HpUfisCalendar.getCurrentUTCTime().getTime() - start));

	}

}
