package com.ufis_as.ufisapp.ek.intf.belt;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.belt.bagDetails.BagDetailsType;
import ek.belt.bagDetails.MsgType;
//import src.main.java.com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
//

/**
 * Session Bean implementation class BlHandleBeltBean
 */
@Stateless(mappedName = "BlHandleBeltBagCheckInBean")
@LocalBean
public class BlHandleBeltBagCheckInBean {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleBeltBagCheckInBean.class);

	@EJB
	private BlIrmtabFacade _irmfacade;

	@EJB
	private BlTransformBeltBagCheckInBean _beltHandler;

	@EJB
	private BlTransformBeltBagScanUpdtBean _scanUpdater;

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;

	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;

	private JAXBContext _cnxJaxb;

	private Unmarshaller _um;

	private List<String> qFromMqList = null;

	String dtflString = HpEKConstants.BELT_SOURCE;
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;

	/**
	 * Default constructor.
	 */
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(BagDetailsType.class);
			dtflString = _startupInitSingleton.getDtflStr();
			_um = _cnxJaxb.createUnmarshaller();

		} catch (JAXBException ex) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
		// Get The Configuration
		// getInterfacesConfig();

	}

	public boolean routeMessage(Message message,
			InterfaceConfig interfaceConfig, Long irmtabRef) {
		BagDetailsType bagDetails;
		msgLogged = Boolean.FALSE;
		logLevel = _startupInitSingleton.getIrmLogLev();
		// List<EntUfis_MsgExp> msgExpDataList = new
		// ArrayList<EntUfis_MsgExp>();
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		TextMessage msg;
		try {
			msg = (TextMessage) message;
			JAXBElement<BagDetailsType> data = _um.unmarshal(new StreamSource(
					new StringReader(msg.getText())), BagDetailsType.class);
			bagDetails = (BagDetailsType) data.getValue();

			/*
			 * bagDetails = (BagDetails) _um.unmarshal(new StreamSource( new
			 * StringReader(message)));
			 */

			if (bagDetails == null) {
				LOG.error("Message is not valid. Dropping the message :{}",
						message);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, "Invalid Message.",
							irmtabRef);

				}
				return false;
			}

			if (bagDetails.getMeta() == null) {
				LOG.error(
						"Dropped Message details(Meta Info is is either missing or incorrect): "
								+ "Meta = {}, ", bagDetails.getMeta());
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF,
							"Meta Info is missing/Incorrect.", irmtabRef);

				}

				return false;
			}
			if (bagDetails.getBagInfo() == null) {
				LOG.error(
						"Dropped Message details(Bag Info is is either missing or incorrect): "
								+ "MessageID = {}, " + "BagInfo = {}, ",
						new Object[] { bagDetails.getMeta().getMessageID(),
								bagDetails.getBagInfo() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF,
							"Bag Info is missing/Incorrect", irmtabRef);

				}
				return false;
			}

			if (bagDetails.getFlightInfo() == null) {
				LOG.error(
						"Dropped Message details(Flight Info is either missing or incorrect): "
								+ "BagTag = {}, " + "Flight info = {}, ",
						new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()) });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF,
							"Flight Info is missing/Incorrect", irmtabRef);

				}
				return false;
			}

			if (bagDetails.getInboundFlight() == null) {
				LOG.warn(
						"Message details(Bag Inbound Flight Details are missing . Proceeding with process: "
								+ "BagTag = {}, "
								+ "InboundFlight Info = {}, "
								+ "Flight Date = {}, ",
						new Object[] { bagDetails.getBagInfo().getBagTag(),
								bagDetails.getInboundFlight(),
								bagDetails.getFlightInfo().getFlightDate() });
			}

			if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
					.getCarrierCode())
					|| HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
							.getDepartureAirport())
					|| HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
							.getFlightDate())
					|| HpUfisUtils.isNullOrEmptyStr(String.valueOf(bagDetails
							.getFlightInfo().getFlightNumber()))
					|| HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
							.getFlightNumber())) {
				LOG.error(
						"Dropped Message details( FlightInfo Mandatory(DeptAirport,carrierCode,FltDate,FltNumb) values are not specified): "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ " Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(
							EnumExceptionCodes.EMAND,
							"Flight Info Mandatory field(DeptAirport,carrierCode,FltDate,FltNumb) values not specified",
							irmtabRef);

				}

				return false;
			}
			if (!(bagDetails.getFlightInfo().getCarrierCode()
					.equalsIgnoreCase("EK") || bagDetails.getFlightInfo()
					.getCarrierCode().equalsIgnoreCase("QF"))) {
				LOG.error(
						"Dropped Message details( Message is for DIfferent Airlines other than EK and QF). Message Details: "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ " Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWALC,
							"Invalid Airline code: "
									+ bagDetails.getFlightInfo()
											.getCarrierCode(), irmtabRef);

				}
				return false;
			}
			if (bagDetails.getMeta().getMessageSource() == null
					|| bagDetails.getMeta().getMessageSubtype() == null
					|| bagDetails.getMeta().getMessageTime() == null
					|| bagDetails.getMeta().getMessageType() == null) {
				LOG.error(
						"Dropped Message details( Meta Data Mandatory values(MessageType,MessageSource,MessageSubType, MessageTime) are not specified): "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ "Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(
							EnumExceptionCodes.EMAND,
							"Meta Data Mandatory values(MessageType,MessageSource,MessageSubType, MessageTime) not specified",
							irmtabRef);

				}
				return false;

			}
			if (!(bagDetails.getMeta().getMessageSource().value()
					.equalsIgnoreCase(HpEKConstants.BELT_SOURCE))) {
				LOG.error(
						"Dropped Message details( Meta Data Mandatory Field MessageSource(BELT) is not valid): "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ "Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWSRC,
							"Invalid MessageSource:"
									+ bagDetails.getMeta().getMessageSource()
											.value(), irmtabRef);
				}

				return false;
			}
			/*if (!(bagDetails.getFlightInfo().getDepartureAirport()
					.equalsIgnoreCase(HpEKConstants.EK_HOPO))) {
				LOG.debug(
						"Dropping the message as the Departure station of flight is not EK bounded (DXB).The Flight details:"
								+ "BagTag = {}, "
								+ "flight Number = {}, "
								+ "Flight Date= {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.ENDEP,
							"Departure station of flight is not DXB.",
							irmtabRef);

				}

				return false;

			}*/

			// LOG.debug("Flight Date:"
			// + bagDetails.getFlightInfo().getFlightDate());
			if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
					.getBagTag())
					|| (bagDetails.getBagInfo().getBagType() == null)) {
				LOG.error(
						"Dropped Message details( BagInfo Mandatory values(BagTag,BagType) are either missing or not specified correctly). Message Details: "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ " Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(
							EnumExceptionCodes.EMAND,
							"BagInfo Mandatory values(BagTag,BagType) are missing/Invalid",
							irmtabRef);

				}
				return false;
			}

			if (bagDetails.getInboundFlight() != null
					&& !HpUfisUtils.isNullOrEmptyStr(bagDetails
							.getInboundFlight().getArrivalAirport())
					&& !(bagDetails.getInboundFlight().getArrivalAirport()
							.equalsIgnoreCase(HpEKConstants.EK_HOPO))) {
				LOG.error(
						"Dropping the message as the Arrival station of Inbound flight is not EK bounded (DXB).The Flight details:"
								+ "BagTag = {}, "
								+ "flight Number = {}, "
								+ "Flight Date= {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								(bagDetails.getFlightInfo().getCarrierCode()
										+ " " + bagDetails.getFlightInfo()
										.getFlightNumber()),
								bagDetails.getFlightInfo().getFlightDate() });
				// if (_startupInitSingleton.isIrmOn()) {
				// _irmfacade.updateIRMStatus(HpUfisAppConstants.IRM_STAT_ERR);
				// }
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(
							EnumExceptionCodes.EWALC,
							"The Arrival station of Inbound flight is not DXB.",
							irmtabRef);

				}
				return false;

			}

			/*
			 * if (!msgExpDataList.isEmpty()) { sendErrInfo(msgExpDataList,
			 * HpUfisAppConstants.BridgeCommands.IRT.name(), irmtabRef,
			 * dtflString); }
			 */

			LOG.debug(" message is Valid flight message");
			if (bagDetails.getMeta().getMessageType().value()
					.equalsIgnoreCase(MsgType.BCI.value())) {
				if (_beltHandler.tranformFlightCrews(message, bagDetails,
						irmtabRef)) {
					return true;
				} else {
					return false;
				}
			} else {
				if (_scanUpdater.tranformFlightCrews(message, bagDetails,
						irmtabRef)) {
					return true;
				} else {
					return false;
				}
			}
		} catch (JAXBException e) {
			LOG.error("!!!!ERROR:  " + e);
			return false;
		} catch (Exception e) {
			LOG.error("!!!!ERROR:  " + e);
			return false;
		}

	}

	private void sendErrInfo(EnumExceptionCodes expCode, String desc,
			Long irmtabRef) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflString);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflString);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from BELT to Data Loader :\n{}", msg);

	}

	public List<String> getqFromMqList() {
		return qFromMqList;
	}

	public void setqFromMqList(List<String> qFromMqList) {
		this.qFromMqList = qFromMqList;
	}
}
