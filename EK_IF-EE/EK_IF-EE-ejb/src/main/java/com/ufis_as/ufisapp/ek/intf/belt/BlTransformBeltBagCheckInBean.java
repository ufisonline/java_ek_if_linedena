package com.ufis_as.ufisapp.ek.intf.belt;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpBeltConfig;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagClassify;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlPaxBean;
import com.ufis_as.ufisapp.ek.eao.IDlLoadBagUpdateLocal;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.belt.bagDetails.BagDetailsType;
import ek.belt.bagDetails.MsgSubType;

// import ek.belt.bagDetails.BagDetails;

@Stateless(mappedName = "BlTransformBeltBagCheckInBean")
@LocalBean
public class BlTransformBeltBagCheckInBean {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlTransformBeltBagCheckInBean.class);

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private IAfttabBeanLocal _afttabBean;
	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private DlPaxBean _paxBean;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private IDlLoadBagUpdateLocal _loadBagUpdate;

	private List<EntDbMdBagType> allBagTypes = new ArrayList<EntDbMdBagType>();
	private List<EntDbMdBagClassify> allBagClassifications = new ArrayList<EntDbMdBagClassify>();
	private static HashMap<String, String> arlnIataMap = new HashMap<String, String>();
	List<String> masterBagTypes = new ArrayList<String>();
	List<String> masterBagClassify = new ArrayList<String>();
	// private DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	String dtflStr = HpEKConstants.BELT_SOURCE;
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;

	// private EntUfisMsgHead header;
	// List<String> fldList = new ArrayList<>();

	@PreDestroy
	private void initialClear() {
		allBagTypes.clear();
		allBagClassifications.clear();
		arlnIataMap.clear();
		masterBagTypes.clear();
		masterBagClassify.clear();
	}

	public BlTransformBeltBagCheckInBean() {
		// default const

		/*
		 * if (header == null) { header = new EntUfisMsgHead();
		 * header.setHop(HpEKConstants.HOPO); // notifyHeader.setReqt(reqt);
		 * header.setOrig(HpEKConstants.BELT_SOURCE);
		 * header.setApp(HpEKConstants.BELT_SOURCE);
		 * header.setUsr(HpEKConstants.BELT_SOURCE); } if (fldList.isEmpty()) {
		 * fldList.add("ID_FLIGHT"); fldList.add("ID_ARR_FLIGHT");
		 * fldList.add("ID_LOAD_PAX"); fldList.add("BAG_TAG");
		 * fldList.add("BAG_TYPE"); fldList.add("BAG_CLASSIFY");
		 * fldList.add("BAG_CLASS"); fldList.add("PAX_REF_NUM");
		 * fldList.add("PAX_NAME"); fldList.add("CHANGE_DATE");
		 * fldList.add("MSG_SEND_DATE"); fldList.add("REC_STATUS"); }
		 */
	}

	public boolean tranformFlightCrews(Message message,
			BagDetailsType bagDetails, Long irmtabRef) {
		msgLogged = Boolean.FALSE;
		String airLineCode = null;
		long startTime = new Date().getTime();
		dtflStr = HpCommonConfig.dtfl;
		logLevel = HpCommonConfig.irmtab;
		// String cmdForBroadcasting = null;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		EntDbLoadBag oldLoadBag = new EntDbLoadBag();
		try {
			EntDbLoadBag loadBagStat = null;
			EntDbLoadBag loadBag = new EntDbLoadBag();

			if (_startupInitSingleton.isMDBagTagListOn()
					&& arlnIataMap.isEmpty()) {
				arlnIataMap = _startupInitSingleton.getAirlineCodeCnvertMap();
			}

			if (bagDetails.getFlightInfo() != null) {
				String urno = null;

				String flightNumber = bagDetails.getFlightInfo()
						.getCarrierCode() == null ? " "
						: bagDetails.getFlightInfo().getCarrierCode()
								.substring(0, 2)
								+ " "
								+ HpUfisUtils.formatCedaFltn(String
										.valueOf(bagDetails.getFlightInfo()
												.getFlightNumber()))
								+ (bagDetails.getFlightInfo().getFlightSuffix() == null ? ""
										: bagDetails.getFlightInfo()
												.getFlightSuffix());

				String fltDate = chgStringToFltDate(bagDetails.getFlightInfo()
						.getFlightDate());
				LOG.debug("flightNumber: " + flightNumber + "   FlightDate:"
						+ fltDate);

				/* Change */
				EntDbAfttab criteriaParams = new EntDbAfttab();
				criteriaParams.setFlno(flightNumber);
				criteriaParams.setFlda(fltDate);
				// criteriaParams.setAdid('D');
				if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
						.getArrivalAirport())) {
					urno = _afttabBean.getUrnoByFldaFlnoAdid(criteriaParams,
							null, bagDetails.getFlightInfo()
									.getDepartureAirport());
				} else {
					urno = _afttabBean.getUrnoByFldaFlnoAdid(criteriaParams,
							bagDetails.getFlightInfo().getArrivalAirport(),
							bagDetails.getFlightInfo().getDepartureAirport());
				}
				if (urno == null || String.valueOf(urno) == null
						|| urno.isEmpty()) {
					// populate ID_FLIGHT as ZERO
					urno = new Long(0).toString();
					LOG.error(
							"No flight is available in AFTTAB with the Flight details:"
									+ "BagTag = {}, " + "flight Number = {}, "
									+ "Flight Date= {}, ", new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									flightNumber, fltDate });
					if (!HpUfisUtils.isNullOrEmptyStr(bagDetails
							.getFlightInfo().getArrivalAirport())) {
						if (logLevel
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(message,
										dtflStr);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(EnumExceptionCodes.ENOFL,
									"FLight not found.Bag Tag:"
											+ bagDetails.getBagInfo()
													.getBagTag()
											+ "  Flight Number:"
											+ bagDetails.getFlightInfo()
													.getFullFlightNumber()
											+ "  Flight Date:"
											+ bagDetails.getFlightInfo()
													.getFlightDate(), irmtabRef);
						}
					}

				}
				// long idFlight = (urno != null && (String.valueOf(urno) !=
				// null) && (!urno
				// .isEmpty())) ? Long.parseLong(urno) : 0;
				long idFlight = Long.parseLong(urno);
				List<EntDbLoadBag> loadBagDetailsList = new ArrayList<EntDbLoadBag>();
				String recStatus = " ";
				if (bagDetails.getMeta().getMessageSubtype().value()
						.equalsIgnoreCase(MsgSubType.DEL.value())) {
					// if
					// (!HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo().getPassengerSeqNo()))
					// {
					startTime = new Date().getTime();
					if (idFlight != 0) {
						if (!HpUfisUtils.isNullOrEmptyStr(bagDetails
								.getBagInfo().getPassengerSeqNo())) {
							// loadBagDetailsList =
							// _loadBagUpdate.getBagMoveDetails(
							// bagDetails.getBagInfo().getBagTag(), bagDetails
							// .getBagInfo().getPassengerSeqNo(),
							// flightNumber, fltDate, bagDetails
							// .getFlightInfo().getArrivalAirport());
							loadBagDetailsList = _loadBagUpdate
									.getBagMoveDetailsByBagtagPax(
											idFlight,
											bagDetails.getBagInfo().getBagTag(),
											bagDetails.getBagInfo()
													.getPassengerSeqNo());
						} else {
							loadBagDetailsList = _loadBagUpdate
									.getBagMoveDetailsByBagTag(idFlight,
											bagDetails.getBagInfo().getBagTag());
						}
					}
					LOG.debug(
							"Total Duration on loading bag details (in ms): {}",
							new Date().getTime() - startTime);
					// }
					if (loadBagDetailsList.size() == 1) {
						if (loadBagDetailsList.get(0).getBagClassify() != null
								&& (loadBagDetailsList.get(0).getBagClassify()
										.equalsIgnoreCase("RUSH"))) {
							oldLoadBag = loadBagDetailsList.get(0).getClone();
							loadBagDetailsList.get(0).setRecStatus("X");
							loadBagDetailsList.get(0).setUpdatedUser(
									HpEKConstants.BELT_SOURCE);
							loadBagDetailsList.get(0).setUpdatedDate(
									HpUfisCalendar.getCurrentUTCTime());
							loadBagStat = _loadBagUpdate
									.updateLoadedBag(loadBagDetailsList.get(0));
							LOG.info(
									"One Record found and Deleting the record, Marking status as('X'):"
											+ "BagTag = {}, "
											+ "flight Number = {}, "
											+ "Flight Date= {}, ",
									new Object[] {
											bagDetails.getBagInfo().getBagTag(),
											flightNumber, fltDate });
							if (loadBagStat != null) {
								/*
								 * sendLoadBagUpdateToCedaInJson(loadBagStat,
								 * oldLoadBag, UfisASCommands.URT.name(), true);
								 */
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
									// send notification message to topic
									ufisTopicProducer.sendNotification(true,
											UfisASCommands.DRT, String
													.valueOf(loadBagStat
															.getId_Flight()),
											oldLoadBag, loadBagStat);
								}
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
									// if topic not defined, send notification
									// to queue
									ufisQueueProducer.sendNotification(true,
											UfisASCommands.DRT, String
													.valueOf(loadBagStat
															.getId_Flight()),
											oldLoadBag, loadBagStat);
								}
								LOG.debug("Bag Record change has beed communicated to Ceda.");
							}
						} else {
							oldLoadBag = loadBagDetailsList.get(0);
							loadBagDetailsList.get(0).setRecStatus("I");
							loadBagDetailsList.get(0).setUpdatedUser(
									HpEKConstants.BELT_SOURCE);
							loadBagDetailsList.get(0).setUpdatedDate(
									HpUfisCalendar.getCurrentUTCTime());
							loadBagStat = _loadBagUpdate
									.updateLoadedBag(loadBagDetailsList.get(0));
							LOG.info(
									"One Record found but it is not RUSH bag updating the record status('I'):"
											+ "BagTag = {}, "
											+ "flight Number = {}, "
											+ "Flight Date= {}, ",
									new Object[] {
											bagDetails.getBagInfo().getBagTag(),
											flightNumber, fltDate });
							if (loadBagStat != null) {
								/*
								 * sendLoadBagUpdateToCedaInJson(loadBagStat,
								 * oldLoadBag, UfisASCommands.URT.name(), true);
								 */
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
									// send notification message to topic
									ufisTopicProducer.sendNotification(true,
											UfisASCommands.URT, String
													.valueOf(loadBagStat
															.getId_Flight()),
											oldLoadBag, loadBagStat);
								}
								if (HpUfisUtils
										.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
									// if topic not defined, send notification
									// to queue
									ufisQueueProducer.sendNotification(true,
											UfisASCommands.URT, String
													.valueOf(loadBagStat
															.getId_Flight()),
											oldLoadBag, loadBagStat);
								}

								LOG.debug("Bag Record change has beed communicated to Ceda.");
							}
						}
					} else if (loadBagDetailsList.size() > 1) {
						boolean isLast = false;
						for (EntDbLoadBag bagRecs : loadBagDetailsList) {
							if (bagRecs.equals(loadBagDetailsList
									.get(loadBagDetailsList.size() - 1))) {
								isLast = true;
							}
							oldLoadBag = bagRecs.getClone();
							if (!bagRecs.getRecStatus().equalsIgnoreCase("R")) {
								bagRecs.setRecStatus("R");
								bagRecs.setUpdatedUser(HpEKConstants.BELT_SOURCE);
								bagRecs.setUpdatedDate(HpUfisCalendar
										.getCurrentUTCTime());
								loadBagStat = _loadBagUpdate
										.updateLoadedBag(bagRecs);
								if (loadBagStat != null) {
									/*
									 * sendLoadBagUpdateToCedaInJson(loadBagStat,
									 * oldLoadBag, UfisASCommands.URT.name(),
									 * true);
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										ufisTopicProducer.sendNotification(
												isLast, UfisASCommands.URT,
												String.valueOf(loadBagStat
														.getId_Flight()),
												oldLoadBag, loadBagStat);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer.sendNotification(
												isLast, UfisASCommands.URT,
												String.valueOf(loadBagStat
														.getId_Flight()),
												oldLoadBag, loadBagStat);
									}
									LOG.debug("Bag Record change has beed communicated to Ceda.");
								}
							}
						}
						LOG.info(
								"Multiple records found related to bagdetails. Updating existing recStatus as 'R'. Ignoring the message."
										+ "BagTag = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										flightNumber, fltDate });
					} else {
						LOG.info("Record not found .Message Details::"
								+ "BagTag = {}, " + "flight Number = {}, "
								+ "Flight Date= {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								flightNumber, fltDate });
						if (logLevel
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(message,
										dtflStr);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(EnumExceptionCodes.ENOBG,
									"Rec not found in LOAD_BAG.Bag Tag:"
											+ bagDetails.getBagInfo()
													.getBagTag()
											+ "  Flight Number:"
											+ bagDetails.getFlightInfo()
													.getFullFlightNumber()
											+ "  Flight Date:"
											+ bagDetails.getFlightInfo()
													.getFlightDate(), irmtabRef);

						}

						return Boolean.TRUE;
					}
				}
				if (_startupInitSingleton.isMdBagTypeOn()
						&& allBagTypes.isEmpty()) {
					allBagTypes = _startupInitSingleton.getMdBagTypeList();
					for (EntDbMdBagType bagTypeRec : allBagTypes) {
						if ((bagTypeRec.getRecStatus() != null)
								&& !bagTypeRec.getRecStatus().equalsIgnoreCase(
										"X")) {
							masterBagTypes.add(bagTypeRec.getBagTypeCode());
						}
					}
				}

				if (_startupInitSingleton.isMdBagTypeOn()
						&& (masterBagTypes.isEmpty() || !(masterBagTypes
								.contains(bagDetails.getBagInfo().getBagType()
										.value())))) {
					LOG.error(
							"Dropping the message .Master Data BagTypes doesn't contain the specified value for the Flight :"
									+ "BagTag = {}, "
									+ "flight Number = {}, "
									+ "Flight Date= {}, ",
							new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									bagDetails.getFlightInfo()
											.getFlightNumber(),
									bagDetails.getFlightInfo().getFlightDate() });

					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade
									.storeRawMsg(message, dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.ENOMD,
								"BagType not in MasterData.Bag Type: "
										+ bagDetails.getBagInfo().getBagType()
												.value()
										+ "  Bag Tag:"
										+ bagDetails.getBagInfo().getBagTag()
										+ "  Flight Number:"
										+ bagDetails.getFlightInfo()
												.getFullFlightNumber()
										+ "  Flight Date:"
										+ bagDetails.getFlightInfo()
												.getFlightDate(), irmtabRef);

					}
					return false;

				}
				if (_startupInitSingleton.isMdBagClassifyOn()
						&& allBagClassifications.isEmpty()) {
					allBagClassifications = _startupInitSingleton
							.getMdBagClassifyList();
					for (EntDbMdBagClassify bagClassifyRec : allBagClassifications) {
						if ((bagClassifyRec.getRecStatus() != null)
								&& !bagClassifyRec.getRecStatus()
										.equalsIgnoreCase("X")) {
							masterBagClassify.add(bagClassifyRec
									.getBagClassify());
						}
					}
				}
				if (bagDetails.getBagInfo().getBagClassification() != null
						&& (_startupInitSingleton.isMdBagClassifyOn() && (masterBagClassify
								.isEmpty() || (!(masterBagClassify
								.contains(bagDetails.getBagInfo()
										.getBagClassification())))))) {
					// LOG.error("Dropping the message .Master Data Bag Classify doesn't contain the specified value for the Flight :"
					// + "BagTag = {}, "
					// + "flight Number = {}, " + "Flight Date= {}, ", new
					// Object[]{bagDetails.getBagInfo().getBagTag(),
					// bagDetails.getFlightInfo().getFlightNumber(),
					// bagDetails.getFlightInfo().getFlightDate()});
					LOG.warn(
							"BagClassification is not found in Master Data.Populating the value for the Flight :"
									+ "BagTag = {}, "
									+ "flight Number = {}, "
									+ "Flight Date= {}, ",
							new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									bagDetails.getFlightInfo()
											.getFlightNumber(),
									bagDetails.getFlightInfo().getFlightDate() });

					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade
									.storeRawMsg(message, dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.WNOMD,
								"Bag Classification not in MasterData.Bag Classify: "
										+ bagDetails.getBagInfo()
												.getBagClassification()
										+ "  Bag Tag:"
										+ bagDetails.getBagInfo().getBagTag()
										+ "  Flight Number:"
										+ bagDetails.getFlightInfo()
												.getFullFlightNumber()
										+ "  Flight Date:"
										+ bagDetails.getFlightInfo()
												.getFlightDate(), irmtabRef);

					}

					// return false;
				}
				// loadBag.setIdFlight(idFlight);
				loadBag.setId_Flight(new BigDecimal(idFlight));
				// loadBag.setFlightNumber(flightNumber);
				loadBag.setBagFlno(flightNumber);
				// loadBag.setFltDate(fltDate);
				loadBag.setFlt_Date(fltDate);
				loadBag.setMsgSendDate(convertDateToUTC(bagDetails.getMeta()
						.getMessageTime()));
				// loadBag.setFltOrigin3(bagDetails.getFlightInfo()
				// .getDepartureAirport());
				loadBag.setFlt_Origin3(bagDetails.getFlightInfo()
						.getDepartureAirport());
				// loadBag.setFltDest3(bagDetails.getFlightInfo()
				// .getArrivalAirport());
				loadBag.setFlt_Dest3(bagDetails.getFlightInfo()
						.getArrivalAirport());
				loadBag.setBagTag(bagDetails.getBagInfo().getBagTag());
				if (HpUfisUtils.isNotEmptyStr(loadBag.getBagTag())
						&& loadBag.getBagTag().length() > 4) {
					airLineCode = getAirLineFromCode(loadBag.getBagTag()
							.substring(1, 4));
					//LOG.info("Airline Code:"+airLineCode);
					if (airLineCode != null
							&& bagDetails.getFlightInfo().getArrivalAirport() != null) {
						loadBag.setBagTagStr(airLineCode.concat(
								loadBag.getBagTag().substring(4)).concat(
								bagDetails.getFlightInfo().getArrivalAirport()));
					}
					/*
					 * loadBag.setBagTagStr(airLineCode != null ? airLineCode :
					 * null);
					 */
				}
				if (bagDetails.getBagInfo().getBagType() != null) {
					loadBag.setBagType(bagDetails.getBagInfo().getBagType()
							.value());
				}
				if (bagDetails.getBagInfo().getBagClassification() != null) {
					loadBag.setBagClassify(bagDetails.getBagInfo()
							.getBagClassification());
				}

				if (!HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
						.getBagClass())) {
					if (!HpBeltConfig.getBusinessClassBagList().isEmpty()
							&& HpBeltConfig.getBusinessClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBag.setBagClass("J");
					} else if (!HpBeltConfig.getFirstClassBagList().isEmpty()
							&& HpBeltConfig.getFirstClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBag.setBagClass("F");
					} else if (!HpBeltConfig.getEconomyClassBagList().isEmpty()
							&& HpBeltConfig.getEconomyClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBag.setBagClass("Y");
					} else {
						LOG.info(
								"Specified Bag Class is not in configured list for the Flight :"
										+ "BagTag = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										bagDetails.getFlightInfo()
												.getFlightNumber(),
										bagDetails.getFlightInfo()
												.getFlightDate() });

						if (logLevel
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(message,
										dtflStr);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(EnumExceptionCodes.WBGCL,
									"Bag Class not in config list.Bag Class: "
											+ bagDetails.getBagInfo()
													.getBagClass()
											+ "  Bag Tag:"
											+ bagDetails.getBagInfo()
													.getBagTag()
											+ "  Flight Number:"
											+ bagDetails.getFlightInfo()
													.getFullFlightNumber()
											+ "  Flight Date:"
											+ bagDetails.getFlightInfo()
													.getFlightDate(), irmtabRef);

						}

						loadBag.setBagClass(bagDetails.getBagInfo()
								.getBagClass());
					}
				}

				loadBag.setPaxName(bagDetails.getBagInfo().getPassengerName());
				loadBag.setPaxRefNum(bagDetails.getBagInfo()
						.getPassengerSeqNo());
				LOG.debug("Pax Seq Num:"
						+ bagDetails.getBagInfo().getPassengerSeqNo());
				if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
						.getPassengerSeqNo())) {
					LOG.warn(
							"Pax (Passenger SeqNO is null) is not available in LOAD_PAX. Flight details:"
									+ "BagTag = {}, " + "PaxRef Number = {}, "
									+ "flight Number = {}, "
									+ "Flight Date= {}, ",
							new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									bagDetails.getBagInfo().getPassengerSeqNo(),
									bagDetails.getFlightInfo()
											.getFlightNumber(),
									bagDetails.getFlightInfo().getFlightDate() });

					/*
					 * if (logLevel
					 * .equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
					 * .name())) { if (!msgLogged) { irmtabRef = _irmfacade
					 * .storeRawMsg(message, dtflStr); msgLogged = Boolean.TRUE;
					 * } } if (irmtabRef > 0) {
					 * sendErrInfo(EnumExceptionCodes.WNOPX,
					 * "Pax num not specified.PaxRefNum: " +
					 * bagDetails.getBagInfo() .getPassengerSeqNo() +
					 * "  Bag Tag:" + bagDetails.getBagInfo().getBagTag() +
					 * "  Flight Number:" + bagDetails.getFlightInfo()
					 * .getFullFlightNumber() + "  Flight Date:" +
					 * bagDetails.getFlightInfo() .getFlightDate(), irmtabRef);
					 * 
					 * }
					 */

				} else {
					startTime = new Date().getTime();
					EntDbLoadPax entLoadPax = _paxBean.getPaxWithPaxRefNum(
							bagDetails.getBagInfo().getPassengerSeqNo(), urno);
					LOG.debug(
							"Total Duration on searching for pax details (in ms): {}",
							new Date().getTime() - startTime);
					if (entLoadPax == null
							|| HpUfisUtils.isNullOrEmptyStr(entLoadPax
									.getUuid())) {
						LOG.warn(
								"Pax details are not available in LOAD_PAX. Flight details:"
										+ "BagTag = {}, "
										+ "PaxRef Number = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										bagDetails.getBagInfo()
												.getPassengerSeqNo(),
										bagDetails.getFlightInfo()
												.getFlightNumber(),
										bagDetails.getFlightInfo()
												.getFlightDate() });

						/*
						 * if (logLevel
						 * .equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev
						 * .LOG_ERR .name())) { if (!msgLogged) { irmtabRef =
						 * _irmfacade.storeRawMsg(message, dtflStr); msgLogged =
						 * Boolean.TRUE; } } if (irmtabRef > 0) {
						 * sendErrInfo(EnumExceptionCodes.WNOPX,
						 * "Pax not found.PaxRefNum: " + bagDetails.getBagInfo()
						 * .getPassengerSeqNo() + "  Bag Tag:" +
						 * bagDetails.getBagInfo() .getBagTag() +
						 * "  Flight Number:" + bagDetails.getFlightInfo()
						 * .getFullFlightNumber() + "  Flight Date:" +
						 * bagDetails.getFlightInfo() .getFlightDate(),
						 * irmtabRef);
						 * 
						 * }
						 */

						// return false;
					} else {
						loadBag.setIdLoadPax(entLoadPax.getUuid());

					}
				}
				loadBag.setChangeDate(chgStringToDate(
						chgStringToFltDate(bagDetails.getBagInfo()
								.getChangeDate()), bagDetails.getBagInfo()
								.getChangeTime()));

				if (bagDetails.getInboundFlight() != null) {
					String inboundUrno = null;
					if (HpUfisUtils.isNullOrEmptyStr(bagDetails
							.getInboundFlight().getCarrierCode())
							|| HpUfisUtils.isNullOrEmptyStr(bagDetails
									.getInboundFlight().getFlightNumber())
							|| HpUfisUtils.isNullOrEmptyStr(bagDetails
									.getInboundFlight().getFlightDate())) {
						LOG.warn(
								"Inbound Flight details are not sufficient to search in AFTTAB. The Inbound Flight details:"
										+ "BagTag = {}, "
										+ "Arrival flight Number = {}, "
										+ "Arrival Flight Date= {}, ",
								new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										bagDetails.getInboundFlight()
												.getFlightNumber(),
										bagDetails.getInboundFlight()
												.getFlightDate() });

						String arrFltNumb = bagDetails.getInboundFlight()
								.getCarrierCode() == null ? "  " : bagDetails
								.getInboundFlight().getCarrierCode()
								.substring(0, 2)
								+ " "
								+ HpUfisUtils.formatCedaFltn(String
										.valueOf(bagDetails.getInboundFlight()
												.getFlightNumber()))
								+ (bagDetails.getInboundFlight()
										.getFlightSuffix() == null ? ""
										: bagDetails.getInboundFlight()
												.getFlightSuffix());
						String arrFltDate = chgStringToFltDate(bagDetails
								.getInboundFlight().getFlightDate());
						// loadBag.setArrFlightNumber(arrFltNumb);
						loadBag.setBagConxFlno(arrFltNumb);
						// loadBag.setArrFltDate(arrFltDate);
						loadBag.setConxFltDate(arrFltDate);
					} else {

						String arrFltNumb = bagDetails.getInboundFlight()
								.getCarrierCode() == null ? "  " : bagDetails
								.getInboundFlight().getCarrierCode()
								.substring(0, 2)
								+ " "
								+ HpUfisUtils.formatCedaFltn(String
										.valueOf(bagDetails.getInboundFlight()
												.getFlightNumber()))
								+ (bagDetails.getInboundFlight()
										.getFlightSuffix() == null ? ""
										: bagDetails.getInboundFlight()
												.getFlightSuffix());
						String arrFltDate = chgStringToFltDate(bagDetails
								.getInboundFlight().getFlightDate());
						LOG.debug("flightNumber: " + arrFltNumb
								+ "   FlightDate:" + arrFltDate);
						startTime = new Date().getTime();

						// Change
						EntDbAfttab criteriaParam = new EntDbAfttab();
						// startTime = new Date().getTime();
						criteriaParam.setFlno(arrFltNumb);
						criteriaParam.setFlda(arrFltDate);
						// criteriaParam.setAdid('A');
						inboundUrno = _afttabBean.getUrnoByFldaFlnoAdid(
								criteriaParam, null, bagDetails
										.getInboundFlight()
										.getDepartureAirport());
						LOG.debug(
								"Total Duration on searching inboundFlight in AFTTAB (in ms): {}",
								new Date().getTime() - startTime);
						if (inboundUrno == null
								|| String.valueOf(inboundUrno) == null
								|| inboundUrno.isEmpty()) {
							inboundUrno = new Long(0).toString();
							LOG.error(
									"No flight is available in AFTTAB with the Inbound Flight details:"
											+ "BagTag = {}, "
											+ "Arrival flight Number = {}, "
											+ "Arrival Flight Date= {}, ",
									new Object[] {
											bagDetails.getBagInfo().getBagTag(),
											arrFltNumb, arrFltDate });

							if (logLevel
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(message,
											dtflStr);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(EnumExceptionCodes.ENOFL,
										"Flight not found. Bag Tag:"
												+ bagDetails.getBagInfo()
														.getBagTag()
												+ "  Flight Number:"
												+ bagDetails.getFlightInfo()
														.getFullFlightNumber()
												+ "  Flight Date:"
												+ bagDetails.getFlightInfo()
														.getFlightDate(),
										irmtabRef);

							}

							// return false;

						}
						// loadBag.setArrFlightNumber(arrFltNumb);
						// loadBag.setArrFltDate(arrFltDate);
						loadBag.setBagConxFlno(arrFltNumb);
						loadBag.setConxFltDate(arrFltDate);
					}
					if (!HpUfisUtils.isNullOrEmptyStr(inboundUrno)) {
						// loadBag.setIdArrFlight(Long.parseLong(inboundUrno));
						loadBag.setIdConxFlight(new BigDecimal(inboundUrno));
					}

					// loadBag.setArrFltOrigin3(bagDetails.getInboundFlight()
					// .getDepartureAirport());
					// loadBag.setArrFltDest3(bagDetails.getInboundFlight()
					// .getArrivalAirport());
					loadBag.setConxFltOrigin3(bagDetails.getInboundFlight()
							.getDepartureAirport());
					loadBag.setConxFltDes3(bagDetails.getInboundFlight()
							.getArrivalAirport());
				}

				loadBag.setRecStatus(recStatus);
				if (bagDetails.getMeta().getMessageSubtype().value()
						.equalsIgnoreCase(MsgSubType.INS.value())) {
					/*
					 * cmdForBroadcasting =
					 * HpUfisAppConstants.UfisASCommands.IRT .toString();
					 */
					loadBagStat = _loadBagUpdate.updateLoadedBag(loadBag);
					if (loadBagStat != null) {
						/*
						 * sendLoadBagUpdateToCedaInJson(loadBagStat,
						 * oldLoadBag, cmdForBroadcasting, true);
						 */
						if (HpUfisUtils
								.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
							// send notification message to topic
							ufisTopicProducer.sendNotification(true,
									UfisASCommands.IRT,
									String.valueOf(loadBagStat.getId_Flight()),
									oldLoadBag, loadBagStat);
						}
						if (HpUfisUtils
								.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
							// if topic not defined, send notification to queue
							ufisQueueProducer.sendNotification(true,
									UfisASCommands.IRT,
									String.valueOf(loadBagStat.getId_Flight()),
									oldLoadBag, loadBagStat);
						}
						LOG.debug("Bag Record change has beed communicated to Ceda.");
					}
				} else if (bagDetails.getMeta().getMessageSubtype().value()
						.equalsIgnoreCase(MsgSubType.UPD.value())) {
					/*
					 * cmdForBroadcasting =
					 * HpUfisAppConstants.UfisASCommands.URT .toString();
					 */
					startTime = new Date().getTime();
					if (idFlight != 0) {
						if (!HpUfisUtils.isNullOrEmptyStr(bagDetails
								.getBagInfo().getPassengerSeqNo())) {
							loadBagDetailsList = _loadBagUpdate
									.getBagMoveDetailsByBagtagPax(
											idFlight,
											bagDetails.getBagInfo().getBagTag(),
											bagDetails.getBagInfo()
													.getPassengerSeqNo());
						} else {
							loadBagDetailsList = _loadBagUpdate
									.getBagMoveDetailsByBagTag(idFlight,
											bagDetails.getBagInfo().getBagTag());
						}
					}
					LOG.debug(
							"Total Duration on loading bag details for FlightID: {}  (in ms): {}",
							idFlight, new Date().getTime() - startTime);

					if (loadBagDetailsList.size() == 1) {
						oldLoadBag = loadBagDetailsList.get(0).getClone();
						loadBagStat = _loadBagUpdate
								.updateLoadedBag(updateloadBagDetails(
										loadBagDetailsList.get(0), loadBag));
						if (loadBagStat != null) {
							/*
							 * sendLoadBagUpdateToCedaInJson(loadBagStat,
							 * oldLoadBag, cmdForBroadcasting, true);
							 */
							if (HpUfisUtils
									.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
								// send notification message to topic
								ufisTopicProducer.sendNotification(true,
										UfisASCommands.URT, String
												.valueOf(loadBagStat
														.getId_Flight()),
										oldLoadBag, loadBagStat);
							}
							if (HpUfisUtils
									.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
								// if topic not defined, send notification to
								// queue
								ufisQueueProducer.sendNotification(true,
										UfisASCommands.URT, String
												.valueOf(loadBagStat
														.getId_Flight()),
										oldLoadBag, loadBagStat);
							}
							LOG.debug("Bag Record change has beed communicated to Ceda.");
						}
					} else if (loadBagDetailsList.size() > 1) {
						boolean isLast = false;
						for (EntDbLoadBag bagRecs : loadBagDetailsList) {
							if (bagRecs.equals(loadBagDetailsList
									.get(loadBagDetailsList.size() - 1))) {
								isLast = true;
							}
							if (!bagRecs.getRecStatus().equalsIgnoreCase("R")) {
								oldLoadBag = bagRecs.getClone();
								bagRecs.setRecStatus("R");
								bagRecs.setUpdatedUser(HpEKConstants.BELT_SOURCE);
								bagRecs.setUpdatedDate(HpUfisCalendar
										.getCurrentUTCTime());
								loadBagStat = _loadBagUpdate
										.updateLoadedBag(bagRecs);
								if (loadBagStat != null) {
									/*
									 * sendLoadBagUpdateToCedaInJson(loadBagStat,
									 * oldLoadBag, cmdForBroadcasting, true);
									 */
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
										// send notification message to topic
										ufisTopicProducer.sendNotification(
												isLast, UfisASCommands.URT,
												String.valueOf(loadBagStat
														.getId_Flight()),
												oldLoadBag, loadBagStat);
									}
									if (HpUfisUtils
											.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
										// if topic not defined, send
										// notification to queue
										ufisQueueProducer.sendNotification(
												isLast, UfisASCommands.URT,
												String.valueOf(loadBagStat
														.getId_Flight()),
												oldLoadBag, loadBagStat);
									}
									LOG.debug("Bag Record change has beed communicated to Ceda.");
								}
							}
						}

						LOG.info(
								"Multiple records found related to bag details. Changing existing record's recStatus as 'R'.Ignoring the message. Message Details."
										+ "BagTag = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										flightNumber, fltDate });
					} else {
						LOG.info(
								"Record not found . Inserting the record.Message Details:"
										+ "BagTag = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										flightNumber, fltDate });
						loadBagStat = _loadBagUpdate.updateLoadedBag(loadBag);
						if (loadBagStat != null) {
							/*
							 * sendLoadBagUpdateToCedaInJson(loadBagStat,
							 * oldLoadBag, HpUfisAppConstants.UfisASCommands.IRT
							 * .name(), true);
							 */
							if (HpUfisUtils
									.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
								// send notification message to topic
								ufisTopicProducer.sendNotification(true,
										UfisASCommands.IRT, String
												.valueOf(loadBagStat
														.getId_Flight()),
										oldLoadBag, loadBagStat);
							}
							if (HpUfisUtils
									.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
								// if topic not defined, send notification to
								// queue
								ufisQueueProducer.sendNotification(true,
										UfisASCommands.IRT, String
												.valueOf(loadBagStat
														.getId_Flight()),
										oldLoadBag, loadBagStat);
							}
							LOG.debug("Bag Record change has beed communicated to Ceda.");
						}
					}
				}

			}
		} catch (NumberFormatException nfe) {
			LOG.error("!!!! Error in Converting the urno(AFTTAB) from String to long");
		} catch (Exception e) {
			LOG.error("Exception :", e);
		}
		return true;
	}

	private EntDbLoadBag updateloadBagDetails(EntDbLoadBag entDbLoadBag,
			EntDbLoadBag loadBag) {
		String airLineCode = null;
		// entDbLoadBag.setIdFlight(loadBag.getIdFlight());
		entDbLoadBag.setId_Flight(loadBag.getId_Flight());
		// entDbLoadBag.setFlightNumber(loadBag.getFlightNumber());
		entDbLoadBag.setBagFlno(loadBag.getBagFlno());
		// entDbLoadBag.setFltDate(loadBag.getFltDate());
		entDbLoadBag.setFlt_Date(loadBag.getFlt_Date());
		entDbLoadBag.setMsgSendDate(loadBag.getMsgSendDate());
		// entDbLoadBag.setFltOrigin3(loadBag.getFltOrigin3());
		// entDbLoadBag.setFltDest3(loadBag.getFltDest3());
		entDbLoadBag.setFlt_Origin3(loadBag.getFlt_Origin3());
		entDbLoadBag.setFlt_Dest3(loadBag.getFlt_Dest3());

		entDbLoadBag.setBagTag(loadBag.getBagTag());
		if (HpUfisUtils.isNotEmptyStr(loadBag.getBagTag())
				&& loadBag.getBagTag().length() > 4) {
			airLineCode = getAirLineFromCode(loadBag.getBagTag()
					.substring(1, 4));
			// entDbLoadBag.setBagTagStr(airLineCode != null ? airLineCode :
			// null);
			if (airLineCode != null && entDbLoadBag.getFlt_Dest3() != null) {
				entDbLoadBag.setBagTagStr(airLineCode.concat(
						loadBag.getBagTag().substring(4)).concat(
						entDbLoadBag.getFlt_Dest3()));
			}
		}
		entDbLoadBag.setBagType(loadBag.getBagType());
		entDbLoadBag.setBagClassify(loadBag.getBagClassify());
		entDbLoadBag.setBagClass(loadBag.getBagClass());
		entDbLoadBag.setPaxName(loadBag.getPaxName());
		entDbLoadBag.setPaxRefNum(loadBag.getPaxRefNum());
		entDbLoadBag.setIdLoadPax(loadBag.getIdLoadPax());
		entDbLoadBag.setChangeDate(loadBag.getChangeDate());
		// entDbLoadBag.setIdArrFlight(loadBag.getIdArrFlight());
		// entDbLoadBag.setArrFlightNumber(loadBag.getArrFlightNumber());
		// entDbLoadBag.setArrFltDate(loadBag.getArrFltDate());
		// entDbLoadBag.setArrFltOrigin3(loadBag.getArrFltOrigin3());
		// entDbLoadBag.setArrFltDest3(loadBag.getArrFltDest3());

		entDbLoadBag.setIdConxFlight(loadBag.getIdConxFlight());
		entDbLoadBag.setBagConxFlno(loadBag.getBagConxFlno());
		entDbLoadBag.setConxFltDate(loadBag.getConxFltDate());
		entDbLoadBag.setConxFltOrigin3(loadBag.getConxFltOrigin3());
		entDbLoadBag.setConxFltDes3(loadBag.getConxFltDes3());

		try {
			entDbLoadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.error(
					"!!!! ERROR while converting the current time to UTC.Exception {}",
					e);
		}
		entDbLoadBag.setUpdatedUser(HpEKConstants.BELT_SOURCE);
		return entDbLoadBag;
	}

	/*
	 * private void sendLoadBagUpdateToCedaInJson(EntDbLoadBag entLoadBag,
	 * EntDbLoadBag oldLoadBag, String cmd, Boolean isLast) throws IOException,
	 * JsonGenerationException, JsonMappingException { String urno = new
	 * Long(entLoadBag.getIdFlight()).toString(); // get the HEAD info
	 * List<String> idFlightList = new ArrayList<String>();
	 * idFlightList.add(urno); header.setIdFlight(idFlightList); if (isLast) {
	 * header.setBcnum(-1); } List<Object> dataList = new ArrayList<Object>();
	 * dataList.add(urno); dataList.add(new
	 * Long(entLoadBag.getIdArrFlight()).toString());
	 * dataList.add(entLoadBag.getIdLoadPax());
	 * dataList.add(entLoadBag.getBagTag());
	 * dataList.add(entLoadBag.getBagType());
	 * dataList.add(entLoadBag.getBagClassify());
	 * dataList.add(entLoadBag.getBagClass());
	 * dataList.add(entLoadBag.getPaxRefNum());
	 * dataList.add(entLoadBag.getPaxName());
	 * dataList.add(df.format(entLoadBag.getChangeDate()));
	 * dataList.add(df.format(entLoadBag.getMsgSendDate()));
	 * dataList.add(entLoadBag.getRecStatus());
	 * 
	 * List<Object> oldList = new ArrayList<>(); if
	 * (UfisASCommands.URT.toString().equals(cmd) && oldLoadBag != null) {
	 * oldList.add(new Long(oldLoadBag.getIdFlight()).toString());
	 * oldList.add(new Long(oldLoadBag.getIdArrFlight()).toString());
	 * oldList.add(oldLoadBag.getIdLoadPax());
	 * oldList.add(oldLoadBag.getBagTag());
	 * oldList.add(oldLoadBag.getBagType());
	 * oldList.add(oldLoadBag.getBagClassify());
	 * oldList.add(oldLoadBag.getBagClass());
	 * oldList.add(oldLoadBag.getPaxRefNum());
	 * oldList.add(oldLoadBag.getPaxName());
	 * oldList.add(df.format(oldLoadBag.getChangeDate()));
	 * oldList.add(df.format(oldLoadBag.getMsgSendDate()));
	 * oldList.add(oldLoadBag.getRecStatus()); }
	 * 
	 * List<String> idList = new ArrayList<>(); idList.add(entLoadBag.getId());
	 * 
	 * List<EntUfisMsgACT> listAction = new ArrayList<>(); EntUfisMsgACT action
	 * = new EntUfisMsgACT(); action.setCmd(cmd); action.setTab("LOAD_BAG");
	 * action.setFld(fldList); action.setData(dataList);
	 * action.setOdat(oldList); action.setId(idList);
	 * action.setSel("WHERE ID = \"" + entLoadBag.getId() + "\"");
	 * 
	 * listAction.add(action); String msg =
	 * HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
	 * ufisTopicProducer.sendMessage(msg);
	 * LOG.debug("Sent Update LoadBag  Notification from BELT to UMD :\n{}",
	 * msg); }
	 */

	public static Date chgStringToDate(String dateString, String timeString) {
		DateFormat timeFormat = new SimpleDateFormat("HHmmss");
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		if (dateString == null) {
			return null;
		}
		try {
			cal1.setTime(dateFormat.parse(dateString));
			if (HpUfisUtils.isNullOrEmptyStr(timeString)) {
				cal.setTime(timeFormat.parse("000000"));
			} else {
				cal.setTime(timeFormat.parse(timeString));
			}
			cal.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
			cal.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
			cal.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH));
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			df.setTimeZone(TimeZone.getTimeZone("UTC"));
			return (new SimpleDateFormat("yyyyMMddHHmmss")).parse(df.format(cal
					.getTime()));
			// return df.parse(dateString);
		} catch (Exception e) {
			LOG.error("ERROR!!! in Formatting the Change Date and Time:" + e);
		}
		return null;
	}

	private String chgStringToFltDate(String flightDate) {
		String fltDate = null;
		DateFormat dmf = new SimpleDateFormat("ddMMM");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		try {
			if (HpUfisUtils.isNullOrEmptyStr(flightDate)) {
				LOG.error("Date is NULL or EMPTY.");
				return null;
			}
			cal1.setTime(dmf.parse(flightDate));
			cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));
			int currMonth = cal.get(Calendar.MONTH);
			int currYear = cal.get(Calendar.YEAR);
			// LOG.debug("cal1:" + cal1.getTime());
			if (cal1 == null) {
				LOG.error("Unable to parse the Flight Date:" + flightDate);
				return null;
			}
			if ((cal1.get(Calendar.MONTH) == Calendar.JANUARY)
					&& currMonth == Calendar.DECEMBER) {
				cal1.set(Calendar.YEAR, (currYear + 1));
			} else if ((cal1.get(Calendar.MONTH) == Calendar.DECEMBER)
					&& currMonth == Calendar.JANUARY) {
				cal1.set(Calendar.YEAR, (currYear - 1));
			}

			DateFormat df = new SimpleDateFormat("yyyyMMdd");

			fltDate = df.format(cal1.getTime());
			if (fltDate != null) {
				return fltDate;
			}
		} catch (ParseException e) {
			LOG.error("Exception :", e);
		}

		return null;
	}

	private Date convertDateToUTC(XMLGregorianCalendar xmlDate)
			throws ParseException {

		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = xmlDate.toGregorianCalendar().getTime();
		return formatter.parse(df.format(utcDate));
	}

	private void sendErrInfo(EnumExceptionCodes expCode, String desc,
			Long irmtabRef) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflStr);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflStr);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from BELT to Data Loader :\n{}", msg);

	}

	public String getAirLineFromCode(String value) {
		//LOG.info("Value:"+value);
		for (String key : arlnIataMap.keySet()) {			
			if (arlnIataMap.get(key)!=null && arlnIataMap.get(key).trim().equalsIgnoreCase("0"+value)) {
				//LOG.info("Key:"+key);
				return key;
			}
		}
		return null;
	}
}
