package com.ufis_as.ufisapp.ek.intf.belt;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpBeltConfig;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagMove;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagAction;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagHandleLoc;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagReconLoc;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.IDlLoadBagMoveUpdateLocal;
import com.ufis_as.ufisapp.ek.eao.IDlLoadBagUpdateLocal;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.belt.bagDetails.BagDetailsType;
import ek.belt.bagDetails.MsgSubType;
import ek.belt.bagDetails.ScanAct;

// import ek.belt.bagDetails.BagDetails;

@Stateless(mappedName = "BlTransformBeltBagScanUpdtBean")
@LocalBean
public class BlTransformBeltBagScanUpdtBean {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlTransformBeltBagScanUpdtBean.class);

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private IAfttabBeanLocal _afttabBean;
	@EJB
	private DlLoadUldBean _loadUldBean;
	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	private IDlLoadBagUpdateLocal _loadBagUpdate;
	@EJB
	private IDlLoadBagMoveUpdateLocal _loadBagMoveUpdate;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;

	// private DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

	private List<EntDbMdBagType> allBagTypes = new ArrayList<EntDbMdBagType>();
	private List<EntDbMdBagAction> allBagActions = new ArrayList<EntDbMdBagAction>();
	private List<EntDbMdBagReconLoc> allBagReconLocs = new ArrayList<EntDbMdBagReconLoc>();
	private List<EntDbMdBagHandleLoc> allBagHandleLocs = new ArrayList<EntDbMdBagHandleLoc>();
	private Map<String, String> idReconLocationMap = new HashMap<String, String>();
	private List<String> allReconLocCodes = new ArrayList<String>();
	private Map<String, String> idHandleLocationMap = new HashMap<String, String>();
	private Map<String, String> idHandleLocationNodeInfoMap = new HashMap<String, String>();
	private Map<String, String> locDescMap = new HashMap<String, String>();
	private Map<String, String> locBIMLocCodeMap = new HashMap<String, String>();
	private List<String> allHandleLocCodes = new ArrayList<String>();
	private List<EntDbMdBagHandleLoc> masterHandleLocs = new ArrayList<EntDbMdBagHandleLoc>();
	private List<EntDbMdBagReconLoc> masterReconLocs = new ArrayList<EntDbMdBagReconLoc>();
	List<String> masterBagTypes = new ArrayList<String>();
	List<String> masterBagAction = new ArrayList<String>();
	String dtflStr = HpEKConstants.BELT_SOURCE;
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;

	// private EntUfisMsgHead header;
	// List<String> fldList = new ArrayList<>();

	@PreDestroy
	private void initialClear() {
		allBagTypes.clear();
		allBagActions.clear();
		allBagReconLocs.clear();
		allBagHandleLocs.clear();
		idReconLocationMap.clear();
		allReconLocCodes.clear();
		idHandleLocationMap.clear();
		idHandleLocationNodeInfoMap.clear();
		locDescMap.clear();
		locBIMLocCodeMap.clear();
		allHandleLocCodes.clear();
		masterHandleLocs.clear();
		masterReconLocs.clear();
		masterBagTypes.clear();
		masterBagAction.clear();
		// fldList.clear();
	}

	public BlTransformBeltBagScanUpdtBean() {
		/*
		 * if (header == null) { header = new EntUfisMsgHead();
		 * header.setHop(HpEKConstants.HOPO); // notifyHeader.setReqt(reqt);
		 * header.setOrig(HpEKConstants.BELT_SOURCE);
		 * header.setApp(HpEKConstants.BELT_SOURCE);
		 * header.setUsr(HpEKConstants.BELT_SOURCE); } if (fldList.isEmpty()) {
		 * fldList.add("ID_FLIGHT"); fldList.add("ID_LOAD_BAG");
		 * fldList.add("ID_LOAD_ULD"); fldList.add("ID_MD_BAG_HANDLE_LOC");
		 * fldList.add("ID_MD_BAG_RECONCILE_LOC"); fldList.add("BAG_TAG");
		 * fldList.add("BAG_TYPE"); fldList.add("BAG_CLASS");
		 * fldList.add("PAX_REF_NUM"); fldList.add("PAX_NAME");
		 * fldList.add("CONTAINER_ID"); fldList.add("SCAN_LOC");
		 * fldList.add("CONTAINER_DEST3"); fldList.add("SCAN_ACTION");
		 * fldList.add("LOC_TYPE"); fldList.add("CHANGE_DATE");
		 * fldList.add("MSG_SEND_DATE"); fldList.add("REC_STATUS"); }
		 */
	}

	public boolean tranformFlightCrews(Message message,
			BagDetailsType bagDetails, Long irmtabRef) {
		// String cmdForBroadcasting = null;
		msgLogged = Boolean.FALSE;
		dtflStr = HpCommonConfig.dtfl;
		EntDbLoadBagMove loadBagMoveStat = null;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		// EntDbLoadBagMove oldLoadBagMove = new EntDbLoadBagMove();
		try {
			EntDbLoadBagMove loadBagMove = new EntDbLoadBagMove();

			if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
					.getScanLocation())
					|| (bagDetails.getBagInfo().getScanAction() == null)
					|| HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
							.getChangeDate())
					|| HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
							.getChangeTime())) {
				LOG.warn(
						"Dropped Message details( BagInfo Mandatory values(BagScanLoc,ScanAction,ChangeTime,ChangeDate) are not specified): "
								+ "BagTag = {}, "
								+ "Flight info = {}, "
								+ " Flight Date = {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								bagDetails.getFlightInfo()
										.getFullFlightNumber(),
								bagDetails.getFlightInfo().getFlightDate() });

				if (HpCommonConfig.irmtab
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflStr);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EMAND,
							"BagInfo Mandatory values missing.Bag Tag:"
									+ bagDetails.getBagInfo().getBagTag()
									+ "  Flight Number:"
									+ bagDetails.getFlightInfo()
											.getFullFlightNumber()
									+ "  Flight Date:"
									+ bagDetails.getFlightInfo()
											.getFlightDate(), irmtabRef);
				}

				return false;
			}
			if (_startupInitSingleton.isMdBagTypeOn() && allBagTypes.isEmpty()) {
				allBagTypes = _startupInitSingleton.getMdBagTypeList();
				for (EntDbMdBagType bagTypeRec : allBagTypes) {
					if ((bagTypeRec.getRecStatus() != null)
							&& !bagTypeRec.getRecStatus().equalsIgnoreCase("X")) {
						masterBagTypes.add(bagTypeRec.getBagTypeCode());
					}
				}
				LOG.debug("Total valid Master BagTypes records: {}",
						masterBagTypes.size());
			}
			if (_startupInitSingleton.isMdBagActionOn()
					&& allBagActions.isEmpty()) {
				allBagActions = _startupInitSingleton.getMdBagActionList();
				for (EntDbMdBagAction bagActRec : allBagActions) {
					if ((bagActRec.getRecStatus() != null)
							&& !bagActRec.getRecStatus().equalsIgnoreCase("X")) {
						masterBagAction.add(bagActRec.getBagActionCode());
					}
				}
				LOG.debug("Total valid Master BagAction records: {}",
						masterBagAction.size());
			}
			if (_startupInitSingleton.isMdBagHandleLocOn()
					&& allBagHandleLocs.isEmpty()) {
				allBagHandleLocs = _startupInitSingleton
						.getMdBagHandleLocList();

				for (EntDbMdBagHandleLoc bagHandleRec : allBagHandleLocs) {
					if ((bagHandleRec.getRecStatus() != null)
							&& !bagHandleRec.getRecStatus().equalsIgnoreCase(
									"X")) {
						masterHandleLocs.add(bagHandleRec);
					}
				}
				LOG.debug("Total valid Master BagHandleLocs records: {}",
						masterHandleLocs.size());
				if (!masterHandleLocs.isEmpty()) {
					for (EntDbMdBagHandleLoc bagHandleLocs : masterHandleLocs) {
						if (!allHandleLocCodes.contains(bagHandleLocs
								.getLocCode())) {
							allHandleLocCodes.add(bagHandleLocs.getLocCode());
						}
						idHandleLocationMap.put(bagHandleLocs.getLocCode(),
								bagHandleLocs.getId());
						idHandleLocationNodeInfoMap.put(
								bagHandleLocs.getLocCode(),
								bagHandleLocs.getNodeInfo());
						locBIMLocCodeMap.put(bagHandleLocs.getLocCode(),
								bagHandleLocs.getBimLocCode());
						locDescMap.put(bagHandleLocs.getLocCode(),
								bagHandleLocs.getLocDesc());
					}
				}
			}
			if (_startupInitSingleton.isMdBagReconLocOn()
					&& allBagReconLocs.isEmpty()) {
				allBagReconLocs = _startupInitSingleton.getMdBagReconLocList();
				for (EntDbMdBagReconLoc bagReconLocRec : allBagReconLocs) {
					if ((bagReconLocRec.getRecStatus() != null)
							&& !bagReconLocRec.getRecStatus().equalsIgnoreCase(
									"X")) {
						masterReconLocs.add(bagReconLocRec);
					}
				}
				LOG.debug("Total valid Master BagReconLocs records: {}",
						masterReconLocs.size());
				if (!masterReconLocs.isEmpty()) {
					for (EntDbMdBagReconLoc bagReconLocs : masterReconLocs) {
						if (!allReconLocCodes.contains(bagReconLocs
								.getLocCode())) {
							allReconLocCodes.add(bagReconLocs.getLocCode());
						}
						idReconLocationMap.put(bagReconLocs.getLocCode(),
								bagReconLocs.getId());
					}
				}
			}
			long startTime = new Date().getTime();

			if (_startupInitSingleton.isMdBagTypeOn()
					&& (!masterBagTypes.isEmpty() && !(masterBagTypes
							.contains(bagDetails.getBagInfo().getBagType()
									.value())))) {
				LOG.error(
						"Dropping the message .Specified BagType is not available in Master Data BagTypes for the Flight :"
								+ "BagTag = {}, "
								+ "flight Number = {}, "
								+ "Flight Date= {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								bagDetails.getFlightInfo()
										.getFullFlightNumber(),
								bagDetails.getFlightInfo().getFlightDate() });

				if (HpCommonConfig.irmtab
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflStr);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.ENOMD,
							"BagType not in MasterData.Bag Type: "
									+ bagDetails.getBagInfo().getBagType()
											.value()
									+ "  Bag Tag:"
									+ bagDetails.getBagInfo().getBagTag()
									+ "  Flight Number:"
									+ bagDetails.getFlightInfo()
											.getFullFlightNumber()
									+ "  Flight Date:"
									+ bagDetails.getFlightInfo()
											.getFlightDate(), irmtabRef);

				}

				return false;

			}

			if (_startupInitSingleton.isMdBagActionOn()
					&& (!masterBagAction.isEmpty()
							&& bagDetails.getBagInfo().getScanAction() != null && !(masterBagAction
								.contains(bagDetails.getBagInfo()
										.getScanAction().value())))) {
				LOG.error(
						"Dropping the message .The specified Scan Action value not available in Master Data Bag Scan Action for the Flight :"
								+ "BagTag = {}, "
								+ "flight Number = {}, "
								+ "Flight Date= {}, ", new Object[] {
								bagDetails.getBagInfo().getBagTag(),
								bagDetails.getFlightInfo()
										.getFullFlightNumber(),
								bagDetails.getFlightInfo().getFlightDate() });

				if (HpCommonConfig.irmtab
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(message, dtflStr);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.ENOMD,
							"Scan Action not in Master data.Scan Action:"
									+ bagDetails.getBagInfo().getScanAction()
									+ "  Bag Tag:"
									+ bagDetails.getBagInfo().getBagTag()
									+ "  Flight Number:"
									+ bagDetails.getFlightInfo()
											.getFullFlightNumber()
									+ "  Flight Date:"
									+ bagDetails.getFlightInfo()
											.getFlightDate(), irmtabRef);

				}
				return false;

			}
			if (bagDetails.getFlightInfo() != null) {
				String urno = null;

				String flightNumber = bagDetails.getFlightInfo()
						.getCarrierCode() == null ? " "
						: bagDetails.getFlightInfo().getCarrierCode()
								.substring(0, 2)
								+ " "
								+ HpUfisUtils.formatCedaFltn(String
										.valueOf(bagDetails.getFlightInfo()
												.getFlightNumber()))
								+ (bagDetails.getFlightInfo().getFlightSuffix() == null ? ""
										: bagDetails.getFlightInfo()
												.getFlightSuffix());
				String fltDate = chgStringToFltDate(bagDetails.getFlightInfo()
						.getFlightDate());
				LOG.debug("flightNumber: " + flightNumber + "   FlightDate:"
						+ fltDate);
				long idFlight = 0;
				// if (HpBeltConfig.getGetLinkTables().equalsIgnoreCase(
				// HpEKBeltConfigConstants.CON_YES)) {
				// if (!HpBeltConfig.getAfttabIstaExlList().isEmpty()) {
				// startTime = new Date().getTime();
				// // urno = _afttabBean.getUrnoFoFlDtOrgExlIsta(flightNumber,
				// // bagDetails.getFlightInfo().getDepartureAirport(),
				// // bagDetails
				// // .getFlightInfo().getArrivalAirport(), fltDate,
				// // HpBeltConfig.getAfttabIstaExlList());
				// urno = _afttabBean.getUrnoFoFlDtAdidExlIsta(flightNumber,
				// bagDetails.getFlightInfo().getArrivalAirport(), fltDate,
				// HpBeltConfig.getAfttabIstaExlList());
				//
				// LOG.debug("Total Duration on searching flight from AFTTAB (in ms): {}",
				// new Date().getTime() - startTime);
				// } else {

				// Change
				EntDbAfttab criteriaParams = new EntDbAfttab();
				// startTime = new Date().getTime();
				criteriaParams.setFlno(flightNumber);
				criteriaParams.setFlda(fltDate);
				// criteriaParams.setAdid('D');
				if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getFlightInfo()
						.getArrivalAirport())) {
					urno = _afttabBean.getUrnoByFldaFlnoAdid(criteriaParams,
							null, bagDetails.getFlightInfo()
									.getDepartureAirport());
				} else {
					urno = _afttabBean.getUrnoByFldaFlnoAdid(criteriaParams,
							bagDetails.getFlightInfo().getArrivalAirport(),
							bagDetails.getFlightInfo().getDepartureAirport());
				}

				// startTime = new Date().getTime();
				// // urno = _afttabBean.getUrnoFoFlDtOrg(flightNumber,
				// // bagDetails.getFlightInfo().getDepartureAirport(),
				// // bagDetails.getFlightInfo()
				// // .getArrivalAirport(), fltDate);
				// urno = _afttabBean.getUrnoByFldtAdid(flightNumber,
				// bagDetails.getFlightInfo().getArrivalAirport(), fltDate);
				// LOG.debug("Total Duration on searching flight from AFTTAB (in ms): {}",
				// new Date().getTime() - startTime);
				// }

				if (urno == null || String.valueOf(urno) == null
						|| urno.isEmpty()) {
					urno = new Long(0).toString();
					LOG.error(
							"No flight is available in AFTTAB with the Flight details:"
									+ "BagTag = {}, " + "flight Number = {}, "
									+ "Flight Date= {}, ", new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									flightNumber, fltDate });

					if (HpCommonConfig.irmtab
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade
									.storeRawMsg(message, dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.ENOFL,
								"Flight Not Found.Bag Tag:"
										+ bagDetails.getBagInfo().getBagTag()
										+ "  Flight Number:"
										+ bagDetails.getFlightInfo()
												.getFullFlightNumber()
										+ "  Flight Date:"
										+ bagDetails.getFlightInfo()
												.getFlightDate(), irmtabRef);
					}
				}
				idFlight = Long.parseLong(urno);
				// }
				// long idFlight = (urno != null && (String.valueOf(urno) !=
				// null) && (!urno
				// .isEmpty())) ? Long.parseLong(urno) : 0;

				String recStatus = " ";
				// if (idFlight != 0) {
				loadBagMove.setIdFlight(new BigDecimal(idFlight));
				// }
				loadBagMove.setFlightNumber(flightNumber);
				loadBagMove.setFltDate(fltDate);
				loadBagMove.setMsgSendDate(convertDateToUTC(bagDetails
						.getMeta().getMessageTime()));
				loadBagMove.setFltOrigin3(bagDetails.getFlightInfo()
						.getDepartureAirport());
				loadBagMove.setFltDest3(bagDetails.getFlightInfo()
						.getArrivalAirport());

				try {
					loadBagMove.setBagTag(bagDetails.getBagInfo().getBagTag());
					// if (HpBeltConfig.getGetLinkTables().equalsIgnoreCase(
					// HpEKBeltConfigConstants.CON_YES)) {
					startTime = new Date().getTime();
					// List<EntDbLoadBag> entLoadBag = _loadBagUpdate
					// .getBagMoveDetailsByBagTag(idFlight,
					// bagDetails.getBagInfo()
					// .getBagTag());
					if (idFlight != 0) {
						EntDbLoadBag entLoadBag = _loadBagUpdate
								.getValidBagMoveDetailsByBagTag(idFlight,
										bagDetails.getBagInfo().getBagTag());
						LOG.info(
								"Total Duration on searching LOAD_BAG (idFlight{} and BagTag{}) (in ms): {}",
								idFlight, bagDetails.getBagInfo().getBagTag(),
								new Date().getTime() - startTime);
						if (entLoadBag != null) {
							loadBagMove.setIdLoadBag(entLoadBag.getId());
						}
					}/*
					 * else { LOG.error(
					 * "No record found in LOAD_BAG table for the Bag Move update details:"
					 * + "BagTag = {}, " + "IdFlight = {} ", new Object[] {
					 * bagDetails.getBagInfo().getBagTag(), idFlight });
					 * 
					 * if (HpCommonConfig.irmtab .equalsIgnoreCase(
					 * HpUfisAppConstants.IrmtabLogLev.LOG_ERR .name())) { if
					 * (!msgLogged) { irmtabRef =
					 * _irmfacade.storeRawMsg(message, dtflStr); msgLogged =
					 * Boolean.TRUE; } } if (irmtabRef > 0) {
					 * sendErrInfo(EnumExceptionCodes.ENOBG,
					 * "Bag details not found.Bag Tag:" +
					 * bagDetails.getBagInfo() .getBagTag() + "  Flight Number:"
					 * + bagDetails.getFlightInfo() .getFullFlightNumber() +
					 * "  Flight Date:" + bagDetails.getFlightInfo()
					 * .getFlightDate(), irmtabRef);
					 * 
					 * }
					 * 
					 * return false; }
					 */
					// }
				} catch (NumberFormatException nfe) {
					LOG.error("!!!! Error in Converting the BagTag from String to long");
				}

				if (HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
						.getContainerID())) {
					LOG.warn(
							"Container ID is null. Skip the search in LOAD_ULD table:"
									+ "BagTag = {}, " + "flight Number = {}, "
									+ "Flight Date= {}, ", new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									flightNumber, fltDate });
				} else {
					EntDbLoadUld containerId = null;
					// if (HpBeltConfig.getGetLinkTables().equalsIgnoreCase(
					// HpEKBeltConfigConstants.CON_YES)) {
					startTime = new Date().getTime();
					if (idFlight != 0) {
						containerId = _loadUldBean.getUldNum(new BigDecimal(
								idFlight), bagDetails.getBagInfo()
								.getContainerID());
						LOG.debug(
								"Total Duration on searching ULD from LOAD_ULD (in ms): {}",
								new Date().getTime() - startTime);
						// !!!!!********** need to UNDO
						if (containerId == null) {
							LOG.warn("No ULD is found in LOAD_ULD "
									+ "idFlight:" + idFlight
									+ "   ContainerID:"
									+ bagDetails.getBagInfo().getContainerID()
									+ " for the Flight details:"
									+ "BagTag = {}, " + "flight Number = {}, "
									+ "Flight Date= {}, ", new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									flightNumber, fltDate });
							// return false;

							if (HpCommonConfig.irmtab
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(message,
											dtflStr);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(EnumExceptionCodes.WNOUD,
										"Container ID not found. Container ID: "
												+ bagDetails.getBagInfo()
														.getContainerID()
												+ "  Bag Tag:"
												+ bagDetails.getBagInfo()
														.getBagTag()
												+ "  Flight Number:"
												+ bagDetails.getFlightInfo()
														.getFullFlightNumber()
												+ "  Flight Date:"
												+ bagDetails.getFlightInfo()
														.getFlightDate(),
										irmtabRef);

							}

						}
						// }
						if (containerId != null) {
							loadBagMove.setIdLoadUld(containerId.getId());
						}
					}
				}
				loadBagMove.setBagType(bagDetails.getBagInfo().getBagType()
						.value());
				if (!HpUfisUtils.isNullOrEmptyStr(bagDetails.getBagInfo()
						.getBagClass())) {
					if (!HpBeltConfig.getBusinessClassBagList().isEmpty()
							&& HpBeltConfig.getBusinessClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBagMove.setBagClass("J");
					} else if (!HpBeltConfig.getFirstClassBagList().isEmpty()
							&& HpBeltConfig.getFirstClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBagMove.setBagClass("F");
					} else if (!HpBeltConfig.getEconomyClassBagList().isEmpty()
							&& HpBeltConfig.getEconomyClassBagList().contains(
									bagDetails.getBagInfo().getBagClass())) {
						loadBagMove.setBagClass("Y");
					} else {
						LOG.info(
								"Specified Bag Class is not in configured list for the Flight :"
										+ "BagTag = {}, "
										+ "flight Number = {}, "
										+ "Flight Date= {}, ", new Object[] {
										bagDetails.getBagInfo().getBagTag(),
										bagDetails.getFlightInfo()
												.getFlightNumber(),
										bagDetails.getFlightInfo()
												.getFlightDate() });

						if (HpCommonConfig.irmtab
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(message,
										dtflStr);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(EnumExceptionCodes.WBGCL,
									"BagClass is not in config list.Bag Class: "
											+ bagDetails.getBagInfo()
													.getBagClass()
											+ "  Bag Tag:"
											+ bagDetails.getBagInfo()
													.getBagTag()
											+ "  Flight Number:"
											+ bagDetails.getFlightInfo()
													.getFullFlightNumber()
											+ "  Flight Date:"
											+ bagDetails.getFlightInfo()
													.getFlightDate(), irmtabRef);

						}

						loadBagMove.setBagClass(bagDetails.getBagInfo()
								.getBagClass());
					}
				}
				loadBagMove.setContainerId(bagDetails.getBagInfo()
						.getContainerID());
				loadBagMove.setContainerDest3(bagDetails.getBagInfo()
						.getContainerDest());
				loadBagMove.setPaxName(bagDetails.getBagInfo()
						.getPassengerName());
				loadBagMove.setPaxRefNum(bagDetails.getBagInfo()
						.getPassengerSeqNo());
				loadBagMove.setScanAction(bagDetails.getBagInfo()
						.getScanAction().value());
				loadBagMove.setScanLoc(bagDetails.getBagInfo()
						.getScanLocation());
				loadBagMove.setLocDesc(recStatus);
				loadBagMove.setBimLocCode(recStatus);
				loadBagMove.setNodeInfo(recStatus);
				loadBagMove.setRecStatus(recStatus);
				LOG.debug("Scan Location {}", loadBagMove.getScanLoc());
				// loadBagMove.setLocType(bagDetails.getBagInfo().getScanLocation()
				// .substring(0, 2));
				if (loadBagMove.getScanAction().equalsIgnoreCase(
						ScanAct.BHS_SCAN.value())) {
					String locType = bagDetails.getBagInfo().getScanLocation()
							.substring(0, 2);
					String location = bagDetails.getBagInfo().getScanLocation()
							.substring(2);
					if (_startupInitSingleton.isMdBagHandleLocOn()) {
						if (locType.equalsIgnoreCase("EN")
								|| locType.equalsIgnoreCase("EX")) {
							if (!allHandleLocCodes.isEmpty()
									&& allHandleLocCodes.contains(location)
									&& !idHandleLocationMap.isEmpty()
									&& (locType.equalsIgnoreCase("EN") || (locType
											.equalsIgnoreCase("EX")
											&& idHandleLocationNodeInfoMap
													.containsKey(location)
											&& !HpUfisUtils
													.isNullOrEmptyStr(idHandleLocationNodeInfoMap
															.get(location)) && idHandleLocationNodeInfoMap
											.get(location).substring(0, 2)
											.equalsIgnoreCase(locType)))) {
								loadBagMove.setScanLoc(location);
								loadBagMove.setLocType(locType);
								loadBagMove
										.setIdMdBagHandleLoc(idHandleLocationMap
												.get(location));
								loadBagMove
										.setLocDesc(locDescMap.get(location));
								loadBagMove.setBimLocCode(locBIMLocCodeMap
										.get(location));
								loadBagMove
										.setNodeInfo(idHandleLocationNodeInfoMap
												.get(location));
							} else {
								LOG.error(
										"Dropping the message .No Scan location related data found in master table MD_BAG_HANDLE_LOC , for the Flight details:"
												+ "BagTag = {}, "
												+ "flight Number = {}, "
												+ "Flight Date= {}, ",
										new Object[] {
												bagDetails.getBagInfo()
														.getBagTag(),
												flightNumber, fltDate });

								if (HpCommonConfig.irmtab
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflStr);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(
											EnumExceptionCodes.ENOMD,
											"Scan Location not in MasterData.Scan Loc: "
													+ bagDetails.getBagInfo()
															.getScanLocation()
													+ "  Bag Tag:"
													+ bagDetails.getBagInfo()
															.getBagTag()
													+ "  Flight Number:"
													+ bagDetails
															.getFlightInfo()
															.getFullFlightNumber()
													+ "  Flight Date:"
													+ bagDetails
															.getFlightInfo()
															.getFlightDate(),
											irmtabRef);

								}

								return false;
							}
						} else {
							if (!allHandleLocCodes.isEmpty()
									&& allHandleLocCodes.contains(loadBagMove
											.getScanLoc())
									&& !idHandleLocationMap.isEmpty()) {
								loadBagMove
										.setIdMdBagHandleLoc(idHandleLocationMap
												.get(loadBagMove.getScanLoc()));
								loadBagMove.setLocDesc(locDescMap
										.get(loadBagMove.getScanLoc()));
								loadBagMove.setBimLocCode(locBIMLocCodeMap
										.get(loadBagMove.getScanLoc()));
								loadBagMove
										.setNodeInfo(idHandleLocationNodeInfoMap
												.get(loadBagMove.getScanLoc()));
							} else {
								LOG.error(
										"Dropping the message(Not EN or EX) .No Scan location related data found in master table MD_BAG_HANDLE_LOC , for the Flight details:"
												+ "BagTag = {}, "
												+ "flight Number = {}, "
												+ "Flight Date= {}, ",
										new Object[] {
												bagDetails.getBagInfo()
														.getBagTag(),
												flightNumber, fltDate });
								if (HpCommonConfig.irmtab
										.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
												.name())) {
									if (!msgLogged) {
										irmtabRef = _irmfacade.storeRawMsg(
												message, dtflStr);
										msgLogged = Boolean.TRUE;
									}
								}
								if (irmtabRef > 0) {
									sendErrInfo(
											EnumExceptionCodes.ENOMD,
											"Scan Loc(neither EN nor EX) not in MasterData.Scan Loc: "
													+ bagDetails.getBagInfo()
															.getScanLocation()
													+ "  Bag Tag:"
													+ bagDetails.getBagInfo()
															.getBagTag()
													+ "  Flight Number:"
													+ bagDetails
															.getFlightInfo()
															.getFullFlightNumber()
													+ "  Flight Date:"
													+ bagDetails
															.getFlightInfo()
															.getFlightDate(),
											irmtabRef);

								}
								return false;
							}
						}
					} else {
						if (locType.equalsIgnoreCase("EN")
								|| locType.equalsIgnoreCase("EX")) {
							loadBagMove.setScanLoc(location);
							loadBagMove.setLocType(locType);
						}
					}
				}

				else {
					if (_startupInitSingleton.isMdBagReconLocOn()) {
						if (!allReconLocCodes.isEmpty()
								&& allReconLocCodes.contains(loadBagMove
										.getScanLoc())
								&& !idReconLocationMap.isEmpty()) {
							loadBagMove
									.setIdMdBagReconcileLoc(idReconLocationMap
											.get(loadBagMove.getScanLoc()));
						} else {
							LOG.error(
									"Dropping the message .No Scan location related data found in master table MD_BAG_RECON_LOC , for the Flight details:"
											+ "BagTag = {}, "
											+ "flight Number = {}, "
											+ "Flight Date= {}, ",
									new Object[] {
											bagDetails.getBagInfo().getBagTag(),
											flightNumber, fltDate });

							if (HpCommonConfig.irmtab
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(message,
											dtflStr);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(EnumExceptionCodes.ENOMD,
										"Scan Loc not in MasterData.Scan Loc: "
												+ bagDetails.getBagInfo()
														.getScanLocation()
												+ "  Bag Tag:"
												+ bagDetails.getBagInfo()
														.getBagTag()
												+ "  Flight Number:"
												+ bagDetails.getFlightInfo()
														.getFullFlightNumber()
												+ "  Flight Date:"
												+ bagDetails.getFlightInfo()
														.getFlightDate(),
										irmtabRef);

							}

							return false;
						}
					}
				}
				loadBagMove.setChangeDate(chgStringToDate(
						chgStringToFltDate(bagDetails.getBagInfo()
								.getChangeDate()), bagDetails.getBagInfo()
								.getChangeTime()));

				if (bagDetails.getMeta().getMessageSubtype().value()
						.equalsIgnoreCase(MsgSubType.UPD.value())) {
					loadBagMoveStat = _loadBagMoveUpdate
							.updateLoadedBagMove(loadBagMove);
					/*
					 * cmdForBroadcasting =
					 * HpUfisAppConstants.UfisASCommands.IRT .toString();
					 */
					if (loadBagMoveStat != null) {
						/*
						 * sendLoadBagUpdateToCedaInJson(loadBagMoveStat, null,
						 * cmdForBroadcasting, true);
						 */
						if (HpUfisUtils
								.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
							// send notification message to topic
							ufisTopicProducer.sendNotification(true,
									HpUfisAppConstants.UfisASCommands.IRT,
									String.valueOf(loadBagMoveStat
											.getIdFlight()), null,
									loadBagMoveStat);
						}
						if (HpUfisUtils
								.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
							// if topic not defined, send notification to queue
							ufisQueueProducer.sendNotification(true,
									HpUfisAppConstants.UfisASCommands.IRT,
									String.valueOf(loadBagMoveStat
											.getIdFlight()), null,
									loadBagMoveStat);
						}
						LOG.debug("Bag Record change has beed communicated to Ceda.");
					}
					LOG.info("Record inserted Successfully to LOAD_BAG_MOVE");
				} else {
					LOG.error(
							"Dropping the message .Only update(UPD) message is allowed. Flight details:"
									+ "BagTag = {}, "
									+ "flight Number = {}, "
									+ "Flight Date= {}, ", new Object[] {
									bagDetails.getBagInfo().getBagTag(),
									flightNumber, fltDate });

					if (HpCommonConfig.irmtab
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade
									.storeRawMsg(message, dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.EWACT,
								"Not an UPD message.Bag Tag:"
										+ bagDetails.getBagInfo().getBagTag()
										+ "  Flight Number:"
										+ bagDetails.getFlightInfo()
												.getFullFlightNumber()
										+ "  Flight Date:"
										+ bagDetails.getFlightInfo()
												.getFlightDate(), irmtabRef);

					}

					return false;
				}

			}
			// if (loadBagMoveStat != null) {
			//
			// sendLoadBagUpdateToCedaInJson(loadBagMoveStat, null,
			// cmdForBroadcasting, true);
			// LOG.debug("Input bag move update has been processed.");
			// }
		} catch (NumberFormatException nfe) {
			LOG.error(
					"!!!! Error in Converting the urno(AFTTAB) from String to long. Exception {}",
					nfe);
		} catch (Exception e) {
			LOG.error("Exception :", e);
		}
		return true;
	}

	/*
	 * private void sendLoadBagUpdateToCedaInJson(EntDbLoadBagMove
	 * entLoadBagMove, EntDbLoadBagMove oldEntLoadBagMove, String cmd, Boolean
	 * isLast) throws IOException, JsonGenerationException, JsonMappingException
	 * { EntUfisMsgDTO msgDTO = new EntUfisMsgDTO(); String urno = new
	 * Long(entLoadBagMove.getIdFlight()).toString(); if (isLast) {
	 * header.setBcnum(-1); } // get the HEAD info List<String> idFlightList =
	 * new ArrayList<>(); idFlightList.add(urno);
	 * header.setIdFlight(idFlightList);
	 * 
	 * // get the BODY info EntUfisMsgBody body = new EntUfisMsgBody();
	 * 
	 * List<Object> dataList = new ArrayList<>(); dataList.add(urno);
	 * dataList.add(entLoadBagMove.getIdLoadBag());
	 * dataList.add(entLoadBagMove.getIdLoadUld());
	 * dataList.add(entLoadBagMove.getIdMdBagHandleLoc());
	 * dataList.add(entLoadBagMove.getIdMdBagReconcileLoc());
	 * dataList.add(entLoadBagMove.getBagTag());
	 * dataList.add(entLoadBagMove.getBagType());
	 * dataList.add(entLoadBagMove.getBagClass());
	 * dataList.add(entLoadBagMove.getPaxRefNum());
	 * dataList.add(entLoadBagMove.getPaxName());
	 * dataList.add(entLoadBagMove.getContainerId());
	 * dataList.add(entLoadBagMove.getScanLoc());
	 * dataList.add(entLoadBagMove.getContainerDest3());
	 * dataList.add(entLoadBagMove.getScanAction());
	 * dataList.add(entLoadBagMove.getLocType());
	 * dataList.add(df.format(entLoadBagMove.getChangeDate()));
	 * dataList.add(df.format(entLoadBagMove.getMsgSendDate()));
	 * dataList.add(entLoadBagMove.getRecStatus());
	 * 
	 * List<String> oldList = new ArrayList<>(); if
	 * (UfisASCommands.URT.toString().equals(cmd) && oldEntLoadBagMove != null)
	 * { oldList.add(new Long(oldEntLoadBagMove.getIdFlight()).toString());
	 * oldList.add(oldEntLoadBagMove.getIdLoadBag().toString());
	 * oldList.add(oldEntLoadBagMove.getIdLoadUld());
	 * oldList.add(oldEntLoadBagMove.getIdMdBagHandleLoc());
	 * oldList.add(oldEntLoadBagMove.getIdMdBagReconcileLoc());
	 * oldList.add(oldEntLoadBagMove.getBagTag());
	 * oldList.add(oldEntLoadBagMove.getBagType());
	 * oldList.add(oldEntLoadBagMove.getBagClass());
	 * oldList.add(oldEntLoadBagMove.getPaxRefNum());
	 * oldList.add(oldEntLoadBagMove.getPaxName());
	 * oldList.add(oldEntLoadBagMove.getContainerId());
	 * oldList.add(oldEntLoadBagMove.getScanLoc());
	 * oldList.add(oldEntLoadBagMove.getContainerDest3());
	 * oldList.add(oldEntLoadBagMove.getScanAction());
	 * oldList.add(oldEntLoadBagMove.getLocType());
	 * oldList.add(df.format(oldEntLoadBagMove.getChangeDate()));
	 * oldList.add(df.format(oldEntLoadBagMove.getMsgSendDate()));
	 * oldList.add(oldEntLoadBagMove.getRecStatus()); }
	 * 
	 * List<String> idList = new ArrayList<>();
	 * idList.add(entLoadBagMove.getId());
	 * 
	 * List<EntUfisMsgACT> listAction = new ArrayList<>(); EntUfisMsgACT action
	 * = new EntUfisMsgACT(); action.setCmd(cmd);
	 * action.setTab("LOAD_BAG_MOVE"); action.setFld(fldList);
	 * action.setData(dataList); action.setId(idList); action.setSel(urno);
	 * 
	 * listAction.add(action); body.setActs(listAction);
	 * 
	 * msgDTO.setHead(header); msgDTO.setBody(body);
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); String msg =
	 * mapper.writeValueAsString(msgDTO); ufisTopicProducer.sendMessage(msg);
	 * LOG.debug(
	 * "Sent Update LoadBagMove  Notification from BELT to UMD :\n{}", msg); }
	 */

	public static Date chgStringToDate(String dateString, String timeString) {
		DateFormat timeFormat = new SimpleDateFormat("HHmmss");
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		if (dateString == null) {
			return null;
		}
		try {
			cal1.setTime(dateFormat.parse(dateString));
			if (HpUfisUtils.isNullOrEmptyStr(timeString)) {
				cal.setTime(timeFormat.parse("000000"));
			} else {
				cal.setTime(timeFormat.parse(timeString));
			}
			cal.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
			cal.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
			cal.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH));
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			df.setTimeZone(TimeZone.getTimeZone("UTC"));
			return (new SimpleDateFormat("yyyyMMddHHmmss")).parse(df.format(cal
					.getTime()));
			// return df.parse(dateString);
		} catch (Exception e) {
			LOG.error("ERROR!!! in Formatting the Change Date and Time:" + e);
		}
		return null;
	}

	private String chgStringToFltDate(String flightDate) {
		String fltDate = null;
		DateFormat dmf = new SimpleDateFormat("ddMMM");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		try {
			if (HpUfisUtils.isNullOrEmptyStr(flightDate)) {
				LOG.error("Date is NULL or EMPTY.");
				return null;
			}
			cal1.setTime(dmf.parse(flightDate));
			cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));
			int currMonth = cal.get(Calendar.MONTH);
			int currYear = cal.get(Calendar.YEAR);
			// LOG.debug("cal1:" + cal1.getTime());
			if (cal1 == null) {
				LOG.error("Unable to parse the Flight Date:" + flightDate);
				return null;
			}
			if ((cal1.get(Calendar.MONTH) == Calendar.JANUARY)
					&& currMonth == Calendar.DECEMBER) {
				cal1.set(Calendar.YEAR, (currYear + 1));
			} else if ((cal1.get(Calendar.MONTH) == Calendar.DECEMBER)
					&& currMonth == Calendar.JANUARY) {
				cal1.set(Calendar.YEAR, (currYear - 1));
			}

			DateFormat df = new SimpleDateFormat("yyyyMMdd");

			fltDate = df.format(cal1.getTime());
			if (fltDate != null) {
				return fltDate;
			}
		} catch (ParseException e) {
			LOG.error("Exception :", e);
		}

		return null;
	}

	private Date convertDateToUTC(XMLGregorianCalendar xmlDate)
			throws ParseException {

		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = xmlDate.toGregorianCalendar().getTime();
		return formatter.parse(df.format(utcDate));
	}

	private void sendErrInfo(EnumExceptionCodes expCode, String desc,
			Long irmtabRef) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflStr);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflStr);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from BELT to Data Loader.");

	}

}
