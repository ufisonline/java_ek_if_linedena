package com.ufis_as.ufisapp.ek.intf.bms;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.bms.entities.EntDbCrewCheckin;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlCrewCheckInBean;
import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.bms.CrewCheckIn;
/**
 * @author BTR
 * @version 1:		2013-07-25 BTR - Released new version to read from the external config.
 * @version 1.1.0:	2013-10-14 BTR 1.) Modified for internal exception handling to UFISDATASTORE
 * 								   2.) Changed to use new table definition of EntDbFltJobAssign
 * @version 1.1.1:  2013-12-11 DMO - Standardize notification process
 * @version 1.1.2	2014-01-07 BTR	- Changed to use common method to get the flight number(formatCedaFlno())
 * 									- Changed default value of REC_STATUS = " "
 * 									- Modified to query flight job assign with staff_Type = :staff_Type AND staff_type_code = "CREW"
 * @version 1.1.3	2014-01-15 BTR	- Changed to use FLDA instead of FLUT (Asked by Ritu on 15Jan2014)
 * @version 1.1.4	2014-01-15 BTR	- Changed to test with tibco conn password
 * @version 1.1.5	2014-01-17 BTR	- Changed to use decrypted tibco conn password function in tibco reconx
 * @version 1.1.6	2014-01-20 BTR	- Added for backward update flt_job_assign.  
 *  
 */
@Stateless
public class BlHandleCrewCheckInBean extends BaseDTO{

	private static Logger LOG = LoggerFactory.getLogger(BlHandleCrewCheckInBean.class);
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;
	@EJB
	private BlIrmtabFacade clsBlIrmtabFacade;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private IDlFlighttJobAssignLocal clsIDlFltJobAssignLocal;
	@EJB
	private DlCrewCheckInBean clsDlCrewCheckInBean;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	/*@EJB
    BlUfisBCTopic clsBlUfisBCTopic;*/
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
	DateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
	DateFormat inputFlDate = new SimpleDateFormat("MM/dd/yyyy");
	DateFormat dfDate = new SimpleDateFormat("yyyyMMdd");
	
	public BlHandleCrewCheckInBean() {
	}
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(CrewCheckIn.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	
	public boolean processCrewCheckIn(String message, long irmtabRef){
	  	// clear
    	data.clear();
    	
    	// set urno of irmtab
    	irmtabUrno = irmtabRef;
		try {
			CrewCheckIn _input = (CrewCheckIn) _um.unmarshal(new StreamSource(new StringReader(message)));
			String legNum = String.valueOf(_input.getFlightID().getLegNo());
			String depNum = String.valueOf(_input.getFlightID().getDepNo());
			//check all madantory fields
			if(_input.getMeta().getMessageTime() == null
					|| HpUfisUtils.isNullOrEmptyStr(String.valueOf(_input.getMeta().getMessageID()))
					|| HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getType())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getSubtype())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getSource())){
				LOG.error("Input meta mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(!"BMS".equals(_input.getMeta().getSource())){
				//clsBlIrmtabFacade.updateIRMStatus("UnknownSrc");
				LOG.error("Processing performs only for BMS Source. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWSRC.name(), _input.getMeta().getSource());
				return false;
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getCxCd())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getFltDate())
					|| HpUfisUtils.isNullOrEmptyStr(String.valueOf(_input.getFlightID().getFltNo()))
					|| (HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getArrStn())&&
							HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getDepStn()))
					|| HpUfisUtils.isNullOrEmptyStr(depNum)
					|| HpUfisUtils.isNullOrEmptyStr(legNum)){
				LOG.error("Input FlightID mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(!_input.getFlightID().getCxCd().contains("EK")){
				LOG.error("Processing performs only for EK flight. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWALC.name(), _input.getFlightID().getCxCd());
				return false;
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getCheckInInfo().getStaffno())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCheckInInfo().getStaffName())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCheckInInfo().getOperatingGrade())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCheckInInfo().getCrewType())
					|| _input.getCheckInInfo().getCheckInTime() == null
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCheckInInfo().getSelfCheckIn())){
				LOG.error("Input Crew Check in info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(!"C".equals(_input.getCheckInInfo().getCrewType())
					&& !"F".equals(_input.getCheckInInfo().getCrewType())){
				LOG.error("Crew type must be F(Flight Crew) or C(Cabin Crew). Message dropped.");
				addExptInfo(EnumExceptionCodes.EENUM.name(), _input.getCheckInInfo().getCrewType());
				return false;
			}
			if(!"Y".equals(_input.getCheckInInfo().getSelfCheckIn())
					&& !"N".equals(_input.getCheckInInfo().getSelfCheckIn())){
				LOG.error("Self check in info must be Y(Yes) or N(No). Message dropped.");
				addExptInfo(EnumExceptionCodes.EENUM.name(), _input.getCheckInInfo().getCrewType());
				return false;
			}
			
			String fltSuffix = null;
			String flNum = null;
			Date flDate = null;
			BigDecimal urno = null;
			EntDbAfttab entFlight = null;
			String idFltJobAssign = "0";
			Date checkInDate = null;
			Date msgInTime = null;
			EntDbFltJobAssign entFltJobAssign = null;
			String cmd = UfisASCommands.IRT.name(); 
			
			fltSuffix = _input.getFlightID().getFltSuffix() == null? "": _input.getFlightID().getFltSuffix();
			
			//format ceda flight string
			/*flNum = _input.getFlightID().getCxCd() +" "+
						HpUfisUtils.formatCedaFltn(String.valueOf(_input.getFlightID().getFltNo())) + fltSuffix;*/
			flNum = HpUfisUtils.formatCedaFlno(_input.getFlightID().getCxCd(), String.valueOf(_input.getFlightID().getFltNo()), fltSuffix);
			
			flDate = inputFlDate.parse(_input.getFlightID().getFltDate());
			//perform only for deptStn = DXB
			if (HpEKConstants.EK_HOPO.equals(_input.getFlightID().getDepStn()))
				entFlight = clsAfttabBeanLocal.findFlightByFlda(flNum, dfDate.format(flDate), _input.getFlightID().getArrStn());
			else{
				LOG.error("Processing performs only for departure station = DXB. Message dropped.");
				addExptInfo(EnumExceptionCodes.ENDEP.name(), _input.getFlightID().getDepStn());
				return false;
			}
			if(entFlight == null){
				LOG.error("Flight flno <{}> is not found. Message dropped.", flNum);
				addExptInfo(EnumExceptionCodes.ENOFL.name(), flNum);
				return false;
			}
			else{
				urno = entFlight.getUrno();
				//get the flt_job_assign by staff_number and id_flight
				entFltJobAssign = clsIDlFltJobAssignLocal.getAssignedCrewIdByFlight(urno, _input.getCheckInInfo().getStaffno(), _input.getCheckInInfo().getCrewType());
				idFltJobAssign = (entFltJobAssign == null)? "0" : entFltJobAssign.getId();
			}
			checkInDate = sf.parse(convertFlDateToUTC(_input.getCheckInInfo().getCheckInTime()));
			msgInTime = sf.parse(convertFlDateToUTC(_input.getMeta().getMessageTime()));
			
			EntDbCrewCheckin entity = null, oldEnt = null; 
			entity = clsDlCrewCheckInBean.getExistingCrewCheckIn(flNum, legNum, 
						depNum, _input.getCheckInInfo().getStaffno(), flDate);
			if(entity == null){
				entity = new EntDbCrewCheckin();
				entity.setFlightNumber(flNum);
				entity.setLegNum(legNum);
				entity.setDepNum(depNum);
				entity.setStaffNumber(_input.getCheckInInfo().getStaffno());
				entity.setFltDate(flDate);
				
				entity.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
				entity.setCreatedUser(HpEKConstants.BMS_SOURCE);
			}
			else{
				oldEnt = new EntDbCrewCheckin(entity);
				entity.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
				entity.setUpdatedUser(HpEKConstants.BMS_SOURCE);
				cmd = UfisASCommands.URT.name(); 
			}
			entity.setIdFlight(urno);
			entity.setIdFltJobAssign(idFltJobAssign);
			entity.setMsgSendDate(msgInTime);
			entity.setFltOrigin3(_input.getFlightID().getDepStn());
			entity.setFltDest3(_input.getFlightID().getArrStn());
			entity.setStaffName(_input.getCheckInInfo().getStaffName());
			entity.setStaffOpGrade(_input.getCheckInInfo().getOperatingGrade());
			entity.setCheckinDate(checkInDate);
			entity.setStaffType(_input.getCheckInInfo().getCrewType());
			entity.setSelfCheckinFlag(_input.getCheckInInfo().getSelfCheckIn());
			entity.setRecStatus(" ");
			entity.setDataSource(HpEKConstants.BMS_SOURCE);
			
			EntDbCrewCheckin entCrewCheckIn = clsDlCrewCheckInBean.merge(entity);
			if(entCrewCheckIn != null){
				sendNotifyUldInfoToInJson(entCrewCheckIn, oldEnt, cmd);
			}
		}catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		}
		catch(ParseException e){
			LOG.error("Date time parsing Error : {}", e.getMessage());
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
		return true;
	}
	
	private void sendNotifyUldInfoToInJson(EntDbCrewCheckin entCrew, EntDbCrewCheckin oldEntCrew, String cmd)
			throws IOException, JsonGenerationException, JsonMappingException {
		String idFlight = entCrew.getIdFlight().toString();
		/*String urno = entCrew.getIdFlight().toString();
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setOrig(dtfl);
		List<String> idFlightList = new ArrayList<>();
		idFlightList.add(urno);
		header.setIdFlight(idFlightList);

		// get the BODY ACTION info
		List<String> fldList = new ArrayList<>();
		fldList.add("ID_FLIGHT");
		fldList.add("ID_FLT_JOB_ASSIGN");
		fldList.add("STAFF_NUMBER");
		fldList.add("STAFF_TYPE");
		fldList.add("CHECKIN_DATE");
		fldList.add("SELF_CHECKIN_FLAG");

		List<Object> dataList = new ArrayList<>();
		dataList.add(urno);
		dataList.add(entCrew.getIdFltJobAssign());
		dataList.add(entCrew.getStaffNumber());
		dataList.add(entCrew.getStaffType());
		dataList.add(sf.format(entCrew.getCheckinDate()));
		dataList.add(entCrew.getSelfCheckinFlag());
		
		List<Object> oldList = new ArrayList<>();
		if(UfisASCommands.URT.toString().equals(cmd)){
			oldList.add(urno);
			oldList.add(oldEntCrew.getIdFltJobAssign());
			oldList.add(oldEntCrew.getStaffNumber());
			oldList.add(oldEntCrew.getStaffType());
			oldList.add(sf.format(oldEntCrew.getCheckinDate()));
			oldList.add(oldEntCrew.getSelfCheckinFlag());
		}
			
		List<String> idList = new ArrayList<>();
		idList.add(entCrew.getId());

		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		action.setCmd(cmd);
		action.setTab("CREW_CHECKIN");
		action.setFld(fldList);
		action.setData(dataList);
		action.setOdat(oldList);
		action.setId(idList);
		action.setSel("WHERE ID = \""+entCrew.getId()+"\"");
		listAction.add(action);

		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		clsBlUfisBCTopic.sendMessage(msg);*/
		
		if (HpUfisUtils
				.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), idFlight, oldEntCrew,
					entCrew);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), idFlight, oldEntCrew,
					entCrew);
		}
		
		//LOG.debug("Sent Notify from BMS.");
	}
	
	
	/**
	 * - ID_FLT_JOB_ASSIGN backward update from ACT-ODS 
	 * @param ufisMsgDTO
	 */
	public void updateFltJobAssign(EntUfisMsgDTO ufisMsgDTO) {
		try{
			switch(ufisMsgDTO.getBody().getActs().get(0).getTab().trim()){
				case "FLT_JOB_ASSIGN" : updateIdFltJobAssign(ufisMsgDTO); break;
			}
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
	
	private void updateIdFltJobAssign(EntUfisMsgDTO ufisMsgDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOG.debug("Flight job assign backward update msg is received..");
		
		List<String> fld = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		String idFlight = ufisMsgDTO.getHead().getIdFlight().get(0);
		String idFltJobAssign = ufisMsgDTO.getBody().getActs().get(0).getId().get(0);
		
		String staffNumber = null, staffCode = null;
		for (int i = 0; i < fld.size(); i++) {
			switch(fld.get(i).toUpperCase()){
				case "STAFF_NUMBER" :  staffNumber = ((String) data.get(i)).trim(); break;
				case "STAFF_TYPE" :  staffCode = ((String) data.get(i)).trim(); break;
			}
		}
		
		if(HpUfisUtils.isNullOrEmptyStr(idFlight) || HpUfisUtils.isNullOrEmptyStr(staffNumber)
				|| HpUfisUtils.isNullOrEmptyStr(staffCode) || HpUfisUtils.isNullOrEmptyStr(idFltJobAssign)){
			LOG.debug("(ID_FLIGHT, STAFF_NUMBER, STAFF_CODE, ID) is empty/null. No processing performed.");
			return;
		}
		BigDecimal idFlt = new BigDecimal(idFlight.trim());
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd();
		switch(cmd){
		case "IRT" :
		case "URT" :
			//find the existing from crew immigration table
			EntDbCrewCheckin entCrew = clsDlCrewCheckInBean.getExistingByIdFlight(idFlt, staffNumber, staffCode, idFltJobAssign);
			if(entCrew == null) //do nothing
				return;
			EntDbCrewCheckin oldEntCrew = new EntDbCrewCheckin(entCrew); //deep copy
			entCrew.setIdFltJobAssign(idFltJobAssign);
			
			cmd = UfisASCommands.URT.name();
			entCrew.setUpdatedUser(HpEKConstants.BMS_SOURCE);
			entCrew.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			entCrew.setRecStatus(" ");
			
			EntDbCrewCheckin resultEntCrew = clsDlCrewCheckInBean.merge(entCrew);
			if(resultEntCrew != null){ //send broadcast message.
				sendNotifyUldInfoToInJson(resultEntCrew, oldEntCrew, cmd);
			}
			break;
		case "DRT" :
			
			//find the existing from crew checkin table
			EntDbCrewCheckin entDeletedCrew = clsDlCrewCheckInBean.getExistingByIdJobAssign(idFlt, idFltJobAssign);
			if(entDeletedCrew == null) //do nothing
				return;
			EntDbCrewCheckin oldEntDeletedCrew = new EntDbCrewCheckin(entDeletedCrew); //deep copy
			entDeletedCrew.setIdFltJobAssign(idFltJobAssign);
			
			cmd = UfisASCommands.DRT.name();
			entDeletedCrew.setUpdatedUser(HpEKConstants.BMS_SOURCE);
			entDeletedCrew.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			entDeletedCrew.setRecStatus("X");
			
			EntDbCrewCheckin resultEntDeletedCrew = clsDlCrewCheckInBean.merge(entDeletedCrew);
			if(resultEntDeletedCrew != null){ //send broadcast message.
				sendNotifyUldInfoToInJson(resultEntDeletedCrew, oldEntDeletedCrew, cmd);
			}
			break;
		}
	}
	
	private String convertFlDateToUTC(XMLGregorianCalendar flightDate)
			throws ParseException {
		
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = flightDate.toGregorianCalendar().getTime();
		return df.format(utcDate);
	}
}
