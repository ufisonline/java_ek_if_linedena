package com.ufis_as.ufisapp.ek.intf.bms;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.bms.entities.EntDbCrewCheckin;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlCrewCheckInBean;
import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgBody;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

@Stateless
public class BlHandleCrewCheckinUpdateBean {

	private static Logger LOG = LoggerFactory
			.getLogger(BlHandleCrewCheckinUpdateBean.class);

	@EJB
	private IDlFlighttJobAssignLocal clsIDlFltJobAssignLocal;
	@EJB
	private DlCrewCheckInBean clsDlCrewCheckInBean;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	public BlHandleCrewCheckinUpdateBean()
	{}
	
	public boolean processCrewCheckInUpdate(EntUfisMsgDTO ufisMsgDTO,
			String dtfl) {
		try {
			long start = System.currentTimeMillis();
			if (ufisMsgDTO != null && ufisMsgDTO.getBody() != null) {
				// EntUfisMsgHead head = ufisMsgDTO.getHead();
				EntUfisMsgBody body = ufisMsgDTO.getBody();
				// currently only action and table are required and only once
				if (body.getActs() != null && body.getActs().size() > 0) {
					EntUfisMsgACT act = body.getActs().get(0);
					// String cmd = act.getCmd();
					String tab = act.getTab();
					List<String> fld = act.getFld();
					List<Object> dat = act.getData();
					if (fld == null || dat == null || fld.size() != dat.size()) {
						LOG.warn("FLD and DAT are required and size should be matched for Alert Processing");
					} else {
						if (tab != null) {
							switch (tab) {
							case "FLT_JOB_ASSIGN":
								validateData(ufisMsgDTO);
								break;
							default:
								LOG.warn("Unsupported table={} encountered",
										tab);
								break;
							}
						}
					}
				}
			} else {
				LOG.warn("Message and message body cannot be empty or null");
			}
			LOG.debug("Message Processing Used: {} ms",
					(System.currentTimeMillis() - start));
		} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}

		return true;
	}

	private void validateData(EntUfisMsgDTO ufisMsgDTO) {
		// idFlight is the main flight
		String idFlight = null;
		String staffNumber = null;
		String id_Flt_job_assign = null;
		String staffType = null;
		List<String> idFlights = ufisMsgDTO.getHead().getIdFlight();
		if (idFlights != null && idFlights.size() > 0) {
			// main flight
			idFlight = idFlights.get(0);
			EntUfisMsgACT act = ufisMsgDTO.getBody().getActs().get(0);
			List<String> fld = act.getFld();
			List<Object> dat = act.getData();
			if (act.getId() != null && act.getId().size() > 0) {
				id_Flt_job_assign = act.getId().get(0);
			}
			for (int i = 0; i < fld.size(); i++) {
				String field = fld.get(i).toUpperCase();
				Object value = dat.get(i);
				if (value != null) {
					switch (field) {
					case "ID_FLIGHT":
						if (!value.toString().equals(idFlight)) {
							LOG.error("different flight Ids in data and Flight Ids section.");
						}
						break;
					case "STAFF_NUMBER":
						staffNumber = value.toString();
						break;
					case "STAFF_TYPE":
						staffType = value.toString();
						break;
					default:
						break;
					}
				}
			}
			if (HpUfisUtils.isNullOrEmptyStr(id_Flt_job_assign)
					|| HpUfisUtils.isNullOrEmptyStr(staffNumber)
					|| HpUfisUtils.isNullOrEmptyStr(idFlight)) {
				LOG.error("Either of Flight ID , Staff Number or Id are not specified in the message. Dropping the message");
				return;
			}
			if (HpUfisUtils.isNullOrEmptyStr(staffType)) {
				LOG.error("Staff Type is not specified in the message. Dropping the message");
				return;
			}
			if (staffType.equalsIgnoreCase("C")
					|| staffType.equalsIgnoreCase("F")) {
				BigDecimal flightId=new BigDecimal(idFlight);
				EntDbCrewCheckin entity = null, oldEnt = null, newEnt = null;
				entity = null;//clsDlCrewCheckInBean.getCrewCheckInByFltIdStaffNum(flightId, staffNumber);
				if (entity == null) {
					LOG.info("No record found in CREW_CHEKIN with idFlight: "
							+ flightId + "  and Staff Number :" + staffNumber);
					// Need to insert or not????
				} else {
					if (!entity.getIdFltJobAssign().equalsIgnoreCase(
							id_Flt_job_assign)) {
						// UfisASCommands.URT
						oldEnt = entity;
						entity.setIdFltJobAssign(id_Flt_job_assign);
						entity.setUpdatedDate(HpUfisCalendar
								.getCurrentUTCTime());
						entity.setUpdatedUser(HpEKConstants.BMS_SOURCE);
						newEnt = clsDlCrewCheckInBean.merge(entity);
						if (HpUfisUtils
								.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
							// send notification message to topic
							ufisTopicProducer.sendNotification(true,
									UfisASCommands.URT, idFlight, oldEnt,
									newEnt);
						} else {
							// if topic not defined, send notification to queue
							ufisQueueProducer.sendNotification(true,
									UfisASCommands.URT, idFlight, oldEnt,
									newEnt);
						}
						LOG.info("Notification message sent from BMS.");
					} else {
						LOG.info("record exist with same Id_Flt_Job_Assign.No updates required.");
					}

				}

			}
		}
	}

}
