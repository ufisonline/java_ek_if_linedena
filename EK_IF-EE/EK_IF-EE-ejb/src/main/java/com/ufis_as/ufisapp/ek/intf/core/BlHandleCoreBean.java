package com.ufis_as.ufisapp.ek.intf.core;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.core.ICoreTmpBean;
import com.ufis_as.ek_if.core.entities.EntDbCoreTmp;
import com.ufis_as.ek_if.core.entities.EntDbFlightDaily;
import com.ufis_as.ek_if.core.entities.EntFlightDailyDTO;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJFLTDAILY;
import com.ufis_as.exco.INFOBJFLTDAILYLIST;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJDCFTAB;
import com.ufis_as.exco.INFOJDCFTABLIST;
import com.ufis_as.exco.INFOJFEVTAB;
import com.ufis_as.exco.INFOJFEVTABLIST;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltDailyBean;
import com.ufis_as.ufisapp.ek.eao.DlFltFuelStatusBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlDcftabBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlFevBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlXaftabBean;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbDcftab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbFevtab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbXaftab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.core.flightevent.FlightEvent;
import ek.core.flightevent.FlightEvent.Events.SchdDateTime;

/**
 * 
 * @author JGO
 * @version 1.00: 2013-04-02 GFO - version
 * @version 1.01: 2013-05-14 JGO - Handle regn
 * @version 1.02: 2013-05-19 JGO - Fevtab and NamedQuery change
 * @version 1.03: 2013-05-28 JGO - Remove actionType for update 
 * 								   Adding RTG
 * 								   Fix est/act onblock time issue 
 * 								   Fix stoa/stod issue
 * @version 1.04: 2013-06-03 JGO - Change Core handler to EJB 
 * @version 1.05: 2013-06-13 JGO - Read HOPO from constant
 * @version 1.06: 2013-06-14 JGO - ISTA for flight status(origin)
 * @version 1.07: 2013-06-21 JGO - Change Singleton to Stateless bean
 * 								   Fuel related info handle
 * 								   Remove internal cache for flight status
 * 								   Via message only pass via related info to ceda
 * 								   Only Hopo message will pass full info
 * 								   Schedule time convert into UTC with timezone it contained
 * 								   MvtDatetime convet into UTC with timezone it contained
 * @version 1.08: 2013-06-22 JGO - Change to use temp table to maintain flight chains
 * @version 1.09: 2013-06-25 JGO - Remove FLT_FUEL_STATUS related
 * @version 1.10: 2013-06-26 JGO - Adding check for incoming message action type
 * 						           If via delete message, change internal action type to update
 * 								   when message flight info update, using internal action to check
 * 							       Adding org3, des3 into infobjflight
 * @version 1.11: 2013-07-02 JGO - Move via update logical outside
 *                                 When delete or cancel message coming and cannot find from cache, drop this message
 *                                 Check action type for updating infobjflight, only when it's non-delete message
 * @version 1.12: 2013-07-10 JGO - Adding configurable function
 * 								   Flda store in utc 
 * 								   Flut store in local
 * @version 1.13: 2013-07-14 JGO - Null check for fltDateLocal(if not found getting from schedule time)
 * @version 1.14: 2013-07-15 JGO - Remove un-used import
 * 					               Null check for fltDateLocal before process it
 * @version 1.15: 2013-07-16 JGO - Send delete message when undo for return
 * @version 1.16: 2013-07-24 JGO - Cover for all undo cases main changes in following:
 * 								   a).delete return new dep and leg message from undo, if contains hopo,
 * 								   will decrement legNum and depNum treat as update
 * 								   b).return flight message with station changed in event(undo) will treat
 * 								   as return record delete and send delete message to backend.
 * @version 1.17: 2013-08-19 JGO - Adding suffix info to flno as well
 * 								   Flno format: carrier(3) + fltn(5) + flns(1)
 * @version 1.18: 2013-08-27 JGO - Fix the FLDA issue (previously substring from stoa/stod directly, now set Local)
 * @version 1.19: 2013-09-06 JGO - adding constant for pattern, format and const
 * @version 1.20: 2013-09-13 JGO - support scenario of the circular flight
 * @version 1.21: 2013-10-07 JGO - Fix the FLDA issue(
 * 								       problem:
 * 									   arrival flight and fltDateLocal not found, get from stod
 * 									   departure flight and fltDateLocal not found, get from stoa)
 * @version 1.22: 2013-10-08 JGO - RTG format change( UTC -> Local)
 * @version 1.23: 2013-10-11 JGO - Adding Error/Warning info data store
 * @version 1.24: 2013-10-23 JGO - Remove message content logging when error encounter(log raw message aldy at received time)
 * @version 1.25: 2013-11-15 JGO - More readable dataStore message content
 *                                 Remove the logical of rejecting cancel message when no data found in db 
 * @version 1.26: 2013-11-22 JGO - Store Remark tag data in AFTTAB.RRMK field
 * @version 1.27: 2013-11-26 JGO - Remove static modifier from BaseDTO
 * @version 1.28: 2013-11-27 JGO - FLUT and FLDA only populate in <INFOBJ_FLIGHT> when station contain HOPO
 *                                 FLDA remove from <INFOBJ_GENERIC>
 * @version 1.29: 2013-12-02 JGO - Exclude infojxaftab when no value set
 * @version 1.30: 2013-12-23 JGO - Store full via info in FLT_DAILY table
 * 								   Send flt_daily id list to CEDA
 *                                 Send IDFD to CEDS when DXB message
 * @version 1.31: 2013-12-30 JGO - Get stoa from origin and get stod from destination hop
 * 
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlHandleCoreBean extends BaseDTO {
//public class BlHandleCoreBean extends BaseDTO implements ICoreHandler {
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BlHandleCoreBean.class);
	
	/**
	 * CDI
	 */
	@EJB
	private ICoreTmpBean coreTmpBean;
	@EJB
	private DlFltDailyBean fltDailyBean;
	@EJB
	private DlFevBean fevtabBean;
	@EJB
	private DlDcftabBean dcftabBean;
	@EJB
	private DlXaftabBean xaftabBean;
	@EJB
    private DlFltFuelStatusBean fltFuelStatusBean;
//	@EJB
//	private BlUfisQueue bridgeMessage;
    @EJB
    private BlUfisExceptionQueue ufisExptLogging;
	
	/**
	 * Constants and Variables
	 */
	private static final String MSGIF = "EK-CORE";
	
	private static final String CONST_DELETE = "DELETE";
	private static final String CONST_CANCEL = "CNLD";
	
	private static final String CONST_FTYP_X = "X";
	private static final String CONST_FTYP_O = "O";

	/** ' ': default value, valid */
	private static final Character CONST_STAT_DEF = ' ';
	private static final Character CONST_STAT_DEL = 'X';
	
	private static final String PATTERN_REGN = "\\d{1,4}";
	
	private static final String FMT_DURA = "%04d";
	
	private static DateFormat df = new SimpleDateFormat("yyyyMMddHHmm00");
	private static DateFormat df1 = new SimpleDateFormat("yyyyMMddHHmmss");

	/**
	 * TOW: take off weight
	 * ZFW: zero fuel weight
	 * TOF: take off fuel
	 * FOA: fuel on arrival
	 */
	//private static final String FUEL_TYPE_TOW = "TOW";
	//private static final String FUEL_TYPE_ZFW = "ZFW";
	//private static final String FUEL_TYPE_TOF = "TOF";
	//private static final String FUEL_TYPE_FOA = "FOA";
    protected String _flightXml = "";
    protected String _returnXML = "";
    protected String _messageType = "";
    protected String _actionType = "";
    protected ACTIONTYPE _msgActionType;
    private FlightEvent flightData;
    
//    private JAXBContext _cnxJaxb;
//    private JAXBContext _cnxJaxbM;
//    private Unmarshaller _um;
//    private Marshaller _ms;

	public BlHandleCoreBean() {
		try {
            _cnxJaxb = JAXBContext.newInstance(FlightEvent.class);
            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
            _um = _cnxJaxb.createUnmarshaller();
            _ms = _cnxJaxbM.createMarshaller();
            
            df.setTimeZone(HpEKConstants.utcTz);
            
            tableRef = HpUfisAppConstants.CON_IRMTAB;
        	exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
        	dtfl = com.ufis_as.ufisapp.configuration.HpEKConstants.EK_CORE_SOURCE;
        } catch (JAXBException ex) {
            LOG.error("JAXBException when initialize JaxbContext, marshaller and unmarshaller");
        }
	}
	
	public FlightEvent unMarshal(String message, long irmtabRef) {
		flightData = null;
    	data.clear();
    	
    	// set urno of irmtab
    	irmtabUrno = irmtabRef;
		try {
			_flightXml = message;
			flightData = (FlightEvent) _um.unmarshal(new StreamSource(new StringReader(_flightXml)));
		} catch (JAXBException ex) {
			LOG.error("Flight Event JAXB exception {}", ex.toString());
			//LOG.error("Message: \n{}", message);
		}
		return flightData;
	}
	
    public boolean tranformFlight(FlightEvent flightEvent) {

		HpUfisCalendar ufisCalendar = new HpUfisCalendar();
        FlightEvent.FlightId flightId = flightEvent.getFlightId();
        
        INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
        INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
        INFOJXAFTAB infojxaftab = new INFOJXAFTAB();
        INFOJDCFTABLIST infojdcftablist = new INFOJDCFTABLIST();
        INFOJFEVTABLIST infojfevtablist = new INFOJFEVTABLIST();
        INFOBJFLTDAILYLIST infobjfltdailylist = new INFOBJFLTDAILYLIST();
        
        boolean isXafFound = false;
        
        // Check the message type and action type
 		_messageType = flightEvent.getMessageType();
 		_actionType = flightEvent.getActionType();
 		if (_actionType == null) {
 			LOG.debug("ActionType cannot be null or empty.");
        	LOG.debug("Message details: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(),
					"Action type cannot be null or empty");
        	return false;
 		}
 		
 		if ("INSERT".equalsIgnoreCase(_actionType)) {
 			_msgActionType = ACTIONTYPE.I;
 		} else if ("UPDATE".equalsIgnoreCase(_actionType)) {
 			// _msgActionType = ACTIONTYPE.U;
 			_msgActionType = null;
 		} else {
 			_msgActionType = ACTIONTYPE.D;
 		}
        
        // Check Flight id
        if (flightId == null) {
        	LOG.debug("FlightId Cannot be null, message dropped.");
        	LOG.debug("Message details: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(),
					"FlightId Cannot be null");
        	return false;
        }
        
        // Mandatory fields in FlightId check
        if (HpUfisUtils.isNullOrEmptyStr(flightId.getCxCd()) 
        		|| HpUfisUtils.isNullOrEmptyStr(flightId.getFltNum())
        		|| HpUfisUtils.isNullOrEmptyStr(flightId.getArrStn())
        		|| HpUfisUtils.isNullOrEmptyStr(flightId.getDepStn())
        		|| flightId.getDepNum() == null
        		|| flightId.getLegNum() == null
        		|| flightId.getFltDate() == null 
        		|| flightId.getFltDate().getValue() == null) {
        	LOG.debug("Mandatory fields in FlightId Cannot be null or empty. ");
        	LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(),
					"Mandatory fields not found in FlightId");
        	return false;
        }
        
        // message events check
        FlightEvent.Events event = null;
        if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
        	if (!flightEvent.getEvents().isEmpty() 
            		&& flightEvent.getEvents().size() == 1) {
        		event = flightEvent.getEvents().get(0);
        	} else {
        		LOG.debug("Events Cannot be empty or more than 1. ");
            	LOG.debug("Message dropped: \n{}", _flightXml);
            	addExptInfo(EnumExceptionCodes.EWVAL.name(),
    					"Non-Delete CORE messge: Flight Event is required and one only");
            	return false;
        	}
        	
        	// Mandatory fields in events
            if (HpUfisUtils.isNullOrEmptyStr(event.getCxCd()) 
            		|| HpUfisUtils.isNullOrEmptyStr(event.getFltNum())
            		|| HpUfisUtils.isNullOrEmptyStr(event.getArrStn())
            		|| HpUfisUtils.isNullOrEmptyStr(event.getDepStn())
            		|| event.getDepNum() == null
            		|| event.getLegNum() == null
            		|| event.getFltDate() == null 
            		|| event.getFltDate().getValue() == null) {
            	LOG.debug("Mandatory fields in Events Cannot be null or empty. ");
            	LOG.debug("Message dropped: \n{}", _flightXml);
            	addExptInfo(EnumExceptionCodes.EMAND.name(),
    					"Mandatory field in FlightId cannot be null or empty in flight event as well");
            	return false;
            }
        }
        
        // temp entity and temp return flight entity
        EntDbCoreTmp tempMsg = null;
        EntDbCoreTmp returnMsg = null;
        
        // fltDate in FlightId default as UTC
        String fldaUtc = "";
    	ufisCalendar.setTime(flightId.getFltDate().getValue());
    	fldaUtc = ufisCalendar.getCedaDateString();
    	
    	// hop schedule time
    	String stoa = "";
    	String stod = "";
    	String fldaFromStoa = "";
    	String fldaFromStod = "";
    	if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
	    	List<SchdDateTime> schedules = event.getSchdDateTime();
	        for (SchdDateTime schedule : schedules) {
	        	ufisCalendar.setTime(schedule.getValue());
	        	if ("Arrival".equalsIgnoreCase(schedule.getType())) {
	        		fldaFromStoa = ufisCalendar.getCedaDateString();
	        		stoa = df.format(ufisCalendar.getTime());
	        	} else if ("Departure".equalsIgnoreCase(schedule.getType())) {
	        		fldaFromStod = ufisCalendar.getCedaDateString();
	        		stod = df.format(ufisCalendar.getTime());
	        	}
	        }
    	}
    	
    	/**
         * Maintain the flight message chains in the temp table
         * CxCd
         * FltNum
         * FltSuffix
         * FltDate
         * DepStn
         * ArrStn
         */
    	List<EntDbCoreTmp> tempMessages = coreTmpBean.getMsgById(flightId);
    	// ======================================================================
    	// if message not found in temp tale, process by actionType/FltStatus
    	// ======================================================================
        if (tempMessages == null || tempMessages.size() == 0) {
        	
        	// According to actionType/FltStatus, process msg
            // if (CONST_DELETE.equalsIgnoreCase(_actionType) || CONST_CANCEL.equalsIgnoreCase(event.getFltStatus())) {
        	if (CONST_DELETE.equalsIgnoreCase(_actionType)) {
        		// Drop this delete message due to the target is not existed
        		LOG.debug("Delete(actionType)/Cancel(FltStatus) message received, but target is not existed in temp table");
            	LOG.debug("Message details: \n{}", _flightXml);
            	addExptInfo(EnumExceptionCodes.ENREC.name(),
    					"No record found in DB to delete");
            	return false;
            } else {
            	// insert message base on flightId
            	tempMsg = new EntDbCoreTmp();
            	tempMsg.setCxcd(flightId.getCxCd());
            	tempMsg.setFltNum(flightId.getFltNum());
            	tempMsg.setFltSuffix(flightId.getFltSuffix());
        		tempMsg.setFltDate(fldaUtc);
            	tempMsg.setDepStn(flightId.getDepStn());
            	tempMsg.setArrStn(flightId.getArrStn());
            	tempMsg.setLegNum(flightId.getLegNum().intValue());
            	tempMsg.setDepNum(flightId.getDepNum().intValue());
            	tempMsg.setSchArr(stoa);
            	tempMsg.setSchDep(stod);
            	tempMsg.setRecStatus(CONST_STAT_DEF);
            	coreTmpBean.create(tempMsg);
            	
            	// check contian return info or not
            	// if id.arrStn != id.depStn and event.arrStn == event.depStn == HOPO
            	if (HpEKConstants.HOPO.equals(event.getArrStn()) 
            			&& HpEKConstants.HOPO.equals(event.getDepStn()) 
            			&& !flightId.getArrStn().equals(flightId.getDepStn())) {
            		returnMsg = new EntDbCoreTmp();
            		returnMsg.setCxcd(flightId.getCxCd());
            		returnMsg.setFltNum(flightId.getFltNum());
            		returnMsg.setFltSuffix(flightId.getFltSuffix());
                	returnMsg.setFltDate(fldaUtc);
                	returnMsg.setDepStn(event.getDepStn());
                	returnMsg.setArrStn(event.getArrStn());
                	returnMsg.setLegNum(flightId.getLegNum().intValue());
                	returnMsg.setDepNum(flightId.getDepNum().intValue());
                	returnMsg.setSchArr(stoa);
                	returnMsg.setSchDep(stod);
                	returnMsg.setRecStatus(CONST_STAT_DEF);
                	coreTmpBean.create(returnMsg);
            	}
            }
        } else {
        	// ===================================================================================
        	// if message found in temp tale, and according to actionType/FltStatus, process msg
        	// ===================================================================================
        	
        	// delete or cancel
            if (CONST_DELETE.equalsIgnoreCase(_actionType) 
            		|| (event != null && CONST_CANCEL.equalsIgnoreCase(event.getFltStatus()))) {
            	// do update(for undo delete new generated leg message)
            	if (CONST_DELETE.equalsIgnoreCase(_actionType) 
            			&& (flightId.getDepNum().intValue() != 1)
            			&& (HpEKConstants.HOPO.equals(flightId.getArrStn()) 
                    			|| HpEKConstants.HOPO.equals(flightId.getDepStn()))) {
            		// update temp table info
            		int dNum = flightId.getDepNum().intValue();
            		int lNum = flightId.getLegNum().intValue();
            		for (EntDbCoreTmp item : tempMessages) {
            			if (item.getDepNum() == dNum && item.getLegNum() == lNum) {
            				//dNum = flightId.getDepNum().subtract(new BigInteger("1")).intValue();
            				//lNum = flightId.getLegNum().subtract(new BigInteger("1")).intValue();
            				dNum -= 1;
            				lNum -= 1;
            				item.setDepNum(dNum);
            				item.setLegNum(lNum);
            				coreTmpBean.edit(item);
            			}
            		}
            		
            		// prepare update message for depNum decrement(undo for return)
            		_msgActionType = ACTIONTYPE.U;
            		infobjFlight.setDEPN(String.valueOf(dNum));
            	} else if (!flightId.getArrStn().equals(flightId.getDepStn())
            			&& (HpEKConstants.HOPO.equals(flightId.getArrStn()) 
            			|| HpEKConstants.HOPO.equals(flightId.getDepStn()))) {
            		// mark the whole chains as deleted
            		// rec_status: X-Delete; I-Invalid; blank-Valid
            		coreTmpBean.updMsgStatus(flightId, true, CONST_STAT_DEL);
            	} else {
            		// mark specific message as deleted
            		coreTmpBean.updMsgStatus(flightId, false, CONST_STAT_DEL);
            	}
            } else {
            	// if return message, update/insert
            	if (HpEKConstants.HOPO.equals(event.getArrStn()) 
            			&& HpEKConstants.HOPO.equals(event.getDepStn())) {
            		// if flightId is return flight
            		if (HpEKConstants.HOPO.equals(flightId.getArrStn()) 
                			&& HpEKConstants.HOPO.equals(flightId.getDepStn())) {
            			// loop to check return flight existing or not
                		boolean isExisting = false;
                		for (EntDbCoreTmp item : tempMessages) {
                			if (item.getDepNum() == flightId.getDepNum().intValue()) {
                				//coreTmpBean.edit(item);
                				isExisting = true;
                				break;
                			}
                		}
                		
                		// if no record to update, create new return record
                		if (!isExisting) {
                			returnMsg = new EntDbCoreTmp();
                    		returnMsg.setCxcd(flightId.getCxCd());
                    		returnMsg.setFltNum(flightId.getFltNum());
                    		returnMsg.setFltSuffix(flightId.getFltSuffix());
                        	returnMsg.setFltDate(fldaUtc);
                        	returnMsg.setDepStn(event.getDepStn());
                        	returnMsg.setArrStn(event.getArrStn());
                        	returnMsg.setLegNum(flightId.getLegNum().intValue());
                        	returnMsg.setDepNum(flightId.getDepNum().intValue());
                        	returnMsg.setSchArr(stoa);
                        	returnMsg.setSchDep(stod);
                        	returnMsg.setRecStatus(CONST_STAT_DEF);
                        	coreTmpBean.create(returnMsg);
                		}
            		}
            	} else if (!flightId.getArrStn().equals(event.getArrStn()) 
            			|| !flightId.getDepStn().equals(event.getDepStn())) {
            		// if the station changed, mark old record as X
            		EntDbCoreTmp msgRecord = tempMessages.get(0);
            		msgRecord.setRecStatus(CONST_STAT_DEL);
            		coreTmpBean.edit(msgRecord);
            		
            		// return flight change to normal flight message
            		if (HpEKConstants.HOPO.equals(flightId.getArrStn()) 
                			&& HpEKConstants.HOPO.equals(flightId.getDepStn())) {
            			// need to send a delete message to ceda to delete return record
            			_msgActionType = ACTIONTYPE.D;
            			_actionType = CONST_DELETE;
            		}
            		
            		// create new changed station record into temp table
            		/*tempMsg = new EntDbCoreTmp();
                	tempMsg.setCxcd(flightId.getCxCd());
                	tempMsg.setFltNum(flightId.getFltNum());
                	tempMsg.setFltSuffix(flightId.getFltSuffix());
            		tempMsg.setFltDate(fldaUtc);
                	tempMsg.setDepStn(event.getDepStn());
                	tempMsg.setArrStn(event.getArrStn());
                	tempMsg.setLegNum(flightId.getLegNum().intValue());
                	tempMsg.setDepNum(flightId.getDepNum().intValue());
                	tempMsg.setSchArr(stoa);
                	tempMsg.setSchDep(stod);
                	tempMsg.setRecStatus(' ');
                	coreTmpBean.create(tempMsg);*/
            		
            	} else {
            		// update record (if header changed in body, take body value)
            		EntDbCoreTmp msgRecord = tempMessages.get(0);
            		boolean isChanged = false;
            		// update carrier
            		if (!msgRecord.getCxcd().equals(event.getCxCd())) {
            			msgRecord.setCxcd(event.getCxCd());
            			isChanged = true;
            		}
            		// update flight number            		
            		if (!msgRecord.getFltNum().equals(event.getFltNum())) {
            			msgRecord.setFltNum(event.getFltNum());
            			isChanged = true;
            		}
            		// update flight suffix
            		if (event.getFltSuffix() != null 
            				&& !event.getFltSuffix().equals(msgRecord.getFltSuffix())) {
            			msgRecord.setFltSuffix(event.getFltSuffix());
            			isChanged = true;
            		}
            		// update flight date
            		if (!msgRecord.getFltDate().equals(fldaUtc)) {
            			msgRecord.setFltDate(fldaUtc);
            			isChanged = true;
            		}
            		// update arrStn
            		if (!msgRecord.getArrStn().equals(event.getArrStn())) {
            			msgRecord.setArrStn(event.getArrStn());
            			isChanged = true;
            		}
            		// update depStn
            		if (!msgRecord.getDepStn().equals(event.getDepStn())) {
            			msgRecord.setDepStn(event.getDepStn());
            			isChanged = true;            			
            		}
            		// update legNum
            		if (msgRecord.getLegNum() != event.getLegNum().intValue()) {
            			msgRecord.setLegNum(event.getLegNum());
            			isChanged = true;
            		}
            		// update depNum
            		if (msgRecord.getDepNum() != event.getDepNum().intValue()) {
            			msgRecord.setDepNum(event.getDepNum().intValue());
            			isChanged = true;
            		}
            		// schedule arr/dep
            		msgRecord.setSchArr(stoa);
            		msgRecord.setSchDep(stod);
            		
            		if (isChanged || msgRecord.getRecStatus() == CONST_STAT_DEL) {
            			msgRecord.setRecStatus(CONST_STAT_DEF);
            			coreTmpBean.edit(msgRecord);
            		}
            	}
            }
        }
        
        /**
         * Info Populate
         * =======================================================================
         * According to action type and status
         * Process and format message and send back to Ceda:
         *	Only when actionType is delete and contains HOPO will perform delete
         *	When body.org3 = body.des3, regarding header, handle as return flight
         *  Via message, will only pass via info (all rest exclude)
         *  Only contains HOPO message, will give full info
         * ========================================================================
         */
        // ==========================================================
        // infobjgeneric - main for flight search
        // ==========================================================
        // Airline
        String carrier = "";
        if (flightId.getCxCd().length() == 2 || flightId.getCxCd().length() == 3) {
        	carrier = flightId.getCxCd();
        } 
        // Flight number in Ceda format
        String fltn = HpUfisUtils.formatCedaFltn(flightId.getFltNum());
        if (carrier.length() == 3) {
        	infobjgeneric.setFLNO(carrier + fltn);
        } else {
        	infobjgeneric.setFLNO(carrier + " " + fltn);
        }
        // Suffix
        if (HpUfisUtils.isNotEmptyStr(flightId.getFltSuffix())) {
        	infobjgeneric.setFLNS(flightId.getFltSuffix());
        	// 2013-08-19 updated by JGO - adding suffix info to flno as well
        	// FLNO format: airline code(3) + flight number(5) + suffix(1)
        	if (fltn.length() == 5) {
        		infobjgeneric.setFLNO(infobjgeneric.getFLNO() + flightId.getFltSuffix());
        	} else if (fltn.length() == 4) {
        		infobjgeneric.setFLNO(infobjgeneric.getFLNO() + " " + flightId.getFltSuffix());
        	} else if (fltn.length() == 3) {
        		infobjgeneric.setFLNO(infobjgeneric.getFLNO() + " " + " " + flightId.getFltSuffix());
        	}
        }
        // FLDA(local) -> FLUT(utc) 
        //ufisCalendar.setTime(flightId.getFltDate().getValue());
        //infobjgeneric.setFLDA(ufisCalendar.getCedaDateString());
        infobjgeneric.setFLUT(fldaUtc);
        // ADID
        if (HpEKConstants.HOPO.equalsIgnoreCase(flightId.getDepStn()) 
        		&& HpEKConstants.HOPO.equalsIgnoreCase(flightId.getArrStn())) {
            //Return Flight /TAxi
        	infobjgeneric.setADID(ADID.B);
        	infobjgeneric.setORG3(HpEKConstants.HOPO);
        	infobjgeneric.setDES3(HpEKConstants.HOPO);
        	// depNum
            infobjgeneric.setDEPN(String.valueOf(flightId.getDepNum()));
            
        } else if (HpEKConstants.HOPO.equalsIgnoreCase(flightId.getArrStn())) {
            //Arival
        	infobjgeneric.setADID(ADID.A);
            infobjgeneric.setDES3(HpEKConstants.HOPO);
            // depNum
            infobjgeneric.setDEPN(String.valueOf(flightId.getDepNum()));
        } else if (HpEKConstants.HOPO.equalsIgnoreCase(flightId.getDepStn())) {
            //Departure
        	infobjgeneric.setADID(ADID.D);
        	infobjgeneric.setORG3(HpEKConstants.HOPO);
        	// depNum
            infobjgeneric.setDEPN(String.valueOf(flightId.getDepNum()));
        }
        
        // if it is via delete message, change ceda actionType to null (ceda decides what to do)
        if (infobjgeneric.getADID() == null && CONST_DELETE.equalsIgnoreCase(_actionType) ) {
        	_msgActionType = null;
        }
        
        /**
         * Find the flight chains in temp table(exclude return flight)
         * CxCd
         * FltNum
         * FltSuffix
         * FltDate
         * DepStn != HOPO
         * ArrStn != HOPO
         */
        List<EntDbCoreTmp> msgChains = coreTmpBean.getMsgChainById(flightId);
        /**
         *  If incoming message does not contain HOPO info
         *  Then check HOPO flight existing in db or not to get ADID, ORG3 or DES3
         */
    	// if via message coming first than HOPO, skip
    	//if (infobjgeneric.getADID() == null && msgChains != null && msgChains.size() > 0) {
    	if (msgChains != null && msgChains.size() > 0) {
//    		boolean isHopoFound = false;
    		boolean isHopoFoundInDepStn = false;
    		boolean isHopoFoundInArrStn = false;
    		for (EntDbCoreTmp item : msgChains) {
    			if (HpEKConstants.HOPO.equals(item.getArrStn())) {
    				infobjgeneric.setADID(ADID.A);
    				infobjgeneric.setDES3(HpEKConstants.HOPO);
    				// depNum
    		        infobjgeneric.setDEPN(String.valueOf(item.getDepNum()));
//    				isHopoFound = true;
    		        isHopoFoundInArrStn = true;
    			} else if (HpEKConstants.HOPO.equals(item.getDepStn())) {
    				infobjgeneric.setADID(ADID.D);
    				infobjgeneric.setORG3(HpEKConstants.HOPO);
    				// depNum
    		        infobjgeneric.setDEPN(String.valueOf(item.getDepNum()));
//    				isHopoFound = true;
    		        isHopoFoundInDepStn = true;
    			}
    		}
//    		if (!isHopoFound) {
    		if (!(isHopoFoundInDepStn || isHopoFoundInArrStn)) {
    			LOG.debug("Message Skipped due to Via message coming first than HOPO message.");
            	//LOG.debug("Message details: \n{}", _flightXml);
            	return false;
    		}
    		
    		// 2013-09-13 updated by JGO - handle circular flight
    		// ====================================================
    		// if ulmate compressing of VIAs resulted in DXB - DXB
    		//    if last leg > 1 then
    		//       conclude is circular
    		// ====================================================
			if (HpEKConstants.HOPO.equals(infobjgeneric.getORG3())
					&& HpEKConstants.HOPO.equals(infobjgeneric.getDES3())) {
				// treat as arrival flight
				if (!HpEKConstants.HOPO.equals(flightId.getArrStn())) {
					// remove the last leg of the Chains
					msgChains.remove(msgChains.size() - 1);
					infobjgeneric.setADID(ADID.D);
					infobjgeneric.setDES3(null);
				} else {
					// treat as departure flight
					if (flightId.getLegNum().intValue() > 1) {
						// remove to keep only current message
						msgChains = null;
						infobjgeneric.setADID(ADID.A);
						infobjgeneric.setORG3(null);						
					}
				}
			}
    	}
    	
    	/**
         * 2013-12-23 updated by JGO - Store all station info into flt_daily
         *  
    	 */
    	String headFlno = "";
        String eventFlno = "";
        String eventFlut = "";
    	// CEDA format flno from message header(FlightId)
		headFlno = HpUfisUtils.formatCedaFlno(flightId.getCxCd(),
				HpUfisUtils.formatCedaFltn(flightId.getFltNum()),
				flightId.getFltSuffix());

		if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
			// CEDA format flno from message event
			eventFlno = HpUfisUtils.formatCedaFlno(event.getCxCd(),
					HpUfisUtils.formatCedaFltn(event.getFltNum()),
					event.getFltSuffix());
			// flut 
			ufisCalendar.setTime(event.getFltDate().getValue());
			eventFlut =  ufisCalendar.getCedaDateString();
    	}
    	// build flight daily query critiera (query by flightId)
    	EntFlightDailyDTO dto = new EntFlightDailyDTO();
    	dto.setFlightNumber(headFlno);
    	dto.setFltOrg3(flightId.getDepStn());
    	dto.setFltDes3(flightId.getArrStn());
    	dto.setLegNum(String.valueOf(flightId.getLegNum()));
    	dto.setFlut(fldaUtc);
    	// query from flt_daily table to insert/update record (data store by event)
    	EntDbFlightDaily fltDaily = fltDailyBean.getFltDaily(dto);
    	if (fltDaily == null) {
    		fltDaily = new EntDbFlightDaily();
    		fltDaily.setCreatedUser(dtfl);
    		fltDailyBean.persist(fltDaily);
    	}
    	
    	// fill the via info
    	String nexi = "";
        boolean isViaXafFound = false;
    	if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
    		fltDaily.setFltNumber(eventFlno);
        	fltDaily.setFltOrigin3(event.getDepStn());
        	fltDaily.setFltDest3(event.getArrStn());
        	fltDaily.setDepNum(String.valueOf(event.getDepNum()));
        	fltDaily.setLegNum(String.valueOf(event.getLegNum()));
        	fltDaily.setFlut(eventFlut);
    		fltDaily.setAct3(event.getAcSubType());
    		fltDaily.setFltServiceType(event.getServiceType());
    		fltDaily.setIsta(event.getFltStatus());
    		fltDaily.setRecStatus(" ");
    		
    		// registration
    		if (event.getTailNo() != null && event.getTailNo().getValue() != null) {
            	if (event.getTailNo().getValue().matches(PATTERN_REGN)) {
            		fltDaily.setFltRegn("");
            	} else {
            		fltDaily.setFltRegn(event.getTailNo().getValue());
            	}
            } else {
            	fltDaily.setFltRegn("");
            }
    		// flda
    		if (event.getFltDateLocal() != null) {
            	ufisCalendar.setTime(event.getFltDateLocal().getValue());
            	fltDaily.setFlda(ufisCalendar.getCedaDateString());
            } else {
            	// if ADID is D, refer from STOD, others STOA (yyyyMMdd)
            	if (ADID.D == infobjgeneric.getADID()) {
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStod)) {
            			fltDaily.setFlda(fldaFromStod);
            		}
            	} else {
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStoa)) {
            			fltDaily.setFlda(fldaFromStoa);
            		}
            	}
            }
    		// flight duration (Duration from airborne to landing in minutes)
    		if (event.getFltTime() != null && event.getFltTime().getValue() != null) {
				fltDaily.setFltDuration(new BigDecimal(event.getFltTime()
						.getValue()));
    		}
    		// schedule block time (off block to on block in minutes)
    		if (event.getBlkTimeOrig() != null && event.getBlkTimeOrig().getValue() != null) {
				fltDaily.setScheOffOnblockDuration(new BigDecimal(event
						.getBlkTimeOrig().getValue()));
    		}
    		// actual block time (off block to on block in minutes)
            if (event.getBlkTimeAct() != null && event.getBlkTimeAct().getValue() != null) {
				fltDaily.setActualOffOnblockDuration(new BigDecimal(event
						.getBlkTimeAct().getValue()));
            }
            // TaxiOut/TaxiIn
            if (event.getTaxiOut() != null) {
                fltDaily.setExot(new BigDecimal(event.getTaxiOut().getValue()));
            }
            if (event.getTaxiIn() != null) {
                fltDaily.setExit(new BigDecimal(event.getTaxiIn().getValue()));
            }
            // flight remark
            StringBuilder viaRemark = new StringBuilder();
            for (FlightEvent.Events.Remark r : event.getRemark()) {
            	viaRemark.append(r.getValue());
            	viaRemark.append(" ");
            }
            fltDaily.setFltRemarks(viaRemark.toString());
            // supplement remark
            StringBuilder viaSiRemarkBuild = new StringBuilder();
        	for (FlightEvent.Events.SiRemark remark : event.getSiRemark()) {
        		viaSiRemarkBuild.append(remark.getValue());
        		viaSiRemarkBuild.append(" ");
        	}
        	fltDaily.setFltSupplementRemarks(viaSiRemarkBuild.toString());
            // schedule block timing
	        for (SchdDateTime schedule : event.getSchdDateTime()) {
	        	ufisCalendar.setTime(schedule.getValue());
	        	if ("Arrival".equalsIgnoreCase(schedule.getType())) {
	        		fltDaily.setSibtLoc(ufisCalendar.getTime());
	        		// Convert to UTC datetime
//					ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC,
//							EnumTimeInterval.Hours);
//	        		fltDaily.setSibt(ufisCalendar.getTime());
	        		try {
						fltDaily.setSibt(df1.parse(df.format(ufisCalendar.getTime())));
					} catch (ParseException e) {
						LOG.warn("Cannot parse datetime: {}", e.getMessage());
					}
	        	} else if ("Departure".equalsIgnoreCase(schedule.getType())) {
	        		fltDaily.setSobtLoc(ufisCalendar.getTime());
	        		// Convert to UTC datetime
//					ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC,
//							EnumTimeInterval.Hours);
//	        		fltDaily.setSobt(ufisCalendar.getTime());
	        		try {
						fltDaily.setSobt(df1.parse(df.format(ufisCalendar.getTime())));
					} catch (ParseException e) {
						LOG.warn("Cannot parse datetime: {}", e.getMessage());
					}
	        	}
	        }
	        
    		// mvtDatetime
    		for (FlightEvent.Events.MvtDateTime mvtDateTime : event.getMvtDateTime()) {
    			String mvtType = mvtDateTime.getType();
                if (HpUfisUtils.isNullOrEmptyStr(mvtType)) {
                	continue;
                }
                Date tempDate = null;
                if (mvtDateTime.getValue() != null) {
                	ufisCalendar.setTime(mvtDateTime.getValue());
                	
                	// Convert to UTC datetime
//					ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC,
//							EnumTimeInterval.Hours);
//					tempDate = ufisCalendar.getTime();
                	try {
                		tempDate = df1.parse(df.format(ufisCalendar.getTime()));
					} catch (ParseException e) {
						LOG.warn("Cannot parse datetime: {}", e.getMessage());
					}
                }
                switch (mvtType) {
                	case "AirborneEst":
                		// estimate airborne
                		fltDaily.setEtotLoc(ufisCalendar.getTime());
                		fltDaily.setEtot(tempDate);
                		break;
                    case "AirborneAct":
                    	// actual airborne
                    	fltDaily.setAtotLoc(ufisCalendar.getTime());
                    	fltDaily.setAtot(tempDate);
                        break;
                    case "LandedEst":
                    	// estimate landing
                    	fltDaily.setEldtLoc(ufisCalendar.getTime());
                    	fltDaily.setEldt(tempDate);
                    	break;
                    case "LandedAct":
                    	// actual landing
                    	fltDaily.setAldtLoc(ufisCalendar.getTime());
                    	fltDaily.setAldt(tempDate);
                        break;
                    case "OnBlocksEst":
                    	// estimate in block
                    	fltDaily.setEibtLoc(ufisCalendar.getTime());
                    	fltDaily.setEibt(tempDate);
                        break;
                    case "OnBlocksAct":
                    	// actual in block
                    	fltDaily.setAibtLoc(ufisCalendar.getTime());
                    	fltDaily.setAibt(tempDate);
                        break;
                    case "OffBlocksEst":
                    	// estimate off block
                    	fltDaily.setEobtLoc(ufisCalendar.getTime());
                    	fltDaily.setEobt(tempDate);
                        break;
                    case "OffBlocksAct":
                    	// actual off block
                    	fltDaily.setAobtLoc(ufisCalendar.getTime());
                    	fltDaily.setAobt(tempDate);
                        break;
                    case "StartTaxiAct":
                    	// actual time of starting taxing
                    	fltDaily.setAsttLoc(ufisCalendar.getTime());
                		fltDaily.setAstt(tempDate);
                        break;
                    case "NextInformation":
                    	nexi = df.format(ufisCalendar.getTime());
                        isXafFound = true;
                        break;
                }
            }
    	} else {
    		fltDaily.setFltNumber(headFlno);
        	fltDaily.setFltOrigin3(flightId.getDepStn());
        	fltDaily.setFltDest3(flightId.getArrStn());
        	fltDaily.setDepNum(String.valueOf(flightId.getDepNum()));
        	fltDaily.setLegNum(String.valueOf(flightId.getLegNum()));
        	fltDaily.setFlut(fldaUtc);
    		fltDaily.setRecStatus("X");
    	}
    	fltDaily.setUpdatedUser(dtfl);
    	fltDailyBean.update(fltDaily);
    	
        // Event, Extension and DelayCode related table only store for non-Hopo by interface 
        if (!HpEKConstants.HOPO.equalsIgnoreCase(flightId.getDepStn()) 
        		&& !HpEKConstants.HOPO.equalsIgnoreCase(flightId.getArrStn())) {
        	
        	if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
        		// XAFTAB related info
                // AcOwner
            	String acow = "";
            	String acex = "";
                if (event.getAcOwner() != null) {
                	acow = event.getAcOwner();
                	isViaXafFound = true;
                }
                // AcVersion
                if (event.getAcVersion() != null) {
                	acex = event.getAcVersion();
                	isViaXafFound = true;
                }
    	    	if (isViaXafFound) {
    	    		// find via related xaftab info
    	        	EntDbXaftab xaftab = xaftabBean.findByFltDailyId(fltDaily.getId());
    	        	if (xaftab == null) {
    	        		xaftab = new EntDbXaftab();
    	        		xaftab.setIdfd(fltDaily.getId());
    	        		xaftabBean.persist(xaftab);
    	        	}
    	        	xaftab.setIdfd(fltDaily.getId());
    	        	xaftab.setAcow(acow);
    	        	xaftab.setAcex(acex);
    	        	xaftab.setNexi(nexi);
    	        	xaftabBean.update(xaftab);
    	    	}
    	        
    	        // DCFTAB related info
    	        // Get Delays
    	        if (flightEvent.getDelays() != null) {
    	        	for (FlightEvent.Delays.Delay delay : flightEvent.getDelays().getDelay()) {
    	        		EntDbDcftab dcftab = dcftabBean.findByFltDailyIdAndDelayCode(fltDaily.getId(), delay.getCode());
    	        		if (dcftab == null) {
    	        			dcftab = new EntDbDcftab();
    	        			dcftab.setUsec(dtfl);
    	        			dcftabBean.persist(dcftab);
    	        		}
    	        		dcftab.setIdfd(fltDaily.getId());
    	                ufisCalendar.setTime(delay.getPostedOn());
    	                dcftab.setCdat(ufisCalendar.getCedaString());
    	                dcftab.setDeca(delay.getCode());
    	                dcftab.setDura(String.format(FMT_DURA, delay.getAmount()));
    	                dcftab.setUseu(dtfl);
    	                dcftabBean.update(dcftab);
    	            }
    	        }
    	        
    	        // FEVTAB related info
    	        // RTG
    	        if (event.getRtg() != null) {
    	        	EntDbFevtab fevtab = fevtabBean.findByFltDailyId(fltDaily.getId());
    	        	if (fevtab == null) {
    	        		fevtab = new EntDbFevtab();
    	        		fevtab.setUSEC(dtfl);
    	        		fevtabBean.persist(fevtab);
    	        	}
    	        	if (event.getRtg().getLastDoorCl() != null) {
    	        		ufisCalendar.setTime(event.getRtg().getLastDoorCl());
    	        		fevtab.setSTTM(df.format(ufisCalendar.getTime()));
    	        	}
    	        	fevtab.setSTNM("Ready to Go");
    	        	fevtab.setSTFL(event.getRtg().getValue());
    	        	fevtab.setSTRM("");
    	        	fevtab.setUSEU(dtfl);
    	        	fevtabBean.update(fevtab);
    	        }
        	}
        }
        
        /**
    	 * 2013-12-26 updated by JGO - Adding fltDaily id to CEDA message
    	 */
    	List<String> fltDailyIds = fltDailyBean.getFltDailyIds(dto);
    	if (fltDailyIds != null && fltDailyIds.size() > 0) {
    		for (String id : fltDailyIds) {
    			INFOBJFLTDAILY infobjfltdaily = new INFOBJFLTDAILY();
    			infobjfltdaily.setID(id);
    			infobjfltdailylist.getINFOBJFLTDAILY().add(infobjfltdaily);
    		}
    	}

    	// ============= infobjFlight ===============================
        // infobjFlight - flight info update
        // ============= infobjFlight ===============================
        if (!CONST_DELETE.equalsIgnoreCase(_actionType)) {
        	// Airline
        	String airline = event.getCxCd();
            if (airline.length() == 2) {
                infobjFlight.setALC2(airline);
            } else {
                infobjFlight.setALC3(airline);
            }
            // Flight number in Ceda format
            infobjFlight.setFLTN(HpUfisUtils.formatCedaFltn(event.getFltNum()));
            if (airline.length() == 3) {
            	infobjFlight.setFLNO(airline + infobjFlight.getFLTN());
            } else {
            	infobjFlight.setFLNO(airline + " " + infobjFlight.getFLTN());
            }
            // Suffix
            if (HpUfisUtils.isNotEmptyStr(event.getFltSuffix())) {
                infobjFlight.setFLNS(event.getFltSuffix());
                // 2013-08-19 updated by JGO - adding suffix info to flno as well
                if (fltn.length() == 5) {
                	infobjFlight.setFLNO(infobjFlight.getFLNO() + event.getFltSuffix());
            	} else if (fltn.length() == 4) {
            		infobjFlight.setFLNO(infobjFlight.getFLNO() + " " + event.getFltSuffix());
            	} else if (fltn.length() == 3) {
            		infobjFlight.setFLNO(infobjFlight.getFLNO() + " " + " " + event.getFltSuffix());
            	}
            }

            /*// FLUT - Flight date in utc
            infobjFlight.setFLUT(fldaUtc);
            // FLDA(local) - if cannot find from tag, getting from schedule time 
            if (event.getFltDateLocal() != null) {
            	ufisCalendar.setTime(event.getFltDateLocal().getValue());
            	infobjFlight.setFLDA(ufisCalendar.getCedaDateString());
            } else {
            	// if ADID is D, refer from STOD, others STOA (yyyyMMdd)
            	if (ADID.D == infobjgeneric.getADID()) {
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStoa)) {
            			infobjFlight.setFLDA(fldaFromStoa);
            		}
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStod)) {
            			infobjFlight.setFLDA(fldaFromStod);
            		}
            	} else {
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStod)) {
            			infobjFlight.setFLDA(fldaFromStod);
            		}
            		if (HpUfisUtils.isNotEmptyStr(fldaFromStoa)) {
            			infobjFlight.setFLDA(fldaFromStoa);
            		}
            	}
            }*/
            
            // ADID
            if (HpEKConstants.HOPO.equalsIgnoreCase(event.getDepStn()) 
            		&& HpEKConstants.HOPO.equalsIgnoreCase(event.getArrStn())) {
                //Return Flight /TAxi
                infobjFlight.setADID(ADID.B);
                infobjFlight.setORG3(HpEKConstants.HOPO);
                infobjFlight.setDES3(HpEKConstants.HOPO);
                infobjFlight.setDEPN(String.valueOf(event.getDepNum()));

                // when body is return flight info, update to header
                infobjgeneric.setADID(ADID.B);
                infobjgeneric.setORG3(HpEKConstants.HOPO);
            	infobjgeneric.setDES3(HpEKConstants.HOPO);
            	// depNum
            } else if (HpEKConstants.HOPO.equalsIgnoreCase(event.getArrStn())) {
                //Arival
                infobjFlight.setADID(ADID.A);
                infobjFlight.setORG3(event.getDepStn());
                infobjFlight.setDES3(HpEKConstants.HOPO);
                // depNum
                infobjFlight.setDEPN(String.valueOf(event.getDepNum()));
            } else if (HpEKConstants.HOPO.equalsIgnoreCase(event.getDepStn())) {
                //Departure
                infobjFlight.setADID(ADID.D);
                infobjFlight.setORG3(HpEKConstants.HOPO);
                infobjFlight.setDES3(event.getArrStn());
                // depNum
                infobjFlight.setDEPN(String.valueOf(event.getDepNum()));
            }
            
        	// only HOPO message will pass full info
        	if (HpEKConstants.HOPO.equals(event.getArrStn()) 
    				|| HpEKConstants.HOPO.equals(event.getDepStn())) {
        		
        		// 2013-11-27 updated by JGO - flda and flut in infobj_flight only when hopo message
        		// FLUT - Flight date in utc
                infobjFlight.setFLUT(fldaUtc);
                
                // FLDA(local) - if cannot find from tag, getting from schedule time 
                if (event.getFltDateLocal() != null) {
                	ufisCalendar.setTime(event.getFltDateLocal().getValue());
                	infobjFlight.setFLDA(ufisCalendar.getCedaDateString());
                } else {
                	// if ADID is D, refer from STOD, others STOA (yyyyMMdd)
                	if (ADID.D == infobjgeneric.getADID()) {
                		/*if (HpUfisUtils.isNotEmptyStr(fldaFromStoa)) {
                			infobjFlight.setFLDA(fldaFromStoa);
                		}*/
                		if (HpUfisUtils.isNotEmptyStr(fldaFromStod)) {
                			infobjFlight.setFLDA(fldaFromStod);
                		}
                	} else {
                		/*if (HpUfisUtils.isNotEmptyStr(fldaFromStod)) {
                			infobjFlight.setFLDA(fldaFromStod);
                		}*/
                		if (HpUfisUtils.isNotEmptyStr(fldaFromStoa)) {
                			infobjFlight.setFLDA(fldaFromStoa);
                		}
                	}
                }
        		
        		// Service Type
                infobjFlight.setSTYP(event.getServiceType());
                
                // AcType
                // IATA aircraft subtype as per SSIM
                infobjFlight.setACT3(event.getAcSubType());
                
                // Registration
                // 2013-05-14 updated by JGO - if 4 digits regn, ignored
                // 2013-05-31 updated by JGO - if 4 digits or less, and all numeric regn, ignored
                if (event.getTailNo() != null && event.getTailNo().getValue() != null) {
                	//if (event.getTailNo().getValue().length() <= 4) {
                	if (event.getTailNo().getValue().matches(PATTERN_REGN)) {
                		infobjFlight.setREGN("");
                	} else {
                		infobjFlight.setREGN(event.getTailNo().getValue());
                	}
                } else {
                	// regn is null, clear value
                	//infobjFlight.setREGN("");
                }
                
                // schedule datetime
                infobjFlight.setSTOA(stoa);
                infobjFlight.setSTOD(stod);
                
                // TaxiOut/TaxiIn
                if (event.getTaxiOut() != null) {
                    infobjFlight.setEXOT(event.getTaxiOut().getValue().toString());
                }
                if (event.getTaxiIn() != null) {
                    infobjFlight.setEXIT(event.getTaxiIn().getValue().toString());
                }
                
                /**
                 * Flight Status(From CORE)
                 * CODE STATUS_DESCRIPTION
                 * 1 RTNG RETURNED, IN FLIGHT
                 * 2 DVTG DIVERTED, IN FLIGHT
                 * 3 PDEP NOT YET DEPARTED
                 * 4 OFBL TAXIING FOR TAKEOFF
                 * 5 ENRT IN FLIGHT
                 * 6 LAND LANDED AND TAXIING
                 * 7 ARVD ARRIVED
                 * 8 CNLD CANCELLED
                 * 9 DVTD ARRIVED DIVERSION AIRPORT
                 * 10 DNLD TAXIING DIVERSION AIRPORT
                 * 11 RROF REROUTED, TAXIING
                 * 12 RRTG REROUTED, IN FLIGHT
                 * 13 RRTD REROUTED, LANDED
                 * 14 RTDR RETURNED TO STAND
                 * 15 RTND RETURNED DEPARTURE AIRPORT
                 * 16 DLND DIVERTED
                 * 17 RRLD REROUTED AND LANDING
                 */
                String flightStatus = event.getFltStatus();
                infobjFlight.setISTA(flightStatus);
                infobjFlight.setFTYP(CONST_FTYP_O);
    			switch (flightStatus) {
    			case CONST_CANCEL:
    				infobjFlight.setFTYP(CONST_FTYP_X);
    				break;
    			}
    			
                // SiRemarks
                List<FlightEvent.Events.SiRemark> siRemarks = event.getSiRemark();
                StringBuilder siRemarkBuild = new StringBuilder();
                if (siRemarks != null) {
                	for (FlightEvent.Events.SiRemark remark : siRemarks) {
                		siRemarkBuild.append(remark.getValue());
                		siRemarkBuild.append(" ");
                	}
                }
                infobjFlight.setREM2(siRemarkBuild.toString());
                
                // 2013-11-22 updated by JGO - Adding Remark
                StringBuilder remarkBuild = new StringBuilder();
                for (FlightEvent.Events.Remark r : event.getRemark()) {
                	remarkBuild.append(r.getValue());
                	remarkBuild.append(" ");
                }
                //infobjFlight.setREM1(remarkBuild.toString());
                infobjFlight.setRRMK(remarkBuild.toString());
                
                // Cabin door and Cargo door events (clarify dorFlag and remark is for which field)
                // 2013-04-18 updated by JGO - change fevtab to fevtab list
                // 2013-05-15 updated by JGO - Remove these info
                /*
				if (event.getCABINDOORCLOSURE() != null) {
					INFOJFEVTAB cabinDoor = new INFOJFEVTAB();
					ufisCalendar.setTime(event.getCABINDOORCLOSURE().getValue());
					cabinDoor.setSTNM("CABIN DOOR CLOSURE");
					cabinDoor.setSTTM(ufisCalendar.getCedaString());
					cabinDoor.setSTFL(event.getDorFlag());
					cabinDoor.setSTRM(event.getDorRemark());
					infojfevtablist.getINFOJFEVTAB().add(cabinDoor);
				}
				if (event.getCARGODOORCLOSURE() != null) {
					INFOJFEVTAB cargoDoor = new INFOJFEVTAB();
					ufisCalendar.setTime(event.getCARGODOORCLOSURE().getValue());
					cargoDoor.setSTNM("CARGO DOOR CLOSURE");
					cargoDoor.setSTTM(ufisCalendar.getCedaString());
					cargoDoor.setSTFL(event.getDorFlag());
					cargoDoor.setSTRM(event.getDorRemark());
					infojfevtablist.getINFOJFEVTAB().add(cargoDoor);
				}
                */
                
                // Flight duration
                if (event.getFltTime() != null && event.getFltTime().getValue() != null) {
                	infobjFlight.setFDUR(event.getFltTime().getValue().toString());
                }
                // Schedule/Actual Block Time (min)
                if (event.getBlkTimeOrig() != null && event.getBlkTimeOrig().getValue() != null) {
                	infobjFlight.setSOOB(event.getBlkTimeOrig().getValue().toString());
                }
                if (event.getBlkTimeAct() != null && event.getBlkTimeAct().getValue() != null) {
                	infobjFlight.setAOOB(event.getBlkTimeAct().getValue().toString());
                }
                
                /**
                 *  <Fuel units="" type="" >xxxx</Fuel>
                 *  units not required
                 *  type required
                 *  value
                 *  
                 *  Design documents: 
                 *  units -> FUEU
                 *  type  -> FUET
                 */
                // 2013-06-19 updated by JGO - Move fuel related into FLT_FUEL_STATUS
                /* 2013-06-23 updated by JGO - As discussed with Ritu(DTP), 
                 * 							   fuel info will get from EGDS, no Core fuel info will be used
                // find flt_fuel_status by flight info
                EntDbFlightFuelStatus fltFuelStatus = fltFuelStatusBean.findByFlight(
                		carrier, 
                		fltn, 
                		infobjgeneric.getFLNS(), 
                		infobjgeneric.getFLDA());
                if (fltFuelStatus == null) {
                	fltFuelStatus = new EntDbFlightFuelStatus();
                	fltFuelStatus.setAirlineCode2(carrier);
                	fltFuelStatus.setFlightNumber(fltn);
                	if (HpUfisUtils.isNotEmptyStr(infobjgeneric.getFLNS())) {
                		fltFuelStatus.setFltSuffix(infobjgeneric.getFLNS().charAt(0));
                	}
                	fltFuelStatus.setFltDate(infobjgeneric.getFLDA());
                	fltFuelStatus.setDataSource(MSGIF);
                	fltFuelStatus.setRecStatus(' ');
                	fltFuelStatusBean.persist(fltFuelStatus);
                }
                // TODO check the default units
                // Units to measurement for the fuel
                List<FlightEvent.Events.Fuel> fuels = event.getFuel();
                boolean isFuelInfoIncluded = false;
            	for (FlightEvent.Events.Fuel fuel : fuels) {
                    //infojxaftab.setFUEU(fuel.getUnits());
                    //infojxaftab.setFUET(fuel.getType());
            		isFuelInfoIncluded = true;
					if (fuel.getType() != null) {
						float value = fuel.getValue().floatValue();
						// fuel units - need to convert to kg
            			if (HpUfisUtils.isNotEmptyStr(fuel.getUnits())) {
            				// convert to kg
            			}
            			// fule types
						switch (fuel.getType()) {
						case FUEL_TYPE_TOW:
							// take off weight
	            			infojxaftab.setTOWT(String.valueOf(value));
							break;
						case FUEL_TYPE_ZFW:
							// zero fuel weight
	            			infojxaftab.setZFWT(String.valueOf(value));
							break;
						case FUEL_TYPE_TOF:
							fltFuelStatus.setTakeoffFuel(value);
							break;
						case FUEL_TYPE_FOA:
							fltFuelStatus.setArrivalFuel(value);
							break;
						default:
							LOG.debug("Unexpected fuel type: {}", fuel.getType());
							break;
						}
					}
                }
            	
            	if (isFuelInfoIncluded) {
            		fltFuelStatusBean.update(fltFuelStatus);
            	}
                */
                
                // XAFTAB related info
                // AcOwner
                if (event.getAcOwner() != null) {
                	infojxaftab.setACOW(event.getAcOwner());
                	isXafFound = true;
                }
                
                // AcVersion
                if (event.getAcVersion() != null) {
                	infojxaftab.setACEX(event.getAcVersion());
                	isXafFound = true;
                }
                
                // MvtDatetimes - Local
                List<FlightEvent.Events.MvtDateTime> mvtDateTimes = event.getMvtDateTime();
                /**
                 * AirborneEst (Estimated Airborne time)      - ETON
                 * AirborneAct (Actual Aiborne time)          - ATON 
                 * LandedEst (Estimated landing time)         - ELDN
                 * LandedAct (Actual landing time)            - ALDN
                 * OnBlocksEst (Estimated on block time)      - EIBN
                 * OnBlocksAct (Actual on block time)	      - AIBN
                 * OffBlocksEst (Estimated off block time) 	  - EOBN
                 * OffBlocksAct (Actual off block time)       - AOBN
                 * StartTaxiAct (Actual time of start taxing) - ASTN
                 * NextInformation:                           - NEXI(XAFTAB) 
                 *  (Time at which next information for the flight shall be made available) 
                 *
                 */
                for (FlightEvent.Events.MvtDateTime mvtDateTime : mvtDateTimes) {
                    String mvtType = mvtDateTime.getType() == null ? "" : mvtDateTime.getType();
                    String cedaTime = "";
                    if (mvtDateTime.getValue() != null) {
                    	ufisCalendar.setTime(mvtDateTime.getValue());
                    	//cedaTime = ufisCalendar.getCedaString();
                    	cedaTime = df.format(ufisCalendar.getTime());
                    }
                    switch (mvtType) {
                        case "AirborneAct":
                            infobjFlight.setATON(cedaTime);
                            break;
                        case "LandedEst":
                            infobjFlight.setELDN(cedaTime);
                            break;
                        case "LandedAct":
                            infobjFlight.setALDN(cedaTime);
                            break;
                        case "OnBlocksAct":
                        	infobjFlight.setAIBN(cedaTime);
                            break;
                        case "OnBlocksEst":
                        	infobjFlight.setEIBN(cedaTime);                            
                            break;
                        case "OffBlocksEst":
                            infobjFlight.setEOBN(cedaTime);
                            break;
                        case "OffBlocksAct":
                            infobjFlight.setAOBN(cedaTime);
                            break;
                        case "AirborneEst":
                            infobjFlight.setETON(cedaTime);
                            break;
                        case "StartTaxiAct":
                            infobjFlight.setASTN(cedaTime);
                            break;
                        case "NextInformation":
                            infobjFlight.setNXTI(cedaTime);
                            infojxaftab.setNEXI(cedaTime);
                            isXafFound = true;
                            break;
                    }
                }
                
                // RTG
                FlightEvent.Events.Rtg rtg = event.getRtg();
                if (rtg != null) {
                	INFOJFEVTAB fev = new INFOJFEVTAB();
                	if (rtg.getLastDoorCl() != null) {
                		ufisCalendar.setTime(rtg.getLastDoorCl());
                		//fev.setSTTM(ufisCalendar.getCedaString());
                		fev.setSTTM(df.format(ufisCalendar.getTime()));
                	}
                    fev.setSTNM("Ready to Go");
                    fev.setSTFL(rtg.getValue());
                    fev.setSTRM("");
                    // flight daily id
                    fev.setIDFD(fltDaily.getId());
                    infojfevtablist.getINFOJFEVTAB().add(fev);
                }
                
                // Get Delays
                if (flightEvent.getDelays() != null) {
                	List<FlightEvent.Delays.Delay> delays = flightEvent.getDelays().getDelay();
                	for (FlightEvent.Delays.Delay delay : delays) {
                        INFOJDCFTAB infojdcftab = new INFOJDCFTAB();
                        ufisCalendar.setTime(delay.getPostedOn());
                        infojdcftab.setCDAT(ufisCalendar.getCedaString());
                        infojdcftab.setDECA(delay.getCode());
                        infojdcftab.setDURA(String.format(FMT_DURA, delay.getAmount()));
                        // flight daily id
                        infojdcftab.setIDFD(fltDaily.getId());
                        infojdcftablist.getINFOJDCFTAB().add(infojdcftab);
                    }
//                } else {
//                	// if tags are null, clear all delay info from dcftab
//                	INFOJDCFTAB infojdcftab = new INFOJDCFTAB();
//                	infojdcftablist.getINFOJDCFTAB().add(infojdcftab);
                }
        	}
        }
        
        if (ACTIONTYPE.D != _msgActionType) {
        	// Handle Via for non-return flights
        	StringBuilder vial = new StringBuilder();
        	boolean isFirstMsg = true;
        	if (msgChains != null && ADID.B != infobjgeneric.getADID()) {
        		// Let Ceda decide insert or update
        		_msgActionType = null;
        		
            	Iterator<EntDbCoreTmp> iter = msgChains.iterator();
            	while (iter.hasNext()) {
            		EntDbCoreTmp temp = iter.next();
            		if (isFirstMsg) {
            			infobjFlight.setORG3(temp.getDepStn());
            			// 2013-12-30 updated by JGO - Set stoa as orign
            			infobjFlight.setSTOD(temp.getSchDep());
                		isFirstMsg = false;
                	}
                	// last record
                	if (!iter.hasNext()) {
            			infobjFlight.setDES3(temp.getArrStn());
            			// 2013-12-30 updated by JGO - Set stod as destination
            			infobjFlight.setSTOA(temp.getSchArr());
                		break;
                	}
                	// update vias
                    vial.append(createVia(temp, infobjgeneric.getADID(), ufisCalendar));
            	}
            	infobjFlight.setVIAN(Integer.toString(msgChains.size() -1));
            	infobjFlight.setVIAL(vial.toString());
            	if (ADID.A.equals(infobjFlight.getADID())) {
    				LOG.info("VIA Found  {} {} {} {}",
    						new Object[] { infobjFlight.getADID(), vial.toString(),
    								flightEvent.getFlightId().getDepStn(),
    								flightEvent.getFlightId().getArrStn() });
                } else {
    				LOG.info(
    						"VIA Found  {} {} {} {}",
    						new Object[] { infobjFlight.getADID(),
    								flightEvent.getFlightId().getDepStn(),
    								flightEvent.getFlightId().getArrStn(),
    								vial.toString() });
                }
        	}
        }
        
        // transUserId and transDatetime
        if (flightEvent.getAudit() != null) {
        	infobjFlight.setUSEU(flightEvent.getAudit().getTransUserId());
        	if (flightEvent.getAudit().getTransDateTime() != null) {
        		ufisCalendar.setTime(flightEvent.getAudit().getTransDateTime());
        		infobjFlight.setLSTU(df.format(ufisCalendar.getTime()));
        	}
        }
        
        MSGOBJECTS msgobjects = new MSGOBJECTS();
        msgobjects.setINFOBJFLIGHT(infobjFlight);
        msgobjects.setINFOJFEVTABLIST(infojfevtablist);
        msgobjects.setINFOBJFLTDAILYLIST(infobjfltdailylist);
        if (isXafFound) {
        	// flight daily id
        	infojxaftab.setIDFD(fltDaily.getId());
        	msgobjects.setINFOJXAFTAB(infojxaftab);
        }
        msgobjects.setINFOJDCFTABLIST(infojdcftablist);
        UfisMarshal ufisMarshal = new UfisMarshal(_ms, _msgActionType, MSGIF, null);
        _returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);
        return true;
    }
	
	public String getReturnXml() {
		return _returnXML;
	}

    //TODO
    //format the VIA to a CEDA VIA Format
    /*EXPLODE_DETAILS = VIAS
     EXPLODE_VIAS = VIAL,8,120,VIA
     VIA_1 =  FLG,Display Flag,1,CHAR, ,N, ,
     VIA_2 =  AP3,APC3 of Via,3,CHAR, ,N, ,
     VIA_3 =  AP4,APC4 of Via,4,CHAR, ,N, ,
     VIA_4 =  STA,STOA at Via,14,DATE, ,N, ,
     VIA_5 =  ETA,ETOA at Via,14,DATE, ,N, ,
     VIA_6 =  LND,LAND at Via,14,DATE, ,N, ,
     VIA_7 =  ONB,ONBL at Via,14,DATE, ,N, ,
     VIA_8 =  STD,STOD at Via,14,DATE, ,N, ,
     VIA_9 =  ETD,ETOD at Via,14,DATE, ,N, ,
     VIA_10 = OFB,OFBL at Via,14,DATE, ,N, ,
     VIA_11 = AIR,AIRB at Via,14,DATE, ,N, ,
     */
    private String createVia(EntDbCoreTmp viaFlightEvent, ADID adid, HpUfisCalendar ufisCalendar) {
        StringBuilder vial = new StringBuilder();
        if (ADID.A.equals(adid)) {
            //VIA_1 FLG
            vial.append(String.format("%-" + 1 + "s", ""));
            //VIA_2 AP3
            //vial.append(String.format("%-" + 3 + "s", viaFlightEvent.getFlightId().getDepStn()));
            vial.append(String.format("%-" + 3 + "s", viaFlightEvent.getArrStn()));
            //VIA_3 AP4
            vial.append(String.format("%-" + 4 + "s", ""));
            //VIA_4 STA
            String sta = viaFlightEvent.getSchArr();
//            for (SchdDateTime schedule : event.getSchdDateTime()) {
//             	if ("Arrival".equalsIgnoreCase(schedule.getType())) {
//             		ufisCalendar.setTime(schedule.getValue());
//             		sta = ufisCalendar.getCedaString();
//             	}
//            }
            vial.append(String.format("%-" + 14 + "s", sta));
            //VIA_5 ETA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_6 LND
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_7 ONB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_8 STD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_9 ETD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_10 OFB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_11 AIR
            vial.append(String.format("%-" + 14 + "s", ""));
        } else {
            //VIA_1 FLG
            vial.append(String.format("%-" + 1 + "s", ""));
            //VIA_2 AP3
            vial.append(String.format("%-" + 3 + "s", viaFlightEvent.getArrStn()));
            //VIA_3 AP4
            vial.append(String.format("%-" + 4 + "s", ""));
            //VIA_4 STA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_5 ETA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_6 LND
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_7 ONB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_8 STD
            String std = viaFlightEvent.getSchDep();
//        	for (SchdDateTime schedule : event.getSchdDateTime()) {
//             	if ("Departure".equalsIgnoreCase(schedule.getType())) {
//             		ufisCalendar.setTime(schedule.getValue());
//             		std = ufisCalendar.getCedaString();
//             	}
//            }
            vial.append(String.format("%-" + 14 + "s", std));
            //VIA_9 ETD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_10 OFB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_11 AIR
            vial.append(String.format("%-" + 14 + "s", ""));
        }
        return vial.toString();
    }

	public FlightEvent getFlightData() {
		return flightData;
	}

	public void setFlightData(FlightEvent flightData) {
		this.flightData = flightData;
	}
	
}
