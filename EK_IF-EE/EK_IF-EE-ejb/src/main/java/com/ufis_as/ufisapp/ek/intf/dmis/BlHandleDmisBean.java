package com.ufis_as.ufisapp.ek.intf.dmis;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.exco.MSGOBJECTS.INFOBJTOWINGDELAY;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.dmis.bothflightevent.BothFlightEvent;
import ek.dmis.bothflightevent.FlightInfo;
import ek.dmis.towdelay.Root;

/**
 * 
 * @author JGO
 * @version 1.00: 2013-04-02 GFO - version
 * @version 1.01: 2013-06-05 JGO - Change the handler to EJB Stateless session bean
 * @version 1.02: 2013-06-07 JGO - adding regn and remove ftyp and adid for tow related messages
 * @version 1.03: 2013-06-11 JGO - TowDetails message root tag will be <root>
 * @version 1.04: 2013-06-13 JGO - Put HOPO to constant
 * @version 1.05: 2013-06-14 JGO - RRMK for flight remark, ISTA for flight status(origin)
 * @version 1.06: 2013-06-17 JGO - DMIS-Flight via3 -> vial
 * @version 1.07: 2013-06-20 BTR
 * @version 1.08: 2013-07-02 JGO - chagne flda to local date
 * @version 1.09: 2013-07-02 JGO - adding flut to store FltDate in UTC
 * @version 1.11: 2013-08-20 JGO - Setting STDT for dmis flight process in the handleBean class
 * @version 1.12: 2013-08-27 JGO - if no xaftab data, remove this empty tag from message
 * @version 1.13: 2013-08-28 JGO - STDT should be in UTC 
 * 								   Fix ScheduleDate convert to local offset
 * 								   Adding history info
 * 								   Adding mandatory check for flight msg
 * 								   For EK flight, only update 3 fields
 * @version 1.14: 2013-09-05 JGO - adding check for fltn(contain invalid character or not)
 * @version 1.15: 2013-10-01 JGO - DateTime format change in flight messages
 * 								   Old: yyyy-MM-dd HH:mm as UTC
 * 								   New: yyyy-MM-ddTHH:mm:ss+hh:mm as Local to Station
 * 								   Adding support for ChoksOn and ChoksOff
 * @version 1.16: 2013-10-06 JGO - Adding error/warn info inform and store feature
 * @version 1.17: 2013-10-23 JGO - Remove message content logging when error encounter(log raw message aldy at received time)
 * 								   Change to more detail exception info(exception_data)
 * @version 1.18: 2013-11-26 JGO - Remove static modifier from BaseDTO
 * @version 1.19: 2013-12-27 JGO - Adding ROUT for INFOBJ_FLIGHT
 *                                 Change R suffix timing field to D suffix (e.g. AIBR -> AIBD)
 *                                 Remove PABX and PAEX fields
 *                                 Store towing plan time to STOA/STOD
 *                                 Store towing actual time to AOBD/AIBD
 * 
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlHandleDmisBean extends BaseDTO {
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BlHandleDmisBean.class);
	
	/**
	 * CDI
	 */
    @EJB
    private IAfttabBeanLocal afttabBean;
    @EJB
    private BlUfisExceptionQueue ufisExptLogging;
    
	/**
	 * Constants and Variables
	 */
    //private static final String HOPO = "DXB";
    private static final String MSGIF = "EK-DMIS";
	private static final String MSGIF_SUBSYS = "-TOW"; 
	//private static final String TOWFTYP = "T";
    private static final int CODESHARESPACE = 8;

    protected String _flightXml = "";
    protected String _returnXML = "";

    protected String _actionType = "";
    protected String _messageType = "";
    protected ACTIONTYPE _msgActionType;
    
//    private JAXBContext _cnxJaxb;
//    private JAXBContext _cnxJaxbM;
//    private Unmarshaller _um;
//    private Marshaller _ms;
    
    private static final Pattern p = Pattern.compile("\\D");
    
    // suffix 
    //private static final String FLT_SUFFIX = "[A-Za-z]";

    // date time related
	private static final String DT_LOC_SS = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(?:[+-]\\d{2}:\\d{2})?";
	//private static final String DT_LOC = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}(?:[+-]\\d{2}:\\d{2})?";
	private static DateFormat DF_CEDADATE = new SimpleDateFormat("yyyyMMdd");
    private static DateFormat DF_CEDA = new SimpleDateFormat("yyyyMMddHHmm00");
    private XMLGregorianCalendar xmlGregCal = null;
    
    private static EntUfisMsgHead notifyHeader;
//    private List<EntUfisMsgExpt> excpts = new ArrayList<>();

    /**
     * Default constructor. 
     */
    public BlHandleDmisBean() {
		try {
            _cnxJaxb = JAXBContext.newInstance(
            		BothFlightEvent.class,
            		ek.dmis.towdetails.Root.class,
            		Root.class);
            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
            _um = _cnxJaxb.createUnmarshaller();
            _ms = _cnxJaxbM.createMarshaller();
            
            DF_CEDADATE.setTimeZone(HpEKConstants.utcTz);
            DF_CEDA.setTimeZone(HpEKConstants.utcTz);
            
            if (notifyHeader == null) {
            	notifyHeader = new EntUfisMsgHead();
            	notifyHeader.setHop(HpEKConstants.HOPO);
            	notifyHeader.setOrig(MSGIF);
            }

            tableRef = HpUfisAppConstants.CON_IRMTAB;
        	exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
        	
        	dtfl = com.ufis_as.ufisapp.configuration.HpEKConstants.EK_DMIS_SOURCE;
        	
        } catch (JAXBException ex) {
            LOG.error("JAXBException when initialize JaxbContext, marshaller and unmarshaller");
        }
    }

    public boolean unMarshal(String message, long irmtabRef) {
        try {
        	// clear
        	_msgActionType = null;
        	data.clear();
        	
        	// set urno of irmtab
        	irmtabUrno = irmtabRef;
        	
        	_flightXml = message;
            if (_flightXml.contains("BothFlightEvent")) {
                BothFlightEvent flightData = (BothFlightEvent) _um.unmarshal(new StreamSource(new StringReader(_flightXml)));
                if (readEvent(flightData)) {
                    LOG.debug("FlightEvent {}", flightData.getFlightInfo().getFlightNumber());
                    return true;
                } else {
                    return false;
                }
            } else if (_flightXml.contains("PLAN_FROM_BAY") 
            		|| _flightXml.contains("PLAN_TO_BAY")
            		|| _flightXml.contains("PLAN_START")
            		|| _flightXml.contains("PLAN_END")
            		|| _flightXml.contains("ACTUAL_FROM_BAY")
            		|| _flightXml.contains("ACTUAL_TO_BAY")
            		|| _flightXml.contains("ACTUAL_START")
            		|| _flightXml.contains("ACTUAL_END")) {
            	JAXBElement<ek.dmis.towdetails.Root> towDetails = _um.unmarshal(new StreamSource(new StringReader(_flightXml)), ek.dmis.towdetails.Root.class);
                if (readEvent(towDetails.getValue())) {
                    LOG.debug("TowDetails {}", towDetails.getValue().getDETAILS().getREGISTRATIONNUMBER());
                    return true;
                } else {
                    return false;
                }
            } else if (_flightXml.contains("root")) {
                Root towDelay = (Root) _um.unmarshal(new StreamSource(new StringReader(_flightXml)));
                if (readEvent(towDelay)) {
                    LOG.debug("TowDelay {}", towDelay.getFLIGHTNUMBER());
                    return true;
                } else {
                    return false;
                }

            }
            return false;
        } catch (JAXBException ex) {
            LOG.error("DMIS JAXB exception {}", ex.toString());
            //LOG.error("Dropped Message details: \n{}", _flightXml);
			addExptInfo(EnumExceptionCodes.EXSDF.name(),
					EnumExceptionCodes.EXSDF.toString());
            return false;
        }
    }

    public boolean readEvent(ek.dmis.towdetails.Root towDetails) {
        if (towDetails != null) {
            return tranformFlight(towDetails);
        } else {
            LOG.warn("Tow Details Null detected");
            return false;
        }
    }

    public boolean readEvent(Root towDelay) {
    	if (towDelay != null) {
    		return transformFlight(towDelay);
    	} else {
    		LOG.warn("Tow Delay Null detected");
    		return false;
    	}
    }

    public boolean readEvent(BothFlightEvent flightEvent) {
        if (flightEvent != null) {
            //Check If flight exists in AFTTAB
            /*
             FlightNumber
             scheduledDate
             arrivalDepartureFlag
             */
            return tranformFlight(flightEvent);
        } else {
            LOG.warn("Flight Event Null detected");
            return false;
        }
    }

    private boolean tranformFlight(ek.dmis.towdetails.Root towDetails) {
        HpUfisCalendar ufisCalendar = new HpUfisCalendar();
        ufisCalendar.setCustomFormat(HpEKConstants.DMIS_TOW_TIME_FORMAT); //03/08/2012 02:30:00


        //INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
        MSGOBJECTS msgobjects = new MSGOBJECTS();
        INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();

        /*Towing */
        MSGOBJECTS.INFOBJTOWING towings = new MSGOBJECTS.INFOBJTOWING();

        // inforgenerics
        infobjgeneric.setDES3(HpEKConstants.HOPO);
        infobjgeneric.setORG3(HpEKConstants.HOPO);
        //infobjgeneric.setADID(ADID.B);
        //infobjgeneric.setFTYP(TOWFTYP);

        String flno = towDetails.getHEADER().getFLIGHTNUMBER();
        // Format the flno as ufis format
        String alc2 = "";
        String fltn = "";
        if (flno != null && flno.length() > 5) {
            alc2 = flno.substring(0, 2);
            fltn = flno.substring(2, 6);
            fltn = HpUfisUtils.formatCedaFltn(fltn);
        } else {
        	LOG.error("Wrong format of flight number");
        	addExptInfo(EnumExceptionCodes.EWVAL.name(),
					"FlightNumber should follow format as IATA airline code + 4 digits number + 1 digit suffix(optional)");
            return false;
        }
        //infobjFlight.setALC2(alc2);
        //infobjFlight.setFLTN(fltn);
        //infobjFlight.setFLNO(alc2 + " " + fltn);
        infobjgeneric.setFLNO(alc2 + " " + fltn);
        
        //Get the Suffix
        if (flno != null && flno.length() > 6) {
            //infobjFlight.setFLNS(flno.substring(6));
            infobjgeneric.setFLNS(flno.substring(6));
        }
        
        // if not EK or QF flight, then drop
        if (!"EK".equalsIgnoreCase(alc2) && !"QF".equalsIgnoreCase(alc2)) {
        	LOG.debug("DMIS message has been dropped due to non-EK and non-QF flights");
        	//LOG.debug("Dropped Message details: \n{}", _flightXml);
			addExptInfo(EnumExceptionCodes.EWALC.name(),
					"Non EK or QF flight found");
        	return false;
        }
        
        if (towDetails.getHEADER().getFLIGHTSTASTD() != null) {
            ufisCalendar.setTime(towDetails.getHEADER().getFLIGHTSTASTD(), ufisCalendar.getCustomFormat());
            infobjgeneric.setSTDT(ufisCalendar.getCedaString());
            // flda in utc -> Flut
            towings.setFLUT(ufisCalendar.getCedaDateString());
            // convert to local for update flda
            ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_UTC_LOCAL, EnumTimeInterval.Hours);
            towings.setFLDA(ufisCalendar.getCedaDateString());
            //infobjgeneric.setSTOA(ufisCalendar.getCedaString());
            //infobjgeneric.setSTOD(ufisCalendar.getCedaString());
        }
        //infobjgeneric.setREGN(towDetails.getDETAILS().getREGISTRATIONNUMBER());

        // towing details
        //towings.setTOID(towDetails.getDETAILS().getJOBID());
        towings.setDEPN(towDetails.getDETAILS().getJOBID());
        towings.setREGN(towDetails.getDETAILS().getREGISTRATIONNUMBER());
        //towings.setTWTP(TOWFTYP);
        // Plan
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getPLANFROMBAY())) {
        	towings.setPFRB(towDetails.getDETAILS().getPLANFROMBAY());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getPLANTOBAY())) {
            towings.setPTOB(towDetails.getDETAILS().getPLANTOBAY());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getPLANSTART())) {
            ufisCalendar.setTime(towDetails.getDETAILS().getPLANSTART(), ufisCalendar.getCustomFormat());
            // 2013-12-27 updated by JGO - Change to STOD
            // towings.setPABS(ufisCalendar.getCedaString());
            towings.setSTOD(ufisCalendar.getCedaString());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getPLANEND())) {
            ufisCalendar.setTime(towDetails.getDETAILS().getPLANEND(), ufisCalendar.getCustomFormat());
            // 2013-12-27 updated by JGO - Change to STOA
            //towings.setPAES(ufisCalendar.getCedaString());
            towings.setSTOA(ufisCalendar.getCedaString());
        }
        
        // Actual
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getACTUALFROMBAY())) {
            towings.setPSTD(towDetails.getDETAILS().getACTUALFROMBAY());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getACTUALTOBAY())) {
            towings.setPSTA(towDetails.getDETAILS().getACTUALTOBAY());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getACTUALSTART())) {
            ufisCalendar.setTime(towDetails.getDETAILS().getACTUALSTART(), ufisCalendar.getCustomFormat());
            // 2013-12-27 updated by JGO - Change to AOBD
            //towings.setPABA(ufisCalendar.getCedaString());
            //towings.setAOBG(ufisCalendar.getCedaString());
            towings.setAOBD(ufisCalendar.getCedaString());
        }
        if (HpUfisUtils.isNotEmptyStr(towDetails.getDETAILS().getACTUALEND())) {
            ufisCalendar.setTime(towDetails.getDETAILS().getACTUALEND(), ufisCalendar.getCustomFormat());
            // 2013-12-27 updated by JGO - Change to AIBD
            //towings.setPAEA(ufisCalendar.getCedaString());
            //towings.setAIBG(ufisCalendar.getCedaString());
            towings.setAIBD(ufisCalendar.getCedaString());
        }

        /* Set the fields that uniquely identify a flight
         * the rest are done in the UfisMarshal
         */
        //msgobjects.setINFOBJFLIGHT(infobjFlight);
        msgobjects.setINFOBJTOWING(towings);

        UfisMarshal ufisMarshal = new UfisMarshal(_ms, null, MSGIF + MSGIF_SUBSYS, null);
        _returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);

        return true;
    }
    
    private boolean transformFlight(Root towDelay) {
    	HpUfisCalendar ufisCalendar = new HpUfisCalendar();
        ufisCalendar.setCustomFormat(HpEKConstants.DMIS_TOW_TIME_FORMAT);
        
        INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
        //INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
        
        String flno = towDelay.getFLIGHTNUMBER();
        String alc2 = "";
        String fltn = "";
        if (flno != null && flno.length() > 5) {
            alc2 = flno.substring(0, 2);
            fltn = flno.substring(2, 6);
            fltn = HpUfisUtils.formatCedaFltn(fltn);
        } else {
        	LOG.error("Wrong format of flight number");
        	addExptInfo(EnumExceptionCodes.EWVAL.name(),
					"FlightNumber should follow format as IATA airline code + 4 digits number + 1 digit suffix(optional)");
            return false;
        }
        //infobjFlight.setALC2(alc2);
        //infobjFlight.setFLTN(fltn);
        //infobjFlight.setFLNO(alc2 + fltn);
        
        //Get the Suffix
        if (flno != null && flno.length() > 6) {
            //infobjFlight.setFLNS(flno.substring(6));
            infobjgeneric.setFLNS(flno.substring(6));
        }
        
        // if not EK or QF flight, then drop
        if (!"EK".equalsIgnoreCase(alc2) && !"QF".equalsIgnoreCase(alc2)) {
        	LOG.debug("DMIS Flight message has been dropped due to non-EK and non-QF flights");
        	//LOG.debug("Dropped Message details: \n{}", _flightXml);
			addExptInfo(EnumExceptionCodes.EWALC.name(),
					"Non EK or QF flight found");
        	return false;
        }
        if (HpUfisUtils.isNotEmptyStr(towDelay.getFLIGHTSTASTD())) {
            ufisCalendar.setTime(towDelay.getFLIGHTSTASTD(), ufisCalendar.getCustomFormat());
            infobjgeneric.setSTDT(ufisCalendar.getCedaString());
            //infobjgeneric.setSTOA(ufisCalendar.getCedaString());
            //infobjgeneric.setSTOD(ufisCalendar.getCedaString());
        }
        infobjgeneric.setFLNO(alc2 + " " + fltn);
        infobjgeneric.setORG3(HpEKConstants.HOPO);
        infobjgeneric.setDES3(HpEKConstants.HOPO);
        //infobjgeneric.setFTYP(TOWFTYP);
        //infobjgeneric.setADID(ADID.B);
        
        //infobjFlight.setFTYP(TOWFTYP);
        //infobjFlight.setADID(ADID.B);
        
        MSGOBJECTS msgobjects = new MSGOBJECTS();
        MSGOBJECTS.INFOBJTOWINGDELAY delay = new INFOBJTOWINGDELAY();
        
        delay.setDURN(towDelay.getDELAYCODE());
        // format duration to 4-char, e.g. 0020
        String dura = towDelay.getDELAYDURATION();
        if (HpUfisUtils.isNotEmptyStr(dura)) {
        	dura = String.format("%1$04d", Integer.parseInt(dura));
        }
        delay.setDURA(dura);
        delay.setREMA(towDelay.getDELAYREMARKS());
        //msgobjects.setINFOBJFLIGHT(infobjFlight);
        msgobjects.setINFOBJTOWINGDELAY(delay);
        
        UfisMarshal ufisMarshal = new UfisMarshal(_ms, null, MSGIF + MSGIF_SUBSYS, null);
        _returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);
    	return true;
    }

    private boolean tranformFlight(BothFlightEvent flightEvent) {
    	
    	// mandatory check
    	boolean mandatoryValid = true;
        if (flightEvent == null || flightEvent.getFlightInfo() == null) {
        	LOG.debug("BothFlightEvent or FlightInfo cannot be null after unmarshal ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "BothFlightEvent or FlightInfo cannot be null after unmarshal");
        	mandatoryValid = false;
        }
        if (HpUfisUtils.isNullOrEmptyStr(flightEvent.getFlightInfo().getFlightNumber())) {
        	LOG.debug("flightNumber cannot be null or empty ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "flightNumber cannot be null or empty");
        	mandatoryValid = false;
        }
        if (HpUfisUtils.isNullOrEmptyStr(flightEvent.getFlightInfo().getScheduledDate())) {
        	LOG.debug("scheduleDate cannot be null or empty ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "scheduleDate cannot be null or empty");
        	mandatoryValid = false;
        }
        if (HpUfisUtils.isNullOrEmptyStr(flightEvent.getFlightInfo().getArrivalDepartureFlag())) {
        	LOG.debug("arrivalDepartureFlag cannot be null or empty ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "arrivalDepartureFlag cannot be null or empty");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getAircraftType() == null) {
        	LOG.debug("AircraftType cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "AircraftType cannot be null");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getAirlineCode() == null) {
        	LOG.debug("airliineCode cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "airliineCode cannot be null");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getHandlingTerminal() == null) {
        	LOG.debug("handlingTerminal cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "handlingTerminal cannot be null");
        	mandatoryValid = false;
        }
        if (HpUfisUtils.isNullOrEmptyStr(flightEvent.getFlightInfo().getDepartureStation())) {
        	LOG.debug("departureStation cannot be null or empty ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "departureStation cannot be null or empty");
        	mandatoryValid = false;
        }
        if (HpUfisUtils.isNullOrEmptyStr(flightEvent.getFlightInfo().getArrivalStation())) {
        	LOG.debug("arrivalStation cannot be null or empty ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "arrivalStation cannot be null or empty");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getEstimatedDate() == null) {
        	LOG.debug("estimateDate cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "estimateDate cannot be null");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getLatestDate() == null) {
        	LOG.debug("latestDate cannot be null ");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "latestDate cannot be null");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getRegistrationNumber() == null) {
        	LOG.debug("registrationNumber cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "registrationNumber cannot be null");
        	mandatoryValid = false;
        }
        if (flightEvent.getFlightInfo().getFlightStatus() == null) {
        	LOG.debug("flightStatus cannot be null");
        	//LOG.debug("Message dropped: \n{}", _flightXml);
        	addExptInfo(EnumExceptionCodes.EMAND.name(), "flightStatus cannot be null");
        	mandatoryValid = false;
        }
        
        if (!mandatoryValid) {
        	return false;
        }
        
        HpUfisCalendar ufisCalendar = new HpUfisCalendar();
        //ufisCalendar.setCustomFormat(HpEKConstants.DMIS_TIME_FORMAT);

        INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
        INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
        INFOJXAFTAB infojxaftab = new INFOJXAFTAB();

        FlightInfo flightInfo = null;
        String adid = "";
        String patkingT = "";
        String dubaiOrigin = "N";

        String viaRoutes = "";
        String arrivalStation = "";
        String departureStation = "";

        boolean isXafFound = false;
        if (flightEvent.getFlightInfo() != null) {
            flightInfo = flightEvent.getFlightInfo();
            // Format the flno as ufis format
            String alc2 = "";
            String fltn = "";
            String flns = "";
            String flno = "";
            if (flightInfo.getFlightNumber() != null && flightInfo.getFlightNumber().length() > 5) {
	            alc2 = flightInfo.getFlightNumber().substring(0, 2);
	            fltn = flightInfo.getFlightNumber().substring(2, 6);
	            fltn = HpUfisUtils.formatCedaFltn(fltn);
	            if ("".equals(fltn)) {
					LOG.debug("FlightNumber should follow format as IATA airline code + 4 digits number + 1 digit suffix(optional) ");
	            	//LOG.debug("Message dropped: \n{}", _flightXml);
	    			addExptInfo(EnumExceptionCodes.EWFMT.name(),
	    					"FlightNumber should follow format as IATA airline code + 4 digits number + 1 digit suffix(optional)");
	            	return false;
	            }
	            
	            // flns
	            if (flightInfo.getFlightNumber().length() == 7) {
	            	flns = flightInfo.getFlightNumber().substring(6);
	            }
            } else {
            	LOG.error("Wrong format of flight number");
            	addExptInfo(EnumExceptionCodes.EWVAL.name(),
    					"FlightNumber should follow format as IATA airline code + 4 digits number + 1 digit suffix(optional)");
                return false;
            }
            infobjFlight.setALC2(alc2);
            infobjFlight.setFLTN(fltn);
            flno = HpUfisUtils.formatCedaFlno(alc2, fltn, flns);
            infobjFlight.setFLNO(flno);
            infobjgeneric.setFLNO(flno);
            
            //Get the Suffix
            if (flightInfo.getFlightNumber() != null && flightInfo.getFlightNumber().length() > 6) {
                infobjFlight.setFLNS(flightInfo.getFlightNumber().substring(6));
            }
            
            // adid
            adid = flightInfo.getArrivalDepartureFlag();
            
            // schedule date in UTC from message
            //if (flightInfo.getScheduledDate() != null) {
            if (HpUfisUtils.isNotEmptyStr(flightInfo.getScheduledDate())) {
            	if (flightInfo.getScheduledDate().matches(DT_LOC_SS)) {
            		try {
						xmlGregCal = DatatypeFactory.newInstance()
								.newXMLGregorianCalendar(flightInfo.getScheduledDate());
						
						//ufisCalendar.setTime(flightInfo.getScheduledDate(), ufisCalendar.getCustomFormat());
	            		ufisCalendar.setTime(xmlGregCal);
	                    
	            		// flda in utc
	                    //infobjFlight.setFLUT(ufisCalendar.getCedaDateString());
	            		infobjFlight.setFLUT(DF_CEDADATE.format(ufisCalendar.getTime()));
	                    
	                    // STDT
	                    //infobjgeneric.setSTDT(ufisCalendar.getCedaString());
	            		infobjgeneric.setSTDT(DF_CEDA.format(ufisCalendar.getTime()));
	                    
	                    // flda in local
	                    ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_UTC_LOCAL, EnumTimeInterval.Hours);
	                    //infobjFlight.setFLDA(ufisCalendar.getCedaDateString());
	                    infobjFlight.setFLDA(DF_CEDADATE.format(ufisCalendar.getTime()));
					} catch (DatatypeConfigurationException e) {
						LOG.error("DatatypeConfigurationException: {}", e);
						//LOG.debug("Dropped Message details: \n{}", _flightXml);
		    			addExptInfo(EnumExceptionCodes.EWVAL.name(),
		    					EnumExceptionCodes.EWVAL.toString());
	                    return false;
					}
            	} else {
            		LOG.error("ScheduledDate format not matched with patten={}", DT_LOC_SS);
            		LOG.error("ScheduledDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                    //LOG.debug("Dropped Message details: \n{}", _flightXml);
	    			addExptInfo(EnumExceptionCodes.EWFMT.name(),
	    					EnumExceptionCodes.EWFMT.toString());
                    return false;
            	}
            } else {
                LOG.error("ScheduledDate with wrong format or NULL");
                //LOG.debug("Dropped Message details: \n{}", _flightXml);
    			addExptInfo(EnumExceptionCodes.EWFMT.name(),
    					EnumExceptionCodes.EWFMT.toString());
                return false;
            }
            
            // For aircraft type processing, igore data from aircraftype tag
            // intead of using aircraft type include in regn (handle by Ceda)
            
            // if not EK or QF flight, then drop
            /*if (!"EK".equalsIgnoreCase(alc2) && !"QF".equalsIgnoreCase(alc2)) {
            	LOG.debug("DMIS Flight message has been dropped due to non-EK and non-QF flights");
            	//LOG.debug("Dropped Message details: \n{}", _flightXml);
    			addExptInfo(EnumExceptionCodes.EWALC.name(),
    					"Non EK or QF flight found");
            	return false;
            } else*/ 
            if ("EK".equalsIgnoreCase(alc2)) {
            	// ===========================
            	// EK flights
            	// ===========================
            	
            	// action type
            	_msgActionType = ACTIONTYPE.U;
            	// schedule date
            	//ufisCalendar.setTime(flightInfo.getScheduledDate(), ufisCalendar.getCustomFormat());
            	infobjFlight.setFLNO(null);
            	infobjFlight.setALC2(null);
            	infobjFlight.setFLTN(null);
            	infobjFlight.setFLDA(null);
            	infobjFlight.setFLUT(null);
            	switch (adid) {
                case "A":
                	//infobjFlight.setSTOA(ufisCalendar.getCedaString());
                    //infobjFlight.setADID(ADID.A);
                	infobjgeneric.setADID(ADID.A);
                    if (flightInfo.getBayNumber() != null && flightInfo.getBayNumber().getValue() != null) {
                        infobjFlight.setPSTA(flightInfo.getBayNumber().getValue());
                    }
                    if (flightInfo.getGateNumber() != null && flightInfo.getGateNumber().getValue() != null) {
                        infobjFlight.setGTA1(flightInfo.getGateNumber().getValue());
                    }
                    break;
                case "D":
                	//infobjFlight.setSTOD(ufisCalendar.getCedaString());
                    //infobjFlight.setADID(ADID.D);
                	infobjgeneric.setADID(ADID.D);
                    if (flightInfo.getBayNumber() != null && flightInfo.getBayNumber().getValue() != null) {
                        infobjFlight.setPSTD(flightInfo.getBayNumber().getValue());
                    }
                    if (flightInfo.getGateNumber() != null && flightInfo.getGateNumber().getValue() != null) {
                        infobjFlight.setGTD1(flightInfo.getGateNumber().getValue());
                    }
                    break;
            	}
            	// short final date
            	if (HpUfisUtils.isNotEmptyStr(flightInfo.getShortFinalDate())) {
                    //ufisCalendar.setTime(flightInfo.getShortFinalDate(), ufisCalendar.getCustomFormat());
                    //infobjFlight.setSFIN(ufisCalendar.getCedaString());
            		if (flightInfo.getShortFinalDate().matches(DT_LOC_SS)) {
	            		try {
							xmlGregCal = DatatypeFactory.newInstance()
									.newXMLGregorianCalendar(flightInfo.getShortFinalDate());
							ufisCalendar.setTime(xmlGregCal);
		            		infobjFlight.setSFIN(DF_CEDA.format(ufisCalendar.getTime()));
						} catch (DatatypeConfigurationException e) {
							LOG.error("DatatypeConfigurationException: {}", e.getMessage());
						}
            		} else {
            			LOG.error("ShortFinalDate format not matched with patten={}", DT_LOC_SS);
                		LOG.error("ShortFinalDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
            		}
            	}
            	
            } else {
            	// ===========================
            	// QF flights
            	// ===========================
                if (flightInfo.getParkingTerminal() != null && flightInfo.getParkingTerminal().getValue() != null) {
                    patkingT = flightInfo.getParkingTerminal().getValue().toString();
                }

                if (flightInfo.getDubaiOrigin() != null && flightInfo.getDubaiOrigin().getValue() != null) {
                    dubaiOrigin = flightInfo.getDubaiOrigin().getValue().toString();
                }
                LOG.info("DubaiOrigin {}", dubaiOrigin);

                //Manipulate arrivalStation / DepartureStation
                //Split by /
                viaRoutes = flightInfo.getViaRoutes();
                // 2013-12-27 updated by JGO - adding ROUT(AFTTAB) to store the routes info
                infobjFlight.setROUT(viaRoutes.replaceAll("/", "|"));
                arrivalStation = flightInfo.getArrivalStation();
                departureStation = flightInfo.getDepartureStation();
                
                switch (adid) {
                    case "A":
                    	//infobjFlight.setSTOA(ufisCalendar.getCedaString());
                    	infobjFlight.setSTOA(infobjgeneric.getSTDT());
                        infobjFlight.setADID(ADID.A);
                        infobjFlight.setTRMA(patkingT);
                        infobjFlight.setDES3(HpEKConstants.HOPO);

                        if (flightInfo.getEstimatedDate() != null 
                        		&& HpUfisUtils.isNotEmptyStr(flightInfo.getEstimatedDate().getValue())) {
                            //ufisCalendar.setTime(flightInfo.getEstimatedDate().getValue(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setEIBR(ufisCalendar.getCedaString());
                        	if (flightInfo.getEstimatedDate().getValue().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getEstimatedDate().getValue());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setEIBD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("EstimatedDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("EstimatedDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }

                        if (flightInfo.getLatestDate() != null 
                        		&& HpUfisUtils.isNotEmptyStr(flightInfo.getLatestDate().getValue())) {
                            //ufisCalendar.setTime(flightInfo.getLatestDate().getValue(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setTLDR(ufisCalendar.getCedaString());
                        	if (flightInfo.getLatestDate().getValue().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getLatestDate().getValue());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setTLDD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("LatestDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("LatestDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }
                        
                        /*if (HpUfisUtils.isNotEmptyStr(flightInfo.getActualDate().getValue())) {
                            ufisCalendar.setTime(flightInfo.getActualDate().getValue(), ufisCalendar.getCustomFormat());
                            //ufisCalendar.setTime(flightInfo.getTouchdownAirborneDate().getValue());
                        }*/
                        
                        if (HpUfisUtils.isNotEmptyStr(flightInfo.getTouchdownAirborneDate())) {
                        	//ufisCalendar.setTime(flightInfo.getTouchdownAirborneDate(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setALDR(ufisCalendar.getCedaString());
                        	if (flightInfo.getTouchdownAirborneDate().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getTouchdownAirborneDate());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setALDD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("TouchdownAirborneDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("TouchdownAirborneDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }

                        if (flightInfo.getBayNumber() != null && flightInfo.getBayNumber().getValue() != null) {
                            infobjFlight.setPSTA(flightInfo.getBayNumber().getValue());
                        }
                        if (flightInfo.getGateNumber() != null && flightInfo.getGateNumber().getValue() != null) {
                            infobjFlight.setGTA1(flightInfo.getGateNumber().getValue());
                        }
                        if (flightInfo.getBaggageBeltNumber() != 
                        		null && flightInfo.getBaggageBeltNumber().getValue() != null) {
                            infobjFlight.setBLT1(flightInfo.getBaggageBeltNumber().getValue());
                        }
                        //Set ORG3 base on depStation
                        //Split with /
                        if (viaRoutes != null && viaRoutes.contains("/")) {
                            String[] splitviaRoutes = viaRoutes.split("/");
                            for (int i = 0; i < splitviaRoutes.length; i++) {
                                if (i == 0) {
                                    infobjFlight.setORG3(splitviaRoutes[i]);
                                }
                                if (i == 1) {
                                    //infobjFlight.setVIA3(splitviaRoutes[i]);
                                	if (HpUfisUtils.isNotEmptyStr(splitviaRoutes[i])) {
                                		infobjFlight.setVIAN("1");
                                		infobjFlight.setVIAL(formatVial(splitviaRoutes[i]));
                                	}
                                }
                            }
                        } else {
                            infobjFlight.setORG3(departureStation);
                        }


                        break;
                    case "D":
                    	//infobjFlight.setSTOD(ufisCalendar.getCedaString());
                    	infobjFlight.setSTOD(infobjgeneric.getSTDT());
                        infobjFlight.setADID(ADID.D);
                        infobjFlight.setTRMD(patkingT);
                        infobjFlight.setORG3(HpEKConstants.HOPO);

                        if (flightInfo.getEstimatedDate() != null 
                        		&& HpUfisUtils.isNotEmptyStr(flightInfo.getEstimatedDate().getValue())) {
                            //ufisCalendar.setTime(flightInfo.getEstimatedDate().getValue(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setEOBR(ufisCalendar.getCedaString());
                        	if (flightInfo.getEstimatedDate().getValue().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getEstimatedDate().getValue());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setEOBD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("EstimatedDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("EstimatedDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }

                        if (flightInfo.getLatestDate() != null 
                        		&& HpUfisUtils.isNotEmptyStr(flightInfo.getLatestDate().getValue())) {
                            //ufisCalendar.setTime(flightInfo.getLatestDate().getValue(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setTLDR(ufisCalendar.getCedaString());
                        	if (flightInfo.getLatestDate().getValue().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getLatestDate().getValue());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setTLDD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("LatestDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("LatestDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }

                        /*if (HpUfisUtils.isNotEmptyStr(flightInfo.getActualDate().getValue())) {
                            ufisCalendar.setTime(flightInfo.getActualDate().getValue(), ufisCalendar.getCustomFormat());
                            //ufisCalendar.setTime(flightInfo.getTouchdownAirborneDate().getValue());
                        }*/
                        
                        if (HpUfisUtils.isNotEmptyStr(flightInfo.getTouchdownAirborneDate())) {
                        	//ufisCalendar.setTime(flightInfo.getTouchdownAirborneDate(), ufisCalendar.getCustomFormat());
                            //infobjFlight.setATOR(ufisCalendar.getCedaString());
                        	if (flightInfo.getTouchdownAirborneDate().matches(DT_LOC_SS)) {
                        		try {
                        			xmlGregCal = DatatypeFactory.newInstance()
                        					.newXMLGregorianCalendar(flightInfo.getTouchdownAirborneDate());
                        			ufisCalendar.setTime(xmlGregCal);
                        			infobjFlight.setATOD(DF_CEDA.format(ufisCalendar.getTime()));
                        		} catch (DatatypeConfigurationException e) {
                        			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                        		}
                        	} else {
                        		LOG.error("TouchdownAirborneDate format not matched with patten={}", DT_LOC_SS);
                        		LOG.error("TouchdownAirborneDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                        	}
                        }

                        if (flightInfo.getBayNumber() != null && flightInfo.getBayNumber().getValue() != null) {
                            infobjFlight.setPSTD(flightInfo.getBayNumber().getValue());
                        }
                        if (flightInfo.getGateNumber() != null && flightInfo.getGateNumber().getValue() != null) {
                            infobjFlight.setGTD1(flightInfo.getGateNumber().getValue());
                        }

                        //Set DES3 base on arrivalStation
                        if (viaRoutes != null && viaRoutes.contains("/")) {
                            String[] splitviaRoutes = viaRoutes.split("/");
                            for (int i = 0; i < splitviaRoutes.length; i++) {
                                if (i == 0) {
                                    //infobjFlight.setVIA3(splitviaRoutes[i]);
                                	if (HpUfisUtils.isNotEmptyStr(splitviaRoutes[i])) {
                                		infobjFlight.setVIAN("1");
                                    	infobjFlight.setVIAL(formatVial(splitviaRoutes[i]));
                                	}
                                }
                                if (i == splitviaRoutes.length - 1) {
                                    infobjFlight.setDES3(splitviaRoutes[i]);
                                }
                            }
                        } else {
                            infobjFlight.setDES3(arrivalStation);
                        }
                        break;
                }
                //Aircraft Type Code Eg. A330200
                //Need to format and break into 2 fields
                if (flightInfo.getAircraftType() != null 
                		&& flightInfo.getAircraftType().getValue() != null) {
                    infobjFlight.setACT3(flightInfo.getAircraftType().getValue().substring(0, 3));
                    infobjFlight.setACT5(flightInfo.getAircraftType().getValue().substring(3));
                }
                if (flightInfo.getRegistrationNumber() != null 
                		&& flightInfo.getRegistrationNumber().getValue() != null) {
                    infobjFlight.setREGN(flightInfo.getRegistrationNumber().getValue());
                }
                //Airline
                if (flightInfo.getAirlineCode() != null 
                		&& flightInfo.getAirlineCode().getValue() != null) {
                    if (flightInfo.getAirlineCode().getValue().length() == 2) {
                        infobjFlight.setALC2(flightInfo.getAirlineCode().getValue());
                    } else if (flightInfo.getAirlineCode().getValue().length() == 3) {
                        infobjFlight.setALC3(flightInfo.getAirlineCode().getValue());
                    }
                }
                //Handling Terminal
                if (flightInfo.getHandlingTerminal() != null 
                		&& flightInfo.getHandlingTerminal().getValue() != null) {
                	String stev = flightInfo.getHandlingTerminal().getValue().toString();
                	Matcher m = p.matcher(stev);
                	stev = m.replaceAll("");
                    infobjFlight.setSTEV(stev);
                }
                //Code Share fixed len
                //A3 1808  LG 1561  SK 3215  TP 7962  UA 9524
                StringBuilder codeShareBuilder = new StringBuilder();
                int codeShareCount = 0;
                boolean codeShareEnable = false;
                if (codeShareEnable == true) {
                    if (flightInfo.getCodeshareFlight1() != null 
                    		&& flightInfo.getCodeshareFlight1().getValue() != null
                            && flightInfo.getCodeshareFlight1().getValue().length() > 1) {
                        codeShareBuilder.append(String.format("%-" + CODESHARESPACE + "s", flightInfo.getCodeshareFlight1().getValue()));
                        codeShareCount++;
                    }
                    if (flightInfo.getCodeshareFlight2() != null
                            && flightInfo.getCodeshareFlight2().length() > 1) {
                        codeShareBuilder.append(String.format("%-" + CODESHARESPACE + "s", flightInfo.getCodeshareFlight2()));
                        codeShareCount++;
                    }
                    if (flightInfo.getCodeshareFlight3() != null
                            && flightInfo.getCodeshareFlight3().length() > 1) {
                        codeShareBuilder.append(String.format("%-" + CODESHARESPACE + "s", flightInfo.getCodeshareFlight3()));
                        codeShareCount++;
                    }
                    if (flightInfo.getCodeshareFlight4() != null
                            && flightInfo.getCodeshareFlight4().length() > 1) {
                        codeShareBuilder.append(String.format("%-" + CODESHARESPACE + "s", flightInfo.getCodeshareFlight4()));
                        codeShareCount++;
                    }
                    if (flightInfo.getCodeshareFlight5() != null
                            && flightInfo.getCodeshareFlight5().length() > 1) {
                        codeShareBuilder.append(String.format("%-" + CODESHARESPACE + "s", flightInfo.getCodeshareFlight5()));
                        codeShareCount++;
                    }
                    if (codeShareCount > 0) {
                        infobjFlight.setJFNO(codeShareBuilder.toString());
                        infobjFlight.setJCNT(Integer.toString(codeShareCount));
                    }
                }
                // short final date
                if (HpUfisUtils.isNotEmptyStr(flightInfo.getShortFinalDate())) {
                	//ufisCalendar.setTime(flightInfo.getShortFinalDate(), ufisCalendar.getCustomFormat());
                	//infobjFlight.setSFIN(ufisCalendar.getCedaString());
                	if (flightInfo.getShortFinalDate().matches(DT_LOC_SS)) {
                		try {
                			xmlGregCal = DatatypeFactory.newInstance()
                					.newXMLGregorianCalendar(flightInfo.getShortFinalDate());
                			ufisCalendar.setTime(xmlGregCal);
                			infobjFlight.setSFIN(DF_CEDA.format(ufisCalendar.getTime()));
                		} catch (DatatypeConfigurationException e) {
                			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                		}
                	} else {
                		LOG.error("ShortFinalDate format not matched with patten={}", DT_LOC_SS);
                		LOG.error("ShortFinalDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                	}
                	
                }
                // choksOn
                if (HpUfisUtils.isNotEmptyStr(flightInfo.getChoksOnDate())) {
                	if (flightInfo.getChoksOnDate().matches(DT_LOC_SS)) {
                		try {
                			xmlGregCal = DatatypeFactory.newInstance()
                					.newXMLGregorianCalendar(flightInfo.getChoksOnDate());
                			ufisCalendar.setTime(xmlGregCal);
                			infobjFlight.setAIBD(DF_CEDA.format(ufisCalendar.getTime()));
                		} catch (DatatypeConfigurationException e) {
                			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                		}
                	} else {
                		LOG.error("ChoksOnDate format not matched with patten={}", DT_LOC_SS);
                		LOG.error("ChoksOnDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                	}
                }
                // choksOff
                if (HpUfisUtils.isNotEmptyStr(flightInfo.getChoksOffDate())) {
                	if (flightInfo.getChoksOffDate().matches(DT_LOC_SS)) {
                		try {
                			xmlGregCal = DatatypeFactory.newInstance()
                					.newXMLGregorianCalendar(flightInfo.getChoksOffDate());
                			ufisCalendar.setTime(xmlGregCal);
                			infobjFlight.setAOBD(DF_CEDA.format(ufisCalendar.getTime()));
                		} catch (DatatypeConfigurationException e) {
                			LOG.error("DatatypeConfigurationException: {}", e.getMessage());
                		}
                	} else {
                		LOG.error("ChoksOnDate format not matched with patten={}", DT_LOC_SS);
                		LOG.error("ChoksOnDate correct format should like: yyyy-MM-ddTHH:mm:ss[+-]hh:mm");
                	}
                }
                // uld charge and airline charge code -> XAFTAB
                if (flightInfo.getUldCharges() != null && flightInfo.getUldCharges().getValue() != null) {
                    infojxaftab.setULDC(flightInfo.getUldCharges().getValue());
                    isXafFound = true;
                }
                if (flightInfo.getChargedAirlineCode() != null 
                		&& flightInfo.getChargedAirlineCode().getValue() != null) {
                    infojxaftab.setCHAL(flightInfo.getChargedAirlineCode().getValue());
                    isXafFound = true;
                }
                if (flightInfo.getFlightCombiType() != null 
                		&& flightInfo.getFlightCombiType().getValue() != null) {
                    String flightCombiType = flightInfo.getFlightCombiType().getValue();
                    switch (flightCombiType) {
                        case "P":
                            infobjFlight.setSTYP("G");
                            break;
                        case "C":
                            infobjFlight.setSTYP("F");
                            break;
                        case "M":
                            infobjFlight.setSTYP("Q");
                            break;
                    }
                }

                if (flightInfo.getFlightStatus() != null 
                		&& flightInfo.getFlightStatus().getValue() != null) {
                    String flightStatus = flightInfo.getFlightStatus().getValue();
                    // put org flightStatus to rem1(new change to ISTA)
                    // infobjFlight.setREM1(flightStatus);
                    infobjFlight.setISTA(flightStatus);
    				switch (flightStatus) {
    				case "X":
    					// Flight Deleted ?
    					//_msgActionType = ACTIONTYPE.D;
    					infobjFlight.setFTYP("X");
    					break;
    				case "C":
    					// Flight CXX
    					infobjFlight.setFTYP("X");
    					break;
    				// case "M":
    				// case "A":
    				// //Flight Diverted
    				// infobjFlight.setFTYP("D");
    				// break;
    				// case "R":
    				// infobjFlight.setFTYP("Z");
    				// infobjFlight.setADID(ADID.B);
    				// break;
    				default:
    					infobjFlight.setFTYP("O");
    					break;
    				}
                }
                
                if (flightInfo.getFlightRemarks() != null) {
                    //infobjFlight.setREM2(flightInfo.getFlightRemarks());
                	infobjFlight.setRRMK(flightInfo.getFlightRemarks());
                }
            }
        }
        MSGOBJECTS msgobjects = new MSGOBJECTS();
        /* Set the fields that uniquely identify a flight
         * the rest are done in the UfisMarshal
         */
        msgobjects.setINFOBJFLIGHT(infobjFlight);
        if (isXafFound) {
        	msgobjects.setINFOJXAFTAB(infojxaftab);
        }
        UfisMarshal ufisMarshal = new UfisMarshal(_ms, _msgActionType, MSGIF, null);
        _returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);

        return true;
    }
    
    private String formatVial(String station) {
    	StringBuilder vial = new StringBuilder();
    	vial.append(String.format("%-" + 1 + "s", ""));
    	if (station == null) {
    		station = "";
    	}
    	vial.append(String.format("%-" + 119 + "s", station));
    	return vial.toString();
    }

	public String getReturnXML() {
		return _returnXML;
	}

	public void setReturnXML(String _returnXML) {
		this._returnXML = _returnXML;
	}
    
}
