package com.ufis_as.ufisapp.ek.intf.egds;

import java.io.StringReader;
import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Tuple;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.configuration.HpEgdsConfig;
import com.ufis_as.ek_if.egds.entities.EntDbFlightFuelStatus;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJFEVTAB;
import com.ufis_as.exco.INFOJFEVTABLIST;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlFltFuelStatusBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.egds.EGDSMSG;
import ek.egds.MSGTYPE;

/**
 * BlHandleEgdsBean
 * 
 * @author JGO
 * @version 1.00: 2013-04-02 GFO - version
 * @version 1.01: 2013-06-05 JGO - Change handler to EJB Bean
 * @version 1.02: 2013-06-07 JGO - Set ADID
 * @version 1.03: 2013-06-11 JGO - Put HOPO to Constant
 * @version 1.04: 2013-06-20 BTR
 * @version 1.05: 2013-06-21 JGO - Reuse constant
 * 								   Update flt_fuel_status reated info (due to column type changed)
 * @version 1.06: 2013-06-25 JGO - Adding a criteria query for edgs flight search
 * 								   Reuse Constant
 *  							   Use urno as key for flight update (infobjgeneric)
 *  							   Only pass changed field to infobjflight
 * @version 1.07: 2013-06-27 JGO - Update logical for process different type of messages
 * @version 1.08: 2013-07-03 JGO - Changes for message field atrribute (carrier, flight number, departure 
 * 								   station and arrival station are not mandatory any more) 
 * @version 1.09: 2013-07-06 JGO - Reading afttab flight search time range from config
 * @version 1.10: 2013-08-06 JGO - Onboard Fuel change notification to LIDO
 *                                 Store onboard_fuel into flt_fuel_status
 * @version 1.11: 2013-08-07 JGO - Notification message command(when new created: IRT, otherwise: URT)
 * 								   Write to log file
 * @version 1.12: 2013-09-26 JGO - Change notifyHeader from static to non-static
 * 								   Adding old_value to notification message
 * 								   Clear id_flight list
 * 								   Adding message process timing for query and process part
 * @version 1.13: 2013-09-30 JGO - Change TopicProducer to persist and with alive time
 * @version 1.14: 2013-10-10 JGO - Adding where clause to SEL field of the notification message
 * 								   Change producer to BlUfisBCTopic(previously: BlUfisBroadcastMessage)
 * 								   Adding error/warning notification message
 * @version 1.15: 2013-10-20 JGO - Change message notification to common method
 * 								   Adding notification for FUEL message as well
 * @version 1.16: 2013-10-22 JGO - If notify topic not defined, then send to queue
 * @version 1.17: 2013-10-23 JGO - Remove message content logging when error encounter(log raw message aldy at received time)
 * @version 1.18: 2013-11-22 JGO - Upgrade the jackson lib from 1.9.7 -> 2.2.3(need to re-import used function)
 * 								   Save LADT for load plan acceptance message in XAFTAB
 * @version 1.19: 2013-11-26 JGO - Remove static modifier from BaseDTO
 * @version 1.20: 2013-12-02 JGO - Remove the mandatory check for ETA of Progress Report message
 * 
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlHandleEgdsBean extends BaseDTO {
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BlHandleEgdsBean.class);
	
	/**
	 * CDI
	 */
	@EJB
	private IAfttabBean aftBean;
    @EJB
    private IAfttabBeanLocal afttabBean;
    @EJB
    private DlFltFuelStatusBean fltFuelStatusBean;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
    
    /**
	 * Constants and Variables
	 */
    private static final String HOPO = "DXB";
    private static final String MSGIF = "EK-EDGS";
    private static final String TAB = "FLT_FUEL_STATUS";
    private static final String NOTIFY_FOB = "ONBOARD_FUEL";
    private ACTIONTYPE _msgActionType;
    private String _flightXml = "";
    private String _returnXML = "";
    private EGDSMSG egdsData = null;

//    private JAXBContext _cnxJaxb;
//    private JAXBContext _cnxJaxbM;
//    private Unmarshaller _um;
//    private Marshaller _ms;
    
    private static final String TIME_PATTERN = "([01]?[0-9]|2[0-3]):?[0-5][0-9]";
    private static final String TIME_FORMATTER = "%1$04d";
    private static final String SYMB_COLON = ":";
    
    private static ObjectMapper mapper = new ObjectMapper();
    private EntUfisMsgHead notifyHeader;
    
    /**
     * Default constructor. 
     */
    public BlHandleEgdsBean() {
		try {
            _cnxJaxb = JAXBContext.newInstance(EGDSMSG.class);
            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
            _um = _cnxJaxb.createUnmarshaller();
            _ms = _cnxJaxbM.createMarshaller();
            
            // notification message header
            if (notifyHeader == null) {
            	notifyHeader = new EntUfisMsgHead();
            	notifyHeader.setHop(HOPO);
            	//notifyHeader.setReqt(reqt);
            	notifyHeader.setOrig(MSGIF);
            	notifyHeader.setApp(MSGIF);
            	notifyHeader.setUsr(MSGIF);
            }
            
        	tableRef = HpUfisAppConstants.CON_IRMTAB;
        	exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
        	dtfl = com.ufis_as.ufisapp.configuration.HpEKConstants.EK_EGDS_SOURCE;
        	
        } catch (JAXBException ex) {
            LOG.error("JAXBException when initialize JaxbContext, marshaller and unmarshaller");
        }
    }

	public boolean unMarshal(String message, long irmtabRef) {
		try {
	    	data.clear();
	    	irmtabUrno = irmtabRef;
	    	
			_msgActionType = ACTIONTYPE.U;
			_flightXml = message;
			egdsData = (EGDSMSG) _um.unmarshal(new StreamSource(new StringReader(message)));
			if (readEvent(egdsData)) {
				LOG.debug("EGDS FNR: {}", egdsData.getEGDSBODY().getFNR());
				return true;
			} else {
				return false;
			}
		} catch (JAXBException ex) {
			LOG.error("Flight Event JAXB exception {}", ex.toString());
			//LOG.error("Message: \n{}", message);
			return false;
		}
	}

	private boolean readEvent(EGDSMSG message) {
		if (message != null) {
			return tranformFlight(message);
		} else {
			LOG.warn("Flight Event Null detected");
			return false;
		}
	}

	private boolean tranformFlight(EGDSMSG message) {
		boolean isTransferred = false;
		try {
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.setCustomFormat(HpEKConstants.EDGS_TIME_FORMAT);
	
			INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
			MSGOBJECTS msgobjects = new MSGOBJECTS();
			INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
			INFOJXAFTAB infojxaftab = new INFOJXAFTAB();
			//INFOBJFUELSTATUS infofuelstatus = new INFOBJFUELSTATUS();
			INFOJFEVTABLIST	infojfevtablist = new INFOJFEVTABLIST();
			Boolean xafFound = false;
			Boolean fevFound = false;
			
			boolean isValid = true;
			// Header and body are mandatory
			if (message.getEGDSHEADER() == null || message.getEGDSBODY() == null) {
				LOG.error("EGDS header and body are mandatory, cannot be null");
				//LOG.error("Message Dropped: \n{}", _flightXml);
				isValid = false;
			}
			
			// if non-EK flight, drop
    		if (!HpEKConstants.EK_ALC3.equals(message.getEGDSBODY().getCRR()) 
    				&& !HpEKConstants.EK_ALC2.equals(message.getEGDSBODY().getCRR())) {
    			LOG.debug("EGDS message dropped due to it's non-EK flight message");
    			//LOG.error("Message Dropped: \n{}", _flightXml);
    			isValid = false;
    		}
    		
    		// timestamp
    		if (HpUfisUtils.isNullOrEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
    			LOG.debug("EGDS message dropped due to timestamp(header) is null or empty");
    			//LOG.error("Message Dropped: \n{}", _flightXml);
    			isValid = false;
    		}
    		
    		// egds message type
    		MSGTYPE type = message.getEGDSBODY().getTYPE();
    		if (type == null) {
    			LOG.debug("EGDS message dropped due to type field is null");
    			//LOG.error("Message Dropped: \n{}", _flightXml);
    			isValid = false;
    		}
    		
    		// flight number and registration number
    		String fltn = HpUfisUtils.formatCedaFltn(message.getEGDSBODY().getFNR());
    		String regn = message.getEGDSBODY().getACR().replaceAll("-", "");
    		// regn should be less than 6 digits
    		if (HpUfisUtils.isNullOrEmptyStr(regn) || regn.length() > 6) {
    			LOG.debug("EGDS message dropped due to regn is either empty(or null), or length greater than 6 digits");
    			//LOG.error("Message Dropped: \n{}", _flightXml);
    			isValid = false;
    		}
    		
    		if (!isValid) {
    			addExptInfo(EnumExceptionCodes.EMAND.name(), EnumExceptionCodes.EMAND.toString());
    			return false;
    		}
    		
    		// org3
    		String org3 = message.getEGDSBODY().getDEP();
    		
    		// des3
    		String des3 = message.getEGDSBODY().getARR();
    		
    		// adid
    		ADID adid = null;
    		if (HOPO.equals(message.getEGDSBODY().getDEP()) && HOPO.equals(message.getEGDSBODY().getARR())) {
    			adid = ADID.B;
			} else if (HOPO.equals(message.getEGDSBODY().getDEP())) {
				adid = ADID.D;
			} else if (HOPO.equals(message.getEGDSBODY().getARR())) {
				adid = ADID.A;
			} 
			
    		/*
    		 * 2013-07-02 updated by JGO - departure and arrival station changed to non-mandatory in ICD
    		 * else {
				if (MSGTYPE.ROUTE != type) {
					LOG.debug("EGDS message dropped due to cannot identify flight adid by ARR tag and DEP tag");
					LOG.error("Message Dropped: \n{}", _flightXml);
	        		return false;
				}
			}
			*/
    		
    		// According to type, build afttab flight search criteria and parameters
    		EntDbAfttab criteriaParams = new EntDbAfttab();
    		criteriaParams.setFltn(fltn);
    		criteriaParams.setRegn(regn);
    		
    		// get the configurable time range offset
    		int from = 0;
    		int to = 0;
    		if (MSGTYPE.MVT == type) {
    			from = HpEgdsConfig.getEgdsMvtFromOffset();
    			to = HpEgdsConfig.getEgdsMvtToOffset();
    		} else {
    			from = HpEgdsConfig.getEgdsDefFromOffset();
    			to = HpEgdsConfig.getEgdsDefToOffset();
    		}
    		
    		// find flight from afttab
			//BigDecimal uaft = aftBean.getUrnoByCriteriaQuery(criteriaParams, type, adid); 
    		long start = System.currentTimeMillis();
			Tuple tuple = aftBean.getUrnoByCriteriaQuery(criteriaParams, type, adid, from, to);
			LOG.debug("EGDS search flight from afttab cost: {}ms", (System.currentTimeMillis() - start));
			if (tuple == null) {
    			// drop message
    			LOG.debug("EGDS message dropped due to cannot find flight from afttab");
    			//LOG.error("Message Dropped: \n{}", _flightXml);
    			addExptInfo(EnumExceptionCodes.ENOFL.name(), EnumExceptionCodes.ENOFL.toString());
        		return false;
        	}
        	
        	// uaft
        	BigDecimal uaft = tuple.get("urno", BigDecimal.class);
        	
        	// tifa
        	String tifa = tuple.get("tifa", String.class);
        	
        	// tifd
        	String tifd = tuple.get("tifd", String.class);
        	
        	// adid
        	Character aftAdid = tuple.get("adid", Character.class);
        	if (adid == null && aftAdid != null) {
        		adid = ADID.fromValue(aftAdid.toString());
        	}
        	
        	// flag: continue to process message or not
        	boolean isContinue = true;
        	
        	// flt fuel info
        	EntDbFlightFuelStatus oldData = null;
        	EntDbFlightFuelStatus data = null;
        	
        	start = System.currentTimeMillis();
        	// according to type settle down return message
        	UfisASCommands cmd = UfisASCommands.URT;
			switch (type) {
			case MVT:
				/*  
				 * OUT -> AOBN
				 * OFF -> ATON
				 * ON  -> ALDN
				 * IN  -> AIBN
				 */
				String trigevt = message.getEGDSBODY().getTRIGEVT();
				String ata = message.getEGDSBODY().getATA();
				if (HpUfisUtils.isNotEmptyStr(trigevt) && ata != null) {
					if (ata.matches(TIME_PATTERN)) {
						
						// HHmm or HH:mm -> HHMM
						ata = String.format(TIME_FORMATTER, Integer.parseInt(ata.replaceAll(SYMB_COLON, "")));
						if (ADID.A == adid) {
							ata = HpUfisUtils.guessTheDate(tifa, ata);
						} else {
							ata = HpUfisUtils.guessTheDate(tifd, ata);
						}
						
						if (trigevt.contains("OUT")) {
							infobjFlight.setAOBN(ata);
						} else if (trigevt.contains("OFF")) {
							infobjFlight.setATON(ata);
						} else if (trigevt.contains("ON")) {
							infobjFlight.setALDN(ata);
						} else if (trigevt.contains("IN")) {
							infobjFlight.setAIBN(ata);
						}
					} else {
						LOG.debug("EGDS: ATA<{}> format is not match with HH:MM or HHMM", ata);
					}
					
					// onboard fuel
					//if (trigevt.contains("IN") && (message.getEGDSBODY().getFOB() != null)) {
					if (message.getEGDSBODY().getFOB() != null) {
						infojxaftab.setFUOB(message.getEGDSBODY().getFOB());
						xafFound = true;
					}
				} else {
					LOG.debug("EGDS: cannot process ATA due to TRIGEVT<{}> or ATA<{}> is null or empty", trigevt, ata);
				}
				
				// Cargo Door closed and Cabin Door closed 
				if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getCARD())) {
					ufisCalendar.setTime(message.getEGDSBODY().getCARD(), ufisCalendar.getCustomFormat());
					INFOJFEVTAB cargoDoor = new INFOJFEVTAB();
					cargoDoor.setSTNM("CARGO DOOR CLOSED");
					cargoDoor.setSTTM(ufisCalendar.getCedaString());
					infojfevtablist.getINFOJFEVTAB().add(cargoDoor);
					fevFound = true;
				}
				if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getCABD())) {
					ufisCalendar.setTime(message.getEGDSBODY().getCABD(), ufisCalendar.getCustomFormat());
					INFOJFEVTAB cabinDoor = new INFOJFEVTAB();
					cabinDoor.setSTNM("CABIN DOOR CLOSED");
					cabinDoor.setSTTM(ufisCalendar.getCedaString());
					infojfevtablist.getINFOJFEVTAB().add(cabinDoor);
					fevFound = true;
				}
				
				// if contains FOB
				if (xafFound) {
					try {
						oldData = fltFuelStatusBean.findByIdFlight(uaft);
						if (oldData == null) {
							// Prepare notification data (dat)
							data = new EntDbFlightFuelStatus();
							// id flight
							data.setIdFlight(uaft);
							// flight number
							data.setFlightNumber(fltn);
							// flight date
							//fuelStatus.setFltDate(aftFlight.getFlda());
							// regn
							data.setFltRegn(regn);
							// alc2
							data.setAirlineCode2(HpEKConstants.EK_ALC2);
							// org3
							data.setFltOrigin3(org3);
							// dep3
							data.setFltDest3(des3);
							data.setCreatedUser(MSGIF);
							data.setDataSource(MSGIF);
							// msg send date
							if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
								ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
								data.setMsgSendDate(ufisCalendar.getTime());
							}
							fltFuelStatusBean.persist(data);
							LOG.info("EGDS MVT: created flight fuel record={} for flight={}", data.getId(), uaft);
							cmd = UfisASCommands.IRT;
						}

						
						if (UfisASCommands.URT == cmd) {
							data = EntDbFlightFuelStatus.valueOf(oldData);
							String fob = message.getEGDSBODY().getFOB().isEmpty() ? 
									"0" : message.getEGDSBODY().getFOB();
							float onBoardFuel = Float.parseFloat(fob);
							// new value
							data.setOnboardFuel(onBoardFuel);

							// msg send date
							if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
								ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(),
										ufisCalendar.getCustomFormat());
								data.setMsgSendDate(ufisCalendar.getTime());
							}
							data.setRecStatus(' ');
							data.setUpdatedUser(MSGIF);
							fltFuelStatusBean.update(data);
							LOG.info("EGDS MVT: updated flight fuel record={} for flight={}", data.getId(), uaft);
						}
						
						if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
							// send notification message to topic
							ufisTopicProducer.sendNotification(true, cmd, String.valueOf(uaft), oldData, data);
						} else {
							// if topic not defined, send notification to queue
							ufisQueueProducer.sendNotification(true, cmd, String.valueOf(uaft), oldData, data);
						}
						
						// onboard fuel
						/*Float oldOnBoardFuel = 0f;
						if (message.getEGDSBODY().getFOB() != null) {
							String fob = message.getEGDSBODY().getFOB().isEmpty() ? 
									"0" : message.getEGDSBODY().getFOB();
							float onBoardFuel = Float.parseFloat(fob);
							// if fuel changed, update and notify
							if (onBoardFuel != fuelStatus.getOnboardFuel()) {
								// old value
								oldOnBoardFuel = fuelStatus.getOnboardFuel();
								// new value
								fuelStatus.setOnboardFuel(onBoardFuel);

								// msg send date
								if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
									ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(),
											ufisCalendar.getCustomFormat());
									fuelStatus.setMsgSendDate(ufisCalendar.getTime());
								}
								fuelStatus.setRecStatus(' ');
								fuelStatus.setUpdatedUser(MSGIF);
								fltFuelStatusBean.update(fuelStatus);

								// notify flag
								isNofified = true;
							}
						}*/
						
						// notify change or not
						/*if (isNofified) {
							EntUfisMsgDTO msgObj = new EntUfisMsgDTO();
							// Header: id flight
							notifyHeader.getIdFlight().clear();
							notifyHeader.getIdFlight().add(String.valueOf(uaft));
							notifyHeader.setReqt(HpUfisCalendar.getCurrentUTCTimeString());
							msgObj.setHead(notifyHeader);
							
							// Body
							EntUfisMsgBody notifyBody = new EntUfisMsgBody();
							EntUfisMsgACT act = new EntUfisMsgACT();
							if (isInsert) {
								act.setCmd(HpUfisAppConstants.UfisASCommands.IRT.toString());
							} else {
								act.setCmd(HpUfisAppConstants.UfisASCommands.URT.toString());
							}
							act.setTab(TAB);
							act.getFld().add(NOTIFY_FOB);
							act.getData().add(message.getEGDSBODY().getFOB());
							act.getOdat().add(oldOnBoardFuel.toString());
							act.getId().add(fuelStatus.getId());
							act.setSel("WHERE id = " + "\"" + fuelStatus.getId() + "\"");
							notifyBody.getActs().add(act);
							msgObj.setBody(notifyBody);
							
							// json message
							String jsonNotifyMsg = mapper.writeValueAsString(msgObj);
							//bcMessaging.sendBroadcastMessage(jsonNotifyMsg);
							ufisTopicProducer.sendMessage(jsonNotifyMsg);
							LOG.debug("EGDS notification for changes: \n{}", jsonNotifyMsg);
						}*/
					} catch (Exception e) {
						LOG.error("Cannot transform egds message: {}", e.getMessage());
						//LOG.error("Message Dropped: \n{}", _flightXml);
					}
				}
				break;
				
			case PROGRESSREPORT:
				String eta = message.getEGDSBODY().getETA();
				if (HpUfisUtils.isNullOrEmptyStr(eta)) {
					// 2013-12-02 updated by JGO - ETA is optional in latest ICD
					// drop message
					/*LOG.debug("EGDS message dropped due to eta is null or empty");
	    			//LOG.error("Message Dropped: \n{}", _flightXml);
	    			addExptInfo(EnumExceptionCodes.EMAND.name(), EnumExceptionCodes.EMAND.toString());*/
	    			isContinue = false;
				}

				if (eta.matches(TIME_PATTERN)) {
					eta = String.format(TIME_FORMATTER, Integer.parseInt(eta.replaceAll(SYMB_COLON, "")));
					eta = HpUfisUtils.guessTheDate(tifa, eta);
					infobjFlight.setEIBN(eta);
				} else {
					LOG.debug("EGDS: ETA<{}> format is not match with HH:MM or HHMM", eta);
					addExptInfo(EnumExceptionCodes.EWVAL.name(), "ETA format not match with HH:MM or HHMM");
					isContinue = false;
				}
				break;
				
			case LOADACC:
				// aft
				//infobjFlight.setELDN(message.getEGDSBODY().getLOADSHEET());
				// xaf
				infojxaftab.setLSHT(message.getEGDSBODY().getLOADSHEET());
				infojxaftab.setZFWT(message.getEGDSBODY().getZFW());
				infojxaftab.setTOWT(message.getEGDSBODY().getTOW());
				// 2013-11-22 updated by JGO - Save LADT
				if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
					ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
					infojxaftab.setLADT(ufisCalendar.getCedaString());
				}
				xafFound = true;
				break;
				
			case ROUTE:
				// confirm message from pilot that crew is on board
				//infobjgeneric.setALC2("EK");
				if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
					ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
					INFOJFEVTAB route = new INFOJFEVTAB();
					route.setSTNM("Route Request");
					route.setSTRM(regn);
					route.setSTTM(ufisCalendar.getCedaString());
					infojfevtablist.getINFOJFEVTAB().add(route);
					fevFound = true;
				}
				break;
				
			case FUEL:
				oldData = fltFuelStatusBean.findByIdFlight(uaft);
				if (oldData == null) {
					data = new EntDbFlightFuelStatus();
					// msg send date
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
						ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
						data.setMsgSendDate(ufisCalendar.getTime());
					}
					// id flight
					data.setIdFlight(uaft);
					// flight number
					data.setFlightNumber(fltn);
					// flight date
					//fuelStatus.setFltDate(aftFlight.getFlda());
					// regn
					data.setFltRegn(regn);
					// alc2
					data.setAirlineCode2(HpEKConstants.EK_ALC2);
					// org3
					data.setFltOrigin3(org3);
					// dep3
					data.setFltDest3(des3);
					
					// trip fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getTripFuel())) {
						data.setTripFuel(Float.parseFloat(message.getEGDSBODY().getTripFuel()));
					}
					// taxi fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getTaxiFuel())) {
						data.setTaxiFuel(Float.parseFloat(message.getEGDSBODY().getTaxiFuel()));
					}
					// ramp fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getRampFuel())) {
						data.setRampFuel(Float.parseFloat(message.getEGDSBODY().getRampFuel()));
					}
//					// onboard fuel
//					//if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getFOB())) {
//					if (message.getEGDSBODY().getFOB() != null) {
//						//fuelStatus.setOnboardFuel(Float.parseFloat(message.getEGDSBODY().getFOB()));
//						String fob = message.getEGDSBODY().getFOB().isEmpty() ? "0"
//								: message.getEGDSBODY().getFOB();
//						float onBoardFuel = Float.parseFloat(fob);
//						if (onBoardFuel != fuelStatus.getOnboardFuel()) {
//							fuelStatus.setOnboardFuel(onBoardFuel);
//							isNofified = true;
//						}
//					}
					data.setTrm(message.getEGDSBODY().getTRM());
					data.setDen(message.getEGDSBODY().getDEN());
					data.setRtw(message.getEGDSBODY().getRTW());
					data.setCreatedUser(MSGIF);
					data.setDataSource(MSGIF);
					fltFuelStatusBean.persist(data);
					LOG.info("EGDS FUEL: created flight fuel record={} for flight={}", data.getId(), uaft);
					cmd = UfisASCommands.IRT;
				}
				
				if (UfisASCommands.URT == cmd) {
					data = EntDbFlightFuelStatus.valueOf(oldData);
					// trip fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getTripFuel())) {
						data.setTripFuel(Float.parseFloat(message.getEGDSBODY().getTripFuel()));
					}
					// taxi fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getTaxiFuel())) {
						data.setTaxiFuel(Float.parseFloat(message.getEGDSBODY().getTaxiFuel()));
					}
					// ramp fuel
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSBODY().getRampFuel())) {
						data.setRampFuel(Float.parseFloat(message.getEGDSBODY().getRampFuel()));
					}
					data.setTrm(message.getEGDSBODY().getTRM());
					data.setDen(message.getEGDSBODY().getDEN());
					data.setRtw(message.getEGDSBODY().getRTW());

					// msg send date
					if (HpUfisUtils.isNotEmptyStr(message.getEGDSHEADER().getTIMESTAMP())) {
						ufisCalendar.setTime(message.getEGDSHEADER().getTIMESTAMP(),
								ufisCalendar.getCustomFormat());
						data.setMsgSendDate(ufisCalendar.getTime());
					}
					data.setRecStatus(' ');
					data.setUpdatedUser(MSGIF);
					fltFuelStatusBean.update(data);
					LOG.info("EGDS FUEL: updated flight fuel record={} for flight={}", data.getId(), uaft);
				}
				
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
					// send notification message to topic
					ufisTopicProducer.sendNotification(true, cmd, String.valueOf(uaft), oldData, data);
				} else {
					// if topic not defined, send notification to queue
					ufisQueueProducer.sendNotification(true, cmd, String.valueOf(uaft), oldData, data);
				}
				
				// fuel message handled by Java adapter no need to pass back to ceda
				isContinue = false;
				break;
			}
			LOG.debug("EGDS message process base on TYPE cost: {}ms", (System.currentTimeMillis() - start));

			// check whether need to pass it back to ceda or not
			if (!isContinue) {
				return false;
			}
	
			/*
			 * Set the fields that uniquely identify a flight the rest are done in
			 * the UfisMarshal
			 */
			//infobjgeneric.setDEPN(infobjFlight.getDEPN());
			//infobjgeneric.setREGN(regn);
			infobjgeneric.setURNO(String.valueOf(uaft));
			msgobjects.setINFOBJFLIGHT(infobjFlight);
			if (xafFound) {
				msgobjects.setINFOJXAFTAB(infojxaftab);
			}
			if (fevFound) {
				//msgobjects.setINFOJFEVTAB(infojfevtab);
				msgobjects.setINFOJFEVTABLIST(infojfevtablist);
			}
	
			UfisMarshal ufisMarshal = new UfisMarshal(_ms, _msgActionType, MSGIF, null);
			_returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);
			isTransferred = true;
		} catch (Exception e) {
			LOG.error("Cannot transform egds message: {}", e.getMessage());
			//LOG.error("Message Dropped: \n{}", _flightXml);
			return false;
		}
		return isTransferred;
	}

	public String getReturnXML() {
		return _returnXML;
	}

	public void setReturnXML(String _returnXML) {
		this._returnXML = _returnXML;
	}

}
