package com.ufis_as.ufisapp.ek.intf.ekrtc;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javassist.expr.Instanceof;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;
import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlEKRTCFltJobAssignBean;
import com.ufis_as.ufisapp.ek.eao.DlEKRTCFltJobTaskBean;
import com.ufis_as.ufisapp.ek.eao.DlEKRTCStaffShiftBean;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

/**
 * 
 * @author chensl
 *
 *  This is the business logic both share by EK-RTC and ENG-RTC
 */

@Stateless
public class BlHandleCedaEKRTC {
	  private static final Logger LOG = LoggerFactory.getLogger(BlHandleCedaEKRTC.class);

	   @EJB
	   IAfttabBean afttabBean;
	   @EJB
	   DlEKRTCFltJobAssignBean dlEKRTCFltJobAssignBean;
	   @EJB
	   DlEKRTCStaffShiftBean dlEKRTCStaffShiftBean;
	   @EJB
	   DlEKRTCFltJobTaskBean dlEKRTCFltJobTaskBean;
		@EJB
		BlUfisExceptionQueue _blUfisExcepQ;
		@EJB
		private BlIrmtabFacade _irmtabFacade;
		   @EJB
		    private EntStartupInitSingleton entStartupInitSingleton;
			@EJB
			private BlUfisBCTopic ufisTopicProducer;
		
		protected Message rawMsg;
		 String logLevel = null;
			Boolean msgLogged = Boolean.FALSE;
			long irmtabRef;
//	   @EJB
//	   EntStartupInitSingleton entStartupInitSingleton;
	   
	   public void handleEKRTC(BlHandleEKRTC blHandleEKRTC, EntDbStaffShift entDbStaffShift){
//		   blHandleEKRTC.setEntStartupInitSingleton(entStartupInitSingleton);
		   
		   logLevel = entStartupInitSingleton.getIrmLogLev();
		   
			msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
		   
		   rawMsg = blHandleEKRTC.getRawMsg();

			   if ("INS".equalsIgnoreCase(blHandleEKRTC.getStaffShiftMsgSubType())){

				   long startTime = System.currentTimeMillis();
				// insert into StaffShift table
				dlEKRTCStaffShiftBean.Persist(entDbStaffShift);
				LOG.info("insert record in StaffShift table, takes {} ms", System.currentTimeMillis()
						- startTime);
				
				// send notification
				ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
						String.valueOf(entDbStaffShift.getIdFlight()), null, entDbStaffShift);
				
				// search record in FltJobTask table

//				   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
				   List<EntDbFltJobAssign> existJobAssignList;
				 startTime = System.currentTimeMillis();
				existJobAssignList = dlEKRTCFltJobAssignBean
						.getExsitRecordByShiftIdX(entDbStaffShift.getShiftId());
				LOG.info("search records in FltJobAssign table, takes {} ms", System.currentTimeMillis()
						- startTime);	
					
					EntDbFltJobAssign existJobAssignForTask = null;
					
					if (existJobAssignList != null && existJobAssignList.size() >0){
						existJobAssignForTask = existJobAssignList.get(0);
					}
					
					
				   if (existJobAssignForTask != null){   
					   // search record in jobAssign table and set value
//					   EntDbFltJobAssign existJobAssign = dlEKRTCFltJobAssignBean.getExsitRecord(existJobAssignForTask.getTaskId(), entDbStaffShift.getShiftId());
					   
					   EntDbFltJobAssign oldEntDbFltJobAssign = (EntDbFltJobAssign)SerializationUtils.clone(existJobAssignForTask);
					   
					 startTime = System.currentTimeMillis();
					// staffShift set idFlight
					entDbStaffShift.setIdFlight(existJobAssignForTask
							.getIdFlight());
					entDbStaffShift.setIdFltJobTask(existJobAssignForTask
							.getIdFltJobTask());
					entDbStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					entDbStaffShift.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
					//					   dlEKRTCStaffShiftBean.Update(entDbStaffShift);
					// added by 2013-11-15
					existJobAssignForTask.setStaffFirstName(entDbStaffShift
							.getStaffFirstName());
					existJobAssignForTask.setStaffName(entDbStaffShift
							.getStaffName());
					existJobAssignForTask.setStaffNumber(entDbStaffShift
							.getStaffNumber());
					existJobAssignForTask.setStaffType(entDbStaffShift
							.getStaffType());
					existJobAssignForTask.setIdStaffShift(entDbStaffShift
							.getId());
					existJobAssignForTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					existJobAssignForTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
					
					LOG.info("update staffShift FltJobAssign fltJobTask table, takes {} ms", System.currentTimeMillis()
							- startTime);
					
					// send notification 
					ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
							String.valueOf(existJobAssignForTask.getIdFlight()), oldEntDbFltJobAssign, existJobAssignForTask);
					   
//					   EntDbFltJobAssign existJobAssign = null;                                          comment on 2013-11-15                                  
//					   
//					  Iterator<EntDbFltJobAssign> existJobAssignListIt = existJobAssignList.iterator();
//					  while (existJobAssignListIt.hasNext()){
//						  EntDbFltJobAssign jobAssign = existJobAssignListIt.next();
//						  if (jobAssign.getShiftId().equals(entDbStaffShift.getShiftId())){
//							  existJobAssign = jobAssign;
//						  }
//					  }
//					
//					   if (existJobAssign == null){
//						   existJobAssign = new EntDbFltJobAssign(); 
//						   
//						   existJobAssign.setStaffFirstName(entDbStaffShift.getStaffFirstName());
//						   existJobAssign.setStaffName(entDbStaffShift.getStaffName());
//						   existJobAssign.setStaffNumber(entDbStaffShift.getStaffNumber());
//						   existJobAssign.setStaffType(entDbStaffShift.getStaffType());
//						   existJobAssign.setIdStaffShift(entDbStaffShift.getId());
//						   existJobAssign.setShiftId(entDbStaffShift.getShiftId());
//						   
////						   dlEKRTCFltJobAssignBean.Persist(existJobAssign);
//					   }else {
//						   existJobAssign.setStaffFirstName(entDbStaffShift.getStaffFirstName());
//						   existJobAssign.setStaffName(entDbStaffShift.getStaffName());
//						   existJobAssign.setStaffNumber(entDbStaffShift.getStaffNumber());
//						   existJobAssign.setStaffType(entDbStaffShift.getStaffType());
//						   existJobAssign.setIdStaffShift(entDbStaffShift.getId());
//						   
////						   dlEKRTCFltJobAssignBean.Update(existJobAssign);			   
//					   }
					   

					  		  
				   }
				   
			   }else if ("UPD".equalsIgnoreCase(blHandleEKRTC.getStaffShiftMsgSubType())){
				   // search exist record
				   EntDbStaffShift existStaffShift;
				long startTime = System.currentTimeMillis();
				existStaffShift = dlEKRTCStaffShiftBean
						.getExsitRecordByShiftIdX(entDbStaffShift.getShiftId());
				LOG.info("search records in staffShift table, takes {} ms", System.currentTimeMillis()
						- startTime);

				    
				   if (existStaffShift != null){
			
					   EntDbStaffShift oldEntDbStaffShift = (EntDbStaffShift)SerializationUtils.clone(existStaffShift);
					   
					 startTime = System.currentTimeMillis();
					// update staffShift table
					Class<EntDbStaffShift> EntDbStaffShiftClass = EntDbStaffShift.class;
					Field[] EntDbStaffShiftClassFields = EntDbStaffShiftClass
							.getDeclaredFields();
					//					   Field[] EntDbStaffShiftClassFields = EntDbStaffShiftClass.getFields();
					for (Field f : EntDbStaffShiftClassFields) {
						if (!"id".equalsIgnoreCase(f.getName())
								&& !"serialVersionUID".equalsIgnoreCase(f
										.getName())) {
							setEntityValue(
									existStaffShift,
									f.getName(),
									getEntityValue(entDbStaffShift, f.getName()));
						}
					}
					existStaffShift.setRecStatus(" ");
					existStaffShift.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
					existStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					//					   dlEKRTCStaffShiftBean.Update(entDbStaffShift);
					LOG.info("update  staffShift table, takes {} ms", System.currentTimeMillis()
							- startTime);
					
					// send notification 
					ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
							String.valueOf(existStaffShift.getIdFlight()), oldEntDbStaffShift, existStaffShift);

//					   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
					   List<EntDbFltJobAssign> existJobAssignList;
					startTime = System.currentTimeMillis();
					existJobAssignList = dlEKRTCFltJobAssignBean
							.getExsitRecordByShiftIdX(entDbStaffShift
									.getShiftId());
					LOG.info("search records in FltJobAssign table, takes {} ms", System.currentTimeMillis()
							- startTime);
		
//					   // search record in FltJobTask table
//					   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
						EntDbFltJobAssign existJobAssignForTask = null;
						
						if (existJobAssignList != null && existJobAssignList.size() >0){
							existJobAssignForTask = existJobAssignList.get(0);
						}
						
						
					   if (existJobAssignForTask != null){
						   EntDbFltJobAssign oldEntDbFltJobAssign = (EntDbFltJobAssign)SerializationUtils.clone(existJobAssignForTask);
						   
						   // staffShift set idFlight
						   existStaffShift.setIdFlight(existJobAssignForTask.getIdFlight());
						   existStaffShift.setIdFltJobTask(existJobAssignForTask.getIdFltJobTask());
						   
						   // added by 2013-11-15
						startTime = System.currentTimeMillis();
						existJobAssignForTask.setStaffFirstName(existStaffShift
								.getStaffFirstName());
						existJobAssignForTask.setStaffName(existStaffShift
								.getStaffName());
						existJobAssignForTask.setStaffNumber(existStaffShift
								.getStaffNumber());
						existJobAssignForTask.setStaffType(existStaffShift
								.getStaffType());
						existJobAssignForTask.setIdStaffShift(existStaffShift
								.getId());
						
						existJobAssignForTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						existJobAssignForTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
						LOG.info("update FltJobAssign table, takes {} ms",
								System.currentTimeMillis() - startTime);

						   
//						   dlEKRTCStaffShiftBean.Update(entDbStaffShift);
						   
//						   // search record in jobAssign table and set value
//						   EntDbFltJobAssign existJobAssign = dlEKRTCFltJobAssignBean.getExsitRecord(existJobTask.getTaskId(), existStaffShift.getShiftId());
						   
//						   EntDbFltJobAssign existJobAssign = null;	          comment on 2013-11-15
//						   
//							  Iterator<EntDbFltJobAssign> existJobAssignListIt = existJobAssignList.iterator();
//							  while (existJobAssignListIt.hasNext()){
//								  EntDbFltJobAssign jobAssign = existJobAssignListIt.next();
//								  if (jobAssign.getShiftId().equals(entDbStaffShift.getShiftId())){
//									  existJobAssign = jobAssign;
//								  }
//							  }
//						   
//						   
//						   if (existJobAssign != null){
//							   startTime = System.currentTimeMillis();
//							   
//							   existJobAssign.setStaffFirstName(existStaffShift.getStaffFirstName());
//							   existJobAssign.setStaffName(existStaffShift.getStaffName());
//							   existJobAssign.setStaffNumber(existStaffShift.getStaffNumber());
//							   existJobAssign.setStaffType(existStaffShift.getStaffType());
//							   existJobAssign.setIdStaffShift(existStaffShift.getId());
////							   dlEKRTCFltJobAssignBean.Update(existJobAssign);		
//							   
//								endTime = System.currentTimeMillis();
//								LOG.info("takes {} ms to update FltJobAssign table ", (endTime - startTime));
//						   }
						  		  
					   }
					   
				   }
						   
				   
			   }
			   else if ("DEL".equalsIgnoreCase(blHandleEKRTC.getStaffShiftMsgSubType())){ // added by 2013-11-15

				   long startTime = System.currentTimeMillis();
				EntDbStaffShift existStaffShift = dlEKRTCStaffShiftBean
						.getExsitRecordByShiftIdX(entDbStaffShift.getShiftId());
				LOG.info("search records in staffShift table, takes {} ms", System.currentTimeMillis()
						- startTime);
				
				// update staffShift
				if (existStaffShift != null){
					existStaffShift.setRecStatus("X");
					
					// send notification 
					ufisTopicProducer.sendNotification(true, UfisASCommands.DET,
							String.valueOf(existStaffShift.getIdFlight()), null, existStaffShift);
				}else{
					LOG.warn("could not find exist record from staffShif for DEL msg");
				}
				
				// update fltJobAssign table

//				   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
				  List<EntDbFltJobAssign> existJobAssignList;
				  
				startTime = System.currentTimeMillis();
				existJobAssignList = dlEKRTCFltJobAssignBean
						.getExsitRecordByShiftIdX(entDbStaffShift.getShiftId());
				LOG.info("search records in FltJobAssign table, takes {} ms", System.currentTimeMillis()
						- startTime);

//				   // search record in FltJobTask table
//				   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
					EntDbFltJobAssign existJobAssignForTask = null;
					
					if (existJobAssignList != null && existJobAssignList.size() >0){
						existJobAssignForTask = existJobAssignList.get(0);
					}
					
					if (existJobAssignForTask != null){
						existJobAssignForTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						existJobAssignForTask.setUpdatedUser("RTC");
						existJobAssignForTask.setRecStatus("X");
						existJobAssignForTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
						
						// send notification 
						ufisTopicProducer.sendNotification(true, UfisASCommands.DET,
								String.valueOf(existJobAssignForTask.getIdFlight()), null, existJobAssignForTask);
					}

				  
			   }
			   

		   
	   }
	   
//	   public void handleEKRTC(BlHandleEKRTC blHandleEKRTC, EntDbFltJobAssign entDbFltJobAssign){
//		   // search flight
//		   BigDecimal urno = afttabBean.getUrnoForEKRTC(blHandleEKRTC.getFlno(), blHandleEKRTC.getStod(), blHandleEKRTC.getStoa(), blHandleEKRTC.getAdid());
//		   
//		   if (urno != null){
//			   if ("INS".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){
//				   
//			   }else if ("UPD".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){
//				   
//			   }
////			   else if ("DEL".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){
////				   // Not applicable for this message. Since only started shift details are sent, there will not be any deletion condition.
////			   }
//			   
//		   }else{
//			   LOG.info("Flight not found,  message is rejected, flno: {}, stod: {}, stoa: {}, adid: {}",blHandleEKRTC.getFlno(), blHandleEKRTC.getStod(), blHandleEKRTC.getStoa(), blHandleEKRTC.getAdid());
//              return; 
//		   }
//		   
//	   }
	   
	   public void handleEKRTC(BlHandleEKRTC blHandleEKRTC, EntDbFltJobTask entDbFltJobTask, EntDbFltJobAssign entDbFltJobAssign){
		   // search flight
		   
		   logLevel = entStartupInitSingleton.getIrmLogLev();
		   rawMsg = blHandleEKRTC.getRawMsg();
		   
			msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}

		   BigDecimal urno;
		   long startTime = System.currentTimeMillis();
		   if (!"0".equals(blHandleEKRTC.getEntRTCFlightSearchObject().getOri())){	
			   urno = afttabBean.getUrnoForEKRTC(blHandleEKRTC.getEntRTCFlightSearchObject()); 
			   LOG.info("search records in afttab table, takes {} ms", System.currentTimeMillis() - startTime);
		   }else{
			   LOG.debug("skip searching the flight as ORI = 0");
			   urno =new BigDecimal(0);
		   }
		   
		   if (urno != null){
			   if ("INS".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){
				   // insert into fltJobTask table
				   entDbFltJobTask.setIdFlight(urno);

				startTime = System.currentTimeMillis();
				dlEKRTCFltJobTaskBean.Persist(entDbFltJobTask);
				LOG.info("persist record to fltJobTask table, takes {} ms", System.currentTimeMillis()
						- startTime);
						
				// send notification 
				ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
						String.valueOf(entDbFltJobTask.getIdFlight()), null, entDbFltJobTask);
				
				// insert into fltJobAssign table
				   entDbFltJobAssign.setIdFlight(urno);
				   entDbFltJobAssign.setIdFltJobTask(entDbFltJobTask.getId());

				 startTime = System.currentTimeMillis();
				dlEKRTCFltJobAssignBean.Persist(entDbFltJobAssign);
				LOG.info("persist record to fltJobAssign table, takes {} ms", System.currentTimeMillis()
						- startTime);
				
				// send notification 
				ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
						String.valueOf(entDbFltJobAssign.getIdFlight()), null, entDbFltJobAssign);
	
				   // search record in staffShift table
				   EntDbStaffShift existStaffShift;
				startTime = System.currentTimeMillis();
				existStaffShift = dlEKRTCStaffShiftBean
						.getExsitRecordByShiftIdX(entDbFltJobAssign
								.getShiftId());
				LOG.info("search records in staffShift table, takes {} ms", System.currentTimeMillis()
						- startTime);
	
				   
				   if (existStaffShift != null){
					   
					   EntDbStaffShift oldEntDbStaffShift = (EntDbStaffShift)SerializationUtils.clone(existStaffShift);
					   EntDbFltJobTask oldEntDbFltJobTask = (EntDbFltJobTask)SerializationUtils.clone(entDbFltJobTask);
					   
					   // staffShift set idFlight and updated
					   existStaffShift.setIdFlight(entDbFltJobTask.getIdFlight());
					   existStaffShift.setIdFltJobTask(entDbFltJobTask.getId());
					   existStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
//					   dlEKRTCStaffShiftBean.Update(existStaffShift);
					   
					   // update fltJboAssign
					   entDbFltJobAssign.setIdStaffShift(existStaffShift.getId());
					   entDbFltJobAssign.setStaffFirstName(existStaffShift.getStaffFirstName());
					   entDbFltJobAssign.setStaffName(existStaffShift.getStaffName());
					   entDbFltJobAssign.setStaffNumber(existStaffShift.getStaffNumber());
					   entDbFltJobAssign.setStaffType(existStaffShift.getStaffType());		   
//					   dlEKRTCFltJobAssignBean.Update(entDbFltJobAssign);			
					   
						// send notification 
						ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
								String.valueOf(entDbFltJobTask.getIdFlight()), oldEntDbFltJobTask, entDbFltJobTask);
						
						ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
								String.valueOf(existStaffShift.getIdFlight()), oldEntDbStaffShift, existStaffShift);
				   }
				   
				   
			   }else if ("UPD".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){
				   
			
				   // search record in FltJobTask table
				   EntDbFltJobTask existJobTask;
				startTime = System.currentTimeMillis();
				existJobTask = dlEKRTCFltJobTaskBean
						.getExsitRecordX(entDbFltJobTask.getTaskId());
				LOG.info("search records in jobTask table, takes {} ms", System.currentTimeMillis()
						- startTime);

//				   // search record in FltJobTask table
//				   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecord(entDbFltJobTask);
//				   // search exist record of fltJobTask
//				   EntDbFltJobAssign existJobAssign = dlEKRTCFltJobAssignBean.getExsitRecord(existJobTask.getId(), entDbFltJobAssign.getShiftId());
				   
				   if (existJobTask != null){
					   // search exist record of FltJobAssign and StaffShift by idFltJobTask					  startTime = System.currentTimeMillis();
					   // search record in staffShift table
					   
					   EntDbFltJobTask oldEntDbFltJobTask = (EntDbFltJobTask)SerializationUtils.clone(existJobTask);
					   
					   boolean isContainShiftId = false;
					   

					   List<EntDbStaffShift> existStaffShiftList;
					startTime = System.currentTimeMillis();
					existStaffShiftList = dlEKRTCStaffShiftBean
							.getExsitRecordByidFltJboTaskX(existJobTask.getId());
					LOG.info("search records in staffShift table, takes {} ms", System.currentTimeMillis()
							- startTime);

//						   EntDbFltJobTask existJobTask = dlEKRTCFltJobTaskBean.getExsitRecordByJobAssignShiftId(entDbStaffShift.getShiftId());
						   List<EntDbFltJobAssign> existJobAssignList;
						startTime = System.currentTimeMillis();
						existJobAssignList = dlEKRTCFltJobAssignBean
								.getExsitRecordX(existJobTask.getId());
						LOG.info("search records in jobAssign table, takes {} ms",
								System.currentTimeMillis() - startTime);

						
							EntDbStaffShift MatchStaffShift = null;
							
						if (existStaffShiftList != null){
						Iterator<EntDbStaffShift> existStaffShiftListIt = existStaffShiftList.iterator();
						while (existStaffShiftListIt.hasNext()){
							EntDbStaffShift StaffShift = existStaffShiftListIt.next();
							if (StaffShift.getShiftId().equals(entDbFltJobAssign.getShiftId())){
								isContainShiftId = true;
								MatchStaffShift = StaffShift;
								break;
							}
						}
						}
						
					   if (isContainShiftId){
		
							startTime = System.currentTimeMillis();
							// update jobTask
							Class<EntDbFltJobTask> EntDbFltJobTask = EntDbFltJobTask.class;
							Field[] EntDbFltJobTaskClassFields = EntDbFltJobTask
									.getDeclaredFields();
							for (Field f : EntDbFltJobTaskClassFields) {
								if (!"id".equalsIgnoreCase(f.getName())
										&& !"serialVersionUID"
												.equalsIgnoreCase(f.getName())) {
									setEntityValue(
											existJobTask,
											f.getName(),
											getEntityValue(entDbFltJobTask,
													f.getName()));
								}
							}
							existJobTask.setRecStatus(" ");
							existJobTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
							existJobTask.setUpdatedDate(HpUfisCalendar
									.getCurrentUTCTime());
							LOG.info("update fltJobTask table, takes {} ms",
									System.currentTimeMillis() - startTime);
							
							// send notification 
							ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
									String.valueOf(existJobTask.getIdFlight()), oldEntDbFltJobTask, existJobTask);
							
						EntDbFltJobAssign matchFltJobAssign = null;
						   Iterator<EntDbFltJobAssign> existJobAssignListIt = existJobAssignList.iterator();
						   while (existJobAssignListIt.hasNext()){
							   EntDbFltJobAssign FltJobAssign = existJobAssignListIt.next();
							   if (FltJobAssign.getShiftId().equals(entDbFltJobAssign.getShiftId())){
								   matchFltJobAssign = FltJobAssign;
								   break;
							   }
						   }
						   
						   if (matchFltJobAssign != null){
				   
						   startTime = System.currentTimeMillis();
							// update jobAssign
							Class<EntDbFltJobAssign> EntDbFltJobAssign = EntDbFltJobAssign.class;
							Field[] EntDbFltJobAssignClassFields = EntDbFltJobAssign
									.getDeclaredFields();
							for (Field f : EntDbFltJobAssignClassFields) {
								if (!"id".equalsIgnoreCase(f.getName())
										&& !"serialVersionUID"
												.equalsIgnoreCase(f.getName())) {
									setEntityValue(
											matchFltJobAssign,
											f.getName(),
											getEntityValue(entDbFltJobAssign,
													f.getName()));
								}
							}
							matchFltJobAssign.setRecStatus(" ");
							matchFltJobAssign.setUpdatedDate(HpUfisCalendar
									.getCurrentUTCTime());
							LOG.info("update fltJobAssign table, takes {} ms",
									System.currentTimeMillis() - startTime);

//							   dlEKRTCFltJobAssignBean.Update(existJobAssign);
							   }else {
								   LOG.info("existJobAssign is null, jobAssign table will not be updated");
							   }
						
						   		if (MatchStaffShift != null){
						   		 startTime = System.currentTimeMillis();
						   			
						   		startTime = System.currentTimeMillis();
								// update staffShift
								MatchStaffShift.setIdFlight(existJobTask
										.getIdFlight());
								MatchStaffShift.setIdFltJobTask(existJobTask
										.getId());
								LOG.info("update staffShift table, takes {} ms",
										System.currentTimeMillis() - startTime);
	
						   		}
						
					   }else{
						  startTime = System.currentTimeMillis();
						   
						   // mark fltJobTask as 'X'
						   existJobTask.setRecStatus("X");
						   existJobTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
						   existJobTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						   
						   Iterator<EntDbStaffShift> existStaffShifListIt = existStaffShiftList.iterator();
						   while(existStaffShifListIt.hasNext()){
							   EntDbStaffShift entDbStaffShiftX = existStaffShifListIt.next();
							   // mark existing staffShift  as 'X', delete
							   entDbStaffShiftX.setRecStatus("X");
							   entDbStaffShiftX.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
							   entDbStaffShiftX.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						   }
						   
						   Iterator<EntDbFltJobAssign> existEntDbFltJobAssignListIt = existJobAssignList.iterator();
						   while (existEntDbFltJobAssignListIt.hasNext()){
							   EntDbFltJobAssign entDbFltJobAssignX  = existEntDbFltJobAssignListIt.next();
							   // mark existing fltJobTask  as 'X', delete
							   entDbFltJobAssignX.setRecStatus("X");
							   entDbFltJobAssignX.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
							   entDbFltJobAssignX.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());

						   }

							LOG.info("update related staffShift fltJobAssign fltJobTask records to 'X', takes {} ms ", (System.currentTimeMillis() - startTime)); 

							startTime = System.currentTimeMillis();
							// insert new record to fltJobTask and fltJobAssign table
							entDbFltJobTask.setIdFlight(urno);
							dlEKRTCFltJobTaskBean.Persist(entDbFltJobTask);
							entDbFltJobAssign.setIdFlight(urno);
							entDbFltJobAssign.setIdFltJobTask(entDbFltJobTask
									.getId());
							dlEKRTCFltJobAssignBean.Persist(entDbFltJobAssign);
							LOG.info("insert new record to fltJobTask and fltJobAssign table, takes {} ms",
									System.currentTimeMillis() - startTime);
		
					   }

			    
				   }
//				   else {
//					   LOG.info("exist record for JobTask not found, resourceAssignmentType 'UPD' message will be insert as new record");
//					   // insert into fltJobTask table
//					   entDbFltJobTask.setIdFlight(urno);
//					   dlEKRTCFltJobTaskBean.Persist(entDbFltJobTask);
//					   // insert into fltJobAssign table
//					   entDbFltJobAssign.setIdFlight(urno);
//					   entDbFltJobAssign.setIdFltJobTask(entDbFltJobTask.getId());
//					   dlEKRTCFltJobAssignBean.Persist(entDbFltJobAssign);
//					   
//					   startTime = System.currentTimeMillis();
//					   // search record in staffShift table
//					   EntDbStaffShift existStaffShift = dlEKRTCStaffShiftBean.getExsitRecordByShiftIdX(entDbFltJobAssign.getShiftId());
//					   endTime = System.currentTimeMillis();
//						LOG.info("takes {} ms to search records in staffShift table ", (endTime - startTime));
//					   
//					   if (existStaffShift != null){
//						   // staffShift set idFlight and updated
//						   existStaffShift.setIdFlight(entDbFltJobTask.getIdFlight());
//						   existStaffShift.setIdFltJobTask(entDbFltJobTask.getId());
//						   existStaffShift.setUpdatedDate(new Date());
////						   dlEKRTCStaffShiftBean.Update(existStaffShift);
//						   
//						   // update fltJboAssign
//						   entDbFltJobAssign.setIdStaffShift(existStaffShift.getId());
//						   entDbFltJobAssign.setStaffFirstName(existStaffShift.getStaffFirstName());
//						   entDbFltJobAssign.setStaffName(existStaffShift.getStaffName());
//						   entDbFltJobAssign.setStaffNumber(existStaffShift.getStaffNumber());
//						   entDbFltJobAssign.setStaffType(existStaffShift.getStaffType());		   
////						   dlEKRTCFltJobAssignBean.Update(entDbFltJobAssign);			
//					   }
//				   }
			   
				   
			   }else if ("DEL".equalsIgnoreCase(blHandleEKRTC.getResAssignMsgSubType())){

				   // search record in FltJobTask table
				   EntDbFltJobTask existJobTask;
				 startTime = System.currentTimeMillis();
				existJobTask = dlEKRTCFltJobTaskBean
						.getExsitRecordX(entDbFltJobTask.getTaskId());
				LOG.info("search records in jobTask table, takes {} ms", System.currentTimeMillis()
						- startTime);
				   
				   if (existJobTask != null){
					   
					   startTime = System.currentTimeMillis();
					   // mark task record as deleted, "X"
						// update jobTask
					   Class<EntDbFltJobTask> entDbFltJobTaskClass = EntDbFltJobTask.class;
					   Field[] entDbFltJobTaskClassFields = entDbFltJobTaskClass.getDeclaredFields();
					   for(Field f : entDbFltJobTaskClassFields){
						   if (!"id".equalsIgnoreCase(f.getName()) && !"serialVersionUID".equalsIgnoreCase(f.getName())){
							   setEntityValue(existJobTask, f.getName(), getEntityValue(entDbFltJobTask, f.getName()));
						   }
					   }   
					   
					   existJobTask.setRecStatus("X");
//					   dlEKRTCFltJobTaskBean.Update(existJobTask);
					   existJobTask.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
					   existJobTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					   
					   // search exist record of jobAssign
					   List<EntDbFltJobAssign> existJobAssignList = dlEKRTCFltJobAssignBean.getExsitRecordX(existJobTask.getId());
					   Iterator<EntDbFltJobAssign>  existJobAssignListIt = existJobAssignList.iterator();
					   while (existJobAssignListIt.hasNext()){
						   EntDbFltJobAssign existFltJobAssign = existJobAssignListIt.next();
						   
							// update jobTask
						   Class<EntDbFltJobAssign> entDbFltJobAssignClass = EntDbFltJobAssign.class;
						   Field[] entDbFltJobAssignClassFields = entDbFltJobAssignClass.getDeclaredFields();
						   for(Field f : entDbFltJobAssignClassFields){
							   if (!"id".equalsIgnoreCase(f.getName()) && !"serialVersionUID".equalsIgnoreCase(f.getName())){
								   setEntityValue(existFltJobAssign, f.getName(), getEntityValue(entDbFltJobAssign, f.getName()));
							   }
						   }   
						// mark job assign as deleted, "X"
						   existFltJobAssign.setIdFlight(existJobTask.getIdFlight());
						   existFltJobAssign.setRecStatus("X");
						   existFltJobAssign.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
//						   dlEKRTCFltJobAssignBean.Update(existFltJobAssign);
						   existFltJobAssign.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						   
							// send notification 
							ufisTopicProducer.sendNotification(true, UfisASCommands.DET,
									String.valueOf(existFltJobAssign.getIdFlight()), null, existFltJobAssign);
					   }
					   
					   // search exist record of staffShift
					   List<EntDbStaffShift> existStaffShiftList  = dlEKRTCStaffShiftBean.getExsitRecordByidFltJboTaskX(existJobTask.getId());
					   Iterator<EntDbStaffShift> existStaffShiftListIt = existStaffShiftList.iterator();
					   while (existStaffShiftListIt.hasNext()){
						   EntDbStaffShift existStaffShift = existStaffShiftListIt.next();
						// mark staff shift record as deleted, "X"
						   existStaffShift.setRecStatus("X");
						   existStaffShift.setUpdatedUser(HpEKConstants.EKRTC_DATA_SOURCE);
//						   dlEKRTCStaffShiftBean.Update(existStaffShift);
						   existStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						   
							// send notification 
							ufisTopicProducer.sendNotification(true, UfisASCommands.DET,
									String.valueOf(existStaffShift.getIdFlight()), null, existStaffShift);
					   }
		   
						LOG.info("update related staffShift fltJobTask fltJobAssign records to 'X' , takes {} ms", ( System.currentTimeMillis() - startTime)); 
					   
				   }else {
					   LOG.info("exist record for JobTask not found, resourceAssignmentType 'DEL' message is rejected");
				   }
			   }
			   
		   }else{
			   LOG.info("Flight not found,  message is rejected, flno: {}, stod: {}", blHandleEKRTC.getEntRTCFlightSearchObject().getFlno(), blHandleEKRTC.getEntRTCFlightSearchObject().getStod());
			   LOG.info("stoa: {}, adid: {}",blHandleEKRTC.getEntRTCFlightSearchObject().getStoa(), blHandleEKRTC.getEntRTCFlightSearchObject().getAdid());
			   
			   sendErroNotification(EnumExceptionCodes.ENOFL);
              return; 
		   }
		   
	   }
	  
	  

	   private Object setEntityValue(Object entity, String columnNm, Object value) {
		   try {
			if (value == null){
				
				return null;
			}
			   
		    String method = getMethodName(columnNm, "set");
		    if (entity instanceof List) {
		    } else {
		     Class<? extends Object> clazz = entity.getClass();
		     Field field = clazz.getDeclaredField(columnNm); 
		     Method getMethod = null;
		     getMethod = clazz.getMethod(method, field.getType());
		     getMethod.invoke(entity, value);
		    }
		   } catch (Exception e) {
		    e.printStackTrace();
		   }
		   return value;
		  }
	   
	   private Object getEntityValue(Object entity, String columnNm) {
		   Object o = null;
		   try {
		    String method = getMethodName(columnNm, "get");
		    Class<? extends Object> clazz = entity.getClass();
		    Method getMethod = (Method) clazz.getDeclaredMethod(method);
		    if (getMethod != null) {
		     o = getMethod.invoke(entity);
		    }
		    
		    if (o == null){
		    Field field  = clazz.getDeclaredField(columnNm); 
		    	if (field.getType() == String.class ){
		    		o = " ";
		    	}
		    
		    }
		    
		    
		   } catch (Exception e) {
		    e.printStackTrace();
		   }
		   
		   return o;
		  }
	   
	   private String getMethodName(String s, String prefix) {
		   s = s.replaceAll("_", "");
		   s = s.substring(0, 1).toUpperCase() + s.substring(1);
		   s = prefix + s;
		   return s;
		  }
	   
		private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
				throws IOException, JsonGenerationException, JsonMappingException {
			List<Object> recList = new ArrayList<Object>();
			List<String> dataList = new ArrayList<String>();
			dataList.add(irmtabRef.toString());
			dataList.add(HpUfisAppConstants.CON_IRMTAB);
			dataList.add(expCode.name());
			dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
			dataList.add(expCode.toString());
			dataList.add(HpEKConstants.EKRTC_DATA_SOURCE);

			recList.add(dataList);

			// need to send to Queue
			String msg = HpUfisMsgFormatter.formExceptionData(recList,
					UfisASCommands.IRT.name(), irmtabRef, true, HpEKConstants.EKRTC_DATA_SOURCE);
			// send to ERRORQUEUE
			_blUfisExcepQ.sendMessage(msg);
			LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

		}
		
		
		private void sendErroNotification(EnumExceptionCodes eec){
			
			if (logLevel
					.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
							.name())) {
				if (!msgLogged) {
					irmtabRef = _irmtabFacade.storeRawMsg(rawMsg, HpEKConstants.EKRTC_DATA_SOURCE);
					msgLogged = Boolean.TRUE;
				}
			
		
			if (irmtabRef > 0) {
				try {
					sendErrInfo(eec, irmtabRef);
				} catch (JsonGenerationException e) {
					LOG.error("Erro send ErroInfo, {}",e);
				} catch (JsonMappingException e) {
					LOG.error("Erro send ErroInfo, {}",e);
				} catch (IOException e) {
					LOG.error("Erro send ErroInfo, {}",e);
				}
			}
			
		}
			
		}
	
	  
	  
	  
}
