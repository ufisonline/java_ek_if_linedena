package com.ufis_as.ufisapp.ek.intf.ekrtc;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.SerializationUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;
import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;
import com.ufis_as.ek_if.rms.entities.EntRTCFlightSearchObject;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.ekrtc.resourceshift.ResourceShiftType;
import ek.ekrtc.rtcassignment.ResourceAssignmentType;

/**
 * 
 * @author SCH
 *
 */
@Stateless
public class BlHandleEKRTC {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(BlHandleEKRTC.class);
    
    @EJB
    private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlIrmtabFacade _irmfacade;
	
    
//    private List<EntDbLoadPaxConn> _edpiods = new ArrayList<>();
    protected String xmlMsg = "";
    protected Message rawMsg;
    protected InterfaceConfig _interfaceConfig;
//    protected Marshaller _ms;
    protected Unmarshaller _um;
//    private JAXBContext _cnxJaxb;
//    private JAXBContext _cnxJaxbM;
    
    private ResourceShiftType resourceShiftType;
    private ResourceAssignmentType resourceAssignmentType;
    private EntDbStaffShift entDbStaffShift;
    private EntDbFltJobAssign entDbFltJobAssign;
	private EntDbFltJobTask entDbFltJobTask;
	
	private String staffShiftMsgSubType;
	private String resAssignMsgSubType;
	
	private EntRTCFlightSearchObject entRTCFlightSearchObject; // added according to flight searching logic change on 2013-11-20
//	
	private String flno;
	private String stod;
	private String stoa;
	private String adid;
//	private BigDecimal fltJobtaskShiftId;

	// array for the enum type data
	 String[] messageTypeArray = {"SFT"};
	 String[] messageShiftSubtypeArray =  {"INS", "UPD", "DEL"};
	 String[] shiftsttArray = {"ACTIVE", "FINISHED", "SCHEDULED", "CANCELLED", "CONFIRMED", "FiConf"};   // "CONFIRMED", "FiConf" added according to version 2
	 String[] shiftrtyArray = {"EMPLOYEE", "RAMP", "EQUIPMENT", "IMMOBILE RESOURCE"};
	 //String[] shiftMsgSourceArray =  {"EKRTC", "GXRTC", "CSRTC"};
	 
	 String[] messageAssignmentTypeArray = {"REA"};
	 String[] messageAssignmentSubtypeArray = {"INS", "UPD", "DEL"};
	 String[] messageSourceArray = {"EKRTC", "GXRTC", "CSRTC"};
	 
	 String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;
	long irmtabRef;
    
//	 @PostConstruct
//		private void initial() {
//		  try {
//	            _cnxJaxb = JAXBContext.newInstance(
//	                    ResourceShiftType.class,
//	                    ResourceAssignmentType.class);
//	            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
//	            _um = _cnxJaxb.createUnmarshaller();
//	            _ms = _cnxJaxbM.createMarshaller();
//	        } catch (JAXBException ex) {
//	            LOG.error("JAXBException when creating Unmarshaller");
//	        }
//		}
	public void init(Unmarshaller um){
//		this._ms = ms;
		this._um = um;
	}


    public boolean unMarshal() {
        try {
            if (xmlMsg.contains("ResourceShift.xsd")) {
            	resourceShiftType = (ResourceShiftType) _um.unmarshal(new StreamSource(new StringReader(xmlMsg)));
                if (readResourceShiftType(resourceShiftType)) {
                    LOG.debug("resourceShiftType messageID {}", resourceShiftType.getMeta().getMessageID());
                    return true;
                }
                return false;
            }
            if (xmlMsg.contains("ResourceAssignment.xsd")) {
            	resourceAssignmentType = (ResourceAssignmentType) _um.unmarshal(new StreamSource(new StringReader(xmlMsg)));
                if (readResourceAssignmentType(resourceAssignmentType)) {
                    LOG.debug("resourceAssignmentType messageID {}", resourceAssignmentType.getMeta().getMessageID());
                    return true;
                } 
                    return false;
            }
         
            
        } catch (JAXBException ex) {
            LOG.error("Flight Event JAXB exception {}", ex);
            LOG.error("Dropped Message details: \n{}", xmlMsg);
            return false;
        } 
        return false;
    }
	 
	 

    public boolean readResourceShiftType(ResourceShiftType resourceShiftType)  {
    	
    	if (resourceShiftType != null){
    		EntDbStaffShift edpiod = new EntDbStaffShift();
    		
    		logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
    		
//    		SimpleDateFormat RTCdf = new SimpleDateFormat(HpEKConstants.EKRTC_TIME_FORMAT);
    		// convert local time to UTC
//    		RTCdf.setTimeZone(HpEKConstants.utcTz);
    		
//    		// get condition
//    		boolean isShiftOut = false;
//    		if (resourceShiftType.getSFT().getAET() == null || "".equals(resourceShiftType.getSFT().getAET())){
//    			isShiftOut = true;
//    		}
    		
    		// check meta
    		if (resourceShiftType.getMeta() == null){
    			LOG.info("Mandatory field Meta is null, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
				
    			return false;
    		}
    		
    		// set message time
    		if (resourceShiftType.getMeta().getMessageTime() != null){
    			edpiod.setMsgSendDate(convertXMLGregorianCalendarToDate(resourceShiftType.getMeta().getMessageTime()));
    		}else {
    			LOG.info("Mandatory field message time is null, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// check messageID
    		if (resourceShiftType.getMeta().getMessageID() == 0){
    			LOG.info("Mandatory field messageID time is 0, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// check message type
    		if (resourceShiftType.getMeta().getMessageType() == null || "".equals(resourceShiftType.getMeta().getMessageType())){
    			LOG.info("Mandatory field message type is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}else if (!Arrays.asList(messageTypeArray).contains(resourceShiftType.getMeta().getMessageType())){
    			LOG.info("Mandatory field message type {} is not in possible value, ResourceShiftType message is rejected", resourceShiftType.getMeta().getMessageType());
    			
    			sendErroNotification(EnumExceptionCodes.EENUM);
    			
    			return false;
    		}
    		
    		// check messageSubtype
    		if (resourceShiftType.getMeta().getMessageSubtype() == null || "".equals(resourceShiftType.getMeta().getMessageSubtype().trim())){
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			LOG.info("Mandatory field messageSubtype is null or empty string, ResourceShiftType message is rejected");
    			return false;
    		}else if (!Arrays.asList(messageShiftSubtypeArray).contains(resourceShiftType.getMeta().getMessageSubtype().toUpperCase().trim())){
    			LOG.info("Mandatory field messageSubtype {} is not in possible value, ResourceShiftType message is rejected", resourceShiftType.getMeta().getMessageSubtype());
    			
    			sendErroNotification(EnumExceptionCodes.EENUM);
    			
    			return false;
    		}else{
    			staffShiftMsgSubType = resourceShiftType.getMeta().getMessageSubtype();
    		}
    		
    		// set messageSource
    		if (resourceShiftType.getMeta().getMessageSource() != null && !"".equals(resourceShiftType.getMeta().getMessageSource().trim())){
    			if (Arrays.asList(messageSourceArray).contains(resourceShiftType.getMeta().getMessageSource().toUpperCase().trim())){
    				switch(resourceShiftType.getMeta().getMessageSource().toUpperCase().trim()){
    					case "EKRTC":
    						edpiod.setStaffType("EK");
    						break;
    					case "GXRTC":
    						edpiod.setStaffType("GX");
    						break;
    					case "CSRTC":
    						edpiod.setStaffType("CS");
    						break;
    				}
    			}else{
    				LOG.info("Mandatory field messageSource {} is not in possible value, ResourceShiftType message is rejected", resourceShiftType.getMeta().getMessageSource());
    				
    				sendErroNotification(EnumExceptionCodes.EENUM);
    				
        			return false;
    			}
    			
    		}else {
    			LOG.info("Mandatory field messageSource is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// check SFT
    		if (resourceShiftType.getSFT() == null){
    			LOG.info("Mandatory field messageSource is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set AID
    		if (resourceShiftType.getSFT().getAID() != null && !"".equals(resourceShiftType.getSFT().getAID())){
    			edpiod.setShiftId(new BigDecimal(resourceShiftType.getSFT().getAID()));
    		}else {
    			LOG.info("Mandatory field AID is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set SNM
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getSNM() != null && !"".equals(resourceShiftType.getSFT().getSNM())){
    			edpiod.setStaffName(resourceShiftType.getSFT().getSNM());
    		}else {
    			LOG.info("Mandatory field SNM is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set FNM
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getFNM() != null && !"".equals(resourceShiftType.getSFT().getFNM())){
    			edpiod.setStaffFirstName(resourceShiftType.getSFT().getFNM());
    		}else {
    			LOG.info("Mandatory field FNM is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set SID (invalid for delete message)
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getSID() != null && !"".equals(resourceShiftType.getSFT().getSID())){
    			edpiod.setStaffNumber(resourceShiftType.getSFT().getSID());
    		}else {
    			LOG.info("Mandatory field SID is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set SST (invalid for delete message)
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType) ){
    		
    		if (resourceShiftType.getSFT().getSST() != null && !"".equals(resourceShiftType.getSFT().getSST())){
    			try {
					edpiod.setShiftScheStartDate(convertStringToDate(resourceShiftType.getSFT().getSST()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date SST, {}", e);
//					LOG.info("Mandatory field SST can not be parsed correctly, ResourceShiftType message is rejected");
//	    			return false;
				}
    		}else {
    			LOG.info("Mandatory field SST is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set SET
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getSET() != null && !"".equals(resourceShiftType.getSFT().getSET())){
    			try {
					edpiod.setShiftScheEndDate(convertStringToDate(resourceShiftType.getSFT().getSET()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date SET, {}", e);
//					LOG.info("Mandatory field SET can not be parsed correctly, ResourceShiftType message is rejected");
//	    			return false;
				}
    		}else {
    			LOG.info("Mandatory field SET is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set EST
    		if (resourceShiftType.getSFT().getEST() != null && !"".equals(resourceShiftType.getSFT().getEST())){
    			try {
					edpiod.setShiftEstStartDate(convertStringToDate(resourceShiftType.getSFT().getEST()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date EST, {}", e);
				}
    		}
//    		else if(isShiftOut){
//    			LOG.error(" field EST under 'out' condition and it is null or empty String, ResourceShiftType message is rejected");
//    			return false;
//    		}
    		
    		// set EET
    		if (resourceShiftType.getSFT().getEET() != null && !"".equals(resourceShiftType.getSFT().getEET())){
    			try {
					edpiod.setShiftEstEndDate(convertStringToDate(resourceShiftType.getSFT().getEET()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date EET, {}", e);
				}
    		}
//    		else if(isShiftOut){
//    			LOG.error(" field EET under 'out' condition and it is null or empty String, ResourceShiftType message is rejected");
//    			return false;
//    		}
    		
    		// set AST
    		if (resourceShiftType.getSFT().getAST() != null && !"".equals(resourceShiftType.getSFT().getAST().trim())){
    			try {
					edpiod.setShiftActualStartDate(convertStringToDate(resourceShiftType.getSFT().getAST()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date AST, {}", e);
//					LOG.info("Mandatory field AST can not be parsed correctly, ResourceShiftType message is rejected");
//	    			return false;
				}
    		}
//    		else {
//    			LOG.info("Mandatory field AST is null or empty String, ResourceShiftType message is rejected");
//    			
//    			sendErroNotification(EnumExceptionCodes.EMAND);
//    			
//    			return false;    			
//    		}
    		
    		// set AET
    		if (resourceShiftType.getSFT().getAET() != null && !"".equals(resourceShiftType.getSFT().getAET())){
    			try {
					edpiod.setShiftActualEndDate(convertStringToDate(resourceShiftType.getSFT().getAET()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date AET, {}", e);
				}
    		}
//    		else if(isShiftOut){
//    			LOG.error(" field EST under 'out' condition and it is null or empty String, ResourceShiftType message is rejected");
//    			return false;
//    		}
    		
    		// set STT
    		if (resourceShiftType.getSFT().getSTT() != null && !"".equals(resourceShiftType.getSFT().getSTT().trim())){
    			edpiod.setMutateCode(resourceShiftType.getSFT().getSTT());
    			if (!Arrays.asList(shiftsttArray).contains(resourceShiftType.getSFT().getSTT().toUpperCase().trim())){
        			LOG.info("Mandatory field STT {} is in possible value, ResourceShiftType message is rejected", resourceShiftType.getSFT().getSTT());
        			
        			sendErroNotification(EnumExceptionCodes.EENUM);
        			
        			return false;
    			}
    		}else {
    			LOG.info("Mandatory field STT is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    		
    		}
    		
    		// set RNO (change to conditional according to version 2)
    		if (resourceShiftType.getSFT().getRNO() != null && !"".equals(resourceShiftType.getSFT().getRNO().trim())){
    			edpiod.setStaffMobileNo(resourceShiftType.getSFT().getRNO());
    		}
//    		else {
//    			LOG.info("Mandatory field RNO is null or empty String, ResourceShiftType message is rejected");
//    			return false;    			
//    		}
    		
    		// set FNC
    		if (resourceShiftType.getSFT().getFNC() != null && !"".equals(resourceShiftType.getSFT().getFNC().trim())){
    			edpiod.setShiftFunction(resourceShiftType.getSFT().getFNC());
    		}else {
    			LOG.info("Mandatory field FNC is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		// set PDA (change to conditional according to version 2.0 )
    		if (resourceShiftType.getSFT().getPDA() != null && !"".equals(resourceShiftType.getSFT().getPDA().trim())){
    			edpiod.setStaffHhdNo(resourceShiftType.getSFT().getPDA());
    		}
//    		else {
//    			LOG.info("Mandatory field PDA is null or empty String, ResourceShiftType message is rejected");
//    			return false;    			
//    		}
    		
    		// set RNG
    		if (resourceShiftType.getSFT().getRNG() != null && !"".equals(resourceShiftType.getSFT().getRNG().trim())){
    			edpiod.setPdaRange(resourceShiftType.getSFT().getRNG());
    		}
    		
    		// set LOC
    		if (resourceShiftType.getSFT().getLOC() != null && !"".equals(resourceShiftType.getSFT().getLOC().trim())){
    			edpiod.setShiftLoc(resourceShiftType.getSFT().getLOC());
    		}
    		
    		
    		// set DEP (invaild for delete message)
    		if (!"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getDEP() != null && !"".equals(resourceShiftType.getSFT().getDEP().trim())){
    			if (entStartupInitSingleton.isMDDeptListOn()){
    			// to check master data
    			switch (edpiod.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASDeptList().contains(resourceShiftType.getSFT().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceShiftType.getSFT().getDEP().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXDeptList().contains(resourceShiftType.getSFT().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceShiftType.getSFT().getDEP().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSDeptList().contains(resourceShiftType.getSFT().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceShiftType.getSFT().getDEP().trim() ,"CS");
    				}
    				break;
    			}
    			
    			sendErroNotification(EnumExceptionCodes.WNOMD);
    			
    		}			
    			edpiod.setStaffDept(resourceShiftType.getSFT().getDEP());
    		}else {
    			LOG.info("Mandatory field DEP is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set MWA (not available for DEP = B)
    		if(!"CB".equalsIgnoreCase(resourceShiftType.getSFT().getDEP())){
    		
    		if (resourceShiftType.getSFT().getMWA() != null && !"".equals(resourceShiftType.getSFT().getMWA().trim())){
    			if (entStartupInitSingleton.isMDWorkAreasListOn()){
    			// to check with master data
      			switch (edpiod.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASWorkAreasList().contains(resourceShiftType.getSFT().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceShiftType.getSFT().getMWA().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXWorkAreasList().contains(resourceShiftType.getSFT().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceShiftType.getSFT().getMWA().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSWorkAreasList().contains(resourceShiftType.getSFT().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceShiftType.getSFT().getMWA().trim() ,"CS");
    				}
    				break;
      			}
      			
      			sendErroNotification(EnumExceptionCodes.WNOMD);
    		}	
    			edpiod.setStaffWorkArea(resourceShiftType.getSFT().getMWA());
    		}else {
    			LOG.info("Mandatory field MWA is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    			
    		}
    		
    		}
    		
    		// set RTY (not available for delete message)
    		if (staffShiftMsgSubType != null && !"DEL".equalsIgnoreCase(staffShiftMsgSubType)){
    		
    		if (resourceShiftType.getSFT().getRTY() != null && !"".equals(resourceShiftType.getSFT().getRTY().trim())){
    			edpiod.setResourceType(resourceShiftType.getSFT().getRTY());
    			if (!Arrays.asList(shiftrtyArray).contains(resourceShiftType.getSFT().getRTY().toUpperCase().trim())){
        			LOG.info("Mandatory field RTY {} is in possible value, ResourceShiftType message is rejected", resourceShiftType.getSFT().getRTY());
        			return false;
    			}
    		}else {
    			LOG.info("Mandatory field RTY is null or empty String, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;    		
    		}
    		
    		}
    		
     		// set WLK
    		if (resourceShiftType.getSFT().getWLK() != null && !"".equals(resourceShiftType.getSFT().getWLK().trim())){
    			edpiod.setStaffWalkieNo(resourceShiftType.getSFT().getWLK());
    		}
//    		else if(isShiftOut){
//    			LOG.error(" field EST under 'out' condition and it is null or empty String, ResourceShiftType message is rejected");
//    			return false;
//    		}
    		
    		
    		entDbStaffShift = (EntDbStaffShift)SerializationUtils.clone(edpiod);
    		
    		return true;
    	}else {
    		LOG.info("RTC is null, ResourceShiftType message is rejected");
    		return false;
    	}
    }

    
    
    public boolean readResourceAssignmentType(ResourceAssignmentType resourceAssignmentType)  {
    	
    	logLevel = entStartupInitSingleton.getIrmLogLev();
    	
 		msgLogged = Boolean.FALSE;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
    	
    	if (resourceAssignmentType != null){
    		EntDbFltJobAssign edpiodEntDbFltJobAssign = new EntDbFltJobAssign();
    		EntDbFltJobTask edpiodEntDbFltJobTask = new EntDbFltJobTask();
//    		EntDbStaffShift edpiodEntDbStaffShift = new EntDbStaffShift();
    		
//    		SimpleDateFormat RTCdf = new SimpleDateFormat(HpEKConstants.EKRTC_TIME_FORMAT);
    		// convert local time to UTC
//    		RTCdf.setTimeZone(HpEKConstants.utcTz);
    		
    		// check meta
    		if (resourceAssignmentType.getMeta() == null){
    			LOG.info("Mandatory field Meta is null, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set message time
    		if (resourceAssignmentType.getMeta().getMessageTime() != null){
    			edpiodEntDbFltJobAssign.setMsgSendDate(convertXMLGregorianCalendarToDate(resourceAssignmentType.getMeta().getMessageTime()));
    		}else {
    			LOG.info("Mandatory field message time is null, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// check messageID
    		if (resourceAssignmentType.getMeta().getMessageID() == 0){
    			LOG.info("Mandatory field messageID time is 0, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// check message type
    		if (resourceAssignmentType.getMeta().getMessageType() == null || "".equals(resourceAssignmentType.getMeta().getMessageType())){
    			LOG.info("Mandatory field message type is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}else if (!Arrays.asList(messageAssignmentTypeArray).contains(resourceAssignmentType.getMeta().getMessageType())){
    			LOG.info("Mandatory field message type {} is in possible value, ResourceShiftType message is rejected", resourceAssignmentType.getMeta().getMessageType());
    			
    			sendErroNotification(EnumExceptionCodes.EENUM);
    			
    			return false;
    		}
    		
    		// check messageSubtype
    		if (resourceAssignmentType.getMeta().getMessageSubtype() == null || "".equals(resourceAssignmentType.getMeta().getMessageSubtype().trim())){
    			LOG.info("Mandatory field messageSubtype is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}else if (!Arrays.asList(messageAssignmentSubtypeArray).contains(resourceAssignmentType.getMeta().getMessageSubtype().toUpperCase().trim())){
    			LOG.info("Mandatory field messageSubtype {} is in possible value, resourceAssignmentType message is rejected", resourceAssignmentType.getMeta().getMessageSubtype());
    			
    			sendErroNotification(EnumExceptionCodes.EENUM);
    			
    			return false;
    		}else {
    			resAssignMsgSubType = resourceAssignmentType.getMeta().getMessageSubtype();
    		}
    		
    		// set messageSource
    		if (resourceAssignmentType.getMeta().getMessageSource() != null && !"".equals(resourceAssignmentType.getMeta().getMessageSource().trim())){
    			if (Arrays.asList(messageSourceArray).contains(resourceAssignmentType.getMeta().getMessageSource().toUpperCase().trim())){
    				switch(resourceAssignmentType.getMeta().getMessageSource().toUpperCase().trim()){
    					case "EKRTC":
    						edpiodEntDbFltJobAssign.setStaffType("EK");
    						edpiodEntDbFltJobTask.setStaffType("EK");
    						break;
    					case "GXRTC":
    						edpiodEntDbFltJobAssign.setStaffType("GX");
    						edpiodEntDbFltJobTask.setStaffType("GX");
    						break;
    					case "CSRTC":
    						edpiodEntDbFltJobAssign.setStaffType("CS");
    						edpiodEntDbFltJobTask.setStaffType("CS");
    						break;
    				}
    			}else{
    				LOG.info("Mandatory field messageSource {} is not in possible value, resourceAssignmentType message is rejected", resourceAssignmentType.getMeta().getMessageSource());
    				
    				sendErroNotification(EnumExceptionCodes.EENUM);
    				
        			return false;
    			}
    			
    		}else {
    			LOG.info("Mandatory field messageSource is null or empty string, ResourceShiftType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set TSK
    		if (resourceAssignmentType.getTSK() == null ){
    			LOG.info("Mandatory field messageSource is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set AID
    		if (resourceAssignmentType.getTSK().getAID() != null && !"".equals(resourceAssignmentType.getTSK().getAID().trim())){
    			edpiodEntDbFltJobTask.setTaskId(new BigDecimal(resourceAssignmentType.getTSK().getAID()));
    		}else {
    			LOG.info("Mandatory field AID is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set TRG (not mandatory for ORI = 0)
    		if (resourceAssignmentType.getTSK().getTRG() != null && !"".equals(resourceAssignmentType.getTSK().getTRG().trim())){
    			edpiodEntDbFltJobTask.setTaskRuleGrp(resourceAssignmentType.getTSK().getTRG());
    		}else {
    			if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
    			LOG.info("Mandatory field TRG is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    			}
    		}
    		
    		// set TRQ (not mandatory for ORI = 0)
    		if (resourceAssignmentType.getTSK().getTRQ() != null && !"".equals(resourceAssignmentType.getTSK().getTRQ().trim())){
    			if (entStartupInitSingleton.isMDTaskReqtListOn()){
    			// to check with master data
      			switch (edpiodEntDbFltJobTask.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASTaskReqtList().contains(resourceAssignmentType.getTSK().getTRQ().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRQ().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXTaskReqtList().contains(resourceAssignmentType.getTSK().getTRQ().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRQ().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSTaskReqtList().contains(resourceAssignmentType.getTSK().getTRQ().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRQ().trim() ,"CS");
    				}
    				break;
      			}
      			
      			sendErroNotification(EnumExceptionCodes.WNOMD);
      			
    		}	
    			edpiodEntDbFltJobTask.setTaskRequirementId(resourceAssignmentType.getTSK().getTRQ());
    		}else {
    			if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
    			LOG.info("Mandatory field TRQ is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    			}
    		}
    		
    		// set TRD
    		if (resourceAssignmentType.getTSK().getTRD() != null && !"".equals(resourceAssignmentType.getTSK().getTRD().trim())){
    			if (entStartupInitSingleton.isMDTaskReqtListOn()){
    			// to check with master data
      			switch (edpiodEntDbFltJobTask.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASTaskReqtList().contains(resourceAssignmentType.getTSK().getTRD().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRD().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXTaskReqtList().contains(resourceAssignmentType.getTSK().getTRD().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRD().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSTaskReqtList().contains(resourceAssignmentType.getTSK().getTRD().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskReqtId {}", resourceAssignmentType.getTSK().getTRD().trim() ,"CS");
    				}
    				break;
      				}
      			
      			sendErroNotification(EnumExceptionCodes.WNOMD);
      			
    			}	
    			edpiodEntDbFltJobTask.setTaskDeptId(resourceAssignmentType.getTSK().getTRD());
    		}
    		
    		// set DEP
    		if (resourceAssignmentType.getTSK().getDEP() != null && !"".equals(resourceAssignmentType.getTSK().getDEP().trim())){
    			if (entStartupInitSingleton.isMDDeptListOn()){
    			// to check master data
    			switch (edpiodEntDbFltJobTask.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASDeptList().contains(resourceAssignmentType.getTSK().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getDEP().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXDeptList().contains(resourceAssignmentType.getTSK().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getDEP().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSDeptList().contains(resourceAssignmentType.getTSK().getDEP().trim())){
    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getDEP().trim() ,"CS");
    				}
    				break;    				
    				}	
    			
    			sendErroNotification(EnumExceptionCodes.WNOMD);
    			
    			}
    			edpiodEntDbFltJobTask.setTaskDeptId(resourceAssignmentType.getTSK().getDEP());
    		}else {
    			LOG.info("Mandatory field DEP is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set TYP
    		if (resourceAssignmentType.getTSK().getTYP() != null && !"".equals(resourceAssignmentType.getTSK().getTYP().trim())){
//    			// to check master data
//    			switch (edpiodEntDbFltJobTask.getStaffType()){
//    			case "EK":
//    				if (!entStartupInitSingleton.getEKASTaskTypesList().contains(resourceAssignmentType.getTSK().getTYP().trim())){
//    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getTYP().trim() ,"EKAS");
//    				}
//    				break;
//    			case "GX":
//    				if (!entStartupInitSingleton.getGXTaskTypesList().contains(resourceAssignmentType.getTSK().getTYP().trim())){
//    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getTYP().trim() ,"GX");
//    				}
//    				break;
//    			case "CS":
//    				if (!entStartupInitSingleton.getCSTaskTypesList().contains(resourceAssignmentType.getTSK().getTYP().trim())){
//    					LOG.warn("not matching the master data, staffType {}, deptId {}", resourceAssignmentType.getTSK().getTYP().trim() ,"CS");
//    				}
//    				break;    				
//    			}	
    			edpiodEntDbFltJobTask.setTaskType(resourceAssignmentType.getTSK().getTYP());
    		}else {
    			LOG.info("Mandatory field TYP is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
//    		// added according icd 2.0 and Mei's design, when ORI = 0 not related to flight
//    		if ("0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
//    			LOG.info("ORI = 0, resourceAssignmentType message is rejected");
//    			return false;
//    		}
    		
    		// set ORI
    		if (resourceAssignmentType.getTSK().getORI() != null && !"".equals(resourceAssignmentType.getTSK().getORI().trim())){
    			if (entStartupInitSingleton.isMDTaskGrptypeListOn()){
    			// to check master data
    			switch (edpiodEntDbFltJobTask.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASTaskGrptypeList().contains(resourceAssignmentType.getTSK().getORI().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getORI().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXTaskGrptypeList().contains(resourceAssignmentType.getTSK().getORI().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getORI().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSTaskGrptypeList().contains(resourceAssignmentType.getTSK().getORI().trim())){
    					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getORI().trim() ,"CS");
    				}
    				break;    				
    			}	
    			
    			sendErroNotification(EnumExceptionCodes.WNOMD);
    			
    			}
    			
    			edpiodEntDbFltJobTask.setTaskGrpType(resourceAssignmentType.getTSK().getORI());
    		}else {
    			LOG.info("Mandatory field ORI is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set SNO
    		if (resourceAssignmentType.getTSK().getSNO() != 0){
    			edpiodEntDbFltJobTask.setTaskNum(new BigDecimal(resourceAssignmentType.getTSK().getSNO()));
    		}else {
    			LOG.info("Mandatory field SNO is 0, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set MNO
    		if (resourceAssignmentType.getTSK().getMNO() != 0){
    			edpiodEntDbFltJobTask.setMainTaskNum(new BigDecimal(resourceAssignmentType.getTSK().getMNO()));
    		}else {
    			LOG.info("Mandatory field MNO is 0, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set RMK
    		if (resourceAssignmentType.getTSK().getRMK() != null && !"".equals(resourceAssignmentType.getTSK().getRMK().trim())){
    			edpiodEntDbFltJobTask.setTaskRemark(resourceAssignmentType.getTSK().getRMK());
    		}
    		
    		// set STI
    		if (resourceAssignmentType.getTSK().getSTI() != null && !"".equals(resourceAssignmentType.getTSK().getSTI())){
    			try {
    				edpiodEntDbFltJobTask.setTaskActualStartDate(convertStringToDate(resourceAssignmentType.getTSK().getSTI()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date STI, {}", e);
//					LOG.info("Mandatory field STI can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}
    		
    		// set ETI
    		if (resourceAssignmentType.getTSK().getETI() != null){
    			try {
    				edpiodEntDbFltJobTask.setTaskActualEndDate(convertStringToDate(resourceAssignmentType.getTSK().getETI()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date ETI, {}", e);
//					LOG.info("Mandatory field ETI can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}
    		
    		// set DUR
    		if (resourceAssignmentType.getTSK().getDUR() != null && resourceAssignmentType.getTSK().getDUR() != 0){
    			edpiodEntDbFltJobTask.setTaskActualDuration(new BigDecimal(resourceAssignmentType.getTSK().getDUR()));
    		}
    		
    		// set EST
    		if (resourceAssignmentType.getTSK().getEST() != null && !"".equals(resourceAssignmentType.getTSK().getEST() )){
    			try {
    				edpiodEntDbFltJobTask.setTaskPlanStartDate(convertStringToDate(resourceAssignmentType.getTSK().getEST()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date DUR, {}", e);
//					LOG.info("Mandatory field EST can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}else {
    			LOG.info("Mandatory field EST is null, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set LET
    		if (resourceAssignmentType.getTSK().getLET() != null && !"".equals(resourceAssignmentType.getTSK().getLET().trim())){
    			try {
    				edpiodEntDbFltJobTask.setTaskPlanEndDate(convertStringToDate(resourceAssignmentType.getTSK().getLET()));
    				// task_plan_duration = task_plan_end_date - task_plan_start_date
    				int days = Days.daysBetween(new DateTime(edpiodEntDbFltJobTask.getTaskPlanStartDate()), new DateTime(edpiodEntDbFltJobTask.getTaskPlanEndDate())).getDays();
    				int seconds = days * 24 * 60 * 60;
    				edpiodEntDbFltJobTask.setTaskPlanDuration(new BigDecimal(seconds));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date LET, {}", e);
//					LOG.info("Mandatory field LET can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}else {
    			LOG.info("Mandatory field LET is null, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set SLO
    		if (resourceAssignmentType.getTSK().getSLO() != null && !"".equals(resourceAssignmentType.getTSK().getSLO().trim())){
    			edpiodEntDbFltJobTask.setTaskStartLoc(resourceAssignmentType.getTSK().getSLO());
    		}else {
    			LOG.info("Mandatory field SLO is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set ELO
    		if (resourceAssignmentType.getTSK().getELO() != null && !"".equals(resourceAssignmentType.getTSK().getELO().trim())){
    			edpiodEntDbFltJobTask.setTaskEndLoc(resourceAssignmentType.getTSK().getELO());
    		}else {
    			LOG.info("Mandatory field ELO is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set HST
    		if (resourceAssignmentType.getTSK().getHST() != null && !"".equals(resourceAssignmentType.getTSK().getHST().trim())){
    			edpiodEntDbFltJobTask.setTaskHandlingState(resourceAssignmentType.getTSK().getHST());
    		}else {
    			LOG.info("Mandatory field HST is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set MWA (not available for DEP =CB)
    		if (!"CB".equalsIgnoreCase(resourceAssignmentType.getTSK().getDEP())){
    		
    		if (resourceAssignmentType.getTSK().getMWA() != null && !"".equals(resourceAssignmentType.getTSK().getMWA().trim())){
    			if (entStartupInitSingleton.isMDWorkAreasListOn()){
    			// to check with master data
      			switch (edpiodEntDbFltJobTask.getStaffType()){
    			case "EK":
    				if (!entStartupInitSingleton.getEKASWorkAreasList().contains(resourceAssignmentType.getTSK().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getMWA().trim() ,"EKAS");
    				}
    				break;
    			case "GX":
    				if (!entStartupInitSingleton.getGXWorkAreasList().contains(resourceAssignmentType.getTSK().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getMWA().trim() ,"GX");
    				}
    				break;
    			case "CS":
    				if (!entStartupInitSingleton.getCSWorkAreasList().contains(resourceAssignmentType.getTSK().getMWA().trim())){
    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getMWA().trim() ,"CS");
    				}
    				break;
    			}
      			
      			sendErroNotification(EnumExceptionCodes.WNOMD);
      			
    			}
    			edpiodEntDbFltJobTask.setTaskWorkArea(resourceAssignmentType.getTSK().getMWA());
    		}
//    		else {
//    			LOG.info("Mandatory field MWA is null or empty string, resourceAssignmentType message is rejected");
//    			return false;
//    		}
    		}
    		
    		// set SWA
    		if (resourceAssignmentType.getTSK().getSWA() != null && !"".equals(resourceAssignmentType.getTSK().getSWA().trim())){
    			edpiodEntDbFltJobTask.setTaskSubworkArea(resourceAssignmentType.getTSK().getSWA());
    		}
    		
    		// set WLD
    		if (resourceAssignmentType.getTSK().getWLD() != null && resourceAssignmentType.getTSK().getWLD() != 0){
    			edpiodEntDbFltJobTask.setTaskWorkloadFactor(new BigDecimal(resourceAssignmentType.getTSK().getWLD()));
    		}
    		
    		// set PRI
    		if (resourceAssignmentType.getTSK().getPRI() != 0){
    			edpiodEntDbFltJobTask.setTaskPriority(new BigDecimal(resourceAssignmentType.getTSK().getPRI()));
    		}
    		
    		// Attention ! make use of this info to search flight in AFTTAB (AFTTAB.ACT3 or ACT5)
    		//  AFT
//    		if (resourceAssignmentType.getTSK().getAFT() != null && !"".equals(resourceAssignmentType.getTSK().getAFT().trim())){
//    		}
    		
       		// set STS
    		if (resourceAssignmentType.getTSK().getSTS() != null && !"".equals(resourceAssignmentType.getTSK().getSTS().trim())){
//      			// to check with master data
//      			switch (edpiodEntDbFltJobTask.getStaffType()){
//    			case "EK":
//    				if (!entStartupInitSingleton.getEKASTaskStatusList().contains(resourceAssignmentType.getTSK().getSTS().trim())){
//    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getSTS().trim() ,"EKAS");
//    				}
//    				break;
//    			case "GX":
//    				if (!entStartupInitSingleton.getGXTaskStatusList().contains(resourceAssignmentType.getTSK().getSTS().trim())){
//    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getSTS().trim() ,"GX");
//    				}
//    				break;
//    			case "CS":
//    				if (!entStartupInitSingleton.getCSTaskStatusList().contains(resourceAssignmentType.getTSK().getSTS().trim())){
//    					LOG.warn("not matching the master data, staffType {}, workArea {}", resourceAssignmentType.getTSK().getSTS().trim() ,"CS");
//    				}
//    				break;
//    			}	
    			edpiodEntDbFltJobTask.setTaskStatus(resourceAssignmentType.getTSK().getSTS());
    		}else {
    			LOG.info("Mandatory field STS is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// set STT
    		if (resourceAssignmentType.getTSK().getSTT() != null && !"".equals(resourceAssignmentType.getTSK().getSTT().trim())){
    			if (entStartupInitSingleton.isMDTaskGrptypeListOn()){
  			// to check with master data
  			switch (edpiodEntDbFltJobTask.getStaffType()){
			case "EK":
				if (!entStartupInitSingleton.getEKASTaskGrptypeList().contains(resourceAssignmentType.getTSK().getSTT().trim())){
					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getSTT().trim() ,"EKAS");
				}
				break;
			case "GX":
				if (!entStartupInitSingleton.getGXTaskGrptypeList().contains(resourceAssignmentType.getTSK().getSTT().trim())){
					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getSTT().trim() ,"GX");
				}
				break;
			case "CS":
				if (!entStartupInitSingleton.getCSTaskGrptypeList().contains(resourceAssignmentType.getTSK().getSTT().trim())){
					LOG.warn("not matching the master data, staffType {}, taskGrptypeId {}", resourceAssignmentType.getTSK().getSTT().trim() ,"CS");
				}
				break;
  				}	
  			
  			sendErroNotification(EnumExceptionCodes.WNOMD);
  			
    		}
    			edpiodEntDbFltJobTask.setSubtaskStatus(resourceAssignmentType.getTSK().getSTT());
    		}else {
    			LOG.info("Mandatory field STT is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		// Attention ! Use this info to form the full flight number (AFTTAB.FLNO)
    		// set DNO
//    		if (resourceAssignmentType.getTSK().getDNO() != null && !"".equals(resourceAssignmentType.getTSK().getDNO().trim())){
//    			// TO CONTINUE>>>>>>
//    		}
    		
    		// Make use of this info to search flight in AFTTAB Convert to UTC prior to search (AFTTAB.STOD)
    		// set DFD
//    		if (resourceAssignmentType.getTSK().getDFD() != null){
//    			// TO CONTINUE>>>>>>
//    		}
    		
    		// Make use of this info to search flight in AFTTAB (AFTTAB.ORG3)
    		// set REG
//    		if (resourceAssignmentType.getTSK().getREG() != null && !"".equals(resourceAssignmentType.getTSK().getREG().trim())){
//    			// TO CONTINUE>>>>>>
//    		}

    		
    		// Make use of this info to search flight in AFTTAB (AFTTAB.ORG3)
       		// check DSN (not mandatory for ORI = 0)
    		if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
    		
    		if (resourceAssignmentType.getTSK().getDSN() == null || "".equals(resourceAssignmentType.getTSK().getDSN().trim())){
    			LOG.info("Mandatory field DSN is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		}
    		
    		// Use this info to form the full flight number (AFTTAB.FLNO)
       		// check ASN (not mandatory for ORI = 0)
    		if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
    		
    		if (resourceAssignmentType.getTSK().getASN() == null || "".equals(resourceAssignmentType.getTSK().getASN().trim())){
    			LOG.info("Mandatory field ASN is null or empty string, resourceAssignmentType message is rejected");
    			
    			sendErroNotification(EnumExceptionCodes.EMAND);
    			
    			return false;
    		}
    		
    		}
    		
    		// set SID (chnage to conditional according to version 2)
    		if (resourceAssignmentType.getTSK().getSID() != null && !"".equals(resourceAssignmentType.getTSK().getSID().trim())){
    			edpiodEntDbFltJobAssign.setShiftId( new BigDecimal(resourceAssignmentType.getTSK().getSID()));
    		}
//    		else {
//    			LOG.info("Mandatory field SID is null or empty string, resourceAssignmentType message is rejected");
//    			return false;
//    		}
    		
       		// set PRF
    		if (resourceAssignmentType.getTSK().getPRF() != null && !"".equals(resourceAssignmentType.getTSK().getPRF().trim())){
    			entDbFltJobAssign.setPaxRefNum(resourceAssignmentType.getTSK().getPRF());
    		}
    		
       		// set SLT
    		if (resourceAssignmentType.getTSK().getSLT() != null && !"".equals(resourceAssignmentType.getTSK().getSLT().trim())){
    			edpiodEntDbFltJobTask.setTaskStartLocType(resourceAssignmentType.getTSK().getSLT());
    		}
    		
//    		// Make use of this info to search flight in AFTTAB (AFTTAB.STOA)
//    		// set STA (not mandatory for ORI = 0)
//    		if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
//    		if (resourceAssignmentType.getTSK().getSTA() != null){
//    			// TO CONTINUE>>>>>>
//
//    			try {
//    				edpiodEntDbFltJobTask.setconvertStringToDate(resourceAssignmentType.getTSK().getDFD()));
//				} catch (ParseException e) {
//					LOG.error("erro when parse String to Date, {}", e);
//					LOG.info("Mandatory field DFD can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
//				}
//    		}
//    		
//    		}
    		
    		// set AST
    		if (resourceAssignmentType.getTSK().getAST() != null && !"".equals(resourceAssignmentType.getTSK().getAST().trim())){
    			try {
    				edpiodEntDbFltJobTask.setTaskAssignStartDate(convertStringToDate(resourceAssignmentType.getTSK().getAST()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date AST, {}", e);
//					LOG.info("Mandatory field AST can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}
    		
    		// set AET
    		if (resourceAssignmentType.getTSK().getAET() != null && !"".equals(resourceAssignmentType.getTSK().getAET())){
    			try {
    				edpiodEntDbFltJobTask.setTaskAssignEndDate(convertStringToDate(resourceAssignmentType.getTSK().getAET()));
				} catch (ParseException e) {
					LOG.error("erro when parse String to Date AET, {}", e);
//					LOG.info("Mandatory field AET can not be parsed correctly, resourceAssignmentType message is rejected");
//	    			return false;
				}
    		}
    		
    		// Use this info to form the full flight number (AFTTAB.ALC2 or ALC3)
    		// AAL (not mandatory for ORI = 0)
    		if (resourceAssignmentType.getTSK().getAAL() == null || "".equals(resourceAssignmentType.getTSK().getAAL().trim())){
//    			edpiodEntDbFltJobTask.setTaskStatus(resourceAssignmentType.getTSK().getAAL());
    			// TO CONTINUE>>>>
        			if (!"0".equalsIgnoreCase(resourceAssignmentType.getTSK().getORI())){
        			LOG.info("Mandatory field TRQ is null or empty string, resourceAssignmentType message is rejected");
        			
        			sendErroNotification(EnumExceptionCodes.EMAND);
        			
        			return false;
        			}
    		}
    		
    		
      		// DPN
    		if (resourceAssignmentType.getTSK().getDPN() != null && !"".equals(resourceAssignmentType.getTSK().getDPN().trim())){
    			edpiodEntDbFltJobTask.setTaskDeptName(resourceAssignmentType.getTSK().getDPN());
    		}
    		
    		// to search flight and set id_flight  
    		String org3 = resourceAssignmentType.getTSK().getDSN();
    	    String des3 = resourceAssignmentType.getTSK().getASN();
//    	    int idFlight = 0;
    	    	    
    	    DateFormat afttabDf = new SimpleDateFormat("yyyyMMddHHmmss");
    	    // get stoa 
    	    stoa = resourceAssignmentType.getTSK().getSTA();
    	    // get stod 
    	    stod = resourceAssignmentType.getTSK().getDFD();
    	    
    	    try {
    	    	if (stoa != null && !"".equals(stoa))
				stoa = afttabDf.format(convertStringToDate(stoa));
    	    	if (stod != null && !"".equals(stod))
				stod = afttabDf.format(convertStringToDate(stod));
			} catch (ParseException e) {
				LOG.error("date format convertion erro, {}", e);
			}
    	    
    	    
    	    // get flno
    	    String airlineNum; 
    	    String airlineCode = resourceAssignmentType.getTSK().getAAL();
    	    if (resourceAssignmentType.getTSK().getDNO() != null && !"".equals(resourceAssignmentType.getTSK().getDNO())){
    	    	airlineNum = resourceAssignmentType.getTSK().getDNO();
    	    } else{
    	    	airlineNum = resourceAssignmentType.getTSK().getANO();
    	    }
//    	    flno = getFlnoWith(airlineCode, airlineNum, null);
    	    flno = HpUfisUtils.formatCedaFlno(airlineCode, HpUfisUtils.formatCedaFltn(airlineNum), null);

    	    // get adid
    	    if ("DXB".equalsIgnoreCase(org3) && "DXB".equalsIgnoreCase(des3)){
    	    	adid = "B";
    	    }else if ("DXB".equalsIgnoreCase(org3)){
    	    	adid = "D";
    	    }else if ("DXB".equalsIgnoreCase(des3)){
    	    	adid = "A";
    	    }
//    	    else{
//    	    	LOG.info("nither ASN and DSN contain 'DXB, EKRTC msg will be rejected'");
//    	    	
//    	    	sendErroNotification(EnumExceptionCodes.EENUM);
//    	    	
//    	    	return false;
//    	    }
    		
//    	    edpiodEntDbFltJobAssign.setIdFlight(idFlight);
//    	    edpiodEntDbFltJobTask.setIdFlight(new BigDecimal(idFlight));
//    	    edpiodEntDbStaffShift.setIdFlight(new BigDecimal(idFlight));
//    		
    		
    		entDbFltJobAssign = (EntDbFltJobAssign)SerializationUtils.clone(edpiodEntDbFltJobAssign);
    		entDbFltJobTask = (EntDbFltJobTask)SerializationUtils.clone(edpiodEntDbFltJobTask);
//    		entDbStaffShift = (EntDbStaffShift)SerializationUtils.clone(edpiodEntDbStaffShift);
    		
    		
    		// set flightSaerchObject
    		entRTCFlightSearchObject = new EntRTCFlightSearchObject();
    		entRTCFlightSearchObject.setAdid(adid);
    		entRTCFlightSearchObject.setDes3(des3);
    		entRTCFlightSearchObject.setOrg3(org3);
    		entRTCFlightSearchObject.setFlno(flno);
    		entRTCFlightSearchObject.setStoa(stoa);
    		entRTCFlightSearchObject.setStod(stod);
    		entRTCFlightSearchObject.setOri(resourceAssignmentType.getTSK().getORI());
    		entRTCFlightSearchObject.setRegn(resourceAssignmentType.getTSK().getREG());
    		
    		
    		return true;

    	}else {
    		LOG.info("RTC is null, ResourceShiftType message is rejected");
    		return false;
    	}
    }

    
	private Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar calendar) {
		Date result = null;
		if (calendar != null) {

			HpUfisCalendar cal = new HpUfisCalendar(calendar);
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			DateFormat df2 = new SimpleDateFormat("yyyyMMddHHmmss");
			df.setTimeZone(HpEKConstants.utcTz);
			String date = df.format(cal.getTime());
			try {
				result = df2.parse(date);
			} catch (ParseException e) {
				LOG.debug("timezone convertion erro" + e);
			}
		}

		return result;

	}
	
	private static Date convertStringToDate(String dateTimeString) throws ParseException {
		  Date result = null;
		  DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		  DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//		  try {
		   if (dateTimeString != null) {
		    LOG.info("df.parse:" + df.parse(dateTimeString));
		    HpUfisCalendar cal = new HpUfisCalendar(df.parse(dateTimeString));

		    df.setTimeZone(HpEKConstants.utcTz);
		    String date = df.format(cal.getTime());
		    LOG.info("(cal):" + cal.getTime());
		    LOG.info("df.format(cal):" + date);
		    LOG.info("df2.parse:" + df2.parse(date));
		    result = df2.parse(date);
		   }
//		  } catch (Exception e) {
//		   LOG.debug("timezone convertion error" + e);
//		  }
		  return result;

		 }
	
	
//	// convert to ufis flno
//	private String getFlnoWith(String airlineCode, String airlineNum, String suffix){
//		String result = null;
//		
//		if (airlineCode != null && airlineNum != null){
//			if(airlineNum.trim().length() < 5){
//			for (int i = 0; i < (5 - airlineNum.trim().length()); i++){
//				airlineNum = airlineNum.trim() + " ";
//			}
//			}
//			
//			if (airlineCode.trim().length() == 2){
//				result = airlineCode.trim() + " " + airlineNum.trim();
//			}else if (airlineCode.trim().length() == 3){
//				result = airlineCode.trim() + airlineNum.trim();
//			}
//			
//			if (suffix != null && !"".equals(suffix)){
//				result = result + suffix;
//			}
//		}
//		
//		return result;
//	}

//	public String getFlno() {
//		return flno;
//	}
//
//	public void setFlno(String flno) {
//		this.flno = flno;
//	}
//
//	public String getStod() {
//		return stod;
//	}
//
//	public void setStod(String stod) {
//		this.stod = stod;
//	}
//
//	public String getStoa() {
//		return stoa;
//	}
//
//	public void setStoa(String stoa) {
//		this.stoa = stoa;
//	}
//
//	public String getAdid() {
//		return adid;
//	}
//
//	public void setAdid(String adid) {
//		this.adid = adid;
//	}

	public String getStaffShiftMsgSubType() {
		return staffShiftMsgSubType;
	}

	public void setStaffShiftMsgSubType(String staffShiftMsgSubType) {
		this.staffShiftMsgSubType = staffShiftMsgSubType;
	}

	public String getResAssignMsgSubType() {
		return resAssignMsgSubType;
	}

	public void setResAssignMsgSubType(String resAssignMsgSubType) {
		this.resAssignMsgSubType = resAssignMsgSubType;
	}

	public EntDbStaffShift getEntDbStaffShift() {
		return entDbStaffShift;
	}

	public void setEntDbStaffShift(EntDbStaffShift entDbStaffShift) {
		this.entDbStaffShift = entDbStaffShift;
	}

	public EntDbFltJobAssign getEntDbFltJobAssign() {
		return entDbFltJobAssign;
	}

	public void setEntDbFltJobAssign(EntDbFltJobAssign entDbFltJobAssign) {
		this.entDbFltJobAssign = entDbFltJobAssign;
	}

	public EntDbFltJobTask getEntDbFltJobTask() {
		return entDbFltJobTask;
	}

	public void setEntDbFltJobTask(EntDbFltJobTask entDbFltJobTask) {
		this.entDbFltJobTask = entDbFltJobTask;
	}

	public String getxmlMsg() {
		return xmlMsg;
	}

	public void setxmlMsg(String xmlMsg) {
		this.xmlMsg = xmlMsg;
	}

	

//	public EntStartupInitSingleton getEntStartupInitSingleton() {
//		return entStartupInitSingleton;
//	}
//
//
//	public void setEntStartupInitSingleton(
//			EntStartupInitSingleton entStartupInitSingleton) {
//		this.entStartupInitSingleton = entStartupInitSingleton;
//	}

//	public InterfaceConfig get_interfaceConfig() {
//		return _interfaceConfig;
//	}
//
//	public void set_interfaceConfig(InterfaceConfig _interfaceConfig) {
//		this._interfaceConfig = _interfaceConfig;
//	}

//	public Marshaller get_ms() {
//		return _ms;
//	}
//
//	public void set_ms(Marshaller _ms) {
//		this._ms = _ms;
//	}
//
//	public Unmarshaller get_um() {
//		return _um;
//	}
//
//	public void set_um(Unmarshaller _um) {
//		this._um = _um;
//	}

	public Message getRawMsg() {
		return rawMsg;
	}


	public void setRawMsg(Message rawMsg) {
		this.rawMsg = rawMsg;
	}
	
	


	public EntRTCFlightSearchObject getEntRTCFlightSearchObject() {
		return entRTCFlightSearchObject;
	}


	public void setEntRTCFlightSearchObject(
			EntRTCFlightSearchObject entRTCFlightSearchObject) {
		this.entRTCFlightSearchObject = entRTCFlightSearchObject;
	}


	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString());
		dataList.add(HpEKConstants.EKRTC_DATA_SOURCE);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, HpEKConstants.EKRTC_DATA_SOURCE);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

	}
	
	
	private void sendErroNotification(EnumExceptionCodes eec){
		
		if (logLevel
				.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
						.name())) {
			if (!msgLogged) {
				irmtabRef = _irmfacade.storeRawMsg(rawMsg, HpEKConstants.EKRTC_DATA_SOURCE);
				msgLogged = Boolean.TRUE;
			}
		
	
		if (irmtabRef > 0) {
			try {
				sendErrInfo(eec, irmtabRef);
			} catch (JsonGenerationException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (JsonMappingException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (IOException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			}
		}
		
	}
		
	}
	


}
