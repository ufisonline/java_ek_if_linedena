package com.ufis_as.ufisapp.ek.intf.iams;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.iams.entities.EntDbCrewImmigration;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlCrewImmigrationBean;
import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.iams.CrewStatusDetails;

/**
 * @author BTR
 * @version 1:		2013-07-24 BTR - Released new version to read from the external config.
 * @version 1.1.0	2013-10-14 BTR - Modified for internal exception handling to UFISDATASTORE
 * @version 1.1.1   2013-12-11 DMO - Standardize notification process
 * @version 1.1.2	2014-01-07 BTR - Changed to use common method to get the flight number(formatCedaFlno())
 * 									- Changed default value of REC_STATUS = " "
 * 									- Modified to query flight job assign with staff_Type = :staff_Type AND staff_type_code = "CREW" 
 * 									- Add checking for Enum Gate Type (EGATE or EXIT_GATE)		
 *  @version 1.1.3	2014-01-13 BTR	- change to use encryption on the tibco connection password
 *  								- added backward update for IAMS.
 *  @version 1.1.4	2014-01-20 BTR	- Added new column STATUSCHANGE_DATE in crew_immigration for the statusChangeDateDTM
 */
@Stateless
public class BlHandleCrewImmigrationBean  extends BaseDTO{

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleCrewImmigrationBean.class);
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private DlCrewImmigrationBean clsDlCrewImmigration;
	@EJB
	private BlIrmtabFacade clsBlIrmtabFacade;
	@EJB
	private IDlFlighttJobAssignLocal clsIDlFltJobAssignLocal;
	/*@EJB
    BlUfisBCTopic clsBlUfisBCTopic;*/
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
	DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	DateFormat sfDate = new SimpleDateFormat("yyyyMMdd");
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(CrewStatusDetails.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
			
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	
	public boolean processCrewImmigration(String message, long irmtabRef){
	  	// clear
    	data.clear();
    	
    	// set urno of irmtab
    	irmtabUrno = irmtabRef;
		try {
			CrewStatusDetails _input = (CrewStatusDetails) _um.unmarshal(new StreamSource(new StringReader(message)));
			
			if(HpUfisUtils.isNullOrEmptyStr(_input.getArrOrDep())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getOriginator())){
				LOG.error("Input Crew Status details mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}else{
				if(!"D".equals(_input.getArrOrDep())){
					LOG.error("Processing performs only for Departure. Message dropped.");
					//clsBlIrmtabFacade.updateIRMStatus("NotDepFlt");
					addExptInfo(EnumExceptionCodes.ENDEP.name(), _input.getArrOrDep());
					return false;
				}
				if(!"IAM".equals(_input.getOriginator())){
					LOG.error("Processing performs only for IAM Originator. Message dropped.");
					//clsBlIrmtabFacade.updateIRMStatus("UnknownSrc");
					addExptInfo(EnumExceptionCodes.EWSRC.name(), _input.getOriginator());
					return false;
				}				
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getCxCd())
					|| _input.getFlightID().getFltDate() == null
					|| HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getFltNum())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getArrStn())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getDepStn())
					|| HpUfisUtils.isNullOrEmptyStr(String.valueOf(_input.getFlightID().getDepNum()))){
				LOG.error("Input Flight ID info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(!_input.getFlightID().getCxCd().contains("EK")){
				LOG.error("Processing performs only for EK flight. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWALC.name(), _input.getFlightID().getCxCd());
				return false;
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getCrewDetails().getStaffNo())
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCrewDetails().getCrewType())){
				LOG.error("Input Crew detail mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(!"C".equals(_input.getCrewDetails().getCrewType())
					&& !"F".equals(_input.getCrewDetails().getCrewType())){
				LOG.error("Crew type must be F(Flight Crew) or C(Cabin Crew). Message dropped.");
				addExptInfo(EnumExceptionCodes.EENUM.name(), _input.getCrewDetails().getCrewType());
				return false;
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getCrewStatusInfo().getStatus())
					|| _input.getCrewStatusInfo().getStatusChageDTM() == null
					|| HpUfisUtils.isNullOrEmptyStr(_input.getCrewStatusInfo().getGateDetails().getGateType())){
				LOG.error("Input Crew status mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			
			if(!"EGATE".equals(_input.getCrewStatusInfo().getGateDetails().getGateType().trim())
					&& !"EXIT_GATE".equals(_input.getCrewStatusInfo().getGateDetails().getGateType().trim())){
				LOG.error("Gate Type is not either EGATE or EXIT_GATE. Message dropped.");
				addExptInfo(EnumExceptionCodes.EENUM.name(), _input.getCrewStatusInfo().getGateDetails().getGateType());
				return false;
			}
			
			String flNum = null;
			Date flDate = null;
			BigDecimal urno = null;
			EntDbAfttab entFlight = null;
			String idGateType = "0";
			String idFltJobAssign = "0";
			Date statusChangeDtm = null;
			String crewActivityFlag = "";
			String fltSuffix = null;
			//List<EntDbMdGateType> gateTypeList = new ArrayList<>();
			EntDbFltJobAssign entFltJobAssign = null;
			String cmd = UfisASCommands.IRT.name(); 
			//sfDate.setTimeZone(HpEKConstants.utcTz);
			
			if(_input.getCrewStatusInfo().getStatus().contains("FAILURE"))
				crewActivityFlag = "F";
			else if(_input.getCrewStatusInfo().getStatus().contains("SUCCESS"))
					crewActivityFlag = "P";
			else{
				LOG.error("Input Crew status Enumeration value is incorrect. Message dropped.");
				addExptInfo(EnumExceptionCodes.EENUM.name(), _input.getCrewStatusInfo().getStatus());
				return false;
			}
			fltSuffix = (_input.getFlightID().getFltSfx() == null)? "": _input.getFlightID().getFltSfx();
			
			//format ceda flight string
			/*flNum = _input.getFlightID().getCxCd() +" "+
						HpUfisUtils.formatCedaFltn(_input.getFlightID().getFltNum()) + fltSuffix;*/
			flNum = HpUfisUtils.formatCedaFlno(_input.getFlightID().getCxCd(), _input.getFlightID().getFltNum(), fltSuffix);
			
			//convert to ceda flight time to UTC
			flDate = _input.getFlightID().getFltDate().toGregorianCalendar().getTime();
			/*LOG.info("Flight Date:"+flDate);
			LOG.info("Formatted Flight Date:"+sfDate.format(flDate));*/
			//perform only for deptStn = DXB
			if (HpEKConstants.EK_HOPO.equals(_input.getFlightID().getDepStn()))
				entFlight = clsAfttabBeanLocal.findFlightByFlut(flNum, sfDate.format(flDate), _input.getFlightID().getArrStn());
			else{
				LOG.error("Processing performs only for departure station = DXB. Message dropped.");
				addExptInfo(EnumExceptionCodes.ENDEP.name(), _input.getFlightID().getDepStn());
				return false;
			}
			
			if(entFlight == null){
				//urno = new BigDecimal(0);
				LOG.error("Flight flno <{}> is not found. Message dropped.", flNum);
				addExptInfo(EnumExceptionCodes.ENOFL.name(), flNum);
				return false;
			}
			else{
				urno = entFlight.getUrno();
				//get the flt_job_assign by staff_number and id_flight
				entFltJobAssign = clsIDlFltJobAssignLocal.getAssignedCrewIdByFlight(urno, _input.getCrewDetails().getStaffNo(), _input.getCrewDetails().getCrewType());
				idFltJobAssign = (entFltJobAssign == null)? "0" : entFltJobAssign.getId();
			}
			
			/*boolean isFound = false;
			//get MD gate type
			gateTypeList = clsEntStartUpInitSingleton.getGateTypeList();
			for (int i = 0; i < gateTypeList.size(); i++) {
				if(_input.getCrewStatusInfo().getGateDetails().getGateType().equals(gateTypeList.get(i).getGateType())){
					idGateType = gateTypeList.get(i).getId();
					isFound = true; break;
				}
			}
			if(!isFound){
				LOG.warn("Input Gate type is not in MD_GATE_TYPE table.");
				addExptInfo(EnumExceptionCodes.WNOMD.name(), _input.getCrewStatusInfo().getGateDetails().getGateType());
			}*/
			statusChangeDtm = df.parse(convertFlDateToUTC(_input.getCrewStatusInfo().getStatusChageDTM()));
			
			EntDbCrewImmigration entCrew = null, oldEntCrew = new EntDbCrewImmigration();
			//find out existing crew immigration record
			entCrew = clsDlCrewImmigration.getExistingCrewImmigration(flNum, flDate, String.valueOf(_input.getFlightID().getDepNum()),
					String.valueOf(_input.getFlightID().getLegNum()), _input.getCrewDetails().getStaffNo(), _input.getCrewStatusInfo().getGateDetails().getGateType());
			
			if(entCrew == null){
				entCrew = new EntDbCrewImmigration();
				entCrew.setCreatedUser(HpEKConstants.IAMS_SOURCE);
				entCrew.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
				//entCrew.setCreatedDate(statusChangeDtm);
				entCrew.setDepNum(String.valueOf(_input.getFlightID().getDepNum()));
				entCrew.setLegNum(String.valueOf(_input.getFlightID().getLegNum()));
				entCrew.setStaffNumber(_input.getCrewDetails().getStaffNo());
				entCrew.setGateType(_input.getCrewStatusInfo().getGateDetails().getGateType());
				entCrew.setIdMdGateType(idGateType);
				entCrew.setFlightNumber(flNum);
			}
			else{
				oldEntCrew = new EntDbCrewImmigration(entCrew); //deep copy
				cmd = UfisASCommands.URT.name();
				entCrew.setUpdatedUser(HpEKConstants.IAMS_SOURCE);
				entCrew.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
			
			entCrew.setIdFltJobAssign(idFltJobAssign);
			if(_input.getTmStmp() != null)	
				entCrew.setMsgSendDate(df.parse(convertFlDateToUTC(_input.getTmStmp())));
			entCrew.setStatusChangeDate(statusChangeDtm);
			entCrew.setCrewTypeCode(_input.getCrewDetails().getCrewType());
			entCrew.setCrewActivityStat(_input.getCrewStatusInfo().getStatus());
			entCrew.setCrewActivityFlag(crewActivityFlag);
			entCrew.setStatusRemarks(_input.getCrewStatusInfo().getReason());
			entCrew.setGateNumber(_input.getCrewStatusInfo().getGateDetails().getGateNo());
			entCrew.setGateLocation(_input.getCrewStatusInfo().getGateDetails().getWing());
			
			entCrew.setIdFlight(urno);
			entCrew.setFltDate(flDate);
			entCrew.setFltOrigin3(_input.getFlightID().getDepStn());
			entCrew.setFltDest3(_input.getFlightID().getArrStn());
			entCrew.setDataSource(HpEKConstants.IAMS_SOURCE);
			entCrew.setRecStatus(" ");
			
			EntDbCrewImmigration resultEntCrew = clsDlCrewImmigration.merge(entCrew);
			if(resultEntCrew != null){
				sendNotifyUldInfoToInJson(resultEntCrew, oldEntCrew, cmd);
			}
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		}catch(ParseException e){
			LOG.error("Date time parsing Error : {}", e);
		}/*catch(CloneNotSupportedException e){
			//throw new AssertionError(e);
			LOG.error("When cloning the object for Old data value to Notify {}", e.getMessage());
			
		}*/
		catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
		return true;
	}
	
	private void sendNotifyUldInfoToInJson(EntDbCrewImmigration entCrew, EntDbCrewImmigration oldEntCrew, String cmd)
			throws IOException, JsonGenerationException, JsonMappingException {
		String idFlight = entCrew.getIdFlight().toString();
		/*// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setOrig(dtfl);
		List<String> idFlightList = new ArrayList<>();
		idFlightList.add(urno);
		header.setIdFlight(idFlightList);

		// get the BODY ACTION info
		List<String> fldList = new ArrayList<>();
		fldList.add("ID_FLIGHT");
		fldList.add("ID_FLT_JOB_ASSIGN");
		fldList.add("STAFF_NUMBER");
		fldList.add("CREW_TYPE_CODE");
		fldList.add("GATE_NUMBER");
		fldList.add("GATE_TYPE");

		List<Object> dataList = new ArrayList<>();
		dataList.add(urno);
		dataList.add(entCrew.getIdFltJobAssign());
		dataList.add(entCrew.getStaffNumber());
		dataList.add(entCrew.getCrewTypeCode());
		dataList.add(entCrew.getGateNumber());
		dataList.add(entCrew.getGateType());
		
		List<Object> oldList = new ArrayList<>();
		if(UfisASCommands.URT.toString().equals(cmd)){
			oldList.add(urno);
			oldList.add(oldEntCrew.getIdFltJobAssign());
			oldList.add(oldEntCrew.getStaffNumber());
			oldList.add(oldEntCrew.getCrewTypeCode());
			oldList.add(oldEntCrew.getGateNumber());
			oldList.add(oldEntCrew.getGateType());
		}
			
		List<String> idList = new ArrayList<>();
		idList.add(entCrew.getId());

		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		action.setCmd(cmd);
		action.setTab("CREW_IMMIGRATION");
		action.setFld(fldList);
		action.setData(dataList);
		action.setOdat(oldList);
		action.setId(idList);
		action.setSel("WHERE ID = \""+entCrew.getId()+"\"");
		listAction.add(action);

		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		clsBlUfisBCTopic.sendMessage(msg);*/
		if (HpUfisUtils
				.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), idFlight, oldEntCrew,
					entCrew);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true,
					UfisASCommands.valueOf(cmd), idFlight, oldEntCrew,
					entCrew);
		}
		
		LOG.debug("Sent Notify from IAMS .");
	}
	
	/**
	 * - ID_FLT_JOB_ASSIGN backward update from ACT-ODS 
	 * @param ufisMsgDTO
	 */
	public void updateFltJobAssign(EntUfisMsgDTO ufisMsgDTO) {
		try{
			switch(ufisMsgDTO.getBody().getActs().get(0).getTab().trim()){
				case "FLT_JOB_ASSIGN" : updateIdFltJobAssign(ufisMsgDTO); break;
			}
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
	
	private void updateIdFltJobAssign(EntUfisMsgDTO ufisMsgDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOG.debug("Flight job assign backward update msg is received..");
		
		List<String> fld = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		String idFlight = ufisMsgDTO.getHead().getIdFlight().get(0);
		String idFltJobAssign = ufisMsgDTO.getBody().getActs().get(0).getId().get(0);
		
		String staffNumber = null, staffCode = null;
		for (int i = 0; i < fld.size(); i++) {
			switch(fld.get(i).toUpperCase()){
				case "STAFF_NUMBER" :  staffNumber = ((String) data.get(i)).trim(); break;
				case "STAFF_TYPE" :  staffCode = ((String) data.get(i)).trim(); break;
			}
		}
		
		if(HpUfisUtils.isNullOrEmptyStr(idFlight) || HpUfisUtils.isNullOrEmptyStr(staffNumber)
				|| HpUfisUtils.isNullOrEmptyStr(staffCode) || HpUfisUtils.isNullOrEmptyStr(idFltJobAssign)){
			LOG.debug("(ID_FLIGHT, STAFF_NUMBER, STAFF_CODE, ID) is empty/null. No processing performed.");
			return;
		}
		BigDecimal idFlt = new BigDecimal(idFlight.trim());
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd();
		switch(cmd){
		case "IRT" :
		case "URT" :
			//find the existing from crew immigration table
			EntDbCrewImmigration entCrew = clsDlCrewImmigration.getExistingByIdFlight(idFlt, staffNumber, staffCode, idFltJobAssign);
			if(entCrew == null) //do nothing
				return;
			EntDbCrewImmigration oldEntCrew = new EntDbCrewImmigration(entCrew); //deep copy
			entCrew.setIdFltJobAssign(idFltJobAssign);
			
			cmd = UfisASCommands.URT.name();
			entCrew.setUpdatedUser(HpEKConstants.IAMS_SOURCE);
			entCrew.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			entCrew.setRecStatus(" ");
			
			EntDbCrewImmigration resultEntCrew = clsDlCrewImmigration.merge(entCrew);
			if(resultEntCrew != null){ //send broadcast message.
				sendNotifyUldInfoToInJson(resultEntCrew, oldEntCrew, cmd);
			}
			break;
		case "DRT" :
			//delete the record for  id_flt_job_assign
		/*	clsDlCrewImmigration.updateIdFltJobAssignDeleted(idFltJobAssign);
			LOG.debug("Updated the deleted id_flt_job_assign successfully.");*/
			
			//find the existing from crew immigration table
			EntDbCrewImmigration entDeletedCrew = clsDlCrewImmigration.getExistingByIdJobAssign(idFlt, idFltJobAssign);
			if(entDeletedCrew == null) //do nothing
				return;
			EntDbCrewImmigration oldEntDeletedCrew = new EntDbCrewImmigration(entDeletedCrew); //deep copy
			entDeletedCrew.setIdFltJobAssign(idFltJobAssign);
			
			cmd = UfisASCommands.DRT.name();
			entDeletedCrew.setUpdatedUser(HpEKConstants.IAMS_SOURCE);
			entDeletedCrew.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			entDeletedCrew.setRecStatus("X");
			
			EntDbCrewImmigration resultEntDeletedCrew = clsDlCrewImmigration.merge(entDeletedCrew);
			if(resultEntDeletedCrew != null){ //send broadcast message.
				sendNotifyUldInfoToInJson(resultEntDeletedCrew, oldEntDeletedCrew, cmd);
			}
			break;
		}
	}

	private String convertFlDateToUTC(XMLGregorianCalendar flightDate)
			throws ParseException {
		/*	HpUfisCalendar ufisCalendar = new HpUfisCalendar();
		ufisCalendar.setTime(flightDate);
		ufisCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));*/	

		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = flightDate.toGregorianCalendar().getTime();
		//LOG.debug("$$$$$ In UTC converter with DF : {}", df.format(utcDate));
		return df.format(utcDate);
	}
}