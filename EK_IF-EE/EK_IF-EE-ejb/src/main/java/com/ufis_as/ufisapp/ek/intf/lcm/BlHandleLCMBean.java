package com.ufis_as.ufisapp.ek.intf.lcm;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.lms.entities.EntDbMsgTelex;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlMsgTelexBean;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntFlightSearchObj;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.lcm.loadControlMessage.MessageDetailsType;

@Stateless(mappedName = "BlHandleLCMBean")
@LocalBean
public class BlHandleLCMBean {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleLCMBean.class);

	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private IAfttabBeanLocal afttabBean;
	@EJB
	private DlMsgTelexBean msgTelexBean;

	public String dtflStr = HpEKConstants.LOAD_CONTROL_MSG;
	public String logLevel = null;
	public Boolean msgLogged = Boolean.FALSE;
	public SimpleDateFormat sdf = new SimpleDateFormat("MMMyy");
	public SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyyhhmm");
	public SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyhhmmss");
	public static SimpleDateFormat sd1 = new SimpleDateFormat("MMyy");
	public SimpleDateFormat ddMMMyy = new SimpleDateFormat("ddMMMyy");
	public SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");

	public BlHandleLCMBean() {

	}

	@PostConstruct
	private void initialize() {
		if (!HpUfisUtils.isNullOrEmptyStr(_startupInitSingleton.getDtflStr())) {
			dtflStr = _startupInitSingleton.getDtflStr();
		} else {
			dtflStr = HpEKConstants.LOAD_CONTROL_MSG;
		}
	}

	public boolean routeMessage(Message inMessage, String messageDetailsType,
			Long irmtabRef, String msgType, String msgSource) {
		// long startTime = new Date().getTime();
		EntDbMsgTelex msgTelex = new EntDbMsgTelex();
		EntDbMsgTelex msgTelexInsrt = new EntDbMsgTelex();
		msgLogged = Boolean.FALSE;
		logLevel = _startupInitSingleton.getIrmLogLev();
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		try {			
			fillIdFlightFromMsg(inMessage,messageDetailsType, irmtabRef, msgLogged,
					msgTelex, msgSource, msgType);
			if (msgTelex.getMsgFltno() == null
					|| msgTelex.getIdFlight() == null) {
				LOG.error("Invalid Message.");
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage, dtflStr);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWVAL, "Invalid Message",
							irmtabRef);
				}
				return true;
			}

			if (msgTelex.getIdFlight().equals(BigDecimal.ZERO)) {
				LOG.error("No flight found in the AFTTAB.");
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage, dtflStr);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.WNOFL, "Flight num:"
							+ msgTelex.getMsgFltno() + " Flight DAY: "
							+ msgTelex.getMsgFltDay(), irmtabRef);

				}

			}

			msgTelex.setRecStatus(" ");
			msgTelex.setDataSource(HpEKConstants.LOAD_CONTROL_MSG);
			msgTelexInsrt = msgTelexBean.updateMsgTelex(msgTelex);
			if (msgTelexInsrt != null) {
				/*
				 * sendLoadBagUpdateToCedaInJson(loadBagStat, oldLoadBag,
				 * UfisASCommands.URT.name(), true);
				 */
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
					// send notification message to topic
					ufisTopicProducer.sendNotification(true,
							UfisASCommands.IRT,
							String.valueOf(msgTelexInsrt.getIdFlight()), null,
							msgTelexInsrt);
				}
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
					// if topic not defined, send
					// notification to queue
					ufisQueueProducer.sendNotification(true,
							UfisASCommands.IRT,
							String.valueOf(msgTelexInsrt.getIdFlight()), null,
							msgTelexInsrt);
				}
				LOG.debug("Msg Telex communicated to Ceda.");
			} else {
				LOG.error("Error inserting Telex msg to DB.");
			}
			/*
			 * LOG.info("Time For Processing Message:" + (new Date().getTime() -
			 * startTime));
			 */

			return true;
		} catch (Exception e) {
			LOG.error("!!!!ERROR(Exception): {}", e);
			return false;
		}
	}

	private void fillIdFlightFromMsg(Message inMessage, String message,
			Long irmtabRef, Boolean msgLogged2, EntDbMsgTelex msgTelex,
			String msgSource, String msgType) {
		EntFlightSearchObj flightSearchObj = new EntFlightSearchObj();
		try {
			Pattern regexCPM = Pattern
					.compile("((?m)^\\.{1}([A-Z]{7})\\s([0-9]{6}).*\\s*)?\\n\\s*([A-Z]{3})\\s*\\n([A-Z]{2,3})([0-9]{3,4})[/]([0-9]{2})\\.{1}([A-Z]{1}[0-9]{1}[A-Z]{3}|\\s+)(\\b*)");
			Pattern regexACARSMacs = Pattern
					.compile("(?m)(^\\.([A-Z]{7})\\s+([0-9]{6})).*\\s*\\n*(?s).*LOADSHEET\\s*\\n[A-Z]*[0-9]*\\s([A-Z]{2,3})\\s*([0-9]{3,4})[/]([0-9]{2})\\s+([A-Z]{6,7})?\\s([A-Z]{1}[0-9]{1}[A-Z]{3})?\\s*([0-9]{2}[A-Z]{3}[0-9]{2})?(\\s*\\b*)");
			// .compile("(?m)(^\\.([A-Z]{7})\\s+([0-9]{6}))\\s*\\n*(?s).*LOADSHEET\\s*\\n([A-Z]*[0-9]*)\\s([A-Z]{2,3})([0-9]{3,4})[/]([0-9]{2})\\s+([A-Z]{6,7})\\s([A-Z]{1}[0-9]{1}[A-Z]{3})\\s+([0-9]{2}[A-Z]{3}[0-9]{2})(\\b*)");
			Pattern regexACARSSalt = Pattern
					.compile("(?m)(^\\.([A-Z]{7})\\s+([0-9]{6}))?.*\\s*\\n*(?s).*LOADSHEET\\s*\\n[A-Z]*[0-9]*\\s([A-Z]{2,3})\\s*([0-9]{3,4})[/]([0-9]{2})\\s+([A-Z]{6,7})?\\s([A-Z]{1}[0-9]{1}[A-Z]{3})?\\s*([0-9]{2}[A-Z]{3}[0-9]{2})?(\\s*\\b*)");
			// .compile("(?m)(^\\.([A-Z]{7})\\s+([0-9]{6}))?\\s*\\n*(?s).*LOADSHEET\\s*\\n([A-Z]*[0-9]*)\\s([A-Z]{2,3})([0-9]{3,4})[/]([0-9]{2})\\s+([A-Z]{6,7})\\s([A-Z]{1}[0-9]{1}[A-Z]{3})\\s+([0-9]{2}[A-Z]{3}[0-9]{2})(\\b*)");
			// "(?m)^\\.{0,1}(([A-Z]{7})\\s+([0-9]{6}))*\\s*\\n*(?s).*LOADSHEET\\s*\\n([A-Z]*[0-9]*)\\s([A-Z]{2,3})([0-9]{3,4})[/]([0-9]{2})\\s+([A-Z]{6,7})\\s([A-Z]{1}[0-9]{1}[A-Z]{3})\\s+([0-9]{2}[A-Z]{3}[0-9]{2})(\\b*)");
			/*
			 * Pattern regexLIR = Pattern .compile(
			 * "(?m)^\\.{1}([A-Z]{7})\\s([0-9]{6})\\s*\\n(LOADING INSTRUCTION/REPORT)(?s).*\\n([A-Z]{3,4})\\s([A-Z]{3,4})\\s([A-Z]{2,3})\\s([0-9]{3,4})\\s+([A-Z]{1}[0-9]{1}[A-Z]{3})\\s+.[^\\n]*\\s*([0-9]{2}[A-Z]{3}[0-9]{2})\\s+([0-9]{4})(\\n)"
			 * );
			 */
			Pattern regexLIR = Pattern
					.compile("(?m)^\\.{1}(([A-Z]{7})\\s([0-9]{6})\\s*)?.*\\n*\\s*LOADING INSTRUCTION/REPORT(?s).*\\n([A-Z]{3,4})?\\s+([A-Z]{3,4})?\\s+([A-Z]{2,3})\\s*([0-9]{3,4})\\s+(([A-Z]{1}[0-9]{1}\\-{0,1}[A-Z]{3}))?\\s+.[^\\n]*\\s*([0-9]{2}[A-Z]{3}[0-9]{2})\\s+(([0-9]{4}))?(\\s*\\b*)");
			Matcher regexMatcher = regexCPM.matcher(message);
			boolean foundMatch = regexMatcher.find();
			// boolean foundMatch1 = regexMatcher.matches();
			if (foundMatch) {
				LOG.info("Group count: {}", regexMatcher.groupCount());
				/*
				 * for (int i = 0; i < regexMatcher.groupCount(); i++) {
				 * 
				 * Group count:9 0.Group :.DXBKLEK 270613 CPM EK432/05.A6EBB
				 * 1.Group :.DXBKLEK 270613 2.Group :DXBKLEK 3.Group :270613
				 * 4.Group :CPM 5.Group :EK 6.Group :432 7.Group :05 8.Group
				 * :A6EBB
				 * 
				 * 
				 * //LOG.info("Group :" + regexMatcher.group(i)); }
				 */
				if (regexMatcher.group(4) == null) {
					LOG.error("Message Type not specified in the Message Details Section.Dropping the message.");
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(inMessage,
									dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.EWFMT,
								"Message Type not specified in Msg Details",
								irmtabRef);
					}
					return;
				}
				msgTelex.setMsgType(regexMatcher.group(4));

				if (msgTelex.getMsgType() != null
						&& !msgType.equalsIgnoreCase(msgTelex.getMsgType())) {
					LOG.error("Message Type not match.Dropping the message.");
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(inMessage,
									dtflStr);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.EWVAL,
								"Message Type not match with Msg Details",
								irmtabRef);
					}
					return;
				}

				msgTelex.setMsgFltno(HpUfisUtils.formatCedaFlno(
						regexMatcher.group(5), regexMatcher.group(6), null));
				msgTelex.setMsgFltDay(regexMatcher.group(7));				
				 msgTelex.setLoadingPt(regexMatcher.group(2)!=null ? regexMatcher.group(2).substring(0,3):null);
				if (HpUfisUtils.isNotEmptyStr(msgTelex.getLoadingPt())) {
					msgTelex.setArrDepFlag(msgTelex.getLoadingPt()
							.equalsIgnoreCase(HpEKConstants.EK_HOPO) ? "D"
							: "A");
				}
				msgTelex.setSendRecvFlag("R");
				msgTelex.setMsgSender(regexMatcher.group(2) == null ? " "
						: regexMatcher.group(2));
				if (HpUfisUtils.isNotEmptyStr(regexMatcher.group(3))) {
					msgTelex.setMsgSendDay(regexMatcher.group(3)
							.substring(0, 2));
					msgTelex.setMsgSendTime(regexMatcher.group(3).substring(2));
				} else {
					msgTelex.setMsgSendDay(" ");
					msgTelex.setMsgSendTime(" ");
				}
				flightSearchObj.setREGN(regexMatcher.group(8));
				// Flight search
			} else {

				regexMatcher = regexACARSMacs.matcher(message);
				boolean regexMatched = regexMatcher.find();
				if (!regexMatched) {
					regexMatcher = regexACARSSalt.matcher(message);
					regexMatched = regexMatcher.find();
				}

				if (regexMatched) {

					LOG.info("Group count: {}", regexMatcher.groupCount());
					/*
					 * for (int i = 0; i < regexMatcher.groupCount(); i++) {
					 * 
					 * Group count:10 0.Group :.HDQKMEK 120440 AGM AN A6-EED/MA
					 * LSHA - LOADSHEET FINAL EK 001/12 DXBLHR A6EED 12MAY13
					 * 
					 * 1.Group :.HDQKMEK 120440 2.Group :HDQKMEK 3.Group :120440
					 * 4.Group :EK 5.Group :001 6.Group :12 7.Group :DXBLHR
					 * 8.Group :A6EED 9.Group :12MAY13
					 * 
					 * 
					 * 
					 * // To-DO //LOG.info("Group :" + regexMatcher.group(i)); }
					 */

					msgTelex.setMsgType("ACARS"); // check
					msgTelex.setMsgFltno(HpUfisUtils.formatCedaFlno(
							regexMatcher.group(4), regexMatcher.group(5), null));
					msgTelex.setMsgFltDay(regexMatcher.group(6));
					msgTelex.setLoadingPt(regexMatcher.group(7) != null ? regexMatcher
							.group(7).substring(0, 3) : "");// Can
					// be
					// of
					// length
					// 4
					// as
					// well
					if (HpUfisUtils.isNotEmptyStr(msgTelex.getLoadingPt())) { // ???
						msgTelex.setArrDepFlag(msgTelex.getLoadingPt()
								.equalsIgnoreCase(HpEKConstants.EK_HOPO) ? "D"
								: "A");
					}
					msgTelex.setSendRecvFlag("R");
					msgTelex.setMsgSender((regexMatcher.group(2) == null) ? " "
							: regexMatcher.group(2));
					// msgTelex.setMsgSender(" ");
					if (regexMatcher.group(3) != null) {
						msgTelex.setMsgSendDay(regexMatcher.group(3).substring(
								0, 2));
						msgTelex.setMsgSendTime(regexMatcher.group(3)
								.substring(2));
					} else {
						msgTelex.setMsgSendDay(" ");
						msgTelex.setMsgSendTime(" ");
					}
					flightSearchObj.setREGN(regexMatcher.group(8));
					if (HpUfisUtils.isNotEmptyStr(regexMatcher.group(9))) {
						flightSearchObj.setFLUT(formatfltDate(regexMatcher
								.group(9)));
					}
					/*flightSearchObj
							.setDES3(regexMatcher.group(7) != null ? regexMatcher
									.group(7).substring(3) : null);*/
					flightSearchObj.setORG3(msgTelex.getLoadingPt());

				} else {
					regexMatcher = regexLIR.matcher(message);
					if (regexMatcher.find()) {

						LOG.info("Group count: {}", regexMatcher.groupCount());
						/*
						 * for (int i = 0; i < regexMatcher.groupCount(); i++) {
						 * 
						 * Group count:13 0.Group :.HDQKMEK 070301 LOADING
						 * INSTRUCTION/REPORT PREPARED BY EDNO ALL WEIGHTS IN
						 * KILOS ABBASS 2 FROM/TO FLIGHT A/C REG VERSION GAT TAR
						 * STD DATE TIME DXB LHR EK 0001 A6EDX 14F76J427Y B15
						 * F19 0745 07MAY13 0701
						 * 
						 * 1.Group :HDQKMEK 070301
						 * 
						 * 2.Group :HDQKMEK 3.Group :070301 4.Group :DXB 5.Group
						 * :LHR 6.Group :EK 7.Group :0001 8.Group :A6EDX 9.Group
						 * :A6EDX 10.Group :07MAY13 11.Group :0701 12.Group
						 * :0701
						 * 
						 * 
						 * // To-DO LOG.info("Group :" + regexMatcher.group(i));
						 * }
						 */
						msgTelex.setMsgType("LIR"); // check
						msgTelex.setMsgFltno(HpUfisUtils.formatCedaFlno(
								regexMatcher.group(6), regexMatcher.group(7),
								null));
						msgTelex.setMsgFltDay(regexMatcher.group(10).substring(
								0, 2));
						msgTelex.setLoadingPt(regexMatcher.group(4));// Can
						// be
						// of
						// length
						// 4
						// as
						// well
						// ???
						msgTelex.setArrDepFlag(msgTelex.getLoadingPt()
								.equalsIgnoreCase(HpEKConstants.EK_HOPO) ? "D"
								: "A");
						msgTelex.setSendRecvFlag("R");
						// msgTelex.setMsgSender(regexMatcher.group(1)); //?????

						msgTelex.setMsgSender(regexMatcher.group(2) != null ? regexMatcher
								.group(2) : " ");
						if (HpUfisUtils.isNotEmptyStr(regexMatcher.group(3))) {
							msgTelex.setMsgSendDay(regexMatcher.group(3)
									.substring(0, 2));
							msgTelex.setMsgSendTime(regexMatcher.group(3)
									.substring(2));
						}
						flightSearchObj.setREGN(regexMatcher.group(9));
						flightSearchObj.setFLUT(formatfltDate(regexMatcher
								.group(10)));
						//flightSearchObj.setDES3(regexMatcher.group(5));
						flightSearchObj.setORG3(msgTelex.getLoadingPt());

					} else {
						LOG.info("Invalid Message.Dropping the Message.");
						return;
					}
				}
			}
			// flightSearchObj.setORG3(msgTelex.getLoadingPt());
			flightSearchObj.setFLNO(msgTelex.getMsgFltno());
			if (HpUfisUtils.isNotEmptyStr(msgTelex.getArrDepFlag())) {
				flightSearchObj.setADID(msgTelex.getArrDepFlag().charAt(0));
			}
			// calling for flight serach
			msgTelex.setIdFlight(afttabBean.findFlightByMultipleParams(
					flightSearchObj, false, msgTelex.getMsgType(),
					HpCommonConfig.fromOffset, HpCommonConfig.toOffset));
			LOG.info("IdFlight:{}" + msgTelex.getIdFlight());
			msgTelex.setMsgSubtype(HpEKConstants.LOAD_CONTROL_MSG);
			msgTelex.setMsgInfo(message);
		} catch (Exception ex) {
			// Syntax error in the regular expression
			LOG.error("Error: {}", ex);
		}

	}

	private void sendErrInfo(EnumExceptionCodes expCode, String desc,
			Long irmtabRef) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflStr);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflStr);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from Load Control to Data Loader.");

	}

	public String formatSendTime(String msgSendTime) {
		try {
			return sd1.format(sdf.parse(msgSendTime));
		} catch (Exception e) {
			LOG.error("Exception on formating the Msg Send Time.Exception :{}",
					e);
		}

		return null;
	}

	public String formatfltDateTime(String fltDate, String fltTime) {
		try {
			if (HpUfisUtils.isNullOrEmptyStr(fltTime)) {
				fltTime = "0000";
			}
			if (!HpUfisUtils.isNullOrEmptyStr(fltDate)
					&& !HpUfisUtils.isNullOrEmptyStr(fltTime)) {
				return sdf2.format(sdf1.parse(fltDate + fltTime));
			}
		} catch (Exception e) {
			LOG.error("Exception on formating the Msg Send Time.Exception :{}",
					e);
		}

		return null;
	}

	public String formatfltDate(String fltDate) {
		try {
			if (!HpUfisUtils.isNullOrEmptyStr(fltDate)) {
				return yyyyMMdd.format(ddMMMyy.parse(fltDate));
			}
		} catch (Exception e) {
			LOG.error("Exception on formating the Msg Send Time.Exception :{}",
					e);
		}

		return null;
	}

}
