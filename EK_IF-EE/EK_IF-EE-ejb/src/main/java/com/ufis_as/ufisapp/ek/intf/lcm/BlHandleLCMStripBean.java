package com.ufis_as.ufisapp.ek.intf.lcm;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.lcm.loadControlMessage.LoadControlMsgType;

@Stateless(mappedName = "BlHandleLCMStripBean")
@LocalBean
public class BlHandleLCMStripBean {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleLCMStripBean.class);

	private JAXBContext _cnxJaxb;

	private Unmarshaller _um;

	// private List<String> qFromMqList = null;

	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private ConnFactorySingleton _connSingleton;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlHandleLCMBean _blLCMBean;

	public String dtflStr = HpEKConstants.LOAD_CONTROL_MSG;
	public String logLevel = null;
	public Boolean msgLogged = Boolean.FALSE;
	private Session session;
	// private MessageProducer messageProducer;
	// private List<MessageProducer> messageProducerList = new
	// ArrayList<MessageProducer>();
	List<String> fldList = new ArrayList<>();

	// private String queueName;

	public BlHandleLCMStripBean() {

	}

	@PostConstruct
	private void initialize() {

		try {
			_cnxJaxb = JAXBContext.newInstance(LoadControlMsgType.class);
			_um = _cnxJaxb.createUnmarshaller();
			/*
			 * if (_startupInitSingleton.getInterfaceConfigs().isEmpty()) {
			 * LOG.warn
			 * ("No Interface configs/connection details specified in config file."
			 * ); return; }
			 */

			/*
			 * List<String> toQueueList =
			 * _startupInitSingleton.getToQueueList();
			 * 
			 * if (toQueueList.isEmpty()) {
			 * LOG.warn("\"TO:AMQ queue name\" has not configured. "); return; }
			 * try { session =
			 * _connSingleton.getActiveMqConnect().createSession( false,
			 * Session.AUTO_ACKNOWLEDGE); for (int i = 0; i <
			 * toQueueList.size(); i++) { queueName = toQueueList.get(i); if
			 * (HpUfisUtils.isNullOrEmptyStr(queueName)) {
			 * LOG.warn("!!!Warning, Queue name is null or empty", queueName); }
			 * else { if (session != null) { messageProducer =
			 * session.createProducer(session .createQueue(queueName));
			 * messageProducerList.add(messageProducer); } else { LOG.error(
			 * "!!!ERROR, creating tibco session for queue: {}", queueName); } }
			 * }
			 * 
			 * fldList.add("MessageTime"); fldList.add("MessageID");
			 * fldList.add("MessageType"); fldList.add("MessageSubType");
			 * fldList.add("MessageSource"); fldList.add("MessageDetails");
			 */
			if (!HpUfisUtils.isNullOrEmptyStr(_startupInitSingleton
					.getDtflStr())) {
				dtflStr = _startupInitSingleton.getDtflStr();
			} else {
				dtflStr = HpEKConstants.LOAD_CONTROL_MSG;
			}

		} catch (JAXBException e) {
			LOG.error("JAXBException: {}", e);
			// LOG.error("Cannot initiate AMQ queue : {}", queueName);
		}

		catch (Exception ex) {
			LOG.error("Exception when creating Unmarshaller.Exception :", ex);
		}

	}

	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close TIBCO session: {}", e);
		}
	}

	public boolean routeMessage(Message inMessage, String message,
			Long irmtabRef) {
		long startTime = new Date().getTime();
		LoadControlMsgType loadControlMsg;// = new LoadControlMsgType();
		// ObjectMapper objectMapper = new ObjectMapper();

		String dtflString = _startupInitSingleton.getDtflStr();
		msgLogged = Boolean.FALSE;
		logLevel = _startupInitSingleton.getIrmLogLev();
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		try {
			JAXBElement<LoadControlMsgType> data = _um.unmarshal(
					new StreamSource(new StringReader(message)),
					LoadControlMsgType.class);
			LOG.info(" Data Declared Type: {}", data.getDeclaredType());
			LOG.info(" Data Name: {}", data.getName());
			LOG.info(" Data Value: {}", data.getValue());
			loadControlMsg = (LoadControlMsgType) data.getValue();
			if (loadControlMsg == null) {
				LOG.info(" Dropping the message. Message is Empty. MSG : {}",
						loadControlMsg);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, "Message is Empty",
							irmtabRef);

				}
				return false;
			}
			
			if (loadControlMsg.getMeta() == null
					|| loadControlMsg.getMessageDetails() == null){// HpUfisUtils.isNullOrEmptyStr(loadControlMsg.getMessageDetails())) {
				LOG.info(
						" Dropping the message. Meta: {} or Message Details: {} are empty.",
						loadControlMsg.getMeta(),
						loadControlMsg.getMessageDetails());
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF,
							"Meta or Message Details are empty", irmtabRef);

				}
				return false;
			}
			if (loadControlMsg.getMeta().getMessageTime() == null
					|| HpUfisUtils.isNullOrEmptyStr(loadControlMsg.getMeta()
							.getMessageType())
					|| HpUfisUtils.isNullOrEmptyStr(loadControlMsg.getMeta()
							.getMessageSubtype())
					|| HpUfisUtils.isNullOrEmptyStr(loadControlMsg.getMeta()
							.getMessageSource())) {
				LOG.info(
						" Dropping the message. Meta section mandatory fields(MessageTime,MessageType,MessageSubType,essageSource) are empty. MSG : {}",
						message);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(
							EnumExceptionCodes.EXSDF,
							"Meta section mandatory fields(MessageTime,MessageType,MessageSubType,essageSource) are empty",
							irmtabRef);

				}
				return false;
			}
			if (!loadControlMsg.getMeta().getMessageTime().isValid()) {
				LOG.info(
						" Dropping the message. Message Time is not Valid. MSG : {}",
						loadControlMsg.getMeta().getMessageTime());
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWFMT,
							"Message Time is not Valid.", irmtabRef);
				}
				return false;
			}

			/*
			 * LOG.info("Start sending message to AMQ..."); TextMessage msg =
			 * session.createTextMessage();
			 * 
			 * objectMapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
			 * objectMapper
			 * .writeValueAsString(loadControlMsg.getMessageDetails());
			 * ObjectMessage msg=session.createObjectMessage();
			 * msg.setObject(loadControlMsg.getMessageDetails());
			 * 
			 * 
			 * String str = loadControlMsg.getMessageDetails();
			 * LOG.info("String:" + str); str = formatMsg(str,
			 * loadControlMsg.getMeta().getMessageType(),
			 * loadControlMsg.getMeta().getMessageSource(), loadControlMsg
			 * .getMeta().getMessageSubtype()); msg.setText(str); for
			 * (MessageProducer msgProd : messageProducerList) {
			 * msgProd.send(msg); }
			 * LOG.debug("!!!Sent a {} message to AMQ Queue : {}",
			 * loadControlMsg .getMeta().getMessageType(), queueName);
			 */			
			_blLCMBean.routeMessage(inMessage,
					loadControlMsg.getMessageDetails(), irmtabRef,
					loadControlMsg.getMeta().getMessageType(),loadControlMsg.getMeta().getMessageSource());

			LOG.info("Time For Processing Message:"
					+ (new Date().getTime() - startTime));

			return true;
		} catch (JAXBException e) {
			LOG.error("!!!!ERROR(JAXBException):  {}" ,e);
			return false;
		} catch (Exception e) {
			LOG.error("!!!!ERROR(Exception):  {}" , e);
			return false;
		}
	}

	private void sendErrInfo(EnumExceptionCodes expCode, String desc,
			Long irmtabRef) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflStr);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflStr);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from LCM to Data Loader.");

	}

	/*
	 * private String formatMsg(String msg, String msgType, String msgSource,
	 * String msgSubType) { String jsonString = null; EntUfisMsgHead header =
	 * new EntUfisMsgHead(); header.setHop(HpEKConstants.HOPO);
	 * header.setOrig(dtflStr); header.setApp(dtflStr); header.setUsr(dtflStr);
	 * // if (isLast) { // header.setBcnum(-1); // }
	 * 
	 * List<String> idFlightList = new ArrayList<String>(); List<String> idList
	 * = new ArrayList<>(); List<Object> dataList = new ArrayList<Object>();
	 * header.setIdFlight(idFlightList);
	 * 
	 * dataList.add(""); // MsgTime dataList.add(""); // MsgID
	 * dataList.add(msgType); // MsgType dataList.add(msgSubType); // MsgSubType
	 * dataList.add(msgSource); // MsgSource dataList.add(msg); // MsgDetails
	 * 
	 * List<EntUfisMsgACT> listAction = new ArrayList<>(); EntUfisMsgACT action
	 * = new EntUfisMsgACT(); action.setCmd(msgSubType);
	 * action.setTab("MSG_TELEX"); action.setFld(fldList);
	 * action.setData(dataList); action.setId(idList); listAction.add(action);
	 * 
	 * jsonString = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
	 * return jsonString; }
	 */

}
