package com.ufis_as.ufisapp.ek.intf.lido;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.egds.entities.EntDbFlightFuelStatus;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlAiportWeatherBean;
import com.ufis_as.ufisapp.ek.eao.DlAtrtabBean;
import com.ufis_as.ufisapp.ek.eao.DlFltFuelStatusBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.lido.LidoMsgInfo;
/**
 * @author BTR
 * @version 1:					BTR - Initial implementation as per design doc
 * @version 1.1.0:	2013-10-18	BTR 1.) Modified for internal exception handling to UFISDATASTORE
 * 								   2.) 
 * @version 1.1.1: 2013-10-30 JGO - Adding notification 
									Adding error/warning info data store
 *                                  Adding Tibco reconnect
 *                                  Handle the parseException for datetime format
 *                                  Change flno format
 * @version 1.1.2: 2013-11-21 JGO - To support the new field of numrecs of internal communication 
 *                                  object to prevent the json-object convert issue
 * @version 1.1.3: 2013-12-10 JGO - Improve MET message processing log
 *                                  Make the code more readable
 */

@Stateless
public class BlHandleLidoBean extends BaseDTO{

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleLidoBean.class);
//	private JAXBContext _cnxJaxb;
//	private Unmarshaller _um;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private BlIrmtabFacade clsBlIrmtabFacade;
	@EJB
	private DlFltFuelStatusBean clsDlFltFuelStatus;
	@EJB
	private DlAiportWeatherBean clsDlAirportWeather;
	@EJB
	BlHandleLidoWeatherBean clsBlHaLidoWeatherBean;
	@EJB
	BlHandleLidoNotamsBean clsBlHandleLidoNotamsBean;
	@EJB
	DlAtrtabBean clsDlAtrtab;
	/*@EJB
	BlUfisMsgFormatter clsBlUfisMsgFormatter;*/
	@EJB
	BlUfisCedaQueue clsUfisCedaQ;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	float fuelRate = 0;
	String blockFuelStr = "0";//set default for dept flight
	String msg = null, airline = null,
		   org3 = null, releaseIndicator = null;
	String notamsId, effectFrom, effectTil, notamsStatus, notamsICAO, notamsIATA, rema;
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(LidoMsgInfo.class);
			_um = _cnxJaxb.createUnmarshaller();
			fuelRate = clsEntStartUpInitSingleton.getFuelRate();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	
	/**
	 * - check the type of the input message (FPL, MET, NOTAMS)
	 * @param message
	 */
	public void processLidoRouter(String message, long irmtabRef){
		// clear
    	data.clear();
    	
		try {
			LidoMsgInfo _input = (LidoMsgInfo) _um.unmarshal(new StreamSource(new StringReader(message)));
			String type = _input.getMeta().getMessageType().trim();
			if(HpUfisUtils.isNullOrEmptyStr(type))
				LOG.warn("Input message type is missing. Message dropped.");
			else{
				LOG.info("Message Type: {}", type);
				switch (type) {
				case "FPL":
					processFlightPlan(_input, irmtabRef);
					break;
				case "MET":
					clsBlHaLidoWeatherBean.processWeather(_input, irmtabRef);
					if (!clsBlHaLidoWeatherBean.getData().isEmpty()) {
						data = clsBlHaLidoWeatherBean.getData();
					}
					break;
				case "NOTAMS":
					clsBlHandleLidoNotamsBean.processNOTAMS(_input, irmtabRef);
					if (!clsBlHandleLidoNotamsBean.getData().isEmpty()) {
						data = clsBlHandleLidoNotamsBean.getData();
					}
					break;
				default:
					LOG.warn("Input message type should be (FPL or MET or NOTAMS). Message dropped.");
					break;
				}
			}
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(),
					EnumExceptionCodes.EXSDF.toString());
//		}catch(ParseException e){
//			LOG.error("Date time parsing Error : {}", e.getMessage());
		} catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
	
	/**
	 * - calculate and update/insert Flight Plan(FPL) for Departure Flight fuel.
	 * - id_RFlight = 0, if the rotational flight does not exist.
	 * - insert requiredFuel, fuelRate, fuelTime and skip [onBoardFuel] which is belong to EGDS.
	 * @param input LidoMsgInfo
	 * @throws Exception, ParseException
	 */
	public boolean processFlightPlan(LidoMsgInfo _input, long irmtabRef) {
		//TODO throw part handling
		
		// clear
//    	data.clear();
    	
    	// set urno of irmtab
    	irmtabUrno = irmtabRef;
		if(!validateFSUMMandatoryData(_input))
			return false;
			
		DateFormat df = new SimpleDateFormat("ddMMMyyyy");
		DateFormat dfCeda = new SimpleDateFormat("yyyyMMdd");
		
		String flightNum = msg.substring(24, 29).trim();
		String suffix = msg.substring(29, 30).trim();
		String flut = msg.substring(30, 39).trim();
		String regn = msg.substring(63, 72).trim();
		float requiredFuel = new Integer(blockFuelStr);
		StringBuilder sb = new StringBuilder();
		
		//format ceda flno
		//String flnoCeda = airline +" " + HpUfisUtils.formatCedaFltn(String.valueOf(flightNum)) + suffix;
		String flnoCeda = HpUfisUtils.formatCedaFlno(airline, flightNum, suffix);
		String flutCeda = "";
		try {
			flutCeda = dfCeda.format(df.parse(flut));
		} catch (ParseException e) {
			LOG.error("Cannot parse flight date into CEDA format");
			sb.append("Cannot parse flight date into Ceda format - ");
			sb.append("FLUT: ").append(flut);
			addExptInfo(EnumExceptionCodes.EWVAL.name(), sb.toString());
			return false;
		}
		
		EntDbAfttab deptFlight = clsAfttabBeanLocal.findFlightForDeptByFlnoFlutRegn(flutCeda, flnoCeda, regn);
		if(deptFlight == null){//flight in flt_fuel_status is not existed in AFTTAB
			LOG.warn("Message dropped.");
			sb.append("Flight not found - ");
			sb.append("FLUT: ").append(flutCeda).append(" ");
			sb.append("FLNO: ").append(flnoCeda).append(" ");
			sb.append("REGN: ").append(regn);
			addExptInfo(EnumExceptionCodes.ENOFL.name(), sb.toString());
			return false;
		}
			
		String atot = deptFlight.getAtot();
		int atotNum = 0;
		if(!HpUfisUtils.isNullOrEmptyStr(atot)){
			 atotNum = new Integer(atot);
			if(atotNum >= 0){//flight already departed.
				LOG.warn("Dept Flight URNO <{}> has ATOT <{}>. Message dropped.", 
						deptFlight.getUrno(), deptFlight.getAtot());
				sb.append("Flight aldy departed - ");
				sb.append("URNO: ").append(deptFlight.getUrno()).append(" ");
				sb.append("ATOT: ").append(deptFlight.getAtot()).append(" ");
				addExptInfo(EnumExceptionCodes.EWVAL.name(), sb.toString());
				return false;
			}
		}
		
		float onBoardFuel = 0;
		EntDbAfttab conxFlight = null;
		BigDecimal idRFlight = new BigDecimal(0);
		
		/***
		 * - calculate onBoardFuel by its RKEY(Arr)
		 ***/
		//(RKEY != URNO?)
		if(new BigDecimal(deptFlight.getRkey()).compareTo(deptFlight.getUrno()) != 0){
			//get the rotation(ARRIVAL) flight by RKEY
			conxFlight = clsAfttabBeanLocal.findFlightForConxByUrno(deptFlight.getRkey());
			if(conxFlight == null){
				LOG.debug("Rotation flight RKEY <{}> for URNO <{}> is not found in AFTTAB.", 
						deptFlight.getRkey(), deptFlight.getUrno());
			}
			else if("A".equals(conxFlight.getAdid().toString().trim())){//rotation flight should be Arrival
				idRFlight = conxFlight.getUrno();
				EntDbFlightFuelStatus conxFltFuel = clsDlFltFuelStatus.findByIdFlight(conxFlight.getUrno());
				if(conxFltFuel != null)
					onBoardFuel = conxFltFuel.getOnboardFuel();
			}
		}
		else//(RKEY == URNO)
			//no rotation flight, onBoardFuel = 0
			LOG.debug("URNO <{}> does not have rotation flights.", deptFlight.getUrno()); 
		
		float fuelTime = calculateFuelTime(onBoardFuel, requiredFuel);
		
		//get existing flight fuel status record for dept flight
		EntDbFlightFuelStatus data = null;
		UfisASCommands cmd = UfisASCommands.URT;
		EntDbFlightFuelStatus entFltFuelStatus = clsDlFltFuelStatus.findByIdFlight(deptFlight.getUrno());
		if(entFltFuelStatus == null){
			//entFltFuelStatus = new EntDbFlightFuelStatus();
			data = new EntDbFlightFuelStatus();
			LOG.debug("Flight fuel status for dept flight <{}> will be added as new record.", deptFlight.getUrno());
			data.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			data.setCreatedUser(HpEKConstants.LIDO_SOURCE);
			cmd = UfisASCommands.IRT;
		}
		else{
			data = EntDbFlightFuelStatus.valueOf(entFltFuelStatus);
			data.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			data.setUpdatedUser(HpEKConstants.LIDO_SOURCE);
		}
		data.setIdRFlight(idRFlight);//set rotation flight
		data.setRequiredFuel(requiredFuel);
		data.setFuelRate(fuelRate);
		data.setFuelTime(fuelTime);
		data.setIdFlight(deptFlight.getUrno());
		data.setMsgSendDate(convertFlDateToUTC(_input.getMeta().getMessageTime()));
		data.setFltRegn(regn);
		data.setFlightNumber(flightNum);
		data.setAirlineCode2(airline);
		data.setFltOrigin3(org3);
		try {
			data.setFltDate(df.parse(flut));
		} catch (ParseException e) {
			LOG.error("Cannot parse {} into date", flut);
		}
		data.setDataSource(HpEKConstants.LIDO_SOURCE);
		
		//merge the input flight fuel status to db
		EntDbFlightFuelStatus entity = clsDlFltFuelStatus.update(data);
		
		// send notification message
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true, cmd, String.valueOf(entity.getIdFlight()), entFltFuelStatus, entity);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true, cmd, String.valueOf(entity.getIdFlight()), entFltFuelStatus, entity);
		}
		if(entity != null)
			LOG.info("Flight Fuel has been processed.");
		return true;
	}
	
	/**
	 * - check all the mandatory fields to drop message
	 * @param _input
	 * @return
	 */
	private boolean validateFSUMMandatoryData(LidoMsgInfo _input) {
		String subType = _input.getMeta().getMessageSubtype().trim();
		String source = _input.getMeta().getMessageSource().trim();
		msg = _input.getMessageDetails().trim();
		if(_input.getMeta().getMessageTime() == null
				|| HpUfisUtils.isNullOrEmptyStr(subType)
				|| HpUfisUtils.isNullOrEmptyStr(source)
				|| HpUfisUtils.isNullOrEmptyStr(msg)){
			LOG.warn("Input message mandatory info are missing. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "Input message mandatory info are missing.");
			return false;
		}
		if(!"UPD".equals(subType)){
			LOG.warn("Message subType should be UPD. Message dropped.");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Message subType not correct: " + subType);
			return false;
		}
		if(!"Lido".equals(source)){
			LOG.warn("Message source should be Lido. Message dropped");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Message source not correct: " + source);
			return false;
		}
		//check for EK airline
		airline = msg.substring(21, 24).trim();
		if(!"EK".equals(airline)){
			LOG.warn("Processing perform only for EK airline.");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Accept EK flight only");
			return false;
		}
			
		org3 = msg.substring(39, 44).trim();
		if(!"DXB".equals(org3)){
			LOG.warn("Processing perform only for departure flight station = DXB. ORG3 = <{}>", org3);
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Accept Departure flight only base on HOPO: DXB");
			return false;
		}

		releaseIndicator = msg.substring(163, 164).trim();
		if(HpUfisUtils.isNullOrEmptyStr(releaseIndicator)){
			LOG.warn("Release Indicator is empty/null. Message dropped.");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Release indicator cannot be null or empty");
			return false;
		}
		
		blockFuelStr = msg.substring(217, 223).trim();
		if("F".equals(releaseIndicator))
			if(HpUfisUtils.isNullOrEmptyStr(blockFuelStr)){
				LOG.warn("BlockFuel is empty when Release Indicator is F. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWVAL.name(), "BlockFuel cannot be empty or null when ReleaseIndicator is F");
				return false;
			}
		return true;
	}

	/**
	 * @param flightDate
	 * @return
	 * @throws ParseException
	 */
	private Date convertFlDateToUTC(XMLGregorianCalendar flightDate) {
		Date utcDate = null;
		try {
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			df.setTimeZone(HpEKConstants.utcTz);
			Date d = flightDate.toGregorianCalendar().getTime();
			utcDate = formatter.parse(df.format(d));
		} catch (ParseException e) {
			LOG.error("Cannot parse flight date into UTC due to: {}", e.getMessage());
		}
		return utcDate;
	}
	
	/**
	 * - call from UFIS internal(UfisMsgDistributor) via AMQ
	 * - to update Departure flight's FuelTime, RequiredFuel
	 * - and Rotational flight(RKEY) in Flt_Fuel_Status
	 * @param ufisMsgDTO
	 */
	public void updateDeptFlightPlan(EntUfisMsgDTO ufisMsgDTO) {
		try{
			switch(ufisMsgDTO.getBody().getActs().get(0).getTab().trim()){
				case "FLT_FUEL_STATUS" : updateForArrFltOnBoardFuel(ufisMsgDTO); break;
				case "AFTTAB" : updateForDepFlt(ufisMsgDTO); break;
			}
		}catch(ParseException pe){
			LOG.error("When parsing the current dateTime" + pe.getMessage());
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}

	/**
	 * - UPDATE flight from UFIS CEDA (split/join) and DELETE flight
	 * - ignore (INSERT) flights(consider insertion case only from TIBCO)
	 * @param ufisMsgDTO
	 */
	private void updateForDepFlt(EntUfisMsgDTO ufisMsgDTO) throws Exception {
		String urno = null, rkey = null, adid = null;

		List<String> fld = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		
		if("UFR".equalsIgnoreCase(cmd)){//UPDATE flight
			for (int i = 0; i < fld.size(); i++) {
				switch(fld.get(i).toUpperCase()){
					case "URNO": urno = ((String) data.get(i)).trim(); break;
					case "RKEY" : rkey = ((String) data.get(i)).trim(); break;
					case "ADID" : adid = ((String) data.get(i)).trim(); break; 
				}
			}
			if(HpUfisUtils.isNullOrEmptyStr(urno) || HpUfisUtils.isNullOrEmptyStr(rkey) 
					|| HpUfisUtils.isNullOrEmptyStr(adid)){
				LOG.debug("URNO, RKEY, ADID is empty/null. No processing performed.");
				return;
			}
			if(!"D".equalsIgnoreCase(adid)){
				LOG.debug("Flight update for only departure flight will be processed. ADID <{}>. Message dropped.", adid);
				return;
			}
			updateDepFltFuelStatus(urno, rkey, false);
		}
		else if("DFR".equalsIgnoreCase(cmd)){//DELETE flight
			updateDepFltFuelStatus(urno, rkey, true);
		}
		else
			LOG.debug("No operation/update for CMD : {}", cmd);
	}
	
	/**
	 * - update to DB(Flt_Fuel_Status)
	 * - URNO == RKEY? split case. Remove fuelTime and RKEY.
	 * - URNO != RKEY? join case. Get the arr flt_fuel_status 
	 * 		and calculate fuelTime and update RKEY
	 * @param urno, rkey, isDeleted
	 * @throws ParseException
	 */
	private void updateDepFltFuelStatus(String urno, String rkey, boolean isDeleted)
			throws ParseException {
		float onBoardFuel = 0, fuelTime = 0, requiredFuel = 0;
		BigDecimal urnoCeda = new BigDecimal(urno);//dept flight urno
		BigDecimal rkeyCeda = new BigDecimal(0);

		//get existing flt_fuel record for this dept flight
		EntDbFlightFuelStatus depFltFuelDat = null;
		EntDbFlightFuelStatus depFltFuel = clsDlFltFuelStatus.findByIdFlight(urnoCeda);
		if(depFltFuel == null)
			//LOG.debug("Flight fuel record is not found for dept flight <{}>. No updating performed", depFltFuel);
			return;
		
		//SPLIT or Deleted
		if(urno.equals(rkey) || isDeleted){
			fuelTime = 0;//remove existing fuel Time and id_rflight
			rkeyCeda = new BigDecimal(0);
		}else{
			//JOIN(urno != rkey)
			rkeyCeda = new BigDecimal(rkey);
			//find the rotation(Arrival) record in flt_fuel_status
			EntDbFlightFuelStatus arrFltFuel = clsDlFltFuelStatus.findByIdFlight(rkeyCeda);
			if(arrFltFuel == null){
				LOG.debug("Flight fuel record is not found for arr flight <{}>. No updating performed.", rkeyCeda);
				return;
			}
			
			requiredFuel = depFltFuel.getRequiredFuel();
			onBoardFuel = arrFltFuel.getOnboardFuel();
			fuelTime = calculateFuelTime(onBoardFuel, requiredFuel);
		}
		
		depFltFuelDat = EntDbFlightFuelStatus.valueOf(depFltFuel);
		depFltFuelDat.setFuelTime(fuelTime);
		depFltFuelDat.setFuelRate(fuelRate);
		depFltFuelDat.setIdRFlight(rkeyCeda);
		depFltFuelDat.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
		depFltFuelDat.setUpdatedUser(HpEKConstants.LIDO_SOURCE);
		EntDbFlightFuelStatus ent = clsDlFltFuelStatus.update(depFltFuelDat);
		// notification
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true, UfisASCommands.URT, String.valueOf(ent.getIdFlight()), depFltFuel, ent);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true, UfisASCommands.URT, String.valueOf(ent.getIdFlight()), depFltFuel, ent);
		}
		if(ent != null)
			LOG.info("Updating Flight Fuel Status for id dept flight <{}> has been processed.", urnoCeda);
	}

	/**
	 * - update Arrival onBoardFuel from EGDS
	 * - get the Dep flight record for flt fuel status and update fuelTime
	 * @param ufisMsgDTO
	 */
	private void updateForArrFltOnBoardFuel(EntUfisMsgDTO ufisMsgDTO) throws Exception{
		String idFlight = null, arrFuel = null;
		float onBoardFuel = 0, fuelTime = 0, requiredFuel = 0;
		List<String> fld = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		idFlight = ufisMsgDTO.getHead().getIdFlight().get(0);
		
		for (int i = 0; i < fld.size(); i++) {
			switch(fld.get(i).toUpperCase()){
			//	case "IDFLIGHT": idFlight = data.get(i).trim(); break; //id_flight of Flt_Fuel_Status
				case "ONBOARD_FUEL" : arrFuel = ((String) data.get(i)).trim(); break; //onBoardFuel from Flt_Fuel_Status 
			}
		}
		if(idFlight == null){
			LOG.debug("IdFlight is empty/null. No processing performed.");
			return;
		}
		BigDecimal idFlt = new BigDecimal(idFlight.trim());
		/**
		 * Only consider the case that the dept flight fuel status
		 * record existed and it has rotation flight in order to update from ARR flight onBoardFuel
		 */
		//get dept flight fuel (from flt_fuel_status where id_rflight = :idFlt)
		EntDbFlightFuelStatus depFltFuelDat = null;
		EntDbFlightFuelStatus depFltFuel = clsDlFltFuelStatus.findByIdRFlight(idFlt);
		if(depFltFuel == null){
			LOG.debug("Rotation flight's fuel record for" +
					" Arr: URNO <{}> is not found. No processing performed.", idFlight);
			return;
		}
		requiredFuel = depFltFuel.getRequiredFuel();
		if(HpUfisUtils.isNullOrEmptyStr(arrFuel))
			LOG.debug("Input OnBoardFuel <{}>", arrFuel);
		else
			onBoardFuel = new Float(arrFuel);
		fuelTime = calculateFuelTime(onBoardFuel, requiredFuel);
		
		depFltFuelDat = EntDbFlightFuelStatus.valueOf(depFltFuel);
		depFltFuelDat.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
		depFltFuelDat.setUpdatedUser(HpEKConstants.LIDO_SOURCE);
		depFltFuelDat.setRequiredFuel(requiredFuel);
		depFltFuelDat.setFuelRate(fuelRate);
		depFltFuelDat.setFuelTime(fuelTime);
		
		EntDbFlightFuelStatus entity = clsDlFltFuelStatus.update(depFltFuelDat);
		// notification
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(true, UfisASCommands.URT, String.valueOf(entity.getIdFlight()), depFltFuel, entity);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(true, UfisASCommands.URT, String.valueOf(entity.getIdFlight()), depFltFuel, entity);
		}
		if(entity != null)
			LOG.info("Updating Flight Fuel Status for id dept flight <{}> has been processed.", depFltFuel.getIdFlight());
	}
	
	/**
	 * - FuelTime = (RequiredFuel - OnBoardFuel)/FuelRate;
	 * @param onBoardFuel, requiredFuel
	 * @return fuelTime
	 */
	private float calculateFuelTime(float onBoardFuel, float requiredFuel) {
		float fuelTime = 0;
		if(requiredFuel <= 0)
			LOG.debug("RequiredFuel <{}> is less than/equal 0. Fuel Time will be 0.", requiredFuel);
		else if(requiredFuel <= onBoardFuel)
			LOG.debug("RequiredFuel for dept flight is <{}> less than onBoardFuel. Fuel Time will be 0.", requiredFuel);
		else{
			float X = (requiredFuel - onBoardFuel); //X = amount of fuel to be pumped..
			fuelTime = X/fuelRate;
		}
		return fuelTime;
	}
}
