package com.ufis_as.ufisapp.ek.intf.lido;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.lido.entities.EntDbApttab;
import com.ufis_as.ek_if.lido.entities.EntDbAtrtab;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlAiportWeatherBean;
import com.ufis_as.ufisapp.ek.eao.DlAtrtabBean;
import com.ufis_as.ufisapp.ek.eao.DlFltFuelStatusBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.lido.LidoMsgInfo;
/**
 * @author BTR
 * @version 1:					BTR - Initial implementation as per design doc
 * @version 1.1.0:	2013-10-18	BTR - Modified for internal exception handling to UFISDATASTORE
 * 								   
 * @version 1.1.1:	2013-10-20	BTR - NOTAMS encoding the \n to ceda format.
 * @version 1.1.2:	2013-10-22	BTR - NOTAMS encoding the \n to one character
 * 									- Format the MsgSendDate in WEATHER broadcast json msg. 
 */

@Stateless
public class BlHandleLidoNotamsBean extends BaseDTO{

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleLidoNotamsBean.class);
	private JAXBContext _cnxJaxb;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private BlIrmtabFacade clsBlIrmtabFacade;
	@EJB
	private DlFltFuelStatusBean clsDlFltFuelStatus;
	@EJB
	private DlAiportWeatherBean clsDlAirportWeather;
	@EJB
	DlAtrtabBean clsDlAtrtab;
	/*@EJB
	BlUfisMsgFormatter clsBlUfisMsgFormatter;*/
	@EJB
	BlUfisCedaQueue clsUfisCedaQ;

	float fuelRate = 0;
	String blockFuelStr = "0";//set default for dept flight
	String msg = null, airline = null,
		   org3 = null, releaseIndicator = null;
	String notamsId, effectFrom, effectTil, notamsStatus, notamsICAO, notamsIATA, rema;
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(LidoMsgInfo.class);
			_um = _cnxJaxb.createUnmarshaller();
			fuelRate = clsEntStartUpInitSingleton.getFuelRate();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	/**
	 * - extract Air-Traffic data from input data
	 * - and pass the input to CEDA in JSON format 
	 * @param _input
	 * @return
	 */
	public boolean processNOTAMS(LidoMsgInfo _input, long irmtabRef){
		try{
		  	// clear
	    	data.clear();
	    	
	    	// set urno of irmtab
	    	irmtabUrno = irmtabRef;
			if(!validateNotams(_input))
				return false;
			String cmd = UfisASCommands.IRT.toString();
			
			//'D', effect from/till are empty.
			if(!"D".equalsIgnoreCase(notamsStatus)){
				effectFrom = convertFlDateToUTC(effectFrom);
				effectTil = convertFlDateToUTC(effectTil);
			}
			
			EntDbAtrtab entResult = null;
			if (!clsEntStartUpInitSingleton.isAppendNotamsFlag()) {
				//find in db only when command is not INSERT..
				if (!"I".equalsIgnoreCase(notamsStatus)) {
					// UPDATE or DELETE
					entResult = clsDlAtrtab.findByMsid(notamsId);//find existing
					if ("D".equalsIgnoreCase(notamsStatus))
						cmd = UfisASCommands.DRT.toString();
					else
						cmd = UfisASCommands.URT.toString();
				}
			}else
				LOG.debug("Append record is TRUE in config. Input will be inserted.");
			
			if (entResult == null) {
				if (UfisASCommands.URT.toString().equals(cmd) 
						|| UfisASCommands.DRT.toString().equals(cmd)){
					LOG.debug("Notams id <{}> is not found to update/delete. Message dropped.", notamsId);
					addExptInfo(EnumExceptionCodes.ENREC.name(), "Notams id=" + notamsId + " not found");
					return false;
				}
			}
			List<EntUfisMsgACT> listAction = formatData(entResult, cmd);
			EntUfisMsgHead header = formatJsonHeader();
			
			String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
			// revert to get the line feed.
			msg = StringEscapeUtils.unescapeJava(msg);
			
//			msg = HpUfisUtils.encodeCeda(msg);
			//msg = HpUfisUtils.decodeCeda(msg);
			clsUfisCedaQ.sendMessage(msg);// send to CEDA
			
		} catch (ParseException e) {
			LOG.error("DateTime parsing error {}", e.toString());
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
		return true;
	}
	
	private EntUfisMsgHead formatJsonHeader() {
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setUsr(HpEKConstants.LIDO_SOURCE);
		header.setApp(HpEKConstants.LIDO_SOURCE);
		header.setOrig(HpEKConstants.LIDO_SOURCE);
		header.setDest(HpEKConstants.LIDO_SOURCE);
		HpUfisCalendar cur = new HpUfisCalendar(new Date());
		header.setReqt(cur.getCedaString());
		
		return header;
	}

	/**
	 * Field list includes (APC3,APC4,VAFR,VATO,TIFR,TITO,FREQ,URNO)
	 * @param entAtrtab, cmd
	 */
	private List<EntUfisMsgACT> formatData(EntDbAtrtab entAtrtab, String cmd) {
		List<String> fldList = new ArrayList<>();
		List<Object> valueList = new ArrayList<>();
		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		
		//fields to INSERT
		fldList.add("APC3");
		fldList.add("APC4");
		fldList.add("VAFR");
		fldList.add("VATO");
		
		valueList.add(notamsIATA);
		valueList.add(notamsICAO);
		valueList.add(effectFrom);
		valueList.add(effectTil);
		
		action.setCmd(cmd);
		
		if(entAtrtab != null){
			//fields to UPDATE
			String urno = String.valueOf(entAtrtab.getUrno());
			/*fldList.add("URNO");
			valueList.add(urno);*/

			action.setSel("WHERE URNO = "+ urno);
		}
			
		//fields for all IRT/URT/DRT
		fldList.add("REST");
		fldList.add("REMA");
		fldList.add("MSID");

		valueList.add(HpEKConstants.EK_LIDO_NOTAMS);
		valueList.add(rema);
		valueList.add(notamsId);
		
		action.setTab("ATRTAB");
		action.setFld(fldList);
		action.setData(valueList);
		
		listAction.add(action);
		return listAction;
	}
	
	private boolean validateNotams(LidoMsgInfo _input) 
			throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		String subType = _input.getMeta().getMessageSubtype().trim();
		String source = _input.getMeta().getMessageSource().trim();
		msg = _input.getMessageDetails().trim();
		
		if(_input.getMeta().getMessageTime() == null
				|| HpUfisUtils.isNullOrEmptyStr(subType)
				|| HpUfisUtils.isNullOrEmptyStr(source)
				|| HpUfisUtils.isNullOrEmptyStr(msg)){
			LOG.warn("Input message META mandatory info are missing. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "Input message mandatory info are missing.");
			return false;
		}
		/*if(!"UPD".equals(subType)){
			LOG.warn("Message subType should be UPD. Message dropped.");
			return false;
		}*/
		if(!"Lido".equals(source)){
			LOG.warn("Message source should be Lido. Message dropped");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Message source not correct: " + source);
			return false;
		}
		
		notamsId = msg.substring(19, 29).trim();
		effectFrom = msg.substring(53, 68).trim();
		effectTil = msg.substring(68, 83).trim();
		notamsStatus = msg.substring(83, 84);
		rema = msg;
		
		//validate Madatory data
		if(HpUfisUtils.isNullOrEmptyStr(notamsId) 
				|| HpUfisUtils.isNullOrEmptyStr(notamsStatus)){
			LOG.warn("Input message detail mandatory info are missing. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "notamsId and notamsStatus are mandatory, cannot be null or empty");
			return false;
		}
		if(!notamsStatus.matches("I|U|D")){
			LOG.debug("Update Status in message detail is none of I or U or D. Message dropped.");
			addExptInfo(EnumExceptionCodes.EWVAL.name(), "Udpate status must be I, U or D");
			return false;
		}
		//check if the Status is D, and no need to Validate later info
		if("D".equalsIgnoreCase(notamsStatus)){
			return true;
		}
		
		if(	HpUfisUtils.isNullOrEmptyStr(effectFrom)
				|| HpUfisUtils.isNullOrEmptyStr(effectTil)){
			LOG.warn("Input msg effective From and Till are missing. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "Effective from and till cannot be null or empty");
			return false;
		}
		String[] arrMsg = msg.split("\\n");
		String airlineMsg = null;
		for(String airline : arrMsg){
			airline = airline.trim();
			if(!airline.isEmpty())
				if(airline.charAt(0) == 'A' && airline.charAt(1) == ')'){
					airlineMsg = airline; break;
				}
		}
		if(airlineMsg == null){
			LOG.debug("Airline code ICAO is not found in input. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "airline code cannot be null or empty");
			return false;
		}
		
		notamsICAO = airlineMsg.substring(2, airlineMsg.length()).trim();
		LOG.debug("ICAO : "+ notamsICAO);
			
		//check for ICAO in APTTAB
		boolean isICAOFound = false;
		List<EntDbApttab> mdApttabList = clsEntStartUpInitSingleton.getCacheApt();
		for(EntDbApttab apt : mdApttabList){
			if(notamsICAO.equalsIgnoreCase(apt.getApc4().trim())){
				notamsIATA = apt.getApc3();
				isICAOFound = true; break;
			}
		}
		if(!isICAOFound){
			LOG.debug("Input airline ICAO is not found in APTTAB. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "ICAO airline code cannot be null or empty");
			return false;
		}
		return true;
	}
	
	/**
	 * @param String flightDate(ddMMMyyyyHHmmss)
	 * @return String CEDA flight date(yyyyMMddHHmmss) in UTC
	 */
	private String convertFlDateToUTC(String flightDate)
			throws ParseException {
		SimpleDateFormat dfm = new SimpleDateFormat("ddMMMyyyyHHmmss");
		Date flDate =  dfm.parse(flightDate);
		HpUfisCalendar utcDate = new HpUfisCalendar(flDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();
	}
}
