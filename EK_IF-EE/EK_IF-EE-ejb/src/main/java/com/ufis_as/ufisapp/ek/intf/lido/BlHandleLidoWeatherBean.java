package com.ufis_as.ufisapp.ek.intf.lido;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.lido.entities.EntDbAirportWeather;
import com.ufis_as.ek_if.lido.entities.EntDbApttab;
import com.ufis_as.ek_if.lido.entities.EntDbAtrtab;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlAiportWeatherBean;
import com.ufis_as.ufisapp.ek.eao.DlAtrtabBean;
import com.ufis_as.ufisapp.ek.eao.DlFltFuelStatusBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.lido.LidoMsgInfo;
/**
 * @author BTR
 * @version 1:					BTR - Initial implementation as per design doc
 * @version 1.1.0:	2013-10-18	BTR 1.) Modified for internal exception handling to UFISDATASTORE
 * 								   2.) 
 */

@Stateless
public class BlHandleLidoWeatherBean extends BaseDTO{

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleLidoWeatherBean.class);
//	private JAXBContext _cnxJaxb;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private BlIrmtabFacade clsBlIrmtabFacade;
	@EJB
	private DlFltFuelStatusBean clsDlFltFuelStatus;
	@EJB
	private DlAiportWeatherBean clsDlAirportWeather;
	@EJB
	DlAtrtabBean clsDlAtrtab;
	/*@EJB
	BlUfisMsgFormatter clsBlUfisMsgFormatter;*/
	@EJB
	BlUfisCedaQueue clsUfisCedaQ;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	float fuelRate = 0;
	String blockFuelStr = "0";//set default for dept flight
	String msg = null, airline = null,
		   org3 = null, releaseIndicator = null;
	String notamsId, effectFrom, effectTil, notamsStatus, notamsICAO, notamsIATA, rema;
	
	private boolean isKeepAllStn = false;
	private boolean isAppend = false;
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(LidoMsgInfo.class);
			_um = _cnxJaxb.createUnmarshaller();
			fuelRate = clsEntStartUpInitSingleton.getFuelRate();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
			isKeepAllStn = clsEntStartUpInitSingleton.isKeepAllStnFlag();
			isAppend = clsEntStartUpInitSingleton.isAppendWeatherFlag();
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	public boolean processWeather(LidoMsgInfo _input, long irmtabRef){
		/** Perform weather processing **/
		try{
		  	// clear
	    	data.clear();
	    	
	    	// set urno of irmtab
	    	irmtabUrno = irmtabRef;
			String subType = _input.getMeta().getMessageSubtype().trim();
			String source = _input.getMeta().getMessageSource().trim();
			String input = _input.getMessageDetails().trim();
		//	boolean flagDxbStn = false;//to log DXB station existence
			LOG.info("Message SubType: {}", subType);
			LOG.info("Source: {}", source);
			
			if(_input.getMeta().getMessageTime() == null
					|| HpUfisUtils.isNullOrEmptyStr(subType)
					|| HpUfisUtils.isNullOrEmptyStr(source)
					|| HpUfisUtils.isNullOrEmptyStr(input)){
				LOG.warn("Input message mandatory info are missing. Message dropped.");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "Input message mandatory info are missing.");
				return false;
			}
			if(!"Lido".equalsIgnoreCase(source)){
				LOG.warn("Message source should be Lido. Message dropped");
				addExptInfo(EnumExceptionCodes.EWVAL.name(), "Message source not correct: " + source);
				return false;
			}
			
			input = input.substring(12, input.length());
			String[] subMsg = input.split("\\=");
			int count = subMsg.length;
			LOG.debug("Input Weather info contains for <{}> stations", count);
		
			LOG.info("Keep the weather info for all station or only DXB: {}", isKeepAllStn);
			LOG.debug("Loop the stations");
			for (int k = 0; k < count; k++) {
				String msg = subMsg[k];
				String icao = msg.substring(0, 4).trim();
				String iata = msg.substring(4, 9).trim();
				String weatherIndent = msg.substring(9, 11).trim();
				LOG.info("ICAO: {}", icao);
				LOG.info("Weather indent: {}", weatherIndent);
				LOG.info("IATA: {}", iata);
				
				if(HpUfisUtils.isNullOrEmptyStr(icao)){
					LOG.debug("ICAO is null or empty. Skipped");
					continue;
				}
				if (!weatherIndent.matches("FA|FK|FV|UA|UC|UO|US|WA|WS|WC|WF|WU|WV|WW")) {
					if(HpUfisUtils.isNullOrEmptyStr(iata)){
						LOG.debug("WeatherIndent not matched and IATA is null or empty, skipped");
						continue;
					}
				} else {
					LOG.debug("Weather indent matched");
				}
				//keep only for DXB??
				if(!isKeepAllStn){
					if(!HpEKConstants.EK_HOPO.equalsIgnoreCase(iata)
							&& !HpEKConstants.EK_APC4.equalsIgnoreCase(icao)){
						//flagDxbStn = true;
						//LOG.info("Input icao is not <{}>. Skipped <{}>", HpEKConstants.EK_APC4, icao);
						LOG.debug("station is not matched with DXB and OMDB, skipped");
						continue;						
					} else {
						LOG.debug("DXB or OMDB is found");
					}
				}
				
				//flagDxbStn = false;
				EntDbAirportWeather entMET = null;
				String createdUser = msg.substring(20, 21).trim();
				String observeTime = msg.substring(11, 17).trim();
				String weatherRemark = msg.substring(17, 20).trim();
				String validStart = msg.substring(21, 27).trim();
				String validEnd = msg.substring(27, 33).trim();
				String weatherText = msg.substring(33, msg.length());
				
				if (HpUfisUtils.isNullOrEmptyStr(observeTime)) {
					LOG.debug("ObserveTime is null or empty, skipped");
					continue;
				}
				if (HpUfisUtils.isNullOrEmptyStr(createdUser)) {
					LOG.debug("CreatedUser is null or empty, skipped");
					continue;
				}
				if (HpUfisUtils.isNullOrEmptyStr(weatherText)) {
					LOG.debug("WeatherText is null or empty, skipped");
					continue;
				}
				
				LOG.info("Create User Code: {}", createdUser);
				switch(createdUser){
				case "A" : createdUser = HpEKConstants.A_INPUT_OFFICE; break;
				case "C" : createdUser = HpEKConstants.C_INPUT_OFFICE; break;
				case "D" : createdUser = HpEKConstants.D_INPUT_OFFICE; break;
				case "I" : createdUser = HpEKConstants.I_INPUT_OFFICE; break;
				case "L" : createdUser = HpEKConstants.L_INPUT_OFFICE; break;
				case "M" : createdUser = HpEKConstants.M_INPUT_OFFICE; break;
				case "S" : createdUser = HpEKConstants.S_INPUT_OFFICE; break;
				case "T" : createdUser = HpEKConstants.T_INPUT_OFFICE; break;
				default : {
							LOG.debug("Input Office for CreatedUser is unknown. Default value LIDO is set.");
							createdUser = HpEKConstants.LIDO_SOURCE; break;
						}
				}
				LOG.info("Create User info: {}", createdUser);
				
				//check icao and iata from Master data table APTTAB
				List<EntDbApttab> aptList = clsEntStartUpInitSingleton.getCacheApt();
				//to decide input icao and iata are correct checking with MD table APTTAB
				boolean flag = false; 
				if (aptList != null) {
					for (int i = 0; i < aptList.size(); i++) {
						EntDbApttab entity = aptList.get(i);
						if (icao.equals(entity.getApc4())) {
								flag = true;
								break;
						}
					}
				}
				
				if(!flag){
					LOG.debug("Input ICAO is unknown(Not exist in APTTAB). Skipped input icao <{}>.", icao);
					continue;
				}
				
				//if append = false, insert or update; append = true, only insert
				EntDbAirportWeather weather = null;
				UfisASCommands cmd = UfisASCommands.URT;
				LOG.debug("Append: {}", isAppend);
				if(!isAppend){
					entMET = clsDlAirportWeather.findExistingByIcaoWeatherIndent(icao, weatherIndent);
					if(entMET == null){
						 //entMET = new EntDbAirportWeather();
						weather = new EntDbAirportWeather();
						weather.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
						weather.setCreatedUser(createdUser);
						cmd = UfisASCommands.IRT;
					}else {
						weather = EntDbAirportWeather.valueOf(entMET);
						weather.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
						weather.setUpdatedUser(HpEKConstants.LIDO_SOURCE);
					}
				}
				else{
					// no matter data existing or not, directly store in db
					weather = new EntDbAirportWeather();
					weather.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					weather.setCreatedUser(createdUser);
					cmd = UfisASCommands.IRT;
				}
				
				weather.setMsgSendDate(convertFlDateToUTC(_input.getMeta().getMessageTime()));
				weather.setAirportCodeIata(iata);
				weather.setAirportCodeIcao(icao);
				weather.setWeatherIndent(weatherIndent);
				weather.setObserveTime(observeTime);
				weather.setWeatherRemark(weatherRemark);
				weather.setValidStart(validStart);
				weather.setValidEnd(validEnd);
				weather.setWeatherText(weatherText);
				weather.setDataSource(HpEKConstants.LIDO_SOURCE);
				
				//merge the input airport weather value to db
				EntDbAirportWeather entity = clsDlAirportWeather.merge(weather);
				LOG.info("Airport Weather for icao : <{}> has been processed.", icao);
				
				// notification
				if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
					// send notification message to topic
					ufisTopicProducer.sendNotification(true, cmd, null, entMET, entity);
				} else {
					// if topic not defined, send notification to queue
					ufisQueueProducer.sendNotification(true, cmd, null, entMET, entity);
				}
				/*if(entity != null)
					LOG.info("Airport Weather for icao : <{}> has been processed.", icao);*/
				//break;
				/*clsDlAirportWeather.persist(entMET);
				LOG.info("Airport Weather has been processed.");*/
			}
		/*	if(flagDxbStn)
				LOG.debug("Weather info for DXB station is not included.");*/
		}catch(ParseException e){
			LOG.error("Date time parsing Error : {}", e);
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
		return true;
	}
	
	/**
	 * - extract Air-Traffic data from input data
	 * - and pass the input to CEDA in JSON format 
	 * @param _input
	 * @return
	 */
	@Deprecated
	public boolean processNOTAMS(LidoMsgInfo _input){
		try{
			if(!validateNotams(_input))
				return false;
			String cmd = UfisASCommands.IRT.toString();
			
			//'D', effect from/till are empty.
			if(!"D".equalsIgnoreCase(notamsStatus)){
				effectFrom = convertFlDateToUTC(effectFrom);
				effectTil = convertFlDateToUTC(effectTil);
			}
			
			EntDbAtrtab entResult = null;
			if (!clsEntStartUpInitSingleton.isAppendNotamsFlag()) {
				//find in db only when command is not INSERT..
				if (!"I".equalsIgnoreCase(notamsStatus)) {
					// UPDATE or DELETE
					entResult = clsDlAtrtab.findByMsid(notamsId);//find existing
					if ("D".equalsIgnoreCase(notamsStatus))
						cmd = UfisASCommands.DRT.toString();
					else
						cmd = UfisASCommands.URT.toString();
				}
			}else
				LOG.debug("Append record is TRUE in config. Input will be inserted.");
			
			if (entResult == null) {
				if (UfisASCommands.URT.toString().equals(cmd) 
						|| UfisASCommands.DRT.toString().equals(cmd)){
					LOG.debug("Notams id <{}> is not found to update/delete. Message dropped.", notamsId);
					return false;
				}
			}
			List<EntUfisMsgACT> listAction = formatData(entResult, cmd);
			EntUfisMsgHead header = formatJsonHeader();
			
			String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
			clsUfisCedaQ.sendMessage(msg);// send to CEDA
			
		} catch (ParseException e) {
			LOG.error("DateTime parsing error {}", e.toString());
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
		return true;
	}
	
	private EntUfisMsgHead formatJsonHeader() {
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setUsr(HpEKConstants.LIDO_SOURCE);
		header.setApp(HpEKConstants.LIDO_SOURCE);
		header.setOrig(HpEKConstants.LIDO_SOURCE);
		header.setDest(HpEKConstants.LIDO_SOURCE);
		HpUfisCalendar cur = new HpUfisCalendar(new Date());
		header.setReqt(cur.getCedaString());
		
		return header;
	}

	/**
	 * Field list includes (APC3,APC4,VAFR,VATO,TIFR,TITO,FREQ,URNO)
	 * @param entAtrtab, cmd
	 */
	private List<EntUfisMsgACT> formatData(EntDbAtrtab entAtrtab, String cmd) {
		List<String> fldList = new ArrayList<>();
		List<Object> valueList = new ArrayList<>();
		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		
		//fields to INSERT
		fldList.add("APC3");
		fldList.add("APC4");
		fldList.add("VAFR");
		fldList.add("VATO");
		
		valueList.add(notamsIATA);
		valueList.add(notamsICAO);
		valueList.add(effectFrom);
		valueList.add(effectTil);
		
		action.setCmd(cmd);
		
		if(entAtrtab != null){
			//fields to UPDATE
			String urno = String.valueOf(entAtrtab.getUrno());
			/*fldList.add("URNO");
			valueList.add(urno);*/

			action.setSel("WHERE URNO = "+ urno);
		}
			
		//fields for all IRT/URT/DRT
		fldList.add("REST");
		fldList.add("REMA");
		fldList.add("MSID");

		valueList.add(HpEKConstants.EK_LIDO_NOTAMS);
		valueList.add(rema);
		valueList.add(notamsId);
		
		action.setTab("ATRTAB");
		action.setFld(fldList);
		action.setData(valueList);
		
		listAction.add(action);
		return listAction;
	}
	
	@Deprecated
	private boolean validateNotams(LidoMsgInfo _input) 
			throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		String subType = _input.getMeta().getMessageSubtype().trim();
		String source = _input.getMeta().getMessageSource().trim();
		msg = _input.getMessageDetails().trim();
		
		if(_input.getMeta().getMessageTime() == null
				|| HpUfisUtils.isNullOrEmptyStr(subType)
				|| HpUfisUtils.isNullOrEmptyStr(source)
				|| HpUfisUtils.isNullOrEmptyStr(msg)){
			LOG.warn("Input message META mandatory info are missing. Message dropped.");
			addExptInfo(EnumExceptionCodes.EMAND.name(), "Input message mandatory info are missing.");
			return false;
		}
		/*if(!"UPD".equals(subType)){
			LOG.warn("Message subType should be UPD. Message dropped.");
			return false;
		}*/
		if(!"Lido".equals(source)){
			LOG.warn("Message source should be Lido. Message dropped");
			return false;
		}
		
		notamsId = msg.substring(19, 29).trim();
		effectFrom = msg.substring(53, 68).trim();
		effectTil = msg.substring(68, 83).trim();
		notamsStatus = msg.substring(83, 84);
		rema = msg;
		
		//validate Madatory data
		if(HpUfisUtils.isNullOrEmptyStr(notamsId) 
				|| HpUfisUtils.isNullOrEmptyStr(notamsStatus)){
			LOG.warn("Input message detail mandatory info are missing. Message dropped.");
			return false;
		}
		if(!notamsStatus.matches("I|U|D")){
			LOG.debug("Update Status in message detail is none of I or U or D. Message dropped.");
			return false;
		}
		//check if the Status is D, and no need to Validate later info
		if("D".equalsIgnoreCase(notamsStatus)){
			return true;
		}
		
		if(	HpUfisUtils.isNullOrEmptyStr(effectFrom)
				|| HpUfisUtils.isNullOrEmptyStr(effectTil)){
			LOG.warn("Input msg effective From and Till are missing. Message dropped.");
			return false;
		}
		String[] arrMsg = msg.split("\\n");
		String airlineMsg = null;
		for(String airline : arrMsg){
			airline = airline.trim();
			if(!airline.isEmpty())
				if(airline.charAt(0) == 'A' && airline.charAt(1) == ')'){
					airlineMsg = airline; break;
				}
		}
		if(airlineMsg == null){
			LOG.debug("Airline code ICAO is not found in input. Message dropped.");
			return false;
		}
		
		notamsICAO = airlineMsg.substring(2, airlineMsg.length()).trim();
		LOG.debug("ICAO : "+ notamsICAO);
			
		//check for ICAO in APTTAB
		boolean isICAOFound = false;
		List<EntDbApttab> mdApttabList = clsEntStartUpInitSingleton.getCacheApt();
		for(EntDbApttab apt : mdApttabList){
			if(notamsICAO.equalsIgnoreCase(apt.getApc4().trim())){
				notamsIATA = apt.getApc3();
				isICAOFound = true; break;
			}
		}
		if(!isICAOFound){
			LOG.debug("Input airline ICAO is not found in APTTAB. Message dropped.");
			return false;
		}
		return true;
	}
	
	/**
	 * @param String flightDate(ddMMMyyyyHHmmss)
	 * @return String CEDA flight date(yyyyMMddHHmmss) in UTC
	 */
	private String convertFlDateToUTC(String flightDate)
			throws ParseException {
		SimpleDateFormat dfm = new SimpleDateFormat("ddMMMyyyyHHmmss");
		Date flDate =  dfm.parse(flightDate);
		HpUfisCalendar utcDate = new HpUfisCalendar(flDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();
	}

	/**
	 * @param flightDate
	 * @return
	 * @throws ParseException
	 */
	private Date convertFlDateToUTC(XMLGregorianCalendar flightDate)
			throws ParseException {
		
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = flightDate.toGregorianCalendar().getTime();
		return formatter.parse(df.format(utcDate));
	}
}
