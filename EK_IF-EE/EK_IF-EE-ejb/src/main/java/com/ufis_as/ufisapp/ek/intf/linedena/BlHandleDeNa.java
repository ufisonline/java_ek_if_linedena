package com.ufis_as.ufisapp.ek.intf.linedena;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.linedena.entities.EntDbAircraftOpsMsg;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.LineDeNA.OpsFlashMsg;

/**
 * 
 * @author SCH
 * 
 */
@Stateless
public class BlHandleDeNa  {
	// @EJB
	
    @EJB
    private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlIrmtabFacade _irmfacade;
	
	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleDeNa.class);
	private OpsFlashMsg opsFlashMsg;
	private EntDbAircraftOpsMsg entDbAircraftOpsMsg;
	private String subtype;
	
    protected String xmlMsg = "";
    protected Message rawMsg;
    protected InterfaceConfig _interfaceConfig;
    protected Unmarshaller _um;
    
    
	 private String logLevel = null;
	private Boolean msgLogged = Boolean.FALSE;
	private long irmtabRef;

//	public BIHandleDeNa(Marshaller marshaller, Unmarshaller um,
//			InterfaceConfig interfaceConfig) {
//		super(marshaller, um, interfaceConfig);
//		// In General it is an Update msg
//		// unless flightStatus is X
//		_msgActionType = ACTIONTYPE.U;
//	}
	public void init(Unmarshaller um){
//		this._ms = ms;
		this._um = um;
	}

	public boolean unMarshal() {

		try {
			if (xmlMsg.contains("OpsFlashMsg")) {
				JAXBElement<OpsFlashMsg> data = _um.unmarshal(new StreamSource(
						new StringReader(xmlMsg)), OpsFlashMsg.class);
				opsFlashMsg = (OpsFlashMsg) data.getValue();
				if (readStoreLineDeNa(opsFlashMsg)) {
					LOG.debug("OpsFlashMsg {}", opsFlashMsg.getFlightId()
							.getFltNo());
					
					sendErroNotification(EnumExceptionCodes.EXSDF);
					return true;
				}else{
					return false;
				}
				
			}

		} catch (JAXBException ex) {

			LOG.error("Flight Event JAXB exception {}", ex.toString());
			LOG.error("Dropped Message details: \n{}", xmlMsg);
			return false;
		}
		return false;

	}

	public boolean readEvent(OpsFlashMsg opsFlashMsg) {
		return false;
	}

	public boolean readStoreLineDeNa(OpsFlashMsg opsFlashMsg) {
		if (opsFlashMsg != null) {
			
			   logLevel = entStartupInitSingleton.getIrmLogLev();
			
			EntDbAircraftOpsMsg edpiod = new EntDbAircraftOpsMsg();

			/* Meta */
			if (opsFlashMsg.getMeta() != null) {
				// set MsgSendDate
				if (opsFlashMsg.getMeta().getMessageTime() != null) {
					edpiod.setMsgSendDate(convertXMLGregorianCalendarToDate(opsFlashMsg
							.getMeta().getMessageTime()));
				} else {
					LOG.info("Mandatory field Message Time is null, Line DeNA message is rejected");
					
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set ID
				edpiod.setId((UUID.randomUUID()).toString());

				// // set messageId
				// edpiod.setMsgId(new
				// BigDecimal(opsFlashMsg.getMeta().getMessageID()));

				// check Type
				if (opsFlashMsg.getMeta().getType() == null) {
					LOG.info("Mandatory field Message Type is null, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}
				if (!"OpsFlash".equalsIgnoreCase(opsFlashMsg.getMeta()
						.getType())) {
					LOG.info("Mandatory field Message Type is not OpsFlash, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set subtype
				if (opsFlashMsg.getMeta().getSubtype() != null) {
					subtype = opsFlashMsg.getMeta().getSubtype();
				} else {
					LOG.info("Mandatory field subtype is null, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}
				if (!("UPD"
						.equalsIgnoreCase(opsFlashMsg.getMeta().getSubtype())
						|| "INS".equalsIgnoreCase(opsFlashMsg.getMeta()
								.getSubtype()) || "DEL"
							.equalsIgnoreCase(opsFlashMsg.getMeta()
									.getSubtype()))) {
					LOG.info("Mandatory field subtype is nither UPD, INS or DEL, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// check source
				if (opsFlashMsg.getMeta().getSource() == null) {
					LOG.info("Mandatory field Message Source is null, Line DeNA message is rejected");
					return false;
				}
				if (!"DeNA".equalsIgnoreCase(opsFlashMsg.getMeta().getSource())) {
					LOG.info("Mandatory field Message Source is not DeNA, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

			} else {
				LOG.info("Mandatory field Meta is null, Line DeNA message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND);
				return false;
			}

			/* FlightId */
			if (opsFlashMsg.getFlightId() != null) {
				if (opsFlashMsg.getFlightId().getCxCd() != null
						&& opsFlashMsg.getFlightId().getFltNo() != null
						&& !opsFlashMsg.getFlightId().getCxCd().isEmpty()
						&& !opsFlashMsg.getFlightId().getFltNo().isEmpty()) {
					// set flight number
					edpiod.setFlightNumber(opsFlashMsg.getFlightId().getCxCd()
							.trim()
							+ HpUfisUtils.formatCedaFltn(opsFlashMsg
									.getFlightId().getFltNo().trim()));
				}

				// set flt date, need to convert local time to UTC time
				// haven't convert the time to UTC yet !!!!!! later
				edpiod.setFltDate(convertXMLGregorianCalendarToDate(opsFlashMsg
						.getFlightId().getFltDate()));

				// set arr dep flag
				if (opsFlashMsg.getFlightId().getArrDepFlag() != null
						&& ("A".equals(opsFlashMsg.getFlightId()
								.getArrDepFlag()) || "D".equals(opsFlashMsg
								.getFlightId().getArrDepFlag()))) {
					edpiod.setArrDepFlag(opsFlashMsg.getFlightId()
							.getArrDepFlag());
				} else {
					edpiod.setArrDepFlag("");
				}

				// set flt regn
				if (opsFlashMsg.getFlightId().getACRegNo() != null
						&& !opsFlashMsg.getFlightId().getACRegNo().isEmpty()) {
					edpiod.setFltRegn(opsFlashMsg.getFlightId().getACRegNo());
				} else {
					LOG.info("Mandatory field Aircraftreg is null or empty, Line DeNA message opsFlashMsg.getFlightId().getACRegNo() is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

			}

			/* OpFlashDetails */
			if (opsFlashMsg.getOpsFlashDetails() != null) {

				// set depaartment
				if (opsFlashMsg.getOpsFlashDetails().getDepartment() != null) {
					// edpiod.setCreatedUser(opsFlashMsg.getOpsFlashDetails().getDepartment());
					// edpiod.setUpdatedUser(opsFlashMsg.getOpsFlashDetails().getDepartment());
					edpiod.setDeptName(opsFlashMsg.getOpsFlashDetails()
							.getDepartment());
				} else {
					LOG.info("Mandatory field Department is null or empty, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// // set create/update user
				// edpiod.setCreatedUser("DeNA");
				// edpiod.setUpdatedUser("DeNA");

				// set subject
				if (opsFlashMsg.getOpsFlashDetails().getSubject() != null) {
					edpiod.setMsgSubject(opsFlashMsg.getOpsFlashDetails()
							.getSubject());
				} else {
					LOG.info("Mandatory field Subject is null or empty, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set reason
				if (opsFlashMsg.getOpsFlashDetails().getReason() != null
						&& !opsFlashMsg.getOpsFlashDetails().getReason()
								.isEmpty()) {
					edpiod.setOpsReason(opsFlashMsg.getOpsFlashDetails()
							.getReason());
				} else {
					LOG.info("Mandatory field Reason is null or empty, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set defect details
				if (opsFlashMsg.getOpsFlashDetails().getDefectDetails() != null
						&& !opsFlashMsg.getOpsFlashDetails().getDefectDetails()
								.isEmpty()) {
					edpiod.setDefectDetails(opsFlashMsg.getOpsFlashDetails()
							.getDefectDetails());
				} else {
					edpiod.setDefectDetails("");
				}

				// set add time reqd
				if (opsFlashMsg.getOpsFlashDetails().getAddTimeFromStd() != null
						&& !opsFlashMsg.getOpsFlashDetails()
								.getAddTimeFromStd().isEmpty()) {
					edpiod.setAddTimeReqd(opsFlashMsg.getOpsFlashDetails()
							.getAddTimeFromStd());
				} else {
					edpiod.setAddTimeReqd("");
				}

				// set next upd date
				if (opsFlashMsg.getOpsFlashDetails().getNextUpdate() != null) {
					edpiod.setNextUpdDate(convertXMLGregorianCalendarToDate(opsFlashMsg
							.getOpsFlashDetails().getNextUpdate()));
				}
				// }else{
				// LOG.info("Mandatory field next upd date is null, Line DeNA message is rejected");
				// return false;
				// }

				// set assist reqd
				if (opsFlashMsg.getOpsFlashDetails().getAssistRequired() != null) {
					edpiod.setAssistReqd(opsFlashMsg.getOpsFlashDetails()
							.getAssistRequired());
				} else {
					edpiod.setAssistReqd("");
				}

				// set action reqd
				if (opsFlashMsg.getOpsFlashDetails().getActionRequired() != null) {
					edpiod.setActionReqd(opsFlashMsg.getOpsFlashDetails()
							.getActionRequired());
				} else {
					edpiod.setActionReqd("");
				}

				// set publishto
				if (opsFlashMsg.getOpsFlashDetails().getPublishTo() != null
						&& !opsFlashMsg.getOpsFlashDetails().getPublishTo()
								.isEmpty()) {
					edpiod.setPublishTo(opsFlashMsg.getOpsFlashDetails()
							.getPublishTo());
				} else {
					LOG.info("Mandatory field publish to is null, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set msg seqn num
				if (opsFlashMsg.getOpsFlashDetails().getOpsSeq() != 0) {
					edpiod.setMsgSeqnNum(new BigDecimal(opsFlashMsg
							.getOpsFlashDetails().getOpsSeq()));
				} else {
					LOG.info("Mandatory field msg seqn num is null, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

				// set final msg flag
				if (opsFlashMsg.getOpsFlashDetails().getCloseMsgSeq() != null) {
					edpiod.setCloseMsgFlag(opsFlashMsg.getOpsFlashDetails()
							.getCloseMsgSeq());
				} else {
					edpiod.setCloseMsgFlag("");
				}

				// set msg post date
				// need to convert local to UTC later
				if (opsFlashMsg.getOpsFlashDetails().getMsgPostedTime() != null) {
					edpiod.setMsgPostDate(convertXMLGregorianCalendarToDate(opsFlashMsg
							.getOpsFlashDetails().getMsgPostedTime()));
				} else {
					LOG.info("Mandatory fieldmsg post date is null, Line DeNA message is rejected");
					sendErroNotification(EnumExceptionCodes.EMAND);
					return false;
				}

			} else {
				LOG.info("Mandatory field OpFlashDetails is null, Line DeNA message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND);
				return false;
			}

			entDbAircraftOpsMsg = (EntDbAircraftOpsMsg) SerializationUtils
					.clone(edpiod);

			return true;
		}

		return false;
	}

	private Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar calendar) {
		Date result = null;
		if (calendar != null) {

			HpUfisCalendar cal = new HpUfisCalendar(calendar);
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			DateFormat df2 = new SimpleDateFormat("yyyyMMddHHmmss");
			df.setTimeZone(HpEKConstants.utcTz);
			String date = df.format(cal.getTime());
			try {
				result = df2.parse(date);
			} catch (ParseException e) {
				LOG.debug("timezone convertion erro" + e);
			}
		}

		// //
		// HpEKConstants.DENA_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		// calendar.setTimezone(0);
		// result = calendar.toGregorianCalendar().getTime();
		// }

		// DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// df.setTimeZone(HpEKConstants.utcTz);
		// calendar.setTimezone(4);
		// result = calendar.toGregorianCalendar().getTime()

		/*
		 * 
		 * GregorianCalendar gcalendar = calendar.toGregorianCalendar();
		 * TimeZone fromTimeZone = gcalendar.getTimeZone(); TimeZone toTimeZone
		 * = TimeZone.getTimeZone("UTC");
		 * 
		 * gcalendar.setTimeZone(fromTimeZone);
		 * gcalendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() *
		 * -1); if (fromTimeZone.inDaylightTime(gcalendar.getTime())) {
		 * gcalendar.add(Calendar.MILLISECOND,
		 * gcalendar.getTimeZone().getDSTSavings() * -1); }
		 * 
		 * gcalendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset()); if
		 * (toTimeZone.inDaylightTime(gcalendar.getTime())) {
		 * gcalendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings()); }
		 * 
		 * result = gcalendar.getTime(); // try { // String utcString =
		 * df.format(utcDate); // result = df.parse(utcString); // } catch
		 * (ParseException e) { // LOG.debug("XMLGregorianCalendar parse erro");
		 * // }
		 */

		// String stoa = df.format(cal.getTime());

		// }
		return result;

	}
	
	
	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString());
		dataList.add(HpEKConstants.EKRTC_DATA_SOURCE);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, HpEKConstants.EKRTC_DATA_SOURCE);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

	}
	
	
	private void sendErroNotification(EnumExceptionCodes eec){
		
		if (logLevel
				.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
						.name())) {
			if (!msgLogged) {
				irmtabRef = _irmfacade.storeRawMsg(rawMsg, HpEKConstants.EKRTC_DATA_SOURCE);
				msgLogged = Boolean.TRUE;
			}
		
	
		if (irmtabRef > 0) {
			try {
				sendErrInfo(eec, irmtabRef);
			} catch (JsonGenerationException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (JsonMappingException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (IOException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			}
		}
		
	}
		
	}
	

	public EntDbAircraftOpsMsg getEntDbAircraftOpsMsg() {
		return entDbAircraftOpsMsg;
	}

	public void setEntDbAircraftOpsMsg(EntDbAircraftOpsMsg entDbAircraftOpsMsg) {
		this.entDbAircraftOpsMsg = entDbAircraftOpsMsg;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getXmlMsg() {
		return xmlMsg;
	}

	public void setXmlMsg(String xmlMsg) {
		this.xmlMsg = xmlMsg;
	}

	public Message getRawMsg() {
		return rawMsg;
	}

	public void setRawMsg(Message rawMsg) {
		this.rawMsg = rawMsg;
	}
	
	
	

}
