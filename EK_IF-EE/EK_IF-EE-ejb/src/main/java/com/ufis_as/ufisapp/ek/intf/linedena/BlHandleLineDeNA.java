package com.ufis_as.ufisapp.ek.intf.linedena;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.persistence.Tuple;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.linedena.entities.EntDbAircraftOpsMsg;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlLineDeNABean;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;

/**
 * 
 * @author SCH
 * 
 */

@Stateless
public class BlHandleLineDeNA {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleLineDeNA.class);

	@EJB
	IAfttabBeanLocal _afttabBeanLocal;
	@EJB
	DlLineDeNABean _dlLineDeNABean;
	@EJB
	private IAfttabBeanLocal afttabBean;
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	private Message rawMsg;
	private String logLevel = null;
	private Boolean msgLogged = Boolean.FALSE;
	private long irmtabRef;

	public void handleLineDeNA(EntDbAircraftOpsMsg entDbAircraftOpsMsg,
			Message msg, String subtype) {

		logLevel = entStartupInitSingleton.getIrmLogLev();
		rawMsg = msg;

		// search flight
		if (entDbAircraftOpsMsg != null) {

			Tuple deNAEntDbAfttab = null;
			String getFlightNumber = null;
			String flDate = null;
			if (entDbAircraftOpsMsg.getFlightNumber() != null) {
				getFlightNumber = entDbAircraftOpsMsg.getFlightNumber()
						.substring(0, 2)
						+ " "
						+ entDbAircraftOpsMsg.getFlightNumber().substring(2);
			}
			if (entDbAircraftOpsMsg.getFltDate() != null) {
				flDate = HpEKConstants.DENA_DATE_FORMAT
						.format(entDbAircraftOpsMsg.getFltDate());
			}

			long startTime = System.currentTimeMillis();
			deNAEntDbAfttab = afttabBean.findFlightForLineDeNAX(flDate,
					entDbAircraftOpsMsg.getArrDepFlag(), getFlightNumber,
					entDbAircraftOpsMsg.getFltRegn());
			LOG.debug("search flight from afttab, takes {}",
					System.currentTimeMillis() - startTime);

			if (deNAEntDbAfttab != null) {
				entDbAircraftOpsMsg.setIdFlight(deNAEntDbAfttab.get("urno",
						BigDecimal.class));
				// entDbAircraftOpsMsg.setIdFlight(deNAEntDbAfttab.getUrno());
				try {
					String aldtStr = deNAEntDbAfttab.get("aldt", String.class)
							.trim();
					String atotStr = deNAEntDbAfttab.get("atot", String.class)
							.trim();
					if (!"".equals(aldtStr)) {
						entDbAircraftOpsMsg
								.setAldt(HpEKConstants.DENA_DATE_FORMAT
										.parse(aldtStr));
					} else {
						LOG.debug("! aldt is empty in afttab record");
					}
					// entDbAircraftOpsMsg.setAldt(HpEKConstants.DENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAldt()));
					if (!"".equals(atotStr)) {
						entDbAircraftOpsMsg
								.setAtot(HpEKConstants.DENA_DATE_FORMAT
										.parse(atotStr));
					} else {
						LOG.debug("! atot is empty in afttab record");
					}
					// entDbAircraftOpsMsg.setAtot(HpEKConstants.sDENA_DATE_FORMAT.parse(deNAEntDbAfttab.getAtot()));
				} catch (ParseException e) {
					LOG.debug("String to Date parse erro {}", e);
				}
			} else {
				LOG.debug(
						"Flight not found for LineDeNA, fltDate: {}, adid: {}, flightNumber: {}, fltRegn: {}",
						flDate, entDbAircraftOpsMsg.getArrDepFlag(),
						getFlightNumber, entDbAircraftOpsMsg.getFltRegn());
				entDbAircraftOpsMsg.setIdFlight(new BigDecimal(0));
				LOG.debug("flight not found , set IdFlight = 0");

				// send erro notification
				sendErroNotification(EnumExceptionCodes.ENOFL);
			}

		} else {
			LOG.debug("entDbAircraftOpsMsg is null, pls check");
			return;
		}

		// set config later
		// String APPEND_RECORD = "NO";
		if ("INS".equalsIgnoreCase(subtype)) {
			entDbAircraftOpsMsg.setUpdatedUser("");
			_dlLineDeNABean.UpdateSameRegnRecords(entDbAircraftOpsMsg);
			_dlLineDeNABean.Persist(entDbAircraftOpsMsg);

			// send change notification
			ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
					String.valueOf(entDbAircraftOpsMsg.getIdFlight()), null,
					entDbAircraftOpsMsg);
			// update those exist

			// // get close sequences number
			// if("Y".equalsIgnoreCase(entDbAircraftOpsMsg.getCloseMsgFlag()) &&
			// new BigDecimal(10).equals(entDbAircraftOpsMsg.getMsgSeqnNum())){
			//
			// for (int i = 1; i < 10; i++){
			// EntDbAircraftOpsMsg existRecord
			// =_dlLineDeNABean.getExsitRecord(entDbAircraftOpsMsg, new
			// BigDecimal(i));
			// if (existRecord != null){
			// existRecord.setCloseMsgFlag("Y");
			// _dlLineDeNABean.Update(existRecord);
			// }
			// }
			//
			// }

		} else if ("UPD".equals(subtype)) {
			// search from entDbAircraftOpsMsg table

			EntDbAircraftOpsMsg existRecord = _dlLineDeNABean.getExsitRecord(
					entDbAircraftOpsMsg, entDbAircraftOpsMsg.getMsgSeqnNum());
			if (existRecord != null) {

				EntDbAircraftOpsMsg OldEntDbAircraftOpsMsg = (EntDbAircraftOpsMsg) SerializationUtils
						.clone(existRecord);

				// // entDbAircraftOpsMsg.setId(existRecord.getId());
				// //
				// entDbAircraftOpsMsg.setCreatedDate(existRecord.getCreatedDate());
				// //
				// existRecord.setActionReqd(entDbAircraftOpsMsg.getActionReqd());
				// //
				// existRecord.setAddTimeMins(entDbAircraftOpsMsg.getAddTimeMins());
				// //
				// existRecord.setAddTimeReqd(entDbAircraftOpsMsg.getAddTimeReqd());
				// //
				// existRecord.setArrDepFlag(entDbAircraftOpsMsg.getArrDepFlag());
				// //
				// existRecord.setAssistReqd(entDbAircraftOpsMsg.getAssistReqd());
				// existRecord.setCloseMsgFlag(entDbAircraftOpsMsg.getCloseMsgFlag());
				// //
				// existRecord.setDefectDetails(entDbAircraftOpsMsg.getDefectDetails());
				// //
				// existRecord.setDeptName(entDbAircraftOpsMsg.getDeptName());
				// //
				// existRecord.setFlightNumber(entDbAircraftOpsMsg.getFlightNumber());
				// // existRecord.setFltDate(entDbAircraftOpsMsg.getFltDate());
				// // existRecord.setFltRegn(entDbAircraftOpsMsg.getFltRegn());
				// //
				// existRecord.setMsgPostDate(entDbAircraftOpsMsg.getMsgPostDate());
				// //
				// existRecord.setMsgSendDate(entDbAircraftOpsMsg.getMsgSendDate());
				// //
				// existRecord.setMsgSeqnNum(entDbAircraftOpsMsg.getMsgSeqnNum());
				// //
				// existRecord.setMsgSubject(entDbAircraftOpsMsg.getMsgSubject());
				// //
				// existRecord.setNextUpdDate(entDbAircraftOpsMsg.getNextUpdDate());
				// // existRecord.setOptLock(entDbAircraftOpsMsg.getOptLock());
				// //
				// existRecord.setPublishTo(entDbAircraftOpsMsg.getPublishTo());
				// existRecord.setIdFlight(entDbAircraftOpsMsg.getIdFlight());

				_dlLineDeNABean.UpdateSameRegnRecords(existRecord);
				_dlLineDeNABean.Update(existRecord);

				// send change notification
				ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
						String.valueOf(existRecord.getIdFlight()),
						OldEntDbAircraftOpsMsg, existRecord);

			} else if (existRecord == null) {
				LOG.debug("Record not found, Msg will not be updated");
				entDbAircraftOpsMsg.setUpdatedUser("");
				_dlLineDeNABean.UpdateSameRegnRecords(entDbAircraftOpsMsg);
				_dlLineDeNABean.Persist(entDbAircraftOpsMsg);

				// send change notification
				ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
						String.valueOf(entDbAircraftOpsMsg.getIdFlight()),
						null, entDbAircraftOpsMsg);

			}
		} else if ("DEL".equalsIgnoreCase(subtype)) {

			EntDbAircraftOpsMsg existRecord = _dlLineDeNABean.getExsitRecord(
					entDbAircraftOpsMsg, entDbAircraftOpsMsg.getMsgSeqnNum());

			EntDbAircraftOpsMsg OldEntDbAircraftOpsMsg = (EntDbAircraftOpsMsg) SerializationUtils
					.clone(existRecord);

			if (existRecord != null) {
				existRecord.setRecStatus("X");
				_dlLineDeNABean.Update(existRecord);
				_dlLineDeNABean.UpdateSameRegnRecords(existRecord);
			}

			// send change notification
			ufisTopicProducer.sendNotification(true, UfisASCommands.DRT,
					String.valueOf(existRecord.getIdFlight()),
					OldEntDbAircraftOpsMsg, existRecord);
		}

	}

	public void handleLineDeNAFromActiveMQ(EntUfisMsgDTO entUfisMsgDTOReceive,
			String topicName) {
		// parse JSON data to communication object
		// ObjectMapper objectMapper = new ObjectMapper();
		// EntUfisMsgDTO entUfisMsgDTOReceive = null;
		// try {
		// entUfisMsgDTOReceive = objectMapper.readValue(jsonString,
		// EntUfisMsgDTO.class);
		// } catch(Exception e){
		// LOG.error("erro to convert jason string to the object from topic {}, {}",topicName,
		// e);
		// return;
		// }

		if (entUfisMsgDTOReceive != null) {
			EntDbAircraftOpsMsg entDbAircraftOpsMsg = new EntDbAircraftOpsMsg();

			List<EntUfisMsgACT> entUfisMsgACTList = entUfisMsgDTOReceive
					.getBody().getActs();
			Iterator<EntUfisMsgACT> entUfisMsgACTIt = entUfisMsgACTList
					.iterator();
			while (entUfisMsgACTIt.hasNext()) {
				EntUfisMsgACT entUfisMsgACT = entUfisMsgACTIt.next();
				List<String> flds = entUfisMsgACT.getFld();
				List<Object> values = entUfisMsgACT.getData();
				BigDecimal idFlight = new BigDecimal(entUfisMsgACT.getId().get(
						0));

				// aldt, alot , fltId, regn need to be set
				Date aldt = null;
				Date atot = null;
				String flitRegn = null;

				for (int i = 0; i < flds.size(); i++) {
					switch (flds.get(i).toUpperCase()) {
					// case "ID_FLIGHT":
					// idFlight = new BigDecimal(values.get(i));
					// break;
					case "ALDT":
						try {
							aldt = HpEKConstants.DENA_DATE_FORMAT
									.parse((String) values.get(i));
						} catch (ParseException e) {
							LOG.error(
									"aldt Date convertion erro from String {} while handling msg from AMQ, erro: {}",
									values.get(i), e);
						}
						break;
					case "ATOT":
						try {
							atot = HpEKConstants.DENA_DATE_FORMAT
									.parse((String) values.get(i));
						} catch (ParseException e) {
							LOG.error(
									"atot Date convertion erro from String {} while handling msg from AMQ, erro: {}",
									values.get(i), e);
						}
						break;
					case "REGN":
						flitRegn = (String) values.get(i);
						break;
					}

				}

				entDbAircraftOpsMsg.setAldt(aldt);
				entDbAircraftOpsMsg.setAtot(atot);
				entDbAircraftOpsMsg.setIdFlight(idFlight);
				entDbAircraftOpsMsg.setFltRegn(flitRegn);

				long startTime = System.currentTimeMillis();
				_dlLineDeNABean.UpdateSameRegnRecords(entDbAircraftOpsMsg);
				LOG.info(
						"update same regn records by receiving msg from AMQ, takes {} ms",
						System.currentTimeMillis() - startTime);
				LOG.info("aldt {}, atot {}, idFlight {}, flitRegn {}", aldt,
						atot, idFlight, flitRegn);

			}

		}
	}

	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString());
		dataList.add(HpEKConstants.EKRTC_DATA_SOURCE);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true,
				HpEKConstants.EKRTC_DATA_SOURCE);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

	}

	private void sendErroNotification(EnumExceptionCodes eec) {

		if (logLevel.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
				.name())) {
			if (!msgLogged) {
				irmtabRef = _irmtabFacade.storeRawMsg(rawMsg,
						HpEKConstants.EKRTC_DATA_SOURCE);
				msgLogged = Boolean.TRUE;
			}

			if (irmtabRef > 0) {
				try {
					sendErrInfo(eec, irmtabRef);
				} catch (JsonGenerationException e) {
					LOG.error("Erro send ErroInfo, {}", e);
				} catch (JsonMappingException e) {
					LOG.error("Erro send ErroInfo, {}", e);
				} catch (IOException e) {
					LOG.error("Erro send ErroInfo, {}", e);
				}
			}

		}

	}

}
