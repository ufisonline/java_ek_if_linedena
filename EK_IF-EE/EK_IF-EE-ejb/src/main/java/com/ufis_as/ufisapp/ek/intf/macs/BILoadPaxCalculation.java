package com.ufis_as.ufisapp.ek.intf.macs;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.macs.BIPaxCalculationRemote;
import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadPaxBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxConBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
/**
 * 
 * @author SCH
 * 
 * 	This the new LOADPAX calculation class according to the design change at the end Jun 2013.
 * 	
 *
 */
@Stateless
public class BILoadPaxCalculation implements BIPaxCalculationRemote{
	private static final Logger LOG = LoggerFactory.getLogger(BILoadPaxCalculation.class);
	
//	private EntityManager em;
	private List<EntDbLoadPaxSummary> loaList;
	
	@EJB
    private DlLoadPaxBean dlLoadPaxBean;
	
	@EJB
	private DlFlightIdMappingBean dlFlightIdMappingBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	 @EJB
	 private BlHandleCedaMacsPax _blHandleCedaMacs;
	 
	@EJB
	DlPaxConBean dlPaxConBean;
	
	@EJB
	DlFlightIdMappingBean dlFlightMappingBean;
		

	@Override
	public boolean SummarizePaxForMainFlight(EntDbFlightIdMapping entDbFlightIdMapping) {
		loaList = new ArrayList<EntDbLoadPaxSummary>();
		
//		int intIdFlight = 0;
//		try {
//		intIdFlight = Integer.parseInt(idFlight);
//		} catch(Exception e){
//			LOG.error("erro to parse string idFlight {} to int type, pax summerize process will be terminated", idFlight);
//			return false;
//		}
		
//		long startTime = new Date().getTime();    		
//		EntDbFlightIdMapping entDbFlightIdMapping= dlFlightMappingBean.getMappingByFlightIdX(new BigDecimal(idFlight));
//		long endTime = new Date().getTime();
//		LOG.info("takes {} ms to search flt_id_mapping table, idFlight: {}", (endTime - startTime), idFlight);
		
//		if (entDbFlightIdMapping == null){
//			LOG.debug("can not find flight in flight_id_mapping table, idFlight: {}", idFlight);
//			return false; 
//		}
		
//	 	Retrieve raw data from database
		long startTime = new Date().getTime();   
		List<EntDbLoadPax> rawPaxList = dlLoadPaxBean.getPaxListByIdFlightX(entDbFlightIdMapping.getIdFlight().intValue());
		long endTime = new Date().getTime();
		LOG.info("takes {} ms to get records from load_pax  table, idFlight: {}", (endTime - startTime), entDbFlightIdMapping.getIdFlight().intValue());

		if (rawPaxList != null){		
		// set basic
		EntDbLoadPaxSummary loa = getLoadSummaryBasic(entDbFlightIdMapping, rawPaxList);
		
		// set booking
		loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"BOK",rawPaxList));
		// set check in
		loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"CIN",rawPaxList));
		// set boarding
		loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"PBO",rawPaxList));
		// set join
		loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"JOI",rawPaxList));
		
		}else{
			LOG.debug("Can not find pax by idFlight "+entDbFlightIdMapping.getIdFlight().intValue());
			return false;
		}
		
//		_blHandleCedaMacs.handleMACSPAX(loaList);
		
		startTime = new Date().getTime();   
		// summarize for connection flight
		ArrayList<Integer> connIdFlightBuffer = new ArrayList<Integer>();
		Iterator<EntDbLoadPax> rawPaxListIt = rawPaxList.iterator();
		while(rawPaxListIt.hasNext()){
			EntDbLoadPax loadPax = rawPaxListIt.next();
			if (loadPax.getEntDbPaxConns() != null && loadPax.getEntDbPaxConns().size() > 0){
				 List<EntDbLoadPaxConn>  loadPaxConnList = loadPax.getEntDbPaxConns();
				 Iterator<EntDbLoadPaxConn> loadPaxConnListIt =loadPaxConnList.iterator();
				 while (loadPaxConnListIt.hasNext()){
					 EntDbLoadPaxConn loadPaxConn = loadPaxConnListIt.next();
					 if (!connIdFlightBuffer.contains(new Integer(loadPaxConn.getIdFlight()))){
						 connIdFlightBuffer.add(new Integer(loadPaxConn.getIdFlight()));
					 } 
				 }
				 if (connIdFlightBuffer != null && connIdFlightBuffer.size() >0){
					 List<EntDbLoadPaxConn>  oneFlightloadPaxConnList = null;
					 for (int i =0; i < connIdFlightBuffer.size(); i++){
						 oneFlightloadPaxConnList = new ArrayList<EntDbLoadPaxConn>();
						 int connFlightId = connIdFlightBuffer.get(i).intValue();
						 for (int j = 0; j < loadPaxConnList.size(); j++ ){
							 if (loadPaxConnList.get(j).getIdFlight() == connFlightId){
								 oneFlightloadPaxConnList.add(loadPaxConnList.get(j));
							 }
						 }
						 	 
					 }		
					 
					 SummarizePaxForConnFlight(entDbFlightIdMapping,oneFlightloadPaxConnList);
					 
				 }
				 
				 
			}
		}
		
		endTime = new Date().getTime();
		LOG.info("takes {} ms to summarize the connection pax for, idFlight: {}", (endTime - startTime), entDbFlightIdMapping.getIdFlight().intValue());

		
		
		
		return true;
	}

	@Override
	public boolean SummarizePaxForConnFlight(String intIdFlight,
			String airlineCode, String flightNumber,
			String flightNumberSuffice, Date scheduledFlightDatetime) {	
		
			loaList = new ArrayList<EntDbLoadPaxSummary>();
			
//			int intIdFlight = 0;
//			try {
//			intIdFlight = Integer.parseInt(idFlight);
//			} catch(Exception e){
//				LOG.error("erro to parse string idFlight {} to int type, pax summerize process will be terminated", idFlight);
//				return false;
//			}
		
			// Get raw passenger information from database
			List<EntDbLoadPaxConn> rawPaxConnList = dlPaxConBean.getPaxConnListByIntflidAndFlightnumberX(intIdFlight, airlineCode, flightNumber, flightNumberSuffice, scheduledFlightDatetime);
			
			if (rawPaxConnList != null){
				// set basic
				EntDbLoadPaxSummary loa = getLoadSummaryBasic(null, rawPaxConnList);
				// set transfer
				loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"TXF",rawPaxConnList));
				
			}else{
				LOG.debug("Can not find paxConn by intIdFlight "+intIdFlight);
				return false;
			}
			
//			_blHandleCedaMacs.handleMACSPAX(loaList);

			return true;
	}
	
	
	
	private EntDbLoadPaxSummary setLoadSummaryResult(EntDbLoadPaxSummary loa, String infoType,  List<?> rawDataList){
		
		loa.setInfoType(infoType);
		loa.setDataSource("SYS");
		loa.setFirstPax(doCalculate(infoType, "T", "N", "F", rawDataList));
		loa.setBusinessPax(doCalculate(infoType, "T", "N", "B", rawDataList));
		loa.setEconPax(doCalculate(infoType, "T", "N", "E", rawDataList));
		loa.setEcon2Pax(doCalculate(infoType, "T", "N", "E2", rawDataList));
		loa.setTotalPax(doCalculate(infoType, "T", "N", "T", rawDataList));
		loa.setMalePax(doCalculate(infoType, "M", "N", "T", rawDataList));
		loa.setFirstMalePax(doCalculate(infoType, "M", "N", "F", rawDataList));
		loa.setBusinessMalePax(doCalculate(infoType, "M", "N", "B", rawDataList));
		loa.setEconMalePax(doCalculate(infoType, "M", "N", "E", rawDataList));
		loa.setEcon2MalePax(doCalculate(infoType, "M", "N", "E2", rawDataList));
		loa.setFemalePax(doCalculate(infoType, "F", "N", "T", rawDataList));
		loa.setFirstFemalePax(doCalculate(infoType, "F", "N", "F", rawDataList));
		loa.setBusinessFemalePax(doCalculate(infoType, "F", "N", "B", rawDataList));
		loa.setEconFemalePax(doCalculate(infoType, "F", "N", "E", rawDataList));
		loa.setEconFemalePax(doCalculate(infoType, "F", "N", "E2", rawDataList));
		loa.setAdultPax(doCalculate(infoType, "A", "N", "T", rawDataList));
		loa.setFirstAdult(doCalculate(infoType, "A", "N", "F", rawDataList));
		loa.setBusinessAdult(doCalculate(infoType, "A", "N", "B", rawDataList));
		loa.setEconAdult(doCalculate(infoType, "A", "N", "E", rawDataList));
		loa.setEcon2Adult(doCalculate(infoType, "M", "N", "E2", rawDataList));
		loa.setChildPax(doCalculate(infoType, "C", "N", "T", rawDataList));
		loa.setFirstChild(doCalculate(infoType, "C", "N", "F", rawDataList));
		loa.setBusinessChild(doCalculate(infoType, "C", "N", "B", rawDataList));
		loa.setEconChild(doCalculate(infoType, "C", "N", "E", rawDataList));
		loa.setEcon2Child(doCalculate(infoType, "C", "N", "E2", rawDataList));
		loa.setInfantPax(doCalculate(infoType, "I", "N", "T", rawDataList));
		loa.setFirstInfant(doCalculate(infoType, "I", "N", "F", rawDataList));
		loa.setBusinessInfant(doCalculate(infoType, "I", "N", "B", rawDataList));
		loa.setEconInfant(doCalculate(infoType, "I", "N", "E", rawDataList));
		loa.setEcon2Infant(doCalculate(infoType, "I", "N", "E2", rawDataList));
		
		return loa;
	}
	
	private EntDbLoadPaxSummary getLoadSummaryBasic(EntDbFlightIdMapping entDbFlightIdMapping, List<?> rawDataList){
		EntDbLoadPaxSummary loa = new EntDbLoadPaxSummary();
		


		
		
		// set main ufis flightId
//		String mainFLightId = dlFlightIdMappingBean.getUfisFlightId(intFlid);
//		mainFLightId = mainFLightId.trim();
		
		// get ADID of main flight

		Character adid =' ';
		if (entDbFlightIdMapping != null){
		adid = entDbFlightIdMapping.getArrDepFlag();
		loa.setInterfaceFltId(new BigDecimal(entDbFlightIdMapping.getIntFltId()));
		}
		
		if ("A".equalsIgnoreCase(String.valueOf(adid))){
			loa.setIdArrFlight(entDbFlightIdMapping.getIdFlight());
		}else if ("D".equalsIgnoreCase(String.valueOf(adid)) || "B".equalsIgnoreCase(String.valueOf(adid))){
			loa.setIdDepFlight(entDbFlightIdMapping.getIdFlight());
		}
		
		if (rawDataList != null && rawDataList.size() > 0 && rawDataList.get(0) instanceof EntDbLoadPaxConn ){
			EntDbLoadPaxConn entDbPaxConn = (EntDbLoadPaxConn)rawDataList.get(0);
			
			loa.setInterfaceFltId(new BigDecimal(entDbPaxConn.getInterfaceConxFltid()));
			// get string flight schedule date time
			String strFlightScheduleDateTime = " ";
			Date flightScheduleDateTime = entDbPaxConn.getConxFltDate();
			DateFormat df = new SimpleDateFormat("yyyymmddhhMMss");
			if (flightScheduleDateTime != null){
				strFlightScheduleDateTime = df.format(flightScheduleDateTime);
			}
			
			// set link flightID
			int linkFlightId = 0;
			linkFlightId = entDbPaxConn.getIdFlight();
			if ("A".equalsIgnoreCase(String.valueOf(adid))){
//				linkFlightId = dlAfttabBean.getUrnoFoDepFlight(entDbPaxConn.getAirlineCode(), entDbPaxConn.getFlightNumber().trim(), entDbPaxConn.getFlightNumberSuffice(), strFlightScheduleDateTime);			
				if (linkFlightId != 0){
					loa.setIdDepFlight(new BigDecimal(linkFlightId));
				}else{
//					loa.setIdDepFlight(" ");
				}
			}else if ("D".equalsIgnoreCase(String.valueOf(adid)) || "B".equalsIgnoreCase(String.valueOf(adid))){
//				linkFlightId = dlAfttabBean.getUrnoFoArrFlight(entDbPaxConn.getAirlineCode(), entDbPaxConn.getFlightNumber().trim(), entDbPaxConn.getFlightNumberSuffice(), strFlightScheduleDateTime);
				if (linkFlightId != 0){
					loa.setIdArrFlight(new BigDecimal(linkFlightId));
				}else{
//					loa.setIdArrFlight(" ");
				}
			}
			
			// set pax connection type
			loa.setConxType(entDbPaxConn.getConnType());
			
			
	    }
		else{
//			// set default value and avoid showing null in database 
//			lsPk.setAirlineCode(" ");
//			lsPk.setFlightNumber(" ");
//			lsPk.setFlightNumberSuffix(" ");
//			Date defaultDate;
//			try {
//				defaultDate = new SimpleDateFormat("yyyy-mm-dd hh:MM:ss").parse("0000-00-00 00:00:00");
//				lsPk.setScheduledFlightDateTime(defaultDate);
//			} catch (ParseException e1) {
//				// TODO Auto-generated catch block
////				e1.printStackTrace();
//			}
//			
//			// default link flight ID
//			if ("A".equals(adid)){
//				loa.setDepFlId(" ");
//			}else{
//				loa.setArrFlId(" ");
//			} 
//			
//			// set default connection type
//			loa.setConnType(" ");

		}
		
		
		
	
		return loa;
	}
	
	
	
	/**
	 * Calculation logics comes here.
	 * @param InfoType (value can be "TXX"="transit, "PBO"=onboard, "TXF"=transfer, "DIS"=disembark, "BOK"=book, "JOI"=join, "CIN"=checkin, "TRU"=thru-checkin, "N" = "null")
	 * @param PaxType (value can be "T"="total", "M"="male", "F"="femal", "A" = "adult", "C = "child", "I"="Infant", "N"= "null")
	 * @param CrewType (value can be "T" = total, "TC"="techCrew", "CC"="CabinCrew", "N"= "null"  )
	 * @param ufisClassType ((value can be "T" = total, "F"="firstClass", "B"="businessClass", "E"="ecnomicClass", "E2"="economicClass2" ))
	 * @param rawDataList
	 * @return calculation result 
	 */
	  
		private BigDecimal doCalculate(String InfoType, String PaxType, String CrewType, String ufisClassType, List<?> rawDataList){
			int result = 0;
			String s = InfoType.trim() + PaxType.trim() + CrewType.trim();
			Iterator<?> it = rawDataList.iterator();	
			ufisClassType = ufisClassType.toUpperCase().trim();
			
			switch(s){		
			// -----------------------------------------------------BOK ----------------------------------------------------------
			//	BOK-T-N  total pax of booking
			case "BOKTN":     // if travel class equals null, the counting will be miss up
				// do calculation
				while(it.hasNext()){
					EntDbLoadPax p =(EntDbLoadPax)it.next();
					
					if (p.getBookedClass() != null && !isCrew( p) && "T".equals(ufisClassType) ){
						result++;
					}else if (p.getBookedClass() != null 
							&&  !isCrew( p) && toUfisClass(p).equals(ufisClassType)){
						result++;
					}	
				}
				break;	
			
//				BOK-M-N toal male pax of booking
				case "BOKMN":    // if not child and no infant indicator, it will be count as adult
							// do calculation
				while(it.hasNext()){
				EntDbLoadPax p =(EntDbLoadPax)it.next();
								
				if(p.getBookedClass() != null 
					&& isMale(p)
					&& "T".equals(ufisClassType)
						&& !isCrew( p)){
					result++;
					}else if (p.getBookedClass() != null 
					&& isMale(p)
					&& toUfisClass(p).equals(ufisClassType)
					&& !isCrew( p)){
					result++;
					}
				}
				break;			
				
//				BOK-F-N toal female pax of booking
				case "BOKFN":    // if not child and no infant indicator, it will be count as adult
							// do calculation
				while(it.hasNext()){
				EntDbLoadPax p =(EntDbLoadPax)it.next();
								
				if(p.getBookedClass() != null 
					&& isFemale(p)
					&& "T".equals(ufisClassType)
						&& !isCrew( p)){
					result++;
					}else if (p.getBookedClass() != null 
					&& isFemale(p)
					&& toUfisClass(p).equals(ufisClassType)
					&& !isCrew( p)){
					result++;
					}
				}
				break;		
				
				//	BOK-A-N toal adult pax of booking
			case "BOKAN":    // if not child and no infant indicator, it will be count as adult
				// do calculation
				while(it.hasNext()){
					EntDbLoadPax p =(EntDbLoadPax)it.next();
					
					if(p.getBookedClass() != null 
							&& isAdult(p)
							&& "T".equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}else if (p.getBookedClass() != null 
							&& isAdult(p)
							&& toUfisClass(p).equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}
				}
				break;
				
				//	BOK-C-N toal child pax of booking
			case "BOKCN":
				// do calculation
				while(it.hasNext()){
					EntDbLoadPax p =(EntDbLoadPax)it.next();
					if(p.getBookedClass() != null  
							&& isChild(p)
							&& "T".equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}else if (p.getBookedClass() != null  
							&& isChild(p)
							&& toUfisClass(p).equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}
				}
				break;
			
				// BOK-I-N toal infant pax of booking
				case "BOKIN":
				// do calculation
				while(it.hasNext()){
					EntDbLoadPax p =(EntDbLoadPax)it.next();
					if(p.getBookedClass() != null  
							&& isInfant(p)
							&& "T".equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}else if (p.getBookedClass() != null  
							&& isInfant(p)
							&& toUfisClass(p).equals(ufisClassType)
							&& !isCrew( p)){
						result++;
					}
				}
				break;
				
				// ----------------------------------------------------------  CIN ---------------------------------------------------------------------
				//	CIN-T-N total pax of check-in
				case "CINTN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getCheckInDateTime() != null
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getCheckInDateTime() != null
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
//					CIN-A-N male pax of check-in
				case "CINMN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(	p.getCheckInDateTime() != null
								&& isMale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getCheckInDateTime() != null
								&& isMale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
//					CIN-A-N female pax of check-in
				case "CINFN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(	p.getCheckInDateTime() != null
								&& isFemale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getCheckInDateTime() != null
								&& isFemale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
					//	CIN-A-N adult pax of check-in
				case "CINAN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(	p.getCheckInDateTime() != null
								&& isAdult(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getCheckInDateTime() != null
								&& isAdult(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					//	CIN-C-N child pax of check-in
				case "CINCN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getCheckInDateTime() != null
								&& isChild(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getCheckInDateTime() != null
								&& isChild(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
				  //	CIN-I-N child pax of check-in
				case "CININ":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getCheckInDateTime() != null 
								&& isInfant(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getCheckInDateTime() != null 
								&& isInfant(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					
					// ----------------------------------------------------------PBO ----------------------------------------------------------------------
					// PBO-T-N total pax of boarding
				case "PBOTN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// PBO-A-N male pax of boarding
				case "PBOMN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isMale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isMale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// PBO-A-N adult pax of boarding
				case "PBOFN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isFemale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isFemale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
					// PBO-A-N adult pax of boarding
				case "PBOAN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isAdult(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y")
								&& isAdult(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// PBO-C-N child pax of boarding
				case "PBOCN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&&p.getBoardingStatus().trim().equals("Y") 
								&& isChild(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if (p.getBoardingStatus() != null
								&&p.getBoardingStatus().trim().equals("Y") 
								&& isChild(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
					// PBO-I-N infant pax of boarding
				case "PBOIN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y") 
								&& isInfant(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if(p.getBoardingStatus() != null
								&& p.getBoardingStatus().trim().equals("Y") 
								&& isInfant(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					
//					// ----------------------------------------------------------BAG -----------------------------------------------------------------------
//					//	BAG-T
//				case "bagt":
//					// do calculation
//					while(it.hasNext()){
//						EntDbLoadPax p =(EntDbLoadPax)it.next();
//						if(p.getBagNoOfPieces() != null
//								&& toUfisClass(p).equals(ufisClassType)
//								&& !isCrew( p)){
//							result += p.getBagNoOfPieces().intValue();
//						}
//					}
//					break;
				
//					// ------------------------------------------------------------BWT ---------------------------------------------------------------------
//					// BWT-T
//				case "bwtt":
//					// do calculation
//					while(it.hasNext()){
//						EntDbLoadPax p =(EntDbLoadPax)it.next();
//						if(p.getBagWeight() != null
//								&& toUfisClass(p).equals(ufisClassType)
//								&& !isCrew( p)){
//							result += p.getBagWeight().intValue();
//						}
//					}
//					break;
					
					// ----------------------------------------------------------JOI ----------------------------------------------------------------------
					// JOI -T-N total pax of join
				case "JOITN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// JOI-A-N  adult pax of join
				case "JOIMN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isMale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isMale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// JOI-A-N  adult pax of join
				case "JOIFN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isFemale(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isFemale(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
					// JOI-A-N  adult pax of join
				case "JOIAN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isAdult(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isAdult(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					// JOI-C-N  child pax of join
				case "JOICN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isChild(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if ((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isChild(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
				
					// JOI-I-N  child pax of join
				case "JOIIN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isInfant(p)
								&& "T".equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}else if ((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
								&& isInfant(p)
								&& toUfisClass(p).equals(ufisClassType)
								&& !isCrew( p)){
							result++;
						}
					}
					break;
					
					
				// -------------------------------------------------------------------TXF ---------------------------------------------------------------------
				// TXFTN total pax of transfer
				case "TXFTN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
//						if(toUfisClass(p.getPax()).equals(ufisClassType)
						if("T".equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}
					}
					break;
					
					//TXF-M-N male pax of transfer
					case "TXFMN":
						// do calculation
						while(it.hasNext()){
							EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
							if(isMale(p.getPax())
									&& "T".equals(ufisClassType)
									&& !isCrew(p.getPax())){
								result++;
							}else if (isMale(p.getPax())
									&& toUfisClass(p.getPax()).equals(ufisClassType)
									&& !isCrew( p.getPax())){
								result++;
							}
						}
						break;
						
						//TXF-F-N male pax of transfer
					case "TXFFN":
						// do calculation
						while(it.hasNext()){
							EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
							if(isFemale(p.getPax())
									&& "T".equals(ufisClassType)
									&& !isCrew( p.getPax())){
								result++;
							}else if (isFemale(p.getPax())
									&& toUfisClass(p.getPax()).equals(ufisClassType)
									&& !isCrew( p.getPax())){
								result++;
							}
						}
						break;
				
				//TXFAN adult pax of transfer
				case "TXFAN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
						if(isAdult(p.getPax())
								&& "T".equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}else if (isAdult(p.getPax())
								&& toUfisClass(p.getPax()).equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}
					}
					break;
					
					//TXFCN child pax of transfer
				case "TXFCN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
						if( isChild(p.getPax())
								&& toUfisClass(p.getPax()).equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}
					}
					break;
				
					//TXFIN infant pax of transfer
				case "TXFIN":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
						if(isInfant(p.getPax())
								&& "T".equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}else if (isInfant(p.getPax())
								&& toUfisClass(p.getPax()).equals(ufisClassType)
								&& !isCrew( p.getPax())){
							result++;
						}
					}
					break;
					
					// -------------------------------------------------------------------CBN ---------------------------------------------------------------------
				// 
				case "CBN-N-T":
					// do calculation
					while(it.hasNext()){
						EntDbLoadPax p =(EntDbLoadPax)it.next();
						if(isCrew( p) && "T".equals(ufisClassType)){
							result++;
						}else if(isCrew( p) && toUfisClass(p).equals(ufisClassType)){
							result++;
						}
					}
					break;
					
				default:
					// return -1 when incorrect command or calculation logic missing
					result = -1;
					break;
				
			}
			
			return new BigDecimal(result);
		}
		
		
		/*
		 *  To Convert the interface class to UFIS class
		 *  current logic is to check travel class first, if passenger don't have travel class information
		 *  the program will continue to check passenger's booked class 
		 */
			private String toUfisClass(EntDbLoadPax p){
				String UfisClass ="";
				String interfaceTravelClass = p.getTravelledClass();
				String interfaceBookedClass = p.getBookedClass();
				
				if(interfaceTravelClass != null && !"".equals(interfaceTravelClass)){
					
					switch(interfaceTravelClass.toUpperCase().trim()){
					case "F":
						UfisClass = "F";
						break;
					case "J":
						UfisClass = "B";
						break;
					case "Y":
						UfisClass = "E";
						break;
					default:
						UfisClass = "E2";
						break;
					}
				
				}else{
					if (interfaceBookedClass != null){
						
						switch(interfaceBookedClass.trim()){
						case "F":
							UfisClass = "F";
							break;
						case "J":
							UfisClass = "B";
							break;
						case "Y":
							UfisClass = "E";
							break;
						default:
							UfisClass = "E2";
							break;
						}
						
					}else{
						UfisClass = "E2";
					}
				}
				
				return UfisClass;
			}
			
			
			/*
			 * logic to check whether passenger is adult 
			 */
			private boolean isAdult(EntDbLoadPax p){
				boolean result = false;
				
				if((p.getPaxType() == null || !"C".equalsIgnoreCase(p.getPaxType()))
						&& (p.getInfantIndicator() == null || ! "Y".equalsIgnoreCase(p.getInfantIndicator()))){
					result = true;
				}
			
				return result;
				
			}
			
			/*
			 * logic to check whether passenger is child
			 */
			private boolean isChild(EntDbLoadPax p){
				boolean result = false;
				
				if(p.getPaxType() != null
						&& "C".equalsIgnoreCase(p.getPaxType().trim())){
					result = true;
				}
			
				return result;
				
			}
			
			/*
			 * logic to check whether passenger is infant
			 */
			private boolean isInfant(EntDbLoadPax p){
				boolean result = false;
				
				if(p.getInfantIndicator() != null
						&& "Y".equalsIgnoreCase(p.getInfantIndicator().trim())){
					result = true;
				}
			
				return result;
				
			}
			
			/*
			 *  logic to check whether passenger is crew
			 */
			private boolean isCrew(EntDbLoadPax p){
				boolean result = false;
				
				if (p != null){
				String statusOnBoard = p.getStatusOnboard();
				if ("FM".equalsIgnoreCase(statusOnBoard) || "DDT".equalsIgnoreCase(statusOnBoard) || "CR4".equalsIgnoreCase(statusOnBoard)){
					result = true;
				}
				}
					
				return result;
			}
			
			/*
			 * logic to check whether passenger is male 
			 */
			private boolean isMale(EntDbLoadPax p){
				boolean result = false;
				
				if("M".equalsIgnoreCase(p.getPaxType())){
					result = true;
				}
			
				return result;
			}
			
			/*
			 * logic to check whether passenger is female 
			 */
			private boolean isFemale(EntDbLoadPax p){
				boolean result = false;
				
				if("F".equalsIgnoreCase(p.getPaxType())){
					result = true;
				}
			
				return result;
			}

			@Override
			public boolean SummarizePaxForConnFlight(EntDbFlightIdMapping entDbFlightIdMapping, List<EntDbLoadPaxConn> rawPaxConnList) {
//				
				loaList = new ArrayList<EntDbLoadPaxSummary>();
			
				// Get raw passenger information from database
//				List<EntDbLoadPaxConn> rawPaxConnList = dlPaxConBean.getPaxConnListByIntflidAndFlightnumberX(intIdFlight, airlineCode, flightNumber, flightNumberSuffice, scheduledFlightDatetime);
				
				if (rawPaxConnList != null){
					// set basic
					EntDbLoadPaxSummary loa = getLoadSummaryBasic(entDbFlightIdMapping, rawPaxConnList);
					// set transfer
					loaList.add(setLoadSummaryResult((EntDbLoadPaxSummary)SerializationUtils.clone(loa),"TXF",rawPaxConnList));
					
				}else{
					LOG.debug("rawPaxConnList is null");
					return false;
				}
				
//				_blHandleCedaMacs.handleMACSPAX(loaList);
//
				return false;
			}



}
