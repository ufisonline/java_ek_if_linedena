//package com.ufis_as.ufisapp.ek.intf.macs;
//
//import java.math.BigDecimal;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.ejb.Stateless;
//import com.ufis_as.ek_if.macs.BIPaxCalculationRemote;
//import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadSummary;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
//import com.ufis_as.ek_if.macs.entities.LoadSummaryPK;
//import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
//import com.ufis_as.ufisapp.ek.eao.DlLoadSummaryBean;
//import com.ufis_as.ufisapp.ek.eao.DlPaxBean;
//import com.ufis_as.ufisapp.ek.eao.DlPaxConBean;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
//
///**
// * 
// * @author SCH
// *
// */
//
//
//@Stateless
//public  class BIPaxCalculation implements BIPaxCalculationRemote {
////	private EntityManager em;
//	private List<EntDbLoadSummary> loaList;
//	
//	@EJB
//	DlLoadSummaryBean dlLoadSuBean;
//	
//	@EJB
//    DlPaxBean dlPaxBean;
//	
//	@EJB
//	DlPaxConBean dlPaxConBean;
//	
//	@EJB
//	DlFlightIdMappingBean dlFlightIdMappingBean;
//	
//	@EJB
//	IAfttabBeanLocal dlAfttabBean;
//	
//    @EJB
//    private BlHandleCedaMacs _blHandleCedaMacs;
//
//	/**
//	 * 	This method is to summarize the passenger data and.
//	 * 	the method take one argument, intFliId which is referring to 
//	 * 	Interface's Flight ID.  
//	 */
//	@Override
//	public boolean SummarizePaxForMainFlight(String intFlId) {
//		loaList = new ArrayList<EntDbLoadSummary>();
//		
//		// 	Retrieve raw data from database
//		List<EntDbLoadPax> rawPaxList = dlPaxBean.getPaxListByFlightId(intFlId);
//		
//		// PXB
//		loaList.add(setLoadSummary("pxb", "t", "", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxb", "c", "a", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxb", "c", "c", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxb", "c", "i", rawPaxList, intFlId));
//		//	PXC
//		loaList.add(setLoadSummary("pxc", "t", "", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxc", "c", "a", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxc", "c", "c", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxc", "c", "i", rawPaxList, intFlId));
//		//	PBO
//		loaList.add(setLoadSummary("pbo", "t", "", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pbo", "c", "a", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pbo", "c", "c", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pbo", "c", "i", rawPaxList, intFlId));
//		//	BAG & BWT
//		loaList.add(setLoadSummary("bag", "t", "", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("bwt", "t", "", rawPaxList, intFlId));
//		// 	PXJ
//		loaList.add(setLoadSummary("pxj", "t", "", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxj", "c", "a", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxj", "c", "c", rawPaxList, intFlId));
//		loaList.add(setLoadSummary("pxj", "c", "i", rawPaxList, intFlId));
//		// CBN
//		loaList.add(setLoadSummary("cbn", "t", "", rawPaxList, intFlId));
//
//		// persist the calculation results into database
////		dlLoadSuBean.persistLoadSummaryList(loaList);
//		_blHandleCedaMacs.handleMACSPAX(loaList);
//		
//		return true;
////		return "SummarizePaxForMainFlight Done !";
//	}
//
//	
//	/**
//	 * 	This method is to summarize the passenger data and.
//	 * 	the method take five argument (intFlid, airlineCode, flightNumber, 
//	 * flightNumberSuffice, scheduledFlightDatetime), intFliId which is referring to 
//	 * 	Interface's Flight ID  
//	 */
//	@Override
//	public boolean SummarizePaxForConnFlight(String intFlId,
//		String airlineCode, String flightNumber,
//		String flightNumberSuffice, Date scheduledFlightDatetime) {
////		DlLoadSummaryBean dlLoadSummaryBean = new DlLoadSummaryBean();
////		DlPaxConBean dlPaxConBean = new DlPaxConBean();
//		loaList = new ArrayList<EntDbLoadSummary>();
//		
//		// Get raw passenger information from database
//		List<EntDbLoadPaxConn> pList = dlPaxConBean.getPaxConnListByIntflidAndFlightnumber(intFlId, airlineCode, flightNumber, flightNumberSuffice, scheduledFlightDatetime);
//		
//		loaList.add(setLoadSummary("pxt", "t", "", pList, intFlId));
//		loaList.add(setLoadSummary("pxt", "c", "a", pList, intFlId));
//		loaList.add(setLoadSummary("pxt", "c", "c", pList, intFlId));
//		loaList.add(setLoadSummary("pxt", "c", "i", pList, intFlId));
//
//		/*
//		 * Persist the calculation results into database 
//		 */
////		dlLoadSuBean.persistLoadSummaryList(loaList);
//		_blHandleCedaMacs.handleMACSPAX(loaList);
//			
//		return true;
////		return "SummarizePaxWithTwoFlightInfo Two Flight  Job Done !";
//	}
//	
//	
//	private String getInfoId(String cat1, String cat2, String cat3){
//		//	if categories input is not correct, method will return null
//		String infoId = null;
//		String s = cat1.toLowerCase().trim() + cat2.toLowerCase().trim() + cat3.toLowerCase().trim();
//		
//		switch(s){
//		//	PXB
//		case "pxbt":
//			infoId = "PXB_T";
//			break;
//		case "pxbca":
//			infoId = "PXB_C_A";
//			break;
//		case "pxbcc":
//			infoId = "PXB_C_C";
//			break;
//		case "pxbci":
//			infoId = "PXB_C_I";
//			break;
//		//	PXC
//		case "pxct":
//			infoId = "PXC_T";
//			break;
//		case "pxcca":
//			infoId = "PXC_C_A";
//			break;
//		case "pxccc":
//			infoId = "PXC_C_C";
//			break;
//		case "pxcci":
//			infoId = "PXC_C_I";
//			break;
//		//	PBO	
//		case "pbot":
//			infoId = "PBO_T";
//			break;
//		case "pboca":
//			infoId = "PBO_C_A";
//			break;
//		case "pbocc":
//			infoId = "PBO_C_C";
//			break;
//		case "pboci":
//			infoId = "PBO_C_I";
//			break;
//		// BAG
//		case "bagt":
//			infoId = "BAG_T";
//			break;
//		//	PWT
//		case "bwtt":
//			infoId = "BWT_T";
//			break;
//		//	PXT
//		case "pxtt":
//			infoId = "PXT_T";
//			break;
//		case "pxtca":
//			infoId = "PXT_C_A";
//			break;
//		case "pxtcc":
//			infoId = "PXT_C_C";
//			break;
//		case "pxtci":
//			infoId = "PXT_C_I";
//			break;
//	    // PXJ 
//		case "pxjt":
//			infoId = "PXJ_T";
//			break;
//		case "pxjca":
//			infoId = "PXJ_C_A";
//			break;
//		case "pxjcc":
//			infoId = "PXJ_C_C";
//			break;
//		case "pxjci":
//			infoId = "PXJ_C_I";
//			break;
//		// CBN
//		case "cbnt":
//			infoId = "CBN_T";
//			break;
//			
//		}
//		
//		return infoId;
//	}
//	
//	// Set and return PaxConnSummary Object after manipulate the data
//	private EntDbLoadSummary setLoadSummary(String cat1, String cat2, String cat3, List<?> pList, String intFlid){
//		EntDbLoadSummary loa = new EntDbLoadSummary();
//		String InfoId = getInfoId(cat1,cat2,cat3);
//		int b = doCalculate(cat1,cat2,cat3,"b", pList );
//		int f = doCalculate(cat1,cat2,cat3,"f", pList );
//		int e = doCalculate(cat1,cat2,cat3,"e", pList );
//		int e2 = doCalculate(cat1,cat2,cat3,"e2", pList );
//
//		int t = b + f + e + e2;
//		
//		LoadSummaryPK lsPk = new LoadSummaryPK();
//		lsPk.setIntFlId(new BigDecimal(intFlid));
//		lsPk.setInfoId(InfoId);
//
//		loa.setIntSystem("MACS");
//		loa.setBusinessClass(b);  
//		loa.setFirstClass(f);  
//		loa.setEconomyClass(e); 
//		loa.setEconomyClass2(e2);
//		loa.setTotalValue(t);
//		
//		
//		
//		
//		// set main ufis flightId
//		String mainFLightId = dlFlightIdMappingBean.getUfisFlightId(intFlid);
//		
//		// get ADID of main flight
//		String adid = dlAfttabBean.getADID(mainFLightId);
//		
//		if ("A".equals(adid)){
//			loa.setArrFlId(mainFLightId);
//		}else{
//			loa.setDepFlId(mainFLightId);
//		}
//		
//		if (pList != null && pList.size() > 0 && pList.get(0) instanceof EntDbLoadPaxConn ){
//			EntDbLoadPaxConn entDbPaxConn = (EntDbLoadPaxConn)pList.get(0);
//			lsPk.setAirlineCode(entDbPaxConn.getAirlineCode());
//			if (entDbPaxConn.getFlightNumber() != null){
//				lsPk.setFlightNumber(entDbPaxConn.getFlightNumber().trim());
//			}
//			lsPk.setFlightNumberSuffix(entDbPaxConn.getFlightNumberSuffice());
//			lsPk.setScheduledFlightDateTime(entDbPaxConn.getScheduledFlightDateTime());	
//			
//			
//			// get string flight schedule date time
//			String strFlightScheduleDateTime = " ";
//			Date flightScheduleDateTime = entDbPaxConn.getScheduledFlightDateTime();
//			DateFormat df = new SimpleDateFormat("yyyymmddhhMMss");
//			if (flightScheduleDateTime != null){
//				strFlightScheduleDateTime = df.format(flightScheduleDateTime);
//			}
//			
//			// set link flightID
//			String linkFlightId = null;
//			if ("A".equals(adid)){
//				linkFlightId = dlAfttabBean.getUrnoFoDepFlight(entDbPaxConn.getAirlineCode(), entDbPaxConn.getFlightNumber().trim(), entDbPaxConn.getFlightNumberSuffice(), strFlightScheduleDateTime);
//				if (linkFlightId != null){
//					loa.setDepFlId(linkFlightId);
//				}else{
//					loa.setDepFlId(" ");
//				}
//			}else{
//				linkFlightId = dlAfttabBean.getUrnoFoArrFlight(entDbPaxConn.getAirlineCode(), entDbPaxConn.getFlightNumber().trim(), entDbPaxConn.getFlightNumberSuffice(), strFlightScheduleDateTime);
//				if (linkFlightId != null){
//					loa.setArrFlId(linkFlightId);
//				}else{
//					loa.setArrFlId(" ");
//				}
//			}
//			
//			// set pax connection type
//			loa.setConnType(entDbPaxConn.getConnType());
//			
//			
//	    }
//		else{
//			// set default value and avoid showing null in database 
//			lsPk.setAirlineCode(" ");
//			lsPk.setFlightNumber(" ");
//			lsPk.setFlightNumberSuffix(" ");
//			Date defaultDate;
//			try {
//				defaultDate = new SimpleDateFormat("yyyy-mm-dd hh:MM:ss").parse("0000-00-00 00:00:00");
//				lsPk.setScheduledFlightDateTime(defaultDate);
//			} catch (ParseException e1) {
//				// TODO Auto-generated catch block
////				e1.printStackTrace();
//			}
//			
//			// default link flight ID
//			if ("A".equals(adid)){
//				loa.setDepFlId(" ");
//			}else{
//				loa.setArrFlId(" ");
//			} 
//			
//			// set default connection type
//			loa.setConnType(" ");
//
//		}
//		
//		loa.setlPk(lsPk);
//		
//	
//		
//		
//		return loa;
//	}
//	
//	
//	// Calculation logics comes here. cat1, cat2, cat3 which are matching the columns of loadSummary table
//	private int doCalculate(String cat1, String cat2, String cat3, String ufisClass, List<?> rawDataList){
//		int result = 0;
//		String s = (cat1.trim() + cat2.trim() + cat3.trim()).toLowerCase();
//		Iterator<?> it = rawDataList.iterator();	
//		
//		switch(s){
//		
//		// -----------------------------------------------------PXB ----------------------------------------------------------
//		//	PXB-T
//		case "pxbt":     // if travel class equals null, the counting will be miss up
//			// do calculation
//			while(it.hasNext()){
//				EntDbLoadPax p =(EntDbLoadPax)it.next();
//				
//				if(p.getBookedClass() != null 
//					&& toUfisClass(p).equals(ufisClass.trim())
//					&& !isCrew( p)){
//					result++;
//				}
//			}
//			break;	
//		
//			//	PXB-C-A
//		case "pxbca":    // if not child and no infant indicator, it will be count as adult
//			// do calculation
//			while(it.hasNext()){
//				EntDbLoadPax p =(EntDbLoadPax)it.next();
//				
//				if(p.getBookedClass() != null 
//						&& isAdult(p)
//						&& toUfisClass(p).equals(ufisClass.trim())
//						&& !isCrew( p)){
//					result++;
//				}
//			}
//			break;
//			
//			//	PAX-C-C
//		case "pxbcc":
//			// do calculation
//			while(it.hasNext()){
//				EntDbLoadPax p =(EntDbLoadPax)it.next();
//				if(p.getBookedClass() != null  
//						&& isChild(p)
//						&& toUfisClass(p).equals(ufisClass.trim())
//						&& !isCrew( p)){
//					result++;
//				}
//			}
//			break;
//		
//			//	PXB-C-I
//		case "pxbci":
//			// do calculation
//			while(it.hasNext()){
//				EntDbLoadPax p =(EntDbLoadPax)it.next();
//				if(p.getBookedClass() != null  
//						&& isInfant(p)
//						&& toUfisClass(p).equals(ufisClass.trim())
//						&& !isCrew( p)){
//					result++;
//				}
//			}
//			break;
//			
//			// ----------------------------------------------------------  PXC ---------------------------------------------------------------------
//			//	PXC-T
//			case "pxct":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getCheckInDateTime() != null
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				//	PXC-C-A
//			case "pxcca":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(	p.getCheckInDateTime() != null
//							&& isAdult(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				// PXC-C-C
//			case "pxccc":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getCheckInDateTime() != null
//							&& isChild(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				// PXC-C-I
//			case "pxcci":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getCheckInDateTime() != null 
//							&& isInfant(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				
//				// ----------------------------------------------------------PBO ----------------------------------------------------------------------
//				// PBO -T
//			case "pbot":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBoardingStatus() != null
//							&& p.getBoardingStatus().trim().equals("Y")
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				// PBO-C-A
//			case "pboca":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBoardingStatus() != null
//							&& p.getBoardingStatus().trim().equals("Y")
//							&& isAdult(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				// PBO-C-C
//			case "pbocc":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBoardingStatus() != null
//							&&p.getBoardingStatus().trim().equals("Y") 
//							&& isChild(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				// PBO-C-I
//			case "pboci":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBoardingStatus() != null
//							&& p.getBoardingStatus().trim().equals("Y") 
//							&& isInfant(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				
//				// ----------------------------------------------------------BAG -----------------------------------------------------------------------
//				//	BAG-T
//			case "bagt":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBagNoOfPieces() != null
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result += p.getBagNoOfPieces().intValue();
//					}
//				}
//				break;
//			
//				// ------------------------------------------------------------BWT ---------------------------------------------------------------------
//				// BWT-T
//			case "bwtt":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(p.getBagWeight() != null
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result += p.getBagWeight().intValue();
//					}
//				}
//				break;
//				
//				// ----------------------------------------------------------PXJ ----------------------------------------------------------------------
//				// PBO -T
//			case "pxjt":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				// PBO-C-A
//			case "pxjca":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
//							&& isAdult(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				// PBO-C-C
//			case "pxjcc":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
//							&& isChild(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//			
//				// PBO-C-I
//			case "pxjci":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if((p.getEntDbPaxConns() == null || p.getEntDbPaxConns().size() == 0)
//							&& isInfant(p)
//							&& toUfisClass(p).equals(ufisClass.trim())
//							&& !isCrew( p)){
//						result++;
//					}
//				}
//				break;
//				
//				
//			// -------------------------------------------------------------------PXT ---------------------------------------------------------------------
//			case "pxtt":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
//					if(toUfisClass(p.getPax()).equals(ufisClass.trim())
//							&& !isCrew( p.getPax())){
//						result++;
//					}
//				}
//				break;
//			
//			case "pxtca":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
//					if(isAdult(p.getPax())
//							&& toUfisClass(p.getPax()).equals(ufisClass.trim())
//							&& !isCrew( p.getPax())){
//						result++;
//					}
//				}
//				break;
//				
//			case "pxtcc":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
//					if( isChild(p.getPax())
//							&& toUfisClass(p.getPax()).equals(ufisClass.trim())
//							&& !isCrew( p.getPax())){
//						result++;
//					}
//				}
//				break;
//			
//			case "pxtci":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPaxConn p =(EntDbLoadPaxConn)it.next();
//					if(isInfant(p.getPax())
//							&& toUfisClass(p.getPax()).equals(ufisClass.trim())
//							&& !isCrew( p.getPax())){
//						result++;
//					}
//				}
//				break;
//				
//				// -------------------------------------------------------------------CBN ---------------------------------------------------------------------
//			case "cbnt":
//				// do calculation
//				while(it.hasNext()){
//					EntDbLoadPax p =(EntDbLoadPax)it.next();
//					if(isCrew( p) && toUfisClass(p).equals(ufisClass.trim())){
//						result++;
//					}
//				}
//				break;
//				
//			default:
//				// return -1 when incorrect command or calculation logic missing
//				result = -1;
//				break;
//			
//		}
//		
//		return result;
//	}
//	
//	
//	/*
//	 *  To Convert the interface class to UFIS class
//	 *  current logic is to check travel class first, if passenger don't have travel class information
//	 *  the program will continue to check passenger's booked class 
//	 */
//		private String toUfisClass(EntDbLoadPax p){
//			String UfisClass ="";
//			String interfaceTravelClass = p.getTravelledClass();
//			String interfaceBookedClass = p.getBookedClass();
//			
//			if(interfaceTravelClass != null && !"".equals(interfaceTravelClass)){
//				
//				switch(interfaceTravelClass.toUpperCase().trim()){
//				case "F":
//					UfisClass = "f";
//					break;
//				case "J":
//					UfisClass = "b";
//					break;
//				case "Y":
//					UfisClass = "e";
//					break;
//				default:
//					UfisClass = "e2";
//					break;
//				}
//			
//			}else{
//				if (interfaceBookedClass != null){
//					
//					switch(interfaceBookedClass.trim()){
//					case "F":
//						UfisClass = "f";
//						break;
//					case "J":
//						UfisClass = "b";
//						break;
//					case "Y":
//						UfisClass = "e";
//						break;
//					default:
//						UfisClass = "e2";
//						break;
//					}
//					
//				}else{
//					UfisClass = "e2";
//				}
//			}
//			
//			return UfisClass;
//		}
//		
//		
//		/*
//		 * logic to check whether passenger is adult 
//		 */
//		private boolean isAdult(EntDbLoadPax p){
//			boolean result = false;
//			
//			if((p.getPaxType() == null || !p.getPaxType().equals("C"))
//					&& (p.getInfantIndicator() == null || !p.getInfantIndicator().equals("Y"))){
//				result = true;
//			}
//		
//			return result;
//			
//		}
//		
//		/*
//		 * logic to check whether passenger is child
//		 */
//		private boolean isChild(EntDbLoadPax p){
//			boolean result = false;
//			
//			if(p.getPaxType() != null
//					&& p.getPaxType().trim().equals("C")){
//				result = true;
//			}
//		
//			return result;
//			
//		}
//		
//		/*
//		 * logic to check whether passenger is infant
//		 */
//		private boolean isInfant(EntDbLoadPax p){
//			boolean result = false;
//			
//			if(p.getInfantIndicator() != null
//					&& p.getInfantIndicator().trim().equals("Y")){
//				result = true;
//			}
//		
//			return result;
//			
//		}
//		
//		/*
//		 *  logic to check whether passenger is crew
//		 */
//		private boolean isCrew(EntDbLoadPax p){
//			boolean result = false;
//			
//			if (p != null){
//			String statusOnBoard = p.getStatusOnboard();
//			if ("FM".equals(statusOnBoard) || "DDT".equals(statusOnBoard) || "CR4".equals(statusOnBoard)){
//				result = true;
//			}
//			}
//				
//			return result;
//		}
//
//}
