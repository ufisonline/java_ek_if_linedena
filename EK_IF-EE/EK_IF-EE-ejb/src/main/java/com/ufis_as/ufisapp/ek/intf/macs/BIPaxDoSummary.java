package com.ufis_as.ufisapp.ek.intf.macs;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.macs.BIPaxCalculationRemote;
import com.ufis_as.ek_if.macs.IPaxDoSummaryRemote;
import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxConBean;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;

public @Stateless class BIPaxDoSummary implements IPaxDoSummaryRemote {

	 private static final Logger LOG = LoggerFactory.getLogger(BIPaxDoSummary.class);
	
	@EJB
	BIPaxCalculationRemote paxCal;

	// testing use
	@EJB
	DlPaxConBean dlTestConBean;
	
	@EJB
	IAfttabBeanLocal dlAfttabBean;

	@EJB
	DlFlightIdMappingBean dlFlightMappingBean;

	@Override
	public void DoSummary() {
		long startTime = 0;
		long endTime = 0;
	
		// get main flight ID list
//		List<?> urnoList = dlAfttabBean.getUrnoList();
//		if (urnoList != null) {
//			Iterator urnoIt = urnoList.iterator();
//			while (urnoIt.hasNext()) {
//				BigDecimal urno = (BigDecimal) urnoIt.next();
//				String urnoString = urno.toString();
//			String idFlight = "303866";//7525
				
				// change ufis flight ID to interface flight ID
//				String intFliId = dlFlightMappingBean.getIntFlightIdByFlightIdX(new BigDecimal(urnoString));
//				List <EntDbLoadPaxConn> entDbPaxConnList = null;
		startTime = new Date().getTime();  
		List<EntDbFlightIdMapping> entDbFlightIdMappingList = dlFlightMappingBean.getMappingByFlightId();
		endTime = new Date().getTime();
		LOG.info("takes {} ms to searching flight records from id_flt_mapping table", (endTime - startTime));
			
		Iterator<EntDbFlightIdMapping> entDbFlightIdMappingListIt = entDbFlightIdMappingList.iterator();
		while(entDbFlightIdMappingListIt.hasNext()){
			EntDbFlightIdMapping entDbFlightIdMapping = entDbFlightIdMappingListIt.next();	
			startTime = new Date().getTime();    	
			try{
	  		paxCal.SummarizePaxForMainFlight(entDbFlightIdMapping);
			}catch(Exception e){
				LOG.error("exception happen for pax summarizer, {}", e);
			}
	  		
	  		endTime = new Date().getTime();
			LOG.info("takes {} ms to summarize the pax info for main flight, idFlight: {}", (endTime - startTime), entDbFlightIdMapping.getIdFlight());
			
		}
	
				
//				if (idFlight != null){
//				/* summarize for main flight */
//			  		startTime = new Date().getTime();    		
//			  		paxCal.SummarizePaxForMainFlight(idFlight.trim());

//			  		endTime = new Date().getTime();
//					LOG.info("takes {} ms to summarize the pax info for main flight, idFlight: {}", (endTime - startTime), idFlight);
//;
//				// get paxConn list using main flight ID
////				List<EntDbFlightIdMapping> flightIdMappingList = dlFlightMappingBean
////						.getFlightIdMappingList(urnoString);

//
//				entDbPaxConnList = dlTestConBean.getPaxConnListByIntFlidX(idFlight.trim());


//				
//
//				if (entDbPaxConnList != null && entDbPaxConnList.size() > 0) {

//					
//					Iterator<EntDbLoadPaxConn> entDbPaxConnIt = entDbPaxConnList
//							.iterator();

//
//					/*summarize the connection flights */
//					while (entDbPaxConnIt.hasNext()) {
//						EntDbLoadPaxConn entDbPaxConn = entDbPaxConnIt.next();
//						startTime = new Date().getTime(); 
//						paxCal.SummarizePaxForConnFlight(idFlight.trim(),
//								entDbPaxConn.getAirlineCode(),
//								entDbPaxConn.getFlightNumber(),
//								entDbPaxConn.getFlightNumberSuffice(),
//								entDbPaxConn.getScheduledFlightDateTime());






//						endTime = new Date().getTime();
//						LOG.info("takes {} ms to summarize the pax info for main flight, intFliId: {}", (endTime - startTime), idFlight);
//					}
//				}
//				
//				
//
////			}
//
//		}
	}
	
}
