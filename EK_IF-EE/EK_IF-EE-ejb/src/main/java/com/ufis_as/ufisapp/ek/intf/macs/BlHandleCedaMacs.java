///*
// * $Id$
// *
// * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
// * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
// */
//package com.ufis_as.ufisapp.ek.intf.macs;
//
//import java.lang.reflect.Field;
//import java.lang.reflect.Method;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.UUID;
//
//import javax.ejb.EJB;
//import javax.ejb.Stateless;
//import javax.ejb.TransactionAttribute;
//import javax.ejb.TransactionAttributeType;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ufis_as.configuration.HpEKConstants;
//import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
//import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
//import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
//import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
//import com.ufis_as.ek_if.macs.entities.EntDbServiceRequest;
//import com.ufis_as.ek_if.macs.entities.LoadPaxPK;
//import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
//import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
//import com.ufis_as.ufisapp.ek.eao.DlFlightInfo;
//import com.ufis_as.ufisapp.ek.eao.DlLoadPaxBean;
//import com.ufis_as.ufisapp.ek.eao.DlPaxConBean;
//import com.ufis_as.ufisapp.ek.eao.DlPaxServiceRequestBean;
//import com.ufis_as.ufisapp.ek.eao.IDlLoadBagUpdateLocal;
//import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
//import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
//import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
//import com.ufis_as.ufisapp.lib.EnumTimeInterval;
//import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
//import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
//import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
//import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.DlFevBean;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.DlJnotabBean;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
//import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
//import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbFevtab;
//import com.ufis_as.ufisapp.utils.HpUfisUtils;
//
//import ek.macs.flightdetails.FlightDetails;
////import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
////import com.ufis_as.ek_if.macs.entities.EntDbLoadSummary;
////import com.ufis_as.ufisapp.ek.eao.DlLoadSummaryBean;
//
//
///**
// * @author $Author$
// * @VERSION $Revision$
// */
//@Stateless
//public class BlHandleCedaMacs extends BaseDTO {
//
//    /**
//     * Logger
//     */
//    private static final Logger LOG = LoggerFactory.getLogger(BlHandleCedaMacs.class);
//    
//    /**
//     * CDI
//     */
//    @EJB
//    IAfttabBeanLocal _afttabBeanLocal;
//    @EJB
//    DlFlightInfo _flightInfo;
//    @EJB
//    DlPaxConBean _paxConBean;
//    @EJB
//    DlLoadPaxBean _paxbean;
//    @EJB
//    DlPaxServiceRequestBean _dlPaxServiceRequestBean;
//    @EJB
//    private DlFlightIdMappingBean _flightIdMappingBean;
//    @EJB
//    private IDlLoadPaxSummaryBeanLocal _loadPaxSummaryBean;
//    @EJB
//    private DlFevBean _fevBean;
//    @EJB
//	private BlIrmtabFacade _irmtabFacade;
//    @EJB
//    private DlJnotabBean _jnoBean;
//	@EJB
//	private DlFlightIdMappingBean dlFlightMappingBean;
//	@EJB
//	private IDlLoadBagUpdateLocal dlLoadBagUpdateLocal;
//	@EJB
//	private BlUfisBCQueue ufisQueueProducer;
//	@EJB
//	private BlUfisBCTopic ufisTopicProducer;
//	
//	
//    /**
//     * Constant and Variables
//     */
//    private static final String INTSYSTEM_MACS_FLT = "MACS";
//    private static final String MSGIF = "MACS-FLT";
//    //private static final String LOAD_INFOID = "PCF_T";
//    private static final String LOAD_INFOID = "CFG";
//    private static final String DATA_SOURCE = "SYS";
//    //private static final String HOPO = "DXB";
//    
//    private static final String CLASS_INDICATOR_FST = "F";
//    private static final String CLASS_INDICATOR_BUS = "J";
//    private static final String CLASS_INDICATOR_ECO = "Y";
//    
//    private static final String TAB_ID_MAPPING = "FLT_ID_MAPPING";
//    private static final String NOTIFY_INTFLID = "INTERFACE_FLTID";
//    
//    private boolean isMapped = false;
//    private EntUfisMsgHead msgHead;
//    private List<EntUfisMsgACT> actions = null;
//    private String intFlid = null;
//    
//    public BlHandleCedaMacs() {
//    	if (msgHead == null) {
//    		msgHead = new EntUfisMsgHead();
//    		msgHead.setHop(HpEKConstants.HOPO);
//    		msgHead.setOrig(MSGIF);
//        }
//    	
//    	tableRef = HpUfisAppConstants.CON_IRMTAB;
//    	exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
//    	dtfl = com.ufis_as.ufisapp.configuration.HpEKConstants.EK_MACSFLT_SOURCE;
//    }
//    
//    private void handleEvent(FlightDetails flightData, EntDbAfttab aftFlight) {
//    	try {
//    		// For macs-flt, only keep Flight status: FO and PD
//    		if (flightData != null && HpUfisUtils.isNotEmptyStr(flightData.getFlightStatus())) {
//    			// 2013-07-10 updated by JGO - store all status
//				// if ("FO".equalsIgnoreCase(flightData.getFlightStatus())
//				// || "PD".equalsIgnoreCase(flightData.getFlightStatus())) {
//				// FEVTAB
//				// find mapped flight from afttab
//		        if (aftFlight != null) {
//		        	HpUfisCalendar nowUtc = new HpUfisCalendar(new Date());
//		        	nowUtc.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
//		        	
//		        	// TODO update event ???
//		        	EntDbFevtab fevRecord = _fevBean.getFevtab(aftFlight.getUrno(), flightData.getFlightStatus());
//		        	if (fevRecord == null) {
//		        		fevRecord = new EntDbFevtab();
//		        		// get id from jnotab
//		        		fevRecord.setURNO(new BigDecimal(HpUfisUtils.formatJavaFevUrno(_jnoBean.getNextValueByKey("FEV-URNO", 1))));
//		        		// cdat
//		            	fevRecord.setCDAT(nowUtc.getCedaString());
//		            	// usec
//		            	fevRecord.setUSEC(INTSYSTEM_MACS_FLT);
//		            	// intSystem and intFlid
//		            	fevRecord.setINAM(INTSYSTEM_MACS_FLT);
//		            	fevRecord.setIFID(flightData.getMflId());
//		            	
//		            	// ------------------------------------------------------
//		            	fevRecord.setUAFT(aftFlight.getUrno());
//		            	fevRecord.setSTNM("FLIGHT_STATUS");
//		            	fevRecord.setSTCO(flightData.getFlightStatus());
//		            	fevRecord.setSTTM(nowUtc.getCedaString());
//		            	fevRecord.setSTFL("");
//		            	//fevRecord.setSTRM("FLIGHT_STATUS");
//		            	//fevRecord.setUSEU(INTSYSTEM_MACS_FLT);
//		            	//fevRecord.setLSTU(nowUtc.getCedaString());
//		            	_fevBean.persist(fevRecord);
//		        	} else {
//		        		fevRecord.setUSEU(INTSYSTEM_MACS_FLT);
//		        		fevRecord.setSTTM(nowUtc.getCedaString());
//		        		fevRecord.setLSTU(nowUtc.getCedaString());
//		        		_fevBean.update(fevRecord);
//		        	}
//		        } else {
//		        	// when afttab record is not found, use intsystem and intflid to insert event
//		        	HpUfisCalendar nowUtc = new HpUfisCalendar(new Date());
//		        	nowUtc.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
//		        	// TODO need to know whether same flight different event got different mflid
//		        	EntDbFevtab fevRecord = _fevBean.getFevtab(INTSYSTEM_MACS_FLT, flightData.getMflId());
//		        	if (fevRecord == null) {
//		        		fevRecord = new EntDbFevtab();
//		        		// get id from jnotab
//		        		fevRecord.setURNO(new BigDecimal(HpUfisUtils.formatJavaFevUrno(_jnoBean.getNextValueByKey("FEV-URNO", 1))));
//		        		// cdat
//		            	fevRecord.setCDAT(nowUtc.getCedaString());
//		            	// usec
//		            	fevRecord.setUSEC(INTSYSTEM_MACS_FLT);
//		            	// intSystem and intFlid
//		            	fevRecord.setINAM(INTSYSTEM_MACS_FLT);
//		            	fevRecord.setIFID(flightData.getMflId());
//		            	
//		            	// ------------------------------------------------------
//		            	//fevRecord.setUAFT(result_entDbAfttab.getUrno());
//		            	
//		            	fevRecord.setSTNM("FLIGHT_STATUS");
//		        		fevRecord.setSTCO(flightData.getFlightStatus());
//		        		fevRecord.setSTTM(nowUtc.getCedaString());
//		            	fevRecord.setSTFL("");
//		            	//fevRecord.setSTRM("FLIGHT_STATUS");
//		            	//fevRecord.setUSEU(INTSYSTEM_MACS_FLT);
//		            	//fevRecord.setLSTU(nowUtc.getCedaString());
//		            	_fevBean.persist(fevRecord);
//		        	}
//		        }
//				// } else {
//				// LOG.debug("Macs-Flt(event handle): flight status<{}> is not FO or PD",
//				// flightData.getFlightStatus());
//				// }
//    		} else {
//    			LOG.debug("Macs-Flt(event handle): flight status is null or empty");
//    		}
//    	} catch (Exception e) {
//    		LOG.error("Macs-Flt(event handle): Cannot insert data to Fevtab");
//    		LOG.error("MAcs-Flt Exception: {}", e.getMessage());
//    	}
//    }
//    
//    private void saveOrUpdateLoadInfo(FlightDetails flightData, EntDbAfttab aftFlight) {
//    	try {
//	    	// ================================================  
//	    	// LoadSummary update (Class-configuration)
//	    	// ================================================
//    		// current time
//    		HpUfisCalendar ufisCal = new HpUfisCalendar(new Date());
//    		ufisCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
//    		
//	    	// 2013-07-10 update JGO move load info to new table<LOAD_PAX_SUMMARY>
//			// LoadSummaryPK key = new LoadSummaryPK();
//			// key.setIntFlId(new BigDecimal(flightData.getMflId()));
//			// key.setInfoId(LOAD_INFOID);
//			// key.setAirlineCode(" ");
//			// key.setFlightNumber(" ");
//			// key.setFlightNumberSuffix(" ");
//			// key.setScheduledFlightDateTime(ufisCal.getTime());
//	    	
//	    	// EntDbLoadSummary load = _loadSummaryBean.getLoadSummaryByPkey(key);
//			// if (load == null) {
//			// load = new EntDbLoadSummary();
//			// load.setlPk(key);
//			// load.setIntSystem(INTSYSTEM_MACS_FLT);
//			// load.set_idHopo(HpEKConstants.HOPO);
//			// load.set_createdDate(ufisCal.getTime());
//			// load.set_createdUser("MACS-FLT");
//			// _loadSummaryBean.persist(load);
//			// }
//	    	
//			EntDbLoadPaxSummary load = _loadPaxSummaryBean
//					.getLoadPaxSummaryByMfid(new BigDecimal(flightData.getMflId()), INTSYSTEM_MACS_FLT);
//			if (load == null) {
//				load = new EntDbLoadPaxSummary();
//				load.setInterfaceFltId(new BigDecimal(flightData.getMflId()));
//				load.setDataSource(DATA_SOURCE);
//				_loadPaxSummaryBean.create(load);
//			}
//			// config load
//			load.setInfoType(LOAD_INFOID);
//			
//    		// update afttab flight urno to load summary table
//	    	if (aftFlight != null) {
//	    		if ('A' == aftFlight.getAdid()) {
//	    			//load.setArrFlId(String.valueOf(aftFlight.getUrno()));
//	    			load.setIdArrFlight(aftFlight.getUrno());
//	    		} else if ('D' == aftFlight.getAdid()) {
//	    			//load.setDepFlId(String.valueOf(aftFlight.getUrno()));
//	    			load.setIdDepFlight(aftFlight.getUrno());
//	    		}
//	    	}
//	    	
//	    	// update class configuration
//	    	String classConfig = flightData.getClassConfiguration();
//	    	if (HpUfisUtils.isNotEmptyStr(classConfig)) {
//	    		int indexF = classConfig.indexOf(CLASS_INDICATOR_FST);
//	    		int indexJ = classConfig.indexOf(CLASS_INDICATOR_BUS);
//	    		int indexY = classConfig.indexOf(CLASS_INDICATOR_ECO);
//	    		String figure = "";
//	    		// first
//	    		if (indexF != -1 && indexJ != -1 && (indexJ - indexF) > 0) {
//	    			figure = classConfig.substring(indexF + 1, indexJ);
//	    			//load.setFirstClass(Integer.parseInt(figure));
//	    			load.setFirstPax(new BigDecimal(figure));
//	    		}
//	    		// business
//	    		if (indexJ != -1 && indexY != -1 && (indexY - indexJ) > 0) {
//	    			figure = classConfig.substring(indexJ + 1, indexY);
//	    			//load.setBusinessClass(Integer.parseInt(figure));
//	    			load.setBusinessPax(new BigDecimal(figure));
//	    		}
//	    		// economy
//	    		if (indexY != -1) {
//	    			figure = classConfig.substring(indexY + 1);
//	    			//load.setEconomyClass(Integer.parseInt(figure));
//	    			load.setEconPax(new BigDecimal(figure));
//	    		}
//	    	}
//	    	load.setUpdatedDate(ufisCal.getTime());
//	    	load.setDataSource(INTSYSTEM_MACS_FLT);
//	    	_loadPaxSummaryBean.edit(load);
//	    	LOG.debug("Class Configuration Info has been created/updated for MFL_ID: {}", 
//	    			flightData.getMflId());
//    	} catch (Exception e) {
//    		LOG.error("Macs-Flt(load info): Cannot insert/update load summary class configuration");
//    		LOG.error("MAcs-Flt Exception: {}", e);
//	    }
//    }
//    
//    private void handleMapping(FlightDetails flightData, EntDbAfttab queryResult) {
//    	try {
//	    	// ================================================
//	    	// MACS-FLT and Ceda Afttab mapping
//	    	// ================================================    	
//	    	// flight id mapping
//	    	EntDbFlightIdMapping flightIdMapping = null;
//	    	
//	    	// MACS FLT unique identifier (mandatory field and unique)
//	    	String intFlid = flightData.getMflId();
//	    	if (HpUfisUtils.isNotEmptyStr(intFlid)) {
//	    		// find mapping record by MFL_ID
//	    		flightIdMapping = _flightIdMappingBean.getFlightIdMapping(intFlid);
//	    		
//	    		// find mapping record by flight info 
//	    		// if aldy exist then drop this message, will not map twice
//	    		// if no mapped record for the flight, then will looking for afttab to create mapping
//	    		if (flightIdMapping == null) {
//	    			//EntDbAfttab criteriaEntity = buildAftCriteria(flightData);
//	    			flightIdMapping = _flightIdMappingBean.getMappingByFlightInfo(queryResult);
//	    			if (flightIdMapping == null) {
//	    				// find flight from afttab
//	    				// EntDbAfttab entDbAfttab = _afttabBeanLocal.findFlight(criteriaEntity);
//	    				if (queryResult != null) {
//	    					flightIdMapping = new EntDbFlightIdMapping();
//	    					// interface info
//	    	            	//flightIdMapping.setIntSystem(INTSYSTEM_MACS_FLT);
//	    	            	flightIdMapping.setIntFltId(intFlid);
//
//	    	            	// flight info
//	    	            	flightIdMapping.setIdFlight(queryResult.getUrno());
//	    	            	flightIdMapping.setArrDepFlag(queryResult.getAdid());
//	    	            	flightIdMapping.setFltNumber(queryResult.getFlno().trim());
//	    	            	
//	    	            	HpUfisCalendar scheduleDateTime = null;
//	    	            	// 2013-09-13 updated by JGO - all message base on boardpoint
////	    	            	if ('D' == queryResult.getAdid()) {
//    	            		scheduleDateTime = new HpUfisCalendar(queryResult.getStod());
////	    	            	} else {
////	    	            		scheduleDateTime = new HpUfisCalendar(queryResult.getStoa());
////	    	            	}
//	    	            	flightIdMapping.setFltDate(scheduleDateTime.getTime());
//	    	            	
//	    	        		// schedule time in local
//	    	        		if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDate())) {
//	    	                	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDate(), 
//	    	                			HpEKConstants.MACS_TIME_FORMAT);
//	    	                	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTime())){
//	    	                        int hour=Integer.parseInt(flightData.getOperationTime().substring(0, 2));
//	    	                        int min=Integer.parseInt(flightData.getOperationTime().substring(2, 4));
//	    	                        scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
//	    	                        scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
//	    	                    }
//	    	                	flightIdMapping.setFltDateLocal(scheduleDateTime.getTime());
//	    	                }
//	    	            	flightIdMapping.setDataSource(INTSYSTEM_MACS_FLT);
//	    		            
//	    		            /*HpUfisCalendar scheduleDateTime = null;
//	    		            if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDateGmt())) {
//	    		            	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDateGmt(), HpEKConstants.MACS_TIME_FORMAT);
//	    		            	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTimeGmt())){
//	    		                    int hour=Integer.parseInt(flightData.getOperationTimeGmt().substring(0, 2));
//	    		                    int min=Integer.parseInt(flightData.getOperationTimeGmt().substring(2, 4));
//	    		                    scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
//	    		                    scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
//	    		                }
//	    		            	flightIdMapping.setScheduledFlightDatetime(scheduleDateTime.getTime());
//	    		            } else {
//	    		            	LOG.debug("MACS-FLT flight operation date is null");
//	    		            }*/
//	    	            	_flightIdMappingBean.persist(flightIdMapping);
//	    	            	intFlid = flightIdMapping.getIntFltId();
//	    	            	isMapped = true;
//	    	            	LOG.debug("Macs-Flt<id: {}> and Afttab<urno: {}> mapping has been created.", 
//	    	            			flightIdMapping.getIntFltId(), flightIdMapping.getIdFlight());
//	    				}
//	    			} else {
//	    				LOG.debug("Macs-Flt(Flight mapping): Mapping table aldy have mapped value for flno={}, flda={}",
//	    						queryResult.getAlc2() + queryResult.getFltn(), queryResult.getFlda());
//	    			}
//	    		}
//	    	} else {
//	    		LOG.debug("MACS-FLT MFL_ID is null or empty");
//	    	}
//    	} catch (Exception e) {
//    		LOG.error("Macs-Flt(Flight mapping): Cannot insert/update data to FlightIdMapping table");
//    		LOG.error("MAcs-Flt Exception: {}", e.getMessage());
//    	}
//    }
//    
//    private EntDbAfttab buildAftCriteria(FlightDetails flightData) {
//    	// build afttab flight search criteria 
//        EntDbAfttab entDbAfttab = new EntDbAfttab();
//        HpUfisCalendar scheduleDateTime = null;
//        
//    	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDateGmt())) {
//        	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDateGmt(), 
//        			HpEKConstants.MACS_TIME_FORMAT);
//        	// 2013-10-03 updated by JGO - adding FLUT for MACS-FLT
//        	entDbAfttab.setFlut(scheduleDateTime.getCedaDateString());
//        	// combine with the time part
//        	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTimeGmt())){
//                int hour=Integer.parseInt(flightData.getOperationTimeGmt().substring(0, 2));
//                int min=Integer.parseInt(flightData.getOperationTimeGmt().substring(2, 4));
//                scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
//                scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
//            }
//        }
//    	
//    	// flight info
//        entDbAfttab.setFltn(HpUfisUtils.formatCedaFltn(flightData.getFlightNumber()));
//		if (HpUfisUtils.isNotEmptyStr(flightData.getFlightNumberExp())) {
//		    entDbAfttab.setFlns(flightData.getFlightNumberExp().charAt(0));
//		}
//		entDbAfttab.setOrg3(flightData.getBoardPoint());
//		if (HpEKConstants.HOPO.equals(flightData.getBoardPoint())) {
//			entDbAfttab.setAdid('D');
//		} else {
//			entDbAfttab.setAdid('A');
//		}
//		// airline codes
//		if (HpUfisUtils.isNotEmptyStr(flightData.getAirlineDesignatorExp())) {
//		    entDbAfttab.setAlc3(flightData.getAirlineDesignator() + 
//		    		flightData.getAirlineDesignatorExp());
//		} else {
//			entDbAfttab.setAlc2(flightData.getAirlineDesignator());
//		}
//		// schedule time
//		if (scheduleDateTime != null) {
//			if (HpEKConstants.HOPO.equals(flightData.getBoardPoint())) {
//				entDbAfttab.setStod(scheduleDateTime.getCedaString());
//			} else {
//				entDbAfttab.setStoa(scheduleDateTime.getCedaString());
//			}
//		}
//		// 2013-10-03 updated by JGO - use flut instead of flda for flight query
//		// execution date in local
//		/*if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDate())) {
//			scheduleDateTime = new HpUfisCalendar(flightData.getOperationDate(), 
//					HpEKConstants.MACS_TIME_FORMAT);
//			entDbAfttab.setFlda(scheduleDateTime.getCedaDateString());
//		}*/
//		return entDbAfttab;
//    }
//    
//    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//    public void handleMACSFLT(FlightDetails flightData, String message, long irmtabRef) {
//    	boolean isValid = true;
//    	isMapped = false;
//    	intFlid = null;
//    	data.clear();
//    	irmtabUrno = irmtabRef;
//    	
//    	// mandatory item check
//    	if (HpUfisUtils.isNullOrEmptyStr(flightData.getMflId())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "MflId");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFltSnapshotDay())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FltSnapshotDay");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getAirlineDesignator())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "AirlineDesignator");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFlightNumber())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FlightNumber");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationDate())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationDate");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationTime())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationTime");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationDateGmt())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationDateGmt");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationTimeGmt())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationTimeGmt");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getBoardPoint())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "BoardPoint");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getClassConfiguration())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "ClassConfiguration");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getRegistrationNumber())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "RegistrationNumber");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFlightStatus())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FlightStatus");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getStatus())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "Status");
//    		LOG.debug("Message dropped: \n{}", message);
//    	}
//    	
//    	if (!isValid) {
//    		addExptInfo(EnumExceptionCodes.EMAND.name(), EnumExceptionCodes.EMAND.toString());
//    	} else {
//    		// according to flight data to search from afttab
//        	EntDbAfttab queryCriteria = buildAftCriteria(flightData);
//            EntDbAfttab result_entDbAfttab = _afttabBeanLocal.findFlight(queryCriteria);
//            if (result_entDbAfttab == null) {
//            	LOG.debug("Flight Record not found in afttab");
//        		LOG.debug("Message dropped: \n{}", message);
//        		addExptInfo(EnumExceptionCodes.ENOFL.name(),
//    					EnumExceptionCodes.ENOFL.toString());
//            } else {
//	        	// handle the macs-flt and afttab mapping
//	        	handleMapping(flightData, result_entDbAfttab);
//	        	// insert/update class-configuration
//	        	saveOrUpdateLoadInfo(flightData, result_entDbAfttab);
//	        	// insert/update fevtab for events
//	        	handleEvent(flightData, result_entDbAfttab);
//	        	
//	        	if (isMapped) {
//	    			if (actions == null) {
//	    				actions = new ArrayList<>();
//	    			} else {
//	    				actions.clear();
//	    			}
//	    			EntUfisMsgACT act = new EntUfisMsgACT();
//	    			// command: MACS ????
//	    			act.setCmd(HpUfisAppConstants.UfisASCommands.IRT.toString());
//	    			// fields: intflid ????
//	    			act.getFld().add(NOTIFY_INTFLID);
//	    			act.setTab(TAB_ID_MAPPING);
//	    			act.getData().add(intFlid);
//	    			actions.add(act);
//	    			String msg;
//					try {
//						msg = HpUfisJsonMsgFormatter.formDataInJson(actions, msgHead);
//						ufisQueueProducer.sendMessage(msg);
//					} catch (Exception e) {
//						LOG.error("Cannot format object to Json String: {}", e);
//					}
//	        	}
//            }
//    	}
//    }
//
//    public void handleMACSPAX(EntDbLoadPaxConn entDbLoadPaxConn) {	
//    	// set ID_FLIGHT
//    	BigDecimal idFlightStr = new BigDecimal(0);
//    	Character adid = 'D';
//    	EntDbFlightIdMapping entDbFlIdMapping= null;
//    	String flightNo = null;
//    	if (entDbLoadPaxConn.getIntFlId() != null){
//    		
//    		long startTime = new Date().getTime();
//    		flightNo = HpUfisUtils.formatCedaFlno(entDbLoadPaxConn.getAirlineCode().trim(), entDbLoadPaxConn.getFlightNumber(), entDbLoadPaxConn.getFlightNumberSuffice());
//    		entDbFlIdMapping = dlFlightMappingBean.getFlightIdMappingX(flightNo, entDbLoadPaxConn.getScheduledFlightDateTime());
//    		long endTime = new Date().getTime();
//			LOG.info("takes {} ms to search flight from flightIdMapping table for paxConn Msg", (endTime - startTime));
//    	}   
//    	
//     	if (entDbFlIdMapping != null){
//    		idFlightStr = entDbFlIdMapping.getIdFlight();
//    		adid = entDbFlIdMapping.getArrDepFlag();
//    		if (adid != null){
//    			adid = adid;
//    		}
//    	}else {
//    		LOG.debug("Flight not found,  loadPaxConn messgae will be droped, flightNo: {}, schedule flight date time: {} ", flightNo, entDbLoadPaxConn.getScheduledFlightDateTime());
//    		LOG.debug("Message will not be inserted into LOAD_PAX and LOAD_BAG table");
//    		return;
//    	}
//     	
//    	int idFlight = 0;
//    	if (idFlightStr != null){
//    	idFlight = idFlightStr.intValue();
//    	}
//    	
//    	entDbLoadPaxConn.setIdFlight(idFlight);
//    	
////    	// get mainFlightMapping
////    	EntDbFlightIdMapping mainEntDbFlIdMapping= null;
////    	long startTime = new Date().getTime();
////    	mainEntDbFlIdMapping = dlFlightMappingBean.getFlightIdMappingX(entDbLoadPaxConn.getIntId());
////		long endTime = new Date().getTime();
////		LOG.info("takes {} ms to search main flight from flightIdMapping table for paxConn Msg", (endTime - startTime));
//    	
////    	paxCal.SummarizePaxWithOneFlightInfo("924470");
//    	long startTime = new Date().getTime();
//    	EntDbLoadPaxConn entDbLoadPaxConnFind = _paxConBean.findByPkIdX(entDbLoadPaxConn.getIntFlId(), entDbLoadPaxConn.getIntRefNumber());
//    	 long endTime = new Date().getTime();
//		LOG.info("takes {} ms to search records from paxConn table for paxConn Msg", (endTime - startTime));
//    	
//    	if (entDbLoadPaxConnFind != null){
//    		startTime = new Date().getTime();
////    		_paxConBean.Update(entDbLoadPaxConn);
//    		// update loadPaxConn
//			   Class<EntDbLoadPaxConn> entDbLoadPaxConnClass = EntDbLoadPaxConn.class;
//			   Field[] entDbLoadPaxConnClassFields = entDbLoadPaxConnClass.getDeclaredFields();
//			   for(Field f : entDbLoadPaxConnClassFields){
//				   if (!"id".equalsIgnoreCase(f.getName()) && !"serialVersionUID".equalsIgnoreCase(f.getName()) && !"intId".equalsIgnoreCase(f.getName())){
//					   setEntityValue(entDbLoadPaxConnFind, f.getName(), getEntityValue(entDbLoadPaxConn, f.getName()));
//				   }
//			   }   
//			   entDbLoadPaxConnFind.set_recStatus(" ");
//			   entDbLoadPaxConnFind.set_updatedDate(new Date());
//    		endTime = new Date().getTime();
//			LOG.info("takes {} ms to update PaxConn Msg to database", (endTime - startTime));
//    	}else{
//    		startTime = new Date().getTime();
//    		_paxConBean.Persist(entDbLoadPaxConn);
//    		endTime = new Date().getTime();
//			LOG.info("takes {} ms to update PaxConn Msg to database", (endTime - startTime));
//    	}
//    	
//    	
//    	// To update LOAD_BAG table
//    	startTime = new Date().getTime();
////    	EntDbLoadPax entDbLoadPax = entDbLoadPaxConn.getPax();
//    	LoadPaxPK pKId = new LoadPaxPK();
//    	pKId.setIntflid(entDbLoadPaxConn.getIntFlId());
//    	pKId.setIntrefnumber(entDbLoadPaxConn.getIntRefNumber());
//    	pKId.setIntSystem(entDbLoadPaxConn.getIntSystem());
//    	EntDbLoadPax entDbLoadPax = _paxbean.findByPkIdX(pKId);
//    	endTime = new Date().getTime();
//		LOG.info("takes {} ms to search pax records for paxConn Msg", (endTime - startTime));
//    	
//    	if (entDbLoadPax != null){
//    		// update for LOAD_BAG
//    	 	String[] bagTagList = null;
//    	 	//String refNum = entDbLoadPax.getpKId().getIntrefnumber();
//    	 	String refNum = entDbLoadPax.getPKId().getIntrefnumber();
//        	String bagTagInfo = entDbLoadPax.getBagTagInfo();
//        	if (bagTagInfo != null && !"".equals(bagTagInfo.trim())){
//        		bagTagList = bagTagInfo.split("/");
//        	}
//        	
//        	if (bagTagList != null){
//            	for (int i = 0; i < bagTagList.length; i++){
//            		if (bagTagList[i] != null && !"".equals(bagTagList[i].trim())){
//            		startTime = new Date().getTime();
//            		List<EntDbLoadBag> loadBagTagList = dlLoadBagUpdateLocal.getBagMoveDetailsByBagtagPaxX(entDbLoadPax.getIdFlight(), bagTagList[i], refNum);
//            		Iterator<EntDbLoadBag> loadBagTagListIt = loadBagTagList.iterator();
//            		while (loadBagTagListIt.hasNext()){
//            			EntDbLoadBag entDbLoadBag = loadBagTagListIt.next();
//            			if ("A".equalsIgnoreCase(adid.toString())){
//        					entDbLoadBag.setIdArrFlight(idFlight);
//        				}else{
//        					entDbLoadBag.setIdFlight(idFlight);
//        				}
//            			entDbLoadBag.setRecStatus(" ");
//            			entDbLoadBag.setUpdatedDate(new Date());
////        				dlLoadBagUpdateLocal.updateLoadedBag(entDbLoadBag);
//        				entDbLoadBag.setUpdatedUser("MACS");
//        				entDbLoadBag.setDataSource("MACS");	
////        				try {
//    						entDbLoadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
////    					} catch (ParseException e) {
////    						LOG.error("ERROR when getting current UTC time : {}", e.toString());
////    					}
//            		}
//            		
//            		endTime = new Date().getTime();
//        			LOG.info("takes {} ms to search records from loadBag table for PaxConn Msg", (endTime - startTime));
////        			startTime = new Date().getTime();
////        			if (loadBagTagList != null || loadBagTagList.size() > 0){
////            				EntDbLoadBag entDbLoadBag = loadBagTagList.get(0);
////            				if ("A".equalsIgnoreCase(adid)){
////            					entDbLoadBag.setIdArrFlight(idFlight);
////            				}else{
////            					entDbLoadBag.setIdFlight(idFlight);
////            				}
////            				dlLoadBagUpdateLocal.updateLoadedBag(entDbLoadBag);
////            				entDbLoadBag.setUpdatedUser("MACS");
////            				entDbLoadBag.setDataSource("MACS");	
////            				try {
////        						entDbLoadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
////        					} catch (ParseException e) {
////        						LOG.error("ERROR when getting current UTC time : {}", e.toString());
////        					}
////            			}
////        			endTime = new Date().getTime();
////        			LOG.info("takes {} ms to update records to loadBag table for PaxConn Msg", (endTime - startTime));
//            		}
//            	}
//
//    	}else{
//    		LOG.debug("Can not find match record in LOAD_PAX for LOAD_PAX_CONN, bagt tag informatoin can not be updated in LOAD_BAT table ");
//    		return; 
//    	}
//    	}else{
//    		LOG.info("pax not found for pax conn msg, intFlId: {}, intRefNum: {} ",entDbLoadPaxConn.getIntFlId(),entDbLoadPaxConn.getIntRefNumber());
//    	}
//    	
//    }
//    
//    /**
//     * @param entDbPax
//     */
//    public void handleMACSPAX(EntDbLoadPax entDbLoadPax) {
//    	// here need to check whether the entity is is null 
////    	if (entDbLoadPax == null){
////    		LOG.warn("entDbLoadPax is null after unmarshal, pls check");
////    		return; 
////    	}
//    	
//    	// set ID_FLIGHT
//    	BigDecimal idFlightStr = new BigDecimal(0);
//    	Character adid = 'D';
////    	String fltNumber = "";
////    	String fltDate = "";
////    	EntDbFlightIdMapping entDbFlIdMapping= null;
//    	EntDbFlightIdMapping entDbFlIdMapping = null;
//    	if (entDbLoadPax.getPKId().getIntflid() != null){
////      if (entDbLoadPax.getpKId().getIntflid() != null){
////    		long startTime = new Date().getTime();
////    		entDbFlIdMapping = dlFlightMappingBean.getFlightIdMappingX(entDbLoadPax.getpKId().getIntflid().toString().trim());		
////    		long endTime = new Date().getTime();
////			LOG.info("takes {} ms to search flight from flightIdMapping table for paxDetails Msg (getFlightIdMappingX)", (endTime - startTime));
//			
//			long startTime = new Date().getTime();
//			entDbFlIdMapping = dlFlightMappingBean.getFlightIdMapping(entDbLoadPax.getPKId().getIntflid().toString().trim());
////			entDbFlIdMapping = dlFlightMappingBean.getFlightIdMapping(entDbLoadPax.getpKId().getIntflid().toString().trim());
//			long endTime = new Date().getTime();
//			LOG.info("takes {} ms to search flight from flightIdMapping table for paxDetails Msg (getFlightIdMapping)", (endTime - startTime));
//			
//    	} 	
//    	
//    	if (entDbFlIdMapping != null){
//    		idFlightStr = entDbFlIdMapping.getIdFlight();
//    		adid = entDbFlIdMapping.getArrDepFlag();
////    		fltNumber = entDbFlIdMapping.getFlightNumber();
////    		if (entDbFlIdMapping.getScheduledFlightDatetime() != null){
////    		fltDate =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(entDbFlIdMapping.getScheduledFlightDatetime());
////    		}
//    		if (adid != null){
//    			adid = adid;
//    		}
//    	}else {
//    		LOG.debug("Flight not found for intFlightId = {}, loadPax message will be droped", entDbLoadPax.getPKId().getIntflid());
////    		LOG.debug("Flight not found for intFlightId = {}, loadPax message will be droped", entDbLoadPax.getpKId().getIntflid());
//    		LOG.debug("Message will not be inserted into LOAD_PAX and LOAD_BAG table");
//    		return;
//    	}
//    	
//    	int idFlight = 0;
//    	if (idFlightStr != null){
//    	idFlight = idFlightStr.intValue();
//    	}
//    	entDbLoadPax.setIdFlight(idFlight);
//    	
//    	// search records form load_pax table 
//    	long startTime = new Date().getTime();
//    	EntDbLoadPax entDbLoadPaxFind = _paxbean.findByPkIdX(entDbLoadPax.getPKId());
//    	long endTime = new Date().getTime();
//		LOG.info("takes {} ms to search records from loadPax table for paxDetails Msg", (endTime - startTime));
//	
//    	// insert or update to LOAD_PAX table
//    	if (entDbLoadPaxFind != null){
//     		startTime = new Date().getTime();
//    		// update loadPax
//			   Class<EntDbLoadPax> entDbLoadPaxClass = EntDbLoadPax.class;
//			   Field[] entDbLoadPaxClassFields = entDbLoadPaxClass.getDeclaredFields();
//			   for(Field f : entDbLoadPaxClassFields){
//				   if (!"id".equalsIgnoreCase(f.getName()) && !"serialVersionUID".equalsIgnoreCase(f.getName())){
//					   setEntityValue(entDbLoadPaxFind, f.getName(), getEntityValue(entDbLoadPax, f.getName()));
//				   }
//			   }   
//			   entDbLoadPaxFind.set_recStatus(" ");
//			   entDbLoadPaxFind.set_updatedDate(new Date());
//    		  
////    		_paxbean.Update(entDbLoadPax);
//    		endTime = new Date().getTime();
//			LOG.info("takes {} ms to update paxDetails Msg to database", (endTime - startTime));
//    	}else{
//    		startTime = new Date().getTime();
//    		_paxbean.Persist(entDbLoadPax);
//    		endTime = new Date().getTime();
//			LOG.info("takes {} ms to insert paxDetails Msg to database", (endTime - startTime));
//    	}
//    	  	   
//    	// insert or update to LOAD_BAG table for bag tag information
//    	String[] bagTagList = null;
//    	String bagTagInfo = entDbLoadPax.getBagTagInfo();
//    	String refNum = entDbLoadPax.getPKId().getIntrefnumber();
////    	String refNum = entDbLoadPax.getpKId().getIntrefnumber();
//    	String paxName = entDbLoadPax.getPaxName();
//    	String idLoadPax  = "";
////    	if (entDbLoadPaxFind != null){
////    	    idLoadPax = entDbLoadPaxFind;
////    	}else {
////    		idLoadPax = entDbLoadPax.getUuid();
////    	}
//    	if (bagTagInfo != null && !"".equals(bagTagInfo.trim())){
//    		bagTagList = bagTagInfo.split("/");
//    	}
//    	
//    	startTime = new Date().getTime();
//    	List<EntDbLoadBag> loadBagList = dlLoadBagUpdateLocal.getBagMoveDetailsByPaxX(idFlight, refNum, adid.toString());
//    	 endTime = new Date().getTime();
//		LOG.info("takes {} ms to search records from loadBag table for paxDetails Msg", (endTime - startTime));
//    	
//    	if (bagTagList != null){
//    	for (int i = 0; i < bagTagList.length; i++){
//    		if (bagTagList[i] != null && !"".equals(bagTagList[i].trim())){
//    			
//    			EntDbLoadBag entDbLoadBag = null;
////    			int intSize = loadBagList.size();
//    			for (int j =0; j < loadBagList.size();){
//    				if (bagTagList[i].equalsIgnoreCase(loadBagList.get(j).getBagTagStr())){
//    					entDbLoadBag = loadBagList.get(j);
//    					loadBagList.remove(j);
//    					break;
//    				}else{
//    					j++;
//    				}
//    			}
//		
//    			// pase bag tag str to bag tag info
//    			String bagTagStr = bagTagList[i];
//    			String bagTag = null;
//    			
//    			if (bagTagStr != null && bagTagStr.length() > 10){
//    				bagTagStr = bagTagStr.trim();
//    				String codeStr = bagTagStr.substring(0,2);
//    				if ("EK".equalsIgnoreCase(codeStr)){
//    					bagTag = "0176" + bagTagStr.substring(2,8);
//    				}else{
//    					LOG.debug("Unknown Bag Tag Info {}", bagTagStr);
//    				}
//    			}else{
//    				LOG.debug("incorrect bag tag information {}",bagTagStr);
//    			}
//    			
//    			startTime = new Date().getTime();
//    			if (entDbLoadBag == null ){	
//    				 entDbLoadBag = new EntDbLoadBag();	
//    				entDbLoadBag.setBagTagStr(bagTagStr); //
//    				entDbLoadBag.setBagTag(bagTag);
//    				if ("A".equalsIgnoreCase(adid.toString())){
//    				entDbLoadBag.setIdArrFlight(idFlight);
////    				entDbLoadBag.setArrFlightNumber(fltNumber);
////    				entDbLoadBag.setArrFltDate(fltDate);
//    				}else if("D".equalsIgnoreCase(adid.toString())){
//    					entDbLoadBag.setIdFlight(idFlight);
////    					entDbLoadBag.setFlightNumber(fltNumber);
////    					entDbLoadBag.setFltDate(fltDate);
//    				}
//    				entDbLoadBag.setPaxRefNum(refNum);
//    				entDbLoadBag.setPaxName(paxName);   
//    				entDbLoadBag.setIdLoadPax(idLoadPax);
//    				entDbLoadBag.setRecStatus(" ");
//    				entDbLoadBag.setId((UUID.randomUUID()).toString());
//    				dlLoadBagUpdateLocal.saveLoadBagMove(entDbLoadBag);
//    				entDbLoadBag.setCreatedUser("MACS");
//    				entDbLoadBag.setDataSource("MACS");			
//    			}else if (entDbLoadBag != null ){
////    				EntDbLoadBag entDbLoadBag = loadBagTagList.get(0);
//    				entDbLoadBag.setBagTagStr(bagTagStr);   //
//    				entDbLoadBag.setBagTag(bagTag);
////    				entDbLoadBag.setIdFlight(idFlight);
//    				entDbLoadBag.setPaxRefNum(refNum);
//    				entDbLoadBag.setPaxName(paxName);   
//    				entDbLoadBag.setIdLoadPax(idLoadPax);
//    				entDbLoadBag.setRecStatus(" ");
////    				dlLoadBagUpdateLocal.updateLoadedBag(entDbLoadBag);
//    				entDbLoadBag.setUpdatedDate(new Date());
//    				entDbLoadBag.setUpdatedUser("MACS");
//    				entDbLoadBag.setDataSource("MACS");	
////    				try {
//						entDbLoadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
////					} catch (ParseException e) {
////						LOG.error("ERROR when getting current UTC time : {}", e.toString());
////					}
//    			}	
//    			endTime = new Date().getTime();
//    			LOG.info("takes {} ms to update or insert records to loadBag table for paxDetails Msg", (endTime - startTime));
//    
//    			
//    		}
//    	}
//    	
//    }
//    	startTime = new Date().getTime();
//		// update those deleted message
//		if (loadBagList != null && loadBagList.size() >0){
//			Iterator<EntDbLoadBag> loadBagIt = loadBagList.iterator();
//			while(loadBagIt.hasNext()){
//				EntDbLoadBag loadBag = loadBagIt.next();
//				loadBag.setRecStatus("X");
//				loadBag.setUpdatedDate(new Date());
////				dlLoadBagUpdateLocal.updateLoadedBag(loadBag);
//				loadBag.setUpdatedUser("MACS");
//				loadBag.setDataSource("MACS");	
////				try {
//					loadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
////				} catch (ParseException e) {
////					LOG.error("ERROR when getting current UTC time : {}", e.toString());
////				}
//			}
//		}
//		endTime = new Date().getTime();
//		LOG.info("takes {} ms to update those deleted records to loadBag table for paxDetails Msg", (endTime - startTime));
//    	
//    	
//    	
////    	}
//    	
//    	
//    	
//
////    	EntDbPax dbPax = _paxbean.getPax(entDbPax.getIntId());
////        if (dbPax != null) {
////        	entDbPax.setId(dbPaxConn.getId());
////        }
//        /* TODO
//         * To be tested it might generate Exception
//         */
////        dbPaxConn = (EntDbPaxConn) SerializationUtils.clone(entDbPaxInbOwnDetails);
//    }
//    
//    
//    public void handleMACSPAX(EntDbServiceRequest entDbPaxServiceRequest) { 	
//		
//    	// set ID_FLIGHT
//    	EntDbFlightIdMapping entDbFlIdMapping= null;
//    	if (entDbPaxServiceRequest.getIntFlId() != null && !"".equals(entDbPaxServiceRequest.getIntFlId().toString().trim())){
//    		long startTime = new Date().getTime();
//    		entDbFlIdMapping = dlFlightMappingBean.getFlightIdMappingX(entDbPaxServiceRequest.getIntFlId().toString().trim());
//    	    long endTime = new Date().getTime();
//			LOG.info("takes {} ms to get flightID from flightIdMapping table for Fct Msg", (endTime - startTime));
//    	}
//    	int idFlight = 0;
//    	if (entDbFlIdMapping != null){
//    	idFlight = entDbFlIdMapping.getIdFlight().intValue();
//    	entDbPaxServiceRequest.setIdFlight(idFlight);
//    	}else {
//    		LOG.debug("Flight not found for intFlightId = {}, service request message will be droped", entDbPaxServiceRequest.getIntFlId());
//    		return; 
//    	}
//    	    	
//    	long startTime = new Date().getTime();
//    	EntDbServiceRequest entDbPaxServiceRequestFind = _dlPaxServiceRequestBean.findByPkIdX(entDbPaxServiceRequest.getIntFlId(),entDbPaxServiceRequest.getIntRefNumber());
//    	long endTime = new Date().getTime();
//		LOG.info("takes {} ms to search record from ServiceRequest table for Fct Msg", (endTime - startTime));
// 	   
//    	if (entDbPaxServiceRequestFind != null){
//    		startTime = new Date().getTime();
////    		_dlPaxServiceRequestBean.Update(entDbPaxServiceRequest);
//    		
//    		// update serviceRequest
//			   Class<EntDbServiceRequest> entDbServiceRequestClass = EntDbServiceRequest.class;
//			   Field[] entDbServiceRequestFields = entDbServiceRequestClass.getDeclaredFields();
//			   for(Field f : entDbServiceRequestFields){
//				   if (!"id".equalsIgnoreCase(f.getName()) && !"serialVersionUID".equalsIgnoreCase(f.getName()) && !"intId".equalsIgnoreCase(f.getName())){
//					   setEntityValue(entDbPaxServiceRequestFind, f.getName(), getEntityValue(entDbPaxServiceRequest, f.getName()));
//				   }
//			   }   
//			   entDbPaxServiceRequestFind.set_recStatus(" ");
//			   entDbPaxServiceRequestFind.set_updatedDate(new Date());
//    		
//    		endTime = new Date().getTime();
//			LOG.info("takes {} ms to update Fct Msg to database", (endTime - startTime));
//    	}else{
//    		startTime = new Date().getTime();
//    		 _dlPaxServiceRequestBean.Persist(entDbPaxServiceRequest);
//    		endTime = new Date().getTime();
//    			LOG.info("takes {} ms to insert Fct Msg to database", (endTime - startTime));
//    	}
//    }
//    
//    
//    public void handleMACSPAX(List<EntDbLoadPaxSummary> entDbLoadPaxSummaryList) { 	
//    	
//    	Iterator<EntDbLoadPaxSummary> it = entDbLoadPaxSummaryList.iterator(); 
//    	while (it.hasNext()){
//    		EntDbLoadPaxSummary entDbLoadPaxSummary = it.next();
//    		
//    		EntDbLoadPaxSummary EntDbLoadSummaryFind = _loadPaxSummaryBean.findByPk(entDbLoadPaxSummary.getId());
//      	   
//        	if (EntDbLoadSummaryFind != null){
//        		_loadPaxSummaryBean.update(entDbLoadPaxSummary);
//        	}else{
//        		_loadPaxSummaryBean.persist(entDbLoadPaxSummary);
//        	}
//    		
//    	} 
//    	
//
//    }
//    
//    private Object setEntityValue(Object entity, String columnNm, Object value) {
//		   try {
//			if (value == null){
//				return null;
//			}
//			   
//		    String method = getMethodName(columnNm, "set");
//		    if (entity instanceof List) {
//		    } else {
//		     Class<? extends Object> clazz = entity.getClass();
//		     Field field = clazz.getDeclaredField(columnNm); 
//		     Method getMethod = null;
//		     getMethod = clazz.getMethod(method, field.getType());
//		     getMethod.invoke(entity, value);
//		    }
//		   } catch (Exception e) {
//		    e.printStackTrace();
//		   }
//		   return value;
//		  }
//	   
//	   private Object getEntityValue(Object entity, String columnNm) {
//		   Object o = null;
//		   try {
//		    String method = getMethodName(columnNm, "get");
//		    Class<? extends Object> clazz = entity.getClass();
//		    Method getMethod = (Method) clazz.getDeclaredMethod(method);
//		    if (getMethod != null) {
//		     o = getMethod.invoke(entity);
//		    }
//		   } catch (Exception e) {
//		    e.printStackTrace();
//		   }
//
//		   return o;
//		  }
//	   
//	   private String getMethodName(String s, String prefix) {
//		   s = s.replaceAll("_", "");
//		   s = s.substring(0, 1).toUpperCase() + s.substring(1);
//		   s = prefix + s;
//		   return s;
//		  }
//    
//
//
//}
