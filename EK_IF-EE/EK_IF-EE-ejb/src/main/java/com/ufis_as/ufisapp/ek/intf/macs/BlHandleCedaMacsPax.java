/*
 * $Id: BlHandleCedaMacs.java 8987 2013-09-23 07:27:37Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.intf.macs;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequest;
import com.ufis_as.ek_if.macs.entities.LoadPaxPK;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
import com.ufis_as.ufisapp.ek.eao.DlFlightInfo;
import com.ufis_as.ufisapp.ek.eao.DlLoadPaxBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxConBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxServiceRequestBean;
import com.ufis_as.ufisapp.ek.eao.IDlLoadBagUpdateLocal;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlFevBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlJnotabBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;

//import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
//import com.ufis_as.ek_if.macs.entities.EntDbLoadSummary;
//import com.ufis_as.ufisapp.ek.eao.DlLoadSummaryBean;

/**
 * @author $Author: sch $
 * @VERSION $Revision: 8987 $
 */
// @Stateful
@Stateless
public class BlHandleCedaMacsPax {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BlHandleCedaMacsPax.class);

	@EJB
	IAfttabBeanLocal _afttabBeanLocal;
	@EJB
	DlFlightInfo _flightInfo;
	@EJB
	DlPaxConBean _paxConBean;
	@EJB
	DlLoadPaxBean _paxbean;
	@EJB
	BlUfisBCTopic clsBlUfisBCTopic;
	@EJB
	DlPaxServiceRequestBean _dlPaxServiceRequestBean;
	@EJB
	private DlFlightIdMappingBean _flightIdMappingBean;
	@EJB
	private IDlLoadPaxSummaryBeanLocal _loadPaxSummaryBean;
	@EJB
	private DlFevBean _fevBean;
	@EJB
	private DlJnotabBean _jnoBean;
	@EJB
	private DlFlightIdMappingBean dlFlightMappingBean;
	@EJB
	private IDlLoadBagUpdateLocal dlLoadBagUpdateLocal;
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	
	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	
	private static Map<String, String> airlineIataNumber = null;
	
	protected Message rawMsg;
	 String logLevel = null;
		Boolean msgLogged = Boolean.FALSE;
//		long irmtabRef;

	/**
	 * @param entDbPax217
	 */
	public void handleMACSPAX(EntDbLoadPax entDbLoadPax, long irmtabRef) {
		
		logLevel = entStartupInitSingleton.getIrmLogLev();
		
		msgLogged = Boolean.FALSE;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		
		// load the master data for ALTTAB if it is null
		if (airlineIataNumber == null) {
			airlineIataNumber = entStartupInitSingleton.getAirlineCodeCnvertMap();
		}
		
//		notifyEntUfisMsgDTOList = new ArrayList<>();
		// set ID_FLIGHT
		BigDecimal idFlightStr = new BigDecimal(0);
		Character adid = 'D';

		// added by 2013.10.22
		String mainFlightNumber = "";
		String connFlightNumber = "";
		String mainOrigin3 = "";
		String connOrigin3 = "";
		String mainDest3 = "";
		String connDest3 = "";
		String mainFlightDate = "";
		String connFLightDate = "";

		EntDbFlightIdMapping entDbFlIdMapping = null;
		if (entDbLoadPax.getPKId() != null && entDbLoadPax.getPKId().getIntflid() != null) {
			long startTime = System.currentTimeMillis();
			entDbFlIdMapping = dlFlightMappingBean
					.getFlightIdMappingX(entDbLoadPax.getPKId().getIntflid()
							.toString().trim());
			LOG.info("1.2.1 search flight from flightIdMapping table for paxDetails Msg, takes {} ms ",
					(System.currentTimeMillis() - startTime));
		}

		if (entDbFlIdMapping != null) {
			idFlightStr = entDbFlIdMapping.getIdFlight();
			if (entDbFlIdMapping.getArrDepFlag() != null) {
				adid = entDbFlIdMapping.getArrDepFlag();
			}

			// added by 2013.10.22
			mainFlightNumber = entDbFlIdMapping.getFltNumber();
			mainFlightDate = HpEKConstants.MACS_PAX_DATE_FORMAT.format(entDbFlIdMapping.getFltDateLocal());
			mainOrigin3 = entDbLoadPax.getCheckInCity();
			mainDest3 = entDbLoadPax.getDestination();

		} else {
			LOG.debug(
					"Flight not found for intFlightId = {}, set id_flight = 0 ",
					entDbLoadPax.getPKId().getIntflid());
			long startTime = System.currentTimeMillis();

			sendErroNotification(EnumExceptionCodes.ENOFL,irmtabRef);
			LOG.debug("1.2.2 flight not found and update IRMTAB, takes {} ms",
					System.currentTimeMillis() - startTime);

		}

		// if CANCELLED = 'Y', update IRMTAB as invalid
		if ("Y".equalsIgnoreCase(entDbLoadPax.getCancelled())) {
			LOG.info("paxDetails Cancelled = 'Y' paxId {}",
					entDbLoadPax.getIntId());
			// LOG.info("paxDetails Msg is cancelled to update IRMTAB to invalid, intFltId: {}",
			// entDbLoadPax.getPKId().getIntflid());
			// long startTime = System.currentTimeMillis();
			// addExptInfo(EnumExceptionCodes.ENOFL.name(),
			// entDbLoadPax.getPKId().getIntflid());
			// _irmtabFacade.updateIRMStatus(PaxProcessSingleton.nextUrno,
			// HpEKConstants.IRMTAB_STAT_INVALID);
			// LOG.debug("1.2.3 update IRMTAB STAT to invalid, urno: {}, takes {} ms",
			// PaxProcessSingleton.nextUrno, System.currentTimeMillis() -
			// startTime );
		}

		String idLoadPax = "";
		String bagType = "";

		int idFlight = 0;
		if (idFlightStr != null) {
			idFlight = idFlightStr.intValue();
		}
		entDbLoadPax.setIdFlight(idFlight);

		// search records form load_pax table
		long startTime = System.currentTimeMillis();
		entDbLoadPax.setIntSystem(HpEKConstants.MACS_PAX_DATA_SOURCE);
		EntDbLoadPax entDbLoadPaxFind = _paxbean.findByPkIdX(entDbLoadPax.getPKId());
		LOG.info("1.2.4 search records from loadPax table for paxDetails Msg, takes {} ms",
				System.currentTimeMillis() - startTime);

		// insert or update to LOAD_PAX table
		if (entDbLoadPaxFind != null) {

			EntDbLoadPax oldEntDbLoadPax = (EntDbLoadPax)SerializationUtils.clone(entDbLoadPaxFind);

			startTime = new Date().getTime();
			// update loadPax
			Class<EntDbLoadPax> entDbLoadPaxClass = EntDbLoadPax.class;
			Field[] entDbLoadPaxClassFields = entDbLoadPaxClass
					.getDeclaredFields();
			for (Field f : entDbLoadPaxClassFields) {
				if (!"id".equalsIgnoreCase(f.getName())
						&& !"serialVersionUID".equalsIgnoreCase(f.getName())) {
					setEntityValue(entDbLoadPaxFind, f.getName(),
							getEntityValue(entDbLoadPax, f.getName()));
				}
			}
			if ("OF".equalsIgnoreCase(entDbLoadPax.getPaxStatus())){
				entDbLoadPaxFind.set_recStatus("X");
			}else{
				entDbLoadPaxFind.set_recStatus(" ");
			}	
			entDbLoadPaxFind.set_updatedDate(HpUfisCalendar.getCurrentUTCTime());
			entDbLoadPaxFind.set_createdUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
//			_paxbean.Update(entDbLoadPaxFind);
			LOG.info("1.2.5 update paxDetails Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));
			
			// send notification 
			ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
					String.valueOf(entDbLoadPaxFind.getIdFlight()), oldEntDbLoadPax, entDbLoadPaxFind);
			

			idLoadPax = entDbLoadPaxFind.getUuid();


			if (entDbLoadPaxFind.getEntDbPaxConns() == null
					|| entDbLoadPaxFind.getEntDbPaxConns().size() < 1) {
				bagType = HpEKConstants.BAGTYPE_L;

				entDbLoadPaxFind.setIdLoadPaxConnect("0");
			} else {
				LOG.info("PaxConn found for Pax, intFlid {}, refNum {}",
						entDbLoadPaxFind.getpKId().getIntflid(),
						entDbLoadPaxFind.getpKId().getIntrefnumber());
				bagType = HpEKConstants.BAGTYPE_T;
				connOrigin3 = entDbLoadPaxFind.getEntDbPaxConns().get(0)
						.getBoardPoint();
				connDest3 = entDbLoadPaxFind.getEntDbPaxConns().get(0)
						.getOffPoint();
				connFlightNumber = entDbLoadPaxFind.getEntDbPaxConns().get(0)
						.getPaxConxFlno();
				connFLightDate = HpEKConstants.MACS_PAX_DATE_FORMAT
						.format(entDbLoadPaxFind.getEntDbPaxConns().get(0)
								.getConxFltDate());
				entDbLoadPaxFind.setIdLoadPaxConnect(entDbLoadPaxFind
						.getEntDbPaxConns().get(0).getUuid());
				entDbLoadPaxFind.getEntDbPaxConns().get(0)
						.setUuid(entDbLoadPaxFind.getUuid());
				entDbLoadPaxFind.getEntDbPaxConns().get(0)
						.setIdFlight(entDbLoadPaxFind.getIdFlight());
				
				ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
						String.valueOf(entDbLoadPaxFind.getIdFlight()), oldEntDbLoadPax, entDbLoadPaxFind);

			}

		} else {
			startTime = System.currentTimeMillis();
			_paxbean.Persist(entDbLoadPax);

			LOG.info("1.2.5  insert paxDetails Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));
			
			// send notification
			ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
					String.valueOf(entDbLoadPax.getIdFlight()), null, entDbLoadPax);

			startTime = System.currentTimeMillis();
			EntDbLoadPaxConn paxConnForPax = _paxConBean.findByintFltRefX(
					entDbLoadPax.getPKId().getIntflid(), entDbLoadPax.getPKId()
							.getIntrefnumber());
			LOG.info("1.2.5+  search paxConn for Pax, takes {} ms",
					(System.currentTimeMillis() - startTime));

			if (paxConnForPax == null) {
				entDbLoadPax.setIdLoadPaxConnect("0");
				bagType = HpEKConstants.BAGTYPE_L;
			} else {
				
				EntDbLoadPaxConn  oldEntDbLoadPaxConn = (EntDbLoadPaxConn)SerializationUtils.clone(paxConnForPax);
				
				bagType = HpEKConstants.BAGTYPE_T;
				connOrigin3 = paxConnForPax.getBoardPoint();
				connDest3 = paxConnForPax.getOffPoint();
				connFlightNumber = paxConnForPax.getPaxConxFlno();
				connFLightDate = HpEKConstants.MACS_PAX_DATE_FORMAT
						.format(paxConnForPax.getConxFltDate());
				entDbLoadPax.setIdLoadPaxConnect(paxConnForPax.getUuid());
				paxConnForPax.setIdLoadPax(entDbLoadPax.getUuid());
				paxConnForPax.setIdFlight(entDbLoadPax.getIdFlight());

				// send notification 
				ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
						String.valueOf(paxConnForPax.getIdFlight()), oldEntDbLoadPaxConn, paxConnForPax);

			}
		}

		// insert or update to LOAD_BAG table for bag tag information
		String[] bagTagList = null;
		String bagTagInfo = entDbLoadPax.getBagTagInfo();
		String refNum = entDbLoadPax.getPKId().getIntrefnumber();
		String paxName = entDbLoadPax.getPaxName();

		if (bagTagInfo != null && !"".equals(bagTagInfo.trim())) {
			bagTagList = bagTagInfo.split("/");
		}

		List<EntDbLoadBag> loadBagList = new ArrayList<EntDbLoadBag>();

		if (entDbLoadPaxFind != null) {
			startTime = System.currentTimeMillis();
			loadBagList = dlLoadBagUpdateLocal.getBagMoveDetailsByPaxX(
					entDbLoadPaxFind.getUuid());
			LOG.info("1.2.6, search records from loadBag table for paxDetails Msg, takes {} ms ",
					(System.currentTimeMillis() - startTime));
		}

		if (bagTagList != null) {

			boolean isTotalBagWeightSet = false;

			for (int i = 0; i < bagTagList.length; i++) {
				if (bagTagList[i] != null && !"".equals(bagTagList[i].trim())) {

					EntDbLoadBag entDbLoadBag = null;
					
					boolean isLastBagRecord = false;
					// int intSize = loadBagList.size();
					for (int j = 0; j < loadBagList.size();) {
						if (loadBagList.size() == 1){
							isLastBagRecord = true;
						}
						
						if (bagTagList[i].equalsIgnoreCase(loadBagList.get(j)
								.getBagTagStr())) {
							entDbLoadBag = loadBagList.get(j);
							loadBagList.remove(j);
							break;
						} else {
							j++;
						}
					}

					// pase bag tag str to bag tag info
					String bagTagStr = bagTagList[i];
					String bagTag = "000";
					// 2013-11-14 updated by JGO - According to Jira: UFIS-3041
					/*boolean isConvertionFoundFromMd = false;
					Map<String, String> airlineCodeConvertMap = entStartupInitSingleton.getAirlineCodeCnvertMap();
					Iterator airlineCodeConvertMapIt = airlineCodeConvertMap.entrySet().iterator();
					if (bagTagStr != null && bagTagStr.length() > 10) {
						bagTagStr = bagTagStr.trim();
						String codeStr = bagTagStr.substring(0, 2);
						LOG.info("airline code from bagTag info : {} , start to searching from master table",
								codeStr);
						while (airlineCodeConvertMapIt.hasNext()) {
							Map.Entry<String, String> pairs = (Entry<String, String>) airlineCodeConvertMapIt
									.next();
							if (codeStr.equals(pairs.getKey())) {
								bagTag = pairs.getValue()
										+ bagTagStr.substring(2, 8);
								isConvertionFoundFromMd = true;
								LOG.info(
										"airline code {} found from MD and convert to {}",
										pairs.getKey(), pairs.getValue());
							}
						}

						if (!isConvertionFoundFromMd) {
							LOG.info(
									"aline code {} not found in MD cache and set bagTag = 0",
									codeStr);
						}
						// if ("EK".equalsIgnoreCase(codeStr)){
						// bagTag = "0176" + bagTagStr.substring(2,8);
						// }else{
						// LOG.warn("Attention !, Unknown Bag Tag Info {}",
						// bagTagStr);
						// }
					} else {
						LOG.warn(
								"Attention !, incorrect bag tag information {}",
								bagTagStr);
					}*/
					String airline = null;
					String iataNum = null;
					if (bagTagStr != null && bagTagStr.length() > 10) {
						bagTagStr = bagTagStr.trim();
						airline = bagTagStr.substring(0, 2);
						LOG.info("Airline code from bagTag info : {}", airline);
						// find airline iata number
						if (airlineIataNumber != null) {
							iataNum = airlineIataNumber.get(airline);
						} else {
							LOG.warn("No ALTTAB info found, will manually mapping IATA code for known airline");
							// if ALTTAB master data is empty or null, check airline code from tag
							if ("EK".equalsIgnoreCase(airline)) {
								iataNum = "0176";
							} else {
								LOG.warn("No Setting found for airline: {}", airline);
							}
						}
						LOG.debug("IATA number for airline={} is: {}", airline, iataNum);
					} else {
						LOG.warn("Invalid bag tag information: {}", bagTagStr);
					}
					
					if (iataNum != null) {
						bagTag = iataNum + bagTagStr.substring(2,8);
					} else {
						bagTag += bagTagStr.substring(2,8);
					}

					startTime = new Date().getTime();
					if (entDbLoadBag == null) {
						entDbLoadBag = new EntDbLoadBag();
						entDbLoadBag.setBagTagStr(bagTagStr); //
						entDbLoadBag.setBagTag(bagTag);
						entDbLoadBag.setBagType(bagType);
						entDbLoadBag.setBagClassify("GATE");
						if ("A".equalsIgnoreCase(adid.toString())) {
							entDbLoadBag.setIdArrFlight(idFlight);
							// added by 2013.10.22
							entDbLoadBag.setArrFlightNumber(mainFlightNumber);
							entDbLoadBag.setFlightNumber(connFlightNumber);
							entDbLoadBag.setArrFltDate(mainFlightDate);
							entDbLoadBag.setFltDate(connFLightDate);
							entDbLoadBag.setArrFltOrigin3(mainOrigin3);
							entDbLoadBag.setFltOrigin3(connOrigin3);
							entDbLoadBag.setArrFltDest3(mainDest3);
							entDbLoadBag.setFltDest3(connDest3);
						} else if ("D".equalsIgnoreCase(adid.toString())) {
							entDbLoadBag.setIdFlight(idFlight);
							// added by 2013.10.22
							entDbLoadBag.setArrFlightNumber(connFlightNumber);
							entDbLoadBag.setFlightNumber(mainFlightNumber);
							entDbLoadBag.setArrFltDate(connFLightDate);
							entDbLoadBag.setFltDate(mainFlightDate);
							entDbLoadBag.setArrFltOrigin3(connOrigin3);
							entDbLoadBag.setFltOrigin3(mainOrigin3);
							entDbLoadBag.setArrFltDest3(connDest3);
							entDbLoadBag.setFltDest3(mainDest3);
						}

						// added by 2013.10.22 2:19
						if (entDbLoadPax.getTravelledClass() != null) {
							entDbLoadBag.setBagClass(entDbLoadPax
									.getTravelledClass());
						} else {
							entDbLoadBag.setBagClass(entDbLoadPax
									.getBookedClass());
						}

						// added by 2013.10.22
						if (!isTotalBagWeightSet) {
							entDbLoadBag.setBagWeight(entDbLoadPax
									.getBagWeight());
							isTotalBagWeightSet = true;
						} else {
							entDbLoadBag.setBagWeight(new BigDecimal(0));
						}

						entDbLoadBag.setPaxRefNum(refNum);
						entDbLoadBag.setPaxName(paxName);
						entDbLoadBag.setIdLoadPax(idLoadPax);
						if ("OF".equalsIgnoreCase(entDbLoadPax.getPaxStatus())){
							entDbLoadBag.setRecStatus("X");
						}else{
							entDbLoadBag.setRecStatus(" ");
						}	
						entDbLoadBag.setId((UUID.randomUUID()).toString());
						dlLoadBagUpdateLocal.saveLoadBagMove(entDbLoadBag);
						entDbLoadBag
								.setCreatedUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
						entDbLoadBag
								.setDataSource(HpEKConstants.MACS_PAX_DATA_SOURCE);
						LOG.info(
								"1.2.6 insert records to loadBag table for paxDetails Msg, takes {} ms",
								(System.currentTimeMillis() - startTime));
						
						if (isLastBagRecord){
							// send notification
							ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
									String.valueOf(entDbLoadBag.getIdFlight()), null, entDbLoadBag);
						}else{
							// send notification
							ufisTopicProducer.sendNotification(false, UfisASCommands.IRT,
									String.valueOf(entDbLoadBag.getIdFlight()), null, entDbLoadBag);
						}
			

					} else if (entDbLoadBag != null) {
						
						EntDbLoadBag oldEntDbLoadBag = (EntDbLoadBag)SerializationUtils.clone(entDbLoadBag);
						
						// EntDbLoadBag entDbLoadBag = loadBagTagList.get(0);
						entDbLoadBag.setBagTagStr(bagTagStr); //
						entDbLoadBag.setBagTag(bagTag);
						// entDbLoadBag.setIdFlight(idFlight);
						entDbLoadBag.setPaxRefNum(refNum);
						entDbLoadBag.setPaxName(paxName);
						// entDbLoadBag.setIdLoadPax(idLoadPax);
						entDbLoadBag.setRecStatus(" ");

						if ("A".equalsIgnoreCase(adid.toString())) {
							entDbLoadBag.setIdArrFlight(idFlight);
							// added by 2013.10.22
							entDbLoadBag.setArrFlightNumber(mainFlightNumber);
							entDbLoadBag.setFlightNumber(connFlightNumber);
							entDbLoadBag.setArrFltDate(mainFlightDate);
							entDbLoadBag.setFltDate(connFLightDate);
							entDbLoadBag.setArrFltOrigin3(mainOrigin3);
							entDbLoadBag.setFltOrigin3(connOrigin3);
							entDbLoadBag.setArrFltDest3(mainDest3);
							entDbLoadBag.setFltDest3(connDest3);
						} else if ("D".equalsIgnoreCase(adid.toString())) {
							entDbLoadBag.setIdFlight(idFlight);
							// added by 2013.10.22
							entDbLoadBag.setArrFlightNumber(connFlightNumber);
							entDbLoadBag.setFlightNumber(mainFlightNumber);
							entDbLoadBag.setArrFltDate(connFLightDate);
							entDbLoadBag.setFltDate(mainFlightDate);
							entDbLoadBag.setArrFltOrigin3(connOrigin3);
							entDbLoadBag.setFltOrigin3(mainOrigin3);
							entDbLoadBag.setArrFltDest3(connDest3);
							entDbLoadBag.setFltDest3(mainDest3);
						}

						// added by 2013.10.22
						if (!isTotalBagWeightSet) {
							entDbLoadBag.setBagWeight(entDbLoadPax
									.getBagWeight());
							isTotalBagWeightSet = true;
						} else {
							entDbLoadBag.setBagWeight(new BigDecimal(0));
						}

						// dlLoadBagUpdateLocal.updateLoadedBag(entDbLoadBag);
						entDbLoadBag.setUpdatedDate(HpUfisCalendar
								.getCurrentUTCTime());
						entDbLoadBag
								.setUpdatedUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
						entDbLoadBag
								.setDataSource(HpEKConstants.MACS_PAX_DATA_SOURCE);
						entDbLoadBag.setUpdatedDate(HpUfisCalendar
								.getCurrentUTCTime());
						LOG.info(
								"1.2.6 update records to loadBag table for paxDetails Msg, takes {} ms",
								(System.currentTimeMillis() - startTime));
						
						if (isLastBagRecord){
						// send notification 
						ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
								String.valueOf(entDbLoadBag.getIdFlight()), oldEntDbLoadBag, entDbLoadBag);
						}else{
							// send notification 
							ufisTopicProducer.sendNotification(false, UfisASCommands.URT,
									String.valueOf(entDbLoadBag.getIdFlight()), oldEntDbLoadBag, entDbLoadBag);
						}
					}

				}
			}

		}
		startTime = System.currentTimeMillis();
		// update those deleted message
		if (loadBagList != null && loadBagList.size() > 0) {
			Iterator<EntDbLoadBag> loadBagIt = loadBagList.iterator();
			while (loadBagIt.hasNext()) {
				EntDbLoadBag loadBag = loadBagIt.next();
				
				loadBag.setRecStatus("X");
				loadBag.setUpdatedDate(new Date());
				// dlLoadBagUpdateLocal.updateLoadedBag(loadBag);
				loadBag.setUpdatedUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
				loadBag.setDataSource(HpEKConstants.MACS_PAX_DATA_SOURCE);
				loadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());


				if (loadBagIt.hasNext()){
					// send notification 
					ufisTopicProducer.sendNotification(false, UfisASCommands.DRT,
							String.valueOf(entDbLoadPaxFind.getIdFlight()), null, entDbLoadPaxFind);
				}else{
					// send notification 
					ufisTopicProducer.sendNotification(true, UfisASCommands.DRT,
							String.valueOf(entDbLoadPaxFind.getIdFlight()), null, entDbLoadPaxFind);
				}
				
			
			}
		}
		LOG.info(
				"1.2.7 update those deleted records to loadBag table for paxDetails Msg, takes {} ms",
				(System.currentTimeMillis() - startTime));
	
	}

	public void handleMACSPAX(EntDbLoadPaxConn entDbLoadPaxConn,
			Character arrDepFlag, long irmtabRef) {
		
		LOG.debug("!!! paxConn intId {}, entDbLoadPaxConn.hashCode(){}", entDbLoadPaxConn.getIntId(),entDbLoadPaxConn.hashCode());
		
		logLevel = entStartupInitSingleton.getIrmLogLev();
		
		msgLogged = Boolean.FALSE;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		
//		notifyEntUfisMsgDTOList = new ArrayList<>();
		// // set ID_FLIGHT
		BigDecimal idFlightStr = new BigDecimal(0);
		EntDbFlightIdMapping entDbFlIdMapping = null;
//		String flightNo = null;
		if (entDbLoadPaxConn.getPaxConnPK().getInterfaceFltid() != null) {
			long startTime = System.currentTimeMillis();

			entDbFlIdMapping = dlFlightMappingBean.getFlightIdMappingX(
					entDbLoadPaxConn.getPaxConxFlno(),
					entDbLoadPaxConn.getConxFltDate(), arrDepFlag);
			LOG.info(
					"1.2.1 search flight from flightIdMapping table for paxConn Msg, takes {} ms",
					(System.currentTimeMillis() - startTime));
		}

		if (entDbFlIdMapping != null) {
			idFlightStr = entDbFlIdMapping.getIdFlight();
			entDbLoadPaxConn.setInterfaceConxFltid(entDbFlIdMapping
					.getIntFltId());
			// if (entDbFlIdMapping.getArrDepFlag() != null){
			// adid = entDbFlIdMapping.getArrDepFlag();
			// }

		} else {
			// commented on 2013-01-20
//			LOG.error(
//					"Flight not found for paxConn intId = {}, set id_conx_flight = 0 and id_loadPax = null",
//					entDbLoadPaxConn.getIntId());
//			long startTime = System.currentTimeMillis();
//
//			sendErroNotification(EnumExceptionCodes.ENOFL,irmtabRef);
//			LOG.debug("1.2.2 flight not found and update IRMTAB, takes {} ms",
//					(System.currentTimeMillis() - startTime));

		}

		int idFlight = 0;
		if (idFlightStr != null) {
			idFlight = idFlightStr.intValue();
		}

//		EntDbLoadPax entDbLoadPax = null;

		entDbLoadPaxConn.setIdConxFlight(idFlight);

		long startTime = System.currentTimeMillis();
		EntDbLoadPaxConn entDbLoadPaxConnFind = _paxConBean.findByintFltRefX(
				entDbLoadPaxConn.getPaxConnPK().getInterfaceFltid(),entDbLoadPaxConn.getPaxConnPK().getIntRefNumber());
		LOG.info(
				"1.2.3 search records from paxConn table for paxConn Msg,  takes {} ms",
				(System.currentTimeMillis() - startTime));
		
		EntDbLoadPaxConn oldEntDbLoadPaxConn = (EntDbLoadPaxConn)SerializationUtils.clone(entDbLoadPaxConnFind);

		if (entDbLoadPaxConnFind != null) {

			startTime = System.currentTimeMillis();
			Class<EntDbLoadPaxConn> entDbLoadPaxConnClass = EntDbLoadPaxConn.class;
			Field[] entDbLoadPaxConnClassFields = entDbLoadPaxConnClass
					.getDeclaredFields();
			for (Field f : entDbLoadPaxConnClassFields) {
				if (!"id".equalsIgnoreCase(f.getName())
						&& !"serialVersionUID".equalsIgnoreCase(f.getName())
						&& !"intId".equalsIgnoreCase(f.getName())) {
					setEntityValue(entDbLoadPaxConnFind, f.getName(),
							getEntityValue(entDbLoadPaxConn, f.getName()));
				}
			}


			boolean isPaxNotFound = false;
			try {
				entDbLoadPaxConnFind.getPax().getUuid();
			} catch (EntityNotFoundException ef) {
				isPaxNotFound = true;
				LOG.warn(
						"pax can not be found for paxConn info, intFlid {}, refNum {}",
						entDbLoadPaxConnFind.getPaxConnPK().getInterfaceFltid(),
						entDbLoadPaxConnFind.getPaxConnPK().getIntRefNumber());
				sendErroNotification(EnumExceptionCodes.WNOPX,irmtabRef);
		
			}

			if (isPaxNotFound) {
				entDbLoadPaxConnFind.setIdLoadPax("0");
				entDbLoadPaxConnFind.setIdFlight(0);
				entDbLoadPaxConnFind
						.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
				entDbLoadPaxConnFind.set_updatedDate(HpUfisCalendar.getCurrentUTCTime());

				LOG.warn("pax not found for pax connct info and to update IRMTAB");
				startTime = System.currentTimeMillis();

				LOG.debug("1.2.4 pax not found and update IRMTAB, takes {} ms",
						System.currentTimeMillis() - startTime);
			} else {
				LOG.debug("Pax found for pax connect info b");

				entDbLoadPaxConnFind.setIdLoadPax(entDbLoadPaxConnFind.getPax()
						.getUuid());
				entDbLoadPaxConnFind.setIdFlight(entDbLoadPaxConnFind.getPax()
						.getIdFlight());
				entDbLoadPaxConnFind.getPaxConnPK().setInterfaceFltid(entDbLoadPaxConnFind
						.getPax().getPKId().getIntflid());
				entDbLoadPaxConnFind.getPax().setIdLoadPaxConnect(
						entDbLoadPaxConnFind.getUuid());
				entDbLoadPaxConnFind
						.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
				entDbLoadPaxConnFind.set_updatedDate(HpUfisCalendar.getCurrentUTCTime());

				
				// send notification 
				ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
						String.valueOf(entDbLoadPaxConnFind.getIdFlight()), oldEntDbLoadPaxConn, entDbLoadPaxConnFind);
				
//				entDbLoadPax = entDbLoadPaxConnFind.getPax();
			}

			LOG.info("1.2.5 update PaxConn Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));

		} else {
			startTime = System.currentTimeMillis();

			_paxConBean.Persist(entDbLoadPaxConn);

			LOG.info("1.2.4 insert PaxConn Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));

			
			// send notification
			ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
					String.valueOf(entDbLoadPaxConn.getIdFlight()), null, entDbLoadPaxConn);

			startTime = System.currentTimeMillis();
			LoadPaxPK pkSearch = new LoadPaxPK();
			pkSearch.setIntflid(entDbLoadPaxConn.getPaxConnPK().getInterfaceFltid());
			pkSearch.setIntrefnumber(entDbLoadPaxConn.getPaxConnPK().getIntRefNumber());
			EntDbLoadPax paxForPaxConn = _paxbean.findByPkIdX(pkSearch);
			LOG.info("1.2.4+  search pax for paxConn, takes {} ms",
					(System.currentTimeMillis() - startTime));

			if (paxForPaxConn == null) {
				entDbLoadPaxConn.setIdLoadPax("0");
				entDbLoadPaxConn.setIdFlight(0);

				LOG.warn("pax not found for pax connct info and to update IRMTAB");
				startTime = System.currentTimeMillis();
				sendErroNotification(EnumExceptionCodes.WNOPX,irmtabRef);
					
				LOG.debug("1.2.4 pax not found and update IRMTAB, takes {} ms",
						(System.currentTimeMillis() - startTime));
			} else {
				LOG.debug("Pax found for pax connect info a");

				entDbLoadPaxConn.setIdLoadPax(paxForPaxConn.getUuid());
				entDbLoadPaxConn.setIdFlight(paxForPaxConn.getIdFlight());
				paxForPaxConn.setIdLoadPaxConnect(entDbLoadPaxConn.getUuid());
				entDbLoadPaxConn.getPaxConnPK().setInterfaceFltid(paxForPaxConn.getPKId()
						.getIntflid());
				
				// added 2013-12-04
				ArrayList<EntDbLoadBag> loadBagList = (ArrayList<EntDbLoadBag>) dlLoadBagUpdateLocal.getBagMoveDetailsByPaxX(entDbLoadPaxConn.getUuid());
				Iterator <EntDbLoadBag> loadBagListIt = loadBagList.iterator();
				while (loadBagListIt.hasNext()){
					loadBagListIt.next().setBagType(HpEKConstants.BAGTYPE_T);
				}
				LOG.debug("update batType to 'T' for pax id {}", entDbLoadPaxConn.getUuid());
			}

			LOG.info("1.2.5 insert PaxConn Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));
		}

		// To update LOAD_BAG table
		// startTime = System.currentTimeMillis();
		// // EntDbLoadPax entDbLoadPax = entDbLoadPaxConn.getPax();
		// LoadPaxPK pKId = new LoadPaxPK();
		// pKId.setIntflid(entDbLoadPaxConn.getInterfaceFltid());
		// pKId.setIntrefnumber(entDbLoadPaxConn.getIntRefNumber());
		// pKId.setIntSystem(entDbLoadPaxConn.getIntSystem());
		// EntDbLoadPax entDbLoadPax = _paxbean.findByPkIdX(pKId);
		// LOG.info("takes {} ms to search pax records for paxConn Msg",
		// (System.currentTimeMillis() - startTime));

		// 2013-11-13 updated by JGO - According to what Shilong said, this part
		// no longer needed
		/*
		 * if (entDbLoadPax != null) { // update for LOAD_BAG String[]
		 * bagTagList = null; // String refNum =
		 * entDbLoadPax.getpKId().getIntrefnumber(); String refNum =
		 * entDbLoadPax.getPKId().getIntrefnumber(); String bagTagInfo =
		 * entDbLoadPax.getBagTagInfo(); if (bagTagInfo != null &&
		 * !"".equals(bagTagInfo.trim())) { bagTagList = bagTagInfo.split("/");
		 * }
		 * 
		 * if (bagTagList != null) { for (int i = 0; i < bagTagList.length; i++)
		 * { if (bagTagList[i] != null && !"".equals(bagTagList[i].trim())) {
		 * startTime = System.currentTimeMillis(); List<EntDbLoadBag>
		 * loadBagTagList = dlLoadBagUpdateLocal .getBagMoveDetailsByBagtagPaxX(
		 * entDbLoadPax.getIdFlight(), bagTagList[i], refNum); LOG.info(
		 * "1.2.6 search records from loadBag table for PaxConn Msg, takes {}",
		 * (System.currentTimeMillis() - startTime)); startTime =
		 * System.currentTimeMillis(); Iterator<EntDbLoadBag> loadBagTagListIt =
		 * loadBagTagList .iterator(); while (loadBagTagListIt.hasNext()) {
		 * EntDbLoadBag entDbLoadBag = loadBagTagListIt.next(); if
		 * ("A".equalsIgnoreCase(arrDepFlag.toString())) {
		 * entDbLoadBag.setIdArrFlight(idFlight); } else { // id_dep_flight
		 * entDbLoadBag.setIdFlight(idFlight); } entDbLoadBag.setRecStatus(" ");
		 * // entDbLoadBag.setUpdatedDate(new Date()); //
		 * dlLoadBagUpdateLocal.updateLoadedBag(entDbLoadBag);
		 * entDbLoadBag.setUpdatedUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
		 * entDbLoadBag.setDataSource(HpEKConstants.MACS_PAX_DATA_SOURCE);
		 * entDbLoadBag.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
		 * 
		 * boolean isLastRecord = false; if (!loadBagTagListIt.hasNext()) {
		 * isLastRecord = true; }
		 * 
		 * addToNotifyJsonList( Long.toString(entDbLoadBag.getIdFlight()),
		 * Long.toString(entDbLoadBag.getIdFlight()), entDbLoadBag.getId(),
		 * "LOAD_BAG", UfisASCommands.URT.toString(), isLastRecord); } LOG.info(
		 * "1.2.7 update records from loadBag table for PaxConn Msg, takes {}",
		 * (System.currentTimeMillis() - startTime)); } } } else { LOG.debug(
		 * "BagTagList is null for the pax base on LOAD_PAX_CONN msg for  id_load_pax {} "
		 * , entDbLoadPax.getUuid()); } } else { LOG.info(
		 * "pax not found for pax conn msg, intFlId: {}, intRefNum: {} ",
		 * entDbLoadPaxConn.getInterfaceFltid(),
		 * entDbLoadPaxConn.getIntRefNumber()); }
		 */
	}

	public void handleMACSPAX(
			EntDbServiceRequest entDbPaxServiceRequest, long irmtabRef) {
		
		logLevel = entStartupInitSingleton.getIrmLogLev();
		
		msgLogged = Boolean.FALSE;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		

		EntDbLoadPax entDbLoadPax = null;

		if (entDbPaxServiceRequest.getServiceRequestPK().getIntFlId() != null
				&& !"".equals(entDbPaxServiceRequest.getServiceRequestPK().getIntFlId().toString()
						.trim())) {
			long startTime = System.currentTimeMillis();
			LoadPaxPK loadPaxPK = new LoadPaxPK();
			loadPaxPK.setIntflid(entDbPaxServiceRequest.getServiceRequestPK().getIntFlId());
			loadPaxPK.setIntrefnumber(entDbPaxServiceRequest.getServiceRequestPK().getIntRefNumber());
			// loadPaxPK.setIntSystem(HpEKConstants.MACS_PAX_DATA_SOURCE);
			entDbLoadPax = _paxbean.findByPkIdX(loadPaxPK);
			LOG.info(
					"1.2.1 search from LoadPax table for Fct Msg, takes {} ms",
					(System.currentTimeMillis() - startTime));
		}

		if (entDbLoadPax != null) {
			entDbPaxServiceRequest.setIdFlight(entDbLoadPax.getIdFlight());
			entDbPaxServiceRequest.setIdLoadPax(entDbLoadPax.getUuid());

			LOG.debug("Pax found for service info");
		} else {
			entDbPaxServiceRequest.setIdFlight(0);
			entDbPaxServiceRequest.setIdLoadPax("0");

			long startTime = System.currentTimeMillis();

			sendErroNotification(EnumExceptionCodes.WNOPX,irmtabRef);
			LOG.warn(
					"1.2.2 pax not found for service request msg update IRMTAB, takes {}",
					System.currentTimeMillis() - startTime);
		}

		long startTime = System.currentTimeMillis();
		EntDbServiceRequest entDbPaxServiceRequestFind = _dlPaxServiceRequestBean.findByPkIdX(entDbPaxServiceRequest.getServiceRequestPK().getIntFlId(),entDbPaxServiceRequest.getServiceRequestPK().getIntRefNumber());
		LOG.info(
				"1.2.3 search records from ServiceRequest table for Fct Msg, takes {}",
				(System.currentTimeMillis() - startTime));

		if (entDbPaxServiceRequestFind != null) {
			startTime = System.currentTimeMillis();
			
			EntDbServiceRequest oldEntDbServiceRequest = (EntDbServiceRequest)SerializationUtils.clone(entDbPaxServiceRequestFind);
			

			// update serviceRequest
			Class<EntDbServiceRequest> entDbServiceRequestClass = EntDbServiceRequest.class;
			Field[] entDbServiceRequestFields = entDbServiceRequestClass
					.getDeclaredFields();
			for (Field f : entDbServiceRequestFields) {
				if (!"id".equalsIgnoreCase(f.getName())
						&& !"serialVersionUID".equalsIgnoreCase(f.getName())
						&& !"intId".equalsIgnoreCase(f.getName())) {
					setEntityValue(entDbPaxServiceRequestFind, f.getName(),
							getEntityValue(entDbPaxServiceRequest, f.getName()));
				}
			}
			entDbPaxServiceRequestFind.set_recStatus(" ");
			entDbPaxServiceRequestFind.set_updatedDate(HpUfisCalendar
					.getCurrentUTCTime());
			entDbPaxServiceRequestFind.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);

			// send notification 
			ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
					String.valueOf(entDbPaxServiceRequestFind.getIdFlight()), oldEntDbServiceRequest, entDbPaxServiceRequestFind);

			LOG.info("update Fct Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));

		} else {
			LOG.warn("fct not found for fct msg and to update IRMTAB");
			startTime = System.currentTimeMillis();
			_dlPaxServiceRequestBean.Persist(entDbPaxServiceRequest);
			LOG.info("insert Fct Msg to database, takes {} ms",
					(System.currentTimeMillis() - startTime));

			// send notification
			ufisTopicProducer.sendNotification(true, UfisASCommands.IRT,
					String.valueOf(entDbPaxServiceRequest.getIdFlight()), null, entDbPaxServiceRequest);
		}

	}

	public void handleMACSPAX(List<EntDbLoadPaxSummary> entDbLoadPaxSummaryList) {

		Iterator<EntDbLoadPaxSummary> it = entDbLoadPaxSummaryList.iterator();
		while (it.hasNext()) {
			EntDbLoadPaxSummary entDbLoadPaxSummary = it.next();

			EntDbLoadPaxSummary EntDbLoadSummaryFind = _loadPaxSummaryBean
					.findByPk(entDbLoadPaxSummary.getId());

			if (EntDbLoadSummaryFind != null) {
				_loadPaxSummaryBean.update(entDbLoadPaxSummary);
			} else {
				_loadPaxSummaryBean.persist(entDbLoadPaxSummary);
			}

		}

	}

	// handle flt_id_mapping notification
	public void handleFltIdMappingNotification(EntUfisMsgDTO ufisMsgDTO) {
		try {

			int idFlight = 0;
			String intFlId = null;
			EntDbFlightIdMapping entDbFlightIdMapping = null;
			
			try{

			if ("INTERFACE_FLTID".equalsIgnoreCase(ufisMsgDTO.getBody()
					.getActs().get(0).getFld().get(0))) {
				intFlId = (String) ufisMsgDTO.getBody().getActs().get(0)
						.getData().get(0);
			} else {
				LOG.warn("fltIdMapping msg from Macs-Flt, field name is not 'INTERFACE_FLTID'. Msg will not be processed");
			}
			
			}catch(Exception e){
				LOG.error("erro when handleFltIdMappingNotification, {}",e);
				return;
			}

			if (intFlId != null) {
				entDbFlightIdMapping = dlFlightMappingBean
						.getFlightIdMappingX(intFlId);
			} else {
				LOG.warn("fltIdMapping msg from Macs-Flt, intFlId is null. Msg will not be processed");
				return;
			}
			// List<String> idFlightList = ufisMsgDTO.getHead().getIdFlight();

			if (entDbFlightIdMapping != null
					&& entDbFlightIdMapping.getIdFlight() != null) {
				idFlight = entDbFlightIdMapping.getIdFlight().intValue();
			} else {
				LOG.warn("fltIdMapping msg from Macs-Flt, entDbFlightIdMapping or entDbFlightIdMapping.getIdFlight() is null. Msg will not be processed");
				return;
			}

			// main handle logic
			if (idFlight != 0 && intFlId != null) {
				// update load_pax
				List<EntDbLoadPax> loadPaxList = _paxbean
						.findByIntFltId(intFlId);
				Iterator<EntDbLoadPax> loadPaxListIt = loadPaxList.iterator();
				while (loadPaxListIt.hasNext()) {
					EntDbLoadPax entDbLoadPax = loadPaxListIt.next();

					if (idFlight != entDbLoadPax.getIdFlight()) {

						EntDbLoadPax oldEntDbLoadPax = (EntDbLoadPax)SerializationUtils.clone(entDbLoadPax);

						entDbLoadPax.setIdFlight(idFlight);
						entDbLoadPax
								.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
						entDbLoadPax.set_updatedDate(HpUfisCalendar
								.getCurrentUTCTime());
						_paxbean.Update(entDbLoadPax);
						// send notification 
						ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
								String.valueOf(entDbLoadPax.getIdFlight()), oldEntDbLoadPax, entDbLoadPax);
						
						// update load_pax_connect
						EntDbLoadPaxConn entDbLoadPaxConn = _paxConBean
								.findByIdLoadPax(entDbLoadPax.getUuid());
						if (entDbLoadPaxConn != null
								&& idFlight != entDbLoadPaxConn.getIdFlight()) {
			
							EntDbLoadPaxConn oldEntDbLoadPaxConn = (EntDbLoadPaxConn)SerializationUtils.clone(entDbLoadPaxConn);

							entDbLoadPaxConn.setIdFlight(idFlight);
							entDbLoadPaxConn
									.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
							entDbLoadPaxConn.set_updatedDate(HpUfisCalendar
									.getCurrentUTCTime());
//							_paxConBean.Update(entDbLoadPaxConn);
							
							// send notification 
							ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
									String.valueOf(entDbLoadPaxConn.getIdFlight()), oldEntDbLoadPaxConn, entDbLoadPaxConn);
						}

						// update load_bag
						List<EntDbLoadBag> loadBagList = dlLoadBagUpdateLocal
								.getBagMoveDetailsByIdLoadPax(entDbLoadPax
										.getUuid());
						Iterator<EntDbLoadBag> loadBagListIt = loadBagList
								.iterator();
						while (loadBagListIt.hasNext()) {
							EntDbLoadBag entDbLoadBag = loadBagListIt.next();
							if (idFlight != entDbLoadBag.getIdFlight()) {

								EntDbLoadBag oldEntDbLoadBag = (EntDbLoadBag)SerializationUtils.clone(entDbLoadBag);

								entDbLoadBag.setIdFlight(idFlight);
								entDbLoadBag
										.setUpdatedUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
								entDbLoadBag.setUpdatedDate(HpUfisCalendar
										.getCurrentUTCTime());			
								
								// send notification 
								ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
										String.valueOf(entDbLoadBag.getIdFlight()), oldEntDbLoadBag, entDbLoadBag);
							}

						}

						// update serviceReques
						List<EntDbServiceRequest> serviceRequestList = _dlPaxServiceRequestBean
								.findByidLoadPax(entDbLoadPax.getUuid());
						Iterator<EntDbServiceRequest> serviceRequestListIt = serviceRequestList
								.iterator();
						while (serviceRequestListIt.hasNext()) {
							EntDbServiceRequest entDbServiceRequest = serviceRequestListIt
									.next();
							if (entDbServiceRequest.getIdFlight() != idFlight) {

								EntDbServiceRequest oldEntDbServiceRequest = (EntDbServiceRequest)SerializationUtils.clone(entDbServiceRequest);

								entDbServiceRequest.setIdFlight(idFlight);
								entDbServiceRequest
										.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
								entDbServiceRequest
										.set_updatedDate(HpUfisCalendar
												.getCurrentUTCTime());
								
								// send notification 
								ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
										String.valueOf(entDbServiceRequest.getIdFlight()), oldEntDbServiceRequest, entDbServiceRequest);
							}
						}

					}

				}

				// update id_conx_flight for load_pax_conn table
				if (entDbFlightIdMapping.getFltNumber() != null
						&& entDbFlightIdMapping.getFltDateLocal() != null) {
					List<EntDbLoadPaxConn> entDbLoadPaxConnList = _paxConBean
							.findByFlnoFltDate(
									entDbFlightIdMapping.getFltNumber(),
									entDbFlightIdMapping.getFltDateLocal());
					Iterator<EntDbLoadPaxConn> entDbLoadPaxConnListIt = entDbLoadPaxConnList
							.iterator();
					while (entDbLoadPaxConnListIt.hasNext()) {
						EntDbLoadPaxConn entDbLoadPaxConn = entDbLoadPaxConnListIt
								.next();
						if (entDbFlightIdMapping.getIdFlight() != null
								&& entDbLoadPaxConn.getIdConxFlight() != entDbFlightIdMapping
										.getIdFlight().intValue()) {

							EntDbLoadPaxConn oldEntDbLoadPaxConn = (EntDbLoadPaxConn)SerializationUtils.clone(entDbLoadPaxConn);

							entDbLoadPaxConn
									.setIdConxFlight(entDbFlightIdMapping
											.getIdFlight().intValue());
							entDbLoadPaxConn
									.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE);
							entDbLoadPaxConn.set_updatedDate(HpUfisCalendar
									.getCurrentUTCTime());
//							_paxConBean.Update(entDbLoadPaxConn);
							
							// send notification 
							ufisTopicProducer.sendNotification(true, UfisASCommands.URT,
									String.valueOf(entDbLoadPaxConn.getIdFlight()), oldEntDbLoadPaxConn, entDbLoadPaxConn);
						}
					}

				} else {
					LOG.warn("fltIdMapping msg from Macs-Flt, entDbFlightIdMapping.getFltNumber() or entDbFlightIdMapping.getFltDateLocal() is null. idConxFlt will not be updated");
				}


			} else {
				LOG.warn("fltIdMapping msg from Macs-Flt, idFlight or interfaceId is null. Msg will not be processed");
			}

		} catch (Exception e) {
			LOG.error(
					"Erro when handle the fltIdMapping notification from ActiveMq, {}",
					e);
		}

	}

	private Object setEntityValue(Object entity, String columnNm, Object value) {
		try {
			if (value == null) {
				return null;
			}

			String method = getMethodName(columnNm, "set");
			if (entity instanceof List) {
			} else {
				Class<? extends Object> clazz = entity.getClass();
				Field field = clazz.getDeclaredField(columnNm);
				Method getMethod = null;
				getMethod = clazz.getMethod(method, field.getType());
				getMethod.invoke(entity, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	private Object getEntityValue(Object entity, String columnNm) {
		Object o = null;
		try {
			String method = getMethodName(columnNm, "get");
			Class<? extends Object> clazz = entity.getClass();
			Method getMethod = (Method) clazz.getDeclaredMethod(method);
			if (getMethod != null) {
				o = getMethod.invoke(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return o;
	}

	private String getMethodName(String s, String prefix) {
		s = s.replaceAll("_", "");
		s = s.substring(0, 1).toUpperCase() + s.substring(1);
		s = prefix + s;
		return s;
	}

	
	
	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(expCode.toString());
		dataList.add(HpEKConstants.MACS_PAX_DATA_SOURCE);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, HpEKConstants.MACS_PAX_DATA_SOURCE);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from MACS_PAX to Data Loader :\n{}", msg);

	}
	
	
	private void sendErroNotification(EnumExceptionCodes eec, long irmtabRef){
		
		if (logLevel
				.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
						.name())) {
			if (!msgLogged) {
				irmtabRef = _irmtabFacade.storeRawMsg(rawMsg, HpEKConstants.MACS_PAX_DATA_SOURCE);
				msgLogged = Boolean.TRUE;
			}
			
		}
		
		if (irmtabRef > 0) {
			try {
				sendErrInfo(eec, irmtabRef);
			} catch (JsonGenerationException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (JsonMappingException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (IOException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			}
		}
		
	
		
	}

	public Message getRawMsg() {
		return rawMsg;
	}

	public void setRawMsg(Message rawMsg) {
		this.rawMsg = rawMsg;
	}
	
	

}
