package com.ufis_as.ufisapp.ek.intf.macs;

/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFlightIdMappingBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlFevBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.DlJnotabBean;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbFevtab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.macs.flightdetails.FlightDetails;


/**
 * @author JGO
 * @version 1.00: 2013-03-14 GFO - version
 * @version 1.01: 2013-04-08 JGO
 * @version 1.02: 2013-04-18 JGO
 * @version 1.03: 2013-04-23 JGO - Handle event
 * 								    Handle flight mapping
 * 									Add load summary update
 * @version 1.04: 2013-05-03 JGO - Change load info update method name
 * 									Change inert/update data to load summary table
 * @version 1.05: 2013-05-17 JGO - Fevtab interface related column name change according to ufis-2984
 * @version 1.06: 2013-06-03 JGO - Empty check for flight_number_exp
 * 									Transaction type change
 * @version 1.07: 2013-06-05 JGO - MACS-FLT use STCO to store flight status event
 * @version 1.08: 2013-06-14 JGO - Read HOPO from constant
 * 									Remove update user/date for record creation
 * @version 1.09: 2013-07-10 JGO - Store all flight status (previous only FO and PD)
 * 									Store load info to new table (Load_pax_summary)
 * @version 1.10: 2013-09-09 JGO - Flt_id_mapping table restructure
 * 									DlFltIdMapping and HandleBean update accordingly with table restructure
 * @version 1.11: 2013-09-13 JGO - Adding constant for class indicator
 * 									Adding constant for datasource
 * 									Change the class configuration record datasource to SYS
 * @version 1.12: 2013-10-01 JGO - Store stod for both arrival and departure flight (MACS is boardpoint based)
 * @version 1.13: 2013-10-03 JGO - Adding flight date local in Flt_id_mapping table
 * 									Use FLUT for flight query(previous use FLDA)
 * @version 1.14: 2013-10-09 JGO - Make the Macs Flt handler separated from BlHandleCedaMacs to BlHandleMacsFltBean
 * 									Adding Error/Warning info data store
 * 									Adding id mapped notification to MACS-PAX
 * @version 1.15: 2013-10-10 JGO - Adding consideration of boardpoint as via in flight query
 * 									Remove adid from search criteria
 * @version 1.16: 2013-10-20 JGO - Fix incorrect data filled in load_pax_summary(datasource, updated_user and rec_status)
 * 									Change the load_pax_summary query to use: MACS flight id and info type(CFG)
 * @version 1.17: 2013-10-22 JGO - Fix fevtab flight event duplicate issue(change search criteria: uaft+stnm to uaft+stco)
 * @version 1.18: 2013-10-23 JGO - Remove message content logging when error encounter(log raw message aldy at received time)
 * 								   Adding id_flight for notification message
 *  							   Provide error/warning data store details info(EXCEPTION_DATA)
 *  							   Change flight event search query to support via event(uaft+stco -> ifid+stco)
 *  							   Datasource and created_user/updated_user -> MSGIF(MACS-FLT)
 * @version 1.19: 2013-11-08 JGO - Remove the mandatory check for registration number(User Request)
 * @version 1.20: 2013-11-26 JGO - Remove static modifier from BaseDTO
 * @version 1.21: 2013-12-16 JGO - Sum the total_pax for CFG
 * @version 1.22: 2013-12-30 JGO - Update fevtab pkey related (urno type: bigDecimal to String)
 * 								   Store changed to lstu of fevtab
 * @version 1.23: 2014-01-14 JGO - Adding id_flight and id_conx_flight for configuration load of load_pax_summary table
 * @version 1.24: 2014-01-15 JGO - save economy class config load info to all_econ_pax field
 * 
 */
@Stateless
public class BlHandleMacsFltBean extends BaseDTO {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(BlHandleMacsFltBean.class);
    
    /**
     * CDI
     */
    @EJB
    IAfttabBeanLocal _afttabBeanLocal;
    @EJB
    private DlFlightIdMappingBean _flightIdMappingBean;
    @EJB
    private IDlLoadPaxSummaryBeanLocal _loadPaxSummaryBean;
    @EJB
    private DlFevBean _fevBean;
    @EJB
    private DlJnotabBean _jnoBean;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
	
    /**
     * Constant and Variables
     */
    //private static final String INTSYSTEM_MACS_FLT = "MACS";
    private static final String MSGIF = "MACS-FLT";
    //private static final String LOAD_INFOID = "PCF_T";
    private static final String LOAD_INFOID = "CFG";
    private static final String DATA_SOURCE = "SYS";
    
    private static final String CLASS_INDICATOR_FST = "F";
    private static final String CLASS_INDICATOR_BUS = "J";
    private static final String CLASS_INDICATOR_ECO = "Y";
    
    private static final String TAB_ID_MAPPING = "FLT_ID_MAPPING";
    private static final String NOTIFY_INTFLID = "INTERFACE_FLTID";
    
    private boolean isMapped = false;
    private EntUfisMsgHead msgHead;
    private List<EntUfisMsgACT> actions = null;
    private String interfaceFltId = null;
    
    public BlHandleMacsFltBean() {
    	if (msgHead == null) {
    		msgHead = new EntUfisMsgHead();
    		msgHead.setHop(HpEKConstants.HOPO);
    		msgHead.setOrig(MSGIF);
    		msgHead.setApp(MSGIF);
    		msgHead.setUsr(MSGIF);
        }
    	
    	tableRef = HpUfisAppConstants.CON_IRMTAB;
    	exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
    	dtfl = com.ufis_as.ufisapp.configuration.HpEKConstants.EK_MACSFLT_SOURCE;
    }
    
    private void handleEvent(FlightDetails flightData, EntDbAfttab aftFlight) {
    	try {
    		// For macs-flt, only keep Flight status: FO and PD
    		if (flightData != null && HpUfisUtils.isNotEmptyStr(flightData.getFlightStatus())) {
    			// 2013-07-10 updated by JGO - store all status
				// if ("FO".equalsIgnoreCase(flightData.getFlightStatus())
				// || "PD".equalsIgnoreCase(flightData.getFlightStatus())) {
				// FEVTAB
				// find mapped flight from afttab
		        if (aftFlight != null) {
		        	HpUfisCalendar nowUtc = new HpUfisCalendar(new Date());
		        	nowUtc.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		        	
		        	// TODO update event ???
		        	//EntDbFevtab fevRecord = _fevBean.getFevtab(aftFlight.getUrno(), flightData.getFlightStatus());
		        	EntDbFevtab fevRecord = _fevBean.getMacsFlightEvent(flightData.getMflId(), flightData.getFlightStatus());
		        	if (fevRecord == null) {
		        		fevRecord = new EntDbFevtab();
		        		// 2013-12-30 updated by JGO - Change urno to uuid
		        		// get id from jnotab
		        		//fevRecord.setURNO(new BigDecimal(HpUfisUtils.formatJavaFevUrno(_jnoBean.getNextValueByKey("FEV-URNO", 1))));
		        		// cdat
		            	fevRecord.setCDAT(nowUtc.getCedaString());
		            	// usec
		            	fevRecord.setUSEC(MSGIF);
		            	// intSystem and intFlid
		            	fevRecord.setINAM(MSGIF);
		            	fevRecord.setIFID(flightData.getMflId());
		            	
		            	// ------------------------------------------------------
		            	fevRecord.setUAFT(aftFlight.getUrno());
		            	fevRecord.setSTNM("FLIGHT_STATUS");
		            	fevRecord.setSTCO(flightData.getFlightStatus());
		            	fevRecord.setSTTM(nowUtc.getCedaString());
		            	fevRecord.setSTFL("");
		            	//fevRecord.setSTRM("FLIGHT_STATUS");
		            	//fevRecord.setUSEU(INTSYSTEM_MACS_FLT);
		            	
		            	// 2013-12-26 updated by JGO - Changed tag
		            	if (HpUfisUtils.isNotEmptyStr(flightData.getChanged())) {
		            		HpUfisCalendar cal = new HpUfisCalendar(flightData.getChanged(), 
		                			HpEKConstants.MACS_TIME_FORMAT);
		            		fevRecord.setLSTU(cal.getCedaString());
		            	}
		            	_fevBean.persist(fevRecord);
		        	} else {
		        		fevRecord.setUSEU(MSGIF);
		        		fevRecord.setSTTM(nowUtc.getCedaString());
		        		// 2013-12-26 updated by JGO - Changed tag
		            	if (HpUfisUtils.isNotEmptyStr(flightData.getChanged())) {
		            		HpUfisCalendar cal = new HpUfisCalendar(flightData.getChanged(), 
		                			HpEKConstants.MACS_TIME_FORMAT);
		            		fevRecord.setLSTU(cal.getCedaString());
		            	}
		        		//fevRecord.setLSTU(nowUtc.getCedaString());
		        		_fevBean.update(fevRecord);
		        	}
		        } else {
		        	// when afttab record is not found, use intsystem and intflid to insert event
		        	HpUfisCalendar nowUtc = new HpUfisCalendar(new Date());
		        	nowUtc.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		        	// TODO need to know whether same flight different event got different mflid
		        	EntDbFevtab fevRecord = _fevBean.getFevtab(MSGIF, flightData.getMflId());
		        	if (fevRecord == null) {
		        		fevRecord = new EntDbFevtab();
		        		// get id from jnotab
		        		//fevRecord.setURNO(new BigDecimal(HpUfisUtils.formatJavaFevUrno(_jnoBean.getNextValueByKey("FEV-URNO", 1))));
		        		// cdat
		            	fevRecord.setCDAT(nowUtc.getCedaString());
		            	// usec
		            	fevRecord.setUSEC(MSGIF);
		            	// intSystem and intFlid
		            	fevRecord.setINAM(MSGIF);
		            	fevRecord.setIFID(flightData.getMflId());
		            	
		            	// ------------------------------------------------------
		            	//fevRecord.setUAFT(result_entDbAfttab.getUrno());
		            	
		            	fevRecord.setSTNM("FLIGHT_STATUS");
		        		fevRecord.setSTCO(flightData.getFlightStatus());
		        		fevRecord.setSTTM(nowUtc.getCedaString());
		            	fevRecord.setSTFL("");
		            	//fevRecord.setSTRM("FLIGHT_STATUS");
		            	//fevRecord.setUSEU(INTSYSTEM_MACS_FLT);
		            	//fevRecord.setLSTU(nowUtc.getCedaString());
		            	_fevBean.persist(fevRecord);
		        	}
		        }
				// } else {
				// LOG.debug("Macs-Flt(event handle): flight status<{}> is not FO or PD",
				// flightData.getFlightStatus());
				// }
    		} else {
    			LOG.debug("Macs-Flt(event handle): flight status is null or empty");
    		}
    	} catch (Exception e) {
    		LOG.error("Macs-Flt(event handle): Cannot insert data to Fevtab");
    		LOG.error("MAcs-Flt Exception: {}", e.getMessage());
    	}
    }
    
    private void saveOrUpdateLoadInfo(FlightDetails flightData, EntDbAfttab aftFlight) {
    	try {
	    	// ================================================  
	    	// LoadSummary update (Class-configuration)
	    	// ================================================
    		// current time
    		HpUfisCalendar ufisCal = new HpUfisCalendar(new Date());
    		ufisCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
    		
	    	// 2013-07-10 update JGO move load info to new table<LOAD_PAX_SUMMARY>
			// LoadSummaryPK key = new LoadSummaryPK();
			// key.setIntFlId(new BigDecimal(flightData.getMflId()));
			// key.setInfoId(LOAD_INFOID);
			// key.setAirlineCode(" ");
			// key.setFlightNumber(" ");
			// key.setFlightNumberSuffix(" ");
			// key.setScheduledFlightDateTime(ufisCal.getTime());
	    	
	    	// EntDbLoadSummary load = _loadSummaryBean.getLoadSummaryByPkey(key);
			// if (load == null) {
			// load = new EntDbLoadSummary();
			// load.setlPk(key);
			// load.setIntSystem(INTSYSTEM_MACS_FLT);
			// load.set_idHopo(HpEKConstants.HOPO);
			// load.set_createdDate(ufisCal.getTime());
			// load.set_createdUser("MACS-FLT");
			// _loadSummaryBean.persist(load);
			// }
	    	
			EntDbLoadPaxSummary load = _loadPaxSummaryBean
					.getLoadPaxSummaryByMfid(new BigDecimal(flightData.getMflId()), DATA_SOURCE);
			if (load == null) {
				load = new EntDbLoadPaxSummary();
				load.setInterfaceFltId(new BigDecimal(flightData.getMflId()));
				load.setDataSource(DATA_SOURCE);
				load.setCreatedUser(MSGIF);
				load.setRecStatus(" ");
				_loadPaxSummaryBean.create(load);
			}
			// config load
			load.setInfoType(LOAD_INFOID);
			
			// 2014-01-14 updated by JGO - change to id_flight and id_conx_flight
    		// update afttab flight urno to load summary table
	    	/*if (aftFlight != null) {
	    		if ('A' == aftFlight.getAdid()) {
	    			//load.setArrFlId(String.valueOf(aftFlight.getUrno()));
	    			load.setIdArrFlight(aftFlight.getUrno());
	    		} else if ('D' == aftFlight.getAdid()) {
	    			//load.setDepFlId(String.valueOf(aftFlight.getUrno()));
	    			load.setIdDepFlight(aftFlight.getUrno());
	    		}
	    	}*/
	    	load.setIdFlight(aftFlight.getUrno());
	    	load.setIdConxFlight(BigDecimal.ZERO);
	    	
	    	// update class configuration
	    	String classConfig = flightData.getClassConfiguration();
	    	if (HpUfisUtils.isNotEmptyStr(classConfig)) {
	    		int indexF = classConfig.indexOf(CLASS_INDICATOR_FST);
	    		int indexJ = classConfig.indexOf(CLASS_INDICATOR_BUS);
	    		int indexY = classConfig.indexOf(CLASS_INDICATOR_ECO);
	    		String figure = "";
	    		// first
	    		if (indexF != -1 && indexJ != -1 && (indexJ - indexF) > 0) {
	    			figure = classConfig.substring(indexF + 1, indexJ);
	    			//load.setFirstClass(Integer.parseInt(figure));
	    			load.setFirstPax(new BigDecimal(figure));
	    		}
	    		// business
	    		if (indexJ != -1 && indexY != -1 && (indexY - indexJ) > 0) {
	    			figure = classConfig.substring(indexJ + 1, indexY);
	    			//load.setBusinessClass(Integer.parseInt(figure));
	    			load.setBusinessPax(new BigDecimal(figure));
	    		}
	    		// economy
	    		if (indexY != -1) {
	    			figure = classConfig.substring(indexY + 1);
	    			//load.setEconomyClass(Integer.parseInt(figure));
	    			load.setEconPax(new BigDecimal(figure));
	    			// 2014-01-15 updated by JGO - save the value to all_econ_pax field as well
	    			load.setAllEconPax(load.getEconPax());
	    		}
	    		
	    		// 2013-12-16 updated by JGO - Sum the total_pax for CFG
		    	load.setTotalPax(load.getFirstPax().add(load.getBusinessPax()).add(load.getEconPax()));
	    	}
	    	load.setUpdatedDate(ufisCal.getTime());
	    	//load.setDataSource(INTSYSTEM_MACS_FLT);
	    	load.setDataSource(DATA_SOURCE);
	    	load.setUpdatedUser(MSGIF);
	    	load.setRecStatus(" ");
	    	_loadPaxSummaryBean.edit(load);
	    	LOG.debug("Class Configuration Info has been created/updated for MFL_ID: {}", 
	    			flightData.getMflId());
    	} catch (Exception e) {
    		LOG.error("Macs-Flt(load info): Cannot insert/update load summary class configuration");
    		LOG.error("MAcs-Flt Exception: {}", e);
	    }
    }
    
    private void handleMapping(FlightDetails flightData, EntDbAfttab queryResult) {
    	try {
	    	// ================================================
	    	// MACS-FLT and Ceda Afttab mapping
	    	// ================================================    	
	    	// flight id mapping
	    	EntDbFlightIdMapping flightIdMapping = null;
	    	
	    	// MACS FLT unique identifier (mandatory field and unique)
	    	String intFlid = flightData.getMflId();
	    	if (HpUfisUtils.isNotEmptyStr(intFlid)) {
	    		// find mapping record by MFL_ID
	    		flightIdMapping = _flightIdMappingBean.getFlightIdMapping(intFlid);
	    		
	    		// find mapping record by flight info 
	    		// if aldy exist then drop this message, will not map twice
	    		// if no mapped record for the flight, then will looking for afttab to create mapping
	    		if (flightIdMapping == null) {
	    			//EntDbAfttab criteriaEntity = buildAftCriteria(flightData);
	    			flightIdMapping = _flightIdMappingBean.getMappingByFlightInfo(queryResult);
	    			if (flightIdMapping == null) {
	    				// find flight from afttab
	    				// EntDbAfttab entDbAfttab = _afttabBeanLocal.findFlight(criteriaEntity);
	    				if (queryResult != null) {
	    					flightIdMapping = new EntDbFlightIdMapping();
	    					// interface info
	    	            	flightIdMapping.setIntFltId(intFlid);
	    	            	flightIdMapping.setCreatedUser(MSGIF);

	    	            	// flight info
	    	            	flightIdMapping.setIdFlight(queryResult.getUrno());
	    	            	flightIdMapping.setArrDepFlag(queryResult.getAdid());
	    	            	flightIdMapping.setFltNumber(queryResult.getFlno().trim());
	    	            	
	    	            	HpUfisCalendar scheduleDateTime = null;
	    	            	// 2013-09-13 updated by JGO - all message base on boardpoint
//	    	            	if ('D' == queryResult.getAdid()) {
    	            		scheduleDateTime = new HpUfisCalendar(queryResult.getStod());
//	    	            	} else {
//	    	            		scheduleDateTime = new HpUfisCalendar(queryResult.getStoa());
//	    	            	}
	    	            	flightIdMapping.setFltDate(scheduleDateTime.getTime());
	    	            	
	    	        		// schedule time in local
	    	        		if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDate())) {
	    	                	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDate(), 
	    	                			HpEKConstants.MACS_TIME_FORMAT);
	    	                	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTime())){
	    	                        int hour=Integer.parseInt(flightData.getOperationTime().substring(0, 2));
	    	                        int min=Integer.parseInt(flightData.getOperationTime().substring(2, 4));
	    	                        scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
	    	                        scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
	    	                    }
	    	                	flightIdMapping.setFltDateLocal(scheduleDateTime.getTime());
	    	                }
	    	            	flightIdMapping.setDataSource(MSGIF);
	    	            	flightIdMapping.setRecStatus(' ');
	    		            
	    		            /*HpUfisCalendar scheduleDateTime = null;
	    		            if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDateGmt())) {
	    		            	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDateGmt(), HpEKConstants.MACS_TIME_FORMAT);
	    		            	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTimeGmt())){
	    		                    int hour=Integer.parseInt(flightData.getOperationTimeGmt().substring(0, 2));
	    		                    int min=Integer.parseInt(flightData.getOperationTimeGmt().substring(2, 4));
	    		                    scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
	    		                    scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
	    		                }
	    		            	flightIdMapping.setScheduledFlightDatetime(scheduleDateTime.getTime());
	    		            } else {
	    		            	LOG.debug("MACS-FLT flight operation date is null");
	    		            }*/
	    	            	_flightIdMappingBean.persist(flightIdMapping);
	    	            	interfaceFltId = flightIdMapping.getIntFltId();
	    	            	isMapped = true;
	    	            	LOG.debug("Macs-Flt<id: {}> and Afttab<urno: {}> mapping has been created.", 
	    	            			flightIdMapping.getIntFltId(), flightIdMapping.getIdFlight());
	    				}
	    			} else {
	    				LOG.debug("Macs-Flt(Flight mapping): Mapping table aldy have mapped value for flno={}, flda={}",
	    						queryResult.getAlc2() + queryResult.getFltn(), queryResult.getFlda());
	    			}
	    		}
	    	} else {
	    		LOG.debug("MACS-FLT MFL_ID is null or empty");
	    	}
    	} catch (Exception e) {
    		LOG.error("Macs-Flt(Flight mapping): Cannot insert/update data to FlightIdMapping table");
    		LOG.error("MAcs-Flt Exception: {}", e.getMessage());
    	}
    }
    
    private EntDbAfttab buildAftCriteria(FlightDetails flightData) {
    	// build afttab flight search criteria 
        EntDbAfttab entDbAfttab = new EntDbAfttab();
        HpUfisCalendar scheduleDateTime = null;
        
    	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDateGmt())) {
        	scheduleDateTime = new HpUfisCalendar(flightData.getOperationDateGmt(), 
        			HpEKConstants.MACS_TIME_FORMAT);
        	// 2013-10-03 updated by JGO - adding FLUT for MACS-FLT
        	entDbAfttab.setFlut(scheduleDateTime.getCedaDateString());
        	// combine with the time part
        	if (HpUfisUtils.isNotEmptyStr(flightData.getOperationTimeGmt())){
                int hour=Integer.parseInt(flightData.getOperationTimeGmt().substring(0, 2));
                int min=Integer.parseInt(flightData.getOperationTimeGmt().substring(2, 4));
                scheduleDateTime.DateAdd(hour, EnumTimeInterval.Hours);
                scheduleDateTime.DateAdd(min, EnumTimeInterval.Minutes);
            }
        }
    	
    	// flight info
        entDbAfttab.setFltn(HpUfisUtils.formatCedaFltn(flightData.getFlightNumber()));
		if (HpUfisUtils.isNotEmptyStr(flightData.getFlightNumberExp())) {
		    entDbAfttab.setFlns(flightData.getFlightNumberExp().charAt(0));
		}
		entDbAfttab.setOrg3(flightData.getBoardPoint());
		// 2013-10-10 udpated by JGO - Add vial into search critiera(boardpoint may be is via)
		entDbAfttab.setVial(flightData.getBoardPoint());
		
		// 2013-10-10 updated by JGO - Remove adid from search criteria
		/*if (HpEKConstants.HOPO.equals(flightData.getBoardPoint())) {
			entDbAfttab.setAdid('D');
		} else {
			entDbAfttab.setAdid('A');
		}*/
		
		// airline codes
		if (HpUfisUtils.isNotEmptyStr(flightData.getAirlineDesignatorExp())) {
		    entDbAfttab.setAlc3(flightData.getAirlineDesignator() + 
		    		flightData.getAirlineDesignatorExp());
		} else {
			entDbAfttab.setAlc2(flightData.getAirlineDesignator());
		}
		
		// schedule time
		if (scheduleDateTime != null) {
			if (HpEKConstants.HOPO.equals(flightData.getBoardPoint())) {
				entDbAfttab.setStod(scheduleDateTime.getCedaString());
			} else {
				entDbAfttab.setStoa(scheduleDateTime.getCedaString());
			}
		}
		
		// 2013-10-03 updated by JGO - use flut instead of flda for flight query
		// execution date in local
		/*if (HpUfisUtils.isNotEmptyStr(flightData.getOperationDate())) {
			scheduleDateTime = new HpUfisCalendar(flightData.getOperationDate(), 
					HpEKConstants.MACS_TIME_FORMAT);
			entDbAfttab.setFlda(scheduleDateTime.getCedaDateString());
		}*/
		return entDbAfttab;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void handleMACSFLT(FlightDetails flightData, String message, long irmtabRef) {
    	boolean isValid = true;
    	isMapped = false;
    	interfaceFltId = null;
    	data.clear();
    	msgHead.getIdFlight().clear();
    	irmtabUrno = irmtabRef;
    	
    	// mandatory item check
    	if (HpUfisUtils.isNullOrEmptyStr(flightData.getMflId())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "MflId");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "MflId is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFltSnapshotDay())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FltSnapshotDay");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "FltSnapshotDay is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getAirlineDesignator())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "AirlineDesignator");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "AirlineDesignator is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFlightNumber())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FlightNumber");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "FlightNumber is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationDate())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationDate");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "OperationDate is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationTime())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationTime");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "OperationTime is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationDateGmt())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationDateGmt");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "OperationDateGmt is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getOperationTimeGmt())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "OperationTimeGmt");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "OperationTimeGmt is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getBoardPoint())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "BoardPoint");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "Boardpoint is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getClassConfiguration())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "ClassConfiguration");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "ClassConfiguration is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
//    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getRegistrationNumber())) {
//    		isValid = false;
//    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "RegistrationNumber");
//    		addExptInfo(EnumExceptionCodes.EMAND.name(), "RegistrationNumber is null or empty");
//    		//LOG.debug("Message dropped: \n{}", message);
//    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getFlightStatus())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "FlightStatus");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "FlightStatus is null or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	if (isValid && HpUfisUtils.isNullOrEmptyStr(flightData.getStatus())) {
    		isValid = false;
    		LOG.debug("Mandatory fields: {} Cannot be null or empty. ", "Status");
    		addExptInfo(EnumExceptionCodes.EMAND.name(), "Status or empty");
    		//LOG.debug("Message dropped: \n{}", message);
    	}
    	
    	long start = System.currentTimeMillis();
    	if (isValid) {
    		// according to flight data to search from afttab
        	EntDbAfttab queryCriteria = buildAftCriteria(flightData);
            EntDbAfttab result_entDbAfttab = _afttabBeanLocal.findFlight(queryCriteria);
            LOG.debug("Flight Search used: {}ms", (System.currentTimeMillis() - start));
            if (result_entDbAfttab == null) {
            	LOG.debug("Flight Record not found in afttab");
        		//LOG.debug("Message dropped: \n{}", message);
            	StringBuilder sb = new StringBuilder();
            	sb.append("Flight Not Found - ");
            	sb.append(" FLUT: ").append(queryCriteria.getFlut());
            	sb.append(" FLTN: ").append(queryCriteria.getFltn());
            	if (queryCriteria.getAlc3() != null) {
            		sb.append(" ALC3: ").append(queryCriteria.getAlc3());
            	} else {
            		sb.append(" ALC2: ").append(queryCriteria.getAlc2());
            	}
            	sb.append(" Boardpoint: ").append(queryCriteria.getOrg3());
        		addExptInfo(EnumExceptionCodes.ENOFL.name(), sb.toString());
            } else {
	        	// handle the macs-flt and afttab mapping
            	start = System.currentTimeMillis();
	        	handleMapping(flightData, result_entDbAfttab);
	        	LOG.debug("Handle mapping used: {}ms", (System.currentTimeMillis() - start));
	        	
	        	// insert/update class-configuration
	        	start = System.currentTimeMillis();
	        	saveOrUpdateLoadInfo(flightData, result_entDbAfttab);
	        	LOG.debug("Handle class config used: {}ms", (System.currentTimeMillis() - start));
	        	
	        	// insert/update fevtab for events
	        	start = System.currentTimeMillis();
	        	handleEvent(flightData, result_entDbAfttab);
	        	LOG.debug("Handle event timing used: {}ms", (System.currentTimeMillis() - start));
	        	
	        	if (isMapped) {
	    			if (actions == null) {
	    				actions = new ArrayList<>();
	    			} else {
	    				actions.clear();
	    			}
	    			// set flight urno of afttab
	    			msgHead.getIdFlight().add(String.valueOf(result_entDbAfttab.getUrno()));
	    			
	    			EntUfisMsgACT act = new EntUfisMsgACT();
	    			// command: MACS ????
	    			act.setCmd(HpUfisAppConstants.UfisASCommands.IRT.toString());
	    			// fields: intflid ????
	    			act.getFld().add(NOTIFY_INTFLID);
	    			act.setTab(TAB_ID_MAPPING);
	    			act.getData().add(interfaceFltId);
	    			actions.add(act);
	    			String msg;
					try {
						msg = HpUfisJsonMsgFormatter.formDataInJson(actions, msgHead);
						start = System.currentTimeMillis();
						ufisQueueProducer.sendMessage(msg);
						LOG.debug("Send notify message used: {}ms", (System.currentTimeMillis() - start));
					} catch (Exception e) {
						LOG.error("Cannot format object to Json String: {}", e);
					}
	        	}
            }
    	}
    }

}
