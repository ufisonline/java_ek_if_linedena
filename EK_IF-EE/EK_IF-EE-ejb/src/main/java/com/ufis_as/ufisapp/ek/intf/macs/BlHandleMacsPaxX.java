package com.ufis_as.ufisapp.ek.intf.macs;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConnX;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxX;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequestX;
import com.ufis_as.ek_if.macs.entities.EnumLoadPaxConnType;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacadeForPax;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IApttabBeanLocal;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.macs.paxdetails.PaxDetails;
import ek.macs.paxfctdetails.FctDetails;
import ek.macs.paxinbdetails.InbDetails;
import ek.macs.paxonwdetails.OnwDetails;

/**
 * 
 * @author SCH
 *
 */

//@Stateful
@Stateless
public class BlHandleMacsPaxX {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlHandleMacsPaxX.class);

	@EJB
	BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlIrmtabFacadeForPax _irmfacade;
     @EJB
	 private EntStartupInitSingleton entStartupInitSingleton;
 	@EJB
 	private IApttabBeanLocal dlApttabBean;
	
	private Unmarshaller _um;

	private OnwDetails _onwDetails;
	private InbDetails _inbDetails;
	private PaxDetails _paxDetails;
	private EntDbLoadPaxConnX _entDbPaxInbOwnDetails;
	private EntDbLoadPaxX _entDbPax;
	private EntDbServiceRequestX _entDbPaxServiceRequest;
	private FctDetails _fctDetails;
	private JAXBContext _cnxJaxb;

	private String[] bookingStatusList = {"CB", "CA","RB","RS","CS","NA","RX","NS","WX","WA","WS"};
	private String[] statusOnBoardList = {"FL", "FM", "DDT", "DDU", "CR4"};
	
//    protected Message rawMsg; // commented on 2014-01-22
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(PaxDetails.class,
					InbDetails.class, OnwDetails.class, FctDetails.class);
			_um = _cnxJaxb.createUnmarshaller();
		} catch (JAXBException ex) {
			LOG.error("JAXBException when creating Unmarshaller");
		}

	}
	
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Object unMarshal(String name, String messageString, long irmtabRef, Message message) {
		try {

			switch (name) {
			case "paxDetails":
				_paxDetails = (PaxDetails) _um.unmarshal(new StreamSource(
						new StringReader(messageString)));
				return readStorePax(_paxDetails,irmtabRef, message);

			case "inb":
				_inbDetails = (InbDetails) _um.unmarshal(new StreamSource(
						new StringReader(messageString)));
				return readStorePax(_inbDetails, irmtabRef, message);
				
			case "onw":
				_onwDetails = (OnwDetails) _um.unmarshal(new StreamSource(
						new StringReader(messageString)));
				return readStorePax(_onwDetails, irmtabRef, message);

			case "fct":
				_fctDetails = (FctDetails) _um.unmarshal(new StreamSource(
						new StringReader(messageString)));
				return readStorePax(_fctDetails, irmtabRef, message);			

			}

		} catch (JAXBException ex) {
			LOG.error("Erro while unmarshal the msg urno: {}, exception : {}, msg : \n {}", irmtabRef,ex, messageString);

  		    logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
			
			sendErroNotification(EnumExceptionCodes.EXSDF.name(),ex.toString(),irmtabRef, message);
			
		}catch (Exception e){
			LOG.error("Erro while unmarshal the msg urno: {}, exception : {}", irmtabRef,e);
		}
		return null;
	}

	public EntDbLoadPaxX readStorePax(PaxDetails paxDetails, long irmtabRef, Message message) {
		EntDbLoadPaxX result = null;
		
		if (paxDetails != null) {
			
  		    logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
    		
    		// == START ==
    		/* - Added by BTR	- 17Jan2014
    		 * - Changes		- Reject to process the msg if the pax_status = "PD"
    		 */
    		if("PD".equalsIgnoreCase(paxDetails.getSTATUS())){
    			LOG.error("PAX status is PD. PaxDetail msg is rejected.");
    		//	sendErroNotification(EnumExceptionCodes.WPXPD.name(), EnumExceptionCodes.WPXPD.toString(), irmtabRef);
    			return null;
    		}
    		// == END ==
    		
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.setCustomFormat(HpEKConstants.MACS_TIME_FORMAT); // 2012-12-18 00:00:00
			SimpleDateFormat macsTimeFormat = new SimpleDateFormat(HpEKConstants.MACS_TIME_FORMAT);

			EntDbLoadPaxX edpiod = new EntDbLoadPaxX();
//			LoadPaxPK paxPk = new LoadPaxPK();

			// check as it's a mandatory field
			if (paxDetails.getMFLID() != null
					&& !paxDetails.getMFLID().isEmpty()) {
//				paxPk.setIntflid(paxDetails.getMFLID());
				edpiod.setIntFlId(paxDetails.getMFLID());
			} else {
				LOG.info("Mandatory field MFL_ID is null or empty, PaxDetails Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field MFL_ID is null or empty, PaxDetails Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (paxDetails.getREFERENCENUMBER() != null
					&& !paxDetails.getREFERENCENUMBER().isEmpty()) {
				edpiod.setIntRefNumber(paxDetails.getREFERENCENUMBER());
			} else {
				LOG.info("Mandatory field REFERENCE_NUMBER is null or empty, PaxDetails Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field REFERENCE_NUMBER is null or empty, PaxDetails Message is rejected",irmtabRef, message);
				 return null;
			}
			
			// check wrong paxType condition when infant indicator have value I (added new logic by 2013-12-2)
			if ("C".equalsIgnoreCase(paxDetails.getPAXTYPE()) && "I".equalsIgnoreCase(paxDetails.getINFANTINDICATOR())){
				LOG.info("wrong paxType 'C' when infant indicator have value 'I', PaxDetails Message is rejected");
				sendErroNotification(EnumExceptionCodes.EWPTY.name(),"wrong paxType 'C' when infant indicator have value 'I', PaxDetails Message is rejected",irmtabRef, message);
				 return null;
			}
			
//			paxPk.setIntSystem("MACS");
			edpiod.setIntSystem(HpEKConstants.MACS_PAX_DATA_SOURCE);
//			edpiod.setPKId(paxPk);
			// edpiod.setpKId(paxPk);

			// check as it's a mandatory field
			if (paxDetails.getPAXID() != null
					&& !paxDetails.getPAXID().isEmpty()) {
				edpiod.setIntId(paxDetails.getPAXID());
			} else {
				LOG.info("Mandatory field PAX_ID is null or empty, PaxDetails Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field PAX_ID is null or empty, PaxDetails Message is rejected",irmtabRef, message);
				 return null;
			}

			edpiod.setDestination(paxDetails.getDESTINATION());

			if (paxDetails.getPAXNAME() != null) {
				String paxName = paxDetails.getPAXNAME().replaceAll("/", " ");
				edpiod.setPaxName(paxName);
			}

			edpiod.setCabinClass(paxDetails.getCABINCLASS());
			edpiod.setBookedClass(paxDetails.getBOOKEDCLASS());
			edpiod.setPaxType(paxDetails.getPAXTYPE());
			edpiod.setPaxGroupCode(paxDetails.getPAXGROUPCODE());
			edpiod.setHandicapped(paxDetails.getHANDICAPPED());
			edpiod.setUnAccompaniedMinor(paxDetails.getUNACCOMPANIEDMINOR());
			edpiod.setPriorityPax(paxDetails.getPRIORITYPAX());
			edpiod.setBoardingPassprint(paxDetails.getBOARDINGPASSPRINT());
			// edpiod.setBagTagPrint(paxDetails.get); !!!! there is no this tag
			// in current xsd
			if(!Arrays.asList(bookingStatusList).contains(paxDetails.getPAXBOOKINGSTATUS())){
//				blIrmtabFacade.updateIRMStatus(PaxProcessSingleton.nextUrno, "WNOMD");
				LOG.warn("bookingStatus {} is not in the possible values from paxDetails msg and updated to IRMTAB, intId {}", paxDetails.getPAXBOOKINGSTATUS(), paxDetails.getPAXID());
				sendErroNotification(EnumExceptionCodes.WENUM.name(),"bookingStatus "+paxDetails.getPAXBOOKINGSTATUS()+" is not in the possible values from paxDetails msg and updated to IRMTAB, intId "+paxDetails.getPAXID()+"",irmtabRef, message);
			}
			edpiod.setPaxBookingStatus(paxDetails.getPAXBOOKINGSTATUS());
			edpiod.setTravelledClass(paxDetails.getTRAVELLEDCLASS());
			edpiod.setBagTagInfo(paxDetails.getVARIABLEDATA());
			edpiod.setScanLocation(paxDetails.getSCANNEDLOCATION());

			// set bag weight and piece, original data: e.g 002/0053/00
			String bagPicAndWeight = "";
			if (paxDetails.getNUMBEROFPCSWEIGHT() != null) {
				bagPicAndWeight = paxDetails.getNUMBEROFPCSWEIGHT();
			}
			String numberOfBag = "0";
			String weightOfBag = "0";
			int tagIndex = bagPicAndWeight.indexOf("/");
			int tagIndex2 = bagPicAndWeight.indexOf("/", tagIndex + 1);
			if (bagPicAndWeight != null && !"".equals(bagPicAndWeight)
					&& tagIndex > 0 && tagIndex2 > 0) {
				numberOfBag = bagPicAndWeight.substring(0, tagIndex);
				weightOfBag = bagPicAndWeight
						.substring(tagIndex + 1, tagIndex2);
			}else if (bagPicAndWeight != null && bagPicAndWeight.length() > 3 && "PLD".equalsIgnoreCase(bagPicAndWeight.substring(0,3))){
				edpiod.setPooledBagRef(bagPicAndWeight);
			}
			edpiod.setBagWeight(new BigDecimal(weightOfBag));
			edpiod.setBagNoOfPieces(new BigDecimal(numberOfBag));

			// // set check in date time
			// String cheInDate = "";
			// if (paxDetails.getCHECKINDATE() != null){
			// cheInDate = paxDetails.getCHECKINDATE();
			// }
			// String cheInTime = paxDetails.getCHECKINTIME();
			// String cheInDateTime = "";
			// Date cheInDateTimeD = null;
			//
			// int spaceIndex = cheInDate.indexOf(" ");
			//
			// if (spaceIndex > 0 && cheInTime.length() >= 4){
			// cheInDate = cheInDate.substring(0, spaceIndex);
			// cheInDateTime = cheInDate +
			// " "+cheInTime.substring(0,2)+":"+cheInTime.substring(2,4)+":00";
			// try {
			// cheInDateTimeD = new
			// SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(cheInDateTime);
			// edpiod.setCheckInDateTime(cheInDateTimeD);
			// } catch (ParseException e) {
			// LOG.debug("set cheInDateTime "+e.toString());
			// }
			// }

			// set check in date time
			if (paxDetails.getCHECKINDATE() != null
					&& !paxDetails.getCHECKINDATE().isEmpty()) {
				ufisCalendar.setTime(paxDetails.getCHECKINDATE(),
						ufisCalendar.getCustomFormat());
				if (paxDetails.getCHECKINTIME().length() >= 4) {
					int hour = 0;
					int min = 0;
					try {
						hour = Integer.parseInt(paxDetails.getCHECKINTIME()
								.substring(0, 2));
						min = Integer.parseInt(paxDetails.getCHECKINTIME()
								.substring(2, 4));
					} catch (Exception e) {
						LOG.info("PaxDetails message Checking Time parse erro");
					}
					ufisCalendar.DateAdd(hour, EnumTimeInterval.Hours);
					ufisCalendar.DateAdd(min, EnumTimeInterval.Minutes);
				}
				edpiod.setCheckInDateTime(ufisCalendar.getTime());
			}

			// // set scan date time
			// String scanDate = "";
			// if (paxDetails.getSCANNEDLOCALDATE() != null){
			// scanDate = paxDetails.getSCANNEDLOCALDATE();
			// }
			// String scanTime = paxDetails.getSCANNEDLOCALTIME();
			// String scanDateTime = "";
			// Date scanDateTimeD = null;
			//
			// int scanSpaceIndex = scanDate.indexOf(" ");
			//
			// if (scanSpaceIndex > 0 && scanTime.length() >= 4){
			// scanDate = scanDate.substring(0, scanSpaceIndex);
			// scanDateTime = scanDate +
			// " "+scanTime.substring(0,2)+":"+scanTime.substring(2,4)+":00";
			// try {
			// scanDateTimeD = new
			// SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(scanDateTime);
			// edpiod.setScanDateTime(scanDateTimeD);
			// } catch (ParseException e) {
			// LOG.debug("set scanDateTime "+e.toString());
			// }
			// }
			//
			
			edpiod.setBoardingStatus(paxDetails.getBOARDINGSTATUS());
			edpiod.setCancelled(paxDetails.getCANCELLED());
			edpiod.setOffLoadedPax(paxDetails.getOFFLOADEDPAX());
			edpiod.setEtkType(paxDetails.getETKTYPE());

			// set scan date time
			if (paxDetails.getSCANNEDLOCALDATE() != null
					&& !paxDetails.getSCANNEDLOCALDATE().isEmpty()) {
				ufisCalendar.setTime(paxDetails.getSCANNEDLOCALDATE(),
						ufisCalendar.getCustomFormat());
				if (paxDetails.getCHECKINTIME().length() >= 4) {
					int hour = 0;
					int min = 0;
					try {
						hour = Integer.parseInt(paxDetails
								.getSCANNEDLOCALTIME().substring(0, 2));
						min = Integer.parseInt(paxDetails.getSCANNEDLOCALTIME()
								.substring(2, 4));
						
//						// added by 2014-01-02
//						// convert local to UTC
//						// search for time difference
//						long startTIme = System.currentTimeMillis();
//						String timeDiffStr =  dlApttabBean.getTDI1(paxDetails.getCHECKINCITY());
//						if (timeDiffStr != null && !"".equals(timeDiffStr)){
//							LOG.debug("parse timeDiffStr {} to int", timeDiffStr);
//							int timeDiffInt = Integer.parseInt(timeDiffStr);
//							min = min - timeDiffInt;
//						}
//						LOG.debug("search tdi1 from apttab table, apc3 {} , takes {}",paxDetails.getCHECKINCITY(), System.currentTimeMillis() - startTIme);
						
					} catch (Exception e) {
						LOG.info("PaxDetails message Scanned Time parse erro, {}" ,e);
					}
					ufisCalendar.DateAdd(hour, EnumTimeInterval.Hours);
					ufisCalendar.DateAdd(min, EnumTimeInterval.Minutes);
				}
				edpiod.setScanDateTime(ufisCalendar.getTime());
			}

			// set date of birth
			Date dob = null;
			try {
				String dobString = paxDetails.getDATEOFBIRTH();
				if (dobString != null && !dobString.isEmpty()) {
					dob = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
							.parse(dobString);
				}
				edpiod.setDob(dob);
			} catch (ParseException e) {
				LOG.info("PaxDetails Message, dob parse erro");
			}

			edpiod.setNationality(paxDetails.getNATIONALITY());
			edpiod.setGender(paxDetails.getGENDER());
			if (Arrays.asList(statusOnBoardList).contains(paxDetails.getSTATUSONBOARD())){
//				blIrmtabFacade.updateIRMStatus(PaxProcessSingleton.nextUrno, "WNOMD");
				LOG.warn("statusOnBoard {} is not in the possible values from paxDetails msg and updated to IRMTAB, intId {}", paxDetails.getSTATUSONBOARD(), paxDetails.getPAXID());
				sendErroNotification(EnumExceptionCodes.WENUM.name(),"statusOnBoard "+paxDetails.getSTATUSONBOARD()+" is not in the possible values from paxDetails msg and updated to IRMTAB, intId "+paxDetails.getPAXID()+"",irmtabRef, message);
			}
			edpiod.setStatusOnboard(paxDetails.getSTATUSONBOARD());
			edpiod.setInfantIndicator(paxDetails.getINFANTINDICATOR());
			edpiod.setCheckInAgentCode(paxDetails.getCHECKINAGENTCODE());
			edpiod.setCheckInHandlingAgent(paxDetails.getCHECKINHANDLINGAGENT());
			edpiod.setCheckInSequence(paxDetails.getCHECKINSEQUENCE());
			edpiod.setCheckInCity(paxDetails.getCHECKINCITY());
			edpiod.setBoardingAgentCode(paxDetails.getBOARDINGAGENTCODE());
			edpiod.setBoardingHandlingAgent(paxDetails
					.getBOARDINGHANDLINGAGENT());

			// // set boarding datetime
			// String boardingDate = "";
			// if (paxDetails.getCHECKINDATE() != null){
			// boardingDate = paxDetails.getBOARDINGDATE();
			// }
			// String boardingTime = paxDetails.getBOARDINGTIME();
			// String boardingDateTime = "";
			// Date boardingDateTimeD = null;
			//
			// int boardSpaceIndex = boardingDate.indexOf(" ");
			//
			// if (boardSpaceIndex > 0 && boardingTime.length() >= 4){
			// boardingDate = boardingDate.substring(0, boardSpaceIndex);
			// boardingDateTime = boardingDate +
			// " "+boardingTime.substring(0,2)+":"+boardingTime.substring(2,4)+":00";
			// try {
			// boardingDateTimeD = new
			// SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(boardingDateTime);
			// edpiod.setBoardingDateTime(boardingDateTimeD);
			// } catch (ParseException e) {
			// LOG.debug("set boardingDateTime "+e.toString());
			// }
			// }

			// set boarding datetime
			if (paxDetails.getBOARDINGDATE() != null
					&& !paxDetails.getBOARDINGDATE().isEmpty()) {
				ufisCalendar.setTime(paxDetails.getBOARDINGDATE(),
						ufisCalendar.getCustomFormat());
				if (paxDetails.getBOARDINGTIME().length() >= 4) {
					int hour = 0;
					int min = 0;
					try {
						hour = Integer.parseInt(paxDetails.getBOARDINGTIME()
								.substring(0, 2));
						min = Integer.parseInt(paxDetails.getBOARDINGTIME()
								.substring(2, 4));
						
//						// added by 2014-01-02
//						// convert local to UTC
//						// search for time difference
//						long startTIme = System.currentTimeMillis();
//						String timeDiffStr =  dlApttabBean.getTDI1(paxDetails.getCHECKINCITY());
//						if (timeDiffStr != null && !"".equals(timeDiffStr)){
//							LOG.debug("parse timeDiffStr {} to int", timeDiffStr);
//							int timeDiffInt = Integer.parseInt(timeDiffStr);
//							min = min - timeDiffInt;
//						}
//						LOG.debug("search tdi1 from apttab table, apc3 {} , takes {}",paxDetails.getCHECKINCITY(), System.currentTimeMillis() - startTIme);
						
					} catch (Exception e) {
						LOG.info("PaxDetails message Boarding Time parse erro");
					}
					ufisCalendar.DateAdd(hour, EnumTimeInterval.Hours);
					ufisCalendar.DateAdd(min, EnumTimeInterval.Minutes);
				}
				edpiod.setBoardingDateTime(ufisCalendar.getTime());
			}

			// added by 2013.10.01 according to the new design document
			edpiod.setUpgradeIndicator(paxDetails.getUPGRADEINDICATOR());
			edpiod.setTransitIndicator(paxDetails.getTRANSITINDICATOR());
			edpiod.setSeatNumber(paxDetails.getSEATNUMBER());
			edpiod.setJtopPax(paxDetails.getJTOPPAXRELATION());
			edpiod.setTransitBagIndicator(paxDetails.getTRANSITBAG());
			edpiod.setETickedId(paxDetails.getETKID());
			edpiod.setTicketNumber(paxDetails.getTICKETNUMBER());
			// edpiod .setChkDigit(paxDetails.getch) !!! there is no CHKDIGIT tag in ICD
			edpiod.setCouponNumber(paxDetails.getCOUPONNUMBER());
			edpiod.setApdType(paxDetails.getAPDTYPE());
			edpiod.setDocumentType(paxDetails.getDOCUMENTTYPE());
			edpiod.setDocumentIssuedCountry(paxDetails.getCOUNTRYOFISSUE());
			edpiod.setCountryOfBirth(paxDetails.getCOUNTRYOFBIRTH());
			edpiod.setCountryOfResidence(paxDetails.getCOUNTRYOFRESIDENCE());
			edpiod.setItnEmbarkation(paxDetails.getITNEMBARKATION());
			edpiod.setItnDisembarkation(paxDetails.getITNDISEMBARKATION());
			// added by 2014-01-02
//			edpiod.setPnr(paxDetails.getPNRID());
			// change mapping to RECLOCATOR()
			edpiod.setPnr(paxDetails.getRECLOCATOR());
//		    edpiod.setScanStation(paxDetails.getSCANNEDLOCATION()); 
			// "scan_station" or scan_location ?
			// edpiod.setScanTerminal(paxDetails.getscan)
			// edpiod.setScanTransferArea(scanTransferArea);
			// edpiod.setScannerId(scannerId);
			
			edpiod.setPaxStatus(paxDetails.getSTATUS());
			
			try {
				if(paxDetails.getEXPIRYDATE() != null && !"".equals(paxDetails.getEXPIRYDATE().trim())){
				edpiod.setDocumentExpiryDate(macsTimeFormat.parse(paxDetails.getEXPIRYDATE()));
				}
			} catch (ParseException e) {
				LOG.error("Erro when parsing the expire date from string to date, {}",e);
			}
			try {
				if (paxDetails.getDATEOFISSUE() != null && !"".equals(paxDetails.getDATEOFISSUE())){
				edpiod.setDocumentIssuedDate(macsTimeFormat.parse(paxDetails.getDATEOFISSUE()));
				}
			} catch (ParseException e) {
				LOG.error("Erro when parsing the issue date from string date, {}",e);
			}
//			SimpleDateFormat dateOfBirthDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
//			try {
//				edpiod.setDocumentExpiryDate(dateOfBirthDf.parse(HpUfisCalendar
//						.toUTCTime("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss", dateOfBirthDf
//								.format(paxDetails.getEXPIRYDATE()))));
//			} catch (ParseException e) {
//				LOG.error("Erro when converting DocumentExpiryDate to UTC DateTime");
//			}
//			try {
//				edpiod.setDocumentIssuedDate(dateOfBirthDf.parse(HpUfisCalendar
//						.toUTCTime("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss", dateOfBirthDf
//								.format(paxDetails.getDATEOFISSUE()))));
//			} catch (ParseException e) {
//				LOG.error("Erro when converting DocumentIssuedDate to UTC DateTime");
//			}

//			result = (EntDbLoadPax) SerializationUtils.clone(edpiod);
			result = edpiod;

			
		}
//		else{
//			// return null;
//		}
		return result;
	}

	public EntDbLoadPaxConnX readStorePax(InbDetails inbDetails, long irmtabRef, Message message) {
		// Store Pax Details for this MAXCS ID
		// find record with the maxcs id and
		EntDbLoadPaxConnX result = null;
		
		if (inbDetails != null) {
			
  		logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
			
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.setCustomFormat(HpEKConstants.MACS_TIME_FORMAT); // 2012-12-18
																			// 00:00:00

			EntDbLoadPaxConnX edpiod = new EntDbLoadPaxConnX();
//			LoadPaxConnPK paxConnPk = new LoadPaxConnPK();

			// To check and set the mandatory fields first
			// check as it's a mandatory field
			if (inbDetails.getInbId() != null
					&& !inbDetails.getInbId().isEmpty()) {
				edpiod.setIntId(inbDetails.getInbId());
			} else {
				LOG.info("Mandatory field INB_ID is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field INB_ID is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (inbDetails.getMflId() != null
					&& !inbDetails.getMflId().isEmpty()) {
				edpiod.setInterfaceFltid(inbDetails.getMflId());
//				edpiod.setInterfaceFltid(inbDetails.getMflId());
			} else {
				LOG.info("Mandatory field MFL_ID is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field MFL_ID is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (inbDetails.getBoardPoint() != null
					&& !inbDetails.getBoardPoint().isEmpty()) {
				edpiod.setBoardPoint(inbDetails.getBoardPoint());
			} else {
				LOG.info("Mandatory field BOARD_POINT is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field BOARD_POINT is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (inbDetails.getOffPoint() != null
					&& !inbDetails.getOffPoint().isEmpty()) {
				edpiod.setOffPoint(inbDetails.getOffPoint());
			} else {
				LOG.info("Mandatory field OFF_POINT is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field OFF_POINT is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// // check as it's a mandatory field
			// if (inbDetails.getAirlineDesignator() != null &&
			// !inbDetails.getAirlineDesignator().isEmpty()){
			// edpiod.setAirlineCode(inbDetails.getAirlineDesignator());
			// }else{
			// LOG.info("Mandatory field AIRLINE_DESIGNATOR is null or empty, Inbound Message is rejected");
			// // return null;
			// }

			// check as it's a mandatory field
			if (inbDetails.getAirlineDesignator() == null
					|| inbDetails.getAirlineDesignator().isEmpty()) {
				LOG.info("Mandatory field AIRLINE_DESIGNATOR is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field AIRLINE_DESIGNATOR is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// // check as it's a mandatory field
			// if (inbDetails.getFlightNumber() != null &&
			// !inbDetails.getFlightNumber().isEmpty()){
			// String strFlightNumber = inbDetails.getFlightNumber();
			// strFlightNumber = HpUfisUtils.formatCedaFltn(strFlightNumber);
			// // if (strFlightNumber.length() != 3){
			// // strFlightNumber =
			// Integer.toString(Integer.parseInt(strFlightNumber));
			// // if (strFlightNumber.length() < 3){
			// // int numAdd = 3 - strFlightNumber.length();
			// // for (int i= 0; i < numAdd; i++ ){
			// // strFlightNumber = "0"+strFlightNumber;
			// // }
			// // }
			// // }
			// edpiod.setFlightNumber(strFlightNumber);
			// }else{
			// LOG.info("Mandatory field FLIGHT_NUMBER is null or empty, Inbound Message is rejected");
			// // return null;
			// }

			// check as it's a mandatory field
			if (inbDetails.getFlightNumber() == null
					|| inbDetails.getFlightNumber().isEmpty()) {
				LOG.info("Mandatory field FLIGHT_NUMBER is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field FLIGHT_NUMBER is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// if(inbDetails.getFlightNumberExp() != null){
			// edpiod.setFlightNumberSuffice(inbDetails.getFlightNumberExp());
			// }else{
			// edpiod.setFlightNumberSuffice(" "); // default value
			// }

			// check as it's a mandatory field
			if (inbDetails.getOperationDate() != null
					&& !inbDetails.getOperationDate().isEmpty()) {
				ufisCalendar.setTime(inbDetails.getOperationDate(),
						ufisCalendar.getCustomFormat());
				if (inbDetails.getOperationTime().length() >= 4) {
					int hour = 0;
					int min = 0;
					try {
						hour = Integer.parseInt(inbDetails.getOperationTime()
								.substring(0, 2));
						min = Integer.parseInt(inbDetails.getOperationTime()
								.substring(2, 4));
					} catch (Exception e) {
						LOG.info("Inbound message OperationTime parse erro");
					}
					ufisCalendar.DateAdd(hour, EnumTimeInterval.Hours);
					ufisCalendar.DateAdd(min, EnumTimeInterval.Minutes);

				}
				edpiod.setConxFltDate(ufisCalendar.getTime());
			} else {
				LOG.info("Mandatory field OPERATION_DATE is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field OPERATION_DATE is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (inbDetails.getReferenceNumber() != null
					&& !inbDetails.getReferenceNumber().isEmpty()) {
//				edpiod.setIntRefNumber(inbDetails.getReferenceNumber());
				edpiod.setIntRefNumber(inbDetails.getReferenceNumber());
			} else {
				LOG.info("Mandatory field REFERENCE_NUMBER is null or empty, Inbound Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field REFERENCE_NUMBER is null or empty, Inbound Message is rejected",irmtabRef, message);
				 return null;
			}
			
//			edpiod.setPaxConnPK(paxConnPk);

			edpiod.setIntSystem(HpEKConstants.MACS_PAX_DATA_SOURCE);
			edpiod.setConnType(EnumLoadPaxConnType.I.toString());
			edpiod.setBookedClass(inbDetails.getBookedClass());
			edpiod.setDownloadDate(inbDetails.getDownloadDate());
			edpiod.setConnStatus(inbDetails.getStatus());
			edpiod.setMsgTimeStamp(inbDetails.getTimeStamp());
			edpiod.setVariableData(inbDetails.getVariableData());
			// added by 2013.10.01 according to the new design document
			edpiod.setPaxConxFlno(HpUfisUtils.formatCedaFlno(inbDetails
					.getAirlineDesignator(), HpUfisUtils.formatCedaFltn(inbDetails.getFlightNumber()),
					inbDetails.getFlightNumberExp() == null ? "" : inbDetails
							.getFlightNumberExp().trim()));
			edpiod.setAirlineCode(inbDetails.getAirlineDesignator()); // added by 2013-12-31

//			result = (EntDbLoadPaxConn) SerializationUtils
//					.clone(edpiod);
//			_edpiods.add(edpiod);
			result = edpiod;
			
		}
//		else{
//		// return null;
//		}
		
		return result;
	}

	public EntDbLoadPaxConnX readStorePax(OnwDetails ownDetails, long irmtabRef, Message message) {
		// Store Pax Details for this MAXCS ID
		// find record with the maxcs id and
		
		EntDbLoadPaxConnX result = null;
		
		if (ownDetails != null) {
			
  		logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
			
			EntDbLoadPaxConnX edpiod = new EntDbLoadPaxConnX();
//			LoadPaxConnPK paxConnPk = new LoadPaxConnPK();
			
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.setCustomFormat(HpEKConstants.MACS_TIME_FORMAT); // 2012-12-18
																			// 00:00:00

			// check as it's a mandatory field
			if (ownDetails.getOnwId() != null
					&& !ownDetails.getOnwId().isEmpty()) {
				edpiod.setIntId(ownDetails.getOnwId());
			} else {
				LOG.info("Mandatory field ONW_ID is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field ONW_ID is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (ownDetails.getMflId() != null
					&& !ownDetails.getMflId().isEmpty()) {
//				edpiod.setInterfaceFltid(ownDetails.getMflId());
				edpiod.setInterfaceFltid(ownDetails.getMflId());
			} else {
				LOG.info("Mandatory field MFL_ID is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field MFL_ID is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (ownDetails.getBoardPoint() != null
					&& !ownDetails.getBoardPoint().isEmpty()) {
				edpiod.setBoardPoint(ownDetails.getBoardPoint());
			} else {
				LOG.info("Mandatory field BOARD_POINT is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field BOARD_POINT is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (ownDetails.getOffPoint() != null
					&& !ownDetails.getOffPoint().isEmpty()) {
				edpiod.setOffPoint(ownDetails.getOffPoint());
			} else {
				LOG.info("Mandatory field OFF_POINT is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field OFF_POINT is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// // check as it's a mandatory field
			// if (ownDetails.getAirlineDesignator() != null &&
			// !ownDetails.getAirlineDesignator().isEmpty()){
			// edpiod.setAirlineCode(ownDetails.getAirlineDesignator());
			// }else{
			// LOG.info("Mandatory field AIRLINE_DESIGNATOR is null or empty, Onward Message is rejected");
			// // return null;
			// }

			// check as it's a mandatory field
			if (ownDetails.getAirlineDesignator() == null
					|| ownDetails.getAirlineDesignator().isEmpty()) {
				LOG.info("Mandatory field AIRLINE_DESIGNATOR is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field AIRLINE_DESIGNATOR is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// // check as it's a mandatory field
			// if (ownDetails.getFlightNumber() != null &&
			// !ownDetails.getFlightNumber().isEmpty()){
			// String strFlightNumber = ownDetails.getFlightNumber();
			// strFlightNumber = HpUfisUtils.formatCedaFltn(strFlightNumber);
			// // if (strFlightNumber.length() != 3){
			// // strFlightNumber =
			// Integer.toString(Integer.parseInt(strFlightNumber));
			// // if (strFlightNumber.length() < 3){
			// // int numAdd = 3 - strFlightNumber.length();
			// // for (int i= 0; i < numAdd; i++ ){
			// // strFlightNumber = "0"+strFlightNumber;
			// // }
			// // }
			// // }
			//
			// edpiod.setFlightNumber(strFlightNumber);
			// }else{
			// LOG.info("Mandatory field FLIGHT_NUMBER is null or empty, Onward Message is rejected");
			// // return null;
			// }

			// check as it's a mandatory field
			if (ownDetails.getFlightNumber() == null
					|| ownDetails.getFlightNumber().isEmpty()) {
				LOG.info("Mandatory field FLIGHT_NUMBER is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field FLIGHT_NUMBER is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

			// if(ownDetails.getFlightNumberExp() != null){
			// edpiod.setFlightNumberSuffice(ownDetails.getFlightNumberExp());
			// }else{
			// edpiod.setFlightNumberSuffice(" "); // default value
			// }

			// check as it's a mandatory field
			if (ownDetails.getOperationDate() != null
					&& !ownDetails.getOperationDate().isEmpty()) {
				ufisCalendar.setTime(ownDetails.getOperationDate(),
						ufisCalendar.getCustomFormat());
				if (ownDetails.getOperationTime().length() >= 4) {
					int hour = 0;
					int min = 0;
					try {
						hour = Integer.parseInt(ownDetails.getOperationTime()
								.substring(0, 2));
						min = Integer.parseInt(ownDetails.getOperationTime()
								.substring(2, 4));
					} catch (Exception e) {
						LOG.info("Onward message OperationTime parse erro");
					}
					ufisCalendar.DateAdd(hour, EnumTimeInterval.Hours);
					ufisCalendar.DateAdd(min, EnumTimeInterval.Minutes);
				}
				edpiod.setConxFltDate(ufisCalendar.getTime());
			} else {
				LOG.info("Mandatory field OPERATION_DATE is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field OPERATION_DATE is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}

//			// check as it's a mandatory field
//			if (ownDetails.getOperationDate() == null
//					|| ownDetails.getOperationDate().isEmpty()) {
//				LOG.info("Mandatory field OPERATION_DATE is null or empty, Onward Message is rejected");
//				addExptInfo(EnumExceptionCodes.EMAND.name(),irmtabRef.name(), "OPERATION_DATE");
//				// return null;
//			}

			// check as it's a mandatory field
			if (ownDetails.getReferenceNumber() != null
					&& !ownDetails.getReferenceNumber().isEmpty()) {
//				edpiod.setIntRefNumber(ownDetails.getReferenceNumber());
				edpiod.setIntRefNumber(ownDetails.getReferenceNumber());
			} else {
				LOG.info("Mandatory field REFERENCE_NUMBER is null or empty, Onward Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field REFERENCE_NUMBER is null or empty, Onward Message is rejected",irmtabRef, message);
				 return null;
			}
			
//			edpiod.setPaxConnPK(paxConnPk);

			edpiod.setIntSystem(HpEKConstants.MACS_PAX_DATA_SOURCE);
			edpiod.setConnType(EnumLoadPaxConnType.O.toString());
			edpiod.setBookedClass(ownDetails.getBookedClass());
			edpiod.setDownloadDate(ownDetails.getDownloadDate());
			edpiod.setConnStatus(ownDetails.getStatus());
			edpiod.setMsgTimeStamp(ownDetails.getTimeStamp());
			edpiod.setVariableData(ownDetails.getVariableData());
			// added by 2013.10.01 according to the new design document
			edpiod.setPaxConxFlno(HpUfisUtils.formatCedaFlno(ownDetails
					.getAirlineDesignator(),  HpUfisUtils.formatCedaFltn(ownDetails.getFlightNumber()),
					ownDetails.getFlightNumberExp() == null ? "" : ownDetails
							.getFlightNumberExp().trim()));
			edpiod.setAirlineCode(ownDetails.getAirlineDesignator()); // added by 2013-12-31

//			result = (EntDbLoadPaxConn) SerializationUtils
//					.clone(edpiod);
			result = edpiod;

//			_edpiods.add(edpiod);
		
		}
//		else{
//		// return null;
//		}
		return result;
	}

	public EntDbServiceRequestX readStorePax(FctDetails fctDetails, long irmtabRef, Message message) {
		EntDbServiceRequestX result = null;

		if (fctDetails != null) {
			
  		logLevel = entStartupInitSingleton.getIrmLogLev();
    		
    		msgLogged = Boolean.FALSE;
    		if (irmtabRef > 0) {
    			msgLogged = Boolean.TRUE;
    		}
			
			EntDbServiceRequestX edpiod = new EntDbServiceRequestX();
//			ServiceRequestPK serviceRequestPK =  new ServiceRequestPK();
			
			edpiod.setIntSystem("MACS");

			// check as it's a mandatory field
			if (fctDetails.getFCTID() != null
					&& !fctDetails.getFCTID().isEmpty()) {
				edpiod.setIntId(fctDetails.getFCTID());
			} else {
				LOG.info("Mandatory field FCT_ID is null or empty, Fact Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field FCT_ID is null or empty, Fact Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (fctDetails.getMFLID() != null
					&& !fctDetails.getMFLID().isEmpty()) {
//				edpiod.setIntFlId(fctDetails.getMFLID());
				edpiod.setIntFlId(fctDetails.getMFLID());
			} else {
				LOG.info("Mandatory field MFL_ID is null or empty, Fact Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field MFL_ID is null or empty, Fact Message is rejected",irmtabRef, message);
				 return null;
			}

			// check as it's a mandatory field
			if (fctDetails.getREFERENCENUMBER() != null
					&& !fctDetails.getREFERENCENUMBER().isEmpty()) {
//				edpiod.setIntRefNumber(fctDetails.getREFERENCENUMBER());
				edpiod.setIntRefNumber(fctDetails.getREFERENCENUMBER());
			} else {
				LOG.info("Mandatory field REFERENCENUMBER is null or empty, Fact Message is rejected");
				sendErroNotification(EnumExceptionCodes.EMAND.name(),"Mandatory field REFERENCENUMBER is null or empty, Fact Message is rejected",irmtabRef, message);
				 return null;
			}

//			edpiod.setServiceRequestPK(serviceRequestPK);
			
			edpiod.setRequestType(fctDetails.getOSISSR());
//			edpiod.setServiceCode(fctDetails.getFACTCODE());
			edpiod.setServiceType(fctDetails.getFACTTYPE());
			edpiod.setExtInfo(fctDetails.getVARIABLEDATA());
			// variable data contains service code and service (base on design 2.0) added on 2013-11-29
			String serviceCode = "";
			if (fctDetails.getVARIABLEDATA() != null ){
				int hIndex = fctDetails.getVARIABLEDATA().indexOf("-");
				if (hIndex != -1){
					serviceCode =fctDetails.getVARIABLEDATA().substring(0,hIndex+1);
				}else if (fctDetails.getVARIABLEDATA().trim().length() == 4){
					serviceCode = fctDetails.getVARIABLEDATA();
				}
			}
			
			edpiod.setServiceCode(serviceCode);

//			result = (EntDbServiceRequest) SerializationUtils
//					.clone(edpiod);
			
			result = edpiod;

		
		}
//		else{
//		// return null;
//		}
		
		return result;
	}
	
	
	
	private void sendErrInfo(String expCode, String desc, Long irmtabRef)
			throws IOException, JsonGenerationException, JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode);
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(HpEKConstants.MACS_PAX_DATA_SOURCE);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, HpEKConstants.MACS_PAX_DATA_SOURCE);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from ACTS-ODS to Data Loader :\n{}", msg);

	}
	
	
	private void sendErroNotification(String eec,String desc, long irmtabRef, Message message){
		
		long startTime = System.currentTimeMillis();
		
		if (logLevel
				.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
						.name())) {
			if (!msgLogged) {
				irmtabRef = _irmfacade.storeRawMsg(message, HpEKConstants.MACS_PAX_DATA_SOURCE);
				msgLogged = Boolean.TRUE;
			}
			
		}
		
	
		if (irmtabRef > 0) {
			try {
				sendErrInfo(eec, desc, irmtabRef);
			} catch (JsonGenerationException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (JsonMappingException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			} catch (IOException e) {
				LOG.error("Erro send ErroInfo, {}",e);
			}
		}
		
		LOG.debug("send exception msg {}, takes {}", eec, System.currentTimeMillis()-startTime);
		
	}

//	public String get_flightXml() {
//		return _flightXml;
//	}
//
//	public void set_flightXml(String _flightXml) {
//		this._flightXml = _flightXml;
//	}

	public Unmarshaller get_um() {
		return _um;
	}

	public void set_um(Unmarshaller _um) {
		this._um = _um;
	}

//	public List<EntDbLoadPaxConn> get_edpiods() {
//		return _edpiods;
//	}
//
//	public void set_edpiods(List<EntDbLoadPaxConn> _edpiods) {
//		this._edpiods = _edpiods;
//	}

	public EntDbLoadPaxConnX get_entDbPaxInbOwnDetails() {
		return _entDbPaxInbOwnDetails;
	}

	public void set_entDbPaxInbOwnDetails(
			EntDbLoadPaxConnX _entDbPaxInbOwnDetails) {
		this._entDbPaxInbOwnDetails = _entDbPaxInbOwnDetails;
	}

	public EntDbLoadPaxX get_entDbPax() {
		return _entDbPax;
	}

	public void set_entDbPax(EntDbLoadPaxX _entDbPax) {
		this._entDbPax = _entDbPax;
	}

	public EntDbServiceRequestX get_entDbPaxServiceRequest() {
		return _entDbPaxServiceRequest;
	}

	public void set_entDbPaxServiceRequest(
			EntDbServiceRequestX _entDbPaxServiceRequest) {
		this._entDbPaxServiceRequest = _entDbPaxServiceRequest;
	}

//	public Message getRawMsg() {
//		return rawMsg;
//	}
//
//	public void setRawMsg(Message rawMsg) {
//		this.rawMsg = rawMsg;
//	}
	
	



}
