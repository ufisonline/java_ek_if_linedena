package com.ufis_as.ufisapp.ek.intf.opera;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.opera.entities.EntDbMdLoungeCode;
import com.ufis_as.ek_if.opera.entities.EntDbPaxLoungeMove;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlPaxBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxLoungeMoveBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.opera.paxloungescan.LoungeUsage;

/**
 * @author btr
 * Session Bean implementation class BlRouterPaxLounge
 */
@Stateless
public class BlHandlePaxLoungeBean extends BaseDTO{

	public static final Logger LOG = LoggerFactory.getLogger(EntDbPaxLoungeMove.class);

	private JAXBContext clscnxJaxb;
	private Unmarshaller clsum;
	DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private DlPaxBean clsDlPaxBean;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private DlPaxLoungeMoveBean clsDlPaxLoungeMoveBean;
	@EJB
    BlUfisBCTopic clsBlUfisBCTopic;
	
	public BlHandlePaxLoungeBean() {
    }

	@PostConstruct
	private void initialize()
	{
		try {
			clscnxJaxb = JAXBContext.newInstance(LoungeUsage.class);
			clsum = clscnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = clsEntStartUpInitSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... ");
		}
	}
	
	public boolean processPaxLoungeUsage(String message, long irmtabRef) {
	  	// clear
    	data.clear();
    	
    	// set urno of irmtab
    	irmtabUrno = irmtabRef;
   	try {
			LoungeUsage _input = (LoungeUsage) clsum.unmarshal(new StreamSource(new StringReader(message)));
			if(	_input.getMeta().getMessageTime() == null ||
					HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getSource())||
					HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getSubtype())||
					HpUfisUtils.isNullOrEmptyStr(_input.getMeta().getType())){
				LOG.error(" Lounge Usage Meta mandatory data are missing. Message dropped. ");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			//check for possible value of type.
			if(!HpEKConstants.OPERA_META_SUBTYPE.equals(_input.getMeta().getSubtype().trim())
					|| !HpEKConstants.OPERA_META_SOURCE.equals(_input.getMeta().getSource().trim())){
				LOG.error("Input Meta source or subtype mandatory are missing. Message dropped. ");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			if(HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getFFltNo())||
					(_input.getFlightID().getFltDate()== null)||
					(HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getArrStn())&& HpUfisUtils.isNullOrEmptyStr(_input.getFlightID().getDepStn()))||
					HpUfisUtils.isNullOrEmptyStr(_input.getPaxDetails().getRegKey())||
					HpUfisUtils.isNullOrEmptyStr(_input.getPaxDetails().getSeqNo()) ||
					HpUfisUtils.isNullOrEmptyStr(_input.getPaxDetails().getPaxName())||
					_input.getAudit().getScanTime() == null||
					HpUfisUtils.isNullOrEmptyStr(_input.getPaxDetails().getLoungeClass())||
					HpUfisUtils.isNullOrEmptyStr(_input.getAudit().getScanBy())){
				LOG.error("Mandatory info for Lounge Usage are empty. Message dropped. ");
				addExptInfo(EnumExceptionCodes.EMAND.name(), "");
				return false;
			}
			//ignore if it's not EK flight
			if(!_input.getFlightID().getFFltNo().contains("EK")){
				LOG.error("Processing performs only for EK flight. Message dropped.");
				addExptInfo(EnumExceptionCodes.EWALC.name(), _input.getFlightID().getCxCd());
				return false;
			}
			
			String ffltNo = null;
			String fltDate = null;
			EntDbAfttab entFlight = null;
			BigDecimal urno = null;
			String id_load_pax = null;
			String id_md_lounge_code = "0";
			String scanTime = null;
			List<EntDbMdLoungeCode> entLoungeCodeList = new ArrayList<>();
			
			//convert the string for flight "EK" to ufis ceda flight num format
			ffltNo = _input.getFlightID().getFFltNo().substring(0,2) + " " +
					HpUfisUtils.formatCedaFltn(_input.getFlightID().getFFltNo().substring(2, _input.getFlightID().getFFltNo().length()));
			
			/**** For input UTC ******/
			HpUfisCalendar inputFlDate = new HpUfisCalendar(_input.getFlightID().getFltDate());
			fltDate = inputFlDate.getCedaString();
			
			/**** For input LOCAL ****/
			//fltDate = convertFlDateToUTC_Lounge(_input.getFlightID().getFltDate());
			
			if(HpEKConstants.EK_HOPO.equals(_input.getFlightID().getArrStn()) 
					&& HpEKConstants.EK_HOPO.equals(_input.getFlightID().getDepStn())){// ArrStn = DepStn = DXB
				entFlight = clsAfttabBeanLocal.findFlightForReturn(ffltNo, fltDate);
			}else if(HpEKConstants.EK_HOPO.equals(_input.getFlightID().getArrStn())){// ArrStn = DXB
				entFlight = clsAfttabBeanLocal.findFlightForEKArr(ffltNo, fltDate);
			}
			else if(HpEKConstants.EK_HOPO.equals(_input.getFlightID().getDepStn())){// DepStn = DXB
				entFlight = clsAfttabBeanLocal.findFlightForEKDept(ffltNo, fltDate);
			}else{// ArrStn != DepStn != DXB
				//search from VIAL
				LOG.debug("Looking for the flight from its VIAL..");
				entFlight = clsAfttabBeanLocal.findFlightFromVial(ffltNo, fltDate, _input.getFlightID().getDepStn(), _input.getFlightID().getArrStn());
			}
			//urno = (entFlight == null) ? new BigDecimal(0) : entFlight.getUrno();
			EntDbLoadPax load_pax = null;
			if(entFlight == null){
				LOG.error("Flight flno <{}> is not found. Message dropped.", ffltNo);
				addExptInfo(EnumExceptionCodes.ENOFL.name(), ffltNo);
				return false;
			}
			else{
				urno = entFlight.getUrno();
				load_pax = clsDlPaxBean.getPaxWithPaxRefNum(_input.getPaxDetails().getSeqNo(), String.valueOf(urno)); 
			}
			/*if(intFlid != null){
				//get the pax id
				load_pax = clsDlPaxBean.getPaxForPaxLounge(_input.getPaxDetails().getSeqNo(), intFlid);
			}*/
			 
			id_load_pax = (load_pax == null)? "0" : load_pax.getUuid();
			
			//get lounge code id from cache 
			boolean isFound = false;
			entLoungeCodeList = clsEntStartUpInitSingleton.getLoungeCodeList();
			for(int i = 0; i < entLoungeCodeList.size() ; i ++ ){
				if(entLoungeCodeList.get(i).getLoungeCode().trim().equals(_input.getPaxDetails().getLoungeCode().trim())){
					id_md_lounge_code = entLoungeCodeList.get(i).getId(); 
					isFound = true; break;
				}
			}
			if(!isFound){
				LOG.warn("Input LoungeCode is not in MD_LOUNGE_CODE table.");
				addExptInfo(EnumExceptionCodes.WNOMD.name(), _input.getPaxDetails().getLoungeCode());
			}
		
			//Date scanInTimeUTC = null;
			Date scanTimeUTC = null;
			if(_input.getAudit().getScanTime() != null){
				scanTime = convertFlDateToUTC_Lounge(_input.getAudit().getScanTime());
				scanTimeUTC = new HpUfisCalendar(scanTime).getTime();
			}
			
			EntDbPaxLoungeMove oldEnt = null; String cmd = UfisASCommands.IRT.name();
			//find existing pax lounge move only if the flight is not null.
			EntDbPaxLoungeMove entPaxLoungeMove = clsDlPaxLoungeMoveBean.getPaxLoungeMove(_input.getPaxDetails().getRegKey(), urno);
			if(entPaxLoungeMove == null){
				entPaxLoungeMove = new EntDbPaxLoungeMove();
				entPaxLoungeMove.setCreatedUser(HpEKConstants.OPERA_SOURCE);
				entPaxLoungeMove.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
			else{
				oldEnt = new EntDbPaxLoungeMove(entPaxLoungeMove);
				entPaxLoungeMove.setUpdatedUser(HpEKConstants.OPERA_SOURCE);
				entPaxLoungeMove.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
				cmd = UfisASCommands.URT.name();
			}
			entPaxLoungeMove.setPaxRegnKey(_input.getPaxDetails().getRegKey());
			entPaxLoungeMove.setIdFlight(urno);
			entPaxLoungeMove.setIdLoadPax(id_load_pax);
			entPaxLoungeMove.setIdMdLoungeCode(id_md_lounge_code);
			if(_input.getMeta().getMessageTime() != null)
				entPaxLoungeMove.setMsgSentDate(new HpUfisCalendar(
						_input.getMeta().getMessageTime().toGregorianCalendar().getTime()).getTime());
			
			entPaxLoungeMove.setMsgId(new BigDecimal(_input.getMeta().getMessageID()));
			entPaxLoungeMove.setFlightNumber(ffltNo);
			entPaxLoungeMove.setDepNum((_input.getFlightID().getDepNo()== null)? null : String.valueOf(_input.getFlightID().getDepNo()));
			entPaxLoungeMove.setFltDate(_input.getFlightID().getFltDate().toGregorianCalendar().getTime());
			entPaxLoungeMove.setFltOrigin3(_input.getFlightID().getDepStn());
			entPaxLoungeMove.setFltDest3(_input.getFlightID().getArrStn());
			entPaxLoungeMove.setLegNum(String.valueOf(_input.getFlightID().getLegNo()));
			
			entPaxLoungeMove.setPaxRefNum(_input.getPaxDetails().getSeqNo());
			entPaxLoungeMove.setPaxNameRec(_input.getPaxDetails().getPNR());
			entPaxLoungeMove.setPaxName(_input.getPaxDetails().getPaxName());
			entPaxLoungeMove.setPaxFreqFlyerId(_input.getPaxDetails().getPaxFFID());
			entPaxLoungeMove.setPaxFreqFlyerTier(_input.getPaxDetails().getPaxFFTier());
			entPaxLoungeMove.setScanLoungeDate(scanTimeUTC);
			entPaxLoungeMove.setEnterExitFlag(_input.getPaxDetails().getStatus());
			entPaxLoungeMove.setLoungeCode(_input.getPaxDetails().getLoungeCode());
			entPaxLoungeMove.setLoungeClass(_input.getPaxDetails().getLoungeClass());
			entPaxLoungeMove.setGuestFlag(_input.getPaxDetails().getGuestFlag());
			entPaxLoungeMove.setGuestMapkey(_input.getPaxDetails().getGuestMapKey());
			entPaxLoungeMove.setScanUser(_input.getAudit().getScanBy());
			//merge the entity input data
			EntDbPaxLoungeMove entPaxLoungeMoveResult = clsDlPaxLoungeMoveBean.merge(entPaxLoungeMove);
			if(entPaxLoungeMoveResult != null)
				sendNotifyUldInfoToInJson(entPaxLoungeMoveResult, oldEnt, cmd);
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		} catch (ParseException e) {
			LOG.error("DateTime parsing error {}", e.toString());
		}catch(Exception e){
			LOG.error("ERRROR : {}", e.toString());
		}
	return true;
	}
	
	private void sendNotifyUldInfoToInJson(EntDbPaxLoungeMove ent, EntDbPaxLoungeMove oldEnt, String cmd)
			throws IOException, JsonGenerationException, JsonMappingException {
		String urno = ent.getIdFlight().toString();
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setOrig(dtfl);
		List<String> idFlightList = new ArrayList<>();
		idFlightList.add(urno);
		header.setIdFlight(idFlightList);

		// get the BODY ACTION info
		List<String> fldList = new ArrayList<>();
		fldList.add("ID_FLIGHT");
		fldList.add("PAX_REGN_KEY");
		fldList.add("PAX_REF_NUM");
		fldList.add("LOUNGE_CODE");
		fldList.add("SCAN_DATE");
		fldList.add("GUEST_FLAG");
		fldList.add("SCAN_USER");
		
		List<Object> dataList = new ArrayList<>();
		dataList.add(urno);
		dataList.add(ent.getPaxRegnKey());
		dataList.add(ent.getPaxRefNum());
		dataList.add(ent.getLoungeCode());
		dataList.add(df.format(ent.getScanLoungeDate()));
		dataList.add(ent.getGuestFlag());
		dataList.add(ent.getScanUser());
		
		List<Object> oldList = new ArrayList<>();
		if(UfisASCommands.URT.toString().equals(cmd)){
			oldList.add(urno);
			oldList.add(oldEnt.getPaxRegnKey());
			oldList.add(oldEnt.getPaxRefNum());
			oldList.add(oldEnt.getLoungeCode());
			oldList.add(df.format(oldEnt.getScanLoungeDate()));
			oldList.add(oldEnt.getGuestFlag());
			oldList.add(oldEnt.getScanUser());		
		}
			
		List<String> idList = new ArrayList<>();
		idList.add(ent.getId());

		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		action.setCmd(cmd);
		action.setTab("PAX_LOUNGE_MOVE");
		action.setFld(fldList);
		action.setData(dataList);
		action.setOdat(oldList);
		action.setId(idList);
		action.setSel("WHERE ID = \""+ent.getId()+"\"");
		listAction.add(action);

		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		clsBlUfisBCTopic.sendMessage(msg);
		
		LOG.debug("Sent Notify :\n{}", msg);
	}
	
	private String convertFlDateToUTC_Lounge(XMLGregorianCalendar flightDate)
			throws ParseException {
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = flightDate.toGregorianCalendar().getTime();
		return df.format(utcDate);
		/*HpUfisCalendar utcDate = new HpUfisCalendar(flightDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();*/
	}
}
