package com.ufis_as.ufisapp.ek.intf.outgoingmvt;

import java.util.List;

import javax.ejb.EJB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufis_as.ufisapp.ek.mdb.BlUfisTibcoOutgoingMVTMessenger;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgBody;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlOrmtabFacade;

/**
 * @author lma
 *
 */
//@Stateless
public class BlHandleOutgoingMVTBean {
	
	public static final Logger LOG = LoggerFactory.getLogger(BlHandleOutgoingMVTBean.class);
	
	@EJB
    BlUfisTibcoOutgoingMVTMessenger blUfisTibcoOutgoingMVTMessenger;
	
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	
	@EJB
	private BlOrmtabFacade blOrmtabFacade;
	
	public BlHandleOutgoingMVTBean() {
	}

	public boolean process(String message) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			EntUfisMsgDTO msgDTO = mapper.readValue(message, EntUfisMsgDTO.class);
			
			if(msgDTO == null) {
				LOG.error("Wrong message format. The message will be dropped.");
				return false;
			}
			
			EntUfisMsgBody body = msgDTO.getBody();
			List<EntUfisMsgACT> actions = body.getActs();
			
			if(msgDTO == null || body == null || actions == null || actions.isEmpty()) {
				LOG.error("Message body is not provided. The message will be dropped.");
				return false;
			}
			
			String rawMessage = null;
			String xmlMessage = null;
			int index = 0;
			
			//One action only at the moment
			EntUfisMsgACT action = actions.get(0);
			
			for (String fld : action.getFld()) {
				if("RAW".equalsIgnoreCase(fld)) {
					rawMessage = (String) action.getData().get(index);
				}
				
				else if("XML".equalsIgnoreCase(fld)) {
					xmlMessage = (String) action.getData().get(index);
				}
				
				index++;
			}
			
			if(rawMessage == null || xmlMessage == null) {
				LOG.error("Raw Telex Message or Xml Telex Message is not provided. The message will be dropped.");
				return false;
			}
			
			LOG.info("Start sending Raw Telex Message out...");
			blUfisTibcoOutgoingMVTMessenger.sendMessage(rawMessage, "Raw Telex Message");
			
			//FIXME: must be sync with telex gen project xml insertion
			xmlMessage = xmlMessage.replaceAll("99999999", blOrmtabFacade.getOrmNextUrno()+"");
			LOG.info("Start sending XML Telex Message out...");
			blUfisTibcoOutgoingMVTMessenger.sendMessage(xmlMessage, "XML Telex Message");
			
			
			return true;
			
		} catch (Exception ex) {
			LOG.error("Exception occurs !!! {}", ex.getMessage());
			ex.printStackTrace();
			return false;
		}
		
	}
	
}
