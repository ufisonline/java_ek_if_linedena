package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.io.StringWriter;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.exco.TIMEID;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgBody;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;

/**
 * Session Bean implementation class BlConnectionSummaryBean
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlConnectionSummaryBean {
	/**
	 * Logger
	 */
	public static final Logger LOG = LoggerFactory.getLogger(BlConnectionSummaryBean.class);

	/**
	 * CDI
	 */
	@EJB
	private BlUfisCedaQueue clsUfisCedaQ;
	@EJB
	private BlHandleFltConxBean fltConxBean;

	/**
	 * Variables and Constants
	 */
	private JAXBContext _cnxJaxbM;
	private Marshaller _ms;
	//private ACTIONTYPE _msgActionType;

	/**
	 * Default constructor.
	 */
	public BlConnectionSummaryBean() {
		try {
			_cnxJaxbM = JAXBContext.newInstance(MSG.class);
			_ms = _cnxJaxbM.createMarshaller();
		} catch (JAXBException ex) {
			LOG.error("JAXBException when initialize JaxbContext, marshaller and unmarshaller");
		}
	}
	
	/**
	 * Notification message processing(From Ceda for best time change and From
	 * summarizer for connection)
	 * 
	 * @param ufisMsgDTO
	 */
	public void processConxMsg(EntUfisMsgDTO ufisMsgDTO) {
		try {
			long start = System.currentTimeMillis();
			if (ufisMsgDTO != null && ufisMsgDTO.getBody() != null) {
				//EntUfisMsgHead head = ufisMsgDTO.getHead();
				EntUfisMsgBody body = ufisMsgDTO.getBody();
				// currently only action and table are required and only once
				if (body.getActs() != null && body.getActs().size() > 0) {
					EntUfisMsgACT act = body.getActs().get(0);
					//String cmd = act.getCmd();
					String tab = act.getTab(); 
					List<String> fld = act.getFld();
					List<Object> dat = act.getData();

					FltConnectionDTO dto = null;
					if (fld == null || dat == null || fld.size() != dat.size()) {
						LOG.warn("FLD and DAT are required and size should be matched for Alert Processing");
					} else {
						if (tab != null) {
							switch (tab) {
							case HpUfisAppConstants.CON_AFTTAB:
								fltConxBean.processConxStat(ufisMsgDTO);
								break;
							case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
							case HpUfisAppConstants.CON_LOAD_BAG_SUMMARY:
							case HpUfisAppConstants.CON_LOAD_ULD_SUMMARY:
								dto = fltConxBean.handleLoadSummaryMsg(ufisMsgDTO);
								break;
							default:
								LOG.warn("Unsupported table={} encountered", tab);
								break;
							}
						}	
					}
					
					// critical notification(main flight only)
					if (dto != null && dto.getCriticalCount() > 0) {
						sendSummaryToCedaInXmlFormat(tab,
								String.valueOf(dto.getIdFlight()),
								dto.getCriticalCount());
					}
				}
			} else {
				LOG.warn("Message and message body cannot be empty or null");
			}
			LOG.debug("Message Processing Used: {} ms",	(System.currentTimeMillis() - start));
		} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}
	}

	/**
	 * Format and send message to CEDA in xml
	 * @param idFlight
	 * @param criticalCount
	 * @throws JAXBException
	 */
	private void sendSummaryToCedaInXmlFormat(String tab, String idFlight, int criticalCount) {
		try {
			StringWriter writer = new StringWriter();
			_ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			HpUfisCalendar ufisCalendar = new HpUfisCalendar(TimeZone.getTimeZone("UTC"));

			MSG msg = new MSG();
			MSG.MSGSTREAMIN msgstream_in = new MSG.MSGSTREAMIN();

			// HEADER
			INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
			infobjgeneric.setACTIONTYPE(ACTIONTYPE.U);
			infobjgeneric.setMESSAGETYPE(HpEKConstants.JCEDA_SOURCE);
			infobjgeneric.setMESSAGEORIGIN(HpEKConstants.FLT_CONX_SOURCE);
			infobjgeneric.setURNO(idFlight);
			infobjgeneric.setTIMEID(TIMEID.UTC);
			infobjgeneric.setTIMESTAMP(ufisCalendar.getCedaString());

			// BODY
			MSGOBJECTS msgObjects = new MSGOBJECTS();
			INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
			String count = Integer.toString(criticalCount);
			//infobjFlight.setCXPX(cxpx);
			if (HpUfisAppConstants.CON_LOAD_BAG_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXBG(count);
			} else if (HpUfisAppConstants.CON_LOAD_PAX_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXPX(count);
			} else if (HpUfisAppConstants.CON_LOAD_ULD_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXUD(count);
			}
			msgObjects.setINFOBJFLIGHT(infobjFlight);

			// set HEADER and BODY to MSG
			msgstream_in.setINFOBJGENERIC(infobjgeneric);
			msgstream_in.setMSGOBJECTS(msgObjects);
			msg.setMSGSTREAMIN(msgstream_in);

			_ms.marshal(msg, writer);

			// String message = writer.toString();
			// message = message.replace("<INFOBJ_FLIGHT/>",
			// "<INFOBJ_FLIGHT>\n\t\t<CXPX>"+cxpx+"</CXPX>\n\t</INFOBJ_FLIGHT>");
			clsUfisCedaQ.sendMessage(writer.toString());
		} catch(JAXBException jae){
			LOG.error("ERROR when marshalling the msg to xml : {}", jae.getMessage());
		}
	}

}
