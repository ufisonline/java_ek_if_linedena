package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DFR;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.UFR;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxAfttabHandler
 */
@Stateless
public class BlConxAfttabHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxAfttabHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxAfttabHandler() {
    }
    
	public void processConxStatForAfttab(EntUfisMsgDTO ufisMsgDTO){
		try {
			String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
			if(!cmd.equalsIgnoreCase(DFR.name()) && !cmd.equalsIgnoreCase(UFR.name())) {
				LOG.warn("AFTTAB record command is {} but not DFR/UFR. The process won't be continued.", cmd);
				return;
			}
			
			EntDbAfttab temp = cmd.equalsIgnoreCase(DFR.name()) ? validateForDFRAfttab(ufisMsgDTO) : validateForUFRAfttab(ufisMsgDTO);
			EntDbAfttab entAft = dlAfttabBean.find(temp.getUrno()); //FIXME: Performance??? How to work with temp??
			if(entAft == null) {
				LOG.warn("Flight information cannot be retrieved for AFTTAB. The process cannot be continued.");
				return;
			}
			if(entAft.getUrno() != null) {
				if(cmd.equalsIgnoreCase(DFR.name())) {
					computeConxStatForFlightDFR(entAft);
				}
				
				if(cmd.equalsIgnoreCase(UFR.name())) {
					computeConxStatForFlightUFR(entAft);
				}
			}
		} catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
	
	private void computeConxStatForFlightUFR(EntDbAfttab entAft) {
		if(entAft.getFtyp() == null) {
			LOG.warn("FTYP is null/empty for Flight {}. No operation will be performed.", entAft.getUrno());
			return;
		}
		
		if(entAft.getFtyp() == 'T') {
			LOG.warn("FTYP is T for Flight {}. No operation will be performed.", entAft.getUrno());
			return;
		}
		
		if(entAft.getFtyp() == 'X' || entAft.getFtyp() == 'N') {
			makeInvalidFltConnectSummary(entAft);
			return;
		}
		
		if(entAft.getAdid() == 'A') {
			List<FltConnectionDTO> fltConDTOs = dlFltConxSummaryBean.findExistingForArrival(entAft.getUrno());
			if(fltConDTOs.isEmpty()) {
				LOG.warn("No Connection Flight in FltConxSummary for Arrival Flight {}", entAft.getUrno());
			}
			fltConxProcessBean.process(HpUfisAppConstants.CON_AFTTAB, entAft, fltConDTOs);
			return;
		}
		
		if(entAft.getAdid() == 'D') {
			List<FltConnectionDTO> fltConDTOs = dlFltConxSummaryBean.findExistingForDeparture(entAft.getUrno());
			if(fltConDTOs.isEmpty()) {
				LOG.warn("No Connection Flight in FltConxSummary for Departure Flight {}", entAft.getUrno());
			}
			fltConxProcessBean.process(HpUfisAppConstants.CON_AFTTAB, entAft, fltConDTOs);
			return;
		}
		
	}
	
	private void computeConxStatForFlightDFR(EntDbAfttab entAft) {
		if(entAft.getFtyp() == null || HpUfisUtils.isNullOrEmptyStr(entAft.getFtyp()+"")) {
			LOG.warn("FTYP is null or empty for Flight {}.", entAft.getUrno());
			return;
		}
		if(entAft.getFtyp() != 'O' && entAft.getFtyp() != 'S') {
			LOG.warn("FTYP is not 'O' nor 'S' for Flight {} for {} command.", entAft.getUrno(), DFR);
			return;
		}
		
		makeInvalidFltConnectSummary(entAft);
	}
	
	private void makeInvalidFltConnectSummary(EntDbAfttab entAft) {
		List<EntDbFltConnectSummary> fltConxSummaryList = dlFltConxSummaryBean.findExistingForFlights(entAft.getUrno());
		
		for (EntDbFltConnectSummary fltConnectSummary : fltConxSummaryList) {
			fltConnectSummary.setRecStatus("I");
			dlFltConxSummaryBean.merge(fltConnectSummary);
			//FIXMD: what is old data?
			notify(true, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(entAft.getUrno()), null, fltConnectSummary);
			LOG.debug("Notify ULDs(ULD) from SkyChain");
		}		
	}
	
	private void notify(boolean lastRecord, HpUfisAppConstants.UfisASCommands cmd, String idFlight, EntDbFltConnectSummary old, EntDbFltConnectSummary newOne) {
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		}
	}
	
	private EntDbAfttab validateForUFRAfttab(EntUfisMsgDTO ufisMsgDTO) {
		String tab = ufisMsgDTO.getBody().getActs().get(0).getTab().trim();
		
		if(!HpUfisAppConstants.CON_AFTTAB.equalsIgnoreCase(tab)) {
			return null;
		}
		
		String urno = null, adid = null, tifa = null, tifd = null, psta = null, pstd = null, tga1 = null, tgd1 = null, ftyp = null;
		List<String> fldList = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "URNO": urno = (String) data.get(i); break;
				case "ADID" : adid = (String) data.get(i); break;
				case "TIFA" : tifa = (String) data.get(i); break;
				case "TIFD" : tifd = (String) data.get(i); break;
				case "PSTA" : psta = (String) data.get(i); break;
				case "PSTD" : pstd = (String) data.get(i); break;
				case "TGA1" : tga1 = (String) data.get(i); break;
				case "TGD1" : tgd1 = (String) data.get(i); break;
				case "FTYP" : ftyp = (String) data.get(i); break;
			}
			i++;
		}
		
		if(HpUfisUtils.isNullOrEmptyStr(urno) || HpUfisUtils.isNullOrEmptyStr(ftyp) || HpUfisUtils.isNullOrEmptyStr(adid)){
			LOG.debug("Data is missing for (URNO,FTYP,ADID) to perform flight connect summary for AFTTAB.");
			return null;
		}
		
		if(HpUfisUtils.isNullOrEmptyStr(tifa) && HpUfisUtils.isNullOrEmptyStr(tifd)
				&& HpUfisUtils.isNullOrEmptyStr(psta) && HpUfisUtils.isNullOrEmptyStr(pstd)
				&& HpUfisUtils.isNullOrEmptyStr(tga1) && HpUfisUtils.isNullOrEmptyStr(tgd1)){
			LOG.debug("Data is missing for either of (TIFA, TIFD, PSTA, PSTD, TGA1, TGD2) to perform flight connect summary for AFTTAB.");
			return null;
		}
		
		EntDbAfttab entAft = new EntDbAfttab();
		entAft.setUrno(new BigDecimal(urno));
		entAft.setAdid(adid.charAt(0));
		entAft.setTifa(tifa);
		entAft.setTifd(tifd);
		entAft.setPsta(psta);
		entAft.setPstd(pstd);
		entAft.setTga1(HpUfisUtils.isNullOrEmptyStr(tga1) ? null : tga1.charAt(0));
		entAft.setTgd1(HpUfisUtils.isNullOrEmptyStr(tgd1) ? null : tgd1.charAt(0));
		entAft.setFtyp(HpUfisUtils.isNullOrEmptyStr(ftyp) ? null : ftyp.charAt(0));
		return entAft;
	}
	
	private EntDbAfttab validateForDFRAfttab(EntUfisMsgDTO ufisMsgDTO) {
		String tab = ufisMsgDTO.getBody().getActs().get(0).getTab().trim();
		
		if(!HpUfisAppConstants.CON_AFTTAB.equalsIgnoreCase(tab)) {
			return null;
		}
		
		String urno = null, ftyp = null;
		List<String> fldList = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "URNO": urno = (String) data.get(i); break;
				case "FTYP" : ftyp = (String) data.get(i); break;
			}
			i++;
		}
		
		if(HpUfisUtils.isNullOrEmptyStr(urno) || HpUfisUtils.isNullOrEmptyStr(ftyp)){
			LOG.debug("Data is missing for (URNO,FTYP) to perform flight connect summary for AFTTAB.");
			return null;
		}
		
		EntDbAfttab entAft = new EntDbAfttab();
		entAft.setUrno(new BigDecimal(urno));
		entAft.setFtyp(HpUfisUtils.isNullOrEmptyStr(ftyp) ? null : ftyp.charAt(0));
		return entAft;
	}
	
}
