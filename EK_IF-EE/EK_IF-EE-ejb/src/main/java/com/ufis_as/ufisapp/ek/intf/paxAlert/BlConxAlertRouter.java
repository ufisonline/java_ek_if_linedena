package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;

/**
 * Session Bean implementation class BlConxAlertRouter
 */
@Stateless
public class BlConxAlertRouter {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxAlertRouter.class);
	
	@EJB
	private IAfttabBean afttabBean;
	@EJB
	private BlConxHandler conxHandleBean;

    /**
     * Default constructor. 
     */
    public BlConxAlertRouter() {
    	
    }
    
    /**
	 * Route to process record
	 * @param tab
	 * @param idList
	 */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void routeToProcess(String tab, List<?> idList) {
		LOG.debug("Route to process by tab: {}", tab);
		if (tab != null) {
			switch (tab) {
			case HpUfisAppConstants.CON_AFTTAB:
			case HpUfisAppConstants.CON_LOAD_BAG_SUMMARY:
			case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
			case HpUfisAppConstants.CON_LOAD_ULD_SUMMARY:
				if (idList != null && idList.size() > 0) {
					for (Object obj : idList) {
						// according to instance type to set process flight
						EntDbAfttab aftRecord = null;
						// if instance of BigDecimal, then find it from afttab
						if (obj instanceof BigDecimal) {
							BigDecimal idFlight = (BigDecimal) obj;
							// SELECT a
							// FROM EntDbAfttab a
							// where a.urno = :urno
							// AND a.adid = :adid
							// AND a.ftyp not in ('N', 'X' ,'T', 'G')
							aftRecord = afttabBean.findFlightForConx(idFlight);
							if (aftRecord == null) {
								LOG.debug("No afttab record found for urno: {}", idFlight);
								continue;
							}
						}
						// if instance of afttab, then process it directly
						if (obj instanceof EntDbAfttab) {
							aftRecord = (EntDbAfttab) obj;
						}
						
						// process
						if (aftRecord != null) {
							conxHandleBean.fltConxHandle(tab, aftRecord);
						}
					}
				} else {
					LOG.warn("Processing record list cannot null or empty for table: {}", tab);
				}
				break;
			default:
				LOG.warn("Unsupported table: {}", tab);
				break;
			}
		} else {
			LOG.warn("Table name cannot be null for message processing");
		}
	}

}
