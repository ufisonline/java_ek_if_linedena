package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.belt.entities.EntDbLoadBagSummary;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxBagSummaryHandler
 */
@Stateless
public class BlConxBagSummaryHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxBagSummaryHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxBagSummaryHandler() {
    }
    
	public void processConxStatForLoadBagSummary(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadBagSummary record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadBagSummary updatedBagSummary = formNewLoadBagSummary(ufisMsgDTO);
		if(updatedBagSummary == null) {
			LOG.error("BagSummary cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", updatedBagSummary.getIdFlight(), updatedBagSummary.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(updatedBagSummary.getIdFlight() == null || updatedBagSummary.getIdConxFlight() == null ||
				updatedBagSummary.getIdFlight().equals(BigDecimal.ZERO) || updatedBagSummary.getIdConxFlight().equals(BigDecimal.ZERO) ||
				updatedBagSummary.getIdFlight().equals(updatedBagSummary.getIdConxFlight())) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", updatedBagSummary.getIdFlight(), updatedBagSummary.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForLoadBagSummaryIRT(updatedBagSummary);
						break;
			case "DRT": processConxStatForLoadBagSummaryDRT(updatedBagSummary);
						break;
			case "URT": EntDbLoadBagSummary oldΒagSummary = formOldLoadBagSummary(ufisMsgDTO);
						if(oldΒagSummary == null) {
							LOG.error("BagSummary cannot be created with provided data. The process will not be performed.");
							return;
						}
						processConxStatForLoadBagSummaryURT(oldΒagSummary, updatedBagSummary);
						break;
		}

	}

	private void processConxStatForLoadBagSummaryIRT(EntDbLoadBagSummary bagSummary) {
		//if info_type <> 'TXF', do nothing
		if(!bagSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("BagSummary info type is {}. The process cannot be continued.", bagSummary.getInfoType());
			return;
		}
		
		//if info_type = 'TXF' but bag_pcs <= 0, do nothing
		if(bagSummary.getBagPcs().longValue() <= 0) {
			LOG.warn("BagSummary bag pacs count is {}. The process cannot be continued.", bagSummary.getBagPcs());
			return;
		}
		
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(bagSummary.getIdFlight(), bagSummary.getIdConxFlight());
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate Bag connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(bagSummary.getIdFlight());
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(bagSummary.getIdFlight());
			dto.setIdConxFlight(bagSummary.getIdConxFlight());
			dto.setBagPcs(bagSummary.getBagPcs().intValue());
			list.add(dto);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘Bag connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatBag().trim()) || entDbFltConnectSummary.getConxStatBag().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
	
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setBagPcs(bagSummary.getBagPcs().intValue()); //FIXME ??? 
				list.add(dto);
	
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, entAft, list);
			}
		}
		
		//FIXME: If Qty changes from 1 to 2 ??
	}
	
	private void processConxStatForLoadBagSummaryDRT(EntDbLoadBagSummary updatedBagSummary) {
		//if info_type <> 'TXF', do nothing
		if(!updatedBagSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("BagSummary info type is {}. The process cannot be continued.", updatedBagSummary.getInfoType());
			return;
		}
		
		List<EntDbFltConnectSummary> fltConnectSummaryForAllFlights = dlFltConxSummaryBean.findExistingForAllFlights(updatedBagSummary.getIdFlight(), updatedBagSummary.getIdConxFlight());
		if(fltConnectSummaryForAllFlights.isEmpty()) {
			LOG.warn("No FltConnectSummary is found for Load Bag Summary DRT command.");
			return;
		}
		
		for (EntDbFltConnectSummary fltConnectSummary : fltConnectSummaryForAllFlights) {
			fltConnectSummary.setConxStatBag(HpUfisAppConstants.CONX_STAT_NOCONNX);
			if (fltConnectSummary.getConxStatUld().equalsIgnoreCase(
					HpUfisAppConstants.CONX_STAT_NOCONNX)
					&& fltConnectSummary.getConxStatPax().equalsIgnoreCase(
							HpUfisAppConstants.CONX_STAT_NOCONNX)) {
				//if all conx status = 'X' for PAX/BAG/ULD of this conx summary record, mark rec_status = 'X' i.e. delete this record 
				fltConnectSummary.setRecStatus("X");
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				notify(true, HpUfisAppConstants.UfisASCommands.DRT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			} else {
				//otherwise Send "URT" notification msg
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				//FIXME: what is the old data?
				notify(true, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			}
		}
		
		//if no record exist, do nothing
		
	}

	private void processConxStatForLoadBagSummaryURT(EntDbLoadBagSummary oldBagSummary, EntDbLoadBagSummary updatedBagSummary) {
		//if info_type = 'TXF' and bag_pcs change from <= 0 to > 0, (compare "ODATA" and "DATA"), Do same as IRT of LOAD_BAG_SUMMARY
		if (oldBagSummary.getBagPcs().longValue() <= 0 && updatedBagSummary.getBagPcs().longValue() > 0) {
			processConxStatForLoadBagSummaryIRT(updatedBagSummary);
		}
		
		//if info_type = 'TXF' and pag_pcs change from > 0 to <= 0, (compare "ODATA" and "DATA"), Do same as DRT of LOAD_BAG_SUMMARY
		if (oldBagSummary.getBagPcs().longValue() > 0 && updatedBagSummary.getBagPcs().longValue() <= 0) {
			processConxStatForLoadBagSummaryDRT(updatedBagSummary);
		}
	}
	
	private EntDbLoadBagSummary formOldLoadBagSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadBagSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getOdat());
	}
	
	private EntDbLoadBagSummary formNewLoadBagSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadBagSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getData());
	}
	
	private EntDbLoadBagSummary formLoadBagSummary(List<String> fldList, List<Object> data) {
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadBagSummary bagSummary = new EntDbLoadBagSummary();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": bagSummary.setIdFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "ID_CONX_FLIGHT" : bagSummary.setIdConxFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "INFO_TYPE" : bagSummary.setInfoType(data.get(i).toString()); break;
				case "BAG_PCS" : bagSummary.setBagPcs(BigDecimal.valueOf(Float.parseFloat(data.get(i).toString()))); break;
			}
			i++;
		}
		
		return bagSummary;
	}
	
	private void notify(boolean lastRecord, HpUfisAppConstants.UfisASCommands cmd, String idFlight, EntDbFltConnectSummary old, EntDbFltConnectSummary newOne) {
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		}
	}
	
}
