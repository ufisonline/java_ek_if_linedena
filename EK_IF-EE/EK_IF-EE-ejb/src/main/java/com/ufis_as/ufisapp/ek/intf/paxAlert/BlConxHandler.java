package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlLoadBagSummaryBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldSummaryBean;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgBody;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxHandler
 *  * 2014-01-13		BTR		- changed loadPaxSummaryBean query statement to find the conx flights depends on (id_flight or id_conx_flight) 
 * 								if the (id_arr_flight or id_dep_flight) is not exist.   
 */
@Stateless
public class BlConxHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxHandler.class);
	
	@EJB
	private DlLoadBagSummaryBean loadBagSummaryBean;
	@EJB
	private IDlLoadPaxSummaryBeanLocal loadPaxSummaryBean;
	@EJB
	private DlLoadUldSummaryBean loadUldSummaryBean;
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private BlConxAfttabHandler blConxAfttabHandler;

	@EJB
	private BlConxUldSummaryHandler blConxUldSummaryHandler;

	@EJB
	private BlConxBagSummaryHandler blConxBagSummaryHandler;

	@EJB
	private BlConxPaxSummaryHandler blConxPaxSummaryHandler;
	
	@EJB
	private BlConxLoadPaxConnectHandler blConxLoadPaxConnectHandler;
	
	@EJB
	private BlConxLoadULDHandler blConxLoadULDHandler;
	
	@EJB
	private BlConxLoadBagHandler blConxLoadBagHandler;
	
    /**
     * Default constructor. 
     */
    public BlConxHandler() {
    }
    
    /**
	 * Handle record according by table
	 * @param tab
	 * @param aftRecord
	 */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void fltConxHandle(String tab, EntDbAfttab aftRecord) {
		
		LOG.debug("Process by tab: {} for flight: {}", tab, aftRecord.getUrno());

		// if no conx found, flt_conx status update and notify(record change)
		// if conx found for either table, update count and notify(record change and count)
		// only when bag, pax and uld are all no conx found, conx will break
		if (HpUfisAppConstants.CON_AFTTAB.equals(tab) 
				|| HpUfisAppConstants.CON_LOAD_BAG_SUMMARY.equals(tab)) {
			// SELECT DISTINCT a.idConxFlight 
			// FROM EntDbLoadBagSummary a 
			// WHERE a.idFlight =:idFlight 
			// AND a.infoType = :infoType 
			// AND a.recStatus != 'X' 
			LOG.info("Query Conx flight in tab: {}", HpUfisAppConstants.CON_LOAD_BAG_SUMMARY);
			List<FltConnectionDTO> conxLoadBags = loadBagSummaryBean.findIdConxFlight(aftRecord.getUrno());
			if (conxLoadBags != null && conxLoadBags.size() > 0) {
				LOG.debug("{} conx flights found", conxLoadBags.size());
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, aftRecord, conxLoadBags);
			} else {
				LOG.debug("No conx flights found for flight: {} in tab: {}",
						aftRecord.getUrno(),
						HpUfisAppConstants.CON_LOAD_BAG_SUMMARY);
			}
		}
		
		if (HpUfisAppConstants.CON_AFTTAB.equals(tab) 
				|| HpUfisAppConstants.CON_LOAD_PAX_SUMMARY.equals(tab)) {
			// SELECT DISTINCT a.idConxFlight 
			// FROM EntDbLoadPaxSummary a 
			// WHERE a.infoType = :infoType
			// AND a.idArrFlight =:idFlight/a.idDepFlight =:idFlight(depends on adid to take one) 
			// AND a.recStatus != 'X' 
			LOG.info("Query Conx flight in tab: {}", HpUfisAppConstants.CON_LOAD_PAX_SUMMARY);
			List<FltConnectionDTO> conxLoadPaxes = loadPaxSummaryBean.findConxFlightById(aftRecord.getUrno());
			if (conxLoadPaxes != null && conxLoadPaxes.size() > 0) {
				LOG.debug("{} conx flights found", conxLoadPaxes.size());
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, aftRecord, conxLoadPaxes);
			} else {
				LOG.debug("No conx flights found for flight: {} in tab: {}",
						aftRecord.getUrno(),
						HpUfisAppConstants.CON_LOAD_PAX_SUMMARY);
			}
		}
		
		if (HpUfisAppConstants.CON_AFTTAB.equals(tab) 
				|| HpUfisAppConstants.CON_LOAD_ULD_SUMMARY.equals(tab)) {
			// SELECT DISTINCT a.idConxFlight 
			// FROM EntDbLoadUldSummary a 
			// WHERE a.idFlight =:idFlight 
			// AND a.infoType = :infoType 
			// AND a.recStatus != 'X' 
			LOG.info("Query Conx flight in tab: {}", HpUfisAppConstants.CON_LOAD_ULD_SUMMARY);
			List<FltConnectionDTO> conxLoadUlds = loadUldSummaryBean.findIdConxFlight(aftRecord.getUrno());
			if (conxLoadUlds != null && conxLoadUlds.size() > 0) {
				LOG.debug("{} conx flights found", conxLoadUlds.size());
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, aftRecord, conxLoadUlds);
			} else {
				LOG.debug("No conx flights found for flight: {} in tab: {}",
						aftRecord.getUrno(),
						HpUfisAppConstants.CON_LOAD_ULD_SUMMARY);
			}
		}
		
	}

	public void processConxMsg(EntUfisMsgDTO ufisMsgDTO) {
		try {
			long start = System.currentTimeMillis();
			
			if(ufisMsgDTO == null || ufisMsgDTO.getBody() == null) {
				LOG.warn("Message and message body cannot be empty or null");
				return;
			}
			
			EntUfisMsgBody body = ufisMsgDTO.getBody();
			
			if(body.getActs() == null && body.getActs().isEmpty()) {
				LOG.warn("Message Action cannot be empty or null");
				return;
			}
			
			EntUfisMsgACT act = body.getActs().get(0);
			String tab = act.getTab(); 
			String cmd = act.getCmd();
			List<String> fld = act.getFld();
			List<Object> dat = act.getData();

			if (fld == null || dat == null || fld.size() != dat.size()) {
				LOG.warn("FLD and DAT are required and size should be matched for Alert Processing");
				return;
			}
			
			if (HpUfisUtils.isNullOrEmptyStr(cmd)) {
				LOG.warn("Command must not be empty.");
				return;
			}
			
			if (HpUfisUtils.isNullOrEmptyStr(tab)) {
				LOG.warn("Table name must not be empty.");
				return;
			}
			
			switch (tab.toUpperCase()) {
			case HpUfisAppConstants.CON_AFTTAB:
				blConxAfttabHandler.processConxStatForAfttab(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_ULD_SUMMARY:
				blConxUldSummaryHandler.processConxStatForLoadULDSummary(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_BAG_SUMMARY:
				blConxBagSummaryHandler.processConxStatForLoadBagSummary(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
				blConxPaxSummaryHandler.processConxStatForLoadPaxSummary(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_ULDS:
				blConxLoadULDHandler.processConxStatForLoadUld(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_PAX_CONNECT:
				blConxLoadPaxConnectHandler.processConxStatForLoadPaxConnect(ufisMsgDTO);
				break;
			case HpUfisAppConstants.CON_LOAD_BAG:
				blConxLoadBagHandler.processConxStatForLoadBag(ufisMsgDTO);
				break;
			default:
				LOG.warn("Unsupported table={} encountered", tab);
				break;
			}
			LOG.debug("Message Processing Used: {} ms",	(System.currentTimeMillis() - start));
		} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}
	}				
}
