package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxLoadBagHandler
 */
@Stateless
public class BlConxLoadBagHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxLoadBagHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxLoadBagHandler() {
    }
    
    public void processConxStatForLoadBag(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadBag record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadBag loadBag = formLoadBag(ufisMsgDTO);
		if(loadBag == null) {
			LOG.error("LoadBag cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", loadBag.getId_Flight(), loadBag.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(loadBag.getId_Flight() == null || loadBag.getIdConxFlight() == null ||
				loadBag.getId_Flight().equals(BigDecimal.ZERO) || loadBag.getIdConxFlight().equals(BigDecimal.ZERO) ||
				loadBag.getId_Flight().equals(loadBag.getIdConxFlight())) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", loadBag.getId_Flight(), loadBag.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForIRT(loadBag);
						break;
			case "URT": processConxStatForURT(loadBag);
						break;
		}
		
    }
    
	private void processConxStatForIRT(EntDbLoadBag loadBag) {
		//Find FLT_CONNECT_SUMMARY with no conxflights (status is 'X' or status = ' ') Which (findExistingWithNoConxFlights-DB Query) handle the situation of "If Qty changes from 1 to 2" where no change in fltconxsummary is required????
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		BigDecimal idFlight = loadBag.getId_Flight();
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(idFlight, loadBag.getIdConxFlight());
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate Bag connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(idFlight);
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(idFlight);
			dto.setIdConxFlight(loadBag.getIdConxFlight());
			dto.setBagPcs(1);
			list.add(dto);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘Bag connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatBag().trim()) || entDbFltConnectSummary.getConxStatBag().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
	
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setBagPcs(1); //FIXME ??? 
				list.add(dto);
	
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, entAft, list);
			}
		}
		
	}
	
	private void processConxStatForURT(EntDbLoadBag loadBag) {
		processConxStatForIRT(loadBag);
	}

	private EntDbLoadBag formLoadBag(EntUfisMsgDTO ufisMsgDTO) {
		List<String> fldList = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadBag loadBag = new EntDbLoadBag();
		
		int i = 0;
		for (String fld : fldList) {
			Object eachData = data.get(i);
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": loadBag.setId_Flight(eachData == null ? BigDecimal.ZERO : BigDecimal.valueOf(Long.parseLong(eachData.toString()))); break;
				case "ID_CONX_FLIGHT" : loadBag.setIdConxFlight(eachData == null ? BigDecimal.ZERO : BigDecimal.valueOf(Long.parseLong(eachData.toString()))); break;
			}
			i++;
		}
		
		return loadBag;
	}
	
}
