package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxLoadPaxConnectHandler
 */
@Stateless
public class BlConxLoadPaxConnectHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxLoadPaxConnectHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxLoadPaxConnectHandler() {
    }
    
    public void processConxStatForLoadPaxConnect(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadPaxConnect record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadPaxConn loadPaxConnect = formLoadPaxConnect(ufisMsgDTO);
		if(loadPaxConnect == null) {
			LOG.error("LoadPaxConnect cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", loadPaxConnect.getIdFlight(), loadPaxConnect.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(loadPaxConnect.getIdFlight() == 0 || loadPaxConnect.getIdConxFlight() == 0 ||
				loadPaxConnect.getIdFlight() == loadPaxConnect.getIdConxFlight()) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", loadPaxConnect.getIdFlight(), loadPaxConnect.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForIRT(loadPaxConnect);
						break;
			case "URT": processConxStatForURT(loadPaxConnect);
						break;
		}
		
    }
    
	private void processConxStatForIRT(EntDbLoadPaxConn loadPaxConnect) {
		//Find FLT_CONNECT_SUMMARY with no conxflights (status is 'X' or status = ' ') Which (findExistingWithNoConxFlights-DB Query) handle the situation of "If Qty changes from 1 to 2" where no change in fltconxsummary is required????
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		BigDecimal idFlight = BigDecimal.valueOf(loadPaxConnect.getIdFlight());
		BigDecimal idConxFlight = BigDecimal.valueOf(loadPaxConnect.getIdConxFlight());
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(idFlight, idConxFlight);
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate Pax connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(idFlight);
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(idFlight);
			dto.setIdConxFlight(idConxFlight);
			dto.setTotalPax(1);
			list.add(dto);
			
			LOG.info("NO FLT CONX SUMMARY => id_flight[{}, {}], id_conx_flight[{}]", idFlight, entAft.getUrno(), idConxFlight);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘PAX connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatPax().trim()) || entDbFltConnectSummary.getConxStatPax().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
	
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setTotalPax(1); 
				list.add(dto);
				
				LOG.info("FLT CONX SUMMARY => id_flight[{}, {}], id_conx_flight[{}]", entDbFltConnectSummary.getIdArrFlight(), entAft.getUrno(), entDbFltConnectSummary.getIdDepFlight());
	
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, entAft, list);
			}
		}
		
	}
	
	private void processConxStatForURT(EntDbLoadPaxConn loadPaxConnect) {
		processConxStatForIRT(loadPaxConnect);
	}

	private EntDbLoadPaxConn formLoadPaxConnect(EntUfisMsgDTO ufisMsgDTO) {
		List<String> fldList = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadPaxConn loadPaxConnect = new EntDbLoadPaxConn();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": loadPaxConnect.setIdFlight(Integer.parseInt(data.get(i).toString())); break;
				case "ID_CONX_FLIGHT" : loadPaxConnect.setIdConxFlight(Integer.parseInt(data.get(i).toString())); break;
			}
			i++;
		}
		
		return loadPaxConnect;
	}
	
}
