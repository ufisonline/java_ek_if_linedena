package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxLoadULDHandler
 */
@Stateless
public class BlConxLoadULDHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxLoadULDHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxLoadULDHandler() {
    }
    
    public void processConxStatForLoadUld(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadUld record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadUld loadUld = formLoadUld(ufisMsgDTO);
		if(loadUld == null) {
			LOG.error("LoadUld cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", loadUld.getIdFlight(), loadUld.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(loadUld.getIdFlight() == null || loadUld.getIdConxFlight() == null ||
				loadUld.getIdFlight().equals(BigDecimal.ZERO) || loadUld.getIdConxFlight().equals(BigDecimal.ZERO) ||
				loadUld.getIdFlight().equals(loadUld.getIdConxFlight())) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", loadUld.getIdFlight(), loadUld.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForIRT(loadUld);
						break;
			case "URT": processConxStatForURT(loadUld);
						break;
		}
		
    }
    
	private void processConxStatForIRT(EntDbLoadUld loadUld) {
		//Find FLT_CONNECT_SUMMARY with no conxflights (status is 'X' or status = ' ') Which (findExistingWithNoConxFlights-DB Query) handle the situation of "If Qty changes from 1 to 2" where no change in fltconxsummary is required????
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		BigDecimal idFlight = loadUld.getIdFlight();
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(idFlight, loadUld.getIdConxFlight());
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate ULD connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(loadUld.getIdFlight());
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(idFlight);
			dto.setIdConxFlight(loadUld.getIdConxFlight());
			dto.setUldPcs(1);
			list.add(dto);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘ULD connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatUld().trim()) || entDbFltConnectSummary.getConxStatUld().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
				
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setUldPcs(1); //FIXME ??? 
				list.add(dto);
				
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, entAft, list);
			}
		}
	}
	
	private void processConxStatForURT(EntDbLoadUld loadUld) {
		processConxStatForIRT(loadUld);
	}

	private EntDbLoadUld formLoadUld(EntUfisMsgDTO ufisMsgDTO) {
		List<String> fldList = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadUld loadUld = new EntDbLoadUld();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": loadUld.setIdFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "ID_CONX_FLIGHT" : loadUld.setIdConxFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
			}
			i++;
		}
		
		return loadUld;
	}
	
}
