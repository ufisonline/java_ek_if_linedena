package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxPaxSummaryHandler
 */
@Stateless
public class BlConxPaxSummaryHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxPaxSummaryHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxPaxSummaryHandler() {
    }
    
	public void processConxStatForLoadPaxSummary(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadPaxSummary record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadPaxSummary updatedPaxSummary = formNewLoadPaxSummary(ufisMsgDTO);
		if(updatedPaxSummary == null) {
			LOG.error("PaxSummary cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", updatedPaxSummary.getIdFlight(), updatedPaxSummary.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(updatedPaxSummary.getIdFlight() == null || updatedPaxSummary.getIdConxFlight() == null ||
				updatedPaxSummary.getIdFlight().equals(BigDecimal.ZERO) || updatedPaxSummary.getIdConxFlight().equals(BigDecimal.ZERO) ||
				updatedPaxSummary.getIdFlight().equals(updatedPaxSummary.getIdConxFlight())) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", updatedPaxSummary.getIdFlight(), updatedPaxSummary.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForLoadPaxSummaryIRT(updatedPaxSummary);
						break;
			case "DRT": processConxStatForLoadPaxSummaryDRT(updatedPaxSummary);
						break;
			case "URT": EntDbLoadPaxSummary oldPaxSummary = formOldLoadPaxSummary(ufisMsgDTO);
						if(oldPaxSummary == null) {
							LOG.error("PaxSummary cannot be created with provided data. The process will not be performed.");
							return;
						}
						processConxStatForLoadPaxSummaryURT(oldPaxSummary, updatedPaxSummary);
						break;
		}

	}

	private void processConxStatForLoadPaxSummaryIRT(EntDbLoadPaxSummary paxSummary) {
		//if info_type <> 'TXF', do nothing
		if(!paxSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("PaxSummary info type is {}. The process cannot be continued.", paxSummary.getInfoType());
			return;
		}
		
		//if info_type = 'TXF' but pax_pcs <= 0, do nothing
		if(paxSummary.getTotalPax().intValue() <= 0) {
			LOG.warn("PaxSummary total pacs count is {}. The process cannot be continued.", paxSummary.getTotalPax());
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with no conxflights (status is 'X' or status = ' ') Which (findExistingWithNoConxFlights-DB Query) handle the situation of "If Qty changes from 1 to 2" where no change in fltconxsummary is required????
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(paxSummary.getIdFlight(), paxSummary.getIdConxFlight());
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘Pax connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate Pax connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(paxSummary.getIdFlight());
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(paxSummary.getIdFlight());
			dto.setIdConxFlight(paxSummary.getIdConxFlight());
			dto.setTotalPax(paxSummary.getTotalPax().intValue());
			list.add(dto);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with no conxflights (status is 'X' or status = ' ') and Record exists / evaluate and update PAX conn status 
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatPax().trim()) || entDbFltConnectSummary.getConxStatPax().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
	
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setTotalPax(paxSummary.getTotalPax().intValue()); //FIXME ??? 
				list.add(dto);
	
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, entAft, list);
			}
		}
		
		//FIXME: If Qty changes from 1 to 2 ??
	}
	
	private void processConxStatForLoadPaxSummaryDRT(EntDbLoadPaxSummary updatedPaxSummary) {
		//if info_type <> 'TXF', do nothing
		if(!updatedPaxSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("PaxSummary info type is {}. The process cannot be continued.", updatedPaxSummary.getInfoType());
			return;
		}
		
		List<EntDbFltConnectSummary> fltConnectSummaryForAllFlights = dlFltConxSummaryBean.findExistingForAllFlights(updatedPaxSummary.getIdFlight(), updatedPaxSummary.getIdConxFlight());
		if(fltConnectSummaryForAllFlights.isEmpty()) {
			LOG.warn("No FltConnectSummary is found for Load Pax Summary DRT command.");
			return;
		}
		
		for (EntDbFltConnectSummary fltConnectSummary : fltConnectSummaryForAllFlights) {
			fltConnectSummary.setConxStatPax(HpUfisAppConstants.CONX_STAT_NOCONNX);
			if (fltConnectSummary.getConxStatBag().equalsIgnoreCase(
					HpUfisAppConstants.CONX_STAT_NOCONNX)
					&& fltConnectSummary.getConxStatUld().equalsIgnoreCase(
							HpUfisAppConstants.CONX_STAT_NOCONNX)) {
				//if all conx status = 'X' for PAX/BAG/ULD of this conx summary record, mark rec_status = 'X' i.e. delete this record 
				fltConnectSummary.setRecStatus("X");
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				notify(true, HpUfisAppConstants.UfisASCommands.DRT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			} else {
				//otherwise Send "URT" notification msg
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				//FIXME: what is the old data?
				notify(true, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			}
		}
		
		//if no record exist, do nothing
		
	}

	private void processConxStatForLoadPaxSummaryURT(EntDbLoadPaxSummary oldPaxSummary, EntDbLoadPaxSummary updatedPaxSummary) {
		//if info_type = 'TXF' and total pax change from <= 0 to > 0, (compare "ODATA" and "DATA"), Do same as IRT of LOAD_PAX_SUMMARY
		if (oldPaxSummary.getTotalPax().intValue() <= 0 && updatedPaxSummary.getTotalPax().intValue() > 0) {
			processConxStatForLoadPaxSummaryIRT(updatedPaxSummary);
		}
		
		//if info_type = 'TXF' and total pax change from > 0 to <= 0, (compare "ODATA" and "DATA"), Do same as DRT of LOAD_PAX_SUMMARY
		if (oldPaxSummary.getTotalPax().intValue() > 0 && updatedPaxSummary.getTotalPax().intValue() <= 0) {
			processConxStatForLoadPaxSummaryDRT(updatedPaxSummary);
		}
	}
	
	private EntDbLoadPaxSummary formOldLoadPaxSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadPaxSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getOdat());
	}
	
	private EntDbLoadPaxSummary formNewLoadPaxSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadPaxSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getData());
	}
	
	private EntDbLoadPaxSummary formLoadPaxSummary(List<String> fldList, List<Object> data) {
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadPaxSummary paxSummary = new EntDbLoadPaxSummary();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": paxSummary.setIdFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "ID_CONX_FLIGHT" : paxSummary.setIdConxFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "INFO_TYPE" : paxSummary.setInfoType(data.get(i).toString()); break;
				case "TOTAL_PAX" : paxSummary.setTotalPax(BigDecimal.valueOf(Float.parseFloat(data.get(i).toString()))); break;
			}
			i++;
		}
		
		return paxSummary;
	}
	
	private void notify(boolean lastRecord, HpUfisAppConstants.UfisASCommands cmd, String idFlight, EntDbFltConnectSummary old, EntDbFltConnectSummary newOne) {
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		}
	}
	
}
