package com.ufis_as.ufisapp.ek.intf.paxAlert;

import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.DRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.IRT;
import static com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands.URT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUldSummary;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Session Bean implementation class BlConxUldSummaryHandler
 */
@Stateless
public class BlConxUldSummaryHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlConxUldSummaryHandler.class);
	
	@EJB
	private BlFltConxProcess fltConxProcessBean;
	
	@EJB
	private IAfttabBeanLocal dlAfttabBean;
	
	@EJB
	private DlFltConxSummaryBean dlFltConxSummaryBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
    /**
     * Default constructor. 
     */
    public BlConxUldSummaryHandler() {
    }
    
	public void processConxStatForLoadULDSummary(EntUfisMsgDTO ufisMsgDTO) {
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		if(!cmd.equalsIgnoreCase(IRT.name()) && !cmd.equalsIgnoreCase(DRT.name()) && !cmd.equalsIgnoreCase(URT.name())) {
			LOG.warn("LoadULDSummary record command is {} but not IRT/DRT/URT. The process won't be continued.", cmd);
			return;
		}
		
		EntDbLoadUldSummary updatedUldSummary = formNewLoadUldSummary(ufisMsgDTO);
		if(updatedUldSummary == null) {
			LOG.error("ULDSummary cannot be created with provided data. The process will not be performed.");
			return;
		}
		
		LOG.info("ID_FLIGHT : {}, ID_CONX_FLIGHT : {}", updatedUldSummary.getIdFlight(), updatedUldSummary.getIdConxFlight());
		
		//To process only when both id_flight and id_conx_flight have value.
		if(updatedUldSummary.getIdFlight() == null || updatedUldSummary.getIdConxFlight() == null ||
				updatedUldSummary.getIdFlight().equals(BigDecimal.ZERO) || updatedUldSummary.getIdConxFlight().equals(BigDecimal.ZERO) ||
				updatedUldSummary.getIdFlight().equals(updatedUldSummary.getIdConxFlight())) {
			LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must be provided and not be the same. Otherwise the process will not be performed.", updatedUldSummary.getIdFlight(), updatedUldSummary.getIdConxFlight());
			return;
		}
		
		switch(cmd) {
			case "IRT": processConxStatForLoadULDSummaryIRT(updatedUldSummary);
						break;
			case "DRT": processConxStatForLoadULDSummaryDRT(updatedUldSummary);
						break;
			case "URT": EntDbLoadUldSummary oldUldSummary = formOldLoadUldSummary(ufisMsgDTO);
						if(oldUldSummary == null) {
							LOG.error("ULDSummary cannot be created with provided data. The process will not be performed.");
							return;
						}
						processConxStatForLoadULDSummaryURT(oldUldSummary, updatedUldSummary);
						break;
		}

	}

	private void processConxStatForLoadULDSummaryIRT(EntDbLoadUldSummary uldSummary) {
		//if info_type <> 'TXF', do nothing
		if(!uldSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("UldSummary info type is {}. The process cannot be continued.", uldSummary.getInfoType());
			return;
		}
		
		//if info_type = 'TXF' but uld_pcs <= 0, do nothing
		if(uldSummary.getUldPcs() <= 0) {
			LOG.warn("UldSummary uld pacs count is {}. The process cannot be continued.", uldSummary.getUldPcs());
			return;
		}
		
		List<EntDbFltConnectSummary> fltConnectSummaryList = dlFltConxSummaryBean.findExistingForAllFlights(uldSummary.getIdFlight(), uldSummary.getIdConxFlight());
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record doesn't exist / evaluate ULD connection status 
		if(fltConnectSummaryList.isEmpty()) {
			EntDbAfttab entAft = dlAfttabBean.find(uldSummary.getIdFlight());
			
			List<FltConnectionDTO> list = new ArrayList<>();
			FltConnectionDTO dto = new FltConnectionDTO();
			dto.setIdFlight(uldSummary.getIdFlight());
			dto.setIdConxFlight(uldSummary.getIdConxFlight());
			dto.setUldPcs((int) uldSummary.getUldPcs());
			list.add(dto);
			
			fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, entAft, list);
			
			return;
		}
		
		//Find FLT_CONNECT_SUMMARY with conxflights and record exists.
		//If ‘ULD connection status’ is blank or ‘no connection’, do processing. Otherwise skip
		//Result is at most 2 - idFlight/arr and idConxFlight/dep OR idConxFlight/arr and idFlight/dep
		for (EntDbFltConnectSummary entDbFltConnectSummary : fltConnectSummaryList) {
			if(HpUfisUtils.isNullOrEmptyStr(entDbFltConnectSummary.getConxStatUld().trim()) || entDbFltConnectSummary.getConxStatUld().trim().equalsIgnoreCase("X")) {
				EntDbAfttab entAft = dlAfttabBean.find(entDbFltConnectSummary.getIdArrFlight());
	
				List<FltConnectionDTO> list = new ArrayList<>();
				FltConnectionDTO dto = new FltConnectionDTO();
				dto.setIdFlight(entDbFltConnectSummary.getIdArrFlight());
				dto.setIdConxFlight(entDbFltConnectSummary.getIdDepFlight());
				dto.setUldPcs((int) uldSummary.getUldPcs()); //FIXME ??? 
				list.add(dto);
	
				//One pair only
				fltConxProcessBean.process(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, entAft, list);
			}
		}
		
		//FIXME: If Qty changes from 1 to 2 ??
	}
	
	private void processConxStatForLoadULDSummaryDRT(EntDbLoadUldSummary updatedUldSummary) {
		//if info_type <> 'TXF', do nothing
		if(!updatedUldSummary.getInfoType().equalsIgnoreCase(HpUfisAppConstants.INFO_TYPE_TRANSFER)) {
			LOG.warn("UldSummary info type is {}. The process cannot be continued.", updatedUldSummary.getInfoType());
			return;
		}
		
		List<EntDbFltConnectSummary> fltConnectSummaryForAllFlights = dlFltConxSummaryBean.findExistingForAllFlights(updatedUldSummary.getIdFlight(), updatedUldSummary.getIdConxFlight());
		if(fltConnectSummaryForAllFlights.isEmpty()) {
			LOG.warn("No FltConnectSummary is found for Load ULD Summary DRT command.");
			return;
		}
		
		for (EntDbFltConnectSummary fltConnectSummary : fltConnectSummaryForAllFlights) {
			fltConnectSummary.setConxStatUld(HpUfisAppConstants.CONX_STAT_NOCONNX);
			if (fltConnectSummary.getConxStatBag().equalsIgnoreCase(
					HpUfisAppConstants.CONX_STAT_NOCONNX)
					&& fltConnectSummary.getConxStatPax().equalsIgnoreCase(
							HpUfisAppConstants.CONX_STAT_NOCONNX)) {
				//if all conx status = 'X' for PAX/BAG/ULD of this conx summary record, mark rec_status = 'X' i.e. delete this record 
				fltConnectSummary.setRecStatus("X");
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				notify(true, HpUfisAppConstants.UfisASCommands.DRT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			} else {
				//otherwise Send "URT" notification msg
				fltConnectSummary = dlFltConxSummaryBean.merge(fltConnectSummary);
				//FIXME: what is the old data?
				notify(true, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(fltConnectSummary.getIdArrFlight()), null, fltConnectSummary);
			}
		}
		
		//if no record exist, do nothing
		
	}

	private void processConxStatForLoadULDSummaryURT(EntDbLoadUldSummary oldUldSummary, EntDbLoadUldSummary updatedUldSummary) {
		//if info_type = 'TXF' and uld_pcs change from <= 0 to > 0, (compare "ODATA" and "DATA"), Do same as IRT of LOAD_ULD_SUMMARY
		if (oldUldSummary.getUldPcs() <= 0 && updatedUldSummary.getUldPcs() > 0) {
			processConxStatForLoadULDSummaryIRT(updatedUldSummary);
		}
		
		//if info_type = 'TXF' and uld_pcs change from > 0 to <= 0, (compare "ODATA" and "DATA"), Do same as DRT of LOAD_ULD_SUMMARY
		if (oldUldSummary.getUldPcs() > 0 && updatedUldSummary.getUldPcs() <= 0) {
			processConxStatForLoadULDSummaryDRT(updatedUldSummary);
		}
	}
	
	private EntDbLoadUldSummary formOldLoadUldSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadUldSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getOdat());
	}
	
	private EntDbLoadUldSummary formNewLoadUldSummary(EntUfisMsgDTO ufisMsgDTO) {
		return formLoadUldSummary(ufisMsgDTO.getBody().getActs().get(0).getFld(), ufisMsgDTO.getBody().getActs().get(0).getData());
	}
	
	private EntDbLoadUldSummary formLoadUldSummary(List<String> fldList, List<Object> data) {
		if(fldList.size() != data.size()) {
			LOG.error("Field list size and data list size are not equal. The process will not be performed.");
			return null;
		}
		
		EntDbLoadUldSummary uldSummary = new EntDbLoadUldSummary();
		
		int i = 0;
		for (String fld : fldList) {
			switch(fld.toUpperCase()){
				case "ID_FLIGHT": uldSummary.setIdFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "ID_CONX_FLIGHT" : uldSummary.setIdConxFlight(BigDecimal.valueOf(Long.parseLong(data.get(i).toString()))); break;
				case "INFO_TYPE" : uldSummary.setInfoType(data.get(i).toString()); break;
				case "ULD_PCS" : uldSummary.setUldPcs(Float.parseFloat(data.get(i).toString())); break;
			}
			i++;
		}
		
		return uldSummary;
	}
	
	private void notify(boolean lastRecord, HpUfisAppConstants.UfisASCommands cmd, String idFlight, EntDbFltConnectSummary old, EntDbFltConnectSummary newOne) {
		if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
			// send notification message to topic
			ufisTopicProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		} else {
			// if topic not defined, send notification to queue
			ufisQueueProducer.sendNotification(lastRecord, cmd, idFlight, old, newOne);
		}
	}
	
}
