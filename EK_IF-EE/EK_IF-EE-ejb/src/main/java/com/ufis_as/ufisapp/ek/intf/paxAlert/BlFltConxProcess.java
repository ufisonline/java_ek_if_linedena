package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.journeymatrix.EntDbMdJourneyMatrix;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.exco.TIMEID;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.eao.DlMdJourneyMatrix;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;

/**
 * Session Bean implementation class BlFltConxProcess
 */
@Stateless
public class BlFltConxProcess {
	
	private static final Logger LOG = LoggerFactory.getLogger(BlFltConxProcess.class);
	
	@EJB
	private IAfttabBean afttabBean;
	@EJB
	private DlFltConxSummaryBean fltConnectSummaryBean;
	@EJB
	private DlMdJourneyMatrix journeyMatrixBean;
	@EJB
    private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	@EJB
	private BlUfisCedaQueue clsUfisCedaQ;
	
	// ADID
    private static final Character ADID_ARRIVAL = 'A';
    private static final Character ADID_DEPARTURE = 'D';
    
	//private static ACTIONTYPE _msgActionType;
    private static Marshaller _ms;
    private static JAXBContext _cnxJaxbM;
	
    /**
     * Default constructor. 
     */
    public BlFltConxProcess() {
    }
    
    @PostConstruct
	private void initialize(){
		try {
            _cnxJaxbM = JAXBContext.newInstance(MSG.class);
            _ms = _cnxJaxbM.createMarshaller();
        } catch (JAXBException ex) {
            LOG.error("JAXBException when initialize JaxbContext, marshaller and unmarshaller");
        }
	}
    
    /**
	 * Process bag, pax and uld conx for main flight and notify accordingly
	 * @param tab
	 * @param mainFlt
	 * @param idConxFlights
	 */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void process(String tab, EntDbAfttab mainFlt, List<FltConnectionDTO> idConxFlights) {
		try {
			// Critical and Short count for main flight
			int shortCount = 0;
			int criticalCount = 0;
			for (Object obj : idConxFlights) {
				FltConnectionDTO dto = (FltConnectionDTO) obj;
				LOG.debug("Main Flt : {}, Conx Flt : {}", mainFlt.getUrno(), dto.getIdConxFlight());
				if(mainFlt.getUrno().equals(dto.getIdConxFlight())) {
					LOG.error("ID_FLIGHT[{}] and ID_CONX_FLIGHT[{}] must not be the same. Otherwise the process will not be performed.", mainFlt.getUrno(), dto.getIdConxFlight());
					return;
				}
				if (dto.getIdConxFlight().intValue() != 0) {
					EntDbAfttab conxFlight = afttabBean.findFlightForConx(dto.getIdConxFlight());
					if (conxFlight != null) {
						// Main flight and conx flight must be either arrival or departure flight
					    if (ADID_ARRIVAL != mainFlt.getAdid() && ADID_DEPARTURE != mainFlt.getAdid()) {
					    	LOG.warn("Main flight must be arrival or departure flight");
					    	LOG.warn("ADID={} for main flight={}", mainFlt.getAdid(), mainFlt.getUrno());
					    	continue;
					    }
					    if (ADID_ARRIVAL != conxFlight.getAdid() && ADID_DEPARTURE != conxFlight.getAdid()) {
					    	LOG.warn("Conx flight must be arrival or departure flight");
					    	LOG.warn("ADID={} for conx flight={}", conxFlight.getAdid(), conxFlight.getUrno());
					    	continue;
					    }
					    
					    // Calculate the time span between main and connect flight
					    BigDecimal idArrFlight = null;
					    BigDecimal idDepFlight = null;
					    HpUfisCalendar tifa = null;
					    HpUfisCalendar tifd = null;
					    char tga1 = ' ';
					    char tgd1 = ' ';
					    if (ADID_ARRIVAL == mainFlt.getAdid()) {
					    	tifa = new HpUfisCalendar(mainFlt.getTifa());
					    	tifd = new HpUfisCalendar(conxFlight.getTifd());
					    	idArrFlight = mainFlt.getUrno();
					    	idDepFlight = conxFlight.getUrno();
					    	tga1 = mainFlt.getTga1();
					    	tgd1 = conxFlight.getTgd1();
					    } else {
					    	tifa = new HpUfisCalendar(conxFlight.getTifa());
					    	tifd = new HpUfisCalendar(mainFlt.getTifd());
					    	idArrFlight = conxFlight.getUrno();
					    	idDepFlight = mainFlt.getUrno();
					    	tga1 = conxFlight.getTgd1();
					    	tgd1 = mainFlt.getTga1();
					    } 
					    int timeDiff = (int) tifa.timeDiff(tifd, EnumTimeInterval.Minutes, false);
					    LOG.debug("Time Difference between main flight and conx flight: {} mins", timeDiff);
					    
					    // check bag/pax/uld number for transfer
					    int transferNum = 0;
						switch (tab) {
						case HpUfisAppConstants.CON_LOAD_BAG_SUMMARY:
							transferNum = dto.getBagPcs();
							break;
						case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
							transferNum = dto.getTotalPax();
							break;
						case HpUfisAppConstants.CON_LOAD_ULD_SUMMARY:
							transferNum = dto.getUldPcs();
							break;
						default:
							break;
						}
						
						// Connection status (no transfer subject or pax, no connection)
					    String status = null;
					    if (transferNum > 0 || tab.equals(HpUfisAppConstants.CON_AFTTAB)) {
						    long shortTime = 0;
						    long criticalTime = 0;
						    if (tga1 != ' ' && tga1 == tgd1) {
						    	shortTime = HpEKConstants.CONX_ST_SHORT;
						    	criticalTime = HpEKConstants.CONX_ST_CRITICAL;
						    } else {
						    	shortTime = HpEKConstants.CONX_DT_SHORT;
						    	criticalTime = HpEKConstants.CONX_DT_CRITICAL;
						    }
						    LOG.debug("TGA1: {} - TGD1: {}", tga1, tgd1);
						    LOG.debug("Short: {}", shortTime);
						    LOG.debug("Critical: {}", criticalTime);
						    
						    if (timeDiff <= criticalTime) {
								// Critical
								status = HpUfisAppConstants.CONX_STAT_CRITICAL;
								criticalCount += 1;
						    } else if (timeDiff > criticalTime && timeDiff <= shortTime) {
						    	// Short
								status = HpUfisAppConstants.CONX_STAT_SHORT;
								shortCount += 1;
							} else if (timeDiff > shortTime) {
								// Normal
								status = HpUfisAppConstants.CONX_STAT_NOMRAL;
							} else {
								// No Connection
								status = HpUfisAppConstants.CONX_STAT_NOCONNX;
							}
					    } else {
					    	// No Connection
							status = HpUfisAppConstants.CONX_STAT_NOCONNX;
					    }
					    
					    
					    // Find FLT_CONNECT_SUMMARY by id_arr_flight and id_dep_flight
					    UfisASCommands cmd =  UfisASCommands.URT;
					    EntDbFltConnectSummary data = null;
					    EntDbFltConnectSummary fltConx = fltConnectSummaryBean.findExisting(idArrFlight, idDepFlight);
					    if (fltConx == null) {
					    	data = new EntDbFltConnectSummary();
					    	cmd =  UfisASCommands.IRT;
					    	data = new EntDbFltConnectSummary();
					    	data.setIdArrFlight(idArrFlight);
					    	data.setIdDepFlight(idDepFlight);
					    	data.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					    	data.setCreatedUser(HpEKConstants.FLT_CONX_SOURCE);
					    	// 2013-11-05 added by JGO - Default connection status
						    data.setConxStatPax(HpUfisAppConstants.CONX_STAT_NOCONNX);
						    data.setConxStatBag(HpUfisAppConstants.CONX_STAT_NOCONNX);
						    data.setConxStatUld(HpUfisAppConstants.CONX_STAT_NOCONNX);
							// 2013-11-04 added by JGO - default decision
					    	data.setConxDecBag(HpUfisAppConstants.CONX_DEC_NODC);
					    	data.setConxDecPax(HpUfisAppConstants.CONX_DEC_NODC);
					    	data.setConxDecUld(HpUfisAppConstants.CONX_DEC_NODC);
					    	data.setRecStatus(" ");
					    	fltConnectSummaryBean.persist(data);
					    } else {
					    	data = EntDbFltConnectSummary.valueOf(fltConx);
					    }
					    
					    //evaluatePaxJourneyTime
					    switch(tab) {
						    case HpUfisAppConstants.CON_AFTTAB:
							case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
								 evaluatePaxJourneyTime(tab, mainFlt, conxFlight, data, timeDiff);
								break;
							default:
								break;
					    }
					   
						switch (tab) {
						case HpUfisAppConstants.CON_AFTTAB:
							data.setConxStatBag(status);
							data.setConxStatPax(status);
							data.setConxStatUld(status);
							break;
						case HpUfisAppConstants.CON_LOAD_BAG_SUMMARY:
							data.setConxStatBag(status);
							break;
						case HpUfisAppConstants.CON_LOAD_PAX_SUMMARY:
							data.setConxStatPax(status);
							break;
						case HpUfisAppConstants.CON_LOAD_ULD_SUMMARY:
							data.setConxStatUld(status);
							break;
						default:
							break;
						}
					    data.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					    data.setUpdatedUser(HpEKConstants.FLT_CONX_SOURCE);
					    
					    //set tifa/tifd
					    data.setTifa(tifa.getTime());
					    data.setTifd(tifd.getTime());
					    
					    data = fltConnectSummaryBean.merge(data);
						LOG.debug("Flt_Connect_Summary record has been created/update for idArrFlight={} and idDepFlight={}", 
								data.getIdArrFlight(),
								data.getIdDepFlight());
						
						// notification for flt_connect_summary
						long start = System.currentTimeMillis();
						if (HpCommonConfig.notifyTopic != null) {
							ufisTopicProducer.sendNotification(
									true, 
									cmd,
									String.valueOf(mainFlt.getUrno()), 
									fltConx,
									data);
						} else {
							ufisQueueProducer.sendNotification(
									true, 
									cmd,
									String.valueOf(mainFlt.getUrno()), 
									fltConx,
									data);
						}
						LOG.debug("Send Notification msg used: {}", (System.currentTimeMillis() - start));
					    
					} else {
						LOG.warn("No conx flight found with urno: {} in afttab for flight: {}",
								dto.getIdConxFlight(),
								mainFlt.getUrno());
					}
				} else {
					LOG.warn("Conx flight urno cannot be 0");
				}
			}
			
			// send critical count notify to Ceda for main flight
			long start = System.currentTimeMillis();
			sendCountToCeda(
					tab, 
					String.valueOf(mainFlt.getUrno()),
					criticalCount, 
					shortCount);
			LOG.debug("Send Count to Ceda used: {}", (System.currentTimeMillis() - start));
		} catch (Exception e) {
			LOG.error("Exception: {}", e);
		}
	}
	
	private void evaluatePaxJourneyTime(String tab, EntDbAfttab mainFlt, EntDbAfttab conxFlight, EntDbFltConnectSummary data, int timeDiff) {
		if (tab.equalsIgnoreCase(HpUfisAppConstants.CON_AFTTAB)
				|| tab.equalsIgnoreCase(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY)
				|| tab.equalsIgnoreCase(HpUfisAppConstants.CON_LOAD_PAX_CONNECT)) {
			 EntDbMdJourneyMatrix journeyTime = retrieveJourneyTime(mainFlt, conxFlight);
			 data.setConxTime(new BigDecimal(timeDiff));
			 if(journeyTime != null) {
				 LOG.debug("PSTA : {} and PSTD : {}", mainFlt.getPsta(), conxFlight.getPstd());
			     data.setPaxJourneyTime(journeyTime.getDuration());
			 } else {
			     LOG.debug("No Journey Time is found for PSTA : {} and PSTD : {}", mainFlt.getPsta(), conxFlight.getPstd());
			 }
		}		
	}

	private EntDbMdJourneyMatrix retrieveJourneyTime(EntDbAfttab mainFlt, EntDbAfttab conxFlight) {
		String psta = mainFlt.getPsta().trim();
		String pstd = conxFlight.getPstd().trim();
		List<EntDbMdJourneyMatrix> matrixList = journeyMatrixBean.findMatrixBy(psta, pstd);
		EntDbMdJourneyMatrix to = null;
		EntDbMdJourneyMatrix from = null;
		EntDbMdJourneyMatrix none = null;
		for (EntDbMdJourneyMatrix matrix : matrixList) {
			if(matrix.getFromLoc().trim().equals(psta) && matrix.getToLoc().trim().equals(pstd)) {
				return matrix; //both - first priority
			}
			
			if(matrix.getFromLoc().trim().equals(psta) && matrix.getToLoc().trim().equals("*")) {
				from = matrix; // 2nd priority
			}
			
			if(matrix.getFromLoc().trim().equals("*") && matrix.getToLoc().trim().equals(pstd)) {
				to = matrix; // 3rd priority
			}
			
			
			if(matrix.getFromLoc().trim().equals("*") && matrix.getToLoc().trim().equals("*")) {
				none = matrix; // no match at all
			}
		}
		
		if(from != null) {
			return from;
		}
		
		if(to != null) {
			return to;
		}
		
		return none;
	}

	/**
	 * Format and send message to CEDA in xml
	 * @param idFlight
	 * @param criticalCount
	 */
	private void sendCountToCeda(String tab, String idFlight, int criticalCount, int shortCount) {
		try {
			StringWriter writer = new StringWriter();
			_ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			HpUfisCalendar ufisCalendar = new HpUfisCalendar(TimeZone.getTimeZone("UTC"));

			MSG msg = new MSG();
			MSG.MSGSTREAMIN msgstream_in = new MSG.MSGSTREAMIN();

			// HEADER
			INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
			infobjgeneric.setACTIONTYPE(ACTIONTYPE.U);
			infobjgeneric.setMESSAGETYPE(HpEKConstants.JCEDA_SOURCE);
			infobjgeneric.setMESSAGEORIGIN(HpEKConstants.FLT_CONX_SOURCE);
			infobjgeneric.setURNO(idFlight);
			infobjgeneric.setTIMEID(TIMEID.UTC);
			infobjgeneric.setTIMESTAMP(ufisCalendar.getCedaString());

			// BODY
			MSGOBJECTS msgObjects = new MSGOBJECTS();
			INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
			String cxCount = Integer.toString(criticalCount);
			String sxCount = Integer.toString(shortCount);
			//infobjFlight.setCXPX(cxpx);
			if (HpUfisAppConstants.CON_LOAD_BAG_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXBG(cxCount);
				infobjFlight.setSXBG(sxCount);
			} else if (HpUfisAppConstants.CON_LOAD_PAX_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXPX(cxCount);
				infobjFlight.setSXPX(sxCount);
			} else if (HpUfisAppConstants.CON_LOAD_ULD_SUMMARY.equalsIgnoreCase(tab)) {
				infobjFlight.setCXUD(cxCount);
				infobjFlight.setSXUD(sxCount);
			}
			msgObjects.setINFOBJFLIGHT(infobjFlight);

			// set HEADER and BODY to MSG
			msgstream_in.setINFOBJGENERIC(infobjgeneric);
			msgstream_in.setMSGOBJECTS(msgObjects);
			msg.setMSGSTREAMIN(msgstream_in);

			_ms.marshal(msg, writer);

			// String message = writer.toString();
			// message = message.replace("<INFOBJ_FLIGHT/>",
			// "<INFOBJ_FLIGHT>\n\t\t<CXPX>"+cxpx+"</CXPX>\n\t</INFOBJ_FLIGHT>");
			clsUfisCedaQ.sendMessage(writer.toString());
		} catch(JAXBException jae){
			LOG.error("ERROR when marshalling the msg to xml : {}", jae.getMessage());
		}
	}

}
