package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.connection.EntDbFltConnectSummary;
import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.DlFltConxSummaryBean;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisCedaQueue;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgACT;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgHead;
import com.ufis_as.ufisapp.server.dto.helper.HpUfisJsonMsgFormatter;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;
/**
 * @author btr
 * @version 1.0.1: xxxx-xx-xx BTR - Timer version
 * @version 1.0.2: 2013-11-08 JGO - Handle live data for summarizer message
 *                                  Remove the critical count for afttab message
 * @version 1.0.3: 2013-11-09 JGO - Rename the handler (pax -> flt conx)
 * @version 1.0.4: 2013-11-11 JGO - Optimization for object-to-json convert
 * @version 1.0.5: 2013-11-15 JGO - Loop all conx flight to calculate the critical count for main flight
 */
@Stateless
public class BlHandleFltConxBean {
	/**
	 * Logger
	 */
	public static final Logger LOG = LoggerFactory.getLogger(BlHandleFltConxBean.class);
	
	/**
	 * CDI
	 */
	@EJB
	private IAfttabBean clsAfttabBeanLocal;
	@EJB
	private IAfttabBeanLocal afttabBean;
	@EJB
	private IDlLoadPaxSummaryBeanLocal clsDlLoadSummaryBean;
	@EJB
	private DlFltConxSummaryBean clsDlFltConxSummaryBean;
	@EJB
	private BlUfisCedaQueue clsUfisCedaQ;
	@EJB
    private BlUfisBCTopic ufisTopicProducer;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	/**
	 * Variables and Constants
	 */
    private BigDecimal idFlight;
    // Regular Expression
    private static final String NUMBER_VALUE = "\\d+";
    // ADID
    private static final Character ADID_ARRIVAL = 'A';
    private static final Character ADID_DEPARTURE = 'D';
    // Fields
    private static final String FLD_ID_FLIGHT = "ID_FLIGHT";
    private static final String FLD_ID_CONX_FLIGHT = "ID_CONX_FLIGHT";
    private static final String FLD_ID_ARR_FLIGHT = "ID_ARR_FLIGHT";
    private static final String FLD_ID_DEP_FLIGHT = "ID_DEP_FLIGHT";
    private static final String FLD_INFO_TYPE = "INFO_TYPE";
    private static final String FLD_BAG_PCS = "BAG_PCS";
    private static final String FLD_TOTAL_PAX = "TOTAL_PAX";
    private static final String FLD_ULD_PCS = "ULD_PCS";
    // default value
    private static final String DEF_FLIGHT_ID = "0";
    
    /**
     * Constructor
     */
	public BlHandleFltConxBean() {
	}
	
	/**
	 * Live message procession
	 * 
	 * - process flight connection status for Pax and
	 * - calculate tifd - tifa < 60 (CRITICAL) count of conx flights
	 * - and send to UFIS CEDA (Queue or Topic)
	 * @param entAft
	 */
	public void processConxStat(EntUfisMsgDTO ufisMsgDTO){
		try {
			EntDbAfttab entAft = extractInputDataAndCheck(ufisMsgDTO);
			if(entAft != null && entAft.getUrno() != null) {
				idFlight = entAft.getUrno();
				computeConxStat(entAft);
			}
		} catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
	
	

	/**
	 * - extract URNO, ADID, TIFA, TIFD only if TAB = AFTTAB and CMD = UFR|IFR|DFR
	 * @param ufisMsgDTO
	 */
	private EntDbAfttab extractInputDataAndCheck(EntUfisMsgDTO ufisMsgDTO) {
		String urno = null, adid = null, tifa = null, tifd = null;
		List<String> fld = ufisMsgDTO.getBody().getActs().get(0).getFld();
		List<Object> data = ufisMsgDTO.getBody().getActs().get(0).getData();
		String cmd = ufisMsgDTO.getBody().getActs().get(0).getCmd().trim();
		
		if("AFTTAB".equalsIgnoreCase(ufisMsgDTO.getBody().getActs().get(0).getTab().trim())){
			if(cmd.equalsIgnoreCase("UFR") || cmd.equalsIgnoreCase("IFR") || cmd.equalsIgnoreCase("DFR")){
				for (int i = 0; i < fld.size(); i++) {
					switch(fld.get(i).toUpperCase()){
						case "URNO": urno = (String) data.get(i); break;
						case "ADID" : adid = (String) data.get(i); break;
						case "TIFA" : tifa = (String) data.get(i); break;
						case "TIFD" : tifd = (String) data.get(i); break;
					}
				}
			}
		}
		EntDbAfttab entAft = null;
		if(HpUfisUtils.isNullOrEmptyStr(urno) || HpUfisUtils.isNullOrEmptyStr(adid) 
				|| HpUfisUtils.isNullOrEmptyStr(tifa) || HpUfisUtils.isNullOrEmptyStr(tifd)){
			LOG.debug("Data is missing for (URNO, ADID, TIFA, TIFD) to perform flight connect summary.");
		}else{
			entAft = new EntDbAfttab();
			entAft.setUrno(new BigDecimal(urno));
			entAft.setAdid(adid.charAt(0));
			entAft.setTifa(tifa);
			entAft.setTifd(tifd);
		}
		return entAft;
	}

	
	/**
	 * - calculate the status of connx flights summary - process only when the
	 * conx flight existed - count the total num of CRITICAL for that flight
	 * 
	 * @param flight
	 * @return total num of CRITICAL
	 */
	public int computeConxStat(EntDbAfttab flight) {
		int criticalNum = 0;
		try {
			if ('A' == flight.getAdid()) {
				// look for connected dep flight from load_pax_summary
				List<EntDbLoadPaxSummary> loadPaxSumList = clsDlLoadSummaryBean.findByArrFlId(flight.getUrno());
				if (loadPaxSumList != null) {
					LOG.debug("{} records found in Load_Pax_Summary for idArrFlight={}", 
							loadPaxSumList.size(), 
							flight.getUrno());
					for (int i = 0; i < loadPaxSumList.size(); i++) {
						EntDbLoadPaxSummary loadPaxSummary = loadPaxSumList.get(i);
						if (loadPaxSummary.getIdDepFlight() != null) {
							// find the conx dept flight from AFTTAB
							EntDbAfttab dept = clsAfttabBeanLocal.findFlightForConx(
											loadPaxSummary.getIdDepFlight());
							if (dept != null) {
								criticalNum += processFltConxSummary(flight, dept);
							} else {
								LOG.debug("No Connected flight(urno:{}) found in AFTTAB for flight(urno: {})", 
										loadPaxSummary.getIdDepFlight(),
										flight.getUrno());
							}
						}
					}
				}
			} else if ('D' == flight.getAdid()) {

				// look for connected arr flight from load_pax_summary
				List<EntDbLoadPaxSummary> loadPaxSumList = clsDlLoadSummaryBean.findByDepFlId(flight.getUrno());
				if (loadPaxSumList != null) {
					LOG.debug("{} records found in Load_Pax_Summary for idDepFlight={}", 
							loadPaxSumList.size(), 
							flight.getUrno());
					for (int i = 0; i < loadPaxSumList.size(); i++) {
						EntDbLoadPaxSummary loadPaxSummary = loadPaxSumList.get(i);
						if (loadPaxSummary.getIdArrFlight() != null) {
							EntDbAfttab arr = clsAfttabBeanLocal.findFlightForConx(
											loadPaxSummary.getIdArrFlight());
							if (arr != null) {
								// process only when the conx flight existed.
								criticalNum += processFltConxSummary(arr, flight);
							} else {
								LOG.debug("No Connected flight(urno:{}) found in AFTTAB for flight(urno: {})", 
										loadPaxSummary.getIdArrFlight(),
										flight.getUrno());
							}
						}
					}
				}
			} else {
				LOG.debug("Invalid flight. ADID Only accepted A or D)");
			}
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
		return criticalNum;
	}

	/**
	 * - MinuteDiff = tifd - tifa; - Identify the status: N (x>75), S (x<75 and
	 * x>60), C (x<60), X (no connection) - Insert/Update the record to
	 * Flt_Connect_Summary
	 * 
	 * @param arr
	 *            , dept
	 * @return
	 */
	private int processFltConxSummary(EntDbAfttab arr, EntDbAfttab dept) {
		long timeDiff = 0;
		String status = null;
		BigDecimal arrUrno = new BigDecimal(0);
		BigDecimal depUrno = new BigDecimal(0);
		Date tifa = new Date();
		Date tifd = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		int critical = 0;
		String cmd = UfisASCommands.IRT.name();

		try {
			tifa = df.parse(arr.getTifa());
			tifd = df.parse(dept.getTifd());
			timeDiff = (tifd.getTime() - tifa.getTime()) / 60000;// in min
			depUrno = dept.getUrno();
			arrUrno = arr.getUrno();

			// calculate conx_stat_pax
			if (timeDiff > HpEKConstants.MAX_CONX_TIME_PAX_ALERT) {
				status = "N"; // Normal
			} else if (timeDiff > HpEKConstants.MIN_CONX_TIME_PAX_ALERT
					&& timeDiff <= HpEKConstants.MAX_CONX_TIME_PAX_ALERT) {
				status = "S"; // Short
			} else if (timeDiff <= HpEKConstants.MIN_CONX_TIME_PAX_ALERT) {
				status = "C"; // Critical
				critical = 1;
			} else {
				status = "X"; // No Connection
			}
			
			EntDbFltConnectSummary fltConxSummary = null;
			fltConxSummary = clsDlFltConxSummaryBean.findExisting(arrUrno, depUrno);
			if (fltConxSummary == null) {
				fltConxSummary = new EntDbFltConnectSummary();
				fltConxSummary.setCreatedDate(HpUfisCalendar
						.getCurrentUTCTime());
				fltConxSummary.setCreatedUser(HpEKConstants.PAX_ALERT_SOURCE);
				// 2013-11-04 added by JGO - default decision
				fltConxSummary.setConxDecBag(HpUfisAppConstants.CONX_DEC_NODC);
				fltConxSummary.setConxDecPax(HpUfisAppConstants.CONX_DEC_NODC);
				fltConxSummary.setConxDecUld(HpUfisAppConstants.CONX_DEC_NODC);
			} else {
				cmd = UfisASCommands.URT.name();
				fltConxSummary.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
				fltConxSummary.setUpdatedUser(HpEKConstants.PAX_ALERT_SOURCE);
			}

			fltConxSummary.setIdArrFlight(arrUrno);
			fltConxSummary.setIdDepFlight(depUrno);
			fltConxSummary.setConxTime(new BigDecimal(timeDiff));
			fltConxSummary.setConxStatPax(status);
			// 2013-11-05 added by JGO - Adding status for bag and uld as well
			fltConxSummary.setConxStatBag(status);
			fltConxSummary.setConxStatUld(status);
			EntDbFltConnectSummary entity = clsDlFltConxSummaryBean.merge(fltConxSummary);

			if (entity != null) {
				// call method to send NOTIFY message
				sendNotifyMsg(entity, cmd);
				LOG.info("Afttab msg has been processed. ArrURNO: <{}>	DepURNO: <{}>",
						arrUrno, depUrno);
			}
		} catch (ParseException e) {
			LOG.error("ERROR when parsing datetime : {}", e);
		}
		return critical;
	}
	
	/**
	 * Send notify message
	 * @param entity
	 * @param cmd
	 */
	private void sendNotifyMsg(EntDbFltConnectSummary entity, String cmd) {
		List<EntUfisMsgACT> listAction = formatData(entity, cmd);
		EntUfisMsgHead header = formatJsonHeader();
		String msg = HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
		ufisTopicProducer.sendMessage(msg);
		LOG.debug("Sent Notify :\n{}", msg);
	}

	private EntUfisMsgHead formatJsonHeader() {
		// get the HEAD info
		EntUfisMsgHead header = new EntUfisMsgHead();
		header.setHop(HpEKConstants.HOPO);
		header.setUsr(HpEKConstants.PAX_ALERT_SOURCE);
		header.setApp(HpEKConstants.PAX_ALERT_SOURCE);
		header.setOrig(HpEKConstants.PAX_ALERT_SOURCE);
		HpUfisCalendar cur = new HpUfisCalendar(new Date());
		header.setReqt(cur.getCedaString());
		List<String> idFltList = new ArrayList<>();
		idFltList.add(idFlight.toString());
		header.setIdFlight(idFltList);
		
		return header;
	}
	
	private List<EntUfisMsgACT> formatData(EntDbFltConnectSummary entFltConxSum, String cmd) {
		List<String> fldList = new ArrayList<>();
		List<Object> valueList = new ArrayList<>();
		List<EntUfisMsgACT> listAction = new ArrayList<>();
		EntUfisMsgACT action = new EntUfisMsgACT();
		
		//fields to INSERT
		fldList.add("ID_ARR_FLIGHT");
		fldList.add("CONX_TIME");
		fldList.add("CONX_STAT_PAX");
		fldList.add("CONX_DEC_PAX");
		fldList.add("CONX_STAT_BAG");
		fldList.add("CONX_DEC_BAG");
		fldList.add("CONX_STAT_ULD");
		fldList.add("CONX_DEC_ULD");

		valueList.add(entFltConxSum.getIdArrFlight().toString());
		valueList.add(entFltConxSum.getConxTime().toString());
		valueList.add(entFltConxSum.getConxStatPax());
		valueList.add(entFltConxSum.getConxDecPax());
		valueList.add(entFltConxSum.getConxStatBag());
		valueList.add(entFltConxSum.getConxDecBag());
		valueList.add(entFltConxSum.getConxStatUld());
		valueList.add(entFltConxSum.getConxDecUld());
		
		action.setCmd(cmd);
		//fields for all IRT/URT/DRT
		
		action.setTab("FLT_CONNECT_SUMMARY");
		action.setFld(fldList);
		action.setData(valueList);
		
		listAction.add(action);
		return listAction;
	}
	
	/**
	 * Handle Load_Pax_Summary message
	 * @param dto
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public FltConnectionDTO handleLoadSummaryMsg(EntUfisMsgDTO dto) {
		FltConnectionDTO fltConx = new FltConnectionDTO();
		try {
			// main flight and connect flight urno(uaft)
			String idFlight = null;
			String idConxFlight = null;
			
			// idFlight is the main flight
			List<String> idFlights = dto.getHead().getIdFlight();
			if (idFlights != null && idFlights.size() > 0) {
				// main flight
				idFlight = idFlights.get(0);
				fltConx.setIdFlight(new BigDecimal(idFlight));
				
				EntUfisMsgACT act = dto.getBody().getActs().get(0);
				String tab = act.getTab(); 
				List<String> fld = act.getFld();
				List<Object> dat = act.getData();
				
				String status = null;
				String bagPcs = null;
				String uldPcs = null;
				String totalPax = null;
				String infoType = null;
				for (int i = 0; i < fld.size(); i++) {
					String field = fld.get(i).toUpperCase();
					Object value = dat.get(i);
					if (value != null) {
						switch (field) {
						case FLD_ID_ARR_FLIGHT:
							if (!value.toString().equals(idFlight)) {
								idConxFlight = value.toString(); 
							}
							break;
						case FLD_ID_DEP_FLIGHT:
							if (!value.toString().equals(idFlight)) {
								idConxFlight = value.toString();
							}
							break;
						case FLD_INFO_TYPE:
							infoType = value.toString();
							break;
						case FLD_ID_FLIGHT:
							//idFlight = value.toString();
							break;
						case FLD_ID_CONX_FLIGHT:
							idConxFlight = value.toString();
							break;
						case FLD_BAG_PCS:
							bagPcs = value.toString();
							break;
						case FLD_TOTAL_PAX:
							totalPax = value.toString();
							break;
						case FLD_ULD_PCS:
							uldPcs = value.toString();
							break;
						default:
							break;
						}
					}
				}
				
				// Check info_type: only accepted TXF(transfer)
				if (HpUfisAppConstants.INFO_TYPE_TRANSFER.equalsIgnoreCase(infoType)) {
					boolean isValid = false;
					// arrival and departure flight urno cannot be null, empty or 0
					// arrival and departure flight urno cannot be same
					if ((HpUfisUtils.isNotEmptyStr(idFlight) && !DEF_FLIGHT_ID.equals(idFlight))
							&& (HpUfisUtils.isNotEmptyStr(idConxFlight) && !DEF_FLIGHT_ID.equals(idConxFlight))
							&& (!idFlight.equals(idConxFlight))) {
						// Transfer pax number should be greater than 0
						if (totalPax != null 
								&& totalPax.matches(NUMBER_VALUE)
								&& Integer.parseInt(totalPax) > 0) {
							isValid = true;
						} else {
							if (HpUfisAppConstants.CON_LOAD_PAX_SUMMARY.equalsIgnoreCase(tab)) {
								LOG.warn("Total Pax number for transfer must be greater than 0. Received value: {}", totalPax);
							}
						}
						// Transfer bag pcs should be greater than 0
						if (bagPcs != null 
								&& bagPcs.matches(NUMBER_VALUE)
								&& Integer.parseInt(bagPcs) > 0) {
							isValid = true;
						} else {
							if (HpUfisAppConstants.CON_LOAD_BAG_SUMMARY.equalsIgnoreCase(tab)) {
								LOG.warn("Total Bag Pieces for transfer must be greater than 0. Received value: {}", bagPcs);
							}
						}
						// Treanfer uld pcs should be greater than 0
						if (uldPcs != null
								&& bagPcs.matches(NUMBER_VALUE)
								&& Integer.parseInt(uldPcs) > 0) {
							isValid = true;
						} else {
							if (HpUfisAppConstants.CON_LOAD_ULD_SUMMARY.equalsIgnoreCase(tab)) {
								LOG.warn("Total Uld Pieces for transfer must be greater than 0. Received value: {}", uldPcs);
							}
						}
							
							
						if (isValid) {	
							long start = System.currentTimeMillis();
							// find arrival and departure flight from afttab
							EntDbAfttab flight = afttabBean.findFlightForConxByUrno(Long.parseLong(idFlight));
						    EntDbAfttab conxFlight = afttabBean.findFlightForConxByUrno(Long.parseLong(idConxFlight));
						    LOG.debug("Search Flight from afttab used: {}", (System.currentTimeMillis() - start));
						    // Main flight and conx flight must be existing in afttab
						    if (flight == null) {
						    	LOG.warn("Flight not found for urno: {}", idFlight);
						    	return null;
						    }
						    if (conxFlight == null) {
						    	LOG.warn("Conx Flight not found for urno: {}", idConxFlight);
						    	return null;
						    }
						    // Main flight and conx flight must be either arrival or departure flight
						    if (ADID_ARRIVAL != flight.getAdid() && ADID_DEPARTURE != flight.getAdid()) {
						    	LOG.warn("Main flight must be arrival or departure flight");
						    	LOG.warn("ADID={} for main flight={}", flight.getAdid(), flight.getUrno());
						    	return null;
						    }
						    if (ADID_ARRIVAL != conxFlight.getAdid() && ADID_DEPARTURE != conxFlight.getAdid()) {
						    	LOG.warn("Conx flight must be arrival or departure flight");
						    	LOG.warn("ADID={} for conx flight={}", conxFlight.getAdid(), conxFlight.getUrno());
						    	return null;
						    }
						    
						    // calculate the time span between main and connect flight
						    BigDecimal idArrFlight = null;
						    BigDecimal idDepFlight = null;
						    HpUfisCalendar tifa = null;
						    HpUfisCalendar tifd = null;
						    if (ADID_ARRIVAL == flight.getAdid()) {
						    	tifa = new HpUfisCalendar(flight.getTifa());
						    	tifd = new HpUfisCalendar(conxFlight.getTifd());
						    	idArrFlight = flight.getUrno();
						    	idDepFlight = conxFlight.getUrno();
						    } else {
						    	tifa = new HpUfisCalendar(conxFlight.getTifd());
						    	tifd = new HpUfisCalendar(flight.getTifa());
						    	idArrFlight = conxFlight.getUrno();
						    	idDepFlight = flight.getUrno();
						    } 
						    
						    int timeDiff = (int) tifa.timeDiff(tifd, EnumTimeInterval.Minutes, false);
						    LOG.debug("Time Difference between main flight and conx flight: {} mins", timeDiff);
							if (timeDiff > HpEKConstants.MAX_CONX_TIME_PAX_ALERT) {
								// Normal
								//status = "N"; 
								status = HpUfisAppConstants.CONX_STAT_NOMRAL;
							} else if (timeDiff > HpEKConstants.MIN_CONX_TIME_PAX_ALERT
									&& timeDiff <= HpEKConstants.MAX_CONX_TIME_PAX_ALERT) {
								// Short
								//status = "S";
								status = HpUfisAppConstants.CONX_STAT_SHORT;
							} else if (timeDiff <= HpEKConstants.MIN_CONX_TIME_PAX_ALERT) {
								// Critical
								//critical = 1;
								//status = "C";
								status = HpUfisAppConstants.CONX_STAT_CRITICAL;
								fltConx.setCriticalCount(1);
							} else {
								// No Connection
								//status = "X";
								status = HpUfisAppConstants.CONX_STAT_NOCONNX;
							}
							
							UfisASCommands cmd =  UfisASCommands.URT;
							// find flight connect summary record
							EntDbFltConnectSummary entity = null;
							start = System.currentTimeMillis();
							EntDbFltConnectSummary fltConxSummary = clsDlFltConxSummaryBean.findExisting(idArrFlight, idDepFlight);
							LOG.debug("Search Record from Flt_Connect_Summary used: {}", (System.currentTimeMillis() - start));
							if (fltConxSummary == null) {
								cmd =  UfisASCommands.IRT;
								entity = new EntDbFltConnectSummary();
								entity.setIdArrFlight(idArrFlight);
								entity.setIdDepFlight(idDepFlight);
								entity.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
								entity.setCreatedUser(HpEKConstants.FLT_CONX_SOURCE);
								// 2013-11-04 added by JGO - default decision
								entity.setConxDecBag(HpUfisAppConstants.CONX_DEC_NODC);
								entity.setConxDecPax(HpUfisAppConstants.CONX_DEC_NODC);
								entity.setConxDecUld(HpUfisAppConstants.CONX_DEC_NODC);
								entity.setRecStatus(" ");
								clsDlFltConxSummaryBean.persist(entity);
							} else {
								entity = EntDbFltConnectSummary.valueOf(fltConxSummary);
							}
							entity.setConxTime(new BigDecimal(timeDiff));
							entity.setConxStatPax(status);
							// 2013-11-05 added by JGO - Adding status for bag and uld as well
							entity.setConxStatBag(status);
							entity.setConxStatUld(status);
							entity.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
							entity.setUpdatedUser(HpEKConstants.FLT_CONX_SOURCE);
							entity = clsDlFltConxSummaryBean.merge(entity);
							LOG.debug("Flt_Connect_Summary record has been created/update for idArrFlight={} and idDepFlight={}", 
									entity.getIdArrFlight(),
									entity.getIdDepFlight());
							// notification for flt_connect_summary
							start = System.currentTimeMillis();
							if (HpCommonConfig.notifyTopic != null) {
								ufisTopicProducer.sendNotification(
										true, 
										cmd,
										idFlight,
										fltConxSummary, entity);
							} else {
								ufisQueueProducer.sendNotification(
										true, 
										cmd,
										idFlight,
										fltConxSummary, entity);
							}
							LOG.debug("Send Notification msg used: {}", (System.currentTimeMillis() - start));
						}
					} else {
						LOG.warn("Flight ID and Conx Flight ID cannot be null, empty or 0");
						LOG.debug("Flight ID: {} and Conx Flight ID: {}", idFlight, idConxFlight);
					}
				} else {
					LOG.warn("Info_Type: Accept Transfer only. Received value: {}", infoType);
				}
			} else {
				LOG.warn("ID_FLIGHT holder cannot be null or empty for Load_Pax_Summary notify message");
			}
		} catch (Exception e) {
			LOG.error("Exception: {}", e.getMessage());
		}
		return fltConx;
	}
	
}