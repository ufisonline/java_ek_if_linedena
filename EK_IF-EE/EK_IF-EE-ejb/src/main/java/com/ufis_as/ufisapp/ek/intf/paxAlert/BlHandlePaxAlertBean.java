package com.ufis_as.ufisapp.ek.intf.paxAlert;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpPaxAlertTimeRangeConfig;
import com.ufis_as.ek_if.macs.IDlLoadPaxSummaryBeanLocal;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlLoadBagSummaryBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldSummaryBean;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
/**
 * @author btr
 */
@Stateless
public class BlHandlePaxAlertBean {

	public static final Logger LOG = LoggerFactory.getLogger(BlHandlePaxAlertBean.class);
	@EJB
	private IAfttabBean afttabBean;
	@EJB
	private DlLoadBagSummaryBean loadBagSummaryBean;
	@EJB
	private IDlLoadPaxSummaryBeanLocal loadPaxSummaryBean;
	@EJB
	private DlLoadUldSummaryBean loadUldSummaryBean;
	@EJB
	private BlConxAlertRouter conxRouterBean;
	
	public BlHandlePaxAlertBean() {
	}
	
	/**
	 * Timer Execution - interval(default: 5mins)
	 * Processing for all arival or main flights in load_xxx_summary table within time range
	 * Processing for all flights in afttab within time range
	 * 
	 */
	public void processConxAlert() {
		try {
			LOG.info("===================== START/END =====================");
			HpUfisCalendar startDate = new HpUfisCalendar();
			HpUfisCalendar endDate = new HpUfisCalendar();
			//startDate.setTimeZone(HpEKConstants.utcTz);
			//endDate.setTimeZone(HpEKConstants.utcTz);
			
			// Convert to UTC time
			startDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
			endDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
			
			// Add search criteria offset
			startDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertFromOffset(), EnumTimeInterval.Minutes);
			endDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertToOffset(), EnumTimeInterval.Minutes);
			
			// ===========================================
			// ============ LOAD SUMMARY =================
			// ===========================================
			LOG.info("Start for flights from LOAD SUMMARY...");
			// query
			long start = System.currentTimeMillis();
			List<Object> loadBagSumListFlightId = loadBagSummaryBean.findByTimeRange(startDate, endDate);
//			// test data
//			loadBagSumListFlightId.add(new BigDecimal("942440"));
//			loadBagSumListFlightId.add(new BigDecimal("943023"));
//			loadBagSumListFlightId.add(new BigDecimal("212401"));
			LOG.debug("Search LoadBagSummary used: {}", (System.currentTimeMillis() - start));
			// process
			start = System.currentTimeMillis();
			conxRouterBean.routeToProcess(HpUfisAppConstants.CON_LOAD_BAG_SUMMARY, loadBagSumListFlightId);
			LOG.debug("Process LoadBagSummary used: {}", (System.currentTimeMillis() - start));
			
			// query
			start = System.currentTimeMillis();
			List<Object> loadPaxSumFlightId = loadPaxSummaryBean.findByTimeRange(startDate, endDate);
//			// test data
//			loadPaxSumFlightId.add(new BigDecimal("761117"));
//			loadPaxSumFlightId.add(new BigDecimal("942440"));
			LOG.debug("Search LoadPaxSummary used: {}", (System.currentTimeMillis() - start));
			// process
			start = System.currentTimeMillis();
			conxRouterBean.routeToProcess(HpUfisAppConstants.CON_LOAD_PAX_SUMMARY, loadPaxSumFlightId);
			LOG.debug("Process LoadPaxSummary used: {}", (System.currentTimeMillis() - start));
			
			// query
			start = System.currentTimeMillis();
			List<Object> loadUldSumList = loadUldSummaryBean.findByTimeRange(startDate, endDate);
//			// test data
//			loadUldSumList.add(new BigDecimal("170918"));
//			loadUldSumList.add(new BigDecimal("169406"));
//			loadUldSumList.add(new BigDecimal("169942"));
//			loadUldSumList.add(new BigDecimal("171502"));
//			loadUldSumList.add(new BigDecimal("169958"));
//			loadUldSumList.add(new BigDecimal("169814"));
//			loadUldSumList.add(new BigDecimal("178110"));
//			loadUldSumList.add(new BigDecimal("171774"));
//			loadUldSumList.add(new BigDecimal("161678"));
//			loadUldSumList.add(new BigDecimal("177382"));
//			loadUldSumList.add(new BigDecimal("177254"));
//			loadUldSumList.add(new BigDecimal("171998"));
			LOG.debug("Search LoadUldSummary used: {}", (System.currentTimeMillis() - start));
			// process
			start = System.currentTimeMillis();
			conxRouterBean.routeToProcess(HpUfisAppConstants.CON_LOAD_ULD_SUMMARY, loadUldSumList);
			LOG.debug("Process LoadUldSummary used: {}", (System.currentTimeMillis() - start));
			
			// ===========================================
			// ================= AFTTAB ==================
			// ===========================================
			LOG.info("Start for flights from AFTTAB ...");
			HpUfisCalendar aftFromDate = new HpUfisCalendar();
			HpUfisCalendar aftToDate = new HpUfisCalendar();
			
			// Convert to UTC time
			aftFromDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
			aftToDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
			
			// -2 ~ +8 (hours)
			aftFromDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertInitFrom(), EnumTimeInterval.Hours);
			aftToDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertInitTo(), EnumTimeInterval.Hours);
			
			start = System.currentTimeMillis();
			List<EntDbAfttab> flightList = afttabBean.findAllFlightByTimeRange(aftFromDate, aftToDate);
			
			// test data
/*			flightList.clear();
			EntDbAfttab entity = afttabBean.findFlightForConx(new BigDecimal(929445));
			flightList.add(entity);*/
			
			LOG.debug("Search Afttab used: {}", (System.currentTimeMillis() - start));
			conxRouterBean.routeToProcess(HpUfisAppConstants.CON_AFTTAB, flightList);
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
	}
	
}