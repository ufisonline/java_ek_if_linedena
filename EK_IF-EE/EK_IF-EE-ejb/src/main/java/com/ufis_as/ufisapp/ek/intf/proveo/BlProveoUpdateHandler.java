package com.ufis_as.ufisapp.ek.intf.proveo;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.configuration.HpProveoConfig;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.proveo.entities.EntDbEquipPos;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.exco.ADID;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.eao.IDlUpdateEquipPosLocal;
import com.ufis_as.ufisapp.ek.hp.HpUfisMsgFormatter;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.messaging.BlUfisExceptionQueue;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.proveo.telemetryDataMessage.EventTypes;
import ek.proveo.telemetryDataMessage.TelemetryDataMessage;
import ek.proveo.telemetryDataMessage.TelemetryDataMessages;

@Stateless(mappedName = "BlProveoUpdateHandler")
// @LocalBean
public class BlProveoUpdateHandler {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlProveoUpdateHandler.class);

	private JAXBContext _cnxJaxb;

	private Unmarshaller _um;

	// private List<String> qFromMqList = null;

	@EJB
	private IAfttabBean aftBean;
	@EJB
	private BlIrmtabFacade _irmfacade;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private IDlUpdateEquipPosLocal _updateEquipPos;
	@EJB
	private BlUfisBCTopic clsBlUfisBCTopic;
	@EJB
	private BlUfisExceptionQueue _blUfisExcepQ;
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	/*
	 * TODO In the future we will use seperate config per Interface for the
	 * moment we will jsut take the first from the list
	 */
	// private List<InterfaceConfig> interfaceConfigs = new ArrayList<>();
	/*
	 * TODO move XMLConfiguration config to the Singleton class
	 */
	// private static XMLConfiguration config;

	// private Map<String, EntDbMdEquipLoc> mdEquipLocMap = new TreeMap<String,
	// EntDbMdEquipLoc>(String.CASE_INSENSITIVE_ORDER);
	// private Map<String, EntDbMdEquip> mdEquipMap = new TreeMap<String,
	// EntDbMdEquip>(String.CASE_INSENSITIVE_ORDER);
	private List<String> mdEquipLocMap = new ArrayList<String>();
	private List<String> mdEquipMap = new ArrayList<String>();
	private Map<String, String> stateInfoMap = new TreeMap<String, String>(
			String.CASE_INSENSITIVE_ORDER);
	private Map<String, HashMap<String, String>> stateCurrValueMap = new TreeMap<String, HashMap<String, String>>(
			String.CASE_INSENSITIVE_ORDER);
	// private DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	String dtflStr = HpEKConstants.PROVEO_SOURCE;
	String logLevel = null;
	Boolean msgLogged = Boolean.FALSE;

	// private EntUfisMsgHead header;
	// List<String> fldList = new ArrayList<>();

	/**
	 * Default constructor.
	 */
	public BlProveoUpdateHandler() {
		/*
		 * if (header == null) { header = new EntUfisMsgHead();
		 * header.setHop(HpEKConstants.HOPO); // notifyHeader.setReqt(reqt);
		 * header.setOrig(HpEKConstants.PROVEO_SOURCE);
		 * header.setApp(HpEKConstants.PROVEO_SOURCE);
		 * header.setUsr(HpEKConstants.PROVEO_SOURCE); } if (fldList.isEmpty())
		 * { fldList.add("ID_DEP_FLIGHT"); fldList.add("ID_ARR_FLIGHT");
		 * fldList.add("TRACK_NUM"); fldList.add("UNIT_NAME");
		 * fldList.add("UNIT_MODEL"); fldList.add("LONGITUDE");
		 * fldList.add("LATITUDE"); fldList.add("X"); fldList.add("Y");
		 * fldList.add("USER_DEFINED_ARREA"); fldList.add("INFO_UPD_TIME");
		 * fldList.add("INFO_TYPE"); fldList.add("INFO_VALUE");
		 * fldList.add("CURR_STATUS_DATE"); fldList.add("PREV_STATUS_DATE");
		 * fldList.add("CURR_VALUE"); fldList.add("PREV_VALUE");
		 * fldList.add("CURR_UNIT_AREA"); fldList.add("PREV_UNIT_AREA");
		 * fldList.add("REC_STATUS"); }
		 */
	}

	@PostConstruct
	private void initialize() {
		/*
		 * try { config = new
		 * XMLConfiguration(HpUfisAppConstants.DEFAULT_CONFIGNAME);
		 * LOG.info("Loading configuration file {}", config.getBasePath()); }
		 * catch (ConfigurationException cex) {
		 * LOG.error("Error Loading configuration file {}", cex.getMessage());
		 * LOG.error("Application is now Stopped"); }
		 */
		try {
			_cnxJaxb = JAXBContext.newInstance(TelemetryDataMessages.class);
			// dtflStr = _startupInitSingleton.getDtflStr();
			_um = _cnxJaxb.createUnmarshaller();

		} catch (JAXBException ex) {
			LOG.error("JAXBException when creating Unmarshaller");
		}

	}

	public boolean routeMessage(Message inMessage, String message,
			InterfaceConfig interfaceConfig, Long irmtabRef) {
		long startTime = new Date().getTime();
		TelemetryDataMessages proveoMessage = new TelemetryDataMessages();
		List<TelemetryDataMessage> telemetryMsgList = Collections.EMPTY_LIST;
		EventTypes eventType = null;
		EntDbEquipPos equipPosIns;
		String dtflString = _startupInitSingleton.getDtflStr();
		msgLogged = Boolean.FALSE;
		logLevel = _startupInitSingleton.getIrmLogLev();
		// List<EntDbEquipPos> equipPosPersistRecs = new
		// ArrayList<EntDbEquipPos>();
		// String cmdForBroadcasting = null;
		EntDbEquipPos equipPosPersistRec = null;
		Boolean isLast = Boolean.FALSE;
		if (irmtabRef > 0) {
			msgLogged = Boolean.TRUE;
		}
		try {
			JAXBElement<TelemetryDataMessages> data = _um.unmarshal(
					new StreamSource(new StringReader(message)),
					TelemetryDataMessages.class);
			proveoMessage = (TelemetryDataMessages) data.getValue();
			if (proveoMessage == null) {
				LOG.info(
						" Dropping the message. Message is NOT Valid. MSG : {}",
						message);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, irmtabRef,
							"Invalid Message");

				}
				return false;
			}
			if (proveoMessage.getTelemetryDataMessage().isEmpty()) {
				LOG.info(
						"Dropping the message. No TelemetryDataMessages are defined in the MSG: {}",
						message);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EXSDF, irmtabRef,
							"No TelemetryDataMessage defined");

				}
				return false;
			}
			if (proveoMessage.getEventType() == null) {
				LOG.info(
						"Dropping the message. Event Type is either not specified or an Invalid value(NOT in GEO,STATE,NOTIFY) in the MSG : {}",
						message);
				if (logLevel
						.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
								.name())) {
					if (!msgLogged) {
						irmtabRef = _irmfacade.storeRawMsg(inMessage,
								dtflString);
						msgLogged = Boolean.TRUE;
					}
				}
				if (irmtabRef > 0) {
					sendErrInfo(EnumExceptionCodes.EWEVT, irmtabRef,
							"Event Type is NULL(Invalid Or Not Specified)");

				}
				return false;
			}
			if (mdEquipMap.isEmpty()) {
				LOG.info("Loading masterdata EntDbMdEquip into Map");
				// List<EntDbMdEquip> equipMasterdataList =
				// _startupInitSingleton.getMdEquipList();
				// for (EntDbMdEquip equipEnt : equipMasterdataList) {
				// mdEquipMap.put(equipEnt.getEquipNum(), equipEnt);
				// }
				mdEquipMap = _startupInitSingleton.getMdEquipList();
			}
			if (mdEquipLocMap.isEmpty()) {
				LOG.info("Loading masterdata EntDbMdEquipLoc into Map");
				// List<EntDbMdEquipLoc> equipMasterdataList =
				// _startupInitSingleton.getMdEquipLocList();
				// for (EntDbMdEquipLoc equipEntLoc : equipMasterdataList) {
				// mdEquipLocMap.put(equipEntLoc.getEquipArea(), equipEntLoc);
				// }
				mdEquipLocMap = _startupInitSingleton.getMdEquipLocList();
			}

			if ((_startupInitSingleton.isMdEquipLocOn() && mdEquipLocMap
					.isEmpty())
					|| (_startupInitSingleton.isMdEquipOn() && mdEquipMap
							.isEmpty())) {
				LOG.error("Either of Master data(MD_EQUIP/MD_EQUIP_LOC) is EMPTY");
			}
			telemetryMsgList = proveoMessage.getTelemetryDataMessage();
			eventType = proveoMessage.getEventType();
			LOG.info("Total TelemetryDataMessages : {}",
					telemetryMsgList.size() + "   Time For Mandatory Checks:"
							+ (new Date().getTime() - startTime));
			int size = telemetryMsgList.size();
			for (TelemetryDataMessage teleMsg : telemetryMsgList) {

				if (teleMsg.equals(telemetryMsgList.get(size - 1))) {
					isLast = Boolean.TRUE;
				}
				LOG.debug("ISLAST:{}", isLast);
				long startTime1 = new Date().getTime();
				// LOG.info("Processing TelemetryDataMessages : {}",
				// (startTime1));
				if (HpUfisUtils.isNullOrEmptyStr(teleMsg.getMessageId())) {
					LOG.info(
							"Dropping the message.Mandatory Field Message ID is not Specified in the TeleMessage. Tele Message ID:{}",
							teleMsg.getMessageId());
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(inMessage,
									dtflString);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(
								EnumExceptionCodes.EMAND,
								irmtabRef,
								("Message Id(Mandatory Field) is Null. Tele Message ID:" + teleMsg
										.getMessageId()));
					}
					continue;
				}
				if (HpUfisUtils.isNullOrEmptyStr(teleMsg.getProveoId())
						|| HpUfisUtils.isNullOrEmptyStr(teleMsg.getUnitName())) {
					LOG.info(
							"Dropping the message.Mandatory Field (Proveo ID/Unit Name) is not Specified in the message. Tele Message ID:{}",
							teleMsg.getMessageId());
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(inMessage,
									dtflString);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(
								EnumExceptionCodes.EMAND,
								irmtabRef,
								("Either Proveo Id OR Unit Name(Mandatory Fields) is Null.Tele Message ID:" + teleMsg
										.getMessageId()));

					}
					// return false;
					continue;
				}

				equipPosIns = new EntDbEquipPos();
				equipPosIns.setTrackNum(teleMsg.getProveoId());
				// Look into master data for the equip Number
				if (!mdEquipMap.isEmpty()) {
					if (!mdEquipMap.contains(teleMsg.getUnitName()
							.toUpperCase())) {
						LOG.error(
								"Incoming equipment name not matched with Master data. MSG ID: {}",
								teleMsg.getMessageId());
						// startTime = new Date().getTime();
						if (logLevel
								.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
										.name())) {
							if (!msgLogged) {
								irmtabRef = _irmfacade.storeRawMsg(inMessage,
										dtflString);
								msgLogged = Boolean.TRUE;
							}
						}
						if (irmtabRef > 0) {
							sendErrInfo(
									EnumExceptionCodes.WNOMD,
									irmtabRef,
									("Equip Name: "
											+ teleMsg.getUnitName()
													.toUpperCase()
											+ " not in Master Table.Tele Message ID:" + teleMsg
											.getMessageId()));

						}
					}
				}
				equipPosIns.setUnitName(teleMsg.getUnitName());
				equipPosIns.setUnitModel(teleMsg.getModelType());
				equipPosIns.setIdArrFlight(new BigDecimal(0));
				equipPosIns.setIdDepFlight(new BigDecimal(0));
				equipPosIns.setRecStatus(" ");
				if (eventType.value().equalsIgnoreCase(EventTypes.GEO.value())) {
					if (!HpUfisUtils.isNullOrEmptyStr(teleMsg.getGeo()
							.getLong())) {
						equipPosIns.setLongitutde(toDouble(teleMsg.getGeo()
								.getLong()));
					}
					if (!HpUfisUtils
							.isNullOrEmptyStr(teleMsg.getGeo().getLat())) {
						equipPosIns.setLatitude(toDouble(teleMsg.getGeo()
								.getLat()));
					}
					if (!HpUfisUtils.isNullOrEmptyStr(teleMsg.getGeo().getX())) {
						equipPosIns.setX(toDouble(teleMsg.getGeo().getX()));
					}
					if (!HpUfisUtils.isNullOrEmptyStr(teleMsg.getGeo().getY())) {
						equipPosIns.setY(toDouble(teleMsg.getGeo().getY()));
					}
					if (!mdEquipLocMap.isEmpty()) {
						if (!mdEquipLocMap.contains(teleMsg.getGeo().getArea()
								.toUpperCase())) {
							LOG.error(
									"Incoming equipment Area/Loc not matched with Master data. MSG ID: {}",
									teleMsg.getMessageId());
							if (logLevel
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(
											inMessage, dtflString);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(
										EnumExceptionCodes.WNOMD,
										irmtabRef,
										("Equip Area/Loc: "
												+ teleMsg.getGeo().getArea()
														.toUpperCase()
												+ " not in Master Table. Tele Message ID:" + teleMsg
												.getMessageId()));

							}
						}
					}
					equipPosIns.setUserDefineArea(teleMsg.getGeo().getArea());
					// LOG.debug("Info Update Time(Timestamp): {}",
					// chgStringToDate(teleMsg.getGeo().getTimestamp()));
					equipPosIns.setInfoUpdTime(convertStringToDate(teleMsg
							.getGeo().getTimestamp()));
				} else if (eventType.value().equalsIgnoreCase(
						EventTypes.STATE.value())) {
					equipPosIns.setInfoValue(teleMsg.getState().getName());
					equipPosIns.setInfoType(getInfoTypeFromStateMap(teleMsg
							.getState().getName()));
					// LOG.debug("Curr Status Date(Current Begin Time): {}",
					// chgStringToDate(teleMsg.getState().getCurrBeginTime()));
					equipPosIns.setCurrStatusDate(convertStringToDate(teleMsg
							.getState().getCurrBeginTime()));
					if (!mdEquipLocMap.isEmpty()) {
						if (!mdEquipLocMap.contains(teleMsg.getState()
								.getCurrArea().toUpperCase())) {
							LOG.error(
									"Incoming equipment Area/Loc not matched with Master data. MSG ID: {}",
									teleMsg.getMessageId());
							if (logLevel
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(
											inMessage, dtflString);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(
										EnumExceptionCodes.WNOMD,
										irmtabRef,
										("Equip Area/Loc: "
												+ teleMsg.getState()
														.getCurrArea()
														.toUpperCase()
												+ " not in Master Table. Tele Message ID:" + teleMsg
												.getMessageId()));

							}
						}
					}
					equipPosIns.setCurrUnitArea(teleMsg.getState()
							.getCurrArea());
					equipPosIns.setCurrValue(teleMsg.getState().getCurrValue());
					if (equipPosIns.getInfoType() != null) {
						equipPosIns.setCurrValueCode(getStCurrValueCodeFromMap(
								equipPosIns.getInfoType(), teleMsg.getState()
										.getCurrValue()));
					}

					// search in AFTTAB for validity of flight
					equipPosIns.setCurrArrFltnum(formatFltNum(teleMsg
							.getState().getCurrFLNInb()));
					EntDbAfttab criteriaParams = new EntDbAfttab();
					// startTime = new Date().getTime();
					if (_startupInitSingleton.isAfttabOn()
							&& !HpUfisUtils.isNullOrEmptyStr(equipPosIns
									.getCurrArrFltnum())) {
						criteriaParams.setFlno(equipPosIns.getCurrArrFltnum());
						/*
						 * criteriaParams
						 * .setFlno(HpUfisUtils.formatCedaFltn(equipPosIns
						 * .getCurrArrFltnum()));
						 * LOG.info("Curr Arr Flight Number: {}", HpUfisUtils
						 * .formatCedaFltn(equipPosIns.getCurrArrFltnum()));
						 */
						// get the configurable time range offset
						short from = 0;
						short to = 0;
						from = HpProveoConfig.getAfttabRangeFromOffset();
						to = HpProveoConfig.getAfttabRangeToOffset();
						LOG.info("FLNO: " + equipPosIns.getCurrArrFltnum()
								+ "  from: " + from + "   To:" + to);
						// find flight from afttab
						// BigDecimal uaft =
						// aftBean.getUrnoByCriteriaQuery(criteriaParams, type,
						// adid);
						BigDecimal urno = aftBean.getUrnoByFilterQuery(
								criteriaParams, ADID.A, from, to);

						if (urno == null) {
							// drop message
							LOG.error("PROVEO Inbound(Arrival) Flight not found from afttab. Dropping the message.");
							if (logLevel
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(
											inMessage, dtflString);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(
										EnumExceptionCodes.ENOFL,
										irmtabRef,
										"FltNum: "
												+ equipPosIns
														.getCurrArrFltnum()
												+ (" Date Range: " + from
														+ "  to: " + to
														+ ". Tele Message ID:" + teleMsg
															.getMessageId()));

							}
							LOG.error("Message Dropped: TeleMSG Message ID:{}",
									teleMsg.getMessageId());
							// return false;
							continue;
						} else {
							LOG.info("Inbound Flight Found in AFTTAB. URNO:"
									+ urno);
							equipPosIns.setIdArrFlight(urno);
						}
						// LOG.info("Total Duration on searching inBound flight from AFTTAB (in ms): {}",
						// new Date().getTime() - startTime);
					}
					equipPosIns.setCurrDepFltnum(formatFltNum(teleMsg
							.getState().getCurrFLNOutb()));
					if (_startupInitSingleton.isAfttabOn()
							&& !HpUfisUtils.isNullOrEmptyStr(equipPosIns
									.getCurrDepFltnum())) {
						/*
						 * LOG.info("Current Dept Flight Number : {}",
						 * HpUfisUtils
						 * .formatCedaFltn(equipPosIns.getCurrDepFltnum()));
						 */
						/*
						 * criteriaParams
						 * .setFlno(HpUfisUtils.formatCedaFltn(equipPosIns
						 * .getCurrDepFltnum()));
						 */
						criteriaParams.setFlno(equipPosIns.getCurrDepFltnum());

						// find flight from afttab
						// BigDecimal uaft =
						// aftBean.getUrnoByCriteriaQuery(criteriaParams, type,
						// adid);
						short from = 0;
						short to = 0;
						from = HpProveoConfig.getAfttabRangeFromOffset();
						to = HpProveoConfig.getAfttabRangeToOffset();
						LOG.info("FLNO: " + equipPosIns.getCurrDepFltnum()
								+ "  from: " + from + "   To:" + to);
						// startTime = new Date().getTime();
						BigDecimal urnoD = aftBean.getUrnoByFilterQuery(
								criteriaParams, ADID.D, from, to);
						if (urnoD == null) {
							// drop message
							LOG.error("PROVEO OutBound(Departure) Flight not found from afttab.");
							if (logLevel
									.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
											.name())) {
								if (!msgLogged) {
									irmtabRef = _irmfacade.storeRawMsg(
											inMessage, dtflString);
									msgLogged = Boolean.TRUE;
								}
							}
							if (irmtabRef > 0) {
								sendErrInfo(
										EnumExceptionCodes.ENOFL,
										irmtabRef,
										("Flno: "
												+ equipPosIns
														.getCurrDepFltnum()
												+ " Date Range: " + from
												+ "  to: " + to
												+ ". Tele Message ID:" + teleMsg
												.getMessageId()));

							}
							LOG.error("Message Dropped.TeleMSG Message ID:{}",
									teleMsg.getMessageId());
							// return false;
							continue;
						} else {
							LOG.info("Outbound Flight Found in AFTTAB. URNO:"
									+ urnoD);
							equipPosIns.setIdDepFlight(urnoD);
						}
						// LOG.info("Total Duration on searching out bound flight from AFTTAB (in ms): {}",
						// new Date().getTime() - startTime);
					}

					equipPosIns.setPrevStatusDate(convertStringToDate(teleMsg
							.getState().getPrevBeginTime()));
					equipPosIns.setPrevUnitArea(teleMsg.getState()
							.getPrevArea());
					equipPosIns.setPrevValue(teleMsg.getState().getPrevValue());
					equipPosIns.setPrevValueCode(getStCurrValueCodeFromMap(
							equipPosIns.getInfoType(), teleMsg.getState()
									.getPrevValue()));
				} else if (eventType.value().equalsIgnoreCase(
						EventTypes.NOTIFY.value())) {
					LOG.warn("Notification message is being ignored.");
					LOG.warn("Dropping the notification message. Message: {}",
							message);
					if (logLevel
							.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR
									.name())) {
						if (!msgLogged) {
							irmtabRef = _irmfacade.storeRawMsg(inMessage,
									dtflString);
							msgLogged = Boolean.TRUE;
						}
					}
					if (irmtabRef > 0) {
						sendErrInfo(EnumExceptionCodes.EWEVT, irmtabRef,
								("EventType is " + EventTypes.NOTIFY.value()
										+ ". Tele Message ID:" + teleMsg
										.getMessageId()));

					}
					// return false;
					continue;
				}
				// cmdForBroadcasting =
				// HpUfisAppConstants.UfisASCommands.IRT.toString();
				equipPosPersistRec = _updateEquipPos
						.saveEqiupPosUpdate(equipPosIns);
				if (equipPosPersistRec != null) {
					// equipPosPersistRecs.add(equipPosPersistRec);
					if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
						// send notification message to topic
						clsBlUfisBCTopic.sendNotification(
								isLast.booleanValue(),
								HpUfisAppConstants.UfisASCommands.IRT, null,
								null, equipPosPersistRec);
					}
					if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
						// if topic not defined, send notification to queue
						ufisQueueProducer.sendNotification(isLast,
								HpUfisAppConstants.UfisASCommands.IRT, null,
								null, equipPosPersistRec);
					}
					LOG.debug("EQUIP_POS Record change communicated to Ceda.");
				} else {
					LOG.error(
							"Failed on saving the record to DB. Message ID: {}",
							teleMsg.getMessageId());
				}
				// _updateEquipPos.saveEqiupPosUpdate(equipPosIns);
				LOG.info(" Time For Processing Telemetry Message:"
						+ (new Date().getTime() - startTime1));
			}
			// cmdForBroadcasting =
			// HpUfisAppConstants.UfisASCommands.IRT.name();
			/*
			 * sendProveoUpdateToCedaInJson(equipPosPersistRecs,
			 * cmdForBroadcasting, isLast);
			 */

			// LOG.debug("EQUIP_POS Records change has beed communicated to Ceda.");

			return true;
		} catch (JAXBException e) {
			LOG.error("!!!!ERROR(JAXBException):  " + e);
			return false;
		} catch (Exception e) {
			LOG.error("!!!!ERROR(Exception):  " + e);
			return false;
		}
	}

	private Double toDouble(String str) {
		try {
			return Double.parseDouble(str);
		} catch (Exception e) {
			LOG.error("ERROR!!! in parsing the string value to Double:" + e);
			return null;
		}
		// return 0.0;
	}

	private String getInfoTypeFromStateMap(String name) {
		StringTokenizer st;
		try {
			if (stateInfoMap.isEmpty()) {
				List<Object> stateInfoList = _startupInitSingleton
						.getStateNameMapList();
				for (Object obj : stateInfoList) {
					st = new StringTokenizer(obj.toString(), ":");
					if (st.countTokens() > 1) {
						stateInfoMap.put(st.nextToken(), st.nextToken());
					}
				}
			}
			if (!stateInfoMap.isEmpty() && stateInfoMap.containsKey(name)) {
				return stateInfoMap.get(name);
			}

			LOG.warn("State Name is not in possible values.(Check for the possible values in Configuration file)");
		} catch (Exception e) {
			LOG.error("ERROR!!! in getting info Type of state:" + e);
		}
		return null;
	}

	private String getStCurrValueCodeFromMap(String infoType, String currValue) {
		StringTokenizer st;
		StringTokenizer ist;
		StringTokenizer iist;
		String key;
		String value;
		String temp;
		HashMap<String, String> valueMap;
		List<String> valuesList;
		try {
			if (stateCurrValueMap.isEmpty()) {
				List<Object> stateInfoList = _startupInitSingleton
						.getStateCurrValueList();
				for (Object obj : stateInfoList) {
					st = new StringTokenizer(obj.toString(), ":");
					if (st.countTokens() > 1) {
						valueMap = new HashMap<String, String>();
						valuesList = new ArrayList<String>();
						key = st.nextToken();
						value = st.nextToken();
						if (!HpUfisUtils.isNullOrEmptyStr(value)) {
							ist = new StringTokenizer(value, ";");
							while (ist.hasMoreTokens()) {
								temp = ist.nextToken();
								if (!HpUfisUtils.isNullOrEmptyStr(temp)) {
									valuesList.add(temp);
								}
							}
							if (valuesList.size() > 0) {
								for (String str : valuesList) {
									iist = new StringTokenizer(str, "-");
									if (iist.countTokens() > 1) {
										valueMap.put(iist.nextToken(),
												iist.nextToken());
									}
								}

							}
						}
						stateCurrValueMap.put(key, valueMap);
					}
				}
			}
			if (!stateCurrValueMap.isEmpty()
					&& !HpUfisUtils.isNullOrEmptyStr(infoType)
					&& stateCurrValueMap.containsKey(infoType)) {

				if (!stateCurrValueMap.get(infoType).isEmpty()) {
					if (!HpUfisUtils.isNullOrEmptyStr(currValue)
							&& stateCurrValueMap.get(infoType).containsKey(
									currValue)) {
						return stateCurrValueMap.get(infoType).get(currValue);
					}
				}
			}
			LOG.warn("State CurrValue is not in possible values.(Check for the possible values in Configuration file)");
		} catch (Exception e) {
			LOG.error("ERROR!!! in reading Current Value code from map:" + e);
		}
		return null;
	}

	public static String formatFltNum(String fltNum) {
		String flightNum = null;
		try {
			if (!HpUfisUtils.isNullOrEmptyStr(fltNum)
					&& (fltNum.length() > 2)
					&& (fltNum.substring(0, 2).equalsIgnoreCase("EK") || fltNum
							.substring(0, 2).equalsIgnoreCase("QF"))) {
				flightNum = fltNum.substring(0, 2)
						+ " "
						+ HpUfisUtils
								.formatCedaFltn(fltNum.substring(2).trim());
			} else {
				return fltNum;
			}
		} catch (Exception e) {
			LOG.error("ERROR!!! in Formatting the Flight Number:" + e);
		}
		return flightNum;

	}

	// public static Date chgStringToDate(String dateTimeString) {
	// // Calendar cal = Calendar.getInstance();
	// // LOG.info("DateString:" + dateTimeString);
	// try {
	// DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	// HpUfisCalendar utcCal = new
	// HpUfisCalendar(dateFormat.parse(dateTimeString));
	// // LOG.info("UTC cal:"+utcCal.getTime());
	// utcCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC,
	// EnumTimeInterval.Hours);
	// // LOG.info("UTC cal after OFFSET:"+utcCal.getTime());
	// return utcCal.getTime();
	// } catch (Exception e) {
	// LOG.error("ERROR!!! in Formatting the Date and Time:" + e);
	// }
	// return null;
	// }
	private static Date convertStringToDate(String dateTimeString) {
		Date result = null;
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat df2 = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			if (dateTimeString != null) {
				HpUfisCalendar cal = new HpUfisCalendar(
						df.parse(dateTimeString));

				df.setTimeZone(HpEKConstants.utcTz);
				String date = df.format(cal.getTime());
				// LOG.info("UTC:" + df2.parse(date));
				result = df2.parse(date);
			}
		} catch (Exception e) {
			LOG.error("timezone convertion error" + e);
		}
		return result;
	}

	private void sendErrInfo(EnumExceptionCodes expCode, Long irmtabRef,
			String desc) throws IOException, JsonGenerationException,
			JsonMappingException {
		List<Object> recList = new ArrayList<Object>();
		List<String> dataList = new ArrayList<String>();
		dataList.add(irmtabRef.toString());
		dataList.add(HpUfisAppConstants.CON_IRMTAB);
		dataList.add(expCode.name());
		dataList.add(HpUfisAppConstants.CON_EXCEP_CATG_INT);
		dataList.add(desc);
		dataList.add(dtflStr);

		recList.add(dataList);

		// need to send to Queue
		String msg = HpUfisMsgFormatter.formExceptionData(recList,
				UfisASCommands.IRT.name(), irmtabRef, true, dtflStr);
		// send to ERRORQUEUE
		_blUfisExcepQ.sendMessage(msg);
		LOG.debug("Sent Error Update from PROVEO to Data Loader.");

	}

	/*
	 * private void sendProveoUpdateToCedaInJson( List<EntDbEquipPos>
	 * entEquipPosList, String cmd, Boolean isLast) throws IOException,
	 * JsonGenerationException, JsonMappingException { List<String> idList = new
	 * ArrayList<>(); List<Object> dataList = new ArrayList<Object>();
	 * List<Object> oldList = new ArrayList<>(); List<Object> dataRec = new
	 * ArrayList<Object>(); StringBuilder idStr = new StringBuilder(); String
	 * urno; List<String> idFlightList = new ArrayList<String>(); try { for
	 * (EntDbEquipPos entEquipPos : entEquipPosList) { urno =
	 * entEquipPos.getIdDepFlight().toString(); // get the HEAD info
	 * idFlightList.add(urno); dataRec.add(urno);
	 * dataRec.add(entEquipPos.getIdArrFlight().toString());
	 * dataRec.add(entEquipPos.getTrackNum());
	 * dataRec.add(entEquipPos.getUnitName());
	 * dataRec.add(entEquipPos.getUnitModel());
	 * dataRec.add(entEquipPos.getLongitutde());
	 * dataRec.add(entEquipPos.getLatitude()); dataRec.add(entEquipPos.getX());
	 * dataRec.add(entEquipPos.getY());
	 * dataRec.add(entEquipPos.getUserDefineArea()); if
	 * (entEquipPos.getInfoUpdTime() != null) {
	 * dataRec.add(df.format(entEquipPos.getInfoUpdTime())); }
	 * dataRec.add(entEquipPos.getInfoType());
	 * dataRec.add(entEquipPos.getInfoValue()); if
	 * (entEquipPos.getCurrStatusDate() != null) {
	 * dataRec.add(df.format(entEquipPos.getCurrStatusDate())); } if
	 * (entEquipPos.getPrevStatusDate() != null) {
	 * dataRec.add(df.format(entEquipPos.getPrevStatusDate())); }
	 * dataRec.add(entEquipPos.getCurrValue());
	 * dataRec.add(entEquipPos.getPrevValue());
	 * dataRec.add(entEquipPos.getCurrUnitArea());
	 * dataRec.add(entEquipPos.getPrevUnitArea());
	 * dataRec.add(entEquipPos.getRecStatus());
	 * 
	 * dataList.add(dataRec); idStr.append("\"" + entEquipPos.getId() + "\",");
	 * 
	 * if (UfisASCommands.URT.toString().equals(cmd) && oldEquipPos != null) {
	 * oldList.add(oldEquipPos.getIdDepFlight().toString());
	 * oldList.add(oldEquipPos.getIdArrFlight().toString());
	 * oldList.add(oldEquipPos.getTrackNum());
	 * oldList.add(oldEquipPos.getUnitName());
	 * oldList.add(oldEquipPos.getUnitModel());
	 * oldList.add(oldEquipPos.getLongitutde().toString());
	 * oldList.add(oldEquipPos.getLatitude().toString());
	 * oldList.add(oldEquipPos.getX().toString());
	 * oldList.add(oldEquipPos.getY().toString());
	 * oldList.add(oldEquipPos.getUserDefineArea());
	 * oldList.add(df.format(oldEquipPos.getInfoUpdTime()));
	 * oldList.add(oldEquipPos.getInfoType());
	 * oldList.add(oldEquipPos.getInfoValue());
	 * oldList.add(df.format(oldEquipPos.getCurrStatusDate()));
	 * oldList.add(df.format(oldEquipPos.getPrevStatusDate()));
	 * oldList.add(oldEquipPos.getCurrValue());
	 * oldList.add(oldEquipPos.getPrevValue());
	 * oldList.add(oldEquipPos.getCurrUnitArea());
	 * oldList.add(oldEquipPos.getPrevUnitArea());
	 * oldList.add(oldEquipPos.getRecStatus()); }
	 * 
	 * idList.add(entEquipPos.getId()); } List<EntUfisMsgACT> listAction = new
	 * ArrayList<>(); header.setIdFlight(idFlightList); if (isLast) {
	 * header.setBcnum(-1); } EntUfisMsgACT action = new EntUfisMsgACT();
	 * action.setCmd(cmd); action.setTab("EQUIP_POS"); action.setFld(fldList);
	 * action.setData(dataList); action.setOdat(oldList); action.setId(idList);
	 * action.setSel("WHERE ID IN (" + (idStr.length() > 0 ? idStr.substring(0,
	 * idStr.length() - 2) : idStr + ")")); listAction.add(action); String msg =
	 * HpUfisJsonMsgFormatter.formDataInJson(listAction, header);
	 * clsBlUfisBCTopic.sendMessage(msg); LOG.debug(
	 * "Sent Update Equip Pos  Notification from PROVEO to UMD . Msg:{}", msg);
	 * } catch (Exception e) { LOG.error("ERROR!!!: {}", e); }
	 * 
	 * }
	 */
}
