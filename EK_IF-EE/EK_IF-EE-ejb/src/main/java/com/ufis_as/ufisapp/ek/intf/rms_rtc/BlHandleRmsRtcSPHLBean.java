package com.ufis_as.ufisapp.ek.intf.rms_rtc;

import static com.ufis_as.ufisapp.utils.HpUfisUtils.isNullOrEmptyStr;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPax;
import com.ufis_as.ek_if.macs.entities.EntDbServiceRequest;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;
import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsOrderType;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsServCode;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsTaskStatus;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsWorkArea;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsWorkLoc;
import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;
import com.ufis_as.ufisapp.ek.eao.DlLoadPaxBean;
import com.ufis_as.ufisapp.ek.eao.DlPaxServiceRequestBean;
import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.eao.IDlFltJobTaskLocal;
import com.ufis_as.ufisapp.ek.eao.IDlStaffShiftLocal;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.rms.rtcAssignment.EquipmentDetailsType;
import ek.rms.rtcAssignment.EquipmentType;
import ek.rms.rtcAssignment.FlightIdType;
import ek.rms.rtcAssignment.MetaType;
import ek.rms.rtcAssignment.RTCAssignmentType;
import ek.rms.rtcAssignment.ServiceDetailsType;
import ek.rms.rtcAssignment.StaffDetailsType;
import ek.rms.rtcAssignment.StaffType;
import ek.rms.rtcAssignment.TaskInfoType;
import ek.rms.rtcAssignment.TaskTimesType;

@Stateless
public class BlHandleRmsRtcSPHLBean {

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleRmsRtcSPHLBean.class);
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private IDlFltJobTaskLocal clsDlFltJobTaskLocal;
	@EJB
	private IDlFlighttJobAssignLocal clsDlFltJobAssignLocal;
	@EJB
	private IDlStaffShiftLocal clsDlStaffShiftLocal;
	@EJB
	private DlPaxServiceRequestBean clsDlPaxServiceRequest;
	@EJB
	private DlLoadPaxBean loadPaxBean;
	@EJB
	private DlPaxServiceRequestBean serviceRequestBean;
	
/*	RTCAssignmentType _input;
	TaskInfoType _inputTaskInfo;
	TaskTimesType _inputTaskTimesType;
	List<StaffDetailsType> _inputStaffDetailList;
	List<EquipmentDetailsType> _inputEquipDetail;
	AuditType _inputAudit;
	MetaType _inputMeta;*/
	
	/**
	 * 1.) Validate the mandatory field and Master data. LOG if MD not found. Keep the data.
	 * 2.) find the flight. Dropped if not found.
	 * 3.) check and route to each impl for data status INS, UPD and DEL.
	 * 4.) Plus each staff assigned status FREE or others.. 
	 * @param _input
	 */
	public void processSpecialHandling(RTCAssignmentType _input) throws ParseException {
		
/*		_input = inputRtc;
		_inputMeta = _input.getMeta();
		_inputTaskInfo = _input.getTaskDetails().getTaskInfo();
		_inputTaskTimesType = _input.getTaskDetails().getTaskTimes();
		_inputStaffDetailList = _input.getTaskDetails().getResources().getStaff().getStaffDetails();
		_inputEquipDetail = _input.getTaskDetails().getResources().getEquipment().getEquipmentDetails();
		_inputAudit = _input.getAudit();*/
		
		if(!validateInputData(_input))
			return;
		String subType = _input.getMeta().getSubtype();
		String taskInfoStatus = _input.getTaskDetails().getTaskInfo().getStatus();
		
		BigDecimal urno = getFlightFromInputFlightId(_input.getFlightId());
		if(urno == null){//flight not found
			LOG.debug("Message dropped..");
			return;
		}
		EntDbFltJobTask entJobTask = null;
		String rec_status = " ";
		
		switch (subType) {
		case "INS":
			if ("FREE".equalsIgnoreCase(taskInfoStatus))
				rec_status = "X";
			else
				rec_status = " ";
			entJobTask = processFltJobTask(_input, urno, false, rec_status);
			if (entJobTask != null)
				// pass _input to include Audit, Equipment
				insertAllJobAndShift(_input, entJobTask.getId(), urno,
						rec_status);
			break;
		case "UPD":
			// 2013-11-22 updated by JGO - when auto and manual mark status as free
			//if ("FREE".equalsIgnoreCase(taskInfoStatus))
			if ("FREE".equalsIgnoreCase(taskInfoStatus)
					|| "AUTO".equalsIgnoreCase(taskInfoStatus)
					|| "MANUAL".equalsIgnoreCase(taskInfoStatus))
				rec_status = "X";
			else
				rec_status = " ";

			entJobTask = processFltJobTask(_input, urno, true, rec_status);
			if (entJobTask != null) {
				processFltJobAndShift(_input, urno, entJobTask.getId(),
						rec_status);
			}
			break;
		case "DEL": 
			rec_status = "X";
			entJobTask = processFltJobTask(_input, urno, true, rec_status);
			if (entJobTask != null) {
				processFltJobAndShift(_input, urno, entJobTask.getId(),
						rec_status);
			}
			break;
		}
	}
	
	private void insertAllJobAndShift(RTCAssignmentType _input, String idJobTask, BigDecimal urno, String recStatus) throws ParseException {
		StaffType staff = _input.getTaskDetails().getResources().getStaff();
		List<StaffDetailsType> listStaff = new ArrayList<>();
		if(staff != null)
			listStaff = staff.getStaffDetails();
		
		int equipSize = 0;
		EquipmentType equipment = _input.getTaskDetails().getResources().getEquipment();
		if(equipment != null)
			equipSize = equipment.getEquipmentDetails().size();
		LOG.debug("Total <{}> staff details and <{}> equipments in input msg.", 
				listStaff.size(), equipSize);
		
		for(StaffDetailsType _inputStaffDetail : listStaff){
			//INSERT each job and its shift
			insertJobAndShift(_input, idJobTask, urno, _inputStaffDetail, recStatus);
		}
	}

	/**
	 * - call from insertAllJobAndShift
	 * - to insert each job and shift one by one..
	 * @param _input, idJobTask, urno, _inputStaffDetail, recStatus
	 * @throws ParseException
	 */
	private void insertJobAndShift(RTCAssignmentType _input, String idJobTask,
			BigDecimal urno, StaffDetailsType _inputStaffDetail, String recStatus)
			throws ParseException {
		Date msgSendDate = convertDateToUTC(_input.getMeta().getMessageTime());
		EntDbStaffShift shift = insertShift(idJobTask, urno, _inputStaffDetail, msgSendDate, recStatus);
		
		//INSERT STAFF
		EntDbFltJobAssign jobAssign = new EntDbFltJobAssign();
		if(_inputStaffDetail.getID() != null)//Conditional case
			jobAssign.setStaffNumber(_inputStaffDetail.getID());
		if(_inputStaffDetail.getName() != null)
			jobAssign.setStaffName(_inputStaffDetail.getName());
		
		//Service Detail
		ServiceDetailsType _inputServiceDetailsType = _inputStaffDetail.getServiceDetails();
		if(_inputServiceDetailsType == null){
			LOG.debug("No Service Detail in input Staff <{}>.", _inputStaffDetail.getID());
			return;
		}
		String serCode = null;
		if (_inputServiceDetailsType.getServiceCode() != null) {// check in MD table
			for (EntDbMdRmsServCode servicCode : clsEntStartUpInitSingleton.getMdRmsServCodesList()) {
				if (_inputServiceDetailsType.getServiceCode().equalsIgnoreCase(servicCode.getStaffTypeCode())) {
					if(servicCode.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
						serCode = _inputStaffDetail.getServiceDetails().getServiceCode(); break;
				}
			}
			if(serCode == null)
				LOG.debug("Service Code <{}> is not found in MdRmsServiceCode table.", _inputServiceDetailsType.getServiceCode());
			jobAssign.setSpecialService(_inputServiceDetailsType.getServiceCode());
		}
		String paxSeq = String.valueOf(_inputServiceDetailsType.getPaxSequence());
		// 2013-11-22 updated by JGO - remove pax_ref_num and add pax_name for sphl
		//jobAssign.setPaxRefNum(paxSeq);
		String paxName = _inputServiceDetailsType.getPaxName();
		if (_input.getMeta().getSource().contains("SPHL") && HpUfisUtils.isNotEmptyStr(paxName)) {
			paxName = paxName.replaceAll("/", " ");
			// find the pax from macs
			// check from load pax table by id_flight, paxname and status <> 'x'
			List<EntDbLoadPax> paxes = loadPaxBean.findPaxByFlightAndName(urno, paxName);
			String idLoadPax = "0";
			String idServiceReq = "0";
			if (paxes.size() == 0) {
				LOG.warn("No Pax found in Lod_Pax table by id_flight: {} and pax_name: {}",
						urno,
						paxName);
			} else if (paxes.size() > 1) {
				LOG.warn("More than 1 Pax found in Load_Pax table by id_flight: {} and pax_name: {}",
						urno,
						paxName);
			} else {
				idLoadPax = paxes.get(0).getUuid();
				// look for service request for the pax
				// fisrt round: search by id_load_pax and service_code
				LOG.debug("Find service request from service_request table");
				List<EntDbServiceRequest> requests = serviceRequestBean
						.findPaxRequest(null, idLoadPax, _inputServiceDetailsType.getServiceCode(), null);
				if (requests == null || requests.size() == 0) {
					LOG.warn("No service request found by id_load_pax: {} and service_code: {}", 
							idLoadPax,
							_inputServiceDetailsType.getServiceCode());
					LOG.debug("Change Criteria to find service request");
					// if not found, second round: search by id_flight, inter_ref_number and service_code
					requests = serviceRequestBean.findPaxRequest(urno, null,
							_inputServiceDetailsType.getServiceCode(), paxes.get(0)
									.getPKId().getIntrefnumber());
				}
				
				if (requests == null || requests.size() == 0) {
					LOG.warn("No service request found by id_flight: {}, interRefNumber: {} and service_code: {}",
							urno,
							paxes.get(0).getPKId().getIntrefnumber(),
							_inputServiceDetailsType.getServiceCode());
				} else {
					idServiceReq = requests.get(0).getUuid();
				}
			}
			jobAssign.setIdLoadPax(idLoadPax);
			jobAssign.setIdServiceReq(idServiceReq);
			jobAssign.setPaxName(paxName);
		}

		//to find from SERVICE_REQUEST
/*		EntDbServiceRequest entSerRequest = clsDlPaxServiceRequest.findByIdFlightAndPaxRefNum(urno.intValue(), paxSeq);
		if(entSerRequest != null){
			jobAssign.setIdServiceRequest(entSerRequest.getIntId());//ID???
		}*/
		jobAssign.setChargeNote(String.valueOf(_inputServiceDetailsType.getDocNumber()));
		
		//Equipment
		EquipmentType equipType = _input.getTaskDetails().getResources().getEquipment();
		EquipmentDetailsType equip = new EquipmentDetailsType();
		if(equipType != null &&	!equipType.getEquipmentDetails().isEmpty()){//Conditional case
			equip = equipType.getEquipmentDetails().get(0);//get the first equip ONLY

			if(equip.getID() != null)//Conditional case
				jobAssign.setEquipId(equip.getID());
			if(equip.getDescription() != null)
				jobAssign.setEquipDesc(equip.getDescription());
		}else{
			jobAssign.setEquipId(null);
			jobAssign.setEquipDesc(null);
		}
		
		jobAssign.setTransactUser(_input.getAudit().getChangeUser());
		jobAssign.setTransactDate(convertDateToUTC(_input.getAudit().getChangeTime()));
		
		jobAssign.setMsgSendDate(msgSendDate);
		jobAssign.setIdStaffShift((shift == null)? null : shift.getId());
		jobAssign.setIdFltJobTask(idJobTask);
		jobAssign.setIdFlight(urno);
		jobAssign.setRecStatus(recStatus);
		jobAssign.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		jobAssign.setCreatedUser(HpEKConstants.RMS_RTC_SOURCE);
		jobAssign.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);
		jobAssign.setDataSource(HpEKConstants.RMS_RTC_SOURCE);
	
		EntDbFltJobAssign resultJobAssign = clsDlFltJobAssignLocal.merge(jobAssign);
		if(resultJobAssign != null)
			LOG.debug("Job assign has been processed for Staff <{}>.", resultJobAssign.getStaffNumber());
	}

	/**
	 * - call from insertJobAndShift
	 * - to insert each Shift one by one.. 
	 * @param idJobTask, urno, _inputStaffDetail
	 * @throws ParseException
	 */
	private EntDbStaffShift insertShift(String idJobTask, BigDecimal urno,
			StaffDetailsType _inputStaffDetail, Date msgSendDate, String rec) throws ParseException {
		
		//INSERT Service Detail
		EntDbStaffShift dbStaffShift = new EntDbStaffShift();
		if(_inputStaffDetail.getID() != null)//Conditional case
			dbStaffShift.setStaffNumber(_inputStaffDetail.getID());
		if(_inputStaffDetail.getName() != null)
			dbStaffShift.setStaffName(_inputStaffDetail.getName());
		if(_inputStaffDetail.getWalkie() != null)
			dbStaffShift.setStaffWalkieNo(_inputStaffDetail.getWalkie());
		if(_inputStaffDetail.getQualification() != null)
			dbStaffShift.setStaffQualification(_inputStaffDetail.getQualification());
		
		dbStaffShift.setResourceType(HpEKConstants.RMS_RTC_RESOURCE_TYPE);
		dbStaffShift.setMsgSendDate(msgSendDate);
		dbStaffShift.setIdFltJobTask(idJobTask);
		dbStaffShift.setIdFlight(urno);
		dbStaffShift.setDataSource(HpEKConstants.RMS_RTC_SOURCE);
		dbStaffShift.setRecStatus(rec);
		dbStaffShift.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);			
		dbStaffShift.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		dbStaffShift.setCreatedUser(HpEKConstants.RMS_RTC_SOURCE);
		
		EntDbStaffShift result = clsDlStaffShiftLocal.merge(dbStaffShift);
		if(result != null)
			LOG.debug("Staff Shift has been processed for Staff <{}>.", result.getStaffNumber());
		return result;
	}

	//call from DEL or UPD status
	private void processFltJobAndShift(RTCAssignmentType _input, BigDecimal urno, 
					String idJobTask, String rec_status) throws ParseException {
		
		List<EntDbFltJobAssign> dbAssignedJobList = clsDlFltJobAssignLocal.findByIdFltJobTask(idJobTask);
		if("X".equals(rec_status)){
			if(dbAssignedJobList.isEmpty())
				LOG.debug("No Job_Assign is found for idJobTask <{}>. No deleting will perform.", idJobTask);
			else
				//update as status UPD(FREE) or DEL
				handleDeleteJobAndShift(dbAssignedJobList, _input);
		}
		else
			//update
			handleUpdateJobAndShift(dbAssignedJobList, idJobTask, urno, _input);
	}

	/**
	 * 1.) check the num of staff detail included in input
	 * 2.) update if the staff is found
	 * 3.) remove if the staff is not in input msg
	 * 4.) add if the staff is not in db (new staff)
	 * ** handle for STAFF. Shift is one to one to STAFF
	 * @param dbAssignedJobList, idJobTask, urno, _input
	 * @throws ParseException
	 */
	private void handleUpdateJobAndShift(List<EntDbFltJobAssign> dbAssignedJobList, String idJobTask,
					BigDecimal urno, RTCAssignmentType _input) throws ParseException {
		StaffType staff = _input.getTaskDetails().getResources().getStaff();
		List<StaffDetailsType> _inputStaffDetailList = new ArrayList<>();
		if(staff != null)
			_inputStaffDetailList = staff.getStaffDetails();
		
		EquipmentType equipType = _input.getTaskDetails().getResources().getEquipment();
		List<EquipmentDetailsType> _inputEquipList = new ArrayList<>();
		if(equipType != null)//Conditional case
			 _inputEquipList = equipType.getEquipmentDetails();
		
		LOG.debug("Total <{}> staff details and <{}> equipments in input msg.", _inputStaffDetailList.size(), _inputEquipList.size());
		if(_inputEquipList.size()>1)
			LOG.debug("Multiple Equipments includes in one msg.");
		
		for (EntDbFltJobAssign dbAssignedJob : dbAssignedJobList) {
			if(_inputStaffDetailList.size() == 0){
				LOG.debug("Empty input Staff detail. Will remove the existing staffs.");
				//remove the existing..
				updateJobAndShift("X", dbAssignedJob, new StaffDetailsType(), _input);
			}else{
				for (int i = 0; i < _inputStaffDetailList.size(); i++) {
					StaffDetailsType _inputStaffDetail = _inputStaffDetailList.get(i);
					//StaffNumber is the same??
					if (dbAssignedJob.getStaffNumber().equalsIgnoreCase(_inputStaffDetail.getID())) {
						updateJobAndShift(" ", dbAssignedJob, _inputStaffDetail, _input);
						break;
					}else if(i == _inputStaffDetailList.size()-1){// existing Job_Assign is not in input list 
						// remove the existing..
						updateJobAndShift("X", dbAssignedJob, _inputStaffDetail, _input);
					}
				}
			}
		}
		
		/**- to INSERT newly added job and shift
		 * - which does not exist in db
		 **/
		String status = " ";
		for (StaffDetailsType _inputStaffDetail : _inputStaffDetailList) {
			if (dbAssignedJobList.size() == 0) {
				LOG.debug("Input Staff detail are not existing. Will insert as new.");
				// insert new input staff/shift..
				insertJobAndShift(_input, idJobTask, urno, _inputStaffDetail, status);
			}else{
				for (int i = 0; i < dbAssignedJobList.size(); i++) {
					EntDbFltJobAssign dbAssignedJob = dbAssignedJobList.get(i);
					if(dbAssignedJob.getStaffNumber().equalsIgnoreCase(_inputStaffDetail.getID()))
						break;
					if(i == dbAssignedJobList.size()-1){//reach max of existing record
						// new input record is not found in db
						// insert input record
						//isUpdate = false;
						//updateJobAndShift(" ", dbAssignedJob, _inputStaffDetail, _input);
						insertJobAndShift(_input, idJobTask, urno, _inputStaffDetail, status);
					}
				}
			}
		}
	}
	
	/**
	 * - remove all the staff and shift in db, looping for the list
	 * @param dbAssignedJobList, _input
	 * @throws ParseException
	 */
	private void handleDeleteJobAndShift(List<EntDbFltJobAssign> dbAssignedJobList,
				RTCAssignmentType _input) throws ParseException {
		StaffType staff = _input.getTaskDetails().getResources().getStaff();
		List<StaffDetailsType> _inputStaffDetailList = new ArrayList<>();
		if(staff != null)
			_inputStaffDetailList = staff.getStaffDetails();
		
		//will not remove 'X', if the record is not found..
		for (StaffDetailsType _inputStaffDetail : _inputStaffDetailList) {
			for (int i = 0; i < dbAssignedJobList.size(); i++) {
				EntDbFltJobAssign dbAssignedJob = dbAssignedJobList.get(i);
	
				if (dbAssignedJob.getStaffNumber().equalsIgnoreCase(_inputStaffDetail.getID())) {
					updateJobAndShift("X", dbAssignedJob, _inputStaffDetail, _input);
					break;
				}else if(i == dbAssignedJobList.size()-1){
					LOG.debug("Input Staff <{}> is not found to Delete.");
				}
			}
		}
	}
	
	/**
	 * - handle to update/delete Staff and Shift
	 * @param rec_status, jobAssign, _inputStaffDetail, _input
	 * @throws ParseException
	 */
	private void updateJobAndShift(String rec_status, EntDbFltJobAssign jobAssign, 
			StaffDetailsType _inputStaffDetail, RTCAssignmentType _input) throws ParseException {
		
		Date msgSendDate = convertDateToUTC(_input.getMeta().getMessageTime());
		
		/** Staff:Shift = 1:1 **/
		//DEL or UPD(FREE)
		if("X".equals(rec_status)){
			//update the existing jobAssign
			jobAssign.setRecStatus(rec_status);
			jobAssign.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			jobAssign.setUpdatedUser(HpEKConstants.RMS_RTC_SOURCE);
			jobAssign.setMsgSendDate(msgSendDate);
			
			EntDbStaffShift dbStaffShift = null;
			if(!isNullOrEmptyStr(jobAssign.getIdStaffShift())){
				//one job per one shift
				 dbStaffShift = clsDlStaffShiftLocal.findById(jobAssign.getIdStaffShift());
				if(dbStaffShift != null){
					//update "X" only when Shift exist
					dbStaffShift.setRecStatus(rec_status);
					dbStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					dbStaffShift.setUpdatedUser(HpEKConstants.RMS_RTC_SOURCE);
					dbStaffShift.setMsgSendDate(msgSendDate);
				}
			}
			//merge to DB
			EntDbFltJobAssign resultJob = clsDlFltJobAssignLocal.merge(jobAssign);
			if(resultJob != null)
				LOG.debug("Removed assigned job for Staff <{}>.", resultJob.getStaffNumber());
			
			EntDbStaffShift resultShift = clsDlStaffShiftLocal.merge(dbStaffShift);
			if(resultShift != null)
				LOG.debug("Removed assign shift for Staff <{}>.", resultShift.getStaffNumber());
			
		}else if(" ".equals(rec_status)){
					
			/** UPDATE JOB_ASSIGN **/
			if(_inputStaffDetail.getID() != null)//Conditional case
				jobAssign.setStaffNumber(_inputStaffDetail.getID());
			if(_inputStaffDetail.getName() != null)
				jobAssign.setStaffName(_inputStaffDetail.getName());
			
			//Service Detail
			ServiceDetailsType _inputServiceDetailsType = _inputStaffDetail.getServiceDetails();
			if(_inputServiceDetailsType == null){
				LOG.debug("No Service Detail in input Staff <{}>.", _inputStaffDetail.getID());
				return;
			}
			String serCode = null;
			if (_inputServiceDetailsType.getServiceCode() != null) {// check in MD table
				for (EntDbMdRmsServCode servicCode : clsEntStartUpInitSingleton.getMdRmsServCodesList()) {
					if (_inputServiceDetailsType.getServiceCode().equalsIgnoreCase(servicCode.getStaffTypeCode())) {
						if(servicCode.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
							serCode = _inputStaffDetail.getServiceDetails().getServiceCode(); break;
					}
				}
				if(serCode == null)
					LOG.debug("Service Code <{}> is not found in MdRmsServiceCode table.", _inputServiceDetailsType.getServiceCode());
				jobAssign.setSpecialService(_inputServiceDetailsType.getServiceCode());
			}
			String paxSeq = String.valueOf(_inputServiceDetailsType.getPaxSequence());
			// 2013-11-22 updated by JGO - remove pax_ref_num and add pax_name for sphl
			//jobAssign.setPaxRefNum(paxSeq);
			String paxName = _inputServiceDetailsType.getPaxName();
			if (_input.getMeta().getSource().contains("SPHL") && HpUfisUtils.isNotEmptyStr(paxName)) {
				paxName = paxName.replaceAll("/", " ");
				// find the pax from macs
				// check from load pax table by id_flight, paxname and status <> 'x'
				List<EntDbLoadPax> paxes = loadPaxBean.findPaxByFlightAndName(jobAssign.getIdFlight(), paxName);
				String idLoadPax = "0";
				String idServiceReq = "0";
				if (paxes.size() == 0) {
					LOG.warn("No Pax found in Lod_Pax table by id_flight: {} and pax_name: {}",
							jobAssign.getIdFlight(),
							paxName);
				} else if (paxes.size() > 1) {
					LOG.warn("More than 1 Pax found in Load_Pax table by id_flight: {} and pax_name: {}",
							jobAssign.getIdFlight(),
							paxName);
				} else {
					idLoadPax = paxes.get(0).getUuid();
					// look for service request for the pax
					// fisrt round: search by id_load_pax and service_code
					LOG.debug("Find service request from service_request table");
					List<EntDbServiceRequest> requests = serviceRequestBean
							.findPaxRequest(null, idLoadPax, _inputServiceDetailsType.getServiceCode(), null);
					if (requests == null || requests.size() == 0) {
						LOG.warn("No service request found by id_load_pax: {} and service_code: {}", 
								idLoadPax,
								_inputServiceDetailsType.getServiceCode());
						LOG.debug("Change Criteria to find service request");
						// if not found, second round: search by id_flight, inter_ref_number and service_code
						requests = serviceRequestBean.findPaxRequest(jobAssign.getIdFlight(), null,
								_inputServiceDetailsType.getServiceCode(), paxes.get(0)
										.getPKId().getIntrefnumber());
					}
					
					if (requests == null || requests.size() == 0) {
						LOG.warn("No service request found by id_flight: {}, interRefNumber: {} and service_code: {}",
								jobAssign.getIdFlight(),
								paxes.get(0).getPKId().getIntrefnumber(),
								_inputServiceDetailsType.getServiceCode());
					} else {
						idServiceReq = requests.get(0).getUuid();
					}
				}
				jobAssign.setIdLoadPax(idLoadPax);
				jobAssign.setIdServiceReq(idServiceReq);
				jobAssign.setPaxName(paxName);
			}

			jobAssign.setChargeNote(String.valueOf(_inputServiceDetailsType.getDocNumber()));
			
			//Equipment
			EquipmentType equipType = _input.getTaskDetails().getResources().getEquipment();
			EquipmentDetailsType equip = new EquipmentDetailsType();
			if(equipType != null &&	!equipType.getEquipmentDetails().isEmpty()){//Conditional case
				equip = equipType.getEquipmentDetails().get(0);//get the first equip ONLY

				if(equip.getID() != null)//Conditional case
					jobAssign.setEquipId(equip.getID());
				if(equip.getDescription() != null)
					jobAssign.setEquipDesc(equip.getDescription());
			}else{
				jobAssign.setEquipId(null);
				jobAssign.setEquipDesc(null);
			}
			
			jobAssign.setTransactUser(_input.getAudit().getChangeUser());
			jobAssign.setTransactDate(convertDateToUTC(_input.getAudit().getChangeTime()));
			
			jobAssign.setMsgSendDate(msgSendDate);
			jobAssign.setRecStatus(rec_status);
			jobAssign.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);
			jobAssign.setDataSource(HpEKConstants.RMS_RTC_SOURCE);
			jobAssign.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
			jobAssign.setUpdatedUser(HpEKConstants.RMS_RTC_SOURCE);
			
			//merge JOB_ASSIGN to db
			EntDbFltJobAssign resultJobAssign = clsDlFltJobAssignLocal.merge(jobAssign);
			if(resultJobAssign != null)
				LOG.debug("Assigned job has been updated for Staff <{}>", resultJobAssign.getStaffNumber());
			
			
			/** UPDATE shift **/
			if(isNullOrEmptyStr(jobAssign.getIdStaffShift())){
				LOG.debug("No input shift to update for staff <{}>", jobAssign.getStaffNumber());
			}else{
				EntDbStaffShift dbStaffShift = clsDlStaffShiftLocal.findById(jobAssign.getIdStaffShift());
				if(dbStaffShift != null){//is existing..
					dbStaffShift.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					dbStaffShift.setUpdatedUser(HpEKConstants.RMS_RTC_SOURCE);
				}
				else{
					dbStaffShift = new EntDbStaffShift();
					dbStaffShift.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					dbStaffShift.setCreatedUser(HpEKConstants.RMS_RTC_SOURCE);
				}
			
				if(_inputStaffDetail.getID() != null)//Conditional case
					dbStaffShift.setStaffNumber(_inputStaffDetail.getID());
				if(_inputStaffDetail.getName() != null)
					dbStaffShift.setStaffName(_inputStaffDetail.getName());
				if(_inputStaffDetail.getWalkie() != null)
					dbStaffShift.setStaffWalkieNo(_inputStaffDetail.getWalkie());
				if(_inputStaffDetail.getQualification() != null)
					dbStaffShift.setStaffQualification(_inputStaffDetail.getQualification());
				
				dbStaffShift.setRecStatus(rec_status);
				dbStaffShift.setMsgSendDate(msgSendDate);
				dbStaffShift.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);
				dbStaffShift.setDataSource(HpEKConstants.RMS_RTC_SOURCE);
				dbStaffShift.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);			
				
				EntDbStaffShift resultShift = clsDlStaffShiftLocal.merge(dbStaffShift);
				if(resultShift != null)
					LOG.debug("Assigned shift has been updated for Staff <{}>.", resultShift.getStaffNumber());
			
			}
		}
	}
		
	/**
	 * - handle Flt Tasks from input, including DEL, FREE, UPD, INS
	 * @param input data RTCAssignmentType
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private EntDbFltJobTask processFltJobTask(RTCAssignmentType _input, BigDecimal idFlight, 
			boolean isUpdate, String recStatus)throws ParseException {
		EntDbFltJobTask inputJobTask = null;
		TaskInfoType taskInfo = _input.getTaskDetails().getTaskInfo();
		TaskTimesType taskTimes = _input.getTaskDetails().getTaskTimes();
		
		/**
		 * .. check for Master Data ..
		 */
		String type = null;
		for(EntDbMdRmsOrderType orderType : clsEntStartUpInitSingleton.getMdRmsOrderTypeList()){
			if(orderType.getOrderType().equalsIgnoreCase(taskInfo.getType())){
				if(orderType.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
					type = taskInfo.getType(); break;
			}
		}
		if(type == null)
			LOG.debug("Order type <{}> is not found in MdRmsOrderType table.", taskInfo.getType());
		
		String workArea = null;
		for(EntDbMdRmsWorkArea wArea : clsEntStartUpInitSingleton.getMdRmsWorkAreasList()){
			if(wArea.getWorkArea().trim().equalsIgnoreCase(taskInfo.getWorkArea())){
				if(wArea.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
					workArea = taskInfo.getWorkArea(); break;
			}
		}
		if(workArea == null)
			LOG.debug("Work Area <{}> is not found in MdRmsWorkArea table.", taskInfo.getWorkArea());
		
		String startLoc = null, stau = null;
		for(EntDbMdRmsWorkLoc loc : clsEntStartUpInitSingleton.getMdRmsWorkLocsList()){
			if(loc.getWorkLocationId().equalsIgnoreCase(taskInfo.getStartLoc())){
				if(loc.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
					startLoc = taskInfo.getStartLoc(); 
			}
			if(loc.getWorkLocationId().equalsIgnoreCase(taskInfo.getEndLoc())){
				if(loc.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
					stau = taskInfo.getStartLoc(); 
			}
		}
		if(startLoc == null)
			LOG.debug("Work start location <{}> is not found in MdRmsWorkLoc table.", taskInfo.getStartLoc());
		if(stau == null)
			LOG.debug("Work end location <{}> is not found in MdRmsWorkLoc table.", taskInfo.getEndLoc());
		
		String status = null;
		for(EntDbMdRmsTaskStatus taskStatus : clsEntStartUpInitSingleton.getMdRmsTaskStatusList()){
			if(taskStatus.getTaskStatusId().equalsIgnoreCase(taskInfo.getStatus())){
				if(taskStatus.getStaffTypeCode().equalsIgnoreCase(HpEKConstants.RMS_STAFF_TYPE_SPHL))
					status = taskInfo.getStatus(); break;
			}
		}
		if(status == null)
			LOG.debug("Task Status Id <{}> is not found in MdRmsTaskStatus table.", taskInfo.getStatus());
		
		/**
		 * UPDATE/INSERT Flight Job Task
		 */
		BigDecimal taskId = new BigDecimal(taskInfo.getSubTaskID());
		if (isUpdate) {
			// find the existing record
			inputJobTask = clsDlFltJobTaskLocal.findExistingFltJobTask(
					idFlight, taskId, HpEKConstants.RMS_STAFF_TYPE_SPHL);
			if (inputJobTask != null) {
				inputJobTask.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
				inputJobTask.setUpdatedUser(HpEKConstants.RMS_RTC_SOURCE);
			}else{
				//flt Task is null to UPDATE
				LOG.debug("Input job task is not existing to Update. Message dropped.");
				return null;
			}
		} else {
			inputJobTask = new EntDbFltJobTask();
			inputJobTask.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			inputJobTask.setCreatedUser(HpEKConstants.RMS_RTC_SOURCE);
		}
		inputJobTask.setRecStatus(recStatus);
		inputJobTask.setIdFlight(idFlight);
		inputJobTask.setStaffType(HpEKConstants.RMS_STAFF_TYPE_SPHL);
		inputJobTask.setDataSource(HpEKConstants.RMS_RTC_SOURCE);
		
		/** TaskInfo **/
		inputJobTask.setTaskId(taskId);
		inputJobTask.setTaskType(taskInfo.getType());
		inputJobTask.setTaskTypeDesc(taskInfo.getTypeDesc());
		inputJobTask.setTaskWorkArea(taskInfo.getWorkArea());
		inputJobTask.setTaskStartLoc(taskInfo.getStartLoc());
		inputJobTask.setTaskEndLoc(taskInfo.getEndLoc());
		inputJobTask.setTaskRemark(taskInfo.getRemarks());
		inputJobTask.setTaskStatus(taskInfo.getStatus());
		inputJobTask.setTaskStatusChngTime(convertDateToUTC(taskInfo.getStatusTimestamp()));
		inputJobTask.setTaskCreatedDate(convertDateToUTC(taskInfo.getCreationTime()));
		
		/** TaskTimes **/
		Date planStartDate = convertDateToUTC(taskTimes.getPlnStartTime());
		Date planEndDate = convertDateToUTC(taskTimes.getPlnEndTime());
		
		inputJobTask.setTaskPlanStartDate(planStartDate);
		inputJobTask.setTaskPlanEndDate(planEndDate);
		//calculate plan minute (duration = endTime - startTime)
		inputJobTask.setTaskPlanDuration(new BigDecimal((planEndDate.getTime() - planStartDate.getTime())/1000));

		if(taskTimes.getActStartTime() != null)//Conditional case
			inputJobTask.setTaskActualStartDate(convertDateToUTC(taskTimes.getActStartTime()));
		
		if(taskTimes.getActEndTime() != null)
			inputJobTask.setTaskActualEndDate(convertDateToUTC(taskTimes.getActEndTime()));
		
		Date taskActualStartDate = inputJobTask.getTaskActualStartDate();
		Date taskActualEndDate = inputJobTask.getTaskActualEndDate();
		if(taskActualEndDate != null && taskActualStartDate != null)
			inputJobTask.setTaskActualDuration(new BigDecimal((taskActualEndDate.getTime() - taskActualStartDate.getTime())/1000));
		else
			LOG.debug("One of Task Actual Start or End date is empty/null. Duration will be empty.");
		
		EntDbFltJobTask resultFltJobTask = clsDlFltJobTaskLocal.merge(inputJobTask);
		if(resultFltJobTask != null)
			LOG.debug("Input Flt Job Task has been inserted/updated.");
		
		return resultFltJobTask;
	}

	/**
	 * - convert XMLGregorianCalendar dateTime to Ceda UTC String format
	 * @param XMLGregorianCalendar flightDate
	 * @return Ceda Format Flight String 
	 * @throws ParseException
	 */
	private static String convertXMLGregorianFlDateToUTC(XMLGregorianCalendar flightDate)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = flightDate.toGregorianCalendar().getTime();
		return df.format(utcDate);
	}
	
	/**
	 * @param XMLGregorianCalendar DateTime
	 * @return java DateTime
	 * @throws ParseException
	 */
	private Date convertDateToUTC(XMLGregorianCalendar xmlDate)
			throws ParseException {
		
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		df.setTimeZone(HpEKConstants.utcTz);
		Date utcDate = xmlDate.toGregorianCalendar().getTime();
		return formatter.parse(df.format(utcDate));
	}
	
	/**
	 * - find Flight from DB by input flightID
	 * - criteria include (flno, stoa or stod, adid, org3, des3)
	 * @param flightId
	 * @return flight's URNO 
	 */
	private BigDecimal getFlightFromInputFlightId(FlightIdType flightId) throws ParseException {
		BigDecimal urno = null;
		String org3 = "", des3 = "";
		if(flightId.getDepStn() != null)
			org3 = flightId.getDepStn().trim();
		else if(flightId.getArrStn() != null)
			des3 = flightId.getArrStn().trim();
		
		String adid = flightId.getArrDepFlag().trim();
		String fltSuffix = flightId.getFltSuffix();
		fltSuffix = (fltSuffix == null) ? "" : fltSuffix.trim();
		//get Ceda flight date format in UTC
		String flDate = convertXMLGregorianFlDateToUTC(flightId.getFltDate());
		//form Ceda flight number..
		String flno = flightId.getCxCd() + " "+HpUfisUtils.formatCedaFltn(String.valueOf(flightId.getFltNo())) + fltSuffix;
		
		//ORG3 = DES3 = "DXB"??
		if("DXB".equalsIgnoreCase(org3) && "DXB".equalsIgnoreCase(des3)){
		
			urno = clsAfttabBeanLocal.findUrnoForReturnFlight(flno, flDate, adid);
		
		}else{
			EntDbAfttab entFlight = null;
			switch(adid){
				case "A" :
					entFlight = clsAfttabBeanLocal.findFlightForEKArr(flno, flDate); break;
				case "D" :
					entFlight = clsAfttabBeanLocal.findFlightForEKDept(flno, flDate); break;
			}
			if(entFlight != null)
				urno = entFlight.getUrno();
		}
		return urno;
	}
	
	private boolean validateInputData(RTCAssignmentType _input) {
		
		//check input <Meta> data
		MetaType _inputMeta = _input.getMeta();
		if (_inputMeta.getMessageTime() == null
				|| isNullOrEmptyStr(_inputMeta.getType())
				|| isNullOrEmptyStr(_inputMeta.getSubtype())){
			LOG.debug("Mandatory data for Meta are missing. Message dropped.");
			return false;
		}
		if (!"REA".equalsIgnoreCase(_inputMeta.getType())) {
			LOG.debug("Meta Type is not REA. Message dropped.");
			return false;
		}
		String subType = _inputMeta.getSubtype();
		if(!subType.matches("INS|UPD|DEL")){
			LOG.debug("Meta SubType is not equal to (INS or UPD or DEL). Message dropped.");
			return false;
		}
		
		//check input <FlightId> data
		FlightIdType flightId = _input.getFlightId();
		if(isNullOrEmptyStr(flightId.getCxCd())
				|| flightId.getFltDate() == null
				|| isNullOrEmptyStr(flightId.getArrDepFlag())){
			LOG.debug("Mandatory data for Flight are missing. Message dropped.");
			return false;
		}
		if(!"EK".equals(flightId.getCxCd())){
			LOG.debug("Processing performs only for EK flights. Message dropped.");
			return false;
		}
		if(!flightId.getArrDepFlag().matches("A|D")){
			LOG.debug("Flight arrival/departure flag is not equal to (A or D). Message dropped.");
			return false;
		}
		
		//check input <TaskDetails> data
		TaskInfoType _inputTaskInfo = _input.getTaskDetails().getTaskInfo();
		if(isNullOrEmptyStr(_inputTaskInfo.getType())
				|| isNullOrEmptyStr(_inputTaskInfo.getTypeDesc())
				|| isNullOrEmptyStr(_inputTaskInfo.getWorkArea())
				|| isNullOrEmptyStr(_inputTaskInfo.getStartLoc())
				|| isNullOrEmptyStr(_inputTaskInfo.getEndLoc())
				|| isNullOrEmptyStr(_inputTaskInfo.getStatus())
				|| _inputTaskInfo.getStatusTimestamp() == null
				|| _inputTaskInfo.getCreationTime() == null){
			LOG.debug("Mandatory data for Task Info are missing. Message dropped.");
			return false;
		}
		//check input <TaskTimes> data
		TaskTimesType _inputTaskTimesType = _input.getTaskDetails().getTaskTimes();
		if(_inputTaskTimesType.getPlnStartTime() == null
				|| _inputTaskTimesType.getPlnEndTime() == null){
			LOG.debug("Mandatory data for Task Times are missing. Message dropped.");
			return false;
		}
		
		//check input <Audit> data
		if(_input.getAudit().getChangeTime() == null
				|| isNullOrEmptyStr(_input.getAudit().getChangeUser())){
			LOG.debug("Mandatory data for Audit are missing. Message dropped.");
			return false;
		}
		return true;
	}
}
