package com.ufis_as.ufisapp.ek.intf.rms_rtc;

import static com.ufis_as.ufisapp.utils.HpUfisUtils.isNullOrEmptyStr;

import java.io.StringReader;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.eao.IDlFlighttJobAssignLocal;
import com.ufis_as.ufisapp.ek.eao.IDlFltJobTaskLocal;
import com.ufis_as.ufisapp.ek.eao.IDlStaffShiftLocal;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;

import ek.rms.rtcAssignment.RTCAssignmentType;

@Stateless
public class BlRmsRtcRouterBean {

	private static final Logger LOG = LoggerFactory.getLogger(BlRmsRtcRouterBean.class);
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	private EntStartupInitSingleton clsEntStartUpInitSingleton;
	@EJB
	private IDlFltJobTaskLocal clsDlFltJobTaskLocal;
	@EJB
	private IDlFlighttJobAssignLocal clsDlFltJobAssignLocal;
	@EJB
	private IDlStaffShiftLocal clsDlStaffShiftLocal;
	@EJB
	BlHandleRmsRtcSPHLBean clsBlRmsRtcBean;
	@EJB	
	BlHandleRmsRtcROPBean clsBlRopBean;
	
	@PostConstruct
	private void initialize() {
		try {
			_cnxJaxb = JAXBContext.newInstance(RTCAssignmentType.class);
			_um = _cnxJaxb.createUnmarshaller();
			
		} catch (JAXBException e) {
			LOG.error("JAXBException when creating Unmarshaller");
		}
	}
	
	/**
	 * - check for source type : ROP or SPHL
	 * - and route to handle Biz Bean
	 * @param message
	 */
	public void processRtcSourceRouter(String message){
		RTCAssignmentType _input;
		try {
			JAXBElement<RTCAssignmentType> data = _um.unmarshal(new StreamSource(new StringReader(message)), RTCAssignmentType.class);
			_input = (RTCAssignmentType) data.getValue();
			String source = _input.getMeta().getSource();
			if(isNullOrEmptyStr(source)){
				LOG.warn("Input message type is missing. Message dropped.");
				return;
			}
			switch(source){
				case "dnRTCSPHL" ://go to special handling
					clsBlRmsRtcBean.processSpecialHandling(_input);
					break;
				case "dnRTCROP" : //go to Ramp Operation(ROP)
					clsBlRopBean.processRampOperation(_input);
					break;
				default:
					LOG.debug("Unknown Source. Processing performs only for (dnRTCSPHL or dnRTCROP). Message dropped."); 
					break;
			}
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
		}catch(ParseException e){
			LOG.error("Date time parsing Error : {}", e.getMessage());
		} catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}
}
