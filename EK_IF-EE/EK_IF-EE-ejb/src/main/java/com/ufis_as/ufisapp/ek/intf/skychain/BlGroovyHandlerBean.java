package com.ufis_as.ufisapp.ek.intf.skychain;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;

/**
 * @author lma
 *
 */
@Stateless
public class BlGroovyHandlerBean {
	
	@EJB
	private EntStartupInitSingleton basicDataSingleton;
	
	private static final Logger LOG = LoggerFactory.getLogger(BlGroovyHandlerBean.class);
	
	public String convertToXml(String message) throws Exception {
		String[] roots = new String[] { basicDataSingleton.getUwsGroovyParserPath() };
		GroovyScriptEngine gse = new GroovyScriptEngine(roots);
		Binding binding = new Binding();
		binding.setProperty("rawMessage", message);
		binding.setProperty("LOG", LOG);
		Object output = gse.run(basicDataSingleton.getUwsGroovyParserFileName(), binding);
		return output.toString();
	}

}
