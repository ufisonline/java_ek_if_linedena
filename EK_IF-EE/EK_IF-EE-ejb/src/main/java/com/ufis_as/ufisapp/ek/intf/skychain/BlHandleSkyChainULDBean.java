package com.ufis_as.ufisapp.ek.intf.skychain;

import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EENUM;
import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EMAND;
import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EWALC;
import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EWVAL;
import static com.ufis_as.ufisapp.utils.HpUfisUtils.formatCedaFltn;
import static com.ufis_as.ufisapp.utils.HpUfisUtils.isNullOrEmptyStr;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.skychain.uldinfo.ULDInfo;
import ek.skychain.uldinfo.ULDInfo.FlightID;
import ek.skychain.uldinfo.ULDInfo.Meta;
import ek.skychain.uldinfo.ULDInfo.ULDDetails;

/**
 * @author lma
 * 	@version 1: initial implementation refers to design doc
 *  @version 1.1.0	2013-10-18 	Modified for:	
 * 								1.) internal exception handling to Q_DATS
 * 								2.) Tibco reconnection
 *  							3.) Data changes Notification to Ufis AMQ topic UFIS_IF_NOTIFY_SKYCHAIN
 *
 */
@Stateless
public class BlHandleSkyChainULDBean extends BaseDTO {
	
	enum OP_TYPE {	
		INS, UPD, DEL;
		
		public static List<String> valueList() {
			List<String> values = new ArrayList<>();
			for (OP_TYPE opType : OP_TYPE.values()) {
				values.add(opType.name());
			}
			
			return values;
		}
	};
	
	private static final String SKY_CHAIN = "SkyChain";

	private static final String ULD_INFO = "ULDInfo";

	private static final String EK = "EK";

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleSkyChainULDBean.class);
	
	@EJB
	private DlLoadUldBean dLLoadUldBean;
	
	@EJB
    private IAfttabBeanLocal afttabBean;
	
	@EJB
	private EntStartupInitSingleton basicDataSingleton;
	
	@EJB
	private BlGroovyHandlerBean blGroovyHandlerBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;

	private static final DateFormat DF_FORMATTER = new SimpleDateFormat("dd-MMM-yy"); //05-MAR-13 00.00.00

	private static final DateFormat YYYYMMDD_FORMATTER = new SimpleDateFormat("yyyyMMdd"); //05-MAR-13 00.00.00
	
	private static final DateFormat TZ_DF_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); //2001-12-31T15:20:00
	
	private static final DateFormat YYYYMMDDHHMMSS = new SimpleDateFormat("yyyyMMddHHmmss");
	
	@PostConstruct
	private void initialize()
	{
		try {
			_cnxJaxb = JAXBContext.newInstance(ULDInfo.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = basicDataSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... ");
		}
	}

	public BlHandleSkyChainULDBean() {
	}
	
	private static String convertFlDateToUTC(DateFormat formatter, String flightDate)
			throws ParseException {
		Date flDate =  formatter.parse(flightDate);
		HpUfisCalendar utcDate = new HpUfisCalendar(flDate);
		utcDate.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
		return utcDate.getCedaString();
	}
	
	public boolean processULD(String message, long irmtabRef) {
		 /**  Perform LOAD_ULDS info processing. **/
	   	try {
			   	// clear
			    data.clear();
			    // set urno of irmtab
			    irmtabUrno = irmtabRef;
			    	
	   			ULDInfo inputUldInfo = (ULDInfo) _um.unmarshal(new StreamSource(new StringReader(message)));
	   			
	   			FlightID inputFlightID = inputUldInfo.getFlightID();
	   			Meta inputMeta = inputUldInfo.getMeta();
	   			ULDDetails inputUldDetails = inputUldInfo.getULDDetails();
	   			
	   			if(existsMandatory(inputFlightID, inputMeta, inputUldDetails)){
					LOG.error("Compulsory uld info are empty. Message dropped. ");
					addExptInfo(EMAND.name(), "");
					return false;
				}
	   			
	   			if(!inputFlightID.getCarrierCode().contains(EK)) {
	   				LOG.error("Processing performs only for EK flight. Message will be dropped.");
	   				addExptInfo(EWALC.name(), inputFlightID.getCarrierCode());
	   				return false;
	   			}
	   			
	   			if(!inputMeta.getType().equalsIgnoreCase(ULD_INFO)) {
	   				LOG.warn("Processing performs only for ULDInfo. Message will be dropped.");
	   				addExptInfo(EWVAL.name(), inputMeta.getType());
	   				return false;
	   			}
	   			
	   			if(!OP_TYPE.valueList().contains(inputMeta.getSubtype().trim().toUpperCase())) {
	   				LOG.warn("Processing performs only for INS, UPD, DEL sub type. Message will be dropped.");
	   				addExptInfo(EENUM.name(), inputMeta.getSubtype());
	   				return false;
	   			}
	   			
	   			if(!inputMeta.getSource().equalsIgnoreCase(SKY_CHAIN)) {
	   				LOG.error("Processing performs only for SkyChain. Message will be dropped.");
	   				addExptInfo(EWVAL.name(), inputMeta.getSource());
	   				return false;
	   			}
	   			
	   			
				String flightNumber = String
						.format("%s %s%s", inputFlightID.getCarrierCode(),
								formatCedaFltn(inputFlightID
										.getFlightNumber()), isNullOrEmptyStr(inputFlightID
										.getFlightSuffix()) ? "" : inputFlightID
												.getFlightSuffix());
				
				String flightDate = formatFlightDate(inputFlightID.getFlightDate());
				
				if(isNullOrEmptyStr(flightDate)) {
					LOG.error("Flight Date provided format parsing error is encountered. Message will be dropped.");
					addExptInfo(EWVAL.name(), inputFlightID.getFlightDate());
	   				return false;
				}
				
				LOG.debug("Formed UFIS Flight Number, Flight Date - {}, {}", flightNumber, flightDate);
				
				EntDbAfttab entDbAfttab = afttabBean.findFlightForUldDept(flightNumber, flightDate);
				
				LOG.debug("Flight Search by Departure is done.");
				
				if(entDbAfttab == null) {
					entDbAfttab = afttabBean.findFlightForUldOrg(flightNumber, flightDate);
				}
				
				LOG.debug("Flight Search by Non-Departure is done as the search by Departure is not found.");
				
				if(entDbAfttab == null) {
					LOG.error("Flight provided cannot be found. Message will be dropped.");
					addExptInfo(EnumExceptionCodes.ENOFL.name(), String.format("%s (%s)", flightNumber, flightDate));
	   				return false;
				}
	   			
				LOG.debug("DB - URNO : {}, FLDA : {}, Flight Date Match : {}", entDbAfttab.getUrno(), entDbAfttab.getFlda(), flightDate.equals(entDbAfttab.getFlda()));
				
				boolean isDel = inputMeta.getSubtype().equalsIgnoreCase(OP_TYPE.DEL.name());
				HpUfisAppConstants.UfisASCommands cmdForBroadcasting;
				
				EntDbLoadUld oldLoadUld = null;
				EntDbLoadUld loadUld = (EntDbLoadUld) dLLoadUldBean.getUldNum(entDbAfttab.getUrno(), inputUldDetails.getULDNumber());
				
				//TODO: Log Telex message
				
				if(loadUld == null) {
					if(isDel){
						LOG.error("ULD record does not exist to delete. Message dropped.");
						addExptInfo(EnumExceptionCodes.ENOUD.name(), inputUldDetails.getULDNumber());
						return false;
					}
					
					loadUld = new EntDbLoadUld();
					
					initialize(loadUld, entDbAfttab);
					
					loadUld.setUldCurrFltno(flightNumber);
					loadUld.setUldNumber(inputUldDetails.getULDNumber().trim());
					loadUld.setCreatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
					loadUld.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.IRT;
				} else {
					oldLoadUld = new EntDbLoadUld(loadUld);
					loadUld.setUpdatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
					loadUld.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.URT;
				}
				
				String uldCreatedDate = convertFlDateToUTC(TZ_DF_FORMATTER, inputUldDetails.getCreatedDate().trim());
				loadUld.setUldCreateDate(new HpUfisCalendar(uldCreatedDate).getTime());
				LOG.debug("ULD Created Date : {}, {}", uldCreatedDate, new HpUfisCalendar(uldCreatedDate).getTime());
				
				loadUld.setUldCreateUser(inputUldDetails.getCreatedBy().trim());
	
				if (!isNullOrEmptyStr(inputUldDetails.getModifiedDate())) {
					String uldModifiedDate = convertFlDateToUTC(TZ_DF_FORMATTER, inputUldDetails.getModifiedDate());
					loadUld.setUldUpdateDate(new HpUfisCalendar(uldModifiedDate).getTime());
					LOG.debug("ULD Modified Date : {}, {}", uldModifiedDate, new HpUfisCalendar(uldModifiedDate).getTime());
				}
	
				if (!isNullOrEmptyStr(inputUldDetails.getModifiedBy())) {
					loadUld.setUldUpdateUser(inputUldDetails.getModifiedBy().trim());
				}
	
				loadUld.setIdFlight(entDbAfttab.getUrno());
				loadUld.setDataSource(HpEKConstants.ULD_SOURCE_SKYCHAIN);
				
				String specialHandlingCodes = loadUld.getShcList();
				List<String> specialHandlingCodeList = isNullOrEmptyStr(specialHandlingCodes) ? Collections.EMPTY_LIST : Arrays.asList(specialHandlingCodes.toUpperCase().split("\\.")) ;
				String inputSHC = inputUldDetails.getSpecialHandlingCode().trim();
				if(!specialHandlingCodeList.contains(inputSHC.toUpperCase())) {
					loadUld.setShcList(isNullOrEmptyStr(specialHandlingCodes) ? inputSHC : specialHandlingCodes.concat(String.format(".%s", inputSHC)));
				}
				
				loadUld.setUldOwner(inputUldDetails.getOwnerCarrierCode());
				loadUld.setUldDest3(inputUldDetails.getPointOfLading());
				loadUld.setRecFlag(" ");
				
				if(isDel) {
					loadUld.setRecFlag("X");
					cmdForBroadcasting = HpUfisAppConstants.UfisASCommands.DRT;
				}
					
				EntDbLoadUld resultUld = dLLoadUldBean.merge(loadUld);
				boolean success =  resultUld != null;
				if(success){
					if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
						// send notification message to topic
						ufisTopicProducer.sendNotification(true, cmdForBroadcasting, String.valueOf(resultUld.getIdFlight()), oldLoadUld, resultUld);
					} else {
						// if topic not defined, send notification to queue
						ufisQueueProducer.sendNotification(true, cmdForBroadcasting, String.valueOf(resultUld.getIdFlight()), oldLoadUld, resultUld);
					}
					LOG.debug("Notify ULDs(ULD) from SkyChain");
				}
				LOG.debug("Uld No. {} has {}been processed.", loadUld.getUldNumber(), success ? "" : "not ");
				
				return true;
				
			} catch (JAXBException e) {
				LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
				addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
				return false;
			} catch(Exception e){
				LOG.error("ERROR : {}", e.toString());
				addExptInfo(EnumExceptionCodes.EWACT.name(), e.getMessage());
				return false;
			}
	}
	
	private void initialize(EntDbLoadUld loadUld, EntDbAfttab entDbAfttab) throws ParseException {
		//Set defaults
		if(!isNullOrEmptyStr(entDbAfttab.getStod())) {
			loadUld.setFltDate(YYYYMMDDHHMMSS.parse(entDbAfttab.getStod()));
		}
		loadUld.setIdConxFlight(BigDecimal.ZERO);
		loadUld.setArrDepFlag(entDbAfttab.getAdid()+"");
		loadUld.setUldSubtype(" ");		
	}
	
	private boolean existsMandatory(FlightID inputFlightID, Meta inputMeta,
			ULDDetails inputUldDetails) {
		if(inputFlightID == null || inputMeta == null || inputUldDetails == null) {
			return true;
		}
		
		if (/* TODO: to check null - isNullOrEmptyStr(inputMeta.getMessageID()) - (int) */ 
				isNullOrEmptyStr(inputMeta.getMessageTime())
				|| isNullOrEmptyStr(inputMeta.getSubtype())
				|| isNullOrEmptyStr(inputMeta.getSource())
				|| isNullOrEmptyStr(inputFlightID.getCarrierCode())
				//TODO: to check null - flight no (int)
				|| isNullOrEmptyStr(inputFlightID.getFlightDate()) 
				|| isNullOrEmptyStr(inputUldDetails.getULDNumber())
				|| isNullOrEmptyStr(inputUldDetails.getOwnerCarrierCode())
				|| isNullOrEmptyStr(inputUldDetails.getCreatedBy())
				|| isNullOrEmptyStr(inputUldDetails.getCreatedDate())) {
			
			LOG.warn("Compulsory uld info are empty. Message will be dropped.");
			return true;
		}
		
		return false;
	}

	private String formatFlightDate(String flightDate) {
		try {
			Date date = DF_FORMATTER.parse(flightDate);
			return YYYYMMDD_FORMATTER.format(date);
		} catch (ParseException e) {
			LOG.error("Flight Date format error - {}", e.getMessage());
		}
		
		return null;
	}

}
