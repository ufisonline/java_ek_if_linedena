package com.ufis_as.ufisapp.ek.intf.skychain;

import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EENUM;
import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EMAND;
import static com.ufis_as.ek_if.enumeration.EnumExceptionCodes.EWVAL;
import static com.ufis_as.ufisapp.lib.time.HpUfisCalendar.addMonths;
import static com.ufis_as.ufisapp.lib.time.HpUfisCalendar.daysBetween;
import static com.ufis_as.ufisapp.lib.time.HpUfisCalendar.getCurrentUTCTime;
import static com.ufis_as.ufisapp.lib.time.HpUfisCalendar.of;
import static com.ufis_as.ufisapp.utils.HpUfisUtils.formatCedaFltn;
import static com.ufis_as.ufisapp.utils.HpUfisUtils.isNullOrEmptyStr;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.joda.time.Days;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.enumeration.EnumExceptionCodes;
import com.ufis_as.ek_if.lms.entities.EntDbMsgTelex;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.DlMsgTelexBean;
import com.ufis_as.ufisapp.ek.eao.generic.BaseDTO;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCQueue;
import com.ufis_as.ufisapp.ek.messaging.BlUfisBCTopic;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.skychain.uws.Cargo;
import ek.skychain.uws.Cargo.Meta;
import ek.skychain.uws.Cargo.UWSInfo;
import ek.skychain.uws.Cargo.UWSInfo.BULKULDDetails;
import ek.skychain.uws.Cargo.UWSInfo.FlightInfo;
import ek.skychain.uws.Cargo.UWSInfo.SIULDDetails;
import ek.skychain.uws.Cargo.UWSInfo.UWSULDDetails;
import groovy.util.XmlSlurper;
import groovy.util.slurpersupport.GPathResult;
import groovy.util.slurpersupport.Node;

/**
 * @author lma
 *
 */
@Stateless
public class BlHandleSkyChainUWSBean extends BaseDTO {
	
	enum OP_TYPE {	
		INS, UPD, DEL;
		
		public static List<String> valueList() {
			List<String> values = new ArrayList<>();
			for (OP_TYPE opType : OP_TYPE.values()) {
				values.add(opType.name());
			}
			
			return values;
		}
	};
	
	private static final String SKY_CHAIN = "SkyChain";

	private static final String UWS = "UWS";

	private static final Logger LOG = LoggerFactory.getLogger(BlHandleSkyChainUWSBean.class);
	
	@EJB
	private DlLoadUldBean dLLoadUldBean;
	
	@EJB
	private DlMsgTelexBean dlMsgTelexBean;
	
	@EJB
    private IAfttabBeanLocal afttabBean;
	
	@EJB
	private EntStartupInitSingleton basicDataSingleton;
	
	@EJB
	private BlGroovyHandlerBean blGroovyHandlerBean;
	
	@EJB
	private BlUfisBCQueue ufisQueueProducer;
	
	@EJB
	private BlUfisBCTopic ufisTopicProducer;
	
	private static final SimpleDateFormat YYYYMMDD_FORMATTER = new SimpleDateFormat("yyyyMMdd"); //05-MAR-13 00.00.00
	
	private static final DateFormat YYYYMMDDHHMMSS = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private static final int NO_OF_MONTHS_IN_YEAR = 12;
	
	@PostConstruct
	private void initialize()
	{
		try {
			_cnxJaxb = JAXBContext.newInstance(Cargo.class);
			_um = _cnxJaxb.createUnmarshaller();
			
			tableRef = HpUfisAppConstants.CON_IRMTAB;
			exptCateg = HpUfisAppConstants.CON_EXCEP_CATG_INT;
			dtfl = basicDataSingleton.getDtflStr();
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... ");
		}
	}

	public BlHandleSkyChainUWSBean() {
	}
	
	public boolean processUWS(String message, long irmtabRef) {
		/**  Perform UWS info processing. **/
		try {
			// clear
		    data.clear();
		    // set urno of irmtab
		    irmtabUrno = irmtabRef;
		    
			String convertToXml = blGroovyHandlerBean.convertToXml(message);
			LOG.debug("Converted Xml by Groovy :\n{}", convertToXml);
			
			Cargo inputUWSInfo = (Cargo) _um.unmarshal(new StreamSource(new StringReader(convertToXml)));
			
			Meta meta = inputUWSInfo.getMeta();
			
			UWSInfo uwsInfo = inputUWSInfo.getUWSInfo();
			
			if(existsMandatory(meta, uwsInfo)) {
				LOG.warn("Compulsory uws info are empty. Message will be dropped.");
				addExptInfo(EMAND.name(), "");
				return false;
			}
			
			if(!meta.getType().equalsIgnoreCase(UWS)) {
				LOG.warn("Processing performs only for UWS. Message will be dropped.");
				addExptInfo(EWVAL.name(), meta.getType());
   				return false;
			}

			if(!OP_TYPE.UPD.name().equalsIgnoreCase(meta.getSubtype().trim())) {
				LOG.warn("Processing performs only for UPD sub type. Message will be dropped.");
				addExptInfo(EENUM.name(), meta.getSubtype());
				return false;
			}
			
			if(!meta.getSource().equalsIgnoreCase(SKY_CHAIN)) {
				LOG.error("Processing performs only for SkyChain. Message will be dropped.");
   				addExptInfo(EWVAL.name(), meta.getSource());
   				return false;
			}
			
			FlightInfo inputFlightID = uwsInfo.getFlightInfo();
			String flightNumber = formatFlightNo(inputFlightID.getFlightNumber());
			LOG.debug("Airport Code - {}, Departure Date of Month - {}, Final Msg Indicator - {}, Original Flight No - {}, Flight No formatted - {}", inputFlightID.getAirportCode(), inputFlightID.getDateOfMonth(), inputFlightID.getFinalMessageIndicator(), inputFlightID.getFlightNumber(), flightNumber);
			
			String flightDate = createFlightDateFrom(inputFlightID.getDateOfMonth());
			
			if(isNullOrEmptyStr(flightDate)) {
				LOG.error("Departure date cannot be constructed. Message will be dropped.");
				addExptInfo(EWVAL.name(), inputFlightID.getDateOfMonth());
   				return false;
			}
			
			LOG.debug("Flight Number, Departure Date - {}, {}", flightNumber, flightDate);
			EntDbAfttab entDbAfttab = afttabBean.findFlightForUws(flightNumber, flightDate);
			
			if(entDbAfttab == null) {
				LOG.error("Flight provided cannot be found. Message will be dropped.");
				addExptInfo(EnumExceptionCodes.ENOFL.name(), String.format("%s (%s)", flightNumber, flightDate));
   				return false;
			}

			LOG.debug("URNO from DB - {}", entDbAfttab.getUrno());
			LOG.debug("FLUT : {}, {}", entDbAfttab.getFlut(), flightDate.equals(entDbAfttab.getFlut()));

			EntDbMsgTelex msgTelex = createMessageTelex(message, uwsInfo, entDbAfttab.getUrno());
			if(msgTelex == null) {
				LOG.error("Message Telex persisting encounter error. It won't be persisted.");
			}
			
			List<EntDbLoadUld> existingLoadUlds = dLLoadUldBean.findUldsByFlight(entDbAfttab.getUrno());
			List<EntDbLoadUld> newLoadUlds = new ArrayList<>();
			Map<EntDbLoadUld, EntDbLoadUld> updatedLoadUlds = new HashMap<>();
			
			//TODO: to confirm mei => if no value eg weight status in xml but there is in db, shall update with blank???
			for (UWSULDDetails uwsUld : uwsInfo.getUWSULDDetails()) {
				//TODO: to refactor ummmm
				EntDbLoadUld loadUld = existedLoadUlds(existingLoadUlds, uwsUld.getULDNumber());
				EntDbLoadUld originalUld = null;
				boolean isExisted = true;
				if(loadUld == null) {
					loadUld = new EntDbLoadUld();
					isExisted = false;
					initialize(loadUld, entDbAfttab);
				} else {
					originalUld = new EntDbLoadUld(loadUld);
				}
				loadUld.setIdFlight(entDbAfttab.getUrno());
				loadUld.setUldCurrFltno(flightNumber);
				loadUld.setUldNumber(uwsUld.getULDNumber());
				loadUld.setUldDest3(uwsUld.getDest3());
				loadUld.setUldWeight(uwsUld.getWeight()+"");
				loadUld.setUldWeightStatus(uwsUld.getWeightStatus());
				loadUld.setUldLoadCat1(uwsUld.getLoadCat1());
				loadUld.setUldLoadCat2(uwsUld.getLoadCat2());
				loadUld.setUldContourCode(uwsUld.getLoadContourCode());
				loadUld.setUldContourNum(uwsUld.getLoadContourNumber());
				loadUld.setShcList(uwsUld.getShcList());
				loadUld.setShcRemarks(uwsUld.getShcRemarks());
				loadUld.setDataSource(HpEKConstants.ULD_SOURCE_SKYCHAIN);
				loadUld.setRecFlag(" ");

				if(isExisted) {
					loadUld.setUpdatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
					loadUld.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
					updatedLoadUlds.put(loadUld, originalUld);
				} else {
					loadUld.setCreatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
					loadUld.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
					newLoadUlds.add(loadUld);
				}
				
			}
			
			if(!uwsInfo.getBULKULDDetails().isEmpty()) {
				//Delete existing bulk records regarding the provided flight 
				dLLoadUldBean.deleteBulkUldsByFlight(entDbAfttab.getUrno());
			}
			
			for (BULKULDDetails bulkUld : uwsInfo.getBULKULDDetails()) {
				//TODO: to refactor ummmm
				EntDbLoadUld loadUld = new EntDbLoadUld();
				initialize(loadUld, entDbAfttab);
				loadUld.setUldNumber(bulkUld.getULDNumber());
				loadUld.setIdFlight(entDbAfttab.getUrno());
				loadUld.setUldCurrFltno(flightNumber);
				loadUld.setUldDest3(bulkUld.getDest3());
				loadUld.setUldWeight(bulkUld.getWeight()+"");
				loadUld.setUldWeightStatus(bulkUld.getWeightStatus());
				loadUld.setUldLoadCat1(bulkUld.getLoadCat1());
				loadUld.setUldLoadCat2(bulkUld.getLoadCat2());
				loadUld.setUldContourCode(bulkUld.getLoadContourCode());
				loadUld.setUldContourNum(bulkUld.getLoadContourNumber());
				loadUld.setShcList(bulkUld.getShcList());
				loadUld.setShcRemarks(bulkUld.getShcRemarks());
				loadUld.setDataSource(HpEKConstants.ULD_SOURCE_SKYCHAIN);
				loadUld.setRecFlag(" ");
				loadUld.setCreatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
				loadUld.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
				
				newLoadUlds.add(loadUld);
			}
			
			
			for (SIULDDetails siUld : uwsInfo.getSIULDDetails()) {
				EntDbLoadUld loadUld = existedLoadUlds(updatedLoadUlds.keySet(), siUld.getULDNumber());
				if(loadUld == null) {
					loadUld = existedLoadUlds(newLoadUlds, siUld.getULDNumber());
				}
				if(loadUld != null) {
					String temp = siUld.getRampTransferFlno();
					
					//TODO: clean code, to reimplement or refactor
					//Reset if there is no QRTX
					if(isNullOrEmptyStr(temp)) {
						loadUld.setRampTransferFlag("");
						loadUld.setRampTransferFlno("");
						loadUld.setIdRampTransferFlight("");
					} else {
						//FIXME: to write API for flight format. This is just temp fix
						loadUld.setRampTransferFlag("Y");
						
						String rampFltNo = formatFlightNo(temp); 
						
						LOG.debug("Ramp Transfer Flno - {}", rampFltNo);
						loadUld.setRampTransferFlno(rampFltNo);
						
						HpUfisCalendar startDate = new HpUfisCalendar();
						startDate.setTime(entDbAfttab.getTifd());// eg. 06/19/2013 21:45
						startDate.add(Calendar.HOUR, basicDataSingleton.getRampOffset()); // eg. 06/19/2013 13:45

						HpUfisCalendar endDate = new HpUfisCalendar();
						endDate.setTime(entDbAfttab.getTifd());// eg. 06/19/2013 21:45
						
						LOG.debug("Ramp Tnxfer Flight Criteria : RampTnxferFlno - {}, Startdate - {}, EndDate - {}", loadUld.getRampTransferFlno(), startDate.toString(), endDate.toString());
						EntDbAfttab entDbAfttabTransfer = afttabBean.findRampTransferFlight(loadUld.getRampTransferFlno(), startDate, endDate);
						
						//TODO: to confirm -> if no msg, jz drop whole msg?
						if(entDbAfttabTransfer != null) {
							loadUld.setIdRampTransferFlight(entDbAfttabTransfer.getUrno()+"");
						} else {
							LOG.warn("No flight is found for ramp transfer.");
						}
						
						String transferFltDebugInfo = String.format(", Ramp Tnxfer Flno (PKey) - %s[%s]", loadUld.getRampTransferFlno(), isNullOrEmptyStr(loadUld.getIdRampTransferFlight()) ? "No P-Key found"
								: loadUld.getIdRampTransferFlight());
						
						LOG.warn(
								"ULD for transfer flight : ULD - {}, {}", loadUld.getUldNumber(), transferFltDebugInfo);
					}
					
				} else {
					LOG.warn("ULD for transfer flight is not found {}", siUld.getULDNumber());
				}
			}

			int total = 0;
			int index = 0;
			for (Entry<EntDbLoadUld, EntDbLoadUld> entry : updatedLoadUlds.entrySet()) {
				boolean isLast = (index == updatedLoadUlds.entrySet().size() - 1 && newLoadUlds.isEmpty());
				EntDbLoadUld resultUld = dLLoadUldBean.merge(entry.getKey());
				boolean success =  resultUld != null;
				if(success){
					EntDbLoadUld original = entry.getValue();
					if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
						// send notification message to topic
						ufisTopicProducer.sendNotification(isLast, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(resultUld.getIdFlight()), original, resultUld);
					} else {
						// if topic not defined, send notification to queue
						ufisQueueProducer.sendNotification(isLast, HpUfisAppConstants.UfisASCommands.URT, String.valueOf(resultUld.getIdFlight()), original, resultUld);
					}
					LOG.debug("Notify ULDs(UWS) from SkyChain");
				}
				LOG.debug("Uld No. {} has {}been processed.", entry.getKey().getUldNumber(), success ? "" : "not ");
				index++;
				total++;
			}
			
			index = 0;
			for (EntDbLoadUld entDbLoadUld : newLoadUlds) {
				boolean isLast = index == newLoadUlds.size() - 1;
				EntDbLoadUld resultUld = dLLoadUldBean.merge(entDbLoadUld);
				boolean success =  resultUld != null;
				if(success){
					if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
						// send notification message to topic
						ufisTopicProducer.sendNotification(isLast, HpUfisAppConstants.UfisASCommands.IRT, String.valueOf(resultUld.getIdFlight()), null, resultUld);
					} else {
						// if topic not defined, send notification to queue
						ufisQueueProducer.sendNotification(isLast, HpUfisAppConstants.UfisASCommands.IRT, String.valueOf(resultUld.getIdFlight()), null, resultUld);
					}
					LOG.debug("Notify ULDs(UWS) from SkyChain");
				}
				LOG.debug("Uld No. {} has {}been processed.", entDbLoadUld.getUldNumber(), success ? "" : "not ");
				index++;
				total++;
			}
			
			LOG.info("Total ULD count is {}.", total);
			if(msgTelex != null) {
				ufisTopicProducer.sendNotification(true, HpUfisAppConstants.UfisASCommands.IRT, String.valueOf(entDbAfttab.getUrno()), null, msgTelex);
			}
			
			return true;
			
		} catch (JAXBException e) {
			LOG.error("JAXBContext when unmarshalling... {}", e.getMessage());
			addExptInfo(EnumExceptionCodes.EXSDF.name(), "");
			return false;
		} catch(Exception e){
			LOG.error("ERROR : {}", e.toString());
			addExptInfo(EnumExceptionCodes.EWACT.name(), e.getMessage());
			return false;
		}
	}
	
	private EntDbMsgTelex createMessageTelex(String message, UWSInfo uwsInfo, BigDecimal flightUrno) {
//		QD VQQACKM
//	    .DXBFMEK 050205
//	    UWS
//	    EK0001/05.DXB.FINAL
		
		String msgMetaRegex = "(?ms)(.*)^\\s?UWS.*$.*";
		String rawUWSInfo = "";
		String msgMeta = "";
		XmlSlurper slupler;
		try {
			slupler = new XmlSlurper();
			GPathResult result = slupler.parseText(message);
			for (Iterator iterator = result.childNodes(); iterator.hasNext();) {
				Node next = (Node)iterator.next();
				if(!"UWSInfo".equalsIgnoreCase(next.name())) {
					continue;
				}
				rawUWSInfo = next.text();
				msgMeta = rawUWSInfo.replaceAll(msgMetaRegex, "$1").trim();
				LOG.info("Message META DATA : {}", msgMeta);
			}
		} catch (Exception ex) {
			LOG.error("Message Meta Data parsing exception : {}", ex.getMessage());
		}
		
		
		FlightInfo flightInfo = uwsInfo.getFlightInfo();
		EntDbMsgTelex msgTelex = new EntDbMsgTelex();
		msgTelex.setMsgType("UWS");
		msgTelex.setMsgFltno(flightInfo.getFlightNumber());
		msgTelex.setMsgFltDay(flightInfo.getDateOfMonth());
		msgTelex.setLoadingPt(flightInfo.getAirportCode());
		msgTelex.setMsgSubtype(flightInfo.getFinalMessageIndicator());
		msgTelex.setMsgInfo(rawUWSInfo);
		msgTelex.setArrDepFlag(HpEKConstants.EK_HOPO.equalsIgnoreCase(flightInfo.getAirportCode().trim()) ? "D" : "A");
		msgTelex.setSendRecvFlag("R");
		msgTelex.setIdFlight(flightUrno);
		msgTelex.setDataSource(HpEKConstants.ULD_SOURCE_SKYCHAIN);
		msgTelex.setCreatedUser(HpEKConstants.ULD_SOURCE_SKYCHAIN);
		msgTelex.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		if(msgMeta.isEmpty()) {
			msgTelex.setMsgSendDay(" ");
			msgTelex.setMsgSendTime(" ");	
		} else {
			setMessageMetaData(msgTelex, msgMeta);
		}
		boolean success = dlMsgTelexBean.saveMsgTelex(msgTelex);
		return success ? msgTelex : null;
	}

	private static void setMessageMetaData(EntDbMsgTelex msgTelex, String msgMeta) {
//		QD VQQACKM
//	    .DXBFMEK 050205
		String msgMetaRegex = "(?ms).*$(.*\\s)(\\d{2})(\\d{4}).*$.*?";
		String msgSender = msgMeta.replaceAll(msgMetaRegex, "$1").trim();
		String msgSendDay = msgMeta.replaceAll(msgMetaRegex, "$2").trim();
		String msgSendTime = msgMeta.replaceAll(msgMetaRegex, "$3").trim();
		
		try {
			String dateString = createMessageSendDateFrom(msgSendDay, msgSendTime);
			LOG.info("Message Send Date : {}", dateString);
			msgTelex.setMsgSendDate(YYYYMMDDHHMMSS.parse(dateString));
		} catch (Exception e) {
			LOG.error("Message Send Date creation error : {}", e.getMessage());
		}
		
		msgTelex.setMsgSender(msgSender);
		msgTelex.setMsgSendDay(msgSendDay);
		msgTelex.setMsgSendTime(msgSendTime);
	}
	

	private void initialize(EntDbLoadUld loadUld, EntDbAfttab entDbAfttab) throws ParseException {
		//Set defaults
		if(!isNullOrEmptyStr(entDbAfttab.getStod())) {
			loadUld.setFltDate(YYYYMMDDHHMMSS.parse(entDbAfttab.getStod()));
		}
		loadUld.setIdConxFlight(BigDecimal.ZERO);
		loadUld.setArrDepFlag(entDbAfttab.getAdid()+"");
		loadUld.setUldSubtype(" ");		
	}

	private boolean existsMandatory(Meta meta, UWSInfo uwsInfo) {
		return meta == null || isNullOrEmptyStr(meta.getType()) || isNullOrEmptyStr(meta.getSubtype()) || uwsInfo == null || uwsInfo.getFlightInfo() == null;
	}

	private EntDbLoadUld existedLoadUlds(Collection<EntDbLoadUld> collection, String uldNumber) {
		for (EntDbLoadUld entDbLoadUld : collection) {
			if(!entDbLoadUld.getUldNumber().equals(uldNumber)) {
				continue;
			}
			return entDbLoadUld;
		}
		
		return null;
	}

	private static String createFlightDateFrom(String dayOfMonth) throws ParseException {
		return createFlightDateFrom(dayOfMonth, getCurrentUTCTime());
	}
	
	private static String createFlightDateFrom(String dayOfMonth, Date currentUTCTime) throws ParseException {
		int flightDayOfMonth = Integer.parseInt(dayOfMonth.trim());
		LocalDate currentTime = new LocalDate(currentUTCTime);
		
		LocalDate flightDate = null;
		LocalDate beforeFlightDate = null;
		LocalDate afterFlightDate = null;
		
		if(currentTime.getDayOfMonth() == flightDayOfMonth) {
			return YYYYMMDD_FORMATTER.format(currentUTCTime);
		}
		
		try {
			//If current month is February and passed date is 31, no calculation on flight date.
			flightDate = currentTime.withDayOfMonth(flightDayOfMonth);
		} catch (IllegalFieldValueException ex) {
		}
		
		boolean invalidFlightDateForThisMonth = flightDate == null;
		boolean invalidBefore = true;
		boolean invalidAfter = true;

		int beforeMth = 0;
		while(invalidBefore) {
			try {
				beforeMth++;
				beforeFlightDate = currentTime.minusMonths(beforeMth).withDayOfMonth(flightDayOfMonth);
				invalidBefore = false;
			} catch (IllegalFieldValueException ex) {
			}
		}
		
		int afterMth = 0;
		while(invalidAfter) {
			try {
				afterMth++;
				afterFlightDate = currentTime.plusMonths(afterMth).withDayOfMonth(flightDayOfMonth);
				invalidAfter = false;
			} catch (IllegalFieldValueException ex) {
			}
		}
			
		int daysDiff1 = Days.daysBetween(beforeFlightDate, currentTime).getDays();
		int daysDiff3 = Days.daysBetween(currentTime, afterFlightDate).getDays();
		
		String beforeDateFormatted = YYYYMMDD_FORMATTER.format(beforeFlightDate.toDate());
		String afterDateFormatted = YYYYMMDD_FORMATTER.format(afterFlightDate.toDate());
		
		LOG.debug("Today (Current UTC Time) - {}", YYYYMMDD_FORMATTER.format(currentUTCTime));
		LOG.debug("1 month before departure - {}, {}", beforeFlightDate, daysDiff1);
		LOG.debug("1 month after departure - {}, {}", afterFlightDate, daysDiff3);
		
		if(invalidFlightDateForThisMonth) {
			//No evaluation on the flight day with current date.
			return daysDiff1 < daysDiff3 ? beforeDateFormatted : afterDateFormatted;
		}
		
		int daysDiff2 = flightDate.isBefore(currentTime) ? Days.daysBetween(flightDate, currentTime).getDays() : Days.daysBetween(currentTime, flightDate).getDays();
		String flightDateFormatted = YYYYMMDD_FORMATTER.format(flightDate.toDate()); 
		
		LOG.debug("departure - {}, {}", flightDate, daysDiff2);
		
		if(daysDiff1 < daysDiff2) {
			if(daysDiff1 < daysDiff3) {
				return beforeDateFormatted;
			}
			
			return afterDateFormatted;
		}
		
		//daysDiff1 == daysDiff2 or daysDiff1 > daysDiff2
		return daysDiff2 < daysDiff3 ? flightDateFormatted : afterDateFormatted;
	}
	
	private static String createMessageSendDateFrom(String monthOfYear, String hhmm) throws ParseException {
		Date currentUTCTime = getCurrentUTCTime();
		int dayOfToday = of(currentUTCTime, Calendar.DAY_OF_MONTH);
		int monthOfToday = of(currentUTCTime, Calendar.MONTH) + 1;
		int yearOfToday = of(currentUTCTime, Calendar.YEAR);
		int msgMonthOfYear = Integer.parseInt(monthOfYear);
		
		if(monthOfToday == msgMonthOfYear) {
			return YYYYMMDD_FORMATTER.format(currentUTCTime);
		}

		String constructedMsgSendDateString = String.format("%s%2$02d%3$02d", yearOfToday, msgMonthOfYear, dayOfToday);
		Date constructedMsgSendDate = YYYYMMDD_FORMATTER.parse(constructedMsgSendDateString);
		
		//UTC
		Date after = addMonths(constructedMsgSendDate, NO_OF_MONTHS_IN_YEAR);
		Date before = addMonths(constructedMsgSendDate, -NO_OF_MONTHS_IN_YEAR);
		
		long daysDiff1 = daysBetween(before, currentUTCTime);
		long daysDiff2 = constructedMsgSendDate.before(currentUTCTime) ? daysBetween(
				constructedMsgSendDate, currentUTCTime) : daysBetween(currentUTCTime, constructedMsgSendDate);
		long daysDiff3 = daysBetween(currentUTCTime, after);
		
		String beforeDateFormatted = YYYYMMDD_FORMATTER.format(before);
		String afterDateFormatted = YYYYMMDD_FORMATTER.format(after);

		LOG.debug("Today (Current UTC Time) - {}", YYYYMMDD_FORMATTER.format(currentUTCTime));
		LOG.debug("1 year before msg send date - {}, {}", beforeDateFormatted, daysDiff1);
		LOG.debug("This year msg send date - {}, {}", constructedMsgSendDateString, daysDiff2);
		LOG.debug("1 year after msg send date - {}, {}", afterDateFormatted, daysDiff3);

		if(daysDiff1 < daysDiff2) {
			if(daysDiff1 < daysDiff3) {
				return beforeDateFormatted;
			}
			
			return afterDateFormatted;
		}
		
		//daysDiff1 == daysDiff2 or daysDiff1 > daysDiff2
		String resultDateString = daysDiff2 < daysDiff3 ? constructedMsgSendDateString : afterDateFormatted;
		return String.format("%s%s00", resultDateString, hhmm);
	}
	
	private static String formatFlightNo(String input) {
		//For skychain, the very first carrier code is always 2 characters which is "EK"
		String carrierCode = input.substring(0,2);
		String flightNumber = input.substring(2);
		return String.format("%s %s", carrierCode, formatCedaFltn(flightNumber));
	}
	
}
