package com.ufis_as.ufisapp.ek.loadSummary;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUld;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbLoadUldSummary;
import com.ufis_as.ek_if.ufis.UfisUldSummaryConfig;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldBean;
import com.ufis_as.ufisapp.ek.eao.DlLoadUldSummaryBean;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.eao.IAfttabBeanLocal;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
/**
 * @author btr
 */
@Stateless(name = "BlHandleUldSummaryBean")
public class BlHandleUldSummaryBean {
	public static final Logger LOG = LoggerFactory.getLogger(BlHandleUldSummaryBean.class);
	
	@EJB
	EntStartupInitSingleton clsEntInitSingleton;
	@EJB
	DlLoadUldBean clsDlLoadUldBean;
	@EJB
	private IAfttabBeanLocal clsAfttabBeanLocal;
	@EJB
	DlLoadUldSummaryBean clsDlLoadUldSummaryBean;
	
	List<String> idFlightList = new ArrayList<>();
	List<String> uldSubTypeList = new ArrayList<>();
	List<String> uldInfoTypeList = new ArrayList<>();
	HashMap<String, List<String>> uldTypeMap = new HashMap<>();
	
	enum INFO_TYPE{
		LOA, LOC, TXF, T2T;
	};
	
	UfisUldSummaryConfig entuldLOA;
	UfisUldSummaryConfig entUld;
	
	public BlHandleUldSummaryBean() {
	}
	
	@PostConstruct
	public void init(){
		uldSubTypeList = clsEntInitSingleton.getUldSubTypeCategoryList();
		uldInfoTypeList = clsEntInitSingleton.getUldInfoTypeCategoryList();
		uldTypeMap = clsEntInitSingleton.getUldTypeMap();
/*		//TODO to remove later << For Testing >>
		idFlightList.add("51990");
		idFlightList.add("275980");*/
	}
	
	@PreDestroy
	private void initialClear() {
		idFlightList.clear();
		uldSubTypeList.clear();
		uldInfoTypeList.clear();
		uldTypeMap.clear();
	}
	
/*	<< For Testing >>
	public void triggerUldSummarizer(){
		for(String urno : idFlightList){
			calculateUldSummary(new BigDecimal(urno));
		}
	}*/
	
	public void calculateUldSummary(BigDecimal idFlightDecimal){
		try {
			EntDbAfttab entAft = clsAfttabBeanLocal.getFlightByUrno(idFlightDecimal);
			if (entAft == null)
				return;
			LOG.debug("############### START for flight <{}> ###############", idFlightDecimal);
			switch (entAft.getAdid()) {
			case 'A':
				// if ARRIVAL, summarize for all conx flights' ULD
				summarizeUld(idFlightDecimal, HpEKConstants.ADID_A);
				break;
			case 'D':
				// if DEPT, no connected flights for uld
				summarizeUld(idFlightDecimal, HpEKConstants.ADID_D);
				break;
			}
		} catch (ParseException e) {
			LOG.error("ERROR : {}", e.getMessage());
		}catch(Exception ex){
			LOG.error("ERROR : {}", ex);
		}
	}

	/**
	 * 1.) get all ULD for input flight URNO
	 * 2.) get all unique conx flights for input flight
	 * 3.) collect all the same ULD for each connected flights
	 * 4.) calculate for LOA(ie. doSummary()) and insert/update to DB(ie. processUldSummary())
	 * 5.) calculate for the rest SubType(ie. calculateForSubTypes())
	 * 
	 * @param idFlightDecimal, adid
	 */
	private void summarizeUld(BigDecimal idFlightDecimal, char adid) throws ParseException {
		//get all Conx Flights
		List<EntDbLoadUld> conxUldList = clsDlLoadUldBean.getUldsByIdFlight(idFlightDecimal);
		if(conxUldList.isEmpty())
			return;
		List<BigDecimal> uniqueConxIdFlightList = null;
		if('A' == adid){
			//get different Conx flights
			uniqueConxIdFlightList = clsDlLoadUldBean.getUniqueConxFlightByIdFlight(idFlightDecimal);
		}else if('D' == adid){
			uniqueConxIdFlightList = new ArrayList<>();
			uniqueConxIdFlightList.add(new BigDecimal(0));
		}
		for(BigDecimal conxIdFlight : uniqueConxIdFlightList){
			entuldLOA = new UfisUldSummaryConfig();
			//collect ulds for same flight
			List<EntDbLoadUld> sameFltUlds = new ArrayList<>();
			for(EntDbLoadUld uld : conxUldList){
				if(conxIdFlight.compareTo(uld.getIdConxFlight()) == 0)
					sameFltUlds.add(uld);
			}
			
			//summarize all ulds for same flight
			for(EntDbLoadUld uld : sameFltUlds){
				entuldLOA = doSummary(uld, entuldLOA);
			}
			//insert/update for LOA for same flight
			processUldSummary(idFlightDecimal, conxIdFlight, INFO_TYPE.LOA.name());
			
			if(uldTypeMap.isEmpty())
				LOG.debug("No config data for Uld SubType. No operation will perform for all SubTypes.");
			else
				calculateForSubTypes(idFlightDecimal, conxIdFlight, sameFltUlds);
			
			LOG.debug("END summary for conx flight <{}>", conxIdFlight);
		}
	}

	/**
	 * 1.) collect same subType ULD for each flights
	 * 2.) calculate summary for each subType and insert/update DB
	 * 
	 * @param idFlightDecimal, conxIdFlight, sameFltUlds
	 */
	private void calculateForSubTypes(BigDecimal idFlightDecimal,
			BigDecimal conxIdFlight, List<EntDbLoadUld> sameFltUlds)
			throws ParseException {
		//do for all SubTypes from config
		for (int i = 0; i < uldSubTypeList.size(); i++) {
			String configSubType = uldSubTypeList.get(i);
			String configInfoType = uldInfoTypeList.get(i);
			
			List<EntDbLoadUld> sameSubTypeUlds = new ArrayList<>();
			entUld = new UfisUldSummaryConfig();
			
			//collect ulds for the same SUBTYPE of same flight
			for (EntDbLoadUld uld : sameFltUlds) {
				if (uld.getUldSubtype().equalsIgnoreCase(configSubType)) {
					sameSubTypeUlds.add(uld);
				}
			}
			
			for(EntDbLoadUld uldSubType : sameSubTypeUlds){
				entUld = doSummary(uldSubType, entUld);
			}
			processUldSummary(idFlightDecimal, conxIdFlight, configInfoType);
		}
	}
	
	//Summarize for LOA and all SubTypes
	private UfisUldSummaryConfig doSummary(EntDbLoadUld uld, UfisUldSummaryConfig uldConfig) throws ParseException {
		//ULDs
		uldConfig.setUld_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
		uldConfig.setUld_pcs(1);//increase by 1
		//cargoUld Weight
		if(uldTypeMap.get("ULDSCARGO").contains(uld.getUldType())){
			uldConfig.setCargo_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setCargo_pcs(1);//increase by 1
		}
		//bagUld Weight
		if(uldTypeMap.get("ULDSBAG").contains(uld.getUldType())){
			uldConfig.setBag_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setBag_pcs(1);
		}
		//BULK_ULD
		if(uldTypeMap.get("BULK_ULDULD").contains(uld.getUldType())){
			uldConfig.setBulk_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setBulk_pcs(1);//increase by 1
		}
		//bulk_uld Cargo Weight
		if(uldTypeMap.get("BULK_ULDCARGO").contains(uld.getUldType())){
			uldConfig.setBulkCargo_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setBulkCargo_pcs(1);
		}
		//bulk_uld Bag Weight
		if(uldTypeMap.get("BULK_ULDBAG").contains(uld.getUldType())){
			uldConfig.setBulkBag_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setBulkBag_pcs(1);
		}
		
		//PALLET_ULD
		if(uldTypeMap.get("PALLET_ULDULD").contains(uld.getUldType())){
			uldConfig.setPallet_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setPallet_pcs(1);//increase by 1
		}
		//pallet_uld Cargo Weight
		if(uldTypeMap.get("PALLET_ULDCARGO").contains(uld.getUldType())){
			uldConfig.setPalletCargo_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setPalletCargo_pcs(1);
		}
		//pallet_uld Bag Weight
		if(uldTypeMap.get("PALLET_ULDBAG").contains(uld.getUldType())){
			uldConfig.setPalletBag_weight(Float.parseFloat(uld.getUldWeight() == null ? "0" : uld.getUldWeight()));
			uldConfig.setPalletBag_pcs(1);
		}
				
		return uldConfig;
	}
	
	//For all SubTypes including LOA insert/update to table 
		private void processUldSummary(BigDecimal idFlightDecimal, BigDecimal conxIdFlight, String infoType)
				throws ParseException {
			//BigDecimal idConxFlight = (new BigDecimal(0).compareTo(conxIdFlight) == 0)? null : conxIdFlight;
			EntDbLoadUldSummary loadUldSum = clsDlLoadUldSummaryBean.getUldMoveByIdFlightUldNum(idFlightDecimal, conxIdFlight, infoType);
			//EntDbLoadUldSummary loadUldSum = null;
			if(loadUldSum == null){//not Existing?
				loadUldSum = new EntDbLoadUldSummary();
				loadUldSum.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
				loadUldSum.setCreatedUser(HpEKConstants.LOAD_SUMMARY);
			}else{
				loadUldSum.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
				loadUldSum.setUpdatedUser(HpEKConstants.LOAD_SUMMARY);
			}
			UfisUldSummaryConfig uldSumObj = INFO_TYPE.LOA.name().equalsIgnoreCase(infoType) ? entuldLOA : entUld;
				
			loadUldSum.setIdFlight(idFlightDecimal);
			loadUldSum.setIdConxFlight(conxIdFlight);
			
			loadUldSum.setUldWeight(uldSumObj.getUld_weight());
			loadUldSum.setUldPcs(uldSumObj.getUld_pcs());
			loadUldSum.setCargoUldPcs(uldSumObj.getCargo_pcs());
			loadUldSum.setCargoUldWt(uldSumObj.getCargo_weight());
			loadUldSum.setBagUldPcs(uldSumObj.getBag_pcs());
			loadUldSum.setBagUldWt(uldSumObj.getBag_weight());

			loadUldSum.setBulkWeight(uldSumObj.getBulk_weight());
			loadUldSum.setBulkPcs(uldSumObj.getBulk_pcs());
			loadUldSum.setBulkBagUldPcs(uldSumObj.getBulkBag_pcs());
			loadUldSum.setBulkBagUldWt(uldSumObj.getBulkBag_weight());
			loadUldSum.setBulkCargoUldWt(uldSumObj.getBulkCargo_weight());
			loadUldSum.setBulkCargoUldPcs(uldSumObj.getBulkCargo_pcs());
		
			loadUldSum.setPalletUldWt(uldSumObj.getPallet_weight());
			loadUldSum.setPalletUldPcs(uldSumObj.getPallet_pcs());

			loadUldSum.setRecStatus(" ");
			loadUldSum.setInfoType(infoType);
			loadUldSum.setDataSource(HpEKConstants.SYS_SOURCE);
			
			clsDlLoadUldSummaryBean.merge(loadUldSum);
		}
}
