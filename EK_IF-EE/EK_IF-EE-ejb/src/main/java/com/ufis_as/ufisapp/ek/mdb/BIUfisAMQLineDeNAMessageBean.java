//package com.ufis_as.ufisapp.ek.mdb;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.ejb.EJB;
//import javax.jms.Destination;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageConsumer;
//import javax.jms.MessageListener;
//import javax.jms.Session;
//import javax.jms.TextMessage;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ufis_as.ufisapp.ek.intf.BlRouter;
//import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleLineDeNA;
//import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
//import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
//
//public class BIUfisAMQLineDeNAMessageBean implements MessageListener{
//	
//
//	private static final Logger LOG = LoggerFactory.getLogger(BIUfisAMQLineDeNAMessageBean.class);
//	@EJB
//	private ConnFactorySingleton _connSingleton;
//	@EJB
//	private BlRouter _entRouter;
//	@EJB
//	private BlIrmtabFacade _irmtabFacade;
//	@EJB
//	private BlHandleLineDeNA bIHandleLineDeNA;
//	@EJB 
//	private EntStartupInitSingleton entStartupInitSingleton;
//	
//	private Session session = null;
//	private Destination destination = null;
//	private MessageConsumer consumer = null;
//	private String queue = null;
//
//	@PostConstruct
//	public void init() {
//		try {
//			
////			List<InterfaceConfig> configList = _connSingleton.getInterfaceConfigs();
//			
//			if (entStartupInitSingleton != null &&_connSingleton != null && _connSingleton.getActiveMqConnect() != null){
//			queue = entStartupInitSingleton.getToQueueList().get(0);
//			Session session = _connSingleton.getActiveMqConnect().createSession(
//					false, Session.AUTO_ACKNOWLEDGE);
//			destination = session.createQueue(queue);
//			MessageConsumer consumer = session.createConsumer(destination);
//			consumer.setMessageListener(this);
//			
//			LOG.info("listen to the queue {}", queue);
//			}
//			
//			
//		} catch (JMSException e) {
//			LOG.error("!!!ERROR, Listening ActiveMQ topic {} error: {}", queue,
//					e.getMessage());
//		}
//	}
//	
//	@PreDestroy
//	public void destroy() {
//			try {
//				if (session != null) {
//					session.close();
//				}
//			} catch (JMSException e) {
//				LOG.error("!!!Cannot close tibco session: {}", e);
//			} 
//	}
//
//	@Override
//	public void onMessage(Message inMessage) {
//		try {
//			if (inMessage instanceof TextMessage) {
//			
//				TextMessage msg = (TextMessage) inMessage;
//				String jsonString = msg.getText();
//				bIHandleLineDeNA.handleLineDeNAFromActiveMQ(jsonString, queue);
//			} else {
//				LOG.warn("Message of wrong type: {}", inMessage.getClass()
//						.getName());
//			}
//		} catch (Exception te) {
//			LOG.error("Tibco Session: {}", session);
//			LOG.error("Tibco consumer: {}", consumer);
//			LOG.error("Exception {}", te);
//		}
//	}
//
//}
