package com.ufis_as.ufisapp.ek.mdb;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.outgoingmvt.BlHandleOutgoingMVTBean;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.ek.singleton.OUTMVTConnFactorySingleton;

//@Singleton
//@Startup
public class BIUfisAMQOutgoingMVTMessageBean implements MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(BIUfisAMQOutgoingMVTMessageBean.class);
	
	@EJB
	private OUTMVTConnFactorySingleton outMVTConnFactorySingleton;
	
	@EJB
	private BlHandleOutgoingMVTBean blHandleOutgoingMVTBean;
	
	@EJB 
	private EntStartupInitSingleton entStartupInitSingleton;
	
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;

	@PostConstruct
	public void init() {
		String queue = null;
		try {
			List<String> fromQueueList = entStartupInitSingleton.getFromQueueList();
			if(fromQueueList == null || fromQueueList.isEmpty()) {
				LOG.error("The ActiveMQ queue name cannot be retrieved for the config. Please check the config.");
				return;
			}
			queue = fromQueueList.get(0);
			LOG.debug("Outgoing MVT Queue Name - {}", queue);
			session = outMVTConnFactorySingleton.getActiveMqConnect().createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queue);
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
			LOG.info("!!!! Listening to ActiveMQ queue {}", queue);
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening to ActiveMQ queue {} error: {}", queue,e.getMessage());
		}
		
	}
	
	@Override
	public void onMessage(Message inMessage) {
		Date before = new Date();
		try {
			if(!(inMessage instanceof TextMessage)) {
				LOG.warn("Message of wrong type: {}", inMessage.getClass().getName());
				return;
			}
			
			TextMessage msg = (TextMessage) inMessage;
			blHandleOutgoingMVTBean.process(msg.getText());
			
		} catch (Exception te) {
			LOG.error("ActiveMQ Session : {}", session);
			LOG.error("ActiveMQ consumer : {}", consumer);
			LOG.error("Exception on ActiveMQ MessageListener#onMessage : {}", te);
		}
		Date after = new Date();
		LOG.info("The total processing time : {} ms", after.getTime() - before.getTime());
	}
	
	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close ActiveMQ session: {}", e);
		} 
	}

}
