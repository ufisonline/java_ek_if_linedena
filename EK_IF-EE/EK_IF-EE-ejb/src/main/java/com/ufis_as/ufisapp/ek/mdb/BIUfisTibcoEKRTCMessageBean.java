package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BIRouterEKRTC;
import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

public class BIUfisTibcoEKRTCMessageBean implements IBlUfisTibcoMDB{


	private static final Logger LOG = LoggerFactory.getLogger(BIUfisTibcoEKRTCMessageBean.class);
	@EJB
	private ConnFactorySingleton _connSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB 
	private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	private BIRouterEKRTC bIRouterEKRTC;
	
	private Session session = null;
	private MessageConsumer consumer = null;

	@Override
	public void init(String queue) {
		try {
			
			if (entStartupInitSingleton != null && _connSingleton != null && _connSingleton.getTibcoConnect() != null){
			Session session = _connSingleton.getTibcoConnect().createSession(
					false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(queue);
			MessageConsumer consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
			
			LOG.info("listen to the queue {}", queue);
			}
					
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}
		
	}
	
	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close tibco session: {}", e);
			} 
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
			if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				
				// later to add switch
				LOG.info("insert IRMTAB is on : {}", entStartupInitSingleton.isIrmOn());
				if (entStartupInitSingleton.getIrmLogLev().equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {
						long startTime = System.currentTimeMillis();			
					_irmtabFacade.storeRawMsg(inMessage,HpEKConstants.EKRTC_DATA_SOURCE);				
					LOG.info("insert into IRMTAB, takes {} ms", System.currentTimeMillis()- startTime);
				}
				
				long startTime = System.currentTimeMillis();
				bIRouterEKRTC.routeMessage(msg);
				LOG.info("process RTC msg, takes {} ms", System.currentTimeMillis()
						- startTime);

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}

	

}
