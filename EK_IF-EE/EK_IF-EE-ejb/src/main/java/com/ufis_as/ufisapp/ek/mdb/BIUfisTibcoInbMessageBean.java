//package com.ufis_as.ufisapp.ek.mdb;
//
//import java.util.Iterator;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.ejb.EJB;
//import javax.jms.Connection;
//import javax.jms.Destination;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageConsumer;
//import javax.jms.MessageListener;
//import javax.jms.Session;
//import javax.jms.TextMessage;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ufis_as.ek_if.ufis.InterfaceConfig;
//import com.ufis_as.ufisapp.ek.intf.BlRouterMacsPax;
//import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
//import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
//
///*@Singleton(name = "BIUfisTibcoInbMessageBean")
//@Startup
//@DependsOn("paxConnFactory")*/
//public class BIUfisTibcoInbMessageBean implements MessageListener {
//
//
//	private static final Logger LOG = LoggerFactory
//			.getLogger(BIUfisTibcoInbMessageBean.class);
//	@EJB
//	ConnFactorySingleton _connSingleton;
//	@EJB
//	BlRouterMacsPax _entRouter;
//	@EJB
//	BlIrmtabFacade _irmtabFacade;
//	@EJB
//	EntStartupInitSingleton _startupInitSingleton;
//	
//	private Connection conn = null;
//	private Session session = null;
//	private Destination destination = null;
//	private MessageConsumer consumer = null;
//	private String dtflString = null;
//
//	@PostConstruct
//	public void init() {
//		String queue = null;
//		try {
//			
//			List<InterfaceConfig> configList = _connSingleton.getInterfaceConfigs();
//			Iterator<InterfaceConfig> it = configList.iterator();
//			
//			while (it.hasNext()){
//				InterfaceConfig inConfig = it.next();
//				if ("inb".equals(inConfig.getName().toLowerCase())){
//					 queue = inConfig.getFromMq().getQueueName();
//					 conn = _connSingleton.getTibcoConnect();
//					session = conn.createSession(
//							false, Session.AUTO_ACKNOWLEDGE);
//					destination = session.createQueue(queue);
//					consumer = session.createConsumer(destination);
//					consumer.setMessageListener(this);
//					LOG.info("sucessfully listen to queue {}", queue);
//					
//					dtflString = _startupInitSingleton.getDtflStr();
//				}
//			}
//			
//			
//		} catch (JMSException e) {
//			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
//					e.getMessage());
//		}
//	}
//
//	@Override
//	public void onMessage(Message inMessage) {
//		TextMessage msg;
//		try {
//			if (inMessage instanceof TextMessage) {
//				msg = (TextMessage) inMessage;
//				
//				if (_startupInitSingleton.isIrmOn()){
//					long startTIme = System.currentTimeMillis();
//					_irmtabFacade.storeRawMsg(inMessage, dtflString);
//					LOG.debug("0 insert {} msg to IRMTAB, takes {} ms", dtflString, System.currentTimeMillis()-startTIme);
//					_entRouter.setIrmtabUrno(_irmtabFacade.getIrmNextUrno()-1);	
//				}else{
//					LOG.debug("_startupInitSingleton.isIrmOn() = false");
//				}
//
//				
//				LOG.debug("inb start");
//				long startTime = System.currentTimeMillis();
//				_entRouter.routeMessage(msg.getText(), null, "inb");
//				LOG.info("1 inb message processing cost: {} ms",(System.currentTimeMillis() - startTime));
//				LOG.debug("inb");
//
//			} else {
//				LOG.warn("Message of wrong type: {}", inMessage.getClass()
//						.getName());
//			}
//		} catch (Exception e) {
//			LOG.error("JMSException {}", e.getMessage());
//		} catch (Throwable te) {
//			LOG.error("JMSException  Throwable {}", te.getMessage());
//		}
//
//	}
//
//	
//
//	@PreDestroy
//	public void destroy() {
//		try {
//			if (session != null) {
//				session.close();
//				LOG.debug("TIBOC mdb existing session is closed.");
//			}
//		} catch (JMSException e) {
//			LOG.error("!!!Cannot close tibco session: {}", e);
//		} 
//		
//	}
//
//	public Session getSession() {
//		return session;
//	}
//
//	public void setSession(Session session) {
//		this.session = session;
//	}
//
//
//}
