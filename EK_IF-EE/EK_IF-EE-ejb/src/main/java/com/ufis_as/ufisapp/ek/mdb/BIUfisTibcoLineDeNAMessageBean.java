package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleDeNa;
import com.ufis_as.ufisapp.ek.intf.linedena.BlHandleLineDeNA;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

import ek.LineDeNA.OpsFlashMsg;

/**
 * 
 * @author SCH
 * 
 */

public class BIUfisTibcoLineDeNAMessageBean implements IBlUfisTibcoMDB {

	private static final Logger LOG = LoggerFactory
			.getLogger(BIUfisTibcoLineDeNAMessageBean.class);
	@EJB
	private ConnFactorySingleton _connSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	@EJB
	private BlHandleDeNa handleDeNA;
	@EJB
	private BlHandleLineDeNA bIHandleLineDeNA;

	private Session session = null;
	// private Destination destination = null;
	private MessageConsumer consumer = null;
	private JAXBContext _cnxJaxb;
	private Unmarshaller _um;

	@Override
	public void init(String queue) {
		try {

			if (entStartupInitSingleton != null && _connSingleton != null
					&& _connSingleton.getTibcoConnect() != null) {
				Session session = _connSingleton.getTibcoConnect()
						.createSession(false, Session.AUTO_ACKNOWLEDGE);
				Destination destination = session.createQueue(queue);
				MessageConsumer consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);

				LOG.info("listen to the queue {}", queue);

				try {
					_cnxJaxb = JAXBContext.newInstance(OpsFlashMsg.class);
					_um = _cnxJaxb.createUnmarshaller();
				} catch (Exception e) {
					LOG.error("JAXBException when creating Unmarshaller: {}", e);
				}

			}

		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}

	}

	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close tibco session: {}", e);
		}
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
			if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;

				// later to add switch
				LOG.info("insert IRMTAB is on : {}",
						entStartupInitSingleton.isIrmOn());
				if (entStartupInitSingleton.getIrmLogLev().equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {
					long startTime = System.currentTimeMillis();
					_irmtabFacade.storeRawMsg(inMessage,
							HpEKConstants.DeNA_DATA_SOURCE);
					LOG.info("insert into IRMTAB, takes {} ms",
							System.currentTimeMillis() - startTime);
				}

				if (handleDeNA == null) {
					handleDeNA = new BlHandleDeNa();
				}
				// handleDeNA.setEdpiods(edpiods);
				handleDeNA.init(_um);
				handleDeNA.setXmlMsg(msg.getText());
				handleDeNA.setRawMsg(msg);
				// handleDeNA.setEntDbAircraftOpsMsg(entDbAircraftOpsMsg);
				long startTime = System.currentTimeMillis();
				boolean isUnMarshalSucess = handleDeNA.unMarshal();
				LOG.debug("unMarshal LineDeNA msg, takes {} ms",
						System.currentTimeMillis() - startTime);

				if (isUnMarshalSucess) {
					startTime = System.currentTimeMillis();
					bIHandleLineDeNA.handleLineDeNA(
							handleDeNA.getEntDbAircraftOpsMsg(), msg,
							handleDeNA.getSubtype());
					LOG.debug("handle lineDeNA msg, takes {} ms",
							System.currentTimeMillis() - startTime);
				} else {
					LOG.debug("Unmarshal not successful, pls check!");
					return;
				}

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e);
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			// LOG.error("Tibco destination: {}", destination.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}

}
