package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BlRouterMacsPax;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacadeForPax;


/*@Singleton(name = "BIUfisTibcoPaxMessageBean")
@Startup
@DependsOn("paxConnFactory")*/

/**
 * 
 * @author SCH
 *
 */
public class BIUfisTibcoMACSPAXMessageBean implements IBlUfisTibcoMDB {


	private static final Logger LOG = LoggerFactory
			.getLogger(BIUfisTibcoMACSPAXMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	BlRouterMacsPax _entRouter;
	@EJB
	BlIrmtabFacadeForPax _irmtabFacade;
	@EJB
	EntStartupInitSingleton entStartupInitSingleton;
	
	private Session session;


	@Override
	public void init(String queue) {
		try {
			
			if (entStartupInitSingleton != null && _connSingleton != null && _connSingleton.getTibcoConnect() != null){
			Session session = _connSingleton.getTibcoConnect().createSession(
					false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(queue);
			MessageConsumer consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);

			}
					
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}
		
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		long irmtabRef = 0;
		try {
			if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				
				// get process name 
				String queueName = entStartupInitSingleton.getQueueNameMap().get(inMessage.getJMSDestination().toString());
				
				LOG.debug(" -------------------------------------------- {} START --------------------------------------------", queueName);
				
				// later to add switch
//				LOG.info("insert IRMTAB is on : {}", entStartupInitSingleton.isIrmOn());
				if (entStartupInitSingleton.getIrmLogLev().equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {
						long startTime = System.currentTimeMillis();			
						irmtabRef = _irmtabFacade.storeRawMsg(inMessage,HpEKConstants.MACS_PAX_DATA_SOURCE);				
					LOG.info("insert into IRMTAB, takes {} ms", System.currentTimeMillis()- startTime);
				}
				
				if(queueName != null && !"".equalsIgnoreCase(queueName)){
				
				long startTime = System.currentTimeMillis();
				_entRouter.routeMessage(msg, queueName, new Long(irmtabRef));
				LOG.info("1 {} message processing cost: {} ms",queueName,(System.currentTimeMillis() - startTime));
				
				}else{
					LOG.warn("Can not find relevent queueName: {} to router the msg from Tibco queue {}, ps check MACS-PAX config", queueName, inMessage.getJMSDestination().toString());
				}
				
				LOG.debug(" -------------------------------------------- {} END --------------------------------------------", queueName);

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (Exception e) {
			LOG.error("JMSException {}", e.getMessage());
		} catch (Throwable te) {
			LOG.error("JMSException  Throwable {}", te);
		}

	}

	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
				LOG.debug("TIBOC mdb existing session is closed.");
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close tibco session: {}", e);
		} 
		
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}




}
