package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.ek.intf.BlAMQMessageRouter;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.dto.EntUfisMsgDTO;
/**
 * MDB to listen to UFIS AMQ where CEDA broadcasting
 * 
 * @author btr
 */
public class BlUfisAMQBridgeMessageBean implements MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisAMQBridgeMessageBean.class);
	@EJB
	private ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
    @EJB
    private BlAMQMessageRouter clsBlAMQRouter;
    
    private static ObjectMapper mapper = new ObjectMapper();
	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;
	private String dtflString = null;

	@PostConstruct
	public void init() {
		////****** Using QUEUE ******* ////
		String queue = null;
		try {
			if(_startupInitSingleton.isBcOn()){
				if(_startupInitSingleton.getBcFromUfisQueue() != null){
					queue = _startupInitSingleton.getBcFromUfisQueue();
//					session = _connSingleton.getActiveMqConnect().createSession(false, Session.AUTO_ACKNOWLEDGE);
					session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
					destination = session.createQueue(queue);
					consumer = session.createConsumer(destination);
					consumer.setMessageListener(this);
					LOG.info("!!!! Listening to AMQ queue {}", queue);
					
					dtflString = _startupInitSingleton.getDtflStr();
					LOG.info("DataFlow : <{}>", dtflString); 
				}
			}//end isBcOn()
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening AMQ queue {} error: {}", queue,
					e.getMessage());
		}
	}
	
	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close tibco session: {}", e);
			} 
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
			if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				String message = msg.getText();
				LOG.debug("======================== START/END ========================");
				LOG.debug("Received msg : \n{}", message);
				
				long start = System.currentTimeMillis();
				//Internal communication via AMQ is in JSON format
				//ObjectMapper mapper = new ObjectMapper();
				EntUfisMsgDTO ufisMsgDTO = mapper.readValue(message, EntUfisMsgDTO.class);
				clsBlAMQRouter.routeUfisAMQMessage(ufisMsgDTO, dtflString);
				LOG.info("Total processing time : {} ms", (System.currentTimeMillis() - start));
			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass().getName());
			}
		} catch (JMSException e) {
			LOG.error("AMQ Session: {}", session);
			LOG.error("AMQ destination: {}", destination.toString());
			LOG.error("AMQ consumer: {}", consumer);
			LOG.error("JMSException {}", e);
		} catch (Exception te) {
			LOG.error("Exception {}", te);
		}
	}
}
