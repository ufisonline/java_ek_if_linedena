package com.ufis_as.ufisapp.ek.mdb;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.lcm.BlHandleLCMStripBean;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

public class BlUfisLCMMessageBean implements IBlUfisTibcoMDB {
	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisLCMMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private BlHandleLCMStripBean blHandleLCMBean;

	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;

	/*@PostConstruct
	public void init() {*/
	@Override
	public void init(String queue) {
		//String queue = null;
		try {
			List<InterfaceConfig> configList = _startupInitSingleton
					.getInterfaceConfigs();
			if (configList.size() < 1) {
				LOG.error("The configuration is not set properly in Config File. Pls check the configuration.");
			}
			Iterator<InterfaceConfig> it = configList.iterator();

			while (it.hasNext()) {
				InterfaceConfig inConfig = it.next();
				if (HpEKConstants.LOAD_CONTROL_MSG.equalsIgnoreCase(inConfig
						.getName())) {
					if (HpUfisUtils.isNullOrEmptyStr(queue)) {
						/*
						 * if
						 * (!_startupInitSingleton.getToQueueList().isEmpty()) {
						 * queue = _startupInitSingleton.getToQueueList()
						 * .get(0);
						 */
						if (!_startupInitSingleton.getFromQueueList().isEmpty()) {
							queue = _startupInitSingleton.getFromQueueList()
									.get(0);
						} else {
							// queue = HpEKConstants.DEFAULT_LCM_QUEUE;
							queue = HpEKConstants.DEFAULT_LCM_STRIP_QUEUE;
							LOG.warn(
									"!!!WARNING, queue is not specified. Defaulting to queue: {}",
									queue);
						}
					}
					/*
					 * session = _connSingleton.getActiveMqConnect()
					 * .createSession(false, Session.AUTO_ACKNOWLEDGE);
					 */
					session = _connSingleton.getTibcoConnect().createSession(
							false, Session.AUTO_ACKNOWLEDGE);
					if (session != null) {
						destination = session.createQueue(queue);
						if (destination != null) {
							consumer = session.createConsumer(destination);
							if (consumer != null) {
								consumer.setMessageListener(this);
								LOG.info("Successfully Connected to queue:"
										+ queue);
							} else {
								LOG.error(
										"!!!ERROR, creating Consumer for destination: {}",
										queue);
							}
						} else {
							LOG.error(
									"!!!ERROR, creating destination for queue: {}",
									queue);
						}
					} else {
						LOG.error("!!!ERROR, creating AMQ session . Queue: {}",
								queue);
					}
				}
			}
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening AMQ queue {} error: {}", queue,
					e.getMessage());
		} catch (Exception e) {
			LOG.error("!!!ERROR, Connecting to AMQ queue {} error: {}", queue,
					e);
		}
	}

	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close  session: {}", e);
		}
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		long irmtabRef = 0;
		try {
			if (inMessage instanceof TextMessage) {
				long startTime = new Date().getTime();
				LOG.debug("1.Received at: {}", new Date());

				msg = (TextMessage) inMessage;
				String message = msg.getText();

				LOG.info("Original Message from AMQ Queue:" + msg.getText());

				if (_startupInitSingleton.getIrmLogLev().equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {

					LOG.debug("2.Before IRMTAB at: {}", new Date());
					irmtabRef = _irmtabFacade.storeRawMsg(inMessage,
							_startupInitSingleton.getDtflStr());
					if (irmtabRef == 0) {
						LOG.error("Error in inserting msg into IRMTAB.");
					}
					LOG.debug("3.After IRMTAB at: {}", new Date());
				}
				LOG.debug("Before LCM Router at: {}", new Date());
				blHandleLCMBean.routeMessage(msg, message, irmtabRef);
				LOG.debug("After LCM Router at: {}", new Date());
				long endTime = new Date().getTime();
				LOG.debug(
						"Total Duration on processing the message (in ms): {}",
						endTime - startTime);

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}

		} catch (JMSException e) {
			LOG.error("JMSException {}", e);
		} catch (Throwable te) {
			LOG.error("JMSException  Throwable {}", te);
		}

	}
/*
	@Override
	public void init(String destination) {
		// TODO Auto-generated method stub

	}*/
}
