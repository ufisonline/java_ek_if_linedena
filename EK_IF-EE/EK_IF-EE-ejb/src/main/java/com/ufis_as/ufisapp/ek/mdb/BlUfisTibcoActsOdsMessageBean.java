package com.ufis_as.ufisapp.ek.mdb;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BlRouterActsOds;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

//@Singleton
//@Startup
//@DependsOn("ConnFactorySingleton")
//@DependsOn("ConnFactory")
public class BlUfisTibcoActsOdsMessageBean implements IBlUfisTibcoMDB {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisTibcoActsOdsMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	// private IBlIrmtabUpdateLocal _irmtabLocal;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlRouterActsOds _actsOdsRouter;

	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;

	@Override
	public void init(String queue) {
		// String queue = null;
		try {
			// LOG.debug("In BlUfisTibcoActsOdsMessageBean Class");
			List<InterfaceConfig> configList = _startupInitSingleton
					.getInterfaceConfigs();
			if (configList.size() < 1) {
				LOG.error("The configuration is not set properly in Config File. Pls check the configuration.");
			}
			Iterator<InterfaceConfig> it = configList.iterator();

			while (it.hasNext()) {
				InterfaceConfig inConfig = it.next();
				if ("acts".equalsIgnoreCase(inConfig.getName())) {
					// if (!_startupInitSingleton.getFromQueueList().isEmpty())
					// {
					if (HpUfisUtils.isNullOrEmptyStr(queue)) {
						if (!_startupInitSingleton.getFromQueueList().isEmpty()) {
							queue = _startupInitSingleton.getFromQueueList()
									.get(0);
						} else {
							queue = HpEKConstants.DEFAULT_ACTS_QUEUE;
							LOG.warn(
									"!!!WARNING, tibco queue is not specified. Defaulting to queue: {}",
									queue);
						}
					}
					LOG.debug("Queue Name:{}", queue);
					session = _connSingleton.getTibcoConnect().createSession(
							false, Session.AUTO_ACKNOWLEDGE);
					if (session != null) {
						destination = session.createQueue(queue);
						if (destination != null) {
							consumer = session.createConsumer(destination);
							if (consumer != null) {
								consumer.setMessageListener(this);
							} else {
								LOG.error(
										"!!!ERROR, creating Consumer for destination: {}",
										queue);
							}
						} else {
							LOG.error(
									"!!!ERROR, creating destination for queue: {}",
									queue);
						}
					} else {
						LOG.error(
								"!!!ERROR, creating tibco session for queue: {}",
								queue);
					}
				}

			}

		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		} catch (Exception e) {
			LOG.error("!!!ERROR, Connecting to TIBCO queue {} error: {}",
					queue, e);
		}
	}

	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
		} catch (JMSException e) {
			LOG.error("!!!Cannot close tibco session: {}", e);
		}
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		long irmtabRef = 0;
		try {
			if (inMessage instanceof TextMessage) {
				LOG.debug("1.Received at: {}", new Date());
				long startTime = new Date().getTime();
				msg = (TextMessage) inMessage;
				LOG.info("Original Message from Queue:" + msg.getText());
				if (HpCommonConfig.irmtab.equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {

					LOG.debug("2.Before IRMTAB at: {}", new Date());
					irmtabRef = _irmtabFacade.storeRawMsg(inMessage,
							HpCommonConfig.dtfl);
					LOG.debug("3.After IRMTAB at: {}", new Date());
				}
				LOG.debug("4.Before ACTS_ODS Router at: {}", new Date());
				_actsOdsRouter.routeMessage(inMessage, null,
						new Long(irmtabRef));

				LOG.debug("5.After ACTS_ODS Router at: {}", new Date());
				long endTime = new Date().getTime();
				LOG.debug(
						"Total Duration on processing the message (in ms): {}",
						endTime - startTime);
			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}

		} catch (JMSException e) {
			LOG.error("JMSException {}", e);
		} catch (Throwable te) {
			LOG.error("JMSException  Throwable {}", te);
		}

	}

}
