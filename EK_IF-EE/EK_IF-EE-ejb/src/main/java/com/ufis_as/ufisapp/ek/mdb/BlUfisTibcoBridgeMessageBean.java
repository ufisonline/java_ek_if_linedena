/*
 * $Id: BlUfisBridgeMessageBean.java 6457 2013-03-14 11:54:22Z gfo $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

/*@Singleton
@Startup
@DependsOn("ConnFactory")*/
public class BlUfisTibcoBridgeMessageBean implements IBlUfisTibcoMDB{

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisTibcoBridgeMessageBean.class);
	//@EJB
	//private ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	
	private Connection conn = null;
	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;
	
	@Override
	public void init(String queue) {
		try {
			//if (_connSingleton.isTibcoOn()) {
			if (HpCommonConfig.isTibcoOn) {
				//conn = _connSingleton.getTibcoConnect();
				conn = HpCommonConfig.tibcoConn;
				session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				destination = session.createQueue(queue);
				consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);
				//conn.start();
				LOG.info("!!!! Listening to TIBCO queue {}", queue);	
			}
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", destination, e.getMessage());
		}
	}
	
	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
					LOG.debug("TIBOC mdb existing session is closed.");
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close tibco session: {}", e);
			} 
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		long irmtabRef = 0;
		try {
			if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				LOG.info("Received message : " + msg.getText());
				long start = System.currentTimeMillis();
				if (_startupInitSingleton.getIrmLogLev().equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {
					irmtabRef = _irmtabFacade.storeRawMsg(inMessage, _startupInitSingleton.getDtflStr());//get the cur URNO number
				}
				
				// if log error, get the next urno 
				if (_startupInitSingleton.getIrmLogLev().equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_ERR.name())) {
					irmtabRef = _irmtabFacade.getIrmNextUrno(_startupInitSingleton.getDtflStr());
				}
				
				// route message to process
				_entRouter.routeMessageWithDTFL(msg, _startupInitSingleton.getDtflStr(), irmtabRef);
				LOG.info("Total msg processing time : {}ms", (System.currentTimeMillis() - start));
			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass().getName());
				LOG.warn("Expected message tyep: TextMessage");
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e);
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco destination: {}", destination.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
//	@Override
//	public void onException(JMSException jmse) {
//		LOG.error("onException: [JMSExcepion: {}]", jmse.getLinkedException().getMessage());
//		try {
//			// close connection
//			conn.close();
//			
//			// recreate it
//			InterfaceMqConfig fromMqConfig = _connSingleton.getInfConfig().getFromMq();
//			TibjmsConnectionFactory tibcoConnFactory = new TibjmsConnectionFactory(fromMqConfig.getUrl());
//			/* set heartbeat interval to detect network issues */
//			//Tibjms.setPingInterval(10);
//			/*  fault-tolerant switch */
//			//Tibjms.setExceptionOnFTSwitch(true);
//			
//			tibcoConnFactory.setConnAttemptCount(10);
//			tibcoConnFactory.setConnAttemptDelay(1000);
//			tibcoConnFactory.setConnAttemptTimeout(1000);
//			tibcoConnFactory.setReconnAttemptCount(1000000000);
//			tibcoConnFactory.setReconnAttemptDelay(1000);
//			tibcoConnFactory.setReconnAttemptTimeout(1000);
//			
//			while (true) {
//				/* create the connection */
//				try {
//					_connSingleton.setTibcoConn(tibcoConnFactory.createConnection(
//							fromMqConfig.getUserName(), fromMqConfig.getPassword()));
//					LOG.info("Recreated connection of tibco");
//					break;
//				} catch(JMSException e) {
//					LOG.error("Still cannot create connection to Tibco, wait...");
//					Thread.sleep(1000);
//				}
//			}
//			init();
//		} catch (Exception e) {
//			LOG.error("!!!!ERROR,connect to TIBCO error : {}", e);
//		}
//	}
	
}
