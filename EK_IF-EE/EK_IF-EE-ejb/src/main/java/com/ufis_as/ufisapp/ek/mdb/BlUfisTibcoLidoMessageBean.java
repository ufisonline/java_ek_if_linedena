/***package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.intf.aacs.BlHandleUldBean;
import com.ufis_as.ufisapp.ek.intf.lido.BlHandleLidoBean;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

public class BlUfisTibcoLidoMessageBean implements IBlUfisTibcoMDB{ 
//implements MessageListener{ 

	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisTibcoLidoMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlHandleUldBean _blRouterUld;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
    private BlHandleLidoBean clsBlHandleLidoBean;
	
	private Connection conn = null;
	private Session session = null;
	private Destination dest = null;
	private MessageConsumer consumer = null;
	
	/**
	 * - call from BlUfisTibcoAdapterStartUp
	 * - method to create MULTIPLE MDB instances for LIDO 
	 *   /
	@Override
	public void init(String queue) {
		try {
			//destroy(); // close jms Session if exist
			
			if (_connSingleton.isTibcoOn()) {
				conn = _connSingleton.getTibcoConnect();
				session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				dest = session.createQueue(queue);
				consumer = session.createConsumer(dest);
				consumer.setMessageListener(this);
				conn.start();
				LOG.info("!!!! Listening to TIBCO queue {}", queue);	
			}
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", dest,e.getMessage());
		}
	}
	
/*	@PostConstruct
	public void init() {
		String queue = null;
		try {
			queue = _startupInitSingleton.getFromQueueList().get(0);//Q for FSUM
			session = _connSingleton.getTibcoConnect().createSession(false, Session.AUTO_ACKNOWLEDGE);
			dest = session.createQueue(queue);
			consumer = session.createConsumer(dest);
			consumer.setMessageListener(this);
			LOG.info("!!!! Listening to TIBCO queue {}", queue);
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}
	}*  //
	
	
	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close tibco session: {}", e);
			} 
	}
	
	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
				if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				String message = msg.getText();
				//LOG.debug("Received message : \n{}", message);
				
				if (_startupInitSingleton.isIrmOn()){
					_irmtabFacade.storeRawMsg(inMessage, _startupInitSingleton.getDtflStr());
					//LOG.debug("==== Inserted received msg to IRMTAB ====");
				}
				clsBlHandleLidoBean.processLidoRouter(message);
				
			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass().getName());
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco destination: {}", dest.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}
}***/
