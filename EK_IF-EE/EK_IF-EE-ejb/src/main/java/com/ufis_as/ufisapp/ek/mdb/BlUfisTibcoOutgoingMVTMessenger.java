package com.ufis_as.ufisapp.ek.mdb;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.ek.singleton.OUTMVTConnFactorySingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlOrmtabFacade;

//@Singleton
//@Startup
public class BlUfisTibcoOutgoingMVTMessenger {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisTibcoOutgoingMVTMessenger.class);

	@EJB
	private OUTMVTConnFactorySingleton outMVTConnFactorySingleton;
	
	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	
	private Session session;
	private MessageProducer messageProducer;
	
	private String queueName;
	
	@EJB
	private BlOrmtabFacade blOrmtabFacade;

	@PostConstruct
	public void init() {
		if(entStartupInitSingleton.getInterfaceConfigs().isEmpty()) {
			LOG.warn("No Interface set up for TIBCO connection. It should be set up at least one.");
			return;
		}
		
		//Assumption: Interface set up is now one only.
//			InterfaceConfig config = entStartupInitSingleton.getInterfaceConfigs().get(0);
		List<String> toQueueList = entStartupInitSingleton.getToQueueList();  //TIBCO
//			InterfaceMqConfig toMq = config.getToMq(); //TIBCO
		
		if(toQueueList.isEmpty()) {
			LOG.warn("\"TO:TIBCO queue name\" has not been configured yet. No message will be sent out.");
			return;
		}
		
		queueName = toQueueList.get(0);
		if(queueName.isEmpty()) {
			LOG.warn("\"TO:TIBCO queue name\" has not been configured yet. No message will be sent out.");
			return;
		}
		
		initializeTIBCOSession();
	}

	private boolean initializeTIBCOSession() {
		try {
			destroy();
			session = outMVTConnFactorySingleton.getTibcoConnect().createSession(false, Session.AUTO_ACKNOWLEDGE);
			messageProducer = session.createProducer(session.createQueue(queueName));
			return true;
		} catch (JMSException e) {
			LOG.error("!!!Cannot initiate TIBCO session: {}", e);
			LOG.error("Cannot initiate TIBCO queue : {}", queueName);
		}
		return false;
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close TIBCO session: {}", e);
			} 
	}

	public void sendMessage(String textMessage, String messageType) {
		int sleep = 200;
		
		Date before = new Date();
		Date after = before;
		
		boolean sent = false;
		int oneHr = 3600 * 1000;
		
		blOrmtabFacade.storeRawMsg(textMessage, String.format("Queue[%s]", queueName));
		
		while(!sent && (after.getTime()-before.getTime()) < oneHr) {
			try {
				send(textMessage, messageType);
				sent = true;
			} catch(Exception e) {
				LOG.error("!!!JMS Exception is encountered when sending the message to TIBCO : {}", e.getMessage());
				LOG.info("Trying to send again...");
				after = new Date();
				Date d1 = new Date();
				LOG.info("Reconnecting started... {} ms", d1.getTime());
				try {
					Thread.sleep(sleep);
					boolean reconnected = outMVTConnFactorySingleton.reconnectTIBCO();
					Date d2 = new Date();
					LOG.info("Reconnecting ended... {} ms", d2.getTime());
					LOG.info("Reconnecting TIBCO duration : {} ms", d2.getTime() - d1.getTime());
					if(reconnected) {
						boolean initialized = initializeTIBCOSession();
						if(initialized) {
							send(textMessage, messageType);
							sent = true;
						}
					}
				} catch (JMSException je) {
					LOG.error("After reconnected, initializing message session error is encountered. {}", je.getMessage());
				} catch (InterruptedException e1) {
					LOG.error("While waiting to reconnect TIBCO, error is encountered. {}", e1.getMessage());
				}
			}
		}
		
		if(!sent) {
			blOrmtabFacade.updateORMStatus("TIBCO");
		}
	}

	private void send(String textMessage, String messageType) throws JMSException {
		LOG.info("Start sending message to TIBCO...");
		TextMessage msg = session.createTextMessage();
		msg.setText(textMessage);
		messageProducer.send(msg);
		LOG.debug("!!!Sent a {} message to TIBCO Queue : {}", messageType, queueName);
	}

}
