package com.ufis_as.ufisapp.ek.mdb;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.ek.intf.proveo.BlProveoUpdateHandler;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

//@Singleton(name = "BlUfisTibcoProveoMessageBean")
//@Startup
//@DependsOn("ConnFactorySingleton")
public class BlUfisTibcoProveoMessageBean implements IBlUfisTibcoMDB {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisTibcoProveoMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private BlProveoUpdateHandler _proveoRouter;

	private Session session = null;
	//private Destination destination = null;
	//private MessageConsumer consumer = null;
	List<String> queueList = new ArrayList<String>();
	List<String> defaultQueueList = new ArrayList<String>();

	@PostConstruct
	public void init(String queue) {				
		// String queueName = null;
		try {
			List<InterfaceConfig> configList = _startupInitSingleton
					.getInterfaceConfigs();
			if(defaultQueueList.isEmpty()){
			defaultQueueList.add(HpEKConstants.DEFAULT_PROVEO_LOC_QUEUE);
			defaultQueueList.add(HpEKConstants.DEFAULT_PROVEO_STATUS_QUEUE);
			}
			if (configList.size() < 1) {
				LOG.error("The configuration is not set properly in Config File. Pls check the configuration.");
			}
			Iterator<InterfaceConfig> it = configList.iterator();

			while (it.hasNext()) {
				InterfaceConfig inConfig = it.next();
				if ("proveo".equalsIgnoreCase(inConfig.getName())) {
					if (HpUfisUtils.isNullOrEmptyStr(queue)) {
						if (!defaultQueueList.isEmpty()) {
							for(String queueName:defaultQueueList)
							{
								if(!queueList.contains(queueName))
								{
									queue=queueName;
									LOG.warn(
											"!!!Warning, Queues are not configured. Assuming default queues: {}",
											queue);
									
								}
							}
						}													
						}
					 else {
						queueList.add(queue);
					}
					session = _connSingleton.getTibcoConnect()
							.createSession(false, Session.AUTO_ACKNOWLEDGE);					
					if (session != null) {
						Destination destination = session.createQueue(queue);
						if (destination != null) {
							MessageConsumer	consumer = session.createConsumer(destination);
							if (consumer != null) {
								consumer.setMessageListener(this);
								LOG.info("Successfully Connected to queue:"
										+ queue);
							} else {
								LOG.error(
										"!!!ERROR, creating Consumer for destination: {}",
										queue);
							}
						} else {
							LOG.error(
									"!!!ERROR, creating destination for queue: {}",
									queue);
						}
					} else {
						LOG.error(
								"!!!ERROR, creating tibco session . Queue: {}",
								queue);
					}
				}

					/*for (int i = 0; i < queue.size(); i++) {
						queueName = queue.get(i);
						if (HpUfisUtils.isNullOrEmptyStr(queueName)) {
							LOG.warn("!!!Warning, Queue name is null or empty",
									queueName);
						} else {
							if (session != null) {
								Destination destination = session
										.createQueue(queue.get(i));
								if (destination != null) {
									MessageConsumer consumer = session
											.createConsumer(destination);
									if (consumer != null) {
										consumer.setMessageListener(this);
										LOG.info("Successfully Connected to queue:"
												+ queueName);
									} else {
										LOG.error(
												"!!!ERROR, creating Consumer for destination: {}",
												queueName);
									}
								} else {
									LOG.error(
											"!!!ERROR, creating destination for queue: {}",
											queueName);
								}
							} else {
								LOG.error(
										"!!!ERROR, creating tibco session for queue: {}",
										queueName);
							}
						}
					}*/									
			}
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}",
					queue, e);
		} catch (Exception e) {
			LOG.error("!!!ERROR, Connecting to TIBCO queue {} error: {}",
					queue, e);
		}
	}
	
	@PreDestroy
	public void destroy() {
		try {
			if (session != null) {
				session.close();
			}
			defaultQueueList.clear();
			queueList.clear();
		} catch (JMSException e) {
			LOG.error("!!!Cannot close tibco session: {}", e);
		}
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		long irmtabRef = 0;
		try {
			if (inMessage instanceof TextMessage) {
				long startTime = new Date().getTime();
				LOG.info("1.Received at: {}", new Date());
				msg = (TextMessage) inMessage;
				LOG.info("Original Message from Queue:" + msg.getText());
				if (HpCommonConfig.irmtab.equalsIgnoreCase(
						HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name())) {

					LOG.debug("2.Before IRMTAB at: {}", new Date());
					irmtabRef = _irmtabFacade.storeRawMsg(inMessage,
							HpCommonConfig.dtfl);
					if (irmtabRef == 0) {
						LOG.error("Error in inserting msg into IRMTAB.");
					}
					LOG.debug("3.After IRMTAB at: {}", new Date());
				}
				LOG.info("4.Before PROVEO Router at: {}", new Date());
				_proveoRouter.routeMessage(inMessage, msg.getText(), null,new Long(irmtabRef));
				LOG.info("5.After PROVEO Router at: {}", new Date());
				long endTime = new Date().getTime();
				LOG.info(
						"Total Duration on processing the message (in ms): {}",
						endTime - startTime);

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}

		} catch (JMSException e) {
			LOG.error("JMSException {}", e);
		} catch (Throwable te) {
			LOG.error("JMSException  Throwable {}", te);
		}

	}
}
