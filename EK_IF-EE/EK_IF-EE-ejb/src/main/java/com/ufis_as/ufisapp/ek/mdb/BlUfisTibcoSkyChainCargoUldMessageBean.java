/*
 * $Id: BlUfisTibcoSkyChainCargoUldMessageBean.java lma $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.mdb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.skychain.BlHandleSkyChainULDBean;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

@Deprecated
public class BlUfisTibcoSkyChainCargoUldMessageBean implements MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisTibcoSkyChainCargoUldMessageBean.class);
	@EJB
	ConnFactorySingleton connSingleton;
	@EJB
	private BlHandleSkyChainULDBean blSkyChainUld;
	@EJB
	private BlIrmtabFacade irmtabFacade;
	@EJB
	private EntStartupInitSingleton startupInitSingleton;
	
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;

	@PostConstruct
	public void init() {
		String queue = null;
		try {
			List<String> fromQueueList = startupInitSingleton.getFromQueueList();
			if(fromQueueList == null || fromQueueList.isEmpty()) {
				LOG.error("The queue name cannot be retrieved for the config.");
				return;
			}
			
			queue = fromQueueList.get(0);
			LOG.debug("Cargo ULD Queue Name - {}", queue);
			session = connSingleton.getTibcoConnect().createSession(
					false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queue);
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
			LOG.info("!!!! Listening to TIBCO queue {}", queue);
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
				if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				String message = msg.getText();
				
				if (startupInitSingleton.isIrmOn()) {
					irmtabFacade.storeRawMsg(inMessage,startupInitSingleton.getDtflStr());
					LOG.debug("==== Inserted received msg to IRMTAB ====");
				}
				
				if(message.contains("ULDInfo")){
					blSkyChainUld.processULD(message, 0);
				} else {
					irmtabFacade.updateIRMStatus("Invalid");
					LOG.warn("Input uld info message is incorrect. Message will be dropped. ");
				}
			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco destination: {}", destination.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}
	
}
