/*
 * $Id: BlUfisBridgeMessageBean.java 6457 2013-03-14 11:54:22Z gfo $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.mdb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.intf.aacs.BlHandleUldBean;
import com.ufis_as.ufisapp.ek.intf.skychain.BlHandleSkyChainUWSBean;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

@Deprecated
public class BlUfisTibcoSkyChainUwsUldMessageBean implements MessageListener {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisTibcoSkyChainUwsUldMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlHandleUldBean _blRouterUld;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	@EJB
	private BlHandleSkyChainUWSBean _blSkyChainUWS;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	
	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;

	@PostConstruct
	public void init() {
		String queue = null;
		try {
			List<String> fromQueueList = _startupInitSingleton.getFromQueueList();
			if(fromQueueList == null || fromQueueList.isEmpty()) {
				LOG.error("The queue name cannot be retrieved for the config.");
				return;
			}
			
			if(fromQueueList.size() == 1) {
				LOG.error("The UWS queue name cannot be retrieved for the config.");
				return;
			}
			
			queue = fromQueueList.get(1);
//			queue = "AOO.HUBMATRIX.SKYCHAIN.UFIS.UWS";//_connSingleton.getqFromMqList().get(1);//ULD_SCANNING Queue
			LOG.debug("UWS ULD Queue Name - {}", queue);
			session = _connSingleton.getTibcoConnect().createSession(
					false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queue);
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(this);
			LOG.info("!!!! Listening to TIBCO queue {}", queue);
		} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}
		
	}

	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
				if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				String message = msg.getText();
//				LOG.debug("Received message : \n{}", msg.getText());
				
				if (_startupInitSingleton.isIrmOn()) {
					_irmtabFacade.storeRawMsg(inMessage,_startupInitSingleton.getDtflStr());
					LOG.debug("==== Inserted received msg to IRMTAB ====");
				}
				
				LOG.debug("==== Inserted received msg to IRMTAB ====");
				if(message.contains("UWS")){
					_blSkyChainUWS.processUWS(message, 0);
				}
				else {
					_irmtabFacade.updateIRMStatus("Invalid");
					LOG.warn("Input uws info message is incorrect. Message will be dropped. ");					
				}

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco destination: {}", destination.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}
}
