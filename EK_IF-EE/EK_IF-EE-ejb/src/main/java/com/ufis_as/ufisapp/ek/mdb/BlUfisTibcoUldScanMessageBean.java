package com.ufis_as.ufisapp.ek.mdb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.BlRouter;
import com.ufis_as.ufisapp.ek.intf.aacs.BlHandleUldBean;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;

//@Singleton
//@Startup
public class BlUfisTibcoUldScanMessageBean implements MessageListener {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlUfisTibcoUldScanMessageBean.class);
	@EJB
	ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	private BlRouter _entRouter;
	@EJB
	private BlHandleUldBean _blRouterUld;
	@EJB
	private BlIrmtabFacade _irmtabFacade;
	
	private Connection conn = null;
	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;

	@PostConstruct
	public void init() {
		/*String queue = null;
		try {
			if (_connSingleton.isTibcoOn()) {
				conn = _connSingleton.getTibcoConnect();
				queue = _startupInitSingleton.getFromQueueList().get(1);//ULD Scan info Q
				session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				destination = session.createQueue(queue);
				consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);
				conn.start();
				LOG.info("!!!! Listening to TIBCO queue {}", queue);							
			}
			} catch (JMSException e) {
			LOG.error("!!!ERROR, Listening TIBCO queue {} error: {}", queue,
					e.getMessage());
		}*/
	}

	@Override
	public void onMessage(Message inMessage) {
		TextMessage msg;
		try {
				if (inMessage instanceof TextMessage) {
				msg = (TextMessage) inMessage;
				String message = msg.getText();
				LOG.debug("Received message : \n{}", msg.getText());
				
				//_irmtabFacade.storeRawMsg(inMessage);
				if (_startupInitSingleton.isIrmOn()){
					_irmtabFacade.storeRawMsg(inMessage, _startupInitSingleton.getDtflStr());
					//LOG.debug("==== Inserted received msg to IRMTAB ====");
				}
				if(message.contains("ULD_SCANNING_DETAILS")){
					//_blRouterUld.processULDScanning(message, 0);
				}
				else
					LOG.warn("Input uld scanning message is incorrect. Message will be dropped. ");

			} else {
				LOG.warn("Message of wrong type: {}", inMessage.getClass()
						.getName());
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		} catch (Exception te) {
			LOG.error("Tibco Session: {}", session);
			LOG.error("Tibco destination: {}", destination.toString());
			LOG.error("Tibco consumer: {}", consumer);
			LOG.error("Exception {}", te);
		}
	}
}
