package com.ufis_as.ufisapp.ek.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisNotifyFormatter;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

@Stateless(name = "BlUfisBCQueue")
public class BlUfisBCQueue {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisBCQueue.class);

	private String ufisQueue;
	private Session session;
	private MessageProducer messageProducer;
	private Destination _targetufisQueue;
	
	@PostConstruct
	public void init() {
		try {
			if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyQueue)) {
				ufisQueue = HpCommonConfig.notifyQueue;
				session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				_targetufisQueue = session.createQueue(ufisQueue);
				messageProducer = session.createProducer(_targetufisQueue);
				LOG.info("!!!! Connect to Queue: {}", ufisQueue);
			}
		} catch (Exception e) {
			LOG.error("!!!Cannot initial amq session: {}", e);
		}
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close amq session: {}", e);
			} 
	}

	public void sendMessage(String textMessage) {
		try {
			//if (_connSingleton.getActiveMqConnect() != null) {
			if (session != null) {
				TextMessage msg = session.createTextMessage();
				msg.setText(textMessage);
				messageProducer.send(msg);
				LOG.debug("Send to AMQ Queue : {}", ufisQueue);
				LOG.debug("Msg: {}", textMessage);
			} else {
				LOG.error("!!! Active mq connection is null");
			}
		} catch (Exception e) {
			LOG.error("!!! Receive notification message from ufis mq error: {}", e);
		}
	}
	
	/**
	 * Sending notification to queue
	 * @param isLast ------> bcNum
	 * @param cmd
	 * @param oldData
	 * @param data
	 */
	public void sendNotification(boolean isLast, UfisASCommands cmd, String idFlight, Object oldData, Object data) {
		try {
			if (session != null) {
				String notificationMsg = HpUfisNotifyFormatter.getInstance()
						.transform(isLast, cmd, idFlight, oldData, data);
				if (notificationMsg != null) {
					// Send notification
					TextMessage msg = session.createTextMessage();
					msg.setText(notificationMsg);
					messageProducer.send(msg);
					LOG.debug("Send notification to Queue: {}", ufisQueue);
				}
			}
		} catch (Exception e) {
			LOG.error("!!! Receive notification message from ufis mq error: {}", e);
		}
	}
	
	/**
	 * Sending notification message asynchronously
	 * @param isLast
	 * @param cmd
	 * @param idFlight
	 * @param oldData
	 * @param data
	 */
	@Asynchronous
	public void sendNotificationAsyn(boolean isLast, UfisASCommands cmd, String idFlight, Object oldData, Object data) {
		sendNotification(isLast, cmd, idFlight, oldData, data);
	}
}
