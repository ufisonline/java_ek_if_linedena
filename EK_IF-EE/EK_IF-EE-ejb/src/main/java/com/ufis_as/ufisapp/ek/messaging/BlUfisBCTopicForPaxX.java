package com.ufis_as.ufisapp.ek.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants.UfisASCommands;
import com.ufis_as.ufisapp.ek.hp.HpUfisNotifyFormatterForPax;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

@Stateless(name = "BlUfisBCTopicForPaxX")
public class BlUfisBCTopicForPaxX {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisBCTopicForPaxX.class);

	private String ufisTopic;
	private Session session;
	private MessageProducer messageProducer;
	private Destination _targetUfisTopic;
	
	@EJB
	HpUfisNotifyFormatterForPax hpUfisNotifyFormatterForPax;
	
	@PostConstruct
	public void init() {
		try {
			if (HpUfisUtils.isNotEmptyStr(HpCommonConfig.notifyTopic)) {
				ufisTopic = HpCommonConfig.notifyTopic;
//				session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
//				_targetUfisTopic = session.createTopic(ufisTopic);
//				messageProducer = session.createProducer(_targetUfisTopic);
//				if (HpCommonConfig.topicMsgTimeout != 0) {
//					messageProducer.setTimeToLive(HpCommonConfig.topicMsgTimeout*1000);
//					LOG.info("Notification topic message timeout set to: {} seconds", HpCommonConfig.topicMsgTimeout);
//				} else {
//					// default is 1hour
//					messageProducer.setTimeToLive(HpUfisAppConstants.DEF_MSG_TIMEOUT*1000);
//					LOG.info("Default notification topic message timeout: {} seconds", HpUfisAppConstants.DEF_MSG_TIMEOUT);
//				}
				LOG.info("!!!! Connect to topic: {}", ufisTopic);
			}
		} catch (Exception e) {
			LOG.error("!!!Cannot initial amq session: {}", e);
		}
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close amq session: {}", e);
			} 
	}

	public void sendMessage(String textMessage) {
		try {
			if (session != null) {
				TextMessage msg = session.createTextMessage();
				msg.setText(textMessage);
				messageProducer.send(msg);
				LOG.debug("Send to AMQ Topic : {}", ufisTopic);
			} else {
				LOG.error("!!! Active mq session is null");
			}
		} catch (Exception e) {
			LOG.error("!!! Receive notification message from ufis mq error: {}", e);
		}
	}
	
	/**
	 * Sending notification to topic
	 * @param isLast ------> bcNum
	 * @param cmd
	 * @param oldData
	 * @param data
	 */
	public void sendNotification(boolean isLast, UfisASCommands cmd, String idFlight, Object oldData, Object data) {
		long startTime = System.currentTimeMillis();
		
		try {
//			if (session != null) {
//				String notificationMsg = HpUfisNotifyFormatter.getInstance()
//						.transform(isLast, cmd, idFlight, oldData, data);
				// change for testing macs-pax
			session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			_targetUfisTopic = session.createTopic(ufisTopic);
			messageProducer = session.createProducer(_targetUfisTopic);
			messageProducer.setTimeToLive(HpUfisAppConstants.DEF_MSG_TIMEOUT*1000);
			
				startTime = System.currentTimeMillis();
				LOG.debug("notification msg format start");
				String notificationMsg = hpUfisNotifyFormatterForPax
						.transformForPax(isLast, cmd, idFlight, oldData, data);
				LOG.debug("notification msg format end");
				LOG.debug("format notification msg, takes {}",System.currentTimeMillis() - startTime);
				
				if (notificationMsg != null) {
					startTime = System.currentTimeMillis();
					LOG.debug("Send notification start");
					// Send notification
					TextMessage msg = session.createTextMessage();
					msg.setText(notificationMsg);
					messageProducer.send(msg);
					
					session.close();
					
					LOG.debug("Send notification end");
					LOG.debug("Send notification, takes {}",System.currentTimeMillis() - startTime);
					LOG.debug("Send notification to Topic: {}", ufisTopic);
				}
//			}
		} catch (Exception e) {
			LOG.error("!!! Receive notification message from ufis mq error: {}", e);
		}
		
		LOG.debug("send db change notification, takes {} ms", System.currentTimeMillis() - startTime);
		
	}
	
	/**
	 * Sending notification message asynchronously
	 * @param isLast
	 * @param cmd
	 * @param idFlight
	 * @param oldData
	 * @param data
	 */
	@Asynchronous
	public void sendNotificationAsyn(boolean isLast, UfisASCommands cmd, String idFlight, Object oldData, Object data) {
		sendNotification(isLast, cmd, idFlight, oldData, data);
	}
	
}
