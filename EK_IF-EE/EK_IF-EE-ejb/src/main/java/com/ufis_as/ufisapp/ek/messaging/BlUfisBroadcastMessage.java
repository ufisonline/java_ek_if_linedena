package com.ufis_as.ufisapp.ek.messaging;

import java.util.Date;

import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;

@Stateless
@DependsOn("EntStartupInitSingleton")
public class BlUfisBroadcastMessage {

	public static Logger LOG = LoggerFactory.getLogger(BlUfisBroadcastMessage.class);
	@EJB
	private ConnFactorySingleton clsConnSingleton;
	
	@EJB
	private EntStartupInitSingleton clsInitSingleton;

	private Session clsSession;
	private MessageProducer clsMessageProducer;
	private Destination clsTargetUfis;
	
	public BlUfisBroadcastMessage() {
	}
	
	public void sendBroadcastMessage(String message){
		try {
			for (int i = 0; i < clsInitSingleton.getToTopicList().size(); i++) {
				if (clsConnSingleton.getActiveMqConnect() != null) {
					clsSession = clsConnSingleton.getActiveMqConnect().createSession(false, Session.AUTO_ACKNOWLEDGE);
					String topic = clsInitSingleton.getToTopicList().get(i);
					clsTargetUfis = clsSession.createTopic(topic);
					clsMessageProducer = clsSession.createProducer(clsTargetUfis);
					//clsMessageProducer.setDeliveryMode(arg0);
					//clsMessageProducer.setTimeToLive(arg0);
					TextMessage msg = clsSession.createTextMessage();
					msg.setText(message);
					clsMessageProducer.send(msg);
					LOG.debug("!!!Send a broadcast message to {}", topic);
					LOG.debug("AMQ: After Send to Amq at: {}", new Date());
					
					clsMessageProducer.close();
					clsSession.close();
				} else {
					LOG.error("!!! Active mq connection is null");
				}
			}
		} catch (JMSException e) {
			LOG.error("ERROR : {}", e.toString());
		}
	}
	
	public void sendBroadcastMessageToQueue(String message) {
		if (clsConnSingleton.getActiveMqConnect() != null) {
			try {
				clsSession = clsConnSingleton.getActiveMqConnect()
						.createSession(false, Session.AUTO_ACKNOWLEDGE);
				String queue = clsInitSingleton.getBcToUfisQueue();
				clsTargetUfis = clsSession.createQueue(queue);
				clsMessageProducer = clsSession
						.createProducer(clsTargetUfis);
				TextMessage msg = clsSession.createTextMessage();
				msg.setText(message);
				clsMessageProducer.send(msg);
				LOG.debug("!!!Send a broadcast message to {} Msg : \n{}",
						queue, message);
				LOG.debug("AMQ: After Send to Amq at: {}", new Date());

				clsMessageProducer.close();
				clsSession.close();
			} catch (JMSException e) {
				LOG.error("ERROR : {}", e.toString());
			}
		}
	}
}
