package com.ufis_as.ufisapp.ek.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.ek.singleton.ConnFactorySingleton;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

@Stateless(name = "BlUfisCedaQueue")
public class BlUfisCedaQueue {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisCedaQueue.class);

	@EJB
	private ConnFactorySingleton _connSingleton;
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	
	private String ufisQueue;
	private Session session;
	private MessageProducer messageProducer;
	private Destination _targetufisQueue;

	@PostConstruct
	public void init() {
		try {
			if (HpUfisUtils.isNotEmptyStr(_startupInitSingleton.getToCedaQ())) {
				ufisQueue = _startupInitSingleton.getToCedaQ();
				session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				_targetufisQueue = session.createQueue(ufisQueue);
				messageProducer = session.createProducer(_targetufisQueue);
			}
		} catch (Exception e) {
			LOG.error("!!!Cannot initial amq session: {}", e);
		}
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close amq session: {}", e);
			} 
	}

	public void sendMessage(String textMessage) {
		try {
			if (session != null) {
				TextMessage msg = session.createTextMessage();
				msg.setText(textMessage);
				messageProducer.send(msg);
				LOG.debug("!!!Send a message to Ceda AMQ Queue : {}", ufisQueue);
				LOG.debug("Msg: \n{}", textMessage);
			} else {
				LOG.error("!!! Active mq to CEDA is null/empty");
			}
		} catch (Exception e) {
			LOG.error("!!!Receive notification message from ufis mq error: {}", e);
		}
	}
}
