package com.ufis_as.ufisapp.ek.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;

@Stateless(name = "BlUfisExceptionQueue")
public class BlUfisExceptionQueue {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisExceptionQueue.class);

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	
	private String ufisQueue;
	private Session session;
	private MessageProducer messageProducer;
	private Destination _targetufisQueue;

	@PostConstruct
	public void init() {
		try {
			if (_startupInitSingleton.getExptDataStore() != null) {
				ufisQueue = _startupInitSingleton.getExptDataStore();
				session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				_targetufisQueue = session.createQueue(ufisQueue);
				messageProducer = session.createProducer(_targetufisQueue);
				LOG.info("!!!! Connect to dataStore queue {}", ufisQueue);	
			}
		} catch (Exception e) {
			LOG.error("!!!Cannot initial amq session: {}", e);
		}
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close amq session: {}", e);
			} 
	}

	public void sendMessage(String textMessage) {
		try {
			if (session != null) {
				TextMessage msg = session.createTextMessage();
				msg.setText(textMessage);
				messageProducer.send(msg);
				LOG.debug("Send to AMQ Queue : {}", ufisQueue);
				LOG.debug("Msg :\n{}", textMessage);
			} else {
				LOG.error("!!! Active mq session is null");
			}
		} catch (Exception e) {
			LOG.error("!!! Receive notification message from ufis mq error: {}", e);
		}
	}
}
