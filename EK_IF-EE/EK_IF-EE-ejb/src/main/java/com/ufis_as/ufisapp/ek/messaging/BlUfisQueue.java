package com.ufis_as.ufisapp.ek.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

//@Singleton
//@DependsOn("coreConnFactory")
@Stateless(name = "BlUfisQueue")
public class BlUfisQueue {

	private static final Logger LOG = LoggerFactory.getLogger(BlUfisQueue.class);

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	
	private String ufisQueue;
	private Session session;
	private MessageProducer messageProducer;
	private Destination _targetUfisQueue;

	@PostConstruct
	public void init() {
		try {
			if (HpCommonConfig.isAmqOn) {
				if(_startupInitSingleton.getToQueueList().size() > 0){
					ufisQueue = _startupInitSingleton.getToQueueList().get(0);
					if (HpUfisUtils.isNotEmptyStr(ufisQueue)) {
						session = HpCommonConfig.activeMqConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
						_targetUfisQueue = session.createQueue(ufisQueue);
						messageProducer = session.createProducer(_targetUfisQueue);	
					} else {
						LOG.warn("JMS Destination Cannot be null or empty.");
					}
				}
			}
		} catch (Exception e) {
			LOG.error("!!!Cannot initial amq session: {}", e);
		}
	}

	@PreDestroy
	public void destroy() {
			try {
				if (session != null) {
					session.close();
				}
			} catch (JMSException e) {
				LOG.error("!!!Cannot close amq session: {}", e);
			} 
	}

	public void sendMessage(String textMessage) {
		try {
			//if (_connSingleton.getActiveMqConnect() != null) {
			if (session != null) {
				//LOG.debug("AMQ: Before Send to Amq at: {}", new Date());
				TextMessage msg = session.createTextMessage();
				msg.setText(textMessage);
				messageProducer.send(msg);
				LOG.debug("!!!Send a message to UFIS AMQ : {}", ufisQueue);
				//LOG.debug("AMQ: After Send to Amq at: {}", new Date());
			} else {
				LOG.error("!!! Active mq session is null");
			}
		} catch (Exception e) {
			LOG.error("!!!Receive notification message from ufis mq error: {}", e);
		}
	}
}
