package com.ufis_as.ufisapp.ek.singleton;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;

public class BlUfisTibcoAdapterStartUp {
	private static final Logger LOG = LoggerFactory.getLogger(BlUfisTibcoAdapterStartUp.class);
	@EJB
	private EntStartupInitSingleton _startupInitSingleton;
	@EJB
	IBlUfisTibcoMDB clsUfisTibcoMdb;
	
	@PostConstruct
	public void init() throws JMSException {
			try {
			// Initialize multiple MDB instances for different Q list Config
			for (String queue : _startupInitSingleton.getFromQueueList()) {
				clsUfisTibcoMdb.init(queue);
			}
		} catch (Exception e) {
			LOG.error("!!!!ERROR:", e);
		}
	}
}
