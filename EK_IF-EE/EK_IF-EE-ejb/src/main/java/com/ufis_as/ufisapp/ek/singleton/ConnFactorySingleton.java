package com.ufis_as.ufisapp.ek.singleton;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tibco.tibjms.TibjmsConnectionFactory;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.InterfaceMqConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB;
import com.ufis_as.ufisapp.lib.HpAESEncryptAndDecrypt;
import javax.jms.ExceptionListener;
import com.tibco.tibjms.Tibjms;

/*@Singleton
 @Startup*/
public class ConnFactorySingleton {

	private static final Logger LOG = LoggerFactory
			.getLogger(ConnFactorySingleton.class);

	@Resource(name = HpUfisAppConstants.DEFAULT_JNDICONFIGNAME)
	private String configFile;

	@EJB
	private EntStartupInitSingleton _startupInitSingleton;

	@EJB
	private BlUfisTibcoAdapterStartUp clsTibcoAdapterStartUp;

	@EJB
	private IBlUfisTibcoMDB clsBlUfisTibocMdb;

	// private XMLConfiguration config;

	private boolean isFile = false;
	// Use the global configuration or per Interface
	private boolean oneMq = true;
	private String filePath = "";

//	private Connection activeMqConn;
//	private Connection tibcoConn;

	private boolean isAmqOn = false;
	private boolean isTibcoOn = false;

	private List<String> qToMqList = null;
	private List<String> qFromMqList = null;

	List<InterfaceConfig> interfaceConfigs = new ArrayList<>();
	private InterfaceConfig infconfig = null;

	@PostConstruct
	public void initCache() {

		// loadConfigFile(configFile);
		interfaceConfigs = _startupInitSingleton.getInterfaceConfigs();
		if (interfaceConfigs.size() > 0) {
			infconfig = interfaceConfigs.get(0);
			InterfaceMqConfig fromMqConfig = infconfig.getFromMq();
			// tibco enabled or not
			if ("TRUE".equalsIgnoreCase(fromMqConfig.getEnabled())) {
				
				String user = fromMqConfig.getUserName();
				
				// Call the common function to decrypt the password
				// Use (16 bytes = 128 bit) secret key.
				
				//String user = HpAESEncryptAndDecrypt.symmetricDecrypt(fromMqConfig.getUserName(), HpEKConstants.UFIS_APP_KEY);
				
				String pwd = HpAESEncryptAndDecrypt.symmetricDecrypt(fromMqConfig.getPassword(), HpEKConstants.UFIS_APP_KEY);
				LOG.debug("Input tibco user name : {} and password : {}", user, pwd);
				
				/*String pwd = fromMqConfig.getPassword();*/
				LOG.debug("Tibco connection url : {}", fromMqConfig.getUrl());
				connectTibco(fromMqConfig.getUrl(), user, pwd);
				
				isTibcoOn = true;
				HpCommonConfig.isTibcoOn = true;
			}

			InterfaceMqConfig toMqConfig = infconfig.getToMq();
			// activemq enabled or not
			if ("TRUE".equalsIgnoreCase(toMqConfig.getEnabled())) {
				connectActiveMQ(toMqConfig.getUrl(), toMqConfig.getUserName(),
						toMqConfig.getPassword());
				isAmqOn = true;
				HpCommonConfig.isAmqOn = true;
			}
		}
	}
	
	/**
	 * - to reconnect TIBCO connection - call from RST msg receiver
	 */
	public void reconnectTibco() {
		int sleep = 200;

		Date before = new Date();
		Date after = before;

		boolean conxUp = false;
		int oneHr = 3600 * 1000;

		while (!conxUp && (after.getTime() - before.getTime()) < oneHr) {
			try {
				//if (tibcoConn != null) {
					//tibcoConn.close();
				if (HpCommonConfig.tibcoConn != null) {
					HpCommonConfig.tibcoConn.close();
					LOG.info("!!! Closed existing TIBCO connection.");
					clsBlUfisTibocMdb.destroy();
				}
				after = new Date();
				Date d1 = new Date();
				LOG.info("Reconnecting started... {} ms", d1.getTime());
				Thread.sleep(sleep);

				interfaceConfigs = _startupInitSingleton.getInterfaceConfigs();
				if (interfaceConfigs.size() == 0) {
					LOG.info("!!! Config for TIBCO is empty. Reconnection will not be performed.");
					return;
				}
				infconfig = interfaceConfigs.get(0);
				InterfaceMqConfig fromMqConfig = infconfig.getFromMq();

				String user = fromMqConfig.getUserName();
				
				// Call the common function to decrypt the password
				// Use (16 bytes = 128 bit) secret key.
				String pwd = HpAESEncryptAndDecrypt.symmetricDecrypt(fromMqConfig.getPassword(), HpEKConstants.UFIS_APP_KEY);
				
				boolean reconnected = connectTibco(fromMqConfig.getUrl(), user, pwd);
				LOG.error(Tibjms.getConnectionActiveURL(HpCommonConfig.tibcoConn));
				
				/*	boolean reconnected = connectTibco(fromMqConfig.getUrl(),
					fromMqConfig.getUserName(), fromMqConfig.getPassword());*/
				setTibcoOn(reconnected);
				LOG.info("Reconnecting TIBCO is {}!!!",
						reconnected ? "successful" : "failed");
				Date d2 = new Date();
				LOG.info("Reconnecting ended... {} ms", d2.getTime());
				LOG.info("Reconnecting TIBCO duration : {} ms", d2.getTime() - d1.getTime());

				if (reconnected) {
					// re-instantiate the MDB
					clsTibcoAdapterStartUp.init();
					conxUp = true;
				}
			} catch (JMSException e) {
				LOG.error("JMSException {}", e.getMessage());
			} catch (Exception ex) {
				LOG.error("Error when reconnecting to Tibco..");
			}
		}
	}

	@PreDestroy
	private void destroy() {
		try {
//			if (tibcoConn != null) {
//				tibcoConn.close();
			if (HpCommonConfig.tibcoConn != null) {
				HpCommonConfig.tibcoConn.close();
				LOG.info("Closed Tibco conx.");
			}
//			if (activeMqConn != null) {
//				activeMqConn.close();
			if (HpCommonConfig.activeMqConn != null) {
				HpCommonConfig.activeMqConn.close();
				LOG.info("Closed AMQ conx.");
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		}
	}

	private boolean connectActiveMQ(String url, String user, String password) {

		try {
			ConnectionFactory activeMqConnFactory = new ActiveMQConnectionFactory(
					user, password, "failover:(" + url + ")");
//			activeMqConn = activeMqConnFactory.createConnection();
//			activeMqConn.start();
			HpCommonConfig.activeMqConn = activeMqConnFactory.createConnection();
			// Adding client id
			HpCommonConfig.activeMqConn.setClientID(HpCommonConfig.dtfl);
			HpCommonConfig.activeMqConn.start();
			LOG.info("!!!!connect to active mq successed");
			System.out.println("!!!!connect to active mq successed");
		} catch (Exception e) {
			LOG.error("!!!!ERROR,connect to active mq error : {}", e);
			return false;
		}

		return true;
	}

	private boolean connectTibco(final String url, final String user,
			final String password) {
		try {
			TibjmsConnectionFactory tibcoConnFactory = new TibjmsConnectionFactory(url);
			/* set heartbeat interval to detect network issues */
			// Tibjms.setPingInterval(10);
			/* fault-tolerant switch */
			// Tibjms.setExceptionOnFTSwitch(true);

//			tibcoConnFactory.setConnAttemptCount(10);
//			tibcoConnFactory.setConnAttemptDelay(1000);
//			tibcoConnFactory.setConnAttemptTimeout(1000);
//			tibcoConnFactory.setReconnAttemptCount(1000000000);
//			tibcoConnFactory.setReconnAttemptDelay(1000);
//			tibcoConnFactory.setReconnAttemptTimeout(1000);
			
			//Tibjms.setExceptionOnFTSwitch(true);
			
			tibcoConnFactory.setConnAttemptCount(10);
			tibcoConnFactory.setConnAttemptDelay(1000);
			tibcoConnFactory.setConnAttemptTimeout(1000);
				   
			tibcoConnFactory.setReconnAttemptCount(10);
			tibcoConnFactory.setReconnAttemptDelay(1000);
		    tibcoConnFactory.setReconnAttemptTimeout(1000);

			/* create the connection */
			//tibcoConn = tibcoConnFactory.createConnection(user, password);
			HpCommonConfig.tibcoConn = tibcoConnFactory.createConnection(user, password);
			// tibcoConn.setExceptionListener(new ExceptionListener(){
			//
			// @Override
			// public void onException(JMSException jmse) {
			// if (tibcoConn != null) {
			// try {
			// tibcoConn.close();
			// } catch (Exception e) {
			// LOG.error("Exception: {}",
			// jmse.getLinkedException().getMessage());
			// }
			// }
			// connectTibco(url, user, password);
			// }
			//
			// });
			//tibcoConn.start();
			HpCommonConfig.tibcoConn.setExceptionListener(new ExceptionListener() {
				@Override
				public void onException(JMSException e) {
					LOG.error("!!!! TIBCO connection failure is received !!!!");
					//System.out.println(e.getMessage());
					reconnectTibco();
					//connectTibco(url, user, password);
				}
			});
			
			
			
			/*		 HpCommonConfig.tibcoConn.setExceptionListener(new ExceptionListener(){
		
			 @Override
			public void onException(JMSException jmse) {
				if (HpCommonConfig.tibcoConn != null) {
					try {
						HpCommonConfig.tibcoConn.close();
					} catch (Exception e) {
						LOG.error("Exception: {}", jmse
								.getLinkedException().getMessage());
					}
				}
				connectTibco(url, user, password);
			}

		});*/
			
			// Adding client id
			//HpCommonConfig.tibcoConn.setClientID(HpCommonConfig.dtfl);
			HpCommonConfig.tibcoConn.start();
			LOG.info("!!!!connect to TIBCO successed");
			System.out.println("!!!!connect to TIBCO successed");
		} catch (Exception e) {
			LOG.error("!!!!ERROR,connect to TIBCO error : {}", e.getMessage());
			return false;
		}

		return true;
	}

	public Connection getActiveMqConnect() {
		//return activeMqConn;
		return HpCommonConfig.activeMqConn;
	}

	public void setTibcoConn(Connection tibcoConn) {
		//this.tibcoConn = tibcoConn;
		HpCommonConfig.tibcoConn = tibcoConn;
	}

	public Connection getTibcoConnect() {
		//return tibcoConn;
		return HpCommonConfig.tibcoConn;
	}

	public InterfaceConfig getInfConfig() {
		return infconfig;
	}

	public List<InterfaceConfig> getInterfaceConfigs() {
		return interfaceConfigs;
	}

	public void setInterfaceConfigs(List<InterfaceConfig> interfaceConfigs) {
		this.interfaceConfigs = interfaceConfigs;
	}

	public List<String> getqToMqList() {
		return qToMqList;
	}

	public void setqToMqList(List<String> qToMqList) {
		this.qToMqList = qToMqList;
	}

	public List<String> getqFromMqList() {
		return qFromMqList;
	}

	public void setqFromMqList(List<String> qFromMqList) {
		this.qFromMqList = qFromMqList;
	}

	public boolean isAmqOn() {
		return isAmqOn;
	}

	public void setAmqOn(boolean isAmqOn) {
		this.isAmqOn = isAmqOn;
	}

	public boolean isTibcoOn() {
		return isTibcoOn;
	}

	public void setTibcoOn(boolean isTibcoOn) {
		this.isTibcoOn = isTibcoOn;
	}

}
