package com.ufis_as.ufisapp.ek.singleton;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.core.flightevent.FlightEvent;
import ek.core.flightevent.FlightEvent.FlightId;
import ek.core.flightevent.FlightEvent.FlightId.FltDate;

/**
 * Core flight cache
 */
//@Singleton(name="FlightEventSingleton")
//@Startup
//@Lock(LockType.WRITE)
public class CoreFlightEventSingleton {
	
	private static final Logger LOG = LoggerFactory.getLogger(CoreFlightEventSingleton.class);
	
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager _em;
	
	private List<FlightEvent> globalFlightEvent = new ArrayList<>();
	private TimeZone utcTz = TimeZone.getTimeZone("UTC");
	private static final int startOffset = -2;
	private static final int endOffset = 1;

    public CoreFlightEventSingleton() {

    }
    
    @PostConstruct
    private void initialCache() {
    	if (globalFlightEvent == null) {
    		globalFlightEvent = new ArrayList<>();
    	}
    	
        HpUfisCalendar fromDate = new HpUfisCalendar(utcTz);
        HpUfisCalendar toDate = new HpUfisCalendar(utcTz);

        fromDate.DateAdd(startOffset, EnumTimeInterval.Days);
        toDate.DateAdd(endOffset, EnumTimeInterval.Days);
    	
		Query query = _em.createQuery(
				"SELECT e " +
				"FROM EntDbAfttab e " +
				"WHERE (e.alc2 = 'EK' OR e.alc3 = 'UAE') " +
				"AND ((e.tifa BETWEEN :startDate AND :endDate ) OR (e.tifd BETWEEN :startDate AND :endDate ))");
		query.setParameter("startDate", fromDate.getCedaString());
		query.setParameter("endDate", toDate.getCedaString());
		try {
			LOG.info("Loading EK flights into cahce from afttab...");
			List<EntDbAfttab> result = query.getResultList();
			if (result != null && result.size() > 0) {
				LOG.info("{} EK flight record found", result.size());
				FlightEvent flight = null;
				FlightId flightId = null;
				FltDate fltDate = null;
				HpUfisCalendar ufisCalendar = new HpUfisCalendar();
				ufisCalendar.setCustomFormat("yyyyMMdd");
				for (EntDbAfttab entity : result) {
					flight = new FlightEvent();
					flightId = new FlightId();
					fltDate = new FltDate();
					// carrier, boardpoint, destination, flight date, suffix
					flightId.setCxCd(entity.getAlc2() == null ? entity.getAlc3() : entity.getAlc2());
					flightId.setDepStn(entity.getOrg3());
					flightId.setArrStn(entity.getDes3());
					if (HpUfisUtils.isNotEmptyStr(entity.getFlda())) {
						ufisCalendar.setTime(entity.getFlda(), ufisCalendar.getCustomFormat());
						fltDate.setValue(HpUfisUtils.chgDateToXMLGregorianCalendar(ufisCalendar.getTime()));
						flightId.setFltDate(fltDate);
					}
					flightId.setFltNum(entity.getFltn());
					if (entity.getFlns() != null 
							&& !"".equals(String.valueOf(entity.getFlns()).trim())) {
						flightId.setFltSuffix(String.valueOf(entity.getFlns()));
					}
					flight.setFlightId(flightId);
					globalFlightEvent.add(flight);
				}
			}
		} catch (Exception e) {
			LOG.error("Cannot Cache EK flights from afttab");
			LOG.error("Exception: {}", e.getMessage());
		}
    }

	public List<FlightEvent> getGlobalFlightEvent() {
		if (globalFlightEvent == null) {
			globalFlightEvent = new ArrayList<>();
		}
		return globalFlightEvent;
	}

	public void setGlobalFlightEvent(List<FlightEvent> globalFlightEvent) {
		this.globalFlightEvent = globalFlightEvent;
	}
	
	public void removeExpiredCache(int exp) {
		// current datetime in UTC
		HpUfisCalendar utcNow = new HpUfisCalendar(utcTz);
		
		// currently only keep past 2 days cache
		if (globalFlightEvent != null) {
			Iterator<FlightEvent> it = globalFlightEvent.iterator();
			while (it.hasNext()) {
				FlightEvent event = it.next();
				if (event.getFlightId() != null 
						&& event.getFlightId().getFltDate() != null) {
					long offset = new HpUfisCalendar(event.getFlightId().getFltDate().getValue()).
							timeDiff(utcNow, EnumTimeInterval.Days, true);
					if (offset > exp) {
						it.remove();
					}
				}
			}
		}
	}
	
}
