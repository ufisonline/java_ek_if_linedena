package com.ufis_as.ufisapp.ek.singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;

import com.ufis_as.configuration.HpBeltConfig;
import com.ufis_as.configuration.HpCommonConfig;
import com.ufis_as.configuration.HpEKBeltConfigConstants;
import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.configuration.HpEgdsConfig;
import com.ufis_as.configuration.HpPaxAlertTimeRangeConfig;
import com.ufis_as.configuration.HpProveoConfig;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldItemType;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldLoc;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldPos;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldScanAct;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldSubType;
import com.ufis_as.ek_if.aacs.uld.entities.EntDbMdUldType;
import com.ufis_as.ek_if.acts_ods.entities.EntDbMdStaffType;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagAction;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagClassify;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagHandleLoc;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagReconLoc;
import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;
import com.ufis_as.ek_if.iams.entities.EntDbMdGateType;
import com.ufis_as.ek_if.lido.entities.EntDbApttab;
import com.ufis_as.ek_if.opera.entities.EntDbMdLoungeCode;
import com.ufis_as.ek_if.proveo.entities.EntDbMdEquip;
import com.ufis_as.ek_if.proveo.entities.EntDbMdEquipLoc;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsDept;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsFltStat;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsMutateCode;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsOrderType;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsResourceType;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsServCode;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsShiftType;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsSubtaskStatus;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsTaskGrptype;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsTaskReqt;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsTaskStatus;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsTaskType;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsWorkArea;
import com.ufis_as.ek_if.rms.entities.EntDbMdRmsWorkLoc;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.InterfaceMqConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbActtab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAlttab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbBlttab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbGattab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbNattab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbPsttab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbSgmtab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * Configuration Load and Basic data Load
 */
// @Singleton
// @Startup
public class EntStartupInitSingleton {

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(EntStartupInitSingleton.class);


	/**
	 * CDI
	 */
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	@Resource(name = HpUfisAppConstants.DEFAULT_JNDICONFIGNAME)
	private String intConfigFile;
	private String extConfigFile = null;
	// private String operaConfigFile = null;

	/**
	 * Constants and Variables
	 */
	private List<EntDbActtab> _cacheAct = new ArrayList<>();
	private List<EntDbAlttab> _cacheAlt = new ArrayList<>();
	private List<EntDbApttab> _cacheApt = new ArrayList<>();
	private List<EntDbNattab> _cacheNat = new ArrayList<>();
	private List<EntDbPsttab> _cachePst = new ArrayList<>();
	private List<EntDbBlttab> _cacheBlt = new ArrayList<>();
	private List<EntDbGattab> _cacheGat = new ArrayList<>();
	private List<EntDbSgmtab> _cacheSgm = new ArrayList<>();

	// Basic data for ulds
	private List<EntDbMdUldItemType> UldItemTypeList = new ArrayList<>();
	private List<EntDbMdUldLoc> UldLocList = new ArrayList<>();
	private List<EntDbMdUldPos> UldPosList = new ArrayList<>();
	private List<EntDbMdUldScanAct> UldScanActList = new ArrayList<>();
	private List<EntDbMdUldSubType> UldSubtypeList = new ArrayList<>();
	private List<EntDbMdUldType> UldTypeList = new ArrayList<>();

	// Basic data for Pax lounge usage
	private List<EntDbMdLoungeCode> LoungeCodeList = new ArrayList<>();
	private List<EntDbMdGateType> GateTypeList = new ArrayList<>();
	private List<EntDbMdStaffType> StaffTypeList = new ArrayList<>();

	// Master Data for BELT
	private List<EntDbMdBagHandleLoc> mdBagHandleLocList = new ArrayList<EntDbMdBagHandleLoc>();
	private List<EntDbMdBagReconLoc> mdBagReconLocList = new ArrayList<EntDbMdBagReconLoc>();
	private List<EntDbMdBagAction> mdBagActionList = new ArrayList<EntDbMdBagAction>();
	private List<EntDbMdBagType> mdBagTypeList = new ArrayList<EntDbMdBagType>();
	private List<EntDbMdBagClassify> mdBagClassifyList = new ArrayList<EntDbMdBagClassify>();
	// Master Data for Proveo
	private List<String> mdEquipLocList = new ArrayList<String>();
	private List<String> mdEquipList = new ArrayList<String>();

	// Master Data for RMS
	private List<EntDbMdRmsDept> mdRmsDeptList = new ArrayList<>();
	private List<EntDbMdRmsFltStat> mdRmsFltStatList = new ArrayList<>();
	private List<EntDbMdRmsMutateCode> mdRmsMutateCodeList = new ArrayList<>();
	private List<EntDbMdRmsOrderType> mdRmsOrderTypeList = new ArrayList<>();
	private List<EntDbMdRmsResourceType> mdRmsRrscTypeList = new ArrayList<>();
	private List<EntDbMdRmsServCode> mdRmsServCodesList = new ArrayList<>();
	private List<EntDbMdRmsTaskReqt> mdRmsTaskReqtsList = new ArrayList<>();
	private List<EntDbMdRmsTaskStatus> mdRmsTaskStatusList = new ArrayList<>();
	private List<EntDbMdRmsTaskType> mdRmsTaskTypesList = new ArrayList<>();
	private List<EntDbMdRmsWorkArea> mdRmsWorkAreasList = new ArrayList<>();
	private List<EntDbMdRmsWorkLoc> mdRmsWorkLocsList = new ArrayList<>();

	// Master Data for EngRTCgf
	private List<EntDbMdRmsShiftType> mdRmsShiftTypeList = new ArrayList<>();
	private List<EntDbMdRmsSubtaskStatus> mdRmsSubtaskStatusList = new ArrayList<>();
	private List<EntDbMdRmsTaskGrptype> mdRmsTaskGrptypeList = new ArrayList<>();

	// Master Data base on staff typefor EK-RTC
	boolean isMDDeptListOn = false;
	private List<String> EKASDeptList = new ArrayList<>();
	private List<String> GXDeptList = new ArrayList<>();
	private List<String> CSDeptList = new ArrayList<>();
	private List<String> ENGRDeptList = new ArrayList<>();

	boolean isMDWorkAreasListOn = false;
	private List<String> EKASWorkAreasList = new ArrayList<>();
	private List<String> GXWorkAreasList = new ArrayList<>();
	private List<String> CSWorkAreasList = new ArrayList<>();
	private List<String> ENGRWorkAreasList = new ArrayList<>();

	boolean isMDTaskStatusListOn = false;
	private List<String> EKASTaskStatusList = new ArrayList<>();
	private List<String> GXTaskStatusList = new ArrayList<>();
	private List<String> CSTaskStatusList = new ArrayList<>();
	// private List<String> ENGRDeptList = new ArrayList();

	boolean isMDTaskTypesListOn = false;
	private List<String> EKASTaskTypesList = new ArrayList<>();
	private List<String> GXTaskTypesList = new ArrayList<>();
	private List<String> CSTaskTypesList = new ArrayList<>();
	// private List<String> ENGRDeptList = new ArrayList();

	boolean isMDTaskGrptypeListOn = false;
	private List<String> EKASTaskGrptypeList = new ArrayList<>();
	private List<String> GXTaskGrptypeList = new ArrayList<>();
	private List<String> CSTaskGrptypeList = new ArrayList<>();
	private List<String> ENGRTaskGrptypeList = new ArrayList<>();

	boolean isMDTaskReqtListOn = false;
	private List<String> EKASTaskReqtList = new ArrayList<>();
	private List<String> GXTaskReqtList = new ArrayList<>();
	private List<String> CSTaskReqtList = new ArrayList<>();
	private List<String> ENGRTaskReqtList = new ArrayList<>();

	// Master Data for MACS-PAX
	boolean isMDBagTagListOn = false;
	private HashMap<String, String> airlineCodeCnvertMap = new HashMap<String, String>();
	private List<String> MdBagTagList = new ArrayList();
	private Map<String,String> queueNameMap = new HashMap<String, String>();

	private List<Object> stateNameMapList = Collections.EMPTY_LIST;
	private List<Object> stateCurrValueList = Collections.EMPTY_LIST;

	private Map<String, String> envMap = new HashMap<>();

	private XMLConfiguration intConfig;
	private XMLConfiguration extConfig;
	private List<InterfaceConfig> interfaceConfigs = new ArrayList<>();
	private List<String> fromQueueList = new ArrayList<>();
	private List<String> toQueueList = new ArrayList<>();

	private List<String> fromTopicList = new ArrayList<>();
	private List<String> toTopicList = new ArrayList<>();
	private String bcFromUfisTopic = null, bcFromUfisQueue = null;
	// private String bcToUfisTopic = null, bcToUfisQueue = null, toCedaQ, errQ;
	private String bcToUfisTopic = null, bcToUfisQueue = null, toCedaQ;
	private String exptDataStore = null;

	private boolean bcOn, appendWeatherFlag = false, appendNotamsFlag,
			keepAllStnFlag;
	private float fuelRate = 15;// set as default value

	// private XMLConfiguration initOperaConfig;

	// irmtab switch with default value: enabled
	private boolean irmOn = true, irmFullLogOn = true, masterDataOn;
	private String irmLogLev = HpUfisAppConstants.IrmtabLogLev.LOG_FULL.name();


	// irmtab switch with default value: enabled
	private boolean ormOn = true;

	// DTFL value to be populated in IRMTAB
	private String dtflStr = "UNKNOWN";

	// Groovy Parser Path
	private String uwsGroovyParserPath;

	// Groovy Parser File Name
	private String uwsGroovyParserFileName;


	// Timer interval config
	int timerInterval;

	private boolean mdApttabOn, mdUldItemTypeOn, mdUldLocOn, mdUldPosOn,
			mdUldScanActOn, mdUldSubTypeOn, mdUldTypeOn, mdLoungeCodeOn,
			mdGateTypeOn, mdStaffTypeOn;

	private boolean mdBagHandleLocOn, mdBagReconLocOn, mdBagActionOn,
			mdBagTypeOn, mdBagClassifyOn, mdEquipLocOn, mdEquipOn;

	private boolean afttabOn = false;


	// Config for ULD summary
	List<String> uldSubTypeCategoryList = new ArrayList<>();
	List<String> uldInfoTypeCategoryList = new ArrayList<>();
	List<String> uldTypeCategoryList = new ArrayList<>();

	HashMap<String, List<String>> uldTypeMap = new HashMap<>();
	
	private Integer rampOffset;



	/**
	 * Default constructor.
	 */
	public EntStartupInitSingleton() {

	}

	@PostConstruct
	private void initial() {

		// Load Environment Variables
		loadEnvConfig();

		// Load Configuration files
		loadFileConfig();

		// Load Configuration from DB
		loadDbConfig();

		// Load Ceda Master data
		loadCedaBasicData();


		// groovy
		// loadScriptPath();
	}

	@PreDestroy
	private void initialClear() {
		LOG.info("Shut down in progress...");
		_cacheAct.clear();
		_cacheAlt.clear();
		_cacheApt.clear();
		_cacheNat.clear();
		_cachePst.clear();
		_cacheBlt.clear();
		_cacheGat.clear();
		_cacheSgm.clear();


		uldTypeMap.clear();
		uldSubTypeCategoryList.clear();
		uldInfoTypeCategoryList.clear();
		uldTypeCategoryList.clear();
		appendWeatherFlag = false;
		appendNotamsFlag = false;
		keepAllStnFlag = false;


		UldItemTypeList.clear();
		UldLocList.clear();
		UldPosList.clear();
		UldScanActList.clear();
		UldSubtypeList.clear();
		UldTypeList.clear();
		LoungeCodeList.clear();
		GateTypeList.clear();
		StaffTypeList.clear();


		bcFromUfisQueue = null;
		bcToUfisQueue = null;


		mdBagHandleLocList.clear();
		mdBagReconLocList.clear();
		mdBagActionList.clear();
		mdBagTypeList.clear();
		mdBagClassifyList.clear();
		mdEquipLocList.clear();
		mdEquipList.clear();
		stateNameMapList.clear();
		stateCurrValueList.clear();

		mdRmsDeptList.clear();
		mdRmsFltStatList.clear();
		mdRmsMutateCodeList.clear();
		mdRmsOrderTypeList.clear();
		mdRmsRrscTypeList.clear();
		mdRmsServCodesList.clear();
		mdRmsTaskReqtsList.clear();
		mdRmsTaskStatusList.clear();
		mdRmsTaskTypesList.clear();
		mdRmsWorkAreasList.clear();
		mdRmsWorkLocsList.clear();


		// envMap.clear();
		LOG.info("Basic data and configuration has been cleared");		


		//LOG.info("Start to destroy the LogBack MBean Context...");
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
	    lc.stop();
	    LOG.info("Destroyed the LogBack MBean Context...");

	}

	private void loadEnvConfig() {
		envMap = System.getenv();
		// if needed, retrieve db config(url, port, sid and credential)

		// if needed, retrieve log file home path

		// if needed, retrieve log file level

	}

	private void loadFileConfig() {
		try {
			// internal config file path injection if fail, read from default
			if (HpUfisUtils.isNullOrEmptyStr(intConfigFile)) {
				intConfigFile = HpUfisAppConstants.DEFAULT_CONFIGNAME;
				LOG.info("Loading internal configuration by default: {}",
						intConfigFile);
			}

			// Loading common shared configuration file
			if (HpUfisUtils.isNotEmptyStr(intConfigFile)) {
				intConfig = new XMLConfiguration(intConfigFile);
				LOG.info("Loading internal configuration file: {}",
						intConfig.getBasePath());

				/*
				 * TODO Internal configurations which is only can be changed by
				 * Developers e.g. Calculation logical, Records filter
				 * criterias, external config path, HOPO(may put in constant),
				 * verison
				 */

				// read external configuration file path
				// extConfigFile = intConfig.getString("File.ExtConfigPath");
				extConfigFile = intConfig.getString("File.ExtProcConfigFile");
				LOG.info("External CFG: {}", extConfigFile);

				// Loading DTFL value
				dtflStr = intConfig.getString("File.DTFL");
				HpCommonConfig.dtfl = dtflStr;
				if (HpUfisUtils.isNullOrEmptyStr(dtflStr)) {
					dtflStr = "UNKNOWN";
					LOG.info("DTFL value is not specified. Populating the Default DTFL value 'UNKNOWN'");
				} else {
					LOG.info("DTFL value: {}", dtflStr);
				}
			}

			// Loading internal configuration file
			if (HpUfisUtils.isNotEmptyStr(extConfigFile)) {
				extConfig = new XMLConfiguration(extConfigFile);
				LOG.info("Loading external configuration file: {}",
						extConfig.getBasePath());

				/*
				 * TODO External configurations which is process based and can
				 * be changed if needed All these configs should have default
				 * value, if config not found e.g. Function switch on/off,
				 * Features, ESB, Activemq Webserivce Timer
				 */

				// Switch for AFTTAB function (default is enabled)
				String afttab = extConfig
						.getString(HpUfisAppConstants.CON_AFTTAB);

				LOG.info("External CFG: AFTTAB={}", afttab);
				if (HpUfisUtils.isNotEmptyStr(afttab)) {
					if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(afttab)) {
						afttabOn = true;
					}
				}

				// Switch for irmtab function (default is enabled)
				String irmtab = extConfig
						.getString(HpUfisAppConstants.CON_IRMTAB);

				LOG.info("External CFG: IRMTAB={}", irmtab);
				if (HpUfisUtils.isNotEmptyStr(irmtab)) {
					if (HpUfisAppConstants.CON_FALSE.equalsIgnoreCase(irmtab)) {
						irmOn = false;
					}
				}


				// config for IRMTAB function(default is LOG_FULL)
				String irmtabLogLev = extConfig
						.getString(HpUfisAppConstants.CON_IRMTAB);

				LOG.info("External CFG: IRMTAB={}", irmtabLogLev);
				if (!HpUfisUtils.isNullOrEmptyStr(irmtabLogLev)) {
					if (containsObj(HpUfisAppConstants.IrmtabLogLev.class,
							irmtabLogLev)) {
						// if
						// (irmtabLogLev.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_ERR.toString())
						// ||
						// irmtabLogLev.equalsIgnoreCase(HpUfisAppConstants.IrmtabLogLev.LOG_OFF.toString()))
						// {
						irmLogLev = irmtabLogLev;
						HpCommonConfig.irmtab = irmLogLev;
					} else {
						LOG.info("IRMTAB Log Level Default To: {}", irmLogLev);
					}

				}


				// Switch for ormtab function (default is enabled)
				String ormtab = extConfig
						.getString(HpUfisAppConstants.CON_ORMTAB);

				LOG.info("External CFG: ORMTAB={}", ormtab);
				if (HpUfisUtils.isNotEmptyStr(ormtab)) {
					if (HpUfisAppConstants.CON_FALSE.equalsIgnoreCase(ormtab)) {
						ormOn = false;
					}
				}

				// Timer interval config
				String intervalConf = extConfig.getString("TIMER_SECS");
				LOG.info("External CFG: TIMER_SECS={}", intervalConf);
				if (HpUfisUtils.isNotEmptyStr(intervalConf))
					timerInterval = Integer.parseInt(intervalConf);




				/*
				 * String irmtabFullLog =
				 * extConfig.getString(HpUfisAppConstants.CON_IRMTAB_FULL_LOG);
				 * LOG.info("External CFG: IRMTAB_FULL_LOG={}", irmtabFullLog);
				 * 
				 * if (HpUfisUtils.isNotEmptyStr(irmtabFullLog)) { if
				 * (HpUfisAppConstants
				 * .CON_FALSE.equalsIgnoreCase(irmtabFullLog)) { irmFullLogOn =
				 * false; } }
				 */



				String bcUfis_enabled = extConfig.getString("MQ.ENABLED");
				LOG.info("External CFG: Config enabled={}", bcUfis_enabled);
				if (HpUfisUtils.isNotEmptyStr(bcUfis_enabled)) {
					if (HpUfisAppConstants.CON_TRUE
							.equalsIgnoreCase(bcUfis_enabled)) {

						bcOn = true;
					}
				}

				if (bcOn) {
					String bcTopic = extConfig.getString("MQ.FROM.AMQTOPIC");
					LOG.info("External CFG: FROM AMQ Topic = <{}>", bcTopic);
					if (!HpUfisUtils.isNullOrEmptyStr(bcTopic))
						bcFromUfisTopic = bcTopic;


					String bcQueue = extConfig.getString("MQ.FROM.AMQQUEUE");
					LOG.info("External CFG: FROM AMQ Queue = <{}>", bcQueue);
					if (!HpUfisUtils.isNullOrEmptyStr(bcQueue))
						bcFromUfisQueue = bcQueue;


					bcTopic = extConfig.getString("MQ.TO.AMQTOPIC");
					LOG.info("External CFG: TO AMQ Topic = <{}>", bcTopic);
					if (!HpUfisUtils.isNullOrEmptyStr(bcTopic))
						bcToUfisTopic = bcTopic;


					bcQueue = extConfig.getString("MQ.TO.AMQQUEUE");
					LOG.info("External CFG: TO AMQ Queue = <{}>", bcQueue);
					if (!HpUfisUtils.isNullOrEmptyStr(bcQueue))
						bcToUfisQueue = bcQueue;


					exptDataStore = extConfig.getString("MQ.QDATASTORE");
					LOG.info("External CFG: AMQ ERROR/WARN INFO Queue = <{}>",
							exptDataStore);
				}


				// notification config

				HpCommonConfig.notifyQueue = extConfig.getString("NOTIFY.NOTIFY_QUEUE");
				LOG.info("External CFG: Notification Queue = {}", HpCommonConfig.notifyQueue);
				HpCommonConfig.notifyTopic = extConfig.getString("NOTIFY.NOTIFY_TOPIC");
				LOG.info("External CFG: Notification Topic = {}", HpCommonConfig.notifyTopic);
				String timeout = extConfig.getString("NOTIFY.TOPIC_MSG_TIMEOUT");
				if (HpUfisUtils.isNotEmptyStr(timeout) && timeout.matches("\\d*")) {
					HpCommonConfig.topicMsgTimeout = Long.parseLong(timeout);
				}
				LOG.info("External CFG: Notification Topic msg timeout = {}",
						HpCommonConfig.topicMsgTimeout);

				int tables = extConfig.getList("NOTIFY.TABLES.TABLE.NAME")
						.size();
				if (tables > 0) {
					for (int i = 0; i < tables; i++) {
						String name = extConfig
								.getString("NOTIFY.TABLES.TABLE(" + i
										+ ").NAME");
						LOG.info("Notification table: {}", name);
						if (HpUfisUtils.isNotEmptyStr(name)) {
							HpCommonConfig.tables.add(name.toUpperCase());


							String flds = extConfig
									.getString("NOTIFY.TABLES.TABLE(" + i
											+ ").FLDS");
							HpCommonConfig.fields.put(name,
									(flds == null ? flds : flds.toUpperCase()));
							LOG.info("{} notification fields: {}", name, flds);


							String excludes = extConfig
									.getString("NOTIFY.TABLES.TABLE(" + i
											+ ").EXCLUDE");
							HpCommonConfig.exclusions.put(
									name,
									(excludes == null ? excludes : excludes
											.toUpperCase()));
							LOG.info("{} notification exclude: {}", name,
									excludes);
						}
					}
				}


				toCedaQ = extConfig.getString("MQ.TO.AMQQUEUE");
				LOG.info("External CFG: TO CEDA AMQ Queue = <{}>", toCedaQ);
//				if (HpUfisUtils.isNullOrEmptyStr(toCedaQ))
//					toCedaQ = null;

				/*

				 * errQ = extConfig.getString("ERRORQUEUE");
				 * LOG.info("External CFG: ERRORQUEUE = <{}>", errQ);
				 * if(HpUfisUtils.isNullOrEmptyStr(errQ)) errQ = null;
				 */



				String fRate = extConfig.getString("FPL.FUEL_RATE");
				if (!HpUfisUtils.isNullOrEmptyStr(fRate)) {
					LOG.info("External CFG: Fuel Rate = <{}>", fRate);
					fuelRate = new Float(fRate);
				}

				String appendRec = extConfig.getString("METAR.APPEND_RECORD");
				if (!HpUfisUtils.isNullOrEmptyStr(appendRec)) {
					LOG.info("External CFG: METAR APPEND_RECORD = <{}>",
							appendRec);
					if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(appendRec)) {
						appendWeatherFlag = true;
					}
				}

				String keepAllStn = extConfig
						.getString("METAR.KEEP_ALL_STATIONS");

				if (!HpUfisUtils.isNullOrEmptyStr(keepAllStn)) {
					LOG.info("External CFG: KEEP_ALL_STATIONS = <{}>",
							keepAllStn);
					if (HpUfisAppConstants.CON_TRUE
							.equalsIgnoreCase(keepAllStn)) {

						keepAllStnFlag = true;
					}
				}

				String appendRecNotams = extConfig
						.getString("NOTAMS.APPEND_RECORD");

				if (!HpUfisUtils.isNullOrEmptyStr(appendRecNotams)) {
					LOG.info("External CFG: NOTAMS APPEND_RECORD = <{}>",
							appendRecNotams);
					if (HpUfisAppConstants.CON_TRUE
							.equalsIgnoreCase(appendRecNotams)) {

						appendNotamsFlag = true;
					}
				}


				// Switch for master data function (default is disabled)
				String master = extConfig.getString("MASTERDATA.ENABLED");
				LOG.info("External CFG: MasterData enabled={}", master);
				if (!HpUfisUtils.isNullOrEmptyStr(master)) {
					if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(master)) {
						masterDataOn = true;
						loadMasterDataTable();
						loadMasterDataForMACSPAX();
						// load EK-RTC master data
						 loadMasterDataForEKRTC();
						// loadMasterDataForENGRTC();
					}
				}

				String equip = extConfig.getString("MASTERDATA.TABLE.MD_EQUIP");
				if (!HpUfisUtils.isNullOrEmptyStr(equip)) {
					LOG.info("External CFG: MasterData MD_EQUIP enabled={}",
							equip);
					if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(equip)) {
						mdEquipOn = true;
					}
				}
				String equipLocs = extConfig
						.getString("MASTERDATA.TABLE.MD_EQUIP_LOC");
				if (!HpUfisUtils.isNullOrEmptyStr(equipLocs)) {
					LOG.info(
							"External CFG: MasterData MD_EQUIP_LOC enabled={}",
							equipLocs);
					if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(equipLocs)) {
						mdEquipLocOn = true;
					}
				}

				// filter criterias:
				// egds search criteria datetime range from offset and to offset
				int egdsRangeList = extConfig.getList("FILTERS.FILTER.NAME")
						.size();
				if (egdsRangeList > 0) {
					for (int i = 0; i < egdsRangeList; i++) {
						String name = extConfig.getString("FILTERS.FILTER(" + i
								+ ").NAME");
						if ("EGDS_MVT_RANGE".equalsIgnoreCase(name)) {
							String egdsMvtRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String egdsMvtRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info("External CFG: EGDS_MVT_OFFSET from={}",
									egdsMvtRangeFrom);
							LOG.info("External CFG: EGDS_MVT_OFFSET to={}",
									egdsMvtRangeTo);
							if (HpUfisUtils.isNotEmptyStr(egdsMvtRangeFrom)) {
								HpEgdsConfig.setEgdsMvtFromOffset(Integer
										.parseInt(egdsMvtRangeFrom));

							}
							if (HpUfisUtils.isNotEmptyStr(egdsMvtRangeTo)) {
								HpEgdsConfig.setEgdsMvtToOffset(Integer
										.parseInt(egdsMvtRangeTo));

							}
						} else if ("EGDS_DEFAULT_RANGE".equalsIgnoreCase(name)) {
							String egdsDefRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String egdsDefRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info("External CFG: EGDS_DEF_OFFSET from={}",
									egdsDefRangeFrom);
							LOG.info("External CFG: EGDS_DEF_OFFSET to={}",
									egdsDefRangeTo);
							if (HpUfisUtils.isNotEmptyStr(egdsDefRangeFrom)) {
								HpEgdsConfig.setEgdsDefFromOffset(Integer
										.parseInt(egdsDefRangeFrom));

							}
							if (HpUfisUtils.isNotEmptyStr(egdsDefRangeTo)) {
								HpEgdsConfig.setEgdsDefToOffset(Integer
										.parseInt(egdsDefRangeTo));

							}
						} else if ("PAX_ALERT_INIT_AFTTAB_RANGE".equals(name)) {
							String paxAlertRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String paxAlertRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info("External CFG: PAX_ALERT from = {}",
									paxAlertRangeFrom);
							LOG.info("External CFG: PAX_ALERT to = {}",
									paxAlertRangeTo);
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeFrom)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertInitFrom(Integer
												.parseInt(paxAlertRangeFrom));

							}
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeTo)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertInitTo(Integer
												.parseInt(paxAlertRangeTo));

							}
						} else if ("PAX_ALERT_RANGE".equals(name)) {
							String paxAlertRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String paxAlertRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info("External CFG: PAX_ALERT from = {}",
									paxAlertRangeFrom);
							LOG.info("External CFG: PAX_ALERT to = {}",
									paxAlertRangeTo);
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeFrom)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertFromOffset(Integer
												.parseInt(paxAlertRangeFrom));

							}
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeTo)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertToOffset(Integer
												.parseInt(paxAlertRangeTo));

							}
						} else if ("PAX_ALERT_AFTTAB_RANGE".equals(name)) {
							String paxAlertRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String paxAlertRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info(
									"External CFG: PAX_ALERT AFTTAB from = {}",
									paxAlertRangeFrom);
							LOG.info("External CFG: PAX_ALERT AFTTAB to = {}",
									paxAlertRangeTo);
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeFrom)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertFlightFrom(Integer
												.parseInt(paxAlertRangeFrom));

							}
							if (HpUfisUtils.isNotEmptyStr(paxAlertRangeTo)) {
								HpPaxAlertTimeRangeConfig
										.setPaxAlertFlightTo(Integer
												.parseInt(paxAlertRangeTo));

							}
						} else if ("PROVEO_AFTTAB_RANGE".equalsIgnoreCase(name)) {
							String afttabRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String afttabRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");


							LOG.info(
									"External CFG: PROVEO_AFTTAB_RANGE from = {}",
									afttabRangeFrom);
							LOG.info(
									"External CFG: PROVEO_AFTTAB_RANGE to = {}",
									afttabRangeTo);
							if (HpUfisUtils.isNotEmptyStr(afttabRangeFrom)) {
								// HpProveoConfig.setAfttabRangeFromOffset(Integer.parseInt(afttabRangeFrom));
								HpProveoConfig.setAfttabRangeFromOffset(Short
										.parseShort(afttabRangeFrom));
							}
							if (HpUfisUtils.isNotEmptyStr(afttabRangeTo)) {
								// HpProveoConfig.setAfttabRangeToOffset(Integer.parseInt(afttabRangeTo));
								HpProveoConfig.setAfttabRangeToOffset(Short
										.parseShort(afttabRangeTo));
							}
						}
						else if("LCM_AFTTAB_RANGE".equalsIgnoreCase(name))
						{
							String afttabRangeFrom = extConfig
									.getString("FILTERS.FILTER(" + i + ").FROM");
							String afttabRangeTo = extConfig
									.getString("FILTERS.FILTER(" + i + ").TO");
							LOG.info(
									"External CFG: LCM_AFTTAB_RANGE from = {}",
									afttabRangeFrom);
							LOG.info(
									"External CFG: LCM_AFTTAB_RANGE to = {}",
									afttabRangeTo);
							if (HpUfisUtils.isNotEmptyStr(afttabRangeFrom)) {
							HpCommonConfig.fromOffset=Integer.parseInt(afttabRangeFrom);
							}
							if (HpUfisUtils.isNotEmptyStr(afttabRangeTo)) {
							HpCommonConfig.toOffset=Integer.parseInt(afttabRangeTo);
							}
						}
					}
				}

				// interfaces Logic(BELT and PROVEO)
				int interfaceLogicalsList = extConfig.getList(
						"LOGICALS.LOGICAL.NAME").size();
				LOG.info("interfaceLogicalsList.size:{}", interfaceLogicalsList);
				if (interfaceLogicalsList > 0) {
					for (int i = 0; i < interfaceLogicalsList; i++) {
						String name = extConfig.getString("LOGICALS.LOGICAL("
								+ i + ").NAME");
						LOG.info("name:{}", name);
						if (HpEKBeltConfigConstants.LOGICAL_NAME
								.equalsIgnoreCase(name)) {

							List<Object> afttabIstaExlList = extConfig
									.getList("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.AFTTAB_ISTA_EXCLUDE_TAG);
							String getLinkTables = extConfig
									.getString("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.GET_LINK_TO_TABLES_TAG);
							String keepLatestMoves = extConfig
									.getString("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.KEEP_LATEST_MOVEMENT_TAG);
							List<Object> firstClassBagList = extConfig
									.getList("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.FIRST_CLASS_BAG_LIST);
							List<Object> bussinessClassBagList = extConfig
									.getList("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.BUSINESS_CLASS_BAG_LIST);
							List<Object> economyClassBagList = extConfig
									.getList("LOGICALS.LOGICAL("
											+ i
											+ ")."
											+ HpEKBeltConfigConstants.ECONOMY_CLASS_BAG_LIST);
							LOG.info(
									"External CFG: AFTTAB_ISTA_EXCLUDE_LIST size={}",
									afttabIstaExlList.size());
							LOG.info("External CFG: GET_LINK_TO_TABLES={}",
									getLinkTables);
							LOG.info("External CFG: KEEP_LATEST_MOVEMENT={}",
									keepLatestMoves);
							LOG.info(
									"External CFG: FIRST_CLASS_BAG_LIST size={}",
									firstClassBagList.size());
							LOG.info(
									"External CFG: BUSINESS_CLASS_BAG_LIST size={}",
									bussinessClassBagList.size());
							LOG.info(
									"External CFG: ECONOMY_CLASS_BAG_LIST size={}",
									economyClassBagList.size());
							if (!afttabIstaExlList.isEmpty()) {

								HpBeltConfig
										.setAfttabIstaExlList(afttabIstaExlList);

							}
							if (HpUfisUtils.isNotEmptyStr(getLinkTables)) {
								HpBeltConfig.setGetLinkTables(getLinkTables);
							}
							if (HpUfisUtils.isNotEmptyStr(keepLatestMoves)) {
								HpBeltConfig
										.setKeepLatestMoves(keepLatestMoves);

							}
							if (!firstClassBagList.isEmpty()) {

								HpBeltConfig
										.setFirstClassBagList(firstClassBagList);

							}
							if (!bussinessClassBagList.isEmpty()) {

								HpBeltConfig
										.setBusinessClassBagList(bussinessClassBagList);

							}
							if (!economyClassBagList.isEmpty()) {

								HpBeltConfig
										.setEconomyClassBagList(economyClassBagList);

							}

						}
						if (HpEKConstants.PROVEO_SOURCE.equalsIgnoreCase(name)) {
							stateNameMapList = extConfig
									.getList("LOGICALS.LOGICAL(" + i + ")."
											+ "STATENAME");
							stateCurrValueList = extConfig
									.getList("LOGICALS.LOGICAL(" + i + ")."
											+ "CURRVALUE");
							LOG.info("External CFG: State Name List size={}",
									stateNameMapList.size());
							LOG.info("External CFG: Curr Value List size={}",
									stateCurrValueList.size());
						}
					}
				}

				// interfaces config (ESB, MQ)
				int infList = extConfig.getList("Interfaces.Interface.name")
						.size();
				if (infList > 0) {
					for (int i = 0; i < infList; i++) {
						InterfaceConfig interfaceConfig = new InterfaceConfig();
						InterfaceMqConfig fromMq = new InterfaceMqConfig();
						InterfaceMqConfig toMq = new InterfaceMqConfig();
						HierarchicalConfiguration fromMqConfig = extConfig
								.configurationAt("Interfaces.Interface(" + i
										+ ").FromMq");
						HierarchicalConfiguration toMqConfig = extConfig
								.configurationAt("Interfaces.Interface(" + i
										+ ").ToMq");
						String name = extConfig
								.getString("Interfaces.Interface(" + i
										+ ").name");
						String xsd = extConfig
								.getString("Interfaces.Interface(" + i
										+ ").xsd");
						String className = extConfig
								.getString("Interfaces.Interface(" + i
										+ ").classname");
						String regExp = extConfig
								.getString("Interfaces.Interface(" + i
										+ ").regexp");

						interfaceConfig.setName(name);
						interfaceConfig.setXsd(xsd);
						interfaceConfig.setClassName(className);
						interfaceConfig.setRegexp(regExp);

						LOG.debug("FROM URL : {}", fromMqConfig.getString("url"));
						fromMq.setUrl(fromMqConfig.getString("url"));
						fromMq.setUserName(fromMqConfig.getString("username"));
						fromMq.setPassword(fromMqConfig.getString("password"));
						fromMq.setQueueName(fromMqConfig.getString("queue"));
						fromMq.setEnabled(fromMqConfig.getString("enabled"));
						interfaceConfig.setFromMq(fromMq);

						List<Object> from = fromMqConfig.getList("queue");
						if (from != null && from.size() > 0) {
							for (int j = 0; j < from.size(); j++) {
								fromQueueList.add(from.get(j) == null ? ""
										: from.get(j).toString());
								LOG.debug("Loaded Q from config : <{}>", from
										.get(j).toString());

							}
						}
						List<Object> fromTopics = toMqConfig.getList("topic");
						if (fromTopics != null && fromTopics.size() > 0) {
							for (int j = 0; j < fromTopics.size(); j++) {
								fromTopicList
										.add(fromTopics.get(j) == null ? ""
												: fromTopics.get(j).toString());
							}
						}

						LOG.debug("URL : {}", toMqConfig.getString("url"));
						toMq.setUrl(toMqConfig.getString("url"));
						toMq.setUserName(toMqConfig.getString("username"));
						toMq.setPassword(toMqConfig.getString("password"));
						toMq.setEnabled(toMqConfig.getString("enabled"));
						interfaceConfig.setToMq(toMq);

						List<Object> to = toMqConfig.getList("queue");
						if (to != null && to.size() > 0) {
							for (int j = 0; j < to.size(); j++) {
								toQueueList.add(to.get(j) == null ? "" : to
										.get(j).toString());

							}
						}
						List<Object> toTopics = toMqConfig.getList("topic");
						if (toTopics != null && toTopics.size() > 0) {
							for (int j = 0; j < toTopics.size(); j++) {
								toTopicList.add(toTopics.get(j) == null ? ""
										: toTopics.get(j).toString());
							}
						}
						interfaceConfigs.add(interfaceConfig);
					}
				}

				uwsGroovyParserPath = extConfig
						.getString("UWSGroovyParser.Path");

				LOG.debug("Groovy Parser Path {}", uwsGroovyParserPath);

				uwsGroovyParserFileName = extConfig
						.getString("UWSGroovyParser.Name");

				LOG.debug("Groovy Parser File Name {}", uwsGroovyParserFileName);
				
				String rampOffsetString = extConfig.getString("RAMPOFFSET");
				if(HpUfisUtils.isNullOrEmptyStr(rampOffsetString)) {
					LOG.warn("Ramp Offset for Skychain is not set up.");
				} else {
					rampOffset =  Integer.parseInt(rampOffsetString);
					LOG.debug("Ramp offset value - {}", rampOffset);
				}
				
				
				// config for MACS-PAX 2013-11-26
				int paxQueueNameList = extConfig.getList("Interfaces.Interface.name")
						.size();
				if ( paxQueueNameList > 0){
					for (int j=0; j < paxQueueNameList; j++){
						String paxValue = extConfig.getString("Interfaces.Interface"+"("+j+")"+".name");
						String paxKey = extConfig.getString("Interfaces.Interface"+"("+j+")"+".FromMq.queue");
						queueNameMap.put("Queue["+paxKey+"]", paxValue);
						LOG.debug("set queueNamemap for MACS-PAX, put key {}, value {}", "Queue["+paxKey+"]", paxValue);
					}
				}

			}
			LOG.info("Loading configuration from file successfully.");
		} catch (ConfigurationException cex) {
			LOG.error("Error Loading configuration file {}", cex);
			LOG.error("Application is now Stopped");
		}



	}

	private void loadMasterDataTable() {

		String apttab = extConfig.getString("MASTERDATA.TABLE.APTTAB");
		if (!HpUfisUtils.isNullOrEmptyStr(apttab)) {
			LOG.info("External CFG: MasterData APTTAB enabled={}", apttab);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(apttab)) {
				mdApttabOn = true;
			}
		}
		String uldItemType = extConfig
				.getString("MASTERDATA.TABLE.MD_ULD_ITEMTYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(uldItemType)) {
			LOG.info("External CFG: MasterData MD_ULD_ITEMTYPE enabled={}",
					uldItemType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldItemType)) {
				mdUldItemTypeOn = true;
			}
		}
		String uldLoc = extConfig.getString("MASTERDATA.TABLE.MD_ULD_LOC");
		if (!HpUfisUtils.isNullOrEmptyStr(uldLoc)) {
			LOG.info("External CFG: MasterData MD_ULD_LOC enabled={}", uldLoc);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldLoc)) {
				mdUldLocOn = true;
			}
		}
		String uldPos = extConfig.getString("MASTERDATA.TABLE.MD_ULD_POS");
		if (!HpUfisUtils.isNullOrEmptyStr(uldPos)) {
			LOG.info("External CFG: MasterData MD_ULD_POS enabled={}", uldPos);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldPos)) {
				mdUldPosOn = true;
			}
		}
		String uldScanAct = extConfig
				.getString("MASTERDATA.TABLE.MD_ULD_SCAN_ACT");
		if (!HpUfisUtils.isNullOrEmptyStr(uldScanAct)) {
			LOG.info("External CFG: MasterData MD_ULD_SCAN_ACT enabled={}",
					uldScanAct);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldScanAct)) {
				mdUldScanActOn = true;
			}
		}
		String uldSubType = extConfig
				.getString("MASTERDATA.TABLE.MD_ULD_SUBTYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(uldSubType)) {
			LOG.info("External CFG: MasterData MD_ULD_SUBTYPE enabled={}",
					uldSubType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldSubType)) {
				mdUldSubTypeOn = true;
			}
		}
		String uldType = extConfig.getString("MASTERDATA.TABLE.MD_ULD_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(uldType)) {
			LOG.info("External CFG: MasterData MD_ULD_TYPE enabled={}", uldType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(uldType)) {
				mdUldTypeOn = true;
			}
		}
		String loungeCode = extConfig
				.getString("MASTERDATA.TABLE.MD_LOUNGE_CODE");
		if (!HpUfisUtils.isNullOrEmptyStr(loungeCode)) {
			LOG.info("External CFG: MasterData MD_LOUNGE_CODE enabled={}",
					loungeCode);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(loungeCode)) {
				mdLoungeCodeOn = true;
			}
		}
		String gateType = extConfig.getString("MASTERDATA.TABLE.MD_GATE_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(gateType)) {
			LOG.info("External CFG: MasterData MD_GATE_TYPE enabled={}",
					gateType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(gateType)) {
				mdGateTypeOn = true;
			}
		}
		String staffType = extConfig
				.getString("MASTERDATA.TABLE.MD_STAFF_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(staffType)) {
			LOG.info("External CFG: MasterData MD_STAFF_TYPE enabled={}",
					staffType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(staffType)) {
				mdStaffTypeOn = true;
			}
		}

		String bagHandleLoc = extConfig
				.getString("MASTERDATA.TABLE.MD_BAG_HANDLE_LOC");
		if (!HpUfisUtils.isNullOrEmptyStr(bagHandleLoc)) {
			LOG.info("External CFG: MasterData BAG_HANDLE_LOC enabled={}",
					bagHandleLoc);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagHandleLoc)) {
				mdBagHandleLocOn = true;
			}
		}
		String bagReconLoc = extConfig
				.getString("MASTERDATA.TABLE.MD_BAG_RECON_LOC");
		if (!HpUfisUtils.isNullOrEmptyStr(bagReconLoc)) {
			LOG.info("External CFG: MasterData BAG_RECON_LOC enabled={}",
					bagReconLoc);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagReconLoc)) {
				mdBagReconLocOn = true;
			}
		}
		String bagAction = extConfig
				.getString("MASTERDATA.TABLE.MD_BAG_ACTION");
		if (!HpUfisUtils.isNullOrEmptyStr(bagAction)) {
			LOG.info("External CFG: MasterData BAG_ACTION enabled={}",
					bagAction);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagAction)) {
				mdBagActionOn = true;
			}
		}
		String bagClassify = extConfig
				.getString("MASTERDATA.TABLE.MD_BAG_CLASSIFY");
		if (!HpUfisUtils.isNullOrEmptyStr(bagClassify)) {
			LOG.info("External CFG: MasterData BAG_CLASSIFY enabled={}",
					bagClassify);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagClassify)) {
				mdBagClassifyOn = true;
			}
		}
		String bagTypes = extConfig.getString("MASTERDATA.TABLE.MD_BAG_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(bagTypes)) {
			LOG.info("External CFG: MasterData BAG_TYPE enabled={}", bagTypes);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagTypes)) {
				mdBagTypeOn = true;
			}
		}

		LoadMDRMS();
		

	}


	/**
	 * Loading Master Data for RMS
	 */
	private void LoadMDRMS() {
		if (!masterDataOn)
			return;// do nothing..


		String workArea = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_WORK_AREA");
		if (!HpUfisUtils.isNullOrEmptyStr(workArea)) {
			LOG.info("External CFG: MasterData MD_RMS_WORK_AREA enabled={}",
					workArea);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(workArea)) {
				List<Object> workAreaList = findAll(EntDbMdRmsWorkArea.class);
				for (Iterator<Object> it = workAreaList.iterator(); it
						.hasNext();) {

					EntDbMdRmsWorkArea record = (EntDbMdRmsWorkArea) it.next();
					mdRmsWorkAreasList.add(record);
				}
				LOG.info("Basic data RMS Work Area of Records = {}",
						mdRmsWorkAreasList.size());
			}
		}


		String workLoc = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_WORK_LOC");
		if (!HpUfisUtils.isNullOrEmptyStr(workLoc)) {
			LOG.info("External CFG: MasterData MD_RMS_WORK_LOC enabled={}",
					workLoc);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(workLoc)) {
				List<Object> workLocList = findAll(EntDbMdRmsWorkLoc.class);
				for (Iterator<Object> it = workLocList.iterator(); it.hasNext();) {
					EntDbMdRmsWorkLoc record = (EntDbMdRmsWorkLoc) it.next();
					mdRmsWorkLocsList.add(record);
				}
				LOG.info("Basic data RMS Work Location of Records = {}",
						mdRmsWorkLocsList.size());
			}
		}


		String taskStat = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_STAT");
		if (!HpUfisUtils.isNullOrEmptyStr(taskStat)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_STAT enabled={}",
					taskStat);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskStat)) {
				List<Object> taskStatList = findAll(EntDbMdRmsTaskStatus.class);
				for (Iterator<Object> it = taskStatList.iterator(); it
						.hasNext();) {
					EntDbMdRmsTaskStatus record = (EntDbMdRmsTaskStatus) it
							.next();


					mdRmsTaskStatusList.add(record);
				}
				LOG.info("Basic data RMS Task Stat of Records = {}",
						mdRmsTaskStatusList.size());
			}
		}


		String orderType = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_ORDER_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(orderType)) {
			LOG.info("External CFG: MasterData MD_RMS_ORDER_TYPE enabled={}",
					orderType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(orderType)) {
				List<Object> orderTypeList = findAll(EntDbMdRmsOrderType.class);
				for (Iterator<Object> it = orderTypeList.iterator(); it
						.hasNext();) {
					EntDbMdRmsOrderType record = (EntDbMdRmsOrderType) it
							.next();


					mdRmsOrderTypeList.add(record);
				}
				LOG.info("Basic data RMS Order Type of Records = {}",
						mdRmsOrderTypeList.size());
			}
		}

		String resourceType = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_RESOURCE_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(resourceType)) {
			LOG.info(
					"External CFG: MasterData MD_RMS_RESOURCE_TYPE enabled={}",
					resourceType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(resourceType)) {
				List<Object> list = findAll(EntDbMdRmsResourceType.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsResourceType record = (EntDbMdRmsResourceType) it
							.next();

					mdRmsRrscTypeList.add(record);
				}
				LOG.info("Basic data RMS Resource Type of Records = {}",
						mdRmsRrscTypeList.size());
			}
		}

		String fltStat = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_FLT_STAT");
		if (!HpUfisUtils.isNullOrEmptyStr(fltStat)) {
			LOG.info("External CFG: MasterData MD_RMS_FLT_STAT enabled={}",
					fltStat);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(fltStat)) {
				List<Object> list = findAll(EntDbMdRmsFltStat.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsFltStat record = (EntDbMdRmsFltStat) it.next();
					mdRmsFltStatList.add(record);
				}
				LOG.info("Basic data RMS Flight Status of Records = {}",
						mdRmsFltStatList.size());
			}
		}

		String taskReq = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_REQT");
		if (!HpUfisUtils.isNullOrEmptyStr(taskReq)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_REQT enabled={}",
					taskReq);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskReq)) {
				List<Object> list = findAll(EntDbMdRmsTaskReqt.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsTaskReqt record = (EntDbMdRmsTaskReqt) it.next();
					mdRmsTaskReqtsList.add(record);
				}
				LOG.info("Basic data RMS Task Reqt of Records = {}",
						mdRmsTaskReqtsList.size());
			}
		}

		String dept = extConfig.getString("MASTERDATA.TABLE.MD_RMS_DEPT");
		if (!HpUfisUtils.isNullOrEmptyStr(dept)) {
			LOG.info("External CFG: MasterData MD_RMS_DEPT enabled={}", dept);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(dept)) {
				List<Object> list = findAll(EntDbMdRmsDept.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsDept record = (EntDbMdRmsDept) it.next();
					mdRmsDeptList.add(record);
				}
				LOG.info("Basic data RMS Department of Records = {}",
						mdRmsDeptList.size());
			}
		}


		String taskType = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(taskType)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_TYPE enabled={}",
					taskType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskType)) {
				List<Object> list = findAll(EntDbMdRmsTaskType.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsTaskType record = (EntDbMdRmsTaskType) it.next();
					mdRmsTaskTypesList.add(record);
				}
				LOG.info("Basic data RMS Task Type of Records = {}",
						mdRmsTaskTypesList.size());
			}
		}


		String mutateCode = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_MUTATE_CODE");
		if (!HpUfisUtils.isNullOrEmptyStr(mutateCode)) {
			LOG.info("External CFG: MasterData MD_RMS_MUTATE_CODE enabled={}",
					mutateCode);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(mutateCode)) {
				List<Object> list = findAll(EntDbMdRmsMutateCode.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsMutateCode record = (EntDbMdRmsMutateCode) it
							.next();

					mdRmsMutateCodeList.add(record);
				}
				LOG.info("Basic data RMS Mutate Code of Records = {}",
						mdRmsMutateCodeList.size());
			}
		}


		String srvCode = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_SERV_CODE");
		if (!HpUfisUtils.isNullOrEmptyStr(srvCode)) {
			LOG.info("External CFG: MasterData MD_RMS_SERV_CODE enabled={}",
					srvCode);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(srvCode)) {
				List<Object> list = findAll(EntDbMdRmsServCode.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsServCode record = (EntDbMdRmsServCode) it.next();
					mdRmsServCodesList.add(record);
				}
				LOG.info("Basic data RMS Service Code of Records = {}",
						mdRmsServCodesList.size());
			}
		}



		// Additional for EngRTC
		String shiftType = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_SHIFT_TYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(shiftType)) {
			LOG.info("External CFG: MasterData MD_RMS_SHIFT_TYPE enabled={}",
					shiftType);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(shiftType)) {
				List<Object> list = findAll(EntDbMdRmsShiftType.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsShiftType record = (EntDbMdRmsShiftType) it
							.next();

					mdRmsShiftTypeList.add(record);
				}
				LOG.info("Basic data RMS Shift Type of Records = {}",
						mdRmsShiftTypeList.size());
			}
		}


		String subtaskStatus = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_SUBTASK_STATUS");
		if (!HpUfisUtils.isNullOrEmptyStr(subtaskStatus)) {
			LOG.info(
					"External CFG: MasterData MD_RMS_SUBTASK_STATUS enabled={}",
					subtaskStatus);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(subtaskStatus)) {
				List<Object> list = findAll(EntDbMdRmsSubtaskStatus.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsSubtaskStatus record = (EntDbMdRmsSubtaskStatus) it
							.next();

					mdRmsSubtaskStatusList.add(record);
				}
				LOG.info("Basic data RMS Subtask Status of Records = {}",
						mdRmsSubtaskStatusList.size());
			}
		}


		String taskGrptype = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_GRPTYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(taskGrptype)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_GRPTYPE enabled={}",
					taskGrptype);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskGrptype)) {
				List<Object> list = findAll(EntDbMdRmsTaskGrptype.class);
				for (Iterator<Object> it = list.iterator(); it.hasNext();) {
					EntDbMdRmsTaskGrptype record = (EntDbMdRmsTaskGrptype) it
							.next();

					mdRmsTaskGrptypeList.add(record);
				}
				LOG.info("Basic data RMS Task Grptype of Records = {}",
						mdRmsTaskGrptypeList.size());
			}
		}




	}


	// sort out master data for EK-RTC
	public void loadMasterDataForEKRTC() {
		String dept = extConfig.getString("MASTERDATA.TABLE.MD_RMS_DEPT");
		if (!HpUfisUtils.isNullOrEmptyStr(dept)) {
			LOG.info("External CFG: MasterData MD_RMS_DEPT enabled={}", dept);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(dept)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsDept.class, "EKAS", "deptId");

				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					EKASDeptList.add(record);
				}
				LOG.info("Basic data EKAS Department of Records = {}",
						EKASDeptList.size());
				//
				List<Object> GXlist = findAllByStaffType(EntDbMdRmsDept.class,
						"GX", "deptId");
				for (Iterator<Object> it = GXlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					GXDeptList.add(record);
				}
				LOG.info("Basic data GX Department of Records = {}",
						GXDeptList.size());
				//
				List<Object> CSlist = findAllByStaffType(EntDbMdRmsDept.class,
						"CS", "deptId");
				for (Iterator<Object> it = CSlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					CSDeptList.add(record);
				}
				LOG.info("Basic data CS Department of Records = {}",
						CSDeptList.size());
				isMDDeptListOn = true;
			}
		}


		String workArea = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_WORK_AREA");
		if (!HpUfisUtils.isNullOrEmptyStr(workArea)) {
			LOG.info("External CFG: MasterData MD_RMS_WORK_AREA enabled={}",
					workArea);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(workArea)) {
				List<Object> EKASworkAreaList = findAllByStaffType(
						EntDbMdRmsWorkArea.class, "EKAS", "workArea");
				for (Iterator<Object> it = EKASworkAreaList.iterator(); it
						.hasNext();) {


					String record = (String) it.next();
					EKASWorkAreasList.add(record);
				}
				LOG.info("Basic data EKAS Work Area of Records = {}",
						EKASWorkAreasList.size());
				//
				List<Object> GXworkAreaList = findAllByStaffType(
						EntDbMdRmsWorkArea.class, "GX", "workArea");
				for (Iterator<Object> it = GXworkAreaList.iterator(); it
						.hasNext();) {


					String record = (String) it.next();
					GXWorkAreasList.add(record);
				}
				LOG.info("Basic data GX Work Area of Records = {}",
						GXWorkAreasList.size());
				//
				List<Object> CSworkAreaList = findAllByStaffType(
						EntDbMdRmsWorkArea.class, "CS", "workArea");
				for (Iterator<Object> it = CSworkAreaList.iterator(); it
						.hasNext();) {


					String record = (String) it.next();
					CSWorkAreasList.add(record);
				}
				LOG.info("Basic data CS Work Area of Records = {}",
						CSWorkAreasList.size());
				isMDWorkAreasListOn = true;
			}
		}


		String taskReq = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_REQT");
		if (!HpUfisUtils.isNullOrEmptyStr(taskReq)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_REQT enabled={}",
					taskReq);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskReq)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsTaskReqt.class, "EKAS", "taskReqtId");

				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					EKASTaskReqtList.add(record);
				}
				LOG.info("Basic data EKAS Task Reqt of Records = {}",
						EKASTaskReqtList.size());
				//
				List<Object> GXlist = findAllByStaffType(
						EntDbMdRmsTaskReqt.class, "GX", "taskReqtId");

				for (Iterator<Object> it = GXlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					GXTaskReqtList.add(record);
				}
				LOG.info("Basic data GX Task Reqt of Records = {}",
						GXTaskReqtList.size());
				//
				List<Object> CSlist = findAllByStaffType(
						EntDbMdRmsTaskReqt.class, "CS", "taskReqtId");

				for (Iterator<Object> it = CSlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					CSTaskReqtList.add(record);
				}
				LOG.info("Basic data CS Task Reqt of Records = {}",
						CSTaskReqtList.size());
				isMDTaskReqtListOn = true;
			}


		}


		String taskGrptype = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_GRPTYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(taskGrptype)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_GRPTYPE enabled={}",
					taskGrptype);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskGrptype)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsTaskGrptype.class, "EKAS", "taskGrptypeId");

				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					EKASTaskGrptypeList.add(record);
				}
				LOG.info("Basic data EKAS Task Grptype of Records = {}",
						EKASTaskGrptypeList.size());
				//
				List<Object> GXlist = findAllByStaffType(
						EntDbMdRmsTaskGrptype.class, "GX", "taskGrptypeId");

				for (Iterator<Object> it = GXlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					GXTaskGrptypeList.add(record);
				}
				LOG.info("Basic data GX Task Grptype of Records = {}",
						GXTaskGrptypeList.size());
				//
				List<Object> CSlist = findAllByStaffType(
						EntDbMdRmsTaskGrptype.class, "CS", "taskGrptypeId");

				for (Iterator<Object> it = CSlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					CSTaskGrptypeList.add(record);
				}
				LOG.info("Basic data CS Task Grptype of Records = {}",
						CSTaskGrptypeList.size());
				isMDTaskGrptypeListOn = true;
			}
		}


		// String subtaskStatus =
		// extConfig.getString("MASTERDATA.TABLE.MD_RMS_SUBTASK_STATUS");
		// if (!HpUfisUtils.isNullOrEmptyStr(subtaskStatus)) {
		// LOG.info("External CFG: MasterData MD_RMS_SUBTASK_STATUS enabled={}",
		// subtaskStatus);
		// if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(subtaskStatus)) {
		// List<Object> list = findAllByStaffType(EntDbMdRmsSubtaskStatus.class,
		// "EKAS", "");
		// for (Iterator<Object> it = list.iterator(); it.hasNext();) {
		// EntDbMdRmsSubtaskStatus record = (EntDbMdRmsSubtaskStatus) it.next();
		// mdRmsSubtaskStatusList.add(record);
		// }
		// LOG.info("Basic data RMS Subtask Status of Records = {}",
		// mdRmsSubtaskStatusList.size());
		// }
		// }



	}


	// sort out master data for ENG-RTC
	public void loadMasterDataForENGRTC() {
		String dept = extConfig.getString("MASTERDATA.TABLE.MD_RMS_DEPT");
		if (!HpUfisUtils.isNullOrEmptyStr(dept)) {
			LOG.info("External CFG: MasterData MD_RMS_DEPT enabled={}", dept);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(dept)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsDept.class, "ERNG", "deptId");
				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					ENGRDeptList.add(record);
				}

				LOG.info("Basic data ERNG Department of Records = {}",
						ENGRDeptList.size());

				isMDDeptListOn = true;

			}
		}


		String workArea = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_WORK_AREA");
		if (!HpUfisUtils.isNullOrEmptyStr(workArea)) {
			LOG.info("External CFG: MasterData MD_RMS_WORK_AREA enabled={}",
					workArea);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(workArea)) {
				List<Object> ENGRworkAreaList = findAllByStaffType(
						EntDbMdRmsWorkArea.class, "ENGR", "workArea");
				for (Iterator<Object> it = ENGRworkAreaList.iterator(); it
						.hasNext();) {
					String record = (String) it.next();
					ENGRWorkAreasList.add(record);



				}
				LOG.info("Basic data ENGR Work Area of Records = {}",
						ENGRWorkAreasList.size());
				isMDWorkAreasListOn = true;
			}
		}



		String taskGrptype = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_GRPTYPE");
		if (!HpUfisUtils.isNullOrEmptyStr(taskGrptype)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_GRPTYPE enabled={}",
					taskGrptype);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskGrptype)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsTaskGrptype.class, "ENGR", "taskGrptypeId");
				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					ENGRTaskGrptypeList.add(record);



				}
				LOG.info("Basic data ENGR Task Grptype of Records = {}",
						ENGRTaskGrptypeList.size());
				isMDTaskGrptypeListOn = true;
			}
		}


		String taskReq = extConfig
				.getString("MASTERDATA.TABLE.MD_RMS_TASK_REQT");
		if (!HpUfisUtils.isNullOrEmptyStr(taskReq)) {
			LOG.info("External CFG: MasterData MD_RMS_TASK_REQT enabled={}",
					taskReq);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(taskReq)) {
				List<Object> EKASlist = findAllByStaffType(
						EntDbMdRmsTaskReqt.class, "ENGR", "taskReqtId");
				for (Iterator<Object> it = EKASlist.iterator(); it.hasNext();) {
					String record = (String) it.next();
					ENGRTaskReqtList.add(record);



				}
				LOG.info("Basic data ENGR Task Reqt of Records = {}",
						ENGRTaskReqtList.size());
				isMDTaskReqtListOn = true;

			}

		}
	}


	// sort out master data for MACS-PAX
	public void loadMasterDataForMACSPAX() {
		if (!masterDataOn) {
			LOG.info("master data is off ");
			return;// do nothing..
		}





		// ==============================================================================================
		// 2013-11-14 updated by JGO - According to Jira: UFIS-3041 to change MD source(change to ALTTAB)
		// ==============================================================================================
	/*	String bagTag = extConfig.getString("MASTERDATA.TABLE.MD_BAG_TAG");
		if (!HpUfisUtils.isNullOrEmptyStr(bagTag)) {
			LOG.info("External CFG: MasterData MD_BAG_TAG enabled={}", bagTag);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(bagTag)) {
				List<Object> tagList = findAll(EntDbMdBagTag.class);
				for (Iterator<Object> it = tagList.iterator(); it.hasNext();) {
					EntDbMdBagTag record = (EntDbMdBagTag) it.next();
					airlineCodeCnvertMap.put(record.getAirlineCode2(),
							record.getPrefixCode());
				}
				LOG.info("Basic data Bag Tag of Records = {}",
						MdBagTagList.size());
				isMDBagTagListOn = true;
			} 
		} */
		


		String alttab = extConfig.getString("MASTERDATA.TABLE.ALTTAB");
		if (!HpUfisUtils.isNullOrEmptyStr(alttab)) {
			LOG.info("External CFG: MasterData ALTTAB enabled={}", alttab);
			if (HpUfisAppConstants.CON_TRUE.equalsIgnoreCase(alttab)) {
				List<Object> tagList = findAll(EntDbAlttab.class);
				if (tagList != null) {
					LOG.info("{} Records found for ALTTAB", tagList.size());
					for (Iterator<Object> it = tagList.iterator(); it.hasNext();) {
						EntDbAlttab record = (EntDbAlttab) it.next();
						String airline = null;
						if (HpUfisUtils.isNotEmptyStr(record.getAlc2())) {
							airline = record.getAlc2();
						} else if (HpUfisUtils.isNotEmptyStr(record.getAlc3())) {
							airline = record.getAlc3();
						}
						if (airline != null) {
							String iano = record.getIano();
							if (HpUfisUtils.isNullOrEmptyStr(iano)) {
								iano = "000";
							} else {
								iano = "0" + iano.trim();
							}
							airlineCodeCnvertMap.put(airline, iano);
						}

					}
					isMDBagTagListOn = true;
				}
			}
		}
		LOG.info("{} airline codes loaded from ALTTAB. ",airlineCodeCnvertMap.keySet().size());
		
		

	}

	private void loadDbConfig() {
		// TODO load from configuration tables if needed
	}

	private void loadCedaBasicData() {
		// master data loading enabled or not
		if (masterDataOn) {

			if (mdApttabOn) {
				List<Object> apttabs = findAll(EntDbApttab.class);
				for (Iterator<Object> it = apttabs.iterator(); it.hasNext();) {
					EntDbApttab record = (EntDbApttab) it.next();
					_cacheApt.add(record);
				}
				LOG.info("Basic data APTTAB No of Records = {}",
						_cacheApt.size());
			}
			/*
			 * List<Object> acttabs = findAll(EntDbActtab.class); for
			 * (Iterator<Object> it = acttabs.iterator(); it.hasNext();) {
			 * EntDbActtab record = (EntDbActtab) it.next();
			 * _cacheAct.add(record); }
			 * LOG.info("Basic data ACTTAB No of Records = {}",
			 * _cacheAct.size()); List<Object> alttabs =
			 * findAll(EntDbAlttab.class); for (Iterator<Object> it =
			 * alttabs.iterator(); it.hasNext();) { EntDbAlttab record =
			 * (EntDbAlttab) it.next(); _cacheAlt.add(record); }
			 * LOG.info("Basic data ALTTAB No of Records = {}",
			 * _cacheAlt.size()); List<Object> nattabs =
			 * findAll(EntDbNattab.class); for (Iterator<Object> it =
			 * nattabs.iterator(); it.hasNext();) { EntDbNattab record =
			 * (EntDbNattab) it.next(); _cacheNat.add(record); }
			 * LOG.info("Basic data NATTAB No of Records = {}",
			 * _cacheNat.size()); List<Object> psttabs =
			 * findAll(EntDbPsttab.class); for (Iterator<Object> it =
			 * psttabs.iterator(); it.hasNext();) { EntDbPsttab record =
			 * (EntDbPsttab) it.next(); _cachePst.add(record); }
			 * LOG.info("Basic data PSTTAB No of Records = {}",
			 * _cachePst.size()); List<Object> blttabs =
			 * findAll(EntDbBlttab.class); for (Iterator<Object> it =
			 * blttabs.iterator(); it.hasNext();) { EntDbBlttab record =
			 * (EntDbBlttab) it.next(); _cacheBlt.add(record); }
			 * LOG.info("Basic data BLTTAB No of Records = {}",
			 * _cacheBlt.size()); List<Object> gattabs =
			 * findAll(EntDbGattab.class); for (Iterator<Object> it =
			 * gattabs.iterator(); it.hasNext();) { EntDbGattab record =
			 * (EntDbGattab) it.next(); _cacheGat.add(record); }
			 * LOG.info("Basic data GATTAB No of Records = {}",
			 * _cacheGat.size()); List<Object> sgmtabs =
			 * findAll(EntDbSgmtab.class); for (Iterator<Object> it =
			 * sgmtabs.iterator(); it.hasNext();) { EntDbSgmtab record =
			 * (EntDbSgmtab) it.next(); _cacheSgm.add(record); }
			 * LOG.info("Basic data SGMTAB No of Records = {}",
			 * _cacheSgm.size());
			 */
			if (mdUldItemTypeOn) {
				List<Object> uldItemType = findAll(EntDbMdUldItemType.class);
				for (Iterator<Object> it = uldItemType.iterator(); it.hasNext();) {
					EntDbMdUldItemType record = (EntDbMdUldItemType) it.next();
					UldItemTypeList.add(record);
				}
				LOG.info("Basic data Uld Item Type of Records = {}",
						UldItemTypeList.size());
			}
			if (mdUldLocOn) {
				List<Object> uldLoc = findAll(EntDbMdUldLoc.class);
				for (Iterator<Object> it = uldLoc.iterator(); it.hasNext();) {
					EntDbMdUldLoc record = (EntDbMdUldLoc) it.next();
					UldLocList.add(record);
				}
				LOG.info("Basic data Uld Location of Records = {}",
						UldLocList.size());
			}
			if (mdUldPosOn) {
				List<Object> uldPos = findAll(EntDbMdUldPos.class);
				for (Iterator<Object> it = uldPos.iterator(); it.hasNext();) {
					EntDbMdUldPos record = (EntDbMdUldPos) it.next();
					UldPosList.add(record);
				}
				LOG.info("Basic data Uld Position of Records = {}",
						UldPosList.size());
			}
			if (mdUldScanActOn) {
				List<Object> uldScanAct = findAll(EntDbMdUldScanAct.class);
				for (Iterator<Object> it = uldScanAct.iterator(); it.hasNext();) {
					EntDbMdUldScanAct record = (EntDbMdUldScanAct) it.next();
					UldScanActList.add(record);
				}
				LOG.info("Basic data Uld Scan Act of Records = {}",
						UldScanActList.size());
			}
			if (mdUldSubTypeOn) {
				List<Object> uldSubtype = findAll(EntDbMdUldSubType.class);
				for (Iterator<Object> it = uldSubtype.iterator(); it.hasNext();) {
					EntDbMdUldSubType record = (EntDbMdUldSubType) it.next();
					UldSubtypeList.add(record);
				}
				LOG.info("Basic data Uld Sub Type of Records = {}",
						UldSubtypeList.size());
			}
			if (mdUldTypeOn) {
				List<Object> uldType = findAll(EntDbMdUldType.class);
				for (Iterator<Object> it = uldType.iterator(); it.hasNext();) {
					EntDbMdUldType record = (EntDbMdUldType) it.next();
					UldTypeList.add(record);
				}
				LOG.info("Basic data Uld Type of Records = {}",
						UldTypeList.size());

				// //////////////////////////////////
				/*



				 * List<String> colsToSelect = new ArrayList<>();

				 * colsToSelect.add("id"); colsToSelect.add("uldTypeCode");
				 * 

				 * Map<String, String> whereClause = new HashMap<String,
				 * String>(); whereClause.put("idHopo", "DXB");
				 * 

				 * List<Object> result =
				 * findAllByColumnList(EntDbMdUldType.class, colsToSelect,
				 * whereClause);
				 * LOG.info("&&&&&&&&&&&&&&&&&&&&&&&& Size of Result : "
				 * +result.size()); //Map<String, String> res = new
				 * HashMap<String, String>(); for (Iterator<Object> it =
				 * result.iterator(); it.hasNext();) { LOG.debug("ID "+
				 * res.+" TypeCode "+ record.getUldTypeCode() + "TypeDesc "+
				 * record.getUldTypeDesc()); UldTypeList.add(record); }
				 */


			}
			if (mdLoungeCodeOn) {
				List<Object> LoungeCode = findAll(EntDbMdLoungeCode.class);
				for (Iterator<Object> it = LoungeCode.iterator(); it.hasNext();) {
					EntDbMdLoungeCode record = (EntDbMdLoungeCode) it.next();
					LoungeCodeList.add(record);
				}
				LOG.info("Basic data Lounge Code of Records = {}",
						LoungeCodeList.size());
			}
			if (mdGateTypeOn) {
				List<Object> gateType = findAll(EntDbMdGateType.class);
				for (Iterator<Object> it = gateType.iterator(); it.hasNext();) {
					EntDbMdGateType record = (EntDbMdGateType) it.next();
					GateTypeList.add(record);
				}
				LOG.info("Basic data gate type of Records = {}",
						GateTypeList.size());
			}
			if (mdStaffTypeOn) {
				List<Object> staffType = findAll(EntDbMdStaffType.class);
				for (Iterator<Object> it = staffType.iterator(); it.hasNext();) {
					EntDbMdStaffType record = (EntDbMdStaffType) it.next();
					StaffTypeList.add(record);
				}
				LOG.info("Basic data staff type of Records = {}",
						StaffTypeList.size());
			}
			// For BELT
			if (mdBagHandleLocOn) {
				List<Object> bagHandleLoc = findAll(EntDbMdBagHandleLoc.class);
				for (Iterator<Object> it = bagHandleLoc.iterator(); it
						.hasNext();) {
					EntDbMdBagHandleLoc record = (EntDbMdBagHandleLoc) it
							.next();


					mdBagHandleLocList.add(record);
				}
				LOG.info("Master data Bag Handle Location of Records = {}",
						mdBagHandleLocList.size());
			}
			if (mdBagReconLocOn) {
				List<Object> bagReconLoc = findAll(EntDbMdBagReconLoc.class);
				for (Iterator<Object> it = bagReconLoc.iterator(); it.hasNext();) {
					EntDbMdBagReconLoc record = (EntDbMdBagReconLoc) it.next();
					mdBagReconLocList.add(record);
				}
				LOG.info("Master data Bag Recon Location of Records = {}",
						mdBagReconLocList.size());
			}
			if (mdBagActionOn) {
				List<Object> bagActionList = findAll(EntDbMdBagAction.class);
				for (Iterator<Object> it = bagActionList.iterator(); it
						.hasNext();) {

					EntDbMdBagAction record = (EntDbMdBagAction) it.next();
					mdBagActionList.add(record);
				}
				LOG.info("Master data Bag Action of Records = {}",
						mdBagActionList.size());
			}
			if (mdBagTypeOn) {
				List<Object> bagTypeList = findAll(EntDbMdBagType.class);
				for (Iterator<Object> it = bagTypeList.iterator(); it.hasNext();) {
					EntDbMdBagType record = (EntDbMdBagType) it.next();
					mdBagTypeList.add(record);
				}
				LOG.info("Master data Bag Types Records = {}",
						mdBagTypeList.size());
			}
			if (mdBagClassifyOn) {
				List<Object> bagClassifyList = findAll(EntDbMdBagClassify.class);
				for (Iterator<Object> it = bagClassifyList.iterator(); it
						.hasNext();) {

					EntDbMdBagClassify record = (EntDbMdBagClassify) it.next();
					mdBagClassifyList.add(record);
				}
				LOG.info("Master data Bag Classify Records = {}",
						mdBagClassifyList.size());
			}
			if (mdEquipLocOn) {
				List<Object> euipLocList = findAll(EntDbMdEquipLoc.class);
				for (Iterator<Object> it = euipLocList.iterator(); it.hasNext();) {
					EntDbMdEquipLoc record = (EntDbMdEquipLoc) it.next();
					if (!HpUfisUtils.isNullOrEmptyStr(record.getEquipArea())) {
						mdEquipLocList.add(record.getEquipArea().toUpperCase());
					}
				}
				LOG.info("Master data Equip Loc Records = {}",
						mdEquipLocList.size());
			}
			if (mdEquipOn) {
				List<Object> equipList = findAll(EntDbMdEquip.class);
				for (Iterator<Object> it = equipList.iterator(); it.hasNext();) {
					EntDbMdEquip record = (EntDbMdEquip) it.next();
					if (!HpUfisUtils.isNullOrEmptyStr(record.getEquipNum())) {
						mdEquipList.add(record.getEquipNum().toUpperCase());
					}
				}
				LOG.info("Master data Equip Records = {}", mdEquipList.size());
			}
		}
		// if (afttabOn) {
		// long startTime = new Date().getTime();
		// List<Object> afttabList = findAll(EntDbAfttab.class);
		// LOG.info("Master data AFTTAB Records = {}", afttabList.size());
		// LOG.info("Total Duration on loading flight records from AFTTAB (in ms): {}",
		// new Date().getTime() - startTime);
		// }
	}

	private List<Object> findAll(Class entityClass) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		cq.select(cq.from(entityClass));
		return em.createQuery(cq).getResultList();
	}


	// for EK-RTC
	private List<Object> findAllByStaffType(Class entityClass,
			String staffType, String x) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root r = cq.from(entityClass);
		cq.select(r.get(x));
		Predicate predicate = cb.equal(r.get("staffTypeCode"), staffType);
		cq.where(predicate);
		return em.createQuery(cq).getResultList();
	}

	private List<Object> findAllByColumnList(Class entityClass,
			List<String> colsToSelect, Map<String, String> whereClauseCriteria) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root r = cq.from(entityClass);
		List<Selection> selections = new ArrayList<>();
		for (String col : colsToSelect) {
			selections.add(r.get(col));
		}
		// cq.multiselect(selections);


		List<Predicate> predicates = new ArrayList<Predicate>();
		for (String col : whereClauseCriteria.keySet()) {
			predicates.add(cb.equal(r.get(col), whereClauseCriteria.get(col)));
		}
		cq.multiselect(selections)
				.where(predicates.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();
	}


	public static boolean containsObj(Class<? extends Enum> clazz, String val) {
		Object[] arr = clazz.getEnumConstants();
		for (Object e : arr) {
			if (((Enum) e).name().equals(val)) {
				return true;
			}



		}
		return false;
	}


	/**
	 * Getter and Setter
	 */
	public String getBcFromUfisTopic() {
		return bcFromUfisTopic;
	}

	public void setBcFromUfisTopic(String bcFromUfisTopic) {
		this.bcFromUfisTopic = bcFromUfisTopic;
	}

	/**
	 * @return the irmLog
	 */
	public String getIrmLogLev() {
		return irmLogLev;
	}

	/**
	 * @param irmLog
	 *            the irmLog to set
	 */
	public void setIrmLogLev(String irmLog) {
		this.irmLogLev = irmLog;
	}


	public String getBcFromUfisQueue() {
		return bcFromUfisQueue;
	}

	public void setBcFromUfisQueue(String bcFromUfisQueue) {
		this.bcFromUfisQueue = bcFromUfisQueue;
	}

	public String getBcToUfisTopic() {
		return bcToUfisTopic;
	}

	public void setBcToUfisTopic(String bcToUfisTopic) {
		this.bcToUfisTopic = bcToUfisTopic;
	}

	public String getBcToUfisQueue() {
		return bcToUfisQueue;
	}

	public void setBcToUfisQueue(String bcToUfisQueue) {
		this.bcToUfisQueue = bcToUfisQueue;
	}

	public List<EntDbActtab> getCacheAct() {
		return _cacheAct;
	}

	public void setCacheAct(List<EntDbActtab> _cacheAct) {
		this._cacheAct = _cacheAct;
	}

	public List<EntDbAlttab> getCacheAlt() {
		return _cacheAlt;
	}

	public void setCacheAlt(List<EntDbAlttab> _cacheAlt) {
		this._cacheAlt = _cacheAlt;
	}

	public List<EntDbApttab> getCacheApt() {
		return _cacheApt;
	}

	public void setCacheApt(List<EntDbApttab> _cacheApt) {
		this._cacheApt = _cacheApt;
	}

	public List<EntDbNattab> getCacheNat() {
		return _cacheNat;
	}

	public void setCacheNat(List<EntDbNattab> _cacheNat) {
		this._cacheNat = _cacheNat;
	}

	public List<EntDbPsttab> getCachePst() {
		return _cachePst;
	}

	public void setCachePst(List<EntDbPsttab> _cachePst) {
		this._cachePst = _cachePst;
	}

	public List<EntDbBlttab> getCacheBlt() {
		return _cacheBlt;
	}

	public void setCacheBlt(List<EntDbBlttab> _cacheBlt) {
		this._cacheBlt = _cacheBlt;
	}

	public List<EntDbGattab> getCacheGat() {
		return _cacheGat;
	}

	public void setCacheGat(List<EntDbGattab> _cacheGat) {
		this._cacheGat = _cacheGat;
	}

	public List<EntDbSgmtab> getCacheSgm() {
		return _cacheSgm;
	}

	public void setCacheSgm(List<EntDbSgmtab> _cacheSgm) {
		this._cacheSgm = _cacheSgm;
	}

	public Map<String, String> getEnvMap() {
		return envMap;
	}

	public void setEnvMap(Map<String, String> envMap) {
		this.envMap = envMap;
	}

	public List<EntDbMdUldItemType> getUldItemTypeList() {
		return UldItemTypeList;
	}

	public void setUldItemTypeList(List<EntDbMdUldItemType> uldItemTypeList) {
		UldItemTypeList = uldItemTypeList;
	}

	public List<EntDbMdUldLoc> getUldLocList() {
		return UldLocList;
	}

	public void setUldLocList(List<EntDbMdUldLoc> uldLocList) {
		UldLocList = uldLocList;
	}

	public List<EntDbMdUldPos> getUldPosList() {
		return UldPosList;
	}

	public void setUldPosList(List<EntDbMdUldPos> uldPosList) {
		UldPosList = uldPosList;
	}

	public List<EntDbMdUldScanAct> getUldScanActList() {
		return UldScanActList;
	}

	public void setUldScanActList(List<EntDbMdUldScanAct> uldScanActList) {
		UldScanActList = uldScanActList;
	}

	public List<EntDbMdUldSubType> getUldSubtypeList() {
		return UldSubtypeList;
	}

	public void setUldSubtypeList(List<EntDbMdUldSubType> uldSubtypeList) {
		UldSubtypeList = uldSubtypeList;
	}

	public List<EntDbMdUldType> getUldTypeList() {
		return UldTypeList;
	}

	public void setUldTypeList(List<EntDbMdUldType> uldTypeList) {
		UldTypeList = uldTypeList;
	}

	public List<EntDbMdLoungeCode> getLoungeCodeList() {
		return LoungeCodeList;
	}

	public void setLoungeCodeList(List<EntDbMdLoungeCode> loungeCodeList) {
		LoungeCodeList = loungeCodeList;
	}

	public List<EntDbMdGateType> getGateTypeList() {
		return GateTypeList;
	}

	public void setGateTypeList(List<EntDbMdGateType> gateTypeList) {
		GateTypeList = gateTypeList;
	}

	public List<EntDbMdStaffType> getStaffTypeList() {
		return StaffTypeList;
	}

	public boolean isIrmOn() {
		return irmOn;
	}

	public void setIrmOn(boolean irmOn) {
		this.irmOn = irmOn;
	}

	public boolean isIrmFullLogOn() {
		return irmFullLogOn;
	}

	public void setIrmFullLogOn(boolean irmFullLogOn) {
		this.irmFullLogOn = irmFullLogOn;
	}

	public boolean isMasterDataOn() {
		return masterDataOn;
	}

	public boolean isOrmOn() {
		return ormOn;
	}


	public void setOrmOn(boolean ormOn) {
		this.ormOn = ormOn;
	}


	public void setMasterDataOn(boolean masterDataOn) {
		this.masterDataOn = masterDataOn;
	}

	public List<InterfaceConfig> getInterfaceConfigs() {
		return interfaceConfigs;
	}

	public void setInterfaceConfigs(List<InterfaceConfig> interfaceConfigs) {
		this.interfaceConfigs = interfaceConfigs;
	}

	public List<String> getFromQueueList() {
		return fromQueueList;
	}

	public void setFromQueueList(List<String> fromQueueList) {
		this.fromQueueList = fromQueueList;
	}

	public List<String> getToQueueList() {
		return toQueueList;
	}

	public void setToQueueList(List<String> toQueueList) {
		this.toQueueList = toQueueList;
	}

	public boolean isBcOn() {
		return bcOn;
	}

	public void setBcOn(boolean bcOn) {
		this.bcOn = bcOn;
	}

	public boolean isAppendWeatherFlag() {
		return appendWeatherFlag;
	}

	public void setAppendWeatherFlag(boolean appendWeatherFlag) {
		this.appendWeatherFlag = appendWeatherFlag;
	}

	public float getFuelRate() {
		return fuelRate;
	}

	public void setFuelRate(float fuelRate) {
		this.fuelRate = fuelRate;
	}

	public String getDtflStr() {
		return dtflStr;
	}

	public void setDtflStr(String dtflStr) {
		this.dtflStr = dtflStr;
	}

	public boolean isKeepAllStnFlag() {
		return keepAllStnFlag;
	}

	public void setKeepAllStnFlag(boolean keepAllStnFlag) {
		this.keepAllStnFlag = keepAllStnFlag;
	}

	public List<EntDbMdBagHandleLoc> getMdBagHandleLocList() {
		return mdBagHandleLocList;
	}

	public void setMdBagHandleLocList(
			List<EntDbMdBagHandleLoc> mdBagHandleLocList) {

		this.mdBagHandleLocList = mdBagHandleLocList;
	}

	public List<EntDbMdBagReconLoc> getMdBagReconLocList() {
		return mdBagReconLocList;
	}

	public void setMdBagReconLocList(List<EntDbMdBagReconLoc> mdBagReconLocList) {
		this.mdBagReconLocList = mdBagReconLocList;
	}

	public List<EntDbMdBagAction> getMdBagActionList() {
		return mdBagActionList;
	}

	public void setMdBagActionList(List<EntDbMdBagAction> mdBagActionList) {
		this.mdBagActionList = mdBagActionList;
	}

	public List<EntDbMdBagType> getMdBagTypeList() {
		return mdBagTypeList;
	}

	public void setMdBagTypeList(List<EntDbMdBagType> mdBagTypeList) {
		this.mdBagTypeList = mdBagTypeList;
	}

	public List<EntDbMdBagClassify> getMdBagClassifyList() {
		return mdBagClassifyList;
	}

	public void setMdBagClassifyList(List<EntDbMdBagClassify> mdBagClassifyList) {
		this.mdBagClassifyList = mdBagClassifyList;
	}

	public boolean isMdBagHandleLocOn() {
		return mdBagHandleLocOn;
	}

	public boolean isAfttabOn() {
		return afttabOn;
	}

	public void setAfttabOn(boolean afttabOn) {
		this.afttabOn = afttabOn;
	}

	public void setMdBagHandleLocOn(boolean mdBagHandleLocOn) {
		this.mdBagHandleLocOn = mdBagHandleLocOn;
	}

	public boolean isMdBagReconLocOn() {
		return mdBagReconLocOn;
	}

	public void setMdBagReconLocOn(boolean mdBagReconLocOn) {
		this.mdBagReconLocOn = mdBagReconLocOn;
	}

	public boolean isMdBagActionOn() {
		return mdBagActionOn;
	}

	public void setMdBagActionOn(boolean mdBagActionOn) {
		this.mdBagActionOn = mdBagActionOn;
	}

	public boolean isMdBagTypeOn() {
		return mdBagTypeOn;
	}

	public void setMdBagTypeOn(boolean mdBagTypeOn) {
		this.mdBagTypeOn = mdBagTypeOn;
	}

	public boolean isMdBagClassifyOn() {
		return mdBagClassifyOn;
	}

	public void setMdBagClassifyOn(boolean mdBagClassifyOn) {
		this.mdBagClassifyOn = mdBagClassifyOn;
	}

	public String getUwsGroovyParserPath() {
		return uwsGroovyParserPath;
	}

	public String getUwsGroovyParserFileName() {
		return uwsGroovyParserFileName;
	}

	public List<String> getFromTopicList() {
		return fromTopicList;
	}

	public void setFromTopicList(List<String> fromTopicList) {
		this.fromTopicList = fromTopicList;
	}

	public List<String> getToTopicList() {
		return toTopicList;
	}

	public void setToTopicList(List<String> toTopicList) {
		this.toTopicList = toTopicList;
	}

	public List<Object> getStateNameMapList() {
		return stateNameMapList;
	}

	public void setStateNameMapList(List<Object> stateNameMapList) {
		this.stateNameMapList = stateNameMapList;
	}

	public List<Object> getStateCurrValueList() {
		return stateCurrValueList;
	}

	public void setStateCurrValueList(List<Object> stateCurrValueList) {
		this.stateCurrValueList = stateCurrValueList;
	}

	public boolean isMdEquipLocOn() {
		return mdEquipLocOn;
	}

	public void setMdEquipLocOn(boolean mdEquipLocOn) {
		this.mdEquipLocOn = mdEquipLocOn;
	}

	public boolean isMdEquipOn() {
		return mdEquipOn;
	}

	public void setMdEquipOn(boolean mdEquipOn) {
		this.mdEquipOn = mdEquipOn;
	}

	public List<String> getMdEquipLocList() {
		return mdEquipLocList;
	}

	public void setMdEquipLocList(List<String> mdEquipLocList) {
		this.mdEquipLocList = mdEquipLocList;
	}

	public List<String> getMdEquipList() {
		return mdEquipList;
	}

	public void setMdEquipList(List<String> mdEquipList) {
		this.mdEquipList = mdEquipList;
	}

	public List<EntDbMdRmsDept> getMdRmsDeptList() {
		return mdRmsDeptList;
	}

	public void setMdRmsDeptList(List<EntDbMdRmsDept> mdRmsDeptList) {
		this.mdRmsDeptList = mdRmsDeptList;
	}

	public List<EntDbMdRmsFltStat> getMdRmsFltStatList() {
		return mdRmsFltStatList;
	}

	public void setMdRmsFltStatList(List<EntDbMdRmsFltStat> mdRmsFltStatList) {
		this.mdRmsFltStatList = mdRmsFltStatList;
	}

	public List<EntDbMdRmsMutateCode> getMdRmsMutateCodeList() {
		return mdRmsMutateCodeList;
	}

	public void setMdRmsMutateCodeList(
			List<EntDbMdRmsMutateCode> mdRmsMutateCodeList) {
		this.mdRmsMutateCodeList = mdRmsMutateCodeList;
	}

	public List<EntDbMdRmsOrderType> getMdRmsOrderTypeList() {
		return mdRmsOrderTypeList;
	}

	public void setMdRmsOrderTypeList(
			List<EntDbMdRmsOrderType> mdRmsOrderTypeList) {

		this.mdRmsOrderTypeList = mdRmsOrderTypeList;
	}

	public List<EntDbMdRmsResourceType> getMdRmsRrscTypeList() {
		return mdRmsRrscTypeList;
	}

	public void setMdRmsRrscTypeList(
			List<EntDbMdRmsResourceType> mdRmsRrscTypeList) {

		this.mdRmsRrscTypeList = mdRmsRrscTypeList;
	}

	public List<EntDbMdRmsServCode> getMdRmsServCodesList() {
		return mdRmsServCodesList;
	}

	public void setMdRmsServCodesList(
			List<EntDbMdRmsServCode> mdRmsServCodesList) {

		this.mdRmsServCodesList = mdRmsServCodesList;
	}

	public List<EntDbMdRmsTaskReqt> getMdRmsTaskReqtsList() {
		return mdRmsTaskReqtsList;
	}

	public void setMdRmsTaskReqtsList(
			List<EntDbMdRmsTaskReqt> mdRmsTaskReqtsList) {

		this.mdRmsTaskReqtsList = mdRmsTaskReqtsList;
	}

	public List<EntDbMdRmsTaskStatus> getMdRmsTaskStatusList() {
		return mdRmsTaskStatusList;
	}

	public void setMdRmsTaskStatusList(
			List<EntDbMdRmsTaskStatus> mdRmsTaskStatusList) {
		this.mdRmsTaskStatusList = mdRmsTaskStatusList;
	}

	public List<EntDbMdRmsTaskType> getMdRmsTaskTypesList() {
		return mdRmsTaskTypesList;
	}

	public void setMdRmsTaskTypesList(
			List<EntDbMdRmsTaskType> mdRmsTaskTypesList) {

		this.mdRmsTaskTypesList = mdRmsTaskTypesList;
	}

	public List<EntDbMdRmsWorkArea> getMdRmsWorkAreasList() {
		return mdRmsWorkAreasList;
	}

	public void setMdRmsWorkAreasList(
			List<EntDbMdRmsWorkArea> mdRmsWorkAreasList) {

		this.mdRmsWorkAreasList = mdRmsWorkAreasList;
	}

	public List<EntDbMdRmsWorkLoc> getMdRmsWorkLocsList() {
		return mdRmsWorkLocsList;
	}

	public void setMdRmsWorkLocsList(List<EntDbMdRmsWorkLoc> mdRmsWorkLocsList) {
		this.mdRmsWorkLocsList = mdRmsWorkLocsList;
	}

	public boolean isAppendNotamsFlag() {
		return appendNotamsFlag;
	}

	public void setAppendNotamsFlag(boolean appendNotamsFlag) {
		this.appendNotamsFlag = appendNotamsFlag;
	}

	public String getToCedaQ() {
		return toCedaQ;
	}

	public void setToCedaQ(String toCedaQ) {
		this.toCedaQ = toCedaQ;
	}

	public List<String> getEKASDeptList() {
		return EKASDeptList;
	}

	public void setEKASDeptList(List<String> eKASDeptList) {
		EKASDeptList = eKASDeptList;
	}

	public List<String> getGXDeptList() {
		return GXDeptList;
	}

	public void setGXDeptList(List<String> gXDeptList) {
		GXDeptList = gXDeptList;
	}

	public List<String> getCSDeptList() {
		return CSDeptList;
	}

	public void setCSDeptList(List<String> cSDeptList) {
		CSDeptList = cSDeptList;
	}

	public List<String> getEKASWorkAreasList() {
		return EKASWorkAreasList;
	}

	public void setEKASWorkAreasList(List<String> eKASWorkAreasList) {
		EKASWorkAreasList = eKASWorkAreasList;
	}

	public List<String> getGXWorkAreasList() {
		return GXWorkAreasList;
	}

	public void setGXWorkAreasList(List<String> gXWorkAreasList) {
		GXWorkAreasList = gXWorkAreasList;
	}

	public List<String> getCSWorkAreasList() {
		return CSWorkAreasList;
	}

	public void setCSWorkAreasList(List<String> cSWorkAreasList) {
		CSWorkAreasList = cSWorkAreasList;
	}

	public List<String> getEKASTaskStatusList() {
		return EKASTaskStatusList;
	}

	public void setEKASTaskStatusList(List<String> eKASTaskStatusList) {
		EKASTaskStatusList = eKASTaskStatusList;
	}

	public List<String> getGXTaskStatusList() {
		return GXTaskStatusList;
	}

	public void setGXTaskStatusList(List<String> gXTaskStatusList) {
		GXTaskStatusList = gXTaskStatusList;
	}

	public List<String> getCSTaskStatusList() {
		return CSTaskStatusList;
	}

	public void setCSTaskStatusList(List<String> cSTaskStatusList) {
		CSTaskStatusList = cSTaskStatusList;
	}

	public List<String> getEKASTaskTypesList() {
		return EKASTaskTypesList;
	}

	public void setEKASTaskTypesList(List<String> eKASTaskTypesList) {
		EKASTaskTypesList = eKASTaskTypesList;
	}

	public List<String> getGXTaskTypesList() {
		return GXTaskTypesList;
	}

	public void setGXTaskTypesList(List<String> gXTaskTypesList) {
		GXTaskTypesList = gXTaskTypesList;
	}

	public List<String> getCSTaskTypesList() {
		return CSTaskTypesList;
	}

	public void setCSTaskTypesList(List<String> cSTaskTypesList) {
		CSTaskTypesList = cSTaskTypesList;
	}

	public List<String> getEKASTaskGrptypeList() {
		return EKASTaskGrptypeList;
	}

	public void setEKASTaskGrptypeList(List<String> eKASTaskGrptypeList) {
		EKASTaskGrptypeList = eKASTaskGrptypeList;
	}

	public List<String> getGXTaskGrptypeList() {
		return GXTaskGrptypeList;
	}

	public void setGXTaskGrptypeList(List<String> gXTaskGrptypeList) {
		GXTaskGrptypeList = gXTaskGrptypeList;
	}

	public List<String> getCSTaskGrptypeList() {
		return CSTaskGrptypeList;
	}

	public void setCSTaskGrptypeList(List<String> cSTaskGrptypeList) {
		CSTaskGrptypeList = cSTaskGrptypeList;
	}

	public List<String> getEKASTaskReqtList() {
		return EKASTaskReqtList;
	}

	public void setEKASTaskReqtList(List<String> eKASTaskReqtList) {
		EKASTaskReqtList = eKASTaskReqtList;
	}

	public List<String> getGXTaskReqtList() {
		return GXTaskReqtList;
	}

	public void setGXTaskReqtList(List<String> gXTaskReqtList) {
		GXTaskReqtList = gXTaskReqtList;
	}

	public List<String> getCSTaskReqtList() {
		return CSTaskReqtList;
	}

	public void setCSTaskReqtList(List<String> cSTaskReqtList) {
		CSTaskReqtList = cSTaskReqtList;
	}

	public boolean isMDDeptListOn() {
		return isMDDeptListOn;
	}

	public void setMDDeptListOn(boolean isMDDeptListOn) {
		this.isMDDeptListOn = isMDDeptListOn;
	}

	public boolean isMDWorkAreasListOn() {
		return isMDWorkAreasListOn;
	}

	public void setMDWorkAreasListOn(boolean isMDWorkAreasListOn) {
		this.isMDWorkAreasListOn = isMDWorkAreasListOn;
	}

	public boolean isMDTaskStatusListOn() {
		return isMDTaskStatusListOn;
	}

	public void setMDTaskStatusListOn(boolean isMDTaskStatusListOn) {
		this.isMDTaskStatusListOn = isMDTaskStatusListOn;
	}

	public boolean isMDTaskTypesListOn() {
		return isMDTaskTypesListOn;
	}

	public void setMDTaskTypesListOn(boolean isMDTaskTypesListOn) {
		this.isMDTaskTypesListOn = isMDTaskTypesListOn;
	}

	public boolean isMDTaskGrptypeListOn() {
		return isMDTaskGrptypeListOn;
	}

	public void setMDTaskGrptypeListOn(boolean isMDTaskGrptypeListOn) {
		this.isMDTaskGrptypeListOn = isMDTaskGrptypeListOn;
	}

	public boolean isMDTaskReqtListOn() {
		return isMDTaskReqtListOn;
	}

	public void setMDTaskReqtListOn(boolean isMDTaskReqtListOn) {
		this.isMDTaskReqtListOn = isMDTaskReqtListOn;
	}

	public List<String> getENGRDeptList() {
		return ENGRDeptList;
	}

	public void setENGRDeptList(List<String> eNGRDeptList) {
		ENGRDeptList = eNGRDeptList;
	}

	public List<String> getENGRWorkAreasList() {
		return ENGRWorkAreasList;
	}

	public void setENGRWorkAreasList(List<String> eNGRWorkAreasList) {
		ENGRWorkAreasList = eNGRWorkAreasList;
	}

	public List<String> getENGRTaskGrptypeList() {
		return ENGRTaskGrptypeList;
	}

	public void setENGRTaskGrptypeList(List<String> eNGRTaskGrptypeList) {
		ENGRTaskGrptypeList = eNGRTaskGrptypeList;
	}

	public List<String> getENGRTaskReqtList() {
		return ENGRTaskReqtList;
	}

	public void setENGRTaskReqtList(List<String> eNGRTaskReqtList) {
		ENGRTaskReqtList = eNGRTaskReqtList;
	}

	public List<String> getUldSubTypeCategoryList() {
		return uldSubTypeCategoryList;
	}

	public void setUldSubTypeCategoryList(List<String> uldSubTypeCategoryList) {
		this.uldSubTypeCategoryList = uldSubTypeCategoryList;
	}

	public List<String> getUldTypeCategoryList() {
		return uldTypeCategoryList;
	}

	public void setUldTypeCategoryList(List<String> uldTypeCategoryList) {
		this.uldTypeCategoryList = uldTypeCategoryList;
	}

	public HashMap<String, List<String>> getUldTypeMap() {
		return uldTypeMap;
	}

	public void setUldTypeMap(HashMap<String, List<String>> uldTypeMap) {
		this.uldTypeMap = uldTypeMap;
	}

	public List<String> getUldInfoTypeCategoryList() {
		return uldInfoTypeCategoryList;
	}

	public void setUldInfoTypeCategoryList(List<String> uldInfoTypeCategoryList) {
		this.uldInfoTypeCategoryList = uldInfoTypeCategoryList;
	}

	public int getTimerInterval() {
		return timerInterval;
	}

	public void setTimerInterval(int timerInterval) {
		this.timerInterval = timerInterval;
	}

	public String getExptDataStore() {
		return exptDataStore;

	}

	public void setExptDataStore(String exptDataStore) {
		this.exptDataStore = exptDataStore;

	}

	public HashMap<String, String> getAirlineCodeCnvertMap() {
		return airlineCodeCnvertMap;
	}
	
	public Integer getRampOffset() {


		return rampOffset;
	}
	
	public void setRampOffset(Integer rampOffset) {
		this.rampOffset = rampOffset;
	}

	public Map<String, String> getQueueNameMap() {
		return queueNameMap;
	}

	public void setQueueNameMap(Map<String, String> queueNameMap) {
		this.queueNameMap = queueNameMap;
	}

	public boolean isMDBagTagListOn() {
		return isMDBagTagListOn;
	}

	public void setMDBagTagListOn(boolean isMDBagTagListOn) {
		this.isMDBagTagListOn = isMDBagTagListOn;
	}

	





}
