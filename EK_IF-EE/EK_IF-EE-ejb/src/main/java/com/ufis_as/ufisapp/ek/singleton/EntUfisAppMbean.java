/*
 * $Id: EntMonitoringResource.java 9001 2013-09-23 08:31:00Z jxi $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.ek.singleton;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JMX Monitor Implementation
 * 
 * @author $Author: jxi $
 * @version $Revision: 9001 $
 */
@Singleton
@Startup
public class EntUfisAppMbean implements DynamicMBean {

	/**
	 * Logger
	 */
	private static final Logger LOG = (Logger) LoggerFactory
			.getLogger(EntUfisAppMbean.class);
	private MBeanServer _platformMBeanServer;
	@Resource
	EJBContext sc;
	private String earName = "";
	private String appInfo = "";
	private ObjectName objectName;

	@PostConstruct
	public void registerInJMX() {
		Object appName = null;
		try {
			Method getComponentMethod = this.findDeclaredMethod(sc,
					"getComponent");
			Object ejbComponet = getComponentMethod.invoke(sc);
			Method getApplicationNameMethod = this.findDeclaredMethod(
					ejbComponet, "getApplicationName");
			appName = getApplicationNameMethod.invoke(ejbComponet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		earName = appName + ":name=UfisAppMbean";
		this.getAppInfo();
		this.setName(earName);
		_platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
		try {
			objectName = new ObjectName(earName);
			_platformMBeanServer.registerMBean(this, objectName);
			LOG.info("JMX Register: {}", objectName);
		} catch (MalformedObjectNameException | InstanceAlreadyExistsException
				| MBeanRegistrationException | NotCompliantMBeanException e) {
			e.printStackTrace();
		}

	}

	@PreDestroy
	public void unregisterFromJMX() {
		try {
			_platformMBeanServer.unregisterMBean(objectName);
			LOG.info("Unregistration of Monitoring  JMX: {}", objectName);
		} catch (MBeanRegistrationException e) {
			e.printStackTrace();
		} catch (InstanceNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getAttribute(String attribute)
			throws AttributeNotFoundException, MBeanException,
			ReflectionException {
		if (attribute == null) {
			throw new AttributeNotFoundException();
		}
		if ("name".equalsIgnoreCase(attribute)) {
			return getName();
		}
		if ("appInfo".equalsIgnoreCase(attribute)) {
			return getAppInfo();
		}
		throw new AttributeNotFoundException();
	}

	@Override
	public void setAttribute(Attribute attribute)
			throws AttributeNotFoundException, InvalidAttributeValueException,
			MBeanException, ReflectionException {
		String name = attribute.getName();
		Object value = attribute.getValue();
		if ("name".equalsIgnoreCase(name)) {
			this.setName(String.valueOf(value));
			return;
		}
		if ("appInfo".equalsIgnoreCase(name)) {
			this.setAppInfo(String.valueOf(value));
			return;
		}
		throw new AttributeNotFoundException();

	}

	@Override
	public AttributeList getAttributes(String[] attributes) {
		return null;
	}

	@Override
	public AttributeList setAttributes(AttributeList attributes) {
		return null;
	}

	@Override
	public Object invoke(String actionName, Object[] params, String[] signature)
			throws MBeanException, ReflectionException {
		if ("printName".equals(actionName)) {
			this.printName();
		}

		return null;
	}

	@Override
	public MBeanInfo getMBeanInfo() {

		MBeanAttributeInfo nameAttr = new MBeanAttributeInfo("name", "String",
				"ear file name", true, true, false);
		MBeanAttributeInfo appInfoAttr = new MBeanAttributeInfo("appInfo",
				"String", "ear app info", true, true, false);
		MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[] { nameAttr,
				appInfoAttr };

		MBeanOperationInfo opertions[] = { new MBeanOperationInfo("printName",
				"print", null, "void", MBeanOperationInfo.ACTION) };

		MBeanInfo info = new MBeanInfo(this.getClass().getName(),
				"Ufis App Mbean", attributes, null, opertions, null);
		return info;
	}

	public String getName() {
		return earName;
	}

	public void setName(String name) {
		this.earName = name;
	}

	public void setAppInfo(String appInfo) {
		this.appInfo = appInfo;
	}

	public void printName() {
		System.out.println(earName);
	}

	private String getAppInfo() {
		if (appInfo.isEmpty()) {
			InputStream fis = null;
			try {
				fis = EntUfisAppMbean.class.getClassLoader()
						.getResourceAsStream("appInfo.properties");
				Properties props = new Properties();

				props.load(fis);

				StringBuilder appInfoStringBuilder = new StringBuilder();
				Iterator<Object> it = props.keySet().iterator();
				LOG.debug("******* AppInfo of {} *******", earName);
				while (it.hasNext()) {
					String key = it.next().toString();
					String value = props.getProperty(key);
					appInfoStringBuilder.append(key);
					appInfoStringBuilder.append(":");
					appInfoStringBuilder.append(value);
					appInfoStringBuilder.append("&");
					LOG.debug("{} = {}", key, value);
				}
				appInfo = appInfoStringBuilder.toString();
			} catch (IOException e) {
				appInfo = "UNKNOWN";
				LOG.error("IOException: " + e.getMessage());
			} finally {
				try {
					if (fis != null)
						fis.close();
				} catch (IOException e) {
					LOG.error("Cannot close the inputStream: " + e.getMessage());
				}
			}
		}
		return appInfo;
	}

	private Method findDeclaredMethod(Object object, String fieldName) {
		Method method = null;
		for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz
				.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod(fieldName, (Class[]) null);
				return method;
			} catch (Exception e) {

			}
		}

		return null;
	}
}
