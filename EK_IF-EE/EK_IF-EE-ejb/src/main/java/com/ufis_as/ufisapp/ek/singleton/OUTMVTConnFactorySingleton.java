package com.ufis_as.ufisapp.ek.singleton;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tibco.tibjms.TibjmsConnectionFactory;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.InterfaceMqConfig;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;

/**
 * 
 * @author lma
 * OUTMVTConnFactorySingleton is different from a typical ConnFactorySingleton as it listens to the AMQ and sends the message out to TIBCO.
 *
 */
/*@Singleton
@Startup*/
public class OUTMVTConnFactorySingleton {

	private static final Logger LOG = LoggerFactory.getLogger(OUTMVTConnFactorySingleton.class);

	@Resource(name = HpUfisAppConstants.DEFAULT_JNDICONFIGNAME)
	private String configFile;

	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;
	
	private Connection activeMqConn;
	private Connection tibcoConn;
	
	private boolean isAmqOn;
	private boolean isTibcoOn;
	
	private InterfaceMqConfig fromMqConfig;
	private InterfaceMqConfig toMqConfig;
	
	boolean enabledFromQ;
	boolean enabledToQ;

	@PostConstruct
	public void initCache() {
		List<InterfaceConfig> interfaceConfigs = entStartupInitSingleton.getInterfaceConfigs();
		
		if(interfaceConfigs.isEmpty()) {
			LOG.warn("There is no configuration for AMQ and TIBCO. The message will not be sent out.");
			return;
		}
		
		InterfaceConfig interfaceConfig = interfaceConfigs.get(0);
		
		//From Q [AMQ]
		fromMqConfig = interfaceConfig.getFromMq();
		enabledFromQ = Boolean.parseBoolean(fromMqConfig.getEnabled());
		if(enabledFromQ) {
			LOG.debug("'FROM Q' config : {}", fromMqConfig.getUrl());
			boolean connected = connectActiveMQ(fromMqConfig.getUrl(), fromMqConfig.getUserName(), fromMqConfig.getPassword());
			setAmqOn(connected);
		}
		
		//To Q [TIBCO]
		toMqConfig = interfaceConfig.getToMq();
		enabledToQ = Boolean.parseBoolean(toMqConfig.getEnabled());
		if(enabledToQ) {
			LOG.debug("'TO Q' config : {}", toMqConfig.getUrl());
			boolean connected = connectTibco(toMqConfig.getUrl(), toMqConfig.getUserName(), toMqConfig.getPassword());
			setTibcoOn(connected);
		}
		
	}
	
	public InterfaceMqConfig getFromMqConfig() {
		return fromMqConfig;
	}
	
	public InterfaceMqConfig getToMqConfig() {
		return toMqConfig;
	}
	
	public boolean isEnabledFromQ() {
		return enabledFromQ;
	}
	
	public boolean isEnabledToQ() {
		return enabledToQ;
	}
	
	@PreDestroy
	private void destroy() {
		try {
			if (tibcoConn != null) {
				tibcoConn.close();
			}
			if (activeMqConn != null) {
				activeMqConn.close();
			}
		} catch (JMSException e) {
			LOG.error("JMSException {}", e.getMessage());
		}
	}
	
	private boolean connectActiveMQ(String url, String user, String password) {

		try {
			ConnectionFactory activeMqConnFactory = new ActiveMQConnectionFactory(
					user, password, "failover:(" + url + ")");
			activeMqConn = activeMqConnFactory.createConnection();
			activeMqConn.start();
			LOG.info("!!!!connect to active mq successed");
		} catch (Exception e) {
			LOG.error("!!!!ERROR,connect to active mq error : {}", e);
			return false;
		}

		return true;
	}

	private boolean connectTibco(final String url, final String user, final String password) {
		try {
			TibjmsConnectionFactory tibcoConnFactory = new TibjmsConnectionFactory(url);
			/* set heartbeat interval to detect network issues */
			//Tibjms.setPingInterval(10);
			/*  fault-tolerant switch */
			//Tibjms.setExceptionOnFTSwitch(true);
			
			tibcoConnFactory.setConnAttemptCount(10);
			tibcoConnFactory.setConnAttemptDelay(1000);
			tibcoConnFactory.setConnAttemptTimeout(1000);
			tibcoConnFactory.setReconnAttemptCount(1000000000);
			tibcoConnFactory.setReconnAttemptDelay(1000);
			tibcoConnFactory.setReconnAttemptTimeout(1000);
			
			/* create the connection */
			tibcoConn = tibcoConnFactory.createConnection(user, password);
//			tibcoConn.setExceptionListener(new ExceptionListener(){
//
//				@Override
//				public void onException(JMSException jmse) {
//					if (tibcoConn != null) {
//						try {
//							tibcoConn.close();
//						} catch (Exception e) {
//							LOG.error("Exception: {}", jmse.getLinkedException().getMessage());
//						}
//					}
//					connectTibco(url, user, password);
//				}
//				
//			});
//			tibcoConn.start();
			LOG.info("!!!!connect to TIBCO successed");
		} catch (Exception e) {
			LOG.error("!!!!ERROR,connect to TIBCO error : {}", e);
			return false;
		}

		return true;
	}
	
	public boolean reconnectTIBCO() {
		if (enabledToQ) {
			try {
				LOG.info("Closing TIBCO connection and reconnecting again...");
				if (tibcoConn != null) {
					tibcoConn.close();
					setTibcoOn(false);
				}
				LOG.debug("'TO Q' config : {}", toMqConfig.getUrl());
				boolean connected = connectTibco(toMqConfig.getUrl(), toMqConfig.getUserName(), toMqConfig.getPassword());
				setTibcoOn(connected);
				LOG.info("Reconnecting TIBCO is {}!!!", connected ? "successful" : "failed");
				return connected;
			} catch (JMSException e) {
				LOG.error("JMSException on reconnecting TIBCO {}", e.getMessage());
			}
		}

		return false;
	}

	public Connection getActiveMqConnect() {
		return activeMqConn;
	}
	
	public Connection getTibcoConnect() {
		return tibcoConn;
	}

	public boolean isAmqOn() {
		return isAmqOn;
	}

	public void setAmqOn(boolean isAmqOn) {
		this.isAmqOn = isAmqOn;
	}

	public boolean isTibcoOn() {
		return isTibcoOn;
	}

	public void setTibcoOn(boolean isTibcoOn) {
		this.isTibcoOn = isTibcoOn;
	}
	
}
