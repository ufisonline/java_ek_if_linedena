//package com.ufis_as.ufisapp.ek.singleton;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.ejb.Stateless;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ufis_as.ufisapp.ek.intf.BlRouterMacsPax;
//import com.ufis_as.ufisapp.server.oldflightdata.eao.BlIrmtabFacade;
//import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbIrmtab;
//
///**
// * Session Bean implementation class PaxProcessSingleton
// */
////@Singleton
////@Startup
////@DependsOn({"irmtabSingleton", "PaxConnFactory"})
//@Stateless
//public class PaxProcessSingleton {
//
//	private static final Logger LOG = LoggerFactory.getLogger(PaxProcessSingleton.class);
//	
//	@EJB
//	private BlIrmtabFacade irmtabFacade;
//	@EJB
//	private BlRouterMacsPax paxRouter;
//	
//	private List<EntDbIrmtab> paxMessages = null;
//	public static long nextUrno = 0;
//
//    public PaxProcessSingleton() {
//    	paxMessages = new ArrayList<>();
//    }
//
//	public void onMessage() {
////		while (true) {
//			try {
//				//paxMessages = irmtabFacade.getPaxMessages();
//				/*if (paxMessages != null && paxMessages.size() > 0) {
//					LOG.info("============ Pax related msg processing START/END =============");
//					for (EntDbIrmtab msg : paxMessages) {
//						long start = System.currentTimeMillis();
//						paxRouter.routeMessage(msg.getData(), null, msg.getStat());
//						// no matter result of message processing, update it to processed
//						irmtabFacade.updateIRMStatus(msg.getId().getUrno(), "P");
//						LOG.info("message processing cost: {} ms", (System.currentTimeMillis() - start));
//					}
//				} 
//				else {
//				// LOG.info("Waiting for MACS-PAX messages, Sleep 1 Second...");
//				// Thread.sleep(1000);
//					LOG.info("no record found in irmtab for [dtfl=PAX-TMP] and [Stat != P] and created at currentday.");
//				}*/
//				
//				LOG.info("============ Pax related msg processing START/END =============");
//				if (nextUrno == 0) {
//					nextUrno = irmtabFacade.getMinPaxUrno();
//				}
//				if (nextUrno != 0) {
//					LOG.info("Message Processing start at: urno = {}", nextUrno);
//					EntDbIrmtab msg = null;
//					// TODO - change the range configurable
//					for (int i = 0; i < 1500; i++) {
//						msg = irmtabFacade.getPaxMsgById(nextUrno);
//						if (msg != null) {
//							if (!"P".equalsIgnoreCase(msg.getStat())) {
//								LOG.debug("{} start", msg.getStat());
//								long start = System.currentTimeMillis();
//								paxRouter.routeMessage(msg.getData(), null, msg.getStat());
//								// no matter result of message processing, update it to processed
//								irmtabFacade.updateIRMStatus(msg.getId().getUrno(), "P");
//								LOG.info("1 {} message processing cost: {} ms",msg.getStat(), (System.currentTimeMillis() - start));
//								LOG.debug("{} end", msg.getStat());
//							}
//							nextUrno++;
//						} else {
//							LOG.debug("No record found in IRMTAB for [dtfl=PAX-TMP] and [urno={}]", nextUrno);
//							//nextUrno++;
//							//continue;
//							nextUrno = 0;
//							break;
//						}
//					}
//					LOG.info("Message Processing end at: urno = {}", nextUrno);
//				} else {
//					LOG.debug("No record found in IRMTAB for [dtfl=PAX-TMP] and [Stat != P] and created at currentday.");
//					nextUrno = 0;
//				}
//			} catch (Exception e) {
//				LOG.error("Exception: {}", e);
//			}
////		}
//	}
//	
//}
