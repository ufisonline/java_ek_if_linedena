package com.ufis_as.ufisapp.ek.timer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.loadSummary.BlHandleUldSummaryBean;

public class EKLoadSummaryTimer {
	@Resource
	private TimerService timerService;
	
	@EJB
	BlHandleUldSummaryBean clsBlUldSummaryBean;
	
	private static final Logger LOG = LoggerFactory
			.getLogger(EKLoadSummaryTimer.class);

	@PostConstruct
	private void construct() {
		createTimers();
	}

	@PreDestroy
	public void initClear() {
		LOG.info("Clear Timers ");
		for (Object obj : timerService.getTimers()) {
			Timer t = (Timer) obj;
			t.cancel();
		}
	}
	
	public void createTimers() {
		/**
		 * - every 2Min
		 * - initial after 1Min
		 */
		timerService.createTimer(1000, 2* 60 * 1000, "create timer");
	}

	@Timeout
	public void timeout(Timer timer) {
		//clsBlUldSummaryBean.triggerUldSummarizer();
	}
}
