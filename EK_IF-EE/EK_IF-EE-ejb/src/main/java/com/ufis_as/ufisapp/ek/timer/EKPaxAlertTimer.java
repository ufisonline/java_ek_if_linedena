package com.ufis_as.ufisapp.ek.timer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.ek.intf.paxAlert.BlHandlePaxAlertBean;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;

/*@Singleton
@Startup
@Lock(LockType.READ)*/
public class EKPaxAlertTimer{
	
	private static final Logger LOG = LoggerFactory.getLogger(EKPaxAlertTimer.class);

	@Resource
	private TimerService timerService;
	@EJB
	private BlHandlePaxAlertBean clsBlHandlePaxAlertBean;
	@EJB
	private EntStartupInitSingleton clsEntStartupInitSingleton;
	
//	private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	private static boolean isCompleted = true;
	
	@PostConstruct
	private void construct() {
		createTimers();
	}

	@PreDestroy
	public void initClear() {
		LOG.info("Clear Timers ");
		if (timerService.getTimers() != null) {
			LOG.info("Size: {}", timerService.getTimers().size());
			for (Object obj : timerService.getTimers()) {
				Timer t = (Timer) obj;
				t.cancel();
			}
		}
	}
	
	public void createTimers() {
		TimerConfig timerConf = new TimerConfig();
		timerConf.setPersistent(false);
		int interval = 0;
		if(clsEntStartupInitSingleton.getTimerInterval() !=0)
			interval = clsEntStartupInitSingleton.getTimerInterval();
		LOG.info("==== Timer set for interval sec <{}>", interval);
		//timerService.createTimer(1000, 5 * 60 * 1000, "create timer");
		timerService.createIntervalTimer(1000, interval * 1000, timerConf);
	}

	@Timeout
	@AccessTimeout(0)
	public void timeout(Timer timer) {
		try {
			// String lastExecStr = null;
			// if (isCompleted) {
			// isCompleted = false;
			LOG.info("==== Trigger Conx Alert Timer ====");
			long start = System.currentTimeMillis();
			// Date executeDate = new Date(start);
			// lastExecStr = formatter.format(executeDate);
			clsBlHandlePaxAlertBean.processConxAlert();
			LOG.debug("Total used: {} ms", (System.currentTimeMillis() - start));
			// isCompleted = true;
			// } else {
			// LOG.debug("Last timer<Start: {}> haven't completed yet, new attempt dropped.",
			// lastExecStr);
			// }
		} catch (Exception e) {
			LOG.error("Timeout Exception: {}", e.getMessage());
		}
	}
	
}
