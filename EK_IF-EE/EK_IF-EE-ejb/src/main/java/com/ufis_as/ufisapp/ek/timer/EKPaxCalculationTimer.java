package com.ufis_as.ufisapp.ek.timer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.transaction.Synchronization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.macs.IPaxDoSummaryRemote;

//@Singleton
//@Startup
public class EKPaxCalculationTimer {
	@Resource
	private TimerService timerService;
	
	@EJB
	private IPaxDoSummaryRemote paxDoSummary;

	private static final Logger LOG = LoggerFactory
			.getLogger(EKPaxCalculationTimer.class);

	@PostConstruct
	private void construct() {
		createTimers();
	}

	@PreDestroy
	public void initClear() {
		LOG.info("Clear Timers ");
		for (Object obj : timerService.getTimers()) {
			Timer t = (Timer) obj;
			t.cancel();
		}
	}

	public void createTimers() {
		// create timer
		timerService.createTimer(600000, 600000, "cteate testing timer");

	}

	@Timeout
     public synchronized void timeout(Timer timer) {
		
		paxDoSummary.DoSummary();

	}

}
