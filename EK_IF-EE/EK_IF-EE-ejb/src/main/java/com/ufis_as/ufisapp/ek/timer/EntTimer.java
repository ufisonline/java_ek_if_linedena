package com.ufis_as.ufisapp.ek.timer;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import javax.ejb.ScheduleExpression;

public class EntTimer {
	private String _timerName = "Empty";
    private String _isEnabled = "False";
    private String _hour = "0";
    private String _min = "0";
    private String _second = "0";
    private String _month = "*";
    private String _year = "*";
    private String _timezone = "UTC";
    private String _dayOfMonth = "*";
    private String _dayOfWeek = "*";
    private String _filterTimeType = "relative";
    private String _from = "0";
    private String _to = "0";
    private String _filterTimeZone = "UTC";
    private Date _lastExecuted;
    private int _timesExecuted;
    private ScheduleExpression _expression = new ScheduleExpression();

    public String getDayOfMonth() {
        return _dayOfMonth;
    }

    public void setDayOfMonth(String _dayOfMonth) {
        if (_dayOfMonth != null) {
            this._dayOfMonth = _dayOfMonth;
        }
    }

    public String getDayOfWeek() {
        return _dayOfWeek;
    }

    public void setDayOfWeek(String _dayOfWeek) {
        if (_dayOfWeek != null) {
            this._dayOfWeek = _dayOfWeek;
        }
    }

    public String getHour() {
        return _hour;
    }

    public void setHour(String _hour) {
        if (_hour != null) {
            this._hour = _hour;
        }
    }

    public String getIsEnabled() {
        return _isEnabled;
    }

    public void setIsEnabled(String _isEnabled) {
        if (_isEnabled != null) {
            this._isEnabled = _isEnabled;
        }
    }

    public String getMin() {
        return _min;
    }

    public void setMin(String _min) {
        if (_min != null) {
            this._min = _min;
        }
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        if (_month != null) {
            this._month = _month;
        }
    }

    public String getSecond() {
        return _second;
    }

    public void setSecond(String _second) {
        if (_second != null) {
            this._second = _second;
        }
    }

    public String getTimerName() {
        return _timerName;
    }

    public void setTimerName(String _timerName) {
        if (_timerName != null) {
            this._timerName = _timerName;
        }
    }

    public String getTimezone() {
        return _timezone;
    }

    public void setTimezone(String _timezone) {
        if (_timezone != null) {
            this._timezone = _timezone;
        }
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        if (_year != null) {
            this._year = _year;
        }
    }

    public String getFilterTimeType() {
        return _filterTimeType;
    }

    public void setFilterTimeType(String _filterTimeType) {
        if (_filterTimeType != null) {
            this._filterTimeType = _filterTimeType;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntTimer other = (EntTimer) obj;
        if (!Objects.equals(this._timerName, other._timerName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this._timerName);
        return hash;
    }

    public String getFrom() {
        return _from;
    }

    public void setFrom(String _from) {
        if (_from != null) {
            this._from = _from;
        }
    }

    public String getTo() {
        return _to;
    }

    public void setTo(String _to) {
        if (_to != null) {
            this._to = _to;
        }
    }

    public Date getLastExecuted() {
		return _lastExecuted;
	}

	public void setLastExecuted(Date _lastExecuted) {
		this._lastExecuted = _lastExecuted;
	}

	public int getTimesExecuted() {
		return _timesExecuted;
	}

	public void setTimesExecuted(int _timesExecuted) {
		this._timesExecuted = _timesExecuted;
	}

	public String getFilterTimeZone() {
        return _filterTimeZone;
    }

    public void setFilterTimeZone(String _filterTimeZone) {
        if (_filterTimeZone != null) {
            this._filterTimeZone = _filterTimeZone;
        }
    }

    public ScheduleExpression getSchedule() {

        /*
         * every sec is not allowed
         */
        if (!getSecond().equals("*")) {
            _expression.second(getSecond());
        }

        _expression.minute(getMin());
        _expression.hour(getHour());

        _expression.dayOfMonth(getDayOfMonth());
        _expression.dayOfWeek(getDayOfWeek());

        _expression.month(getMonth());
        _expression.year(getYear());

        _expression.timezone(getTimezone());
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        _expression.start(cal.getTime());


        return _expression;

    }
	
}
