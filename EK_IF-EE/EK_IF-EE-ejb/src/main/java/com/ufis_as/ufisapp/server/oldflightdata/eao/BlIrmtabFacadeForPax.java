package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbIrmCompositeId;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbIrmtab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @author $Author: $
 * @version $Revision: $
 */
//@Singleton(name = "irmtabSingleton")
@Stateless

//@Lock(LockType.WRITE)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlIrmtabFacadeForPax {

	private static final Logger LOG = LoggerFactory
			.getLogger(BlIrmtabFacadeForPax.class);
	private boolean isAllocated = true;
	private static long irmNextUrno = 0;
	private long ReUrno = 0;
	private Query query = null;
	String dtfl = "";

	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public BlIrmtabFacadeForPax() {

	}


	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateIRMStatus(String stat) {
		// update the existing record for Status
		try {
			query = em
					.createQuery("UPDATE EntDbIrmtab a SET a.stat = :stat WHERE a.id.urno = :urno AND a.id.dtfl = :dtfl");
			query.setParameter("stat", stat);
			query.setParameter("urno", irmNextUrno - 1);
			query.setParameter("dtfl", dtfl);
			query.executeUpdate();
			LOG.debug(
					"Current Irmtab's URNO : <{}>. Updated incorrect input msg as STAT = <{}>",
					irmNextUrno - 1, stat);
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public long storeRawMsg(Message incomingMessage, String dtflStr) {
		long startTime = new Date().getTime();
		try {
			Date utcCdat = null;
			TextMessage msg = null;
			if (HpUfisUtils.isNullOrEmptyStr(dtflStr)) {
				dtfl="Unknown";
			} else {
				dtfl=dtflStr;
			}
			if (incomingMessage != null
					&& incomingMessage instanceof TextMessage) {
				msg = (TextMessage) incomingMessage;
				String messageBody = msg.getText();
				// if first init, get max urno for current interface from irmtab
				// URNO + DTFL as composite key
				if (isAllocated) {
					try {
						query = em.createQuery("SELECT max(a.id.urno) FROM EntDbIrmtab a WHERE a.id.dtfl = :dtfl");
						query.setParameter("dtfl", dtfl);
						irmNextUrno = (query.getSingleResult() == null
								? 0
								: (long) query.getSingleResult());
					} catch (NoResultException e) {
						irmNextUrno = 1;
					} catch (NonUniqueResultException e) {
						// if maximum number not unique consider as maximum
						// found
					}
					irmNextUrno += 1;
					isAllocated = false;
				}

				HpUfisCalendar ufisCal = new HpUfisCalendar(new Date());
				ufisCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC,
						EnumTimeInterval.Hours);
				utcCdat = ufisCal.getTime();

				EntDbIrmtab irmEntity = new EntDbIrmtab();
				
				ReUrno = irmNextUrno;  //  added by 2014-01-22
				// composite pkey
				irmEntity.setId(new EntDbIrmCompositeId(ReUrno, dtfl));
		
				
				irmEntity.setData(messageBody);
				irmEntity.setCdat(utcCdat);
				irmEntity.setQnam(incomingMessage.getJMSDestination()
						.toString());
				em.persist(irmEntity);
				// cache the maximum urno
				irmNextUrno += 1;
				LOG.info("Total Time to insert the record in IRMTAB(ms) :{}", (new Date().getTime() - startTime));
				return (ReUrno);
			}
		} catch (Exception e) {
			// if error, get the max urno over again
			isAllocated = true;
			LOG.error("Cannot insert raw message data: {}", e.getMessage());
		}		
		return 0;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeInvalidMsg(Message incomingMessage, String dtflStr, String statMsg) {
		long start = new Date().getTime();
		try {
			Date utcCdat = null;
			TextMessage msg = null;
			dtfl = dtflStr;
			if (incomingMessage != null && incomingMessage instanceof TextMessage) {
				msg = (TextMessage) incomingMessage;
				String messageBody = msg.getText();
				// if first init, get max urno for current interface from irmtab
				// URNO + DTFL as composite key
				if (isAllocated) {
					try {
						query = em.createQuery("SELECT max(a.id.urno) FROM EntDbIrmtab a WHERE a.id.dtfl = :dtfl");
						query.setParameter("dtfl", dtfl);
						irmNextUrno = (query.getSingleResult() == null ? 0 : (long) query.getSingleResult());
					} catch (NoResultException e) {
						irmNextUrno = 1;
					} catch (NonUniqueResultException e) {
						// if maximum number not unique consider as maximum
						// found
					}
					irmNextUrno += 1;
					isAllocated = false;
				}

				HpUfisCalendar ufisCal = new HpUfisCalendar(new Date());
				ufisCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
				utcCdat = ufisCal.getTime();

				EntDbIrmtab irmEntity = new EntDbIrmtab();
				// composite pkey
				irmEntity.setId(new EntDbIrmCompositeId(irmNextUrno, dtfl));

				irmEntity.setData(messageBody);
				irmEntity.setCdat(utcCdat);
				irmEntity.setQnam(incomingMessage.getJMSDestination().toString());
				irmEntity.setStat(statMsg);
				em.persist(irmEntity);
				// cache the maximum urno
				irmNextUrno += 1;
			}
		} catch (Exception e) {
			// if error, get the max urno over again
			isAllocated = true;
			LOG.error("Cannot insert invalid message data: {}", e.getMessage());
		}
		LOG.info("Total Time to Insert the record in IRMTAB(ms) :{}", (new Date().getTime() - start));
	}
	
	public long getMinPaxUrno() {
		long urno = 0;
		try {
			// current date in UTC
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);
			
			// query
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT MAX(e.id.urno) FROM EntDbIrmtab e ")
					.append(" WHERE e.id.dtfl = :dtfl ")
					.append(" AND to_char(e.cdat, 'yyyyMMdd') = :currentDate")
					.append(" AND e.stat <> :stat")
					.append(" ORDER BY e.cdat");
			Query paxUrnoQuery = em.createQuery(sb.toString());
			paxUrnoQuery.setParameter("dtfl", "PAX-TMP");
			paxUrnoQuery.setParameter("currentDate", ufisCalendar.getCedaDateString());
			paxUrnoQuery.setParameter("stat", "P");
			
//			List testList = paxUrnoQuery;
//			if (testList.size() > 0){
//				long s;
//			}
			
			long start = System.currentTimeMillis();
			urno = (paxUrnoQuery.getSingleResult() == null ? 0 : (long) paxUrnoQuery.getSingleResult());
			LOG.debug("Find first un-processed pax msg from irmtab cost: {}ms", (System.currentTimeMillis() - start));
			LOG.debug("DTFL=PAX-TMP, URNO={}", urno);
		} catch (Exception e) {
			LOG.error("ERROR : {}", e);
		}
		return urno;
	}
	
	public EntDbIrmtab getPaxMsgById(long urno) {
		EntDbIrmtab entity = null;
		try {
			Query paxQuery = em.createQuery(
					"SELECT e FROM EntDbIrmtab e WHERE e.id.dtfl = :dtfl AND e.id.urno = :urno",
					EntDbIrmtab.class);
			paxQuery.setParameter("dtfl", "PAX-TMP");
			paxQuery.setParameter("urno", urno);
			
			Object obj = paxQuery.getSingleResult();
			if (obj != null) {
				entity = (EntDbIrmtab) obj;
			}
		} catch (NoResultException e) {
			LOG.error("No result found");
		} catch (NonUniqueResultException e) {
			LOG.error("Getting duplicate records");
		} catch (Exception e) {
			LOG.error("ERROR : {}", e);
		}
		return entity;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateIRMStatus(long urno, String stat) {
		// update the existing record for Status
		try {
			query = em
					.createQuery("UPDATE EntDbIrmtab a SET a.stat = :stat WHERE a.id.urno = :urno AND a.id.dtfl = :dtfl");
			query.setParameter("stat", stat);
			query.setParameter("urno", urno);
			query.setParameter("dtfl", "PAX-TMP");
			query.executeUpdate();
//			LOG.debug(
//					"Current Irmtab's URNO for IAMS : <{}>. Updated incorrect input msg as STAT = <{}>",
//					irmNextUrno - 1, stat);
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
	}
	
	public long getIrmNextUrno(String dtflStr) {
		if (isAllocated) {
			try {
				query = em.createQuery("SELECT max(a.id.urno) FROM EntDbIrmtab a WHERE a.id.dtfl = :dtfl");
				query.setParameter("dtfl", dtflStr);
				irmNextUrno = (query.getSingleResult() == null ? 0 : (long) query.getSingleResult());
			} catch (NoResultException e) {
				irmNextUrno = 1;
			} catch (NonUniqueResultException e) {
				// if maximum number not unique consider as maximum
				// found
			}
			irmNextUrno += 1;
			isAllocated = false;
		}
		return isAllocated ? irmNextUrno : (irmNextUrno + 1);
	}

	public static long getIrmNextUrno() {
		return irmNextUrno;
	}

	public static void setIrmNextUrno(long irmNextUrno) {
		BlIrmtabFacadeForPax.irmNextUrno = irmNextUrno;
	}
	
	
}
