package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.singleton.EntStartupInitSingleton;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbOrmCompositeId;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbOrmtab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * @author lma
 */
@Singleton(name = "ormtabSingleton")
@Lock(LockType.WRITE)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BlOrmtabFacade {

	private static final Logger LOG = LoggerFactory.getLogger(BlOrmtabFacade.class);
	
	private boolean isAllocated = true;
	private long ormNextUrno = 0;

	@EJB
	private EntStartupInitSingleton entStartupInitSingleton;

	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;
	
	private String dtfl;

	public BlOrmtabFacade() {
	}
	
	@PostConstruct
	private void init() {
		// if first init, get max urno for current interface from ormtab
		// URNO + DTFL as composite key
		if (isAllocated) {
			try {
				Query query = em.createQuery("SELECT max(a.id.urno) FROM EntDbOrmtab a WHERE a.id.dtfl = :dtfl");
				dtfl = entStartupInitSingleton.getDtflStr();
				dtfl = HpUfisUtils.isNullOrEmptyStr(dtfl) ? "Unknown" : dtfl;
				query.setParameter("dtfl", dtfl);
				ormNextUrno = (query.getSingleResult() == null ? 0 : (long) query.getSingleResult());
			} catch (NoResultException e) {
				ormNextUrno = 1;
				LOG.error(e.getMessage());
			} catch (NonUniqueResultException e) {
				// if maximum number not unique consider as maximum found
				LOG.error(e.getMessage());
			}
			ormNextUrno += 1;
			isAllocated = false;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeRawMsg(String outgoingMessage, String dest) {
		LOG.info("Start storing outgoing message into ORMTAB");
		
		long startTime = new Date().getTime();
		if(outgoingMessage == null) {
			LOG.warn("Outgoing raw message is null. The raw message will not be inserted.");
			return;
		}
		
		try {
			HpUfisCalendar ufisCal = new HpUfisCalendar(new Date());
			ufisCal.DateAdd(HpUfisAppConstants.OFFSET_LOCAL_UTC, EnumTimeInterval.Hours);

			EntDbOrmtab entDbOrmtab = new EntDbOrmtab();
			// composite pkey
			entDbOrmtab.setId(new EntDbOrmCompositeId(ormNextUrno, dtfl));
			entDbOrmtab.setData(outgoingMessage);
			entDbOrmtab.setCdat(ufisCal.getTime());
			entDbOrmtab.setQnam(dest);
			em.persist(entDbOrmtab);
			LOG.info("Outgoing message has been inserted into ORMTAB.");
			// cache the maximum urno
			ormNextUrno += 1;
		} catch (Exception e) {
			// if error, get the max urno over again
			isAllocated = true;
			LOG.error("Cannot insert raw message data: {}", e.getMessage());
		}
		
		LOG.info("Total Time to insert the record in ORMTAB(ms) :{}", (new Date().getTime() - startTime));
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateORMStatus(String stat) {
		// update the existing record for Status
		try {
			Query query = em.createQuery("UPDATE EntDbOrmtab a SET a.stat = :stat WHERE a.id.urno = :urno AND a.id.dtfl = :dtfl");
			query.setParameter("stat", stat);
			query.setParameter("urno", ormNextUrno - 1);
			query.setParameter("dtfl", dtfl);
			query.executeUpdate();
			LOG.debug("Current Ormtab's URNO : <{}>. Updated incorrect input msg as STAT = <{}>", ormNextUrno - 1, stat);
		} catch (Exception ex) {
			LOG.error("ERROR : {}", ex);
		}
	}
	
	public long getOrmNextUrno() {
		return ormNextUrno;
	}
	
}
