/*
 * DlAfttabBean.java Created on August 15, 2007, 8:55 AM To change this
 * template, choose Tools | Template Manager and open the template in the
 * editor.
 */
package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.Trimspec;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.CharUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.configuration.HpPaxAlertTimeRangeConfig;
import com.ufis_as.ek_if.rms.entities.EntRTCFlightSearchObject;
import com.ufis_as.ek_if.ufis.IAfttabBean;
import com.ufis_as.exco.ADID;
import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.ek.hp.HpUfisFlightUtils;
import com.ufis_as.ufisapp.lib.EnumTimeInterval;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntFlightSearchObj;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.egds.MSGTYPE;

/**
 * @author usc
 */
@Stateless(name = "AfttabEAO")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DlAfttabBean implements IAfttabBeanLocal, IAfttabBean {

	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	EntityManager em;
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(DlAfttabBean.class);

	/**
	 * Creates a new instance of DlAfttabBean
	 */
	public DlAfttabBean() {

	}

	@Override
	public EntDbAfttab find(Object pk) {
		return (EntDbAfttab) em.find(EntDbAfttab.class, pk);
	}

	@Override
	public EntDbAfttab getFlightByUrno(BigDecimal urno) {
		Query query = em
				.createQuery("SELECT a FROM EntDbAfttab a where a.urno = :urno");
		query.setParameter("urno", urno);
		try {
			return (EntDbAfttab) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord urno<" + urno + "> not found");
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord urno<" + urno + "> not found "
					+ nur.getMessage());
		}
		return null;
	}

	@Override
	public List<EntDbAfttab> getRotationByRkey(Long rkey) {
		Query query = em
				.createQuery("SELECT a FROM EntDbAfttab a where a.rkey = :rkey  order by a.tifa ");
		query.setParameter("rkey", rkey);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			LOG.debug("AftRecord rkey<" + rkey + "> not found");
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord rkey<" + rkey + "> not found "
					+ nur.getMessage());
		}
		return null;
	}

	@Override
	public List<BigDecimal> findSubset(int start, int count) {
		return em.createQuery("SELECT a.urno FROM EntDbAfttab a")
				.setFirstResult(start).setMaxResults(count).getResultList();
	}

	@Override
	public List<BigDecimal> findSubsetBySeason(int start, int count, String seas) {
		return em
				.createQuery(
						"SELECT a.urno FROM EntDbAfttab a WHERE a.seas = '"
								+ seas + "'").setFirstResult(start)
				.setMaxResults(count).getResultList();
	}

	@Override
	public List<BigDecimal> findBySeason(String season) {
		return em.createQuery(
				"SELECT a.urno FROM EntDbAfttab a WHERE a.seas = '" + season
						+ "'").getResultList();
	}

	public List<BigDecimal> findSubsetByTowing(int start, int count,
			String season) {
		return em
				.createQuery(
						"SELECT a.urno FROM EntDbAfttab a WHERE a.ftyp='T' and a.seas='"
								+ season + "'").setFirstResult(start)
				.setMaxResults(count).getResultList();
	}

	public List<BigDecimal> findByTowing(String season) {
		return em.createQuery(
				"SELECT a.urno FROM EntDbAfttab a WHERE a.seas='" + season
						+ "' and a.ftyp='T'").getResultList();
	}

	@Override
	public List<EntDbAfttab> findFlights(String startDate, String endDate) {
		Query query = em.createNamedQuery("EntDbAfttab.findByTifaTifd");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		List<EntDbAfttab> result = query.getResultList();
		LOG.trace("EntDbAfttab records found count {}  ", result.size());
		return result;
	}

	@Override
	public List<EntDbAfttab> findFlights(String startDate, String endDate,
			String uDate) {
		// Query query = _em.createNamedQuery("EntDbPtsInstance.findByFlight");
		Query query = em.createNamedQuery("EntDbAfttab.findByTifaTifdUdat");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setParameter("uDate", uDate);

		List<EntDbAfttab> result = query.getResultList();
		LOG.trace("EntDbAfttab records found count {}  ", result.size());
		return result;
	}

	@Override
	public EntDbAfttab findFlightByUaft(String uaft) {
		EntDbAfttab entity = null;
		Query query = em.createNamedQuery("EntDbAfttab.findByUaft",
				EntDbAfttab.class);
		query.setParameter("urno", uaft);

		List<EntDbAfttab> result = query.getResultList();
		if (result != null && result.size() > 0) {
			entity = result.get(0);
		}
		return entity;
	}

	@Override
	public EntDbAfttab findFlight(String carrier, String fltn, String regn,
			String org3) {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT a FROM EntDbAfttab a WHERE ");

		if (HpUfisUtils.isNotEmptyStr(carrier)) {
			if (carrier.length() == 2) {
				queryStr.append("TRIM(BOTH ' ' FROM a.alc2) = :alc2");
				// queryStr.append(carrier);
				queryStr.append(" AND ");
			} else if (carrier.length() == 3) {
				queryStr.append("a.alc3 = :alc3");
				// queryStr.append(carrier);
				queryStr.append(" AND ");
			}
		}
		if (HpUfisUtils.isNotEmptyStr(fltn)) {
			queryStr.append("TRIM(BOTH ' ' FROM a.fltn) = :fltn");
			// queryStr.append(fltn);
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(org3)) {
			queryStr.append("a.org3 = :org3");
			// queryStr.append(org3);
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(regn)) {
			queryStr.append("TRIM(BOTH ' ' FROM a.regn) = :regn");
			// queryStr.append(regn);
			queryStr.append(" AND ");
		}

		// ftyp
		queryStr.append("a.ftyp = :ftyp");

		// tifa or tifd in range (range: -3 ~ +3, Local to UTC: -4)
		HpUfisCalendar ufisCalendarFrom = new HpUfisCalendar();
		ufisCalendarFrom.DateAdd(-7, EnumTimeInterval.Hours);

		HpUfisCalendar ufisCalendarTo = new HpUfisCalendar();
		ufisCalendarTo.DateAdd(-1, EnumTimeInterval.Hours);
		queryStr.append(" AND ");
		queryStr.append("((a.tifa between :fromDate and :endDate) or (a.tifd between :fromDate and :endDate))");

		Query query = em.createQuery(queryStr.toString());
		if (HpUfisUtils.isNotEmptyStr(carrier) && carrier.length() == 2) {
			query.setParameter("alc2", carrier);
		}
		if (HpUfisUtils.isNotEmptyStr(carrier) && carrier.length() == 3) {
			query.setParameter("alc3", carrier);
		}
		if (HpUfisUtils.isNotEmptyStr(fltn)) {
			query.setParameter("fltn", fltn);
		}
		if (HpUfisUtils.isNotEmptyStr(org3)) {
			query.setParameter("org3", org3);
		}
		if (HpUfisUtils.isNotEmptyStr(regn)) {
			query.setParameter("regn", regn);
		}
		query.setParameter("ftyp", 'O');
		query.setParameter("fromDate", ufisCalendarFrom.getCedaString());
		query.setParameter("endDate", ufisCalendarTo.getCedaString());
		EntDbAfttab flight = null;
		try {
			List<EntDbAfttab> list = query.getResultList();
			if (list != null && list.size() > 0) {
				flight = list.get(0);
			}

		} catch (NoResultException e) {
			LOG.debug("AftRecord  not found");
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord  not Unique" + nur.getMessage());
		}
		return flight;
	}

	@Override
	public EntDbAfttab findFlight(EntDbAfttab dbAfttab) {

		StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT a FROM EntDbAfttab a WHERE ");

		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getAlc2())) {
			// queryStr.append("TRIM(a.alc2) = ");
			queryStr.append("TRIM(BOTH ' ' FROM a.alc2) = ");
			// queryStr.append(dbAfttab.getAlc2());
			queryStr.append(":alc2");
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getAlc3())) {
			queryStr.append("a.alc3 = ");
			// queryStr.append(dbAfttab.getAlc3());
			queryStr.append(":alc3");
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFltn())) {
			// queryStr.append("TRIM(a.fltn) = ");
			queryStr.append("TRIM(BOTH ' ' FROM a.fltn) = ");
			// queryStr.append(dbAfttab.getFltn());
			queryStr.append(":fltn");
			queryStr.append(" AND ");
		}
		if (dbAfttab.getFlns() != null) {
			queryStr.append("a.flns = ");
			// queryStr.append(dbAfttab.getFlns());
			queryStr.append(":flns");
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFlda())) {
			queryStr.append("a.flda = ");
			// queryStr.append(dbAfttab.getFlda());
			queryStr.append(":flda");
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFlut())) {
			queryStr.append("a.flut = ");
			// queryStr.append(dbAfttab.getFlda());
			queryStr.append(":flut");
			queryStr.append(" AND ");
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getOrg3())) {
			queryStr.append("(");
			queryStr.append("a.org3 = ");
			// queryStr.append(dbAfttab.getOrg3());
			queryStr.append(":org3");

			if (HpUfisUtils.isNotEmptyStr(dbAfttab.getVial())) {
				queryStr.append(" OR ");
				queryStr.append("a.vial like ");
				queryStr.append(":vial");
			}
			queryStr.append(")");
			queryStr.append(" AND ");
		}

		// ftyp
		queryStr.append("(a.ftyp = :scheduleType or a.ftyp = :operateType)");
		Query query = em.createQuery(queryStr.toString());

		// set parameters
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getAlc2())) {
			query.setParameter("alc2", dbAfttab.getAlc2());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getAlc3())) {
			query.setParameter("alc3", dbAfttab.getAlc3());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFltn())) {
			query.setParameter("fltn", dbAfttab.getFltn());
		}
		if (dbAfttab.getFlns() != null) {
			query.setParameter("flns", dbAfttab.getFlns());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFlda())) {
			query.setParameter("flda", dbAfttab.getFlda());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getFlut())) {
			query.setParameter("flut", dbAfttab.getFlut());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getOrg3())) {
			query.setParameter("org3", dbAfttab.getOrg3());
		}
		if (HpUfisUtils.isNotEmptyStr(dbAfttab.getVial())) {
			query.setParameter("vial", "%" + dbAfttab.getVial() + "%");
		}
		query.setParameter("scheduleType", 'S');
		query.setParameter("operateType", 'O');

		EntDbAfttab flight = null;
		try {
			List<EntDbAfttab> list = query.getResultList();
			if (list != null && list.size() > 0) {
				flight = list.get(0);
			}

		} catch (NoResultException e) {
			LOG.debug("AftRecord  not found");
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord  not Unique" + nur.getMessage());
		}
		return flight;
	}

	public List<?> getUrnoList() {
		List<?> urnoList = null;

		Query query = em.createNamedQuery("EntDbAfttab.getUrnoList");
		// to add parameter later
		urnoList = (List<?>) query.getResultList();

		return urnoList;
	}

	@Override
	public String getADID(String urno) {
		Character adid = null;

		Query query = em.createNamedQuery("EntDbAfttab.getADID");
		BigDecimal bdUrno = new BigDecimal(urno.trim());
		query.setParameter("urno", bdUrno);
		List<Character> resultList = (List<Character>) query.getResultList();

		if (resultList != null && resultList.size() > 0) {
			adid = resultList.get(0);
		}

		return adid.toString();
	}

	@Override
	public String getUrnoFoDepFlight(String airlineCode, String flightNumber,
			String flightNumberSuffice, String scheduledFlightDatetime) {
		String flightId = null;
		Character charFlightNumberSuffice = ' ';
		if (flightNumberSuffice != null && !" ".equals(flightNumberSuffice)) {
			charFlightNumberSuffice = flightNumberSuffice.charAt(0);
		}
		Query query = em.createNamedQuery("EntDbAfttab.getUrnoForDepFlight");
		query.setParameter("alc2", airlineCode);
		query.setParameter("fltn", flightNumber);
		query.setParameter("flns", charFlightNumberSuffice);
		query.setParameter("stod", scheduledFlightDatetime);

		List<?> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}",
					result.size(), flightId);
			if (result.size() != 1) {
				LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
			}
			flightId = result.get(0).toString();
		}
		return flightId;
	}

	@Override
	public String getUrnoFoArrFlight(String airlineCode, String flightNumber,
			String flightNumberSuffice, String scheduledFlightDatetime) {

		String flightId = null;
		Character charFlightNumberSuffice = ' ';
		if (flightNumberSuffice != null && !" ".equals(flightNumberSuffice)) {
			charFlightNumberSuffice = flightNumberSuffice.charAt(0);
		}
		Query query = em.createNamedQuery("EntDbAfttab.getUrnoForArrFlight");
		query.setParameter("alc2", airlineCode);
		query.setParameter("fltn", flightNumber);
		query.setParameter("flns", charFlightNumberSuffice);
		query.setParameter("stoa", scheduledFlightDatetime);

		List<?> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flightIdMapping by MFL_ID = {}",
					result.size(), flightId);
			if (result.size() != 1) {
				LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
			}
			flightId = result.get(0).toString();
		}
		return flightId;
	}

	@Override
	public EntDbAfttab findUrnoForArr(String flno, String stoa) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKArrDepn");
		query.setParameter("flno", flno);
		query.setParameter("stoa", stoa);
		query.setParameter("adid", 'A');
		query.setParameter("des3", HpEKConstants.EK_HOPO);
		// query.setParameter("uldFtyp", HpEKConstants.ULD_FTYP);
		EntDbAfttab res = null;

		try {
			res = (EntDbAfttab) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findUrnoForDept(String flno, String stod) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKDeptDepn");
		query.setParameter("flno", flno);
		query.setParameter("stod", stod);
		query.setParameter("adid", 'D');
		query.setParameter("org3", HpEKConstants.EK_HOPO);
		// query.setParameter("uldFtyp", HpEKConstants.ULD_FTYP);
		EntDbAfttab res = null;
		try {
			res = (EntDbAfttab) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public BigDecimal findUrnoForNotArr(String flno, String stod) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKNotArrDepn");
		query.setParameter("flno", flno);
		query.setParameter("stod", stod);
		query.setParameter("adid", 'A');
		query.setParameter("org3", HpEKConstants.EK_HOPO);
		// query.setParameter("uldFtyp", HpEKConstants.ULD_FTYP);
		BigDecimal res = null;

		try {
			res = (BigDecimal) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public BigDecimal findUrnoForNotDept(String flno, String stoa) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKNotDeptDepn");
		query.setParameter("flno", flno);
		query.setParameter("stoa", stoa);
		query.setParameter("adid", 'D');
		query.setParameter("des3", HpEKConstants.EK_HOPO);
		// query.setParameter("uldFtyp", HpEKConstants.ULD_FTYP);
		BigDecimal res = null;

		try {
			res = (BigDecimal) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public BigDecimal findUrnoForReturnFlight(String flno, String flightDate,
			String adid) {
		String queryString = "EntDbAfttab.findForEKArrDepn";
		boolean isArr = true;

		if ("D".equalsIgnoreCase(adid)) {
			queryString = "EntDbAfttab.findForEKDepDepn";
			isArr = false;
		}
		Query query = em.createNamedQuery(queryString);
		if (isArr) {
			query.setParameter("stoa", flightDate);
			query.setParameter("des3", HpEKConstants.EK_HOPO);
		} else {
			query.setParameter("stod", flightDate);
			query.setParameter("org3", HpEKConstants.EK_HOPO);
		}
		query.setParameter("flno", flno);
		query.setParameter("adid", 'B');
		BigDecimal res = null;
		try {
			res = (BigDecimal) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightForEKArr(String flno, String stoa) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKArr");
		query.setParameter("flno", flno);
		query.setParameter("stoa", stoa);
		query.setParameter("adid", 'A');
		query.setParameter("des3", HpEKConstants.EK_HOPO);
		EntDbAfttab res = null;

		try {
			res = (EntDbAfttab) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightForEKDept(String flno, String stod) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKDept");
		query.setParameter("flno", flno);
		query.setParameter("stod", stod);
		query.setParameter("adid", 'D');
		query.setParameter("org3", HpEKConstants.EK_HOPO);
		EntDbAfttab res = null;

		try {
			res = (EntDbAfttab) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightByFlda(String flno, String flda,
			String destCode) {
		LOG.debug("Input Flight date (FLDA) : <{}>", flda);
		Query query = em.createNamedQuery("EntDbAfttab.findByFlda");
		query.setParameter("flno", flno);
		query.setParameter("flda", flda);
		query.setParameter("org3", HpEKConstants.EK_HOPO);

		List<EntDbAfttab> resultList = null;
		EntDbAfttab result = null;
		try {
			resultList = query.getResultList();

			// check for destCode
			for (EntDbAfttab obj : resultList) {
				if (destCode.length() > 3
						&& obj.getDes4().equalsIgnoreCase(destCode)) {// is
																		// icao?
					result = obj;
					break;
				} else if ((destCode.length() < 4)
						&& obj.getDes3().equalsIgnoreCase(destCode)) {// is
																		// iata?
					result = obj;
					break;
				} else {
					result = HpUfisFlightUtils.searchDestCodeInVial(obj,
							destCode);
					if (result != null)
						break;
				}
			}
		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return result;
	}

	@Override
	public EntDbAfttab findFlightByFlut(String flno, String flut,
			String destCode) {
		LOG.debug("Input Flight date (FLUT) : <{}>", flut);
		Query query = em.createNamedQuery("EntDbAfttab.findByFlut");
		query.setParameter("flno", flno);
		query.setParameter("flut", flut);
		query.setParameter("org3", HpEKConstants.EK_HOPO);

		List<EntDbAfttab> resultList = null;
		EntDbAfttab result = null;
		try {
			resultList = query.getResultList();

			// check for destCode
			for (EntDbAfttab obj : resultList) {
				if (destCode.length() > 3
						&& obj.getDes4().equalsIgnoreCase(destCode)) {// is
																		// icao?
					result = obj;
					break;
				} else if ((destCode.length() < 4)
						&& obj.getDes3().equalsIgnoreCase(destCode)) {// is
																		// iata?
					result = obj;
					break;
				} else {
					result = HpUfisFlightUtils.searchDestCodeInVial(obj,
							destCode);
					if (result != null)
						break;
				}
			}
		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return result;
	}
	
	@Override
	public List<EntDbAfttab> findAllFlightByTimeRange() {
		List<EntDbAfttab> res = new ArrayList<>();

		HpUfisCalendar startDate = new HpUfisCalendar();
		HpUfisCalendar endDate = new HpUfisCalendar();
		startDate.setTimeZone(HpEKConstants.utcTz);
		endDate.setTimeZone(HpEKConstants.utcTz);

		startDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertFromOffset(),
				EnumTimeInterval.Hours);
		endDate.DateAdd(HpPaxAlertTimeRangeConfig.getPaxAlertToOffset(),
				EnumTimeInterval.Hours);
		LOG.debug("TIFA FromOffset : <{}>  ToOffset : <{}>",
				startDate.getCedaString(), endDate.getCedaString());

		Query query = em
				.createNamedQuery("EntDbAfttab.findForEKByTifaTimeRange");
		query.setParameter("startDate", startDate.getCedaString());
		query.setParameter("endDate", endDate.getCedaString());
		query.setParameter("adid", 'A');// find for arrival flight only
		try {
			res = query.getResultList();
			LOG.debug("Total Arrival Flight for  <{}>", res.size());

		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightForConx(BigDecimal urno) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKConx");
		query.setParameter("urno", urno);
		// query.setParameter("adid", adid);
		EntDbAfttab res = null;
		try {
			res = (EntDbAfttab) query.getSingleResult();

		} catch (NoResultException e) {
			LOG.debug("AftRecord urno <{}> not found", urno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord urno <{}> not found. {}", urno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightForConxByUrno(long urno) {
		Query query = em.createNamedQuery("EntDbAfttab.findForEKConxByUrno");
		query.setParameter("urno", new BigDecimal(urno));
		EntDbAfttab res = null;
		try {
			res = (EntDbAfttab) query.getSingleResult();
			if (res != null)
				LOG.debug("Flight is found URNO : <{}>", urno);
		} catch (NoResultException e) {
			LOG.debug("AftRecord urno <{}> not found", urno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord urno <{}> not found. {}", urno,
					nur.getMessage());
		}
		return res;
	}

	@Override
	public EntDbAfttab findFlightForDeptByFlnoFlutRegn(String flut,
			String flno, String regn) {
		boolean isRegnNull = false;
		String queryString = "EntDbAfttab.findForEKDeptByFlutAndRegn";
		if (HpUfisUtils.isNullOrEmptyStr(regn)) {
			queryString = "EntDbAfttab.findForEKDeptByFlut";
			isRegnNull = true;
		}
		Query query = em.createNamedQuery(queryString);
		query.setParameter("flut", flut);
		query.setParameter("flno", flno);
		if (!isRegnNull)
			query.setParameter("regn", regn);
		query.setParameter("org3", HpEKConstants.EK_HOPO);
		EntDbAfttab res = null;
		try {
			res = (EntDbAfttab) query.getSingleResult();
			if (res != null)
				LOG.debug("Dept Flight URNO : <{}> RKEY: <{}>", res.getUrno(),
						res.getRkey());
		} catch (NoResultException e) {
			LOG.debug("AftRecord flight <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flight <{}> not found. {}", flno,
					nur.getMessage());
		}
		return res;
	}

	public Tuple getUrnoByCriteriaQuery(EntDbAfttab params,
			MSGTYPE egdsMsgType, ADID adid, int fromOffset, int toOffset) {
		// BigDecimal uaft = null;
		HpUfisCalendar ufisCalendarFrom = new HpUfisCalendar();
		HpUfisCalendar ufisCalendarTo = new HpUfisCalendar();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		// CriteriaQuery<BigDecimal> cq = cb.createQuery(BigDecimal.class);
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();

		// from
		Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

		// select
		// cq.select(root.<BigDecimal>get("urno"));
		cq.multiselect(root.get("urno").alias("urno"),
				root.get("tifa").alias("tifa"), root.get("tifd").alias("tifd"),
				root.get("adid").alias("adid"));

		// result value limited filters
		List<Predicate> filters = new LinkedList<>();
		// alc2
		filters.add(cb.equal(root.get("alc2"), HpEKConstants.EK_ALC2));
		// fltn
		if (HpUfisUtils.isNotEmptyStr(params.getFltn())) {
			filters.add(cb.equal(cb.trim(root.<String> get("fltn")),
					params.getFltn()));
		}
		// regn
		filters.add(cb.equal(cb.trim(root.<String> get("regn")),
				params.getRegn()));
		// ftyp
		filters.add(cb.equal(root.get("ftyp"), HpEKConstants.EK_FTYP_OPERATION));

		// range
		ufisCalendarFrom.DateAdd(fromOffset, EnumTimeInterval.Hours);
		ufisCalendarTo.DateAdd(toOffset, EnumTimeInterval.Hours);
		LOG.debug("EGDS: fromOffset={}", ufisCalendarFrom.getCedaString());
		LOG.debug("EGDS: toOffset={}", ufisCalendarTo.getCedaString());

		if (adid != null && egdsMsgType != MSGTYPE.ROUTE) {
			if (MSGTYPE.MVT == egdsMsgType) {
				// time range
				if (ADID.A == adid) {
					filters.add(cb.between(root.<String> get("stoa"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
				} else {
					filters.add(cb.between(root.<String> get("stod"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
				}
			} else if (MSGTYPE.ROUTE == egdsMsgType) {
				// time range
				filters.add(cb.between(root.<String> get("tifd"),
						ufisCalendarFrom.getCedaString(),
						ufisCalendarTo.getCedaString()));
				// atot (actual takeo-ff time)
				// filters.add(cb.equal(root.<String>get("atot"), " "));
				filters.add(cb.isNull(cb.trim(root.<String> get("atot"))));
				// aobt (actual off-block time
				// filters.add(cb.equal(root.<String>get("aobt"), " "));
				filters.add(cb.isNull(cb.trim(root.<String> get("aobt"))));
			} else {
				if (ADID.A == adid) {
					// time range
					filters.add(cb.between(root.<String> get("tifa"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
					if (MSGTYPE.FUEL == egdsMsgType) {
						// aldt
						filters.add(cb.isNotNull(cb.trim(root
								.<String> get("aldt"))));
					}
				} else {
					// time range
					filters.add(cb.between(root.<String> get("tifd"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
					if (MSGTYPE.FUEL == egdsMsgType) {
						filters.add(cb.isNull(cb.trim(root.<String> get("atot"))));
						filters.add(cb.isNull(cb.trim(root.<String> get("aobt"))));
					}
				}
			}
		} else {
			// (adid = A And tifa between x and y) or (adid = D And tifd between
			// x and y)
			Predicate p = cb.and(cb.or(cb.and(
					cb.equal(root.get("adid"), HpEKConstants.ADID_A),
					cb.between(root.<String> get("tifa"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString())), cb.and(
					cb.equal(root.get("adid"), HpEKConstants.ADID_D),
					cb.between(root.<String> get("tifd"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()))));
			filters.add(p);
		}

		cq.where(cb.and(filters.toArray(new Predicate[0])));
		Query query = em.createQuery(cq);
		// List<BigDecimal> result = query.getResultList();
		Tuple result = null;
		List<Tuple> results = query.getResultList();
		if (results == null || results.size() == 0) {
			LOG.debug("EGDS: Cannot find flight from afttab");
			LOG.debug("CriteriaQuery: {}",
					query.unwrap(org.hibernate.Query.class).getQueryString());
			// LOG.debug("CriteriaQuery: {}", cq);
		} else if (results.size() != 1) {
			LOG.debug("EGDS: More than 1 record found for flight from afttab");
			LOG.debug("CriteriaQuery: {}",
					query.unwrap(org.hibernate.Query.class).getQueryString());
			// LOG.debug("CriteriaQuery: {}", cq);
		} else {
			result = results.get(0);
		}
		return result;
	}

	// @Override
	public String getUrnoFoFlDtOrg(String flno, String org3, String des3,
			String flightDate) {
		String flightId = null;
		// Character charFlightNumberSuffice = ' ';
		// if (flightNumberSuffice != null && !" ".equals(flightNumberSuffice))
		// {
		// charFlightNumberSuffice = flightNumberSuffice.charAt(0);
		// }
		if (org3.length() > 3) {
			Query query1 = em.createNamedQuery("EntDbAfttab.findByFldtOrg4");
			// query1.setParameter("alc2", airlineCode);
			// query1.setParameter("fltn", flightNumber);
			// query1.setParameter("flns", charFlightNumberSuffice);
			query1.setParameter("flno", flno);
			query1.setParameter("org4", org3);
			query1.setParameter("des4", des3);
			query1.setParameter("flda", flightDate);

			List<?> result = query1.getResultList();
			if (result != null && result.size() > 0) {
				LOG.debug("{} record found for flight = {}", result.size(),
						flno);
				if (result.size() != 1) {
					LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
				}
				flightId = result.get(0).toString();
			}

		} else {
			Query query = em.createNamedQuery("EntDbAfttab.findByFldtOrg3");
			// query.setParameter("alc2", airlineCode);
			// query.setParameter("fltn", flightNumber);
			// query.setParameter("flns", charFlightNumberSuffice);
			query.setParameter("flno", flno);
			query.setParameter("org3", org3);
			query.setParameter("des3", des3);
			query.setParameter("flda", flightDate);

			List<?> result = query.getResultList();
			if (result != null && result.size() > 0) {
				LOG.debug("{} record found for flightId = {}", result.size(),
						flno);
				if (result.size() != 1) {
					LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
				}
				flightId = result.get(0).toString();
			}
		}
		return flightId;
	}

	// for LineDeNA
	@Override
	public EntDbAfttab findDepFlightByFlnoAndDateTime(String flightNumber,
			String scheduledFlightDatetime) {
		EntDbAfttab entDbAfttab = null;

		Query query = em
				.createNamedQuery("EntDbAfttab.findDepFlightByFlnoAndDateTime");
		query.setParameter("fltn", flightNumber);
		query.setParameter("stod", scheduledFlightDatetime);

		List<?> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for entDbAfttab", result.size());
			entDbAfttab = (EntDbAfttab) result.get(0);
		}

		return entDbAfttab;
	}

	@Override
	public EntDbAfttab findArrFlightByFlnoAndDateTime(String flightNumber,
			String scheduledFlightDatetime) {
		EntDbAfttab entDbAfttab = null;

		Query query = em
				.createNamedQuery("EntDbAfttab.findArrFlightByFlnoAndDateTime");
		query.setParameter("fltn", flightNumber);
		query.setParameter("stod", scheduledFlightDatetime);

		List<?> result = query.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for entDbAfttab", result.size());
			entDbAfttab = (EntDbAfttab) result.get(0);
		}

		return entDbAfttab;
	}

	@Override
	public EntDbAfttab findFlightForLineDeNA(String stodStoa, String adid,
			String flno, String regn) {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT a FROM EntDbAfttab a WHERE trim(a.ftyp) NOT IN ('N', 'X', 'T') AND trim(a.atot) IS NULL ");
		// queryStr.append("SELECT a FROM EntDbAfttab a WHERE ");

		if (stodStoa == null && adid == null && flno == null && regn == null) {
			return null;
		}

		if (adid != null && !"".equals(adid.trim())) {
			queryStr.append(" AND trim(a.adid) = :adid ");
		}
		if (stodStoa != null && !"".equals(stodStoa.trim())) {
			if (adid != null && "A".equalsIgnoreCase(adid)) {
				queryStr.append(" AND trim(a.stoa) = :stoa");
			} else if (adid != null && !"A".equalsIgnoreCase(adid)) {
				queryStr.append(" AND trim(a.stod) = :stod");
			}
			// }else{
			// queryStr.append(" AND  (trim(a.stoa) = :stoa OR trim(a.stod) = :stod) ");
			// }
		}
		if (regn != null && !"".equals(regn.trim())) {
			queryStr.append(" AND  trim(a.regn) = :regn ");
		}
		if (flno != null && !"".equals(flno.trim())) {
			queryStr.append(" AND  trim(a.flno) = :flno ");
		}

		if (stodStoa == null && adid == null && flno == null) {
			queryStr.append(" (NOT adid='A') ");
		}

		if (stodStoa == null) {
			queryStr.append("  ORDER BY TIFD ");
		}
		// String test =
		// "SELECT a FROM EntDbAfttab a WHERE  trim(a.adid) = :adid  AND  trim(a.regn) = :regn  AND  trim(a.flno) = :flno ";
		// Query query = em.createQuery(test);

		Query query = em.createQuery(queryStr.toString());
		// char tChar = adid.charAt(0);

		if (adid != null && !"".equals(adid.trim())) {
			query.setParameter("adid", adid);
		}
		if (stodStoa != null && !"".equals(stodStoa.trim())) {
			if (adid != null && "A".equalsIgnoreCase(adid)) {
				query.setParameter("stoa", stodStoa);
			} else if (adid != null && "D".equalsIgnoreCase(adid)) {
				query.setParameter("stod", stodStoa);
			} else {
				query.setParameter("stoa", stodStoa.trim());
				query.setParameter("stod", stodStoa.trim());
			}
		}
		if (regn != null && !"".equals(regn.trim())) {
			query.setParameter("regn", regn.trim());
		}
		if (flno != null && !"".equals(flno.trim())) {
			query.setParameter("flno", flno.trim());
		}

		EntDbAfttab flight = null;
		try {
			// EntDbAfttab tab = (EntDbAfttab) query.getSingleResult();
			List<EntDbAfttab> list = query.getResultList();
			if (list != null && list.size() > 0) {
				flight = list.get(0);
			}
		} catch (Exception ex) {
			LOG.debug("erro while search the flight : " + ex);
		}

		// further search
		if (flight == null) {
			// LOG.info("2.2.1 flight not found, select query {}, stodStoa: {}, adid: {}, flno: {}, regn: {}",
			// queryStr, stodStoa, adid, flno, regn);

			String getFlightQueryStr = "";
			String getRkeyQueryStr = "SELECT a.rkey FROM EntDbAfttab a WHERE NOT trim(a.aldt) ='' AND a.adid = 'A' AND trim(a.regn) =:regn AND a.ftyp NOT IN ( 'N', 'X', 'T') ORDER BY ALDT ";
			Query getRkeyQuery = em.createQuery(getRkeyQueryStr);
			String rKey = null;
			if (regn != null && !"".equals(regn.trim())) {
				getRkeyQuery.setParameter("regn", regn.trim());
			}
			List<String> rkeyList = getRkeyQuery.getResultList();
			if (rkeyList != null && rkeyList.size() > 0) {
				rKey = rkeyList.get(0);
			}
			if (rKey != null) {
				getFlightQueryStr = "SELECT a FROM EntDbAfttab a WHERE a.rkey = '"
						+ rKey
						+ "' AND trim(a.atot) = '' AND a.ftyp NOT IN ( 'N', 'X', 'T')";
				Query getFlightQuery = em.createQuery(getFlightQueryStr);
				List<EntDbAfttab> afttabList = getFlightQuery.getResultList();
				if (afttabList != null && afttabList.size() > 0) {
					flight = afttabList.get(0);
				}
			}

			if (flight == null) {
				LOG.info(
						"2.22 futher search, flight not found, select query {}, rKey: {}",
						getFlightQueryStr, rKey);
			}
		}

		return flight;
	}

	@Override
	public Tuple findFlightForLineDeNAX(String stodStoa, String adid,
			String flno, String regn) {
		Tuple result = null;

		if (stodStoa == null && adid == null && flno == null && regn == null) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();

		// from
		Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

		// select
		// cq.select(root.<BigDecimal>get("urno"));
		cq.multiselect(root.get("urno").alias("urno"),
				root.get("aldt").alias("aldt"), root.get("atot").alias("atot"));

		// result value limited filters
		List<Predicate> filters = new LinkedList<>();

		filters.add(cb.not(cb.in(root.get("ftyp")).value('X').value('N')
				.value('T')));
		LOG.debug("ftyp not in ('X','N','T')");
		filters.add(cb.isNull(cb.trim(root.<String> get("atot"))));
		LOG.debug("atot is null");

		if (adid != null && !"".equals(adid.trim())) {
			filters.add(cb.equal(root.get("adid"), adid.charAt(0)));
			LOG.debug("adid, {}", adid.charAt(0));
		}

		if (stodStoa != null && !"".equals(stodStoa.trim())) {
			if (adid != null && "A".equalsIgnoreCase(adid)) {
				filters.add(cb.equal(root.get("stoa"), stodStoa));
				LOG.debug("stoa, {}", stodStoa);
			} else if (adid != null && !"A".equalsIgnoreCase(adid)) {
				filters.add(cb.equal(root.get("stod"), stodStoa));
				LOG.debug("stod, {}", stodStoa);
			}
		}

		if (regn != null && !"".equals(regn.trim())) {
			filters.add(cb.equal(cb.trim(root.<String> get("regn")), regn));
			LOG.debug("regn, {}", regn);
		}
		if (flno != null && !"".equals(flno.trim())) {
			filters.add(cb.equal(cb.trim(root.<String> get("flno")), flno));
			LOG.debug("flno: {}", flno);
		}

		if (stodStoa == null && adid == null && flno == null) {
			filters.add(cb.not(cb.in(root.get("adid")).value('A')));
			LOG.debug("adid not in ('A')");
		}

		if (stodStoa == null) {
			cq.orderBy((cb.asc(root.get("tifd"))));
		}

		cq.where(cb.and(filters.toArray(new Predicate[0])));
		Query query = em.createQuery(cq);

		LOG.debug(" CriteriaQuery: {}", query.unwrap(org.hibernate.Query.class)
				.getQueryString());
		// List<BigDecimal> result = query.getResultList();
		List<Tuple> results = query.getResultList();
		if (results == null || results.size() == 0) {
			LOG.debug(" LineDeNA: Cannot find flight from afttab");
			LOG.debug(" CriteriaQuery: {}", cq.toString());
		} else if (results.size() != 1) {
			LOG.debug(" LineDeNA: More than 1 record found for flight from afttab");
		} else {
			result = results.get(0);
		}

		// further search
		if (result == null) {

			String getFlightQueryStr = "";
			String getRkeyQueryStr = "SELECT a.rkey FROM EntDbAfttab a WHERE NOT trim(a.aldt) ='' AND a.adid = 'A' AND trim(a.regn) =:regn AND a.ftyp NOT IN ( 'N', 'X', 'T') ORDER BY ALDT ";
			Query getRkeyQuery = em.createQuery(getRkeyQueryStr);
			String rKey = null;
			if (regn != null && !"".equals(regn.trim())) {
				getRkeyQuery.setParameter("regn", regn.trim());
			}
			List<String> rkeyList = getRkeyQuery.getResultList();
			if (rkeyList != null && rkeyList.size() > 0) {
				rKey = rkeyList.get(0);
			}
			if (rKey != null) {
				CriteriaBuilder cb2 = em.getCriteriaBuilder();
				CriteriaQuery<Tuple> cq2 = cb.createTupleQuery();

				Root<EntDbAfttab> root2 = cq.from(EntDbAfttab.class);

				cq2.multiselect(root.get("urno").alias("urno"), root
						.get("aldt").alias("tifa"),
						root.get("atot").alias("tifd"));

				List<Predicate> filters2 = new LinkedList<>();

				filters.add(cb.not(cb.in(root.get("ftyp")).value('X')
						.value('N').value('T')));
				filters.add(cb.isNull(cb.trim(root.<String> get("atot"))));
				filters.add(cb.equal(root.get("rkey"), rKey));

				cq2.where(cb2.and(filters2.toArray(new Predicate[0])));
				Query query2 = em.createQuery(cq);
				LOG.debug(" futher search CriteriaQuery: {}",
						query.unwrap(org.hibernate.Query.class)
								.getQueryString());
				// List<BigDecimal> result = query.getResultList();
				List<Tuple> results2 = query2.getResultList();
				if (results2 == null || results2.size() == 0) {
					LOG.debug("2.2.2 LineDeNA: Cannot find flight from afttab by futher search");
					LOG.debug("2.2.2 CriteriaQuery: {}", cq.toString());
				} else if (results2.size() != 1) {
					LOG.debug("2.2.2 LineDeNA: More than 1 record found for flight from afttab by futher search");
					LOG.debug("2.2.2 CriteriaQuery: {}", cq.toString());
				} else {
					result = results.get(0);
				}

			}
		}

		return result;
	}

	// BELT
	@Override
	public String getUrnoFoFlDtOrgExlIsta(String flno, String org3,
			String des3, String flightDate, List<String> istaList) {
		String flightId = null;
		// Character charFlightNumberSuffice = ' ';
		// if (flightNumberSuffice != null && !" ".equals(flightNumberSuffice))
		// {
		// charFlightNumberSuffice = flightNumberSuffice.charAt(0);
		// }
		if (org3.length() > 3) {
			Query query1 = em
					.createNamedQuery("EntDbAfttab.findByFldtOrg4ExlIsta");
			// query1.setParameter("alc2", airlineCode);
			// query1.setParameter("fltn", flightNumber);
			// query1.setParameter("flns", charFlightNumberSuffice);
			query1.setParameter("flno", flno);
			query1.setParameter("org4", org3);
			query1.setParameter("des4", des3);
			query1.setParameter("flda", flightDate);
			query1.setParameter("istaExclude", istaList);
			List<?> result = query1.getResultList();
			if (result != null && result.size() > 0) {
				LOG.debug("{} record found for flight = {}", result.size(),
						flno);
				if (result.size() != 1) {
					LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
				}
				flightId = result.get(0).toString();
			}

		} else {
			Query query = em
					.createNamedQuery("EntDbAfttab.findByFldtOrg3ExlIsta");
			// query.setParameter("alc2", airlineCode);
			// query.setParameter("fltn", flightNumber);
			// query.setParameter("flns", charFlightNumberSuffice);
			query.setParameter("flno", flno);
			query.setParameter("org3", org3);
			query.setParameter("des3", des3);
			query.setParameter("flda", flightDate);
			query.setParameter("istaExclude", istaList);
			List<?> result = query.getResultList();
			if (result != null && result.size() > 0) {
				LOG.debug("{} record found for flightId = {}", result.size(),
						flno);
				if (result.size() != 1) {
					LOG.debug("the mapping flight ID between ufis and interfaceresult should be one !!!");
				}
				flightId = result.get(0).toString();
			}
		}
		return flightId;
	}

	@Override
	public EntDbAfttab findFlightForUldDept(String flno, String flda) {
		Query query = em.createNamedQuery("EntDbAfttab.findFlightForUldDept");
		query.setParameter("flno", flno);
		query.setParameter("flda", flda);

		try {
			List resultList = query.getResultList();
			return resultList.isEmpty() ? null : (EntDbAfttab) resultList
					.get(0);
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return null;
	}

	@Override
	public EntDbAfttab findFlightForUldOrg(String flno, String flda) {
		Query query = em.createNamedQuery("EntDbAfttab.findFlightForUldOrg");
		query.setParameter("flno", flno);
		query.setParameter("flda", flda);
		query.setParameter("org3", HpEKConstants.EK_HOPO);

		try {
			List resultList = query.getResultList();
			return resultList.isEmpty() ? null : (EntDbAfttab) resultList
					.get(0);
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return null;
	}

	@Override
	public EntDbAfttab findFlightForUws(String flno, String flut) {
		Query query = em.createNamedQuery("EntDbAfttab.findFlightForUws");
		query.setParameter("flno", flno);
		query.setParameter("flut", flut);
		query.setParameter("org3", HpEKConstants.EK_HOPO);

		// LOG.debug("UWS Parameter - flno : {}, flut : {}, org3 : {}", flno,
		// flut, HpEKConstants.EK_HOPO);

		try {
			List resultList = query.getResultList();
			return resultList.isEmpty() ? null : (EntDbAfttab) resultList
					.get(0);
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return null;
	}

	@Override
	public String getUrnoFoFlDtAdidExlIsta(String flno, String des3,
			String flightDate, List<String> istaList) {
		String flightId = null;
		Query query1 = em.createNamedQuery("EntDbAfttab.findByFldtAdidExlIsta");
		query1.setParameter("flno", flno);
		query1.setParameter("flda", flightDate);
		query1.setParameter("istaExclude", istaList);
		List<EntDbAfttab> result = query1.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flight = {}", result.size(), flno);
			for (EntDbAfttab obj : result) {
				if (des3.length() > 3 && obj.getDes4().equalsIgnoreCase(des3)) {
					return obj.getUrno().toString();
				} else if ((des3.length() < 4)
						&& obj.getDes3().equalsIgnoreCase(des3)) {
					return obj.getUrno().toString();
				} else {
					int index = 1; // first char is space
					long vianValue = Long.parseLong(obj.getVian().trim());
					if (vianValue > 0) {
						for (int i = 0; i < vianValue; i++) {
							LOG.debug("VIAL index:" + index);
							if (obj.getVial().substring(index, index + 3)
									.equalsIgnoreCase(des3)) {
								LOG.debug(
										"Destination matched with VIAL at index: {}",
										index);
								return obj.getUrno().toString();
							}
							index += 120;
						}
					} else {
						LOG.debug("No Via to be mapped with");
						return null;
					}
				}
			}
		} else {
			LOG.debug("{} record found for flight = {}", result.size(), flno);
			return null;
		}
		return flightId;
	}

	@Override
	public String getUrnoByFldtAdid(String flno, String des3, String flightDate) {
		String flightId = null;
		Query query1 = em.createNamedQuery("EntDbAfttab.findByFldtAdid");
		query1.setParameter("flno", flno);
		query1.setParameter("flda", flightDate);
		List<EntDbAfttab> result = query1.getResultList();
		if (result != null && result.size() > 0) {
			LOG.debug("{} record found for flight = {}", result.size(), flno);
			for (EntDbAfttab obj : result) {
				if (des3.length() > 3 && obj.getDes4().equalsIgnoreCase(des3)) {
					return obj.getUrno().toString();
				} else if ((des3.length() < 4)
						&& obj.getDes3().equalsIgnoreCase(des3)) {
					return obj.getUrno().toString();
				} else {
					int index = 1; // first char is space
					long vianValue = Long.parseLong(obj.getVian().trim());
					if (vianValue > 0) {
						for (int i = 0; i < vianValue; i++) {
							LOG.debug("VIAL index:" + index);
							if (obj.getVial().substring(index, index + 3)
									.equalsIgnoreCase(des3)) {
								LOG.debug(
										"Destination matched with VIAL at index: {}",
										index);
								return obj.getUrno().toString();
							}
							index += 120;
						}
					} else {
						LOG.debug("No Via to be mapped with");
						return null;
					}
				}
			}
		} else {
			LOG.debug("{} record found for flight = {}", result.size(), flno);
			return null;
		}
		return flightId;
	}

	// For PROVEO
	@Override
	public BigDecimal getUrnoByFilterQuery(EntDbAfttab params, ADID adid,
			short fromOffset, short toOffset) {
		// BigDecimal uaft = null;
		long startTime = new Date().getTime();
		HpUfisCalendar ufisCalendarFrom = new HpUfisCalendar();
		HpUfisCalendar ufisCalendarTo = new HpUfisCalendar();
		HpUfisCalendar ufisCurrentCalendar = new HpUfisCalendar();
		BigDecimal result = null;
		try {
			ufisCalendarFrom.setTime(ufisCalendarFrom.getCurrentUTCTime());
			ufisCalendarTo.setTime(ufisCalendarTo.getCurrentUTCTime());
			ufisCurrentCalendar
					.setTime(ufisCurrentCalendar.getCurrentUTCTime());
			// LOG.info("ufisCalendarFrom:"+ufisCalendarFrom.getCedaString()+"  ufisCalendarTo:"+ufisCalendarTo.getCedaString()+"  ufisCurrentCalendar:"+ufisCurrentCalendar.getCedaString());
			long currTimeInMillis = ufisCurrentCalendar.getTimeInMillis();
			// LOG.info("Current Time:" + ufisCurrentCalendar.getCedaString());
			// LOG.info("Current Time In Millis:" + currTimeInMillis);
			CriteriaBuilder cb = em.getCriteriaBuilder();
			// CriteriaQuery cq = cb.createQuery();
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();

			// fromO
			Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

			// select
			// cq.select(root.<BigDecimal>get("urno"));
			// cq.select(root.get("urno").alias("urno"));
			cq.multiselect(root.get("urno").alias("urno"), root.get("stoa")
					.alias("stoa"), root.get("stod").alias("stod"));
			// result value limited filters
			List<Predicate> filters = new LinkedList<>();
			// LOG.info("FLNO:" + params.getFlno());
			// flno
			if (HpUfisUtils.isNotEmptyStr(params.getFlno())) {
				// filters.add(cb.equal(cb.trim(root.<String> get("flno")),
				// params.getFlno()));
				filters.add(cb.equal(cb.trim(root.<String> get("flno")), params
						.getFlno().trim()));
			}

			filters.add(cb.not(cb.in(root.get("ftyp")).value('T').value('G')
					.value('X').value('N')));

			// range
			ufisCalendarFrom.DateAdd(fromOffset, EnumTimeInterval.Minutes);
			ufisCalendarTo.DateAdd(toOffset, EnumTimeInterval.Minutes);
			// LOG.info("PROVEO: fromOffset={}",
			// ufisCalendarFrom.getCedaString());
			// LOG.info("PROVEO: toOffset={}", ufisCalendarTo.getCedaString());
			LOG.info("ufisCalendarFrom After adding Offset:"
					+ ufisCalendarFrom.getCedaString()
					+ "  ufisCalendarTo after adding Offset:"
					+ ufisCalendarTo.getCedaString());
			if (adid != null) {
				// time range
				if (ADID.A.equals(adid)) {
					filters.add(cb.between(root.<String> get("stoa"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
				} else {
					filters.add(cb.between(root.<String> get("stod"),
							ufisCalendarFrom.getCedaString(),
							ufisCalendarTo.getCedaString()));
				}

			}
			// SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			// cb.diff(root.<String> get("stoa"),sdf.parse(new
			// HpUfisCalendar().getCedaString()));

			cq.where(cb.and(filters.toArray(new Predicate[0])));
			if (ADID.A.equals(adid)) {
				cq.orderBy(cb.desc(root.<String> get("stoa")));
			} else {
				cq.orderBy(cb.desc(root.<String> get("stod")));
			}
			Query query = em.createQuery(cq);
			// List<BigDecimal> result = query.getResultList();
			List<Tuple> results = query.getResultList();
			if (results == null || results.size() == 0) {
				LOG.warn("PROVEO: Cannot find flight from afttab");
				// LOG.info("CriteriaQuery: {}", cq.toString());
			} else if (results.size() > 1) {
				long min = 0;
				long stoaCal = 0;
				long stodCal = 0;
				LOG.info("PROVEO: More than 1 record found for flight from afttab");
				// LOG.info("CriteriaQuery: {}", cq.toString());
				for (int i = 0; i < results.size(); i++) {
					if (ADID.A == adid) {
						// LOG.info("results.get(i).get(1):" +
						// results.get(i).get(1) + "   String:" +
						// results.get(i).get(1).toString());
						stoaCal = new HpUfisCalendar(results.get(i).get(1)
								.toString(), "yyyyMMddHHmmss")
								.getTimeInMillis();
						// LOG.info("STOACAL:" + stoaCal);
						if (i == 0) {
							min = Math.abs(currTimeInMillis - stoaCal);
							result = (BigDecimal) results.get(0).get(0);
						}
						if (Math.abs(currTimeInMillis - stoaCal) < min) {
							min = Math.abs(currTimeInMillis - stoaCal);
							result = (BigDecimal) results.get(i).get(0);
						}
						// LOG.info("MIN:" + min + "  Result:" + result);
					} else if (ADID.D == adid) {
						// LOG.info("results.get(i).get(2):" +
						// results.get(i).get(2) + "   String:" +
						// results.get(i).get(2).toString());
						stodCal = new HpUfisCalendar(results.get(i).get(2)
								.toString(), "yyyyMMddHHmmss")
								.getTimeInMillis();
						// LOG.info("STODCAL:" + stodCal);
						if (i == 0) {
							min = Math.abs(currTimeInMillis - stodCal);
							result = (BigDecimal) results.get(0).get(0);
						}
						if (Math.abs(currTimeInMillis - stodCal) < min) {
							min = Math.abs(currTimeInMillis - stodCal);
							result = (BigDecimal) results.get(i).get(0);
						}
						// LOG.info("MIN:" + min + "  Result:" + result);
					}
				}
				// result = results.get(0);
			} else if (results.size() == 1) {
				LOG.info("PROVEO: 1 record found for flight from afttab");
				result = (BigDecimal) results.get(0).get(0);
			}
			// else {
			// result = results.get(0);
			// }
		} catch (Exception e) {
			LOG.error("Error in Parsing the Current Time to UTC. Exception: "
					+ e);
		}
		LOG.info("Total Time to search for the flight in AFTTAB(ms) :{}",
				(new Date().getTime() - startTime));
		return result;
	}

	// BELT
	@Override
	public String getUrnoByFldaFlnoAdid(EntDbAfttab params, String des3,
			String org3) {
		// BigDecimal uaft = null;
		long startTime = new Date().getTime();
		boolean isMatched=false;
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			// CriteriaQuery cq = cb.createQuery();
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();

			// fromO
			Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

			// select
			// cq.select(root.<BigDecimal>get("urno"));
			// cq.select(root.get("urno").alias("urno"));
			cq.multiselect(root.get("urno").alias("urno"), root.get("des3")
					.alias("des3"), root.get("des4").alias("des4"),
					root.get("vian").alias("vian"),
					root.get("vial").alias("vial"),
					root.get("org3").alias("org3"),
					root.get("org4").alias("0rg4"));
			// result value limited filters
			List<Predicate> filters = new LinkedList<>();
			// LOG.info("FLNO:" + params.getFlno());
			// flno
			if (HpUfisUtils.isNotEmptyStr(params.getFlno())) {
				// filters.add(cb.equal(cb.trim(root.<String> get("flno")),
				// params.getFlno()));
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("flno")),
						params.getFlno().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getFlda())) {
				filters.add(cb.equal(root.<String> get("flda"),
						params.getFlda()));
			}
			if (params.getAdid() != null) {
				filters.add(cb.notEqual(root.<Character> get("adid"),
						'B'));
			}
			filters.add(cb.not(cb.in(root.get("ftyp")).value('T').value('G')
					.value('X').value('N')));

			cq.where(cb.and(filters.toArray(new Predicate[0])));
			cq.orderBy(cb.desc(root.<String> get("stod")));
			Query query = em.createQuery(cq);
			// List<BigDecimal> result = query.getResultList();
			List<Tuple> results = query.getResultList();
			if (results == null || results.size() == 0) {
				LOG.warn("BELT: Cannot find flight from afttab");
				// LOG.info("CriteriaQuery: {}", cq.toString());
			} else if (results.size() > 0) {
				LOG.info("BELT: {} records found for flight from afttab.",results.size());
				// LOG.info("CriteriaQuery: {}", cq.toString());
				for (int i = 0; i < results.size(); i++) {
					if (HpUfisUtils.isNullOrEmptyStr(org3)
							&& HpUfisUtils.isNullOrEmptyStr(des3)) {
						return results.get(i).get(0).toString();						
					}else if(!HpUfisUtils.isNullOrEmptyStr(org3)){
						if (org3.length() > 3
								&& results.get(i).get(6).equals(org3)) {
							if (!HpUfisUtils.isNullOrEmptyStr(des3)) {							
								isMatched=lookForDest(results.get(i),des3);
								if(isMatched)
								{
									LOG.info(
											"Total Time to search for the flight in AFTTAB(ms) :{}",
											(new Date().getTime() - startTime));
									return results.get(i).get(0).toString();
								}
								else{
									LOG.error("Dest not matched.");
								}
							}
							else{
							LOG.info(
									"Total Time to search for the flight in AFTTAB(ms) :{}",
									(new Date().getTime() - startTime));
							return results.get(i).get(0).toString();
							}
						} else if ((org3.length() < 4)
								&& results.get(i).get(5).equals(org3)) {
							if (!HpUfisUtils.isNullOrEmptyStr(des3)) {							
								isMatched=lookForDest(results.get(i),des3);
								if(isMatched)
								{
									LOG.info(
											"Total Time to search for the flight in AFTTAB(ms) :{}",
											(new Date().getTime() - startTime));
									return results.get(i).get(0).toString();
								}
								else{
									LOG.error("Dest not matched.");
								}
							}
							else{
							LOG.info(
									"Total Time to search for the flight in AFTTAB(ms) :{}",
									(new Date().getTime() - startTime));
							return results.get(i).get(0).toString();
							}
						}
						else {
							int index = 1; // first char is space
							long vianValue = 0;
							if (results.get(i).get(3) == null) {
								vianValue = 0;
							} else {
								vianValue = Long.parseLong(results.get(i)
										.get(3).toString().trim());
							}
							if (vianValue > 0) {
								for (int j = 0; j < vianValue; j++) {
									LOG.debug("VIAL index:" + index);
									if (org3.trim().length() == 4) {
										if (results.get(i).get(4).toString()
												.substring(index, index + 4)
												.equalsIgnoreCase(org3.trim())) {
											LOG.debug(
													"Origin matched with VIAL at index: {}",
													index);
											if (!HpUfisUtils.isNullOrEmptyStr(des3)) {							
												isMatched=lookForDest(results.get(i),des3);
												if(isMatched)
												{
													LOG.info(
															"Total Time to search for the flight in AFTTAB(ms) :{}",
															(new Date().getTime() - startTime));
													return results.get(i).get(0).toString();
												}
												else{
													LOG.error("Dest not matched.");
												}
											}else{
											LOG.info(
													"Total Time to search for the flight in AFTTAB(ms) :{}",
													(new Date().getTime() - startTime));
											return results.get(i).get(0)
													.toString();
											}
										}
									} else if (org3.trim().length() == 3) {
										if (results.get(i).get(4).toString()
												.substring(index, index + 3)
												.equalsIgnoreCase(org3.trim())) {
											LOG.debug(
													"Origin matched with VIAL at index: {}",
													index);
											if (!HpUfisUtils.isNullOrEmptyStr(des3)) {							
												isMatched=lookForDest(results.get(i),des3);
												if(isMatched)
												{
													LOG.info(
															"Total Time to search for the flight in AFTTAB(ms) :{}",
															(new Date().getTime() - startTime));
													return results.get(i).get(0).toString();
												}
												else{
													LOG.error("Dest not matched.");
												}
											}
											else{
											LOG.info(
													"Total Time to search for the flight in AFTTAB(ms) :{}",
													(new Date().getTime() - startTime));
											return results.get(i).get(0)
													.toString();
											}
										}
									}
									index += 120;
								}
							} else {
								LOG.debug("No Via to be mapped with");
								// return null;
							}
						}
					}
					else if (!HpUfisUtils.isNullOrEmptyStr(des3)) {						
						isMatched=lookForDest(results.get(i),des3);
						if(isMatched)
						{
							LOG.info(
									"Total Time to search for the flight in AFTTAB(ms) :{}",
									(new Date().getTime() - startTime));
							return results.get(i).get(0).toString();
						}
						else{
							LOG.error("Dest not matched.");
						}
					}					
				}

			}
			// result = results.get(0);
			// else {
			// result = results.get(0);
			// }
		} catch (Exception e) {
			LOG.error("Error finding the flight in AFTTAB. Exception: {}" + e);
		}
		LOG.info("Total Time to search for the flight in AFTTAB(ms) :{}",
				(new Date().getTime() - startTime));
		return null;
	}

	// for EK-RTC
	@Override
	public BigDecimal getUrnoForEKRTC(
			EntRTCFlightSearchObject entRTCFlightSearchObject) {

		BigDecimal urno = null;
		StringBuilder queryStr = new StringBuilder();

		LOG.debug(
				"Start to search flight for RTC, flno {}, adid {}, stoa {}, stod {}, org3 {}, des3 {}, regn {}",
				entRTCFlightSearchObject.getFlno(),
				entRTCFlightSearchObject.getAdid(),
				entRTCFlightSearchObject.getStoa(),
				entRTCFlightSearchObject.getStod(),
				entRTCFlightSearchObject.getOrg3(),
				entRTCFlightSearchObject.getDes3(),
				entRTCFlightSearchObject.getRegn());

		if (entRTCFlightSearchObject != null) {

			switch (entRTCFlightSearchObject.getOri()) {
			case "1":
			case "2":

				if ("B".equalsIgnoreCase(entRTCFlightSearchObject.getAdid())) {
					if (entRTCFlightSearchObject.getStod() != null) {

						queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stod = :stod AND a.adid = 'B' AND a.ftyp NOT IN ('N','X','T','G') ORDER BY a.depn DESC ");
					} else if (entRTCFlightSearchObject.getStoa() != null) {

						queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stoa = :stoa AND a.adid = 'B' AND a.ftyp NOT IN ('N','X','T','G') ORDER BY a.depn DESC ");
					}
				} else if ("D".equalsIgnoreCase(entRTCFlightSearchObject
						.getAdid())) {
					queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stod = :stod AND a.adid = 'D' AND a.ftyp NOT IN ('N','X','T','G') ");
				} else if ("A".equalsIgnoreCase(entRTCFlightSearchObject
						.getAdid())) {
					queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stoa = :stoa AND a.adid = 'A' AND a.ftyp NOT IN ('N','X','T','G') ");
				} else {
					LOG.warn(
							"not expected adid value, adid: {}, flight searching will not be perform",
							entRTCFlightSearchObject.getAdid());
					return urno;
				}

				break;
			case "3":

				if (entRTCFlightSearchObject.getStod() != null) {
					queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stod = :stod AND a.adid = 'D' AND trim(a.regn) = :regn AND a.ftyp NOT IN ('N','X') ");
				} else if (entRTCFlightSearchObject.getStoa() != null) {
					queryStr.append("SELECT a.urno FROM EntDbAfttab a WHERE trim(a.flno) = :flno AND a.stoa = :stoa AND a.adid = 'A' AND trim(a.regn) = :regn AND a.ftyp NOT IN ('N','X') ");
				}

				break;
			}

			Query query = em.createQuery(queryStr.toString());
			LOG.debug("successful create query: {}", queryStr.toString());

			query.setParameter("flno", entRTCFlightSearchObject.getFlno());

			switch (entRTCFlightSearchObject.getOri()) {
			case "1":
			case "2":

				if ("B".equalsIgnoreCase(entRTCFlightSearchObject.getAdid())) {
					if (entRTCFlightSearchObject.getStod() != null) {
						query.setParameter("stod",
								entRTCFlightSearchObject.getStod());
					} else if (entRTCFlightSearchObject.getStoa() != null) {
						query.setParameter("stoa",
								entRTCFlightSearchObject.getStoa());
					}

				} else if ("D".equalsIgnoreCase(entRTCFlightSearchObject
						.getAdid())) {
					query.setParameter("stod",
							entRTCFlightSearchObject.getStod());
				} else if ("A".equalsIgnoreCase(entRTCFlightSearchObject
						.getAdid())) {
					query.setParameter("stoa",
							entRTCFlightSearchObject.getStoa());

				} else {
					LOG.warn(
							"not expected adid value, adid: {}, flight searching will not be perform",
							entRTCFlightSearchObject.getAdid());

					return urno;
				}

				break;
			case "3":

				if (entRTCFlightSearchObject.getStod() != null) {
					query.setParameter("stod",
							entRTCFlightSearchObject.getStod());
					query.setParameter("regn",
							entRTCFlightSearchObject.getRegn());
				} else if (entRTCFlightSearchObject.getStoa() != null) {
					query.setParameter("stoa",
							entRTCFlightSearchObject.getStoa());
					query.setParameter("regn",
							entRTCFlightSearchObject.getRegn());
				}

				break;
			}

			LOG.debug("parameter set");
			List<BigDecimal> urnoList = query.getResultList();

			LOG.debug("result get");

			if (urnoList != null && urnoList.size() > 0) {
				urno = urnoList.get(0);
				LOG.debug("flight found");

			} else {
				LOG.debug("flight not found");

			}

		} else {
			LOG.warn("flight serching object is null , pls check !!!");

		}

		return urno;
	}

	@Override
	public List<EntDbAfttab> findAllFlightByTimeRange(HpUfisCalendar startDate,
			HpUfisCalendar endDate) {
		List<EntDbAfttab> res = new ArrayList<>();
		LOG.debug("Timer in AFTTAB UTC From : <{}>  To : <{}>",
				startDate.getCedaString(), endDate.getCedaString());

		Query query = em
				.createNamedQuery("EntDbAfttab.findByTifaTifdTimeRange");
		query.setParameter("startDate", startDate.getCedaString());
		query.setParameter("endDate", endDate.getCedaString());
		try {
			res = query.getResultList();
			LOG.debug("Total flights : <{}>", res.size());

		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return res;
	}

	// ACTS-ODS
	@Override
	public String getUrnoByFldaFlnoOrgDes(EntDbAfttab params, String des3,
			String org3) {
		// BigDecimal uaft = null;
		boolean isMatched = false;
		long startTime = new Date().getTime();
		Character adid = null;
		if (!HpUfisUtils.isNullOrEmptyStr(des3)
				&& !HpUfisUtils.isNullOrEmptyStr(org3)) {
			if (des3.equalsIgnoreCase(org3)
					&& des3.equalsIgnoreCase(HpUfisAppConstants.EK_HOPO)) {
				adid = ADID.B.name().charAt(0);
			} else if (des3.equalsIgnoreCase(HpUfisAppConstants.EK_HOPO)) {
				adid = ADID.A.name().charAt(0);
			} else if (org3.equalsIgnoreCase(HpUfisAppConstants.EK_HOPO)) {
				adid = ADID.D.name().charAt(0);
			}
		}
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			// CriteriaQuery cq = cb.createQuery();
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();

			// fromO
			Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

			// select
			// cq.select(root.<BigDecimal>get("urno"));
			// cq.select(root.get("urno").alias("urno"));
			cq.multiselect(root.get("urno").alias("urno"), root.get("des3")
					.alias("des3"), root.get("des4").alias("des4"),
					root.get("vian").alias("vian"),
					root.get("vial").alias("vial"),
					root.get("org3").alias("org3"),
					root.get("org4").alias("0rg4"));
			// result value limited filters
			List<Predicate> filters = new LinkedList<>();
			// LOG.info("FLNO:" + params.getFlno());
			// flno
			if (HpUfisUtils.isNotEmptyStr(params.getFlno())) {
				// filters.add(cb.equal(cb.trim(root.<String> get("flno")),
				// params.getFlno()));
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("flno")),
						params.getFlno().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getFlda())) {
				filters.add(cb.equal(root.<String> get("flda"),
						params.getFlda()));
			}
			if (adid != null && HpUfisUtils.isNotEmptyStr(adid.toString())) {
				filters.add(cb.equal(root.<String> get("adid"), adid));
			}
			filters.add(cb.not(cb.in(root.get("ftyp")).value('T').value('G')
					.value('X').value('N')));

			cq.where(cb.and(filters.toArray(new Predicate[0])));
			cq.orderBy(cb.desc(root.<String> get("stod")));

			Query query = em.createQuery(cq);
			// List<BigDecimal> result = query.getResultList();
			List<Tuple> results = query.getResultList();
			if (results == null || results.size() == 0) {
				LOG.warn("ACTS: Cannot find flight from afttab");
				// LOG.info("CriteriaQuery: {}", cq.toString());
			} else if (results.size() > 0) {
				LOG.info("ACTS: More than 1 record found for flight from afttab");
				// LOG.info("CriteriaQuery: {}", cq.toString());
				for (int i = 0; i < results.size(); i++) {
					if (HpUfisUtils.isNullOrEmptyStr(org3)
							&& HpUfisUtils.isNullOrEmptyStr(des3)) {
						return results.get(i).get(0).toString();
					}
					if (!HpUfisUtils.isNullOrEmptyStr(org3)
							&& (((org3.length() > 3) && results.get(i).get(6)
									.equals(org3)) || ((org3.length() < 4) && results
									.get(i).get(5).equals(org3)))) {
						isMatched = lookForDest(results.get(i), des3);
						if (isMatched) {
							LOG.info(
									"Total Time to search for the flight in AFTTAB(ms) :{}",
									(new Date().getTime() - startTime));
							return results.get(i).get(0).toString();
						}
					}// Origin can be in vial
					else {
						int index = 1; // first char is space
						long vianValue = 0;
						if (results.get(i).get(3) == null) {
							vianValue = 0;
						} else {
							vianValue = Long.parseLong(results.get(i).get(3)
									.toString().trim());
						}
						if (vianValue > 0) {
							for (int j = 0; j < vianValue; j++) {
								LOG.debug("VIAL index:" + index);
								if (org3.trim().length() == 4) {
									if (results.get(i).get(4).toString()
											.substring(index, index + 4)
											.equalsIgnoreCase(org3)) {
										LOG.debug(
												"Origin matched with VIAL at index: {}",
												index);
										isMatched = lookForDest(results.get(i),
												des3);
										if (isMatched) {
											LOG.info(
													"Total Time to search for the flight in AFTTAB(ms) :{}",
													(new Date().getTime() - startTime));
											return results.get(i).get(0)
													.toString();
										}
									}
								} else if (org3.trim().length() == 3) {
									if (results.get(i).get(4).toString()
											.substring(index, index + 3)
											.equalsIgnoreCase(org3)) {
										LOG.debug(
												"Origin matched with VIAL at index: {}",
												index);
										isMatched = lookForDest(results.get(i),
												des3);
										if (isMatched) {
											LOG.info(
													"Total Time to search for the flight in AFTTAB(ms) :{}",
													(new Date().getTime() - startTime));
											return results.get(i).get(0)
													.toString();
										}
									}
								}
								index += 120;
							}
						} else {
							LOG.debug("No Via to be mapped with");
							// return null;
						}
					}
					/*
					 * else { LOG.info(
					 * "Origin of the flight doesnt match with existing records."
					 * ); }
					 */
					/*
					 * if (!HpUfisUtils.isNullOrEmptyStr(org3)) { if
					 * (org3.length() > 3 && results.get(i).get(6).equals(org3))
					 * { LOG.info(
					 * "Total Time to search for the flight in AFTTAB(ms) :{}",
					 * (new Date().getTime() - startTime)); return
					 * results.get(i).get(0).toString(); } else if
					 * ((org3.length() < 4) &&
					 * results.get(i).get(5).equals(org3)) { LOG.info(
					 * "Total Time to search for the flight in AFTTAB(ms) :{}",
					 * (new Date().getTime() - startTime)); return
					 * results.get(i).get(0).toString(); } }
					 */
				}

			}
			// result = results.get(0);
			// else {
			// result = results.get(0);
			// }
		} catch (Exception e) {
			LOG.error("Error finding the flight in AFTTAB. Exception: {}" + e);
		}
		LOG.info("Total Time to search for the flight in AFTTAB(ms) :{}",
				(new Date().getTime() - startTime));
		return null;
	}

	public boolean lookForDest(Tuple tuple, String des3) {
		if (!HpUfisUtils.isNullOrEmptyStr(des3)) {
			if (des3.length() > 3 && tuple.get(2).equals(des3)) {
				/*
				 * LOG.info(
				 * "Total Time to search for the flight in AFTTAB(ms) :{}", (new
				 * Date().getTime() - startTime));
				 */
				// return tuple.get(0).toString();
				return true;

			} else if ((des3.length() < 4) && tuple.get(1).equals(des3)) {
				/*
				 * LOG.info(
				 * "Total Time to search for the flight in AFTTAB(ms) :{}", (new
				 * Date().getTime() - startTime));
				 */
				// return tuple.get(0).toString();
				return true;
			} else {
				int index = 1; // first char is space
				long vianValue = 0;
				if (tuple.get(3) == null) {
					vianValue = 0;
				} else {
					vianValue = Long.parseLong(tuple.get(3).toString().trim());
				}
				if (vianValue > 0) {
					for (int j = 0; j < vianValue; j++) {
						LOG.debug("VIAL index:" + index);
						if (des3.trim().length() == 4) {
							if (tuple.get(4).toString()
									.substring(index, index + 4)
									.equalsIgnoreCase(des3)) {
								LOG.debug(
										"Destination matched with VIAL at index: {}",
										index);
								/*
								 * LOG.info(
								 * "Total Time to search for the flight in AFTTAB(ms) :{}"
								 * , (new Date().getTime() - startTime));
								 */

								return true;
							}
						} else if (des3.trim().length() == 3) {
							if (tuple.get(4).toString()
									.substring(index, index + 3)
									.equalsIgnoreCase(des3)) {
								LOG.debug(
										"Destination matched with VIAL at index: {}",
										index);
								/*
								 * LOG.info(
								 * "Total Time to search for the flight in AFTTAB(ms) :{}"
								 * , (new Date().getTime() - startTime));
								 */
								return true;
							}
						}
						index += 120;
					}
				} else {
					LOG.debug("No Via to be mapped with");
					// return null;
				}
			}
		}
		return false;
	}

	@Override
	public List<EntDbAfttab> findAllFlightByTimeRange(int from, int to) {
		return null;
	}

	// OPERA
	@Override
	public EntDbAfttab findFlightForReturn(String flno, String flightDate) {
		Query query = em.createNamedQuery("EntDbAfttab.findForReturnFlt");
		query.setParameter("flno", flno);
		query.setParameter("stoa", flightDate);
		query.setParameter("stod", flightDate);
		query.setParameter("adid", 'B');
		query.setParameter("org3", HpEKConstants.EK_HOPO);
		query.setParameter("des3", HpEKConstants.EK_HOPO);
		List<EntDbAfttab> resultList = null;
		EntDbAfttab result = null;
		try {
			resultList = query.getResultList();
			if (resultList.size() > 0)
				result = resultList.get(0);
		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return result;
	}

	// OPERA
	@Override
	public EntDbAfttab findFlightFromVial(String flno, String flightDate,
			String org3, String des3) {
		Query query = em.createNamedQuery("EntDbAfttab.findFromVial");
		query.setParameter("flno", flno);
		query.setParameter("org3", "%" + org3 + "%");
		query.setParameter("des3", "%" + des3 + "%");
		query.setParameter("stoa", flightDate);
		query.setParameter("stod", flightDate);
		query.setParameter("adidA", 'A');
		query.setParameter("adidD", 'D');

		List<EntDbAfttab> resultList = null;
		EntDbAfttab result = null;
		try {
			resultList = query.getResultList();
			if (resultList.size() > 0)
				result = resultList.get(0);
		} catch (Exception ex) {
			LOG.error("!!!!ERROR retrieving flight records in afttab");
		}
		return result;
	}

	@Override
	public EntDbAfttab findRampTransferFlight(String flno,
			HpUfisCalendar startDate, HpUfisCalendar endDate) {
		String startDateString = startDate.getCedaString();
		String endDateString = endDate.getCedaString();

		Query query = em.createNamedQuery("EntDbAfttab.findRampTransferFlight");
		query.setParameter("flno", flno);
		query.setParameter("startDate", startDateString);
		query.setParameter("endDate", endDateString);

		LOG.debug(
				"findRampTransferFlight => flno : {}, startDate : {}, endDate : {}",
				flno, startDateString, endDateString);

		try {
			List resultList = query.getResultList();
			return resultList.isEmpty() ? null : (EntDbAfttab) resultList
					.get(0);
		} catch (NoResultException e) {
			LOG.debug("AftRecord flno <{}> not found", flno);
		} catch (NonUniqueResultException nur) {
			LOG.error("AftRecord flno <{}> not found. {}", flno,
					nur.getMessage());
		}
		return null;
	}

	/*
	 * common method : used by LCM logic to find a flight in AFTTAB using below
	 * parameters flno - mandatory flda - mandatory adid - optional(enum A,B,D)
	 * either of org and des is mandatory. des - optional(look into vial if
	 * exist and not match with des3) adid - optional regn - optional fltFrom -
	 * optional(flight stoa/stod starting from in Minutes) fltTo - optional (To
	 * stoa/stod time in Minutes) ftyp not in (N,X,T,G)
	 */
	@Override
	public BigDecimal findFlightByMultipleParams(EntFlightSearchObj params,
			boolean viaSearch, String msgType, int fltFrom, int fltTo) {
		long startTime = new Date().getTime();
		BigDecimal result = BigDecimal.ZERO;
		;
		try {
			if (HpUfisUtils.isNullOrEmptyStr(params.getFLNO())) {
				LOG.error("Insufficient data to search for flight in AFTTAB. Please provide flight number.");
				return result;
			}

			/*
			 * if (typeSearch.equalsIgnoreCase("FULL")) { if
			 * (HpUfisUtils.isNullOrEmptyStr(params.getORG3()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getALC3()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getDES3()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLDA()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLIGHT_DAY()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLNO()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLNS()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLTN()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getFLUT()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getREGN()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getSTOA()) ||
			 * HpUfisUtils.isNullOrEmptyStr(params.getSTOD()) ||
			 * (CharUtils.toString(params.getADID()) == null || params
			 * .getADID() == ' ')) { LOG.info(
			 * "Search for Flight cannot proceed in FULL mode as few values are missing."
			 * ); return result; } }
			 */

			HpUfisCalendar ufisCalendarFrom = new HpUfisCalendar();
			HpUfisCalendar ufisCalendarTo = new HpUfisCalendar();

			ufisCalendarFrom.setTime(ufisCalendarFrom.getCurrentUTCTime());
			ufisCalendarTo.setTime(ufisCalendarTo.getCurrentUTCTime());
			LOG.debug("ufisCalendarFrom={}", ufisCalendarFrom.getCedaString());
			LOG.debug("ufisCalendarTo={}", ufisCalendarTo.getCedaString());
			// range
			ufisCalendarFrom.DateAdd(fltFrom, EnumTimeInterval.Minutes);
			ufisCalendarTo.DateAdd(fltTo, EnumTimeInterval.Minutes);
			LOG.info("ufisCalendarFrom After adding Offset:"
					+ ufisCalendarFrom.getCedaString()
					+ "  ufisCalendarTo after adding Offset:"
					+ ufisCalendarTo.getCedaString());
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();
			Root<EntDbAfttab> root = cq.from(EntDbAfttab.class);

			cq.multiselect(root.get("urno").alias("urno"), root.get("org3")
					.alias("org3"), root.get("org4").alias("org4"),
					root.get("des3").alias("des3"),
					root.get("des4").alias("des4"),
					root.get("flda").alias("flda"),
					root.get("regn").alias("regn"),
					root.get("stoa").alias("stoa"),
					root.get("stod").alias("stod"));
			List<Predicate> filters = new LinkedList<>();
			// LOG.info("FLNO:" + params.getFlno());
			// flno
			filters.add(cb.equal(
					cb.trim(Trimspec.BOTH, ' ', root.<String> get("flno")),
					params.getFLNO().trim()));
			if (HpUfisUtils.isNotEmptyStr(params.getFLDA())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("flda")),
						params.getFLDA().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(Character.toString(params.getADID()))
					&& CharUtils.toString(params.getADID()) != null) {
				filters.add(cb.equal(root.<String> get("adid"),
						params.getADID()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getREGN())) {
				filters.add(cb.or(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("regn")),
						params.getREGN().trim()), cb.equal(
						root.<String> get("regn"), "            ")));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getORG3())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("org3")),
						params.getORG3().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getDES3())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("des3")),
						params.getDES3().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getALC3())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("alc3")),
						params.getALC3().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getFLTN())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("fltn")),
						params.getFLTN().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getFLNS())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("flns")),
						params.getFLNS().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getFLUT())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("flut")),
						params.getFLUT().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getSTDT())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("stdt")),
						params.getSTDT().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getSTOA())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("stoa")),
						params.getSTOA().trim()));
			}
			if (HpUfisUtils.isNotEmptyStr(params.getSTOD())) {
				filters.add(cb.equal(
						cb.trim(Trimspec.BOTH, ' ', root.<String> get("stod")),
						params.getSTOD().trim()));
			}
			filters.add(cb.between(root.<String> get("tifd"),
					ufisCalendarFrom.getCedaString(),
					ufisCalendarTo.getCedaString()));
			filters.add(cb.not(cb.in(root.get("ftyp")).value('T').value('G')));

			cq.where(cb.and(filters.toArray(new Predicate[0])));
			cq.orderBy(cb.desc(root.<String> get("regn")));
			Query query = em.createQuery(cq);
			List<Tuple> results = query.getResultList();
			if (results == null || results.size() == 0) {
				LOG.warn("No flight from afttab");
			} else if (results.size() > 0) {
				LOG.info("{} records found for flight from afttab",
						results.size());
				params.setFLDA((String) results.get(0).get(5));
				// LOG.info("REGN :"+results.get(0).get(6));
				return (BigDecimal) results.get(0).get(0);
			}
		} catch (Exception e) {
			LOG.error("Error in Parsing the Current Time to UTC. Exception: "
					+ e);
		}
		LOG.info("Total Time to search for the flight in AFTTAB(ms) :{}",
				(new Date().getTime() - startTime));
		return result;
	}

	@Override
	public BigDecimal getFlightByFldaAdid(String flda,Character adid, String flno) {
		/*Query query = em
				.createQuery("SELECT a.urno FROM EntDbAfttab a where a.flda = :flda and a.adid = :adid ");*/
		
		// added by BTR 
		Query query = em
				.createQuery("SELECT a.urno FROM EntDbAfttab a where trim(a.flno) = :flno and a.flda = :flda and a.adid = :adid");
		
		query.setParameter("flno", flno);
		query.setParameter("flda", flda);
		query.setParameter("adid", adid);
		
		try {
			return (BigDecimal) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("AftRecord not found, SELECT a.urno FROM EntDbAfttab a where trim(a.flno) = {} and a.flda = {} and a.adid = {}", flno,flda,adid);
		} catch (NonUniqueResultException nur) {
			LOG.debug(" more than one AftRecord  found, SELECT a.urno FROM EntDbAfttab a where  trim(a.flno) = {} and a.flda = {} and a.adid = {}, {}", flno, flda,adid,nur);
		}
		return null;
	}

	@Override
	public BigDecimal getUrno(EntDbAfttab criteria) {
		// TODO Auto-generated method stub
		return null;
	}
}
