package com.ufis_as.ufisapp.server.oldflightdata.eao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbApttab;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DlApttabBean implements IApttabBeanLocal{
	
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	EntityManager em;
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(DlAfttabBean.class);

	/**
	 * Creates a new instance of DlAfttabBean
	 */
	public DlApttabBean() {

	}
	
	public String getTDI1(String apc3){
		Query query = em
				.createQuery("SELECT a.tdi1 FROM EntDbApttab a where a.apc3 = :apc3");
		query.setParameter("apc3", apc3);
		try {
			return (String) query.getSingleResult();
		} catch (NoResultException e) {
			LOG.debug("SELECT a.tdi1 FROM EntDbApttab a where a.apc3 = {}",apc3);
		} catch (NonUniqueResultException nur) {
			LOG.debug(" more than one AftRecord  found, SELECT a.tdi1 FROM EntDbApttab a where a.apc3 = {}, {}", apc3,nur);
		}
		return null;
	}

	@Override
	public EntDbApttab findByIcao(String icao) {
		// TODO Auto-generated method stub
		return null;
	}
}
