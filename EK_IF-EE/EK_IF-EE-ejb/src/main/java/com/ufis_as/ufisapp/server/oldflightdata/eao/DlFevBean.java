/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbFevtab;

/**
 * @author $Author$
 * @version $Revision$
 */
@Stateless(name = "DlFevBean")
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class DlFevBean {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlFevBean.class);
    //@PersistenceContext(unitName = HpUfisAppConstants.DEFAULT_PU_CEDA)
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;

    public EntDbFevtab getFevtab(BigDecimal uaft, String stco) {
        EntDbFevtab result = null;
        Query query = _em.createNamedQuery("EntDbFevtab.findByFlid");
        query.setParameter("uaft", uaft);
        //query.setParameter("stnm", stnm);
        query.setParameter("stco", stco);

        List<EntDbFevtab> list = query.getResultList();
        if (list != null && list.size() > 0) {
            LOG.debug("{} record found for EntDbFevtab by uaft={}", list.size(), uaft);
            result = list.get(0);
        }
        return result;
    }
    
    public EntDbFevtab getMacsFlightEvent(String ifid, String stco) {
    	EntDbFevtab result = null;
        Query query = _em.createNamedQuery("EntDbFevtab.findByIfid");
        query.setParameter("intFlid", ifid);
        //query.setParameter("stnm", stnm);
        query.setParameter("stco", stco);

        List<EntDbFevtab> list = query.getResultList();
        if (list != null && list.size() > 0) {
            LOG.debug("{} record found for EntDbFevtab by intFlid={}, stco={}", list.size(), ifid, stco);
            result = list.get(0);
        }
        return result;
    }
    
    public EntDbFevtab getFevtab(String intSystem, String intFlid) {
        EntDbFevtab result = null;
        Query query = _em.createNamedQuery("EntDbFevtab.findByIntFlid");
        query.setParameter("intSystem", intSystem);
        query.setParameter("intFlid", intFlid);

        List<EntDbFevtab> list = query.getResultList();
        if (list != null && list.size() > 0) {
            LOG.debug("{} record found for EntDbFevtab  by intFlid={}", list.size(), intFlid);
            result = list.get(0);
        }
        return result;
    }
    
    public EntDbFevtab findByFltDailyId(String idfd) {
    	EntDbFevtab entity = null;
    	try {
    		Query query = _em.createNamedQuery("EntDbFevtab.findByIdfd");
    		query.setParameter("idfd", idfd);
    		List<EntDbFevtab> list = query.getResultList();
    		if (list != null && list.size() > 0) {
    			if (list.size() == 1) {
					entity = list.get(0);
				} else {
					LOG.warn("No unique records found by: idfd={}", idfd);
				}
    		}
    	} catch (Exception e) {
    		LOG.error("Exception: {}", e.getMessage());
    	}
    	return entity;
    }
    
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void persist(EntDbFevtab entity){
    	_em.persist(entity); 	
    }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EntDbFevtab update(EntDbFevtab entity) {
		try {
			entity = _em.merge(entity);
		} catch (OptimisticLockException Oexc) {
			// getEntityManager().refresh(entity);
			LOG.error("OptimisticLockException Entity:{} , Error:{}", entity,
					Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", entity, exc);
		}
		return entity;
	}

}
