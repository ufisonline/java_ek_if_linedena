package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbJnotab;

/**
 * @author $author:  $
 * @version $version:  $
 */
@Singleton(name = "DlJnotabBean")
@Lock(LockType.WRITE)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DlJnotabBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(DlJnotabBean.class);
	
	@PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
	private EntityManager em;

	public DlJnotabBean() {
	
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public long getNextValueByKey(String keys, int allocateSize) {
		long nextValue = 0;
		Query query = em.createNamedQuery("EntDbJnotab.findByKey");
		query.setParameter("keys", keys);
		try {
			List<EntDbJnotab> list = query.getResultList();
			if (list != null && list.size() > 0) {
				EntDbJnotab entity = list.get(0);
				nextValue = entity.getValue();
				// after fetched next value, update new value
				entity.setValue(entity.getValue() + allocateSize);
				em.merge(entity);
			}
		} catch (Exception e) {
			LOG.error("Cannot get next value by key.");
			LOG.error("Exception: {}", e.getMessage());
		}
		return nextValue;
	}

}
