package com.ufis_as.ufisapp.server.oldflightdata.eao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbXaftab;

@Stateless(name = "DlXaftabBean")
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class DlXaftabBean {

	/**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DlXaftabBean.class);
    
    @PersistenceContext(unitName = HpEKConstants.DEFAULT_PU_EK)
    private EntityManager _em;
    
    public EntDbXaftab findByFltDailyId(String idfd) {
    	EntDbXaftab entity = null;
    	try {
    		Query query = _em.createNamedQuery("EntDbXaftab.findByIdfd");
    		query.setParameter("idfd", idfd);
    		List<EntDbXaftab> list = query.getResultList();
    		if (list != null && list.size() > 0) {
    			if (list.size() == 1) {
					entity = list.get(0);
				} else {
					LOG.warn("No unique records found by: idfd={}", idfd);
				}
    		}
    	} catch (Exception e) {
    		LOG.error("Exception: {}", e.getMessage());
    	}
    	return entity;
    }
    
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void persist(EntDbXaftab entity){
    	_em.persist(entity); 	
    }
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EntDbXaftab update(EntDbXaftab entity) {
		try {
			entity = _em.merge(entity);
		} catch (OptimisticLockException Oexc) {
			LOG.error("OptimisticLockException Entity:{} , Error:{}", entity, Oexc);
		} catch (Exception exc) {
			LOG.error("Exception Entity:{} , Error:{}", entity, exc);
		}
		return entity;
	}
}
