/*
05/03/2013 02:38:55 
*/
CREATE TABLE Pax (
  IntSystem          varchar2(10), 
  IntId              varchar2(12), 
  IntFlId            varchar2(10), 
  IntRefNumber       varchar2(10), 
  Destination        varchar2(3), 
  PaxName            varchar2(30), 
  CabinClass         varchar2(2), 
  BookedClass        varchar2(2), 
  PaxType            char(1), 
  PaxGroupCode       varchar2(10), 
  Handicapped        char(1), 
  UnaccompaniedMinor char(1), 
  PriorityPax        char(1), 
  BoardingPassPrint  char(1), 
  BagTagPrint        char(1), 
  PaxBookingStatus   varchar2(3), 
  TravelledClass     varchar2(2), 
  BagWeight          number(10), 
  BagNoOfPieces      number(10), 
  CheckInDateTime    timestamp, 
  BoardingStatus     char(1), 
  Cancelled          char(1), 
  OffloadedPax       varchar2(2), 
  EtkType            varchar2(2), 
  DOB                date, 
  Nationality        varchar2(3), 
  Gender             char(1), 
  StatusOnBoard      varchar2(3), 
  InfantIndicator    char(1),
  id_HOPO      varchar2(5), 
  Created_Date timestamp, 
  Created_User varchar2(50), 
  Updated_Date timestamp, 
  Updated_Time varchar2(50), 
  Rec_Status   char(1));
COMMENT ON COLUMN Pax.IntId IS 'PaxId - Unique Id from Interface for Pax Details Info';
COMMENT ON COLUMN Pax.CheckInDateTime IS 'Check-In Date and Time';
COMMENT ON COLUMN Pax.StatusOnBoard IS 'FL   -  PASSENGER    
FM  - CREW MEMBER   
DDT- IN-TRANSIT CREW MEMBER   
DDU - IN-TRANSIT PASSENGER   
CR4 - NON CREW (CREW ON CARGO FLT)';
COMMENT ON COLUMN UfisStdInfo.Rec_Status IS 'Status of the record.';
COMMENT ON COLUMN Pax.InfantIndicator IS '‘I’ for infant or blank for adult ';
CREATE TABLE UfisStdInfo (
  id_HOPO      varchar2(5), 
  Created_Date timestamp, 
  Created_User varchar2(50), 
  Updated_Date timestamp, 
  Updated_Time varchar2(50), 
  Rec_Status   char(1));
COMMENT ON COLUMN UfisStdInfo.Rec_Status IS 'Status of the record.
''D'' - Deleted Record';
CREATE TABLE PaxConn (
  IntSystem               varchar2(10), 
  ConnType                char(1), 
  IntId                   varchar2(12), 
  IntFlId                 varchar2(10), 
  IntRefNumber            varchar2(10), 
  AirlineCode             varchar2(3), 
  FlightNumber            varchar2(4), 
  FlightNumberSuffice     char(1), 
  ScheduledFlightDateTime timestamp, 
  BoardPoint              varchar2(3), 
  OffPoint                varchar2(3), 
  BookedClass             varchar2(2), 
  ConnStatus              varchar2(2));
COMMENT ON COLUMN PaxConn.ConnType IS 'Connection Type
I - Inbound Connecting Pax. Looking from Departure Flight Point of View.
    IntFlId is Departure Flight Id.
    AirlineCode + Flight Number + ScheduledDateTime is the Arrival Flight from which Pax is connecting

O - Onward Connecting Pax. Looking from Arrival Flight Point of View.
    IntFlId is Arrival Flight Id.
    AirlineCode + Flight Number + ScheduledDateTime is the Departure Flight to which Pax is connecting';
COMMENT ON COLUMN PaxConn.ConnStatus IS 'Connection Status';
CREATE TABLE ServiceRequest (
  IntSystem    varchar2(10), 
  IntId        varchar2(12), 
  IntFlId      varchar2(10), 
  IntRefNumber varchar2(10), 
  RequestType  varchar2(2), 
  ServiceCode  varchar2(10), 
  ServiceType  varchar2(2), 
  ExtInfo      varchar2(1000));
COMMENT ON COLUMN ServiceRequest.IntSystem IS 'Interface System ID
- e.g. MACS
For EK - Emirate, it is "MACS"';
COMMENT ON COLUMN ServiceRequest.RequestType IS 'Request Type
''O'' - Other Service Information (OSI)
''S'' - Special Service Request (SSR)';
COMMENT ON COLUMN ServiceRequest.ServiceCode IS '
Valid Codes are :  
UMNR  
YP  
VIP  
CIP  
VVIP  
iO  
BLND  
DEAF  
MAAS  
STCR  
WCHC  
WCHR  
WCHS  
WCBD  
WCBW  
WCMP  
DPNA  
MEDA';
COMMENT ON COLUMN ServiceRequest.ServiceType IS '
''A'' Special assistance   
''L'' for loyalty bonus program  
''M'' for meal,   
''P'' for preboarding   
''S'' for seat type  
for other blank';
COMMENT ON COLUMN ServiceRequest.ExtInfo IS '
Remarks in Pax record  
First four characters are standard( consistent) and some examples are listed below  
EXST-  extra seat on seating flights                            
NSST -  no smoking seat                                          
NSSW -   no smoking window seat                                   
RQST  - seat request                                             
SMST -  smoking seat                                             
SMSW-smoking window seat  
VGML-low diet meal';
