package com.ufis_as.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HpBeltConfig {

	private static List<String> afttabIstaExlList = Collections.EMPTY_LIST;
	private static String getLinkTables = "Yes";
	private static String keepLatestMoves = "No";
	private static List<String> firstClassBagList = new ArrayList<String>();
	private static List<String> businessClassBagList = new ArrayList<String>();
	private static List<String> economyClassBagList = new ArrayList<String>();

	public static List<String> getAfttabIstaExlList() {
		return afttabIstaExlList;
	}

	public static void setAfttabIstaExlList(List<Object> afttabIstaExlList) {
		List<String> ObjecToStrList = new ArrayList<String>();
		for (Object excludeEle : afttabIstaExlList) {
			if (excludeEle != null) {
				ObjecToStrList.add(excludeEle.toString());
			}
		}
		HpBeltConfig.afttabIstaExlList = ObjecToStrList;
	}

	public static String getGetLinkTables() {
		return getLinkTables;
	}

	public static void setGetLinkTables(String getLinkTables) {
		HpBeltConfig.getLinkTables = getLinkTables;
	}

	public static String getKeepLatestMoves() {
		return keepLatestMoves;
	}

	public static void setKeepLatestMoves(String keepLatestMoves) {
		HpBeltConfig.keepLatestMoves = keepLatestMoves;
	}

	public static List<String> getFirstClassBagList() {
		return firstClassBagList;
	}

	public static void setFirstClassBagList(List<Object> firstClassBagList) {
		List<String> ObjecToStrList = new ArrayList<String>();
		for (Object firstClass : firstClassBagList) {
			if (firstClass != null) {
				ObjecToStrList.add(firstClass.toString());
			}
		}
		HpBeltConfig.firstClassBagList = ObjecToStrList;
	}

	public static List<String> getBusinessClassBagList() {
		return businessClassBagList;
	}

	public static void setBusinessClassBagList(List<Object> businessClassBagList) {
		List<String> ObjecToStrList = new ArrayList<String>();
		for (Object businessClass : businessClassBagList) {
			if (businessClass != null) {
				ObjecToStrList.add(businessClass.toString());
			}
		}
		HpBeltConfig.businessClassBagList = ObjecToStrList;
	}

	public static List<String> getEconomyClassBagList() {
		return economyClassBagList;
	}

	public static void setEconomyClassBagList(List<Object> economyClassBagList) {
		List<String> ObjecToStrList = new ArrayList<String>();
		for (Object economyClass : economyClassBagList) {
			if (economyClass != null) {
				ObjecToStrList.add(economyClass.toString());
			}
		}
		HpBeltConfig.economyClassBagList = ObjecToStrList;
	}

}
