package com.ufis_as.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;

public class HpCommonConfig {
	
	private HpCommonConfig() {
	}
	
	// dtfl
	public static String dtfl = null;
	
	// irmtab
	public static String irmtab = null;
	
	// connection
	public static boolean isAmqOn = false;
	public static boolean isTibcoOn = false;
	public static Connection activeMqConn = null;
	public static Connection tibcoConn = null;
	
	// notification
	public static String notifyQueue = null;
	public static String notifyTopic = null;
	public static long topicMsgTimeout = 0;
	public static List<String> tables = new ArrayList<>();
	public static Map<String, String> fields = new HashMap<>();
	public static Map<String, String> exclusions = new HashMap<>();
	public static int fromOffset=0;
	public static int toOffset=0;
	
}
