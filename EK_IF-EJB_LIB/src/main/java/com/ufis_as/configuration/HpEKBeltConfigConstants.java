package com.ufis_as.configuration;

public class HpEKBeltConfigConstants {

	public static final String AFTTAB_ISTA_EXCLUDE_TAG = "AFTTAB_ISTA_EXCLUDE";
	public static final String LOGICAL_NAME = "BELT";
	public static final String GET_LINK_TO_TABLES_TAG = "GET_LINK_TABLES";
	public static final String KEEP_LATEST_MOVEMENT_TAG = "KEEP_LATEST_MOVEMENT_ONLY";
	public static final String CON_YES="YES";
	public static final String CON_NO="NO";
	public static final String FIRST_CLASS_BAG_LIST="BAG_CLASS_F";
	public static final String BUSINESS_CLASS_BAG_LIST="BAG_CLASS_J";
	public static final String ECONOMY_CLASS_BAG_LIST="BAG_CLASS_Y";
}
