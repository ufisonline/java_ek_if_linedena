package com.ufis_as.configuration;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;

/**
 *
 * @author fou
 */
public final class HpEKConstants  {
	
	public static final String SYS_SOURCE = "SYS";
	public static final String LOAD_SUMMARY = "LSUM";
	public static final String ULD_SOURCE_SKYCHAIN = "SkyChain";
    public static final String ULD_SOURCE = "AACS";
    public static final String EK_HOPO = "DXB";
    public static final String EK_APC4 = "OMDB";
    public static final String OPERA_SOURCE = "OPERA";
    public static final String OPERA_META_SOURCE = "OPERA/LESA";
    public static final String OPERA_META_SUBTYPE = "PAX";
    public static final String IAMS_SOURCE = "IAMS";
    public static final String ACTS_DATA_SOURCE = "ACTS-ODS";
    public static final String BMS_SOURCE = "BMS";
    public static final String CREW_STAFF_TYPE_CODE= "CREW";
    public static final String RMS_RTC_SOURCE = "RMS-RTC";
    public static final String PAX_ALERT_SOURCE = "PAX-ALERT";
    public static final String FLT_CONX_SOURCE = "FLT_CONX_SUMMARY";
    public static final String JCEDA_SOURCE = "JCEDA";
    public static final String LIDO_SOURCE = "LIDO";
    public static final String EK_LIDO_NOTAMS = "NOTAMS";
    public static final String LOAD_CONTROL_MSG = "LCM";
    public static final String EK_LCM_STRIP = "LCM_STRIP";
    
    public static final String A_INPUT_OFFICE = "AFTN Telex"; 
    public static final String C_INPUT_OFFICE = "Customer local"; 
    public static final String D_INPUT_OFFICE = "DWD" ;
    public static final String I_INPUT_OFFICE = "ISCS"; 
    public static final String L_INPUT_OFFICE = "Lido Helpdesk"; 
    public static final String M_INPUT_OFFICE = "Manual corrected"; 
    public static final String S_INPUT_OFFICE = "SADIS"; 
    public static final String T_INPUT_OFFICE ="SITA Telex";
    
    public static final String RMS_STAFF_TYPE_ROP = "ROP";
    public static final String RMS_STAFF_TYPE_SPHL = "SPHL";
    public static final String RMS_RTC_RESOURCE_TYPE = "EMPLOYEE";
    
    public static final String BELT_SOURCE = "BELT";
    public static final String DEFAULT_BELT_QUEUE = "AAO.HUBMATRIX.BELT.UFIS.BAGSTATUS.BRIDGED";
    public static final String DEFAULT_LCM_STRIP_QUEUE = "AAO.HUBMATRIX.LCM.UFIS.BRIDGED";
    public static final String DEFAULT_LCM_QUEUE = "UFIS_NOTIFY_TELEX";
    public static final String DEFAULT_ACTS_QUEUE="AAO.HUBMATRIX.ACTSODS.FLTFULL.INFO";
    public static final String PROVEO_SOURCE = "PROVEO";
    public static final String DEFAULT_PROVEO_LOC_QUEUE = "AAO.HUBMATRIX.PROVEO.UFIS.LOCATION.BRIDGED";
    public static final String DEFAULT_PROVEO_STATUS_QUEUE = "AAO.HUBMATRIX.PROVEO.UFIS.STATUS.BRIDGED";
    public static final String DeNA_DATA_SOURCE = "DeNA";
    public static final String ENGRTC_DATA_SOURCE = "ENG-RTC";
    public static final String MACS_PAX_DATA_SOURCE = "MACS-PAX";
    public static final String EKRTC_DATA_SOURCE = "EK-RTC";
    //public static final String OPERA_META_TYPE = "Tracking";
    
    public static final String UFIS_APP_KEY = "UFISAppSecretKey"; //16 bytes = 128 bit
    
    public static final String INFO_ID_PAX_ALERT = "TXF"; 
    public static final String LOAD_TYPE_CONFIG = "CFG"; 
    public static final long MAX_CONX_TIME_PAX_ALERT = 75;
    public static final long MIN_CONX_TIME_PAX_ALERT = 60;
    
    // critical connection timing for same and different terminal
    public static final long CONX_ST_CRITICAL = 45;
    public static final long CONX_DT_CRITICAL = 60;
    // short connection timing for same and different terminal
    public static final long CONX_ST_SHORT = 60;
    public static final long CONX_DT_SHORT = 75;
    
    public static final String IRMTAB_STAT_INVALID = "I";
    public static final String BAGTYPE_L = "L";
    public static final String BAGTYPE_T = "T";
    public static final Character ARR_DEP_FALG_A = 'A';
    public static final Character ARR_DEP_FALG_D = 'D';
    
    public static final String MDB_LIDO = "ejb:EK_IF-EE-ear/EK_IF-EE-ejb-1.0-SNAPSHOT/BlUfisTibcoLidoMessageBean!com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB?stateful";
   // public static final String MDB_LIDO = "java:global/EK_IF-EE-ear-1.0-LIDO/EK_IF-EE-ejb-1.0-SNAPSHOT/BlUfisTibcoLidoMessageBean!com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB";
    

    public static final String MDB_BI_UFISTIBCO = "java:global/EK_IF-EE-ear/EK_IF-EE-ejb-1.0-SNAPSHOT/BIUfisTibcoMACSPAXMessageBean!com.ufis_as.ufisapp.ek.eao.IBlUfisTibcoMDB";
    


    //Var for QUERY ULD Basic Data
    public static final String QUERY_ULD_TYPE = "EntDbMdUldType.getAllData";
    /*
     * Default Queue Name for Receive From CEDA
     */

    /*
     * Speciall for the MDB destination we have to set this in the ejb-jar.xml
     * if we change it !
     */
    public static final String DEFAULT_QUEUE_EK_RECV_NAME = HpUfisAppConstants.prefix + "/ufisas/jms/queue/FromCeda";
    public static final String DEFAULT_QUEUE_EK_RECV_FACTORY = HpUfisAppConstants.prefix + "/ufisas/jmsf/queue/fromCedaFactory";

    /*
     * Default Queue Name for Send to CEDA
     */
    public static final String DEFAULT_QUEUE_EK_SEND_NAME = HpUfisAppConstants.prefix + "/ufisas/jms/queue/ToCeda";
    public static final String DEFAULT_QUEUE_EK_SEND_FACTORY = HpUfisAppConstants.prefix + "/ufisas/jmsf/queue/fromCedaFactory";
    
    // for UfisMsgDistributor
    public static final String MDB_UFIS__MSG_DISTIBUTOR = "java:global/UfisMsgDistributor/EK_IF-EE-ejb-1.0-SNAPSHOT/BIUfisMsgDistributorMessageBean!com.ufis_as.ek_if.core.msgdistributor.IBIUfisMsgDistributorMessageBean";
   


    // PU Names
    // if Using for Sats UAT, pls using second one.
    public static final String DEFAULT_PU_EK = "EK-ejbPU";

    public static final String DMIS_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String DMIS_TOW_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    //public static final String EDGS_TIME_FORMAT = "MM/dd/yyyy HH:mm";
    public static final String EDGS_TIME_FORMAT = "dd/MM/yyyy HH:mm";
    public static final String MACS_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    // for EK-RTC
    public static final String EKRTC_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    
    public static final String HOPO = "DXB";
    public static final String EK_ALC2 = "EK";
    public static final String EK_ALC3 = "UAE";
    
    public static DateFormat DENA_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    public static DateFormat MACS_PAX_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    
    
    /**
     * FTYP
     */
    public static final char EK_FTYP_OPERATION = 'O';
    public static final char EK_FTYP_SCHEDULE = 'S';
    public static final char EK_FTYP_CANCEL = 'X';
    public static final char EK_FTYP_TOWING = 'T';
    
    /**
     * ADID
     * A: arrival
     * D: departure
     * B: return or towing
     */
    public static final char ADID_A = 'A';
    public static final char ADID_D = 'D';
    public static final char ADID_B = 'B';
    
    public static final String SPACE = " ";
    
    // Timezone
    public static final TimeZone utcTz = TimeZone.getTimeZone("UTC");
    
    


    // PRIVATE //
    /**
     * The caller references the constants using
     * <tt>HpEKConstants.EMPTY_STRING</tt>, and so on. Thus, the caller
     * should be prevented from constructing objects of this class, by declaring
     * this private constructor.
     */
    private HpEKConstants() {
        //this prevents even the native class from
        //calling this ctor as well :
        throw new AssertionError();
    }
}
