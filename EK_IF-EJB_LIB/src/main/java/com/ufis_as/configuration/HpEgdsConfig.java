package com.ufis_as.configuration;

public class HpEgdsConfig {

	private static int egdsDefFromOffset = 0;
	private static int egdsDefToOffset = 0;

	private static int egdsMvtFromOffset = 0;
	private static int egdsMvtToOffset = 0;
	
	public static int getEgdsDefFromOffset() {
		return egdsDefFromOffset;
	}

	public static void setEgdsDefFromOffset(int egdsDefFromOffset) {
		HpEgdsConfig.egdsDefFromOffset = egdsDefFromOffset;
	}

	public static int getEgdsDefToOffset() {
		return egdsDefToOffset;
	}

	public static void setEgdsDefToOffset(int egdsDefToOffset) {
		HpEgdsConfig.egdsDefToOffset = egdsDefToOffset;
	}

	public static int getEgdsMvtFromOffset() {
		return egdsMvtFromOffset;
	}

	public static void setEgdsMvtFromOffset(int egdsMvtFromOffset) {
		HpEgdsConfig.egdsMvtFromOffset = egdsMvtFromOffset;
	}

	public static int getEgdsMvtToOffset() {
		return egdsMvtToOffset;
	}

	public static void setEgdsMvtToOffset(int egdsMvtToOffset) {
		HpEgdsConfig.egdsMvtToOffset = egdsMvtToOffset;
	}

}
