package com.ufis_as.configuration;
/**
 * Entity class uses to map the config variables 
 * for pax-alert timer's fromOffSet and toOffSet.
 * 
 * @author btr
 *
 */
public class HpPaxAlertTimeRangeConfig {

	//in minutes from LoadPaxSummary 
	private static int paxAlertFrom = -10;
	private static int paxAlertTo = 0;
	
	//in minutes from Afttab
	private static int paxAlertFlightFrom = -10;
	private static int paxAlertFlightTo = 10;
	
	//in hours
	private static int paxAlertInitFrom = -2;
	private static int paxAlertInitTo = 8;
	
	public static int getPaxAlertFromOffset() {
		return paxAlertFrom;
	}

	public static void setPaxAlertFromOffset(int paxAlertFromOffset) {
		HpPaxAlertTimeRangeConfig.paxAlertFrom = paxAlertFromOffset;
	}

	public static int getPaxAlertToOffset() {
		return paxAlertTo;
	}

	public static void setPaxAlertToOffset(int paxAlertToOffset) {
		HpPaxAlertTimeRangeConfig.paxAlertTo = paxAlertToOffset;
	}

	public static int getPaxAlertFlightFrom() {
		return paxAlertFlightFrom;
	}

	public static void setPaxAlertFlightFrom(int paxAlertFlightFrom) {
		HpPaxAlertTimeRangeConfig.paxAlertFlightFrom = paxAlertFlightFrom;
	}

	public static int getPaxAlertFlightTo() {
		return paxAlertFlightTo;
	}

	public static void setPaxAlertFlightTo(int paxAlertFlightTo) {
		HpPaxAlertTimeRangeConfig.paxAlertFlightTo = paxAlertFlightTo;
	}

	public static int getPaxAlertInitFrom() {
		return paxAlertInitFrom;
	}

	public static void setPaxAlertInitFrom(int paxAlertInitFrom) {
		HpPaxAlertTimeRangeConfig.paxAlertInitFrom = paxAlertInitFrom;
	}

	public static int getPaxAlertInitTo() {
		return paxAlertInitTo;
	}

	public static void setPaxAlertInitTo(int paxAlertInitTo) {
		HpPaxAlertTimeRangeConfig.paxAlertInitTo = paxAlertInitTo;
	}
}
