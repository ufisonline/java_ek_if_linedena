package com.ufis_as.configuration;

public class HpProveoConfig {
	private static short afttabRangeFromOffset = -8;
	private static short afttabRangeToOffset = 8;

	public static short getAfttabRangeFromOffset() {
		return afttabRangeFromOffset;
	}
	public static void setAfttabRangeFromOffset(short afttabRangeFromOffset) {
		HpProveoConfig.afttabRangeFromOffset = afttabRangeFromOffset;
	}
	public static short getAfttabRangeToOffset() {
		return afttabRangeToOffset;
	}
	public static void setAfttabRangeToOffset(short afttabRangeToOffset) {
		HpProveoConfig.afttabRangeToOffset = afttabRangeToOffset;
	}

}
