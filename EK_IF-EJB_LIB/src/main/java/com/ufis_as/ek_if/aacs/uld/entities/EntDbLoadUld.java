package com.ufis_as.ek_if.aacs.uld.entities;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.utils.HpUfisUtils;


/**
 * The persistent class for the LOAD_ULDS database table.
 * 
 */
@Entity
@Table(name="LOAD_ULDS")
@NamedQueries({
	@NamedQuery(name = "EntDbLoadUld.findUld", query = "SELECT a FROM EntDbLoadUld a WHERE a.idFlight = :idFlight AND a.uldNumber = :uldNumber AND a.rec_status <> :recStatus"),
	@NamedQuery(name = "EntDbLoadUld.findUldsByIdFlight", query = "SELECT a FROM EntDbLoadUld a WHERE a.idFlight = :idFlight AND a.rec_status = :recStatus"),
	@NamedQuery(name = "EntDbLoadUld.findUniqueConxFlightByIdFlight", query = "SELECT DISTINCT(a.idConxFlight) FROM EntDbLoadUld a WHERE a.idFlight = :idFlight AND a.rec_status = :recStatus"),
	@NamedQuery(name = "EntDbLoadUld.findUldByPk", query = "SELECT a FROM EntDbLoadUld a WHERE a.uldCurrFltno = :uldCurrFltno AND a.uldNumber = :uldNumber and a.fltDate  = To_Date( :fltDate, 'YYYYMMDDhh24miss') AND a.rec_status <> :recStatus ORDER BY a._createdDate DESC"),
	@NamedQuery(name = "EntDbLoadUld.findUldByFlightAndULDNo", query = "SELECT a FROM EntDbLoadUld a WHERE a.uldCurrFltno = :uldCurrFltno AND a.uldNumber = :uldNumber AND (a.rec_status is null OR a.rec_status <> :recStatus) ORDER BY a._createdDate DESC"),
	@NamedQuery(name = "EntDbLoadUld.findUldsByFlight", query = "SELECT a FROM EntDbLoadUld a WHERE a.idFlight = :idFlight AND (a.rec_status is null OR a.rec_status <> :recStatus) ORDER BY a._createdDate DESC")
})
public class EntDbLoadUld extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;
	public static final Logger LOG = LoggerFactory.getLogger(EntDbLoadUld.class);

//	@Id
//	private String id;

	@Column(name="ARR_DEP_FLAG")
	private String arrDepFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONX_FLT_DATE")
	private Date conxFltDate;

	@Column(name="CONX_TO_ULD")
	private String conxToUld;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="ID_CONX_FLIGHT")
	private BigDecimal idConxFlight;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_MD_ULD_ITEMTYPE")
	private String idMdUldItemtype;

	@Column(name="ID_MD_ULD_LOC")
	private String idMdUldLoc;

	@Column(name="ID_MD_ULD_POS")
	private String idMdUldPos;

	@Column(name="ID_MD_ULD_SUBTYPE")
	private String idMdUldSubtype;

	@Column(name="ID_MD_ULD_TYPE")
	private String idMdUldType;

	@Column(name="ID_RAMP_TRANSFER_FLIGHT")
	private String idRampTransferFlight;

	@Column(name="RAMP_TRANSFER_FLAG")
	private String rampTransferFlag;

	@Column(name="RAMP_TRANSFER_FLNO")
	private String rampTransferFlno;

	@Column(name="REC_STATUS")
	private String rec_status;

	@Column(name="SHC_LIST")
	private String shcList;

	@Column(name="SHC_REMARKS")
	private String shcRemarks;

	@Column(name="ULD_CONTOUR_CODE")
	private String uldContourCode;

	@Column(name="ULD_CONTOUR_NUM")
	private String uldContourNum;

	@Column(name="ULD_CONX_FLNO")
	private String uldConxFlno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ULD_CREATE_DATE")
	private Date uldCreateDate;

	@Column(name="ULD_CREATE_USER")
	private String uldCreateUser;

	@Column(name="ULD_CURR_FLNO")
	private String uldCurrFltno;

	@Column(name="ULD_DEST3")
	private String uldDest3;

	@Column(name="ULD_DISP_LOC_CODE")
	private String uldDispLocCode;

	@Column(name="ULD_DISP_LOC_DESC")
	private String uldDispLocDesc;

	@Column(name="ULD_ITEMTYPE")
	private String uldItemtype;

	@Column(name="ULD_LOAD_CAT1")
	private String uldLoadCat1;

	@Column(name="ULD_LOAD_CAT2")
	private String uldLoadCat2;

	@Column(name="ULD_NUMBER")
	private String uldNumber;

	@Column(name="ULD_OWNER")
	private String uldOwner;

	@Column(name="ULD_POSITION")
	private String uldPosition;

	@Column(name="ULD_REMARKS")
	private String uldRemarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ULD_SENT_DATE")
	private Date uldSentDate;

	@Column(name="ULD_SUBTYPE")
	private String uldSubtype;

	@Column(name="ULD_TYPE")
	private String uldType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ULD_UPDATE_DATE")
	private Date uldUpdateDate;

	@Column(name="ULD_UPDATE_USER")
	private String uldUpdateUser;

	@Column(name="ULD_WEIGHT")
	private String uldWeight;

	@Column(name="ULD_WEIGHT_STATUS")
	private String uldWeightStatus;

	public EntDbLoadUld() {
	}
	
	public EntDbLoadUld(EntDbLoadUld obj){
		this.idFlight = obj.idFlight;
		this.idConxFlight = obj.idConxFlight;
		this.uldNumber = obj.uldNumber;
		this.uldCurrFltno = obj.uldCurrFltno;
		this.uldConxFlno = obj.uldConxFlno;
		this.rec_status = obj.rec_status;
		this.uldDest3 = obj.uldDest3;
		this.shcList = obj.shcList;
		this.uldOwner = obj.uldOwner;
		this._createdDate = obj._createdDate;
		this._createdUser = obj._createdUser;
		this._updatedDate = obj._updatedDate;
		this._updatedUser = obj._updatedUser;
		this.uldCreateUser = obj.uldCreateUser;
		this.uldCreateDate = obj.uldCreateDate;
	}

	public String getArrDepFlag() {
		return this.arrDepFlag;
	}

	public void setArrDepFlag(String arrDepFlag) {
		this.arrDepFlag = arrDepFlag;
	}

	public Date getConxFltDate() {
		return this.conxFltDate;
	}

	public void setConxFltDate(Date conxFltDate) {
		this.conxFltDate = conxFltDate;
	}

	public String getConxToUld() {
		return this.conxToUld;
	}

	public void setConxToUld(String conxToUld) {
		this.conxToUld = conxToUld;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public BigDecimal getIdConxFlight() {
		return this.idConxFlight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdMdUldItemtype() {
		return this.idMdUldItemtype;
	}

	public void setIdMdUldItemtype(String idMdUldItemtype) {
		this.idMdUldItemtype = idMdUldItemtype;
	}

	public String getIdMdUldLoc() {
		return this.idMdUldLoc;
	}

	public void setIdMdUldLoc(String idMdUldLoc) {
		this.idMdUldLoc = idMdUldLoc;
	}

	public String getIdMdUldPos() {
		return this.idMdUldPos;
	}

	public void setIdMdUldPos(String idMdUldPos) {
		this.idMdUldPos = idMdUldPos;
	}

	public String getIdMdUldSubtype() {
		return this.idMdUldSubtype;
	}

	public void setIdMdUldSubtype(String idMdUldSubtype) {
		this.idMdUldSubtype = idMdUldSubtype;
	}

	public String getIdMdUldType() {
		return this.idMdUldType;
	}

	public void setIdMdUldType(String idMdUldType) {
		this.idMdUldType = idMdUldType;
	}

	public String getIdRampTransferFlight() {
		return this.idRampTransferFlight;
	}

	public void setIdRampTransferFlight(String idRampTransferFlight) {
		this.idRampTransferFlight = idRampTransferFlight;
	}

	public String getRampTransferFlag() {
		return this.rampTransferFlag;
	}

	public void setRampTransferFlag(String rampTransferFlag) {
		this.rampTransferFlag = rampTransferFlag;
	}

	public String getRampTransferFlno() {
		return this.rampTransferFlno;
	}

	public void setRampTransferFlno(String rampTransferFlno) {
		this.rampTransferFlno = rampTransferFlno;
	}

	public String getRecFlag() {
		return this.rec_status;
	}

	public void setRecFlag(String rec_status) {
		this.rec_status = rec_status;
	}

	public String getShcList() {
		return this.shcList;
	}

	public void setShcList(String shcList) {
		this.shcList = shcList;
	}

	public String getShcRemarks() {
		return this.shcRemarks;
	}

	public void setShcRemarks(String shcRemarks) {
		this.shcRemarks = shcRemarks;
	}

	public String getUldContourCode() {
		return this.uldContourCode;
	}

	public void setUldContourCode(String uldContourCode) {
		this.uldContourCode = uldContourCode;
	}

	public String getUldContourNum() {
		return this.uldContourNum;
	}

	public void setUldContourNum(String uldContourNum) {
		this.uldContourNum = uldContourNum;
	}

	public String getUldConxFlno() {
		return this.uldConxFlno;
	}

	public void setUldConxFlno(String uldConxFlno) {
		this.uldConxFlno = uldConxFlno;
	}

	public Date getUldCreateDate() {
		return this.uldCreateDate;
	}

	public void setUldCreateDate(Date uldCreateDate) {
		this.uldCreateDate = uldCreateDate;
	}

	public String getUldCreateUser() {
		return this.uldCreateUser;
	}

	public void setUldCreateUser(String uldCreateUser) {
		this.uldCreateUser = uldCreateUser;
	}

	public String getUldCurrFltno() {
		return this.uldCurrFltno;
	}

	public void setUldCurrFltno(String uldCurrFltno) {
		this.uldCurrFltno = uldCurrFltno;
	}

	public String getUldDest3() {
		return this.uldDest3;
	}

	public void setUldDest3(String uldDest3) {
		this.uldDest3 = uldDest3;
	}

	public String getUldDispLocCode() {
		return this.uldDispLocCode;
	}

	public void setUldDispLocCode(String uldDispLocCode) {
		this.uldDispLocCode = uldDispLocCode;
	}

	public String getUldDispLocDesc() {
		return this.uldDispLocDesc;
	}

	public void setUldDispLocDesc(String uldDispLocDesc) {
		this.uldDispLocDesc = uldDispLocDesc;
	}

	public String getUldItemtype() {
		return this.uldItemtype;
	}

	public void setUldItemtype(String uldItemtype) {
		this.uldItemtype = uldItemtype;
	}

	public String getUldLoadCat1() {
		return this.uldLoadCat1;
	}

	public void setUldLoadCat1(String uldLoadCat1) {
		this.uldLoadCat1 = uldLoadCat1;
	}

	public String getUldLoadCat2() {
		return this.uldLoadCat2;
	}

	public void setUldLoadCat2(String uldLoadCat2) {
		this.uldLoadCat2 = uldLoadCat2;
	}

	public String getUldNumber() {
		return this.uldNumber;
	}

	public void setUldNumber(String uldNumber) {
		this.uldNumber = uldNumber;
	}

	public String getUldOwner() {
		return this.uldOwner;
	}

	public void setUldOwner(String uldOwner) {
		this.uldOwner = uldOwner;
	}

	public String getUldPosition() {
		return this.uldPosition;
	}

	public void setUldPosition(String uldPosition) {
		this.uldPosition = uldPosition;
	}

	public String getUldRemarks() {
		return this.uldRemarks;
	}

	public void setUldRemarks(String uldRemarks) {
		this.uldRemarks = uldRemarks;
	}

	public Date getUldSentDate() {
		return this.uldSentDate;
	}

	public void setUldSentDate(Date uldSentDate) {
		this.uldSentDate = uldSentDate;
	}

	public String getUldSubtype() {
		return this.uldSubtype;
	}

	public void setUldSubtype(String uldSubtype) {
		this.uldSubtype = uldSubtype;
	}

	public String getUldType() {
		return this.uldType;
	}

	public void setUldType(String uldType) {
		this.uldType = uldType;
	}

	public Date getUldUpdateDate() {
		return this.uldUpdateDate;
	}

	public void setUldUpdateDate(Date uldUpdateDate) {
		this.uldUpdateDate = uldUpdateDate;
	}

	public String getUldUpdateUser() {
		return this.uldUpdateUser;
	}

	public void setUldUpdateUser(String uldUpdateUser) {
		this.uldUpdateUser = uldUpdateUser;
	}

	public String getUldWeight() {
		return this.uldWeight;
	}

	public void setUldWeight(String uldWeight) {
		this.uldWeight = uldWeight;
	}

	public String getUldWeightStatus() {
		return this.uldWeightStatus;
	}

	public void setUldWeightStatus(String uldWeightStatus) {
		this.uldWeightStatus = uldWeightStatus;
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpUfisUtils.isNullOrEmptyStr(_createdUser) ? HpEKConstants.ULD_SOURCE : _createdUser;
		this.dataSource = HpUfisUtils.isNullOrEmptyStr(dataSource) ? HpEKConstants.ULD_SOURCE : dataSource;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.ULD_SOURCE);
       }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntDbLoadUld other = (EntDbLoadUld) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}