package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * @author btr
 * The persistent class for the LOAD_ULD_MOVE database table.
 * 
 */
@Entity
@Table(name="LOAD_ULD_MOVE")
@NamedQueries({
	@NamedQuery(name = "EntDbLoadUldMove.findUldMoveByPk", query = "SELECT a FROM EntDbLoadUldMove a WHERE a.flightNumber = :flightNumber AND a.uldNumber = :uldNumber and a.flightDate  = To_Date( :flightDate, 'YYYYMMDDhh24miss') AND a.rec_status <> :recStatus ORDER BY a._createdDate DESC"),
	@NamedQuery(name = "EntDbLoadUldMove.findUldMoveByidFlightUldNum", query = "SELECT a FROM EntDbLoadUldMove a WHERE a.idFlight = :idFlight AND a.uldNumber = :uldNumber AND a.rec_status <> :recStatus ORDER BY a._createdDate DESC"),
	@NamedQuery(name = "EntDbLoadUldMove.updateDeletedUlds", query ="UPDATE EntDbLoadUldMove a SET a.rec_status = :recStatus WHERE a.id IN (SELECT a.id FROM EntDbLoadUldMove a WHERE a.idLoadUlds = :idLoadUlds AND a.rec_status <> :curStatus)")
	})
public class EntDbLoadUldMove  extends EntHopoAssociated  implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final Logger LOG = LoggerFactory.getLogger(EntDbLoadUldMove.class);
	
/*	@EmbeddedId
	private UldMovePK pkId;
	
	String id;*/
	
	@Column(name="ARR_DEP_FLAG")
	private String arrDepFlag;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DISP_LOC_CODE")
	private String dispLocCode;

	@Column(name="DISP_LOC_DESC")
	private String dispLocDesc;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_LOAD_ULDS")
	private String idLoadUlds;

	@Column(name="ID_MD_ULD_SCAN_ACT")
	private String idMdUldScanAct;

	@Column(name="ID_SCAN_MD_ULD_LOC")
	private String idScanMdUldLoc;

	@Column(name="ID_DISP_MD_ULD_LOC")
	private String idDispMdUldLoc;

	@Column(name="REC_STATUS")
	private String rec_status;

	@Column(name="SCAN_ACTION")
	private String scanAction;

	@Column(name="SCAN_DATE")
	private Date scanDate;

	@Column(name="SCAN_LOC_CODE")
	private String scanLocCode;

	@Column(name="SCAN_LOC_DESC")
	private String scanLocDesc;

	@Column(name="SCAN_USER")
	private String scanUser;

	@Column(name="ULD_SENT_DATE")
	private Date uldSentDate;
	
	//PKey
	@Column(name="ULD_NUMBER")
	private String uldNumber;

	@Column(name="FLIGHT_DATE")
	private Date flightDate;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	public String getUldFltno() {
		return this.flightNumber;
	}

	public void setUldFltno(String uldFltno) {
		this.flightNumber = uldFltno;
	}

	public String getUldNumber() {
		return this.uldNumber;
	}

	public void setUldNumber(String uldNumber) {
		this.uldNumber = uldNumber;
	}

	public Date getFltDate() {
		return this.flightDate;
	}

	public void setFltDate(Date fltDate) {
		this.flightDate = fltDate;
	}


	public EntDbLoadUldMove() {
	}

	public EntDbLoadUldMove(EntDbLoadUldMove obj){
		this.idFlight = obj.idFlight;
		this.idLoadUlds = obj.idLoadUlds;
		this.uldNumber = obj.uldNumber;
		this.flightNumber = obj.flightNumber;
		this.rec_status = obj.rec_status;
	}
	
	public String getIdMdUldScanAct() {
		return idMdUldScanAct;
	}

	public void setIdMdUldScanAct(String idMdUldScanAct) {
		this.idMdUldScanAct = idMdUldScanAct;
	}

	public String getIdScanMdUldLoc() {
		return idScanMdUldLoc;
	}

	public void setIdScanMdUldLoc(String idScanMdUldLoc) {
		this.idScanMdUldLoc = idScanMdUldLoc;
	}

	public String getIdDispMdUldLoc() {
		return idDispMdUldLoc;
	}

	public void setIdDispMdUldLoc(String idDispMdUldLoc) {
		this.idDispMdUldLoc = idDispMdUldLoc;
	}
	
	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this.dataSource = HpEKConstants.ULD_SOURCE;
		this.setHopo( HpEKConstants.EK_HOPO);
		
//		try {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.ULD_SOURCE);
       }
	}

	@PreUpdate
	void onUpdateMaintenance() {
//		try {
			this.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getUpdatedUser() == null) {
           this.setUpdatedUser(HpEKConstants.ULD_SOURCE);
       }
	}

	public String getArrDepFlag() {
		return this.arrDepFlag;
	}

	public void setArrDepFlag(String arrDepFlag) {
		this.arrDepFlag = arrDepFlag;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDispLocCode() {
		return this.dispLocCode;
	}

	public void setDispLocCode(String dispLocCode) {
		this.dispLocCode = dispLocCode;
	}

	public String getDispLocDesc() {
		return this.dispLocDesc;
	}

	public void setDispLocDesc(String dispLocDesc) {
		this.dispLocDesc = dispLocDesc;
	}

/*	public UldMovePK getPkId() {
		return pkId;
	}

	public void setPkId(UldMovePK pkId) {
		this.pkId = pkId;
	}
*/
	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdLoadUlds() {
		return this.idLoadUlds;
	}

	public void setIdLoadUlds(String idLoadUlds) {
		this.idLoadUlds = idLoadUlds;
	}

	public String getRecFlag() {
		return this.rec_status;
	}

	public void setRecFlag(String recFlag) {
		this.rec_status = recFlag;
	}

	public String getScanAction() {
		return this.scanAction;
	}

	public void setScanAction(String scanAction) {
		this.scanAction = scanAction;
	}

	public Date getScanDate() {
		return this.scanDate;
	}

	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}

	public String getScanLocCode() {
		return this.scanLocCode;
	}

	public void setScanLocCode(String scanLocCode) {
		this.scanLocCode = scanLocCode;
	}

	public String getScanLocDesc() {
		return this.scanLocDesc;
	}

	public void setScanLocDesc(String scanLocDesc) {
		this.scanLocDesc = scanLocDesc;
	}

	public String getScanUser() {
		return this.scanUser;
	}

	public void setScanUser(String scanUser) {
		this.scanUser = scanUser;
	}

	public Date getUldSentDate() {
		return this.uldSentDate;
	}

	public void setUldSentDate(Date uldSentDate) {
		this.uldSentDate = uldSentDate;
	}
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
/*
	@Column(name="ID_HOPO")
	private String idHopo;

	private String id;

	@Column(name="OPT_LOCK")
	@Version
	long optLock;
	
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public void setCreatedDate(Date createdDate) {
		this._createdDate = createdDate;
		if (this._updatedDate == null) {
			this._updatedDate = createdDate;
		}
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public long getOptLock() {
		return this.optLock;
	}

	public void setOptLock(long optLock) {
		this.optLock = optLock;
	}
*/

 /*   @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
   
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object.getClass() == getClass())) {
            return false;
        }
        EntDbLoadUld other =  (EntDbLoadUld) object;
        if (id != other.id && (id == null || !id.equals(other.id))) {
            return false;
        }
        return true;
    }*/
}