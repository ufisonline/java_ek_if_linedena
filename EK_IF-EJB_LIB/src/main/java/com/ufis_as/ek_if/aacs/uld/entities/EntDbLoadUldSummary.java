package com.ufis_as.ek_if.aacs.uld.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the LOAD_ULD_SUMMARY database table.
 * @author btr
 */
@Entity
@Table(name="LOAD_ULD_SUMMARY")
@NamedQueries({
	@NamedQuery(name = "EntDbLoadUldSummary.getExisting", query = "SELECT a FROM EntDbLoadUldSummary a WHERE a.idFlight = :idFlight AND a.idConxFlight = :idConxFlight AND a.infoType = :infoType AND a.recStatus = :recStatus"),
	@NamedQuery(name = "EntDbLoadUldSummary.findIdFlightByRange", query = "SELECT DISTINCT a.idFlight FROM EntDbLoadUldSummary a WHERE ((a._createdDate BETWEEN :starDate AND :endDate) OR (a._updatedDate BETWEEN :starDate AND :endDate)) AND a.infoType = :infoType AND a.idFlight is not null"),
	@NamedQuery(name = "EntDbLoadUldSummary.findConxFlightById", query = "SELECT a FROM EntDbLoadUldSummary a WHERE a.idFlight =:idFlight AND a.infoType = :infoType AND a.recStatus <> 'X' ")
})
public class EntDbLoadUldSummary extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="BAG_ULD_PCS")
	private float bagUldPcs;

	@Column(name="BAG_ULD_WT")
	private float bagUldWt;

	@Column(name="BULK_BAG_ULD_PCS")
	private float bulkBagUldPcs;

	@Column(name="BULK_BAG_ULD_WT")
	private float bulkBagUldWt;

	@Column(name="BULK_CARGO_ULD_PCS")
	private float bulkCargoUldPcs;

	@Column(name="BULK_CARGO_ULD_WT")
	private float bulkCargoUldWt;

	@Column(name="BULK_PCS")
	private float bulkPcs;

	@Column(name="BULK_WEIGHT")
	private float bulkWeight;

	@Column(name="CARGO_ULD_PCS")
	private float cargoUldPcs;

	@Column(name="CARGO_ULD_WT")
	private float cargoUldWt;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DOLLY_PCS")
	private float dollyPcs;

	@Column(name="HOP_FROM")
	private String hopFrom;

	@Column(name="HOP_TO")
	private String hopTo;

	@Column(name="ID_CONX_FLIGHT")
	private BigDecimal idConxFlight;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="INFO_TYPE")
	private String infoType;

	@Column(name="PALLET_ULD_PCS")
	private float palletUldPcs;

	@Column(name="PALLET_ULD_WT")
	private float palletUldWt;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="ULD_PCS")
	private float uldPcs;

	@Column(name="ULD_WEIGHT")
	private float uldWeight;

	public EntDbLoadUldSummary() {
	}

	public float getBagUldPcs() {
		return this.bagUldPcs;
	}

	public void setBagUldPcs(float bagUldPcs) {
		this.bagUldPcs = bagUldPcs;
	}

	public float getBagUldWt() {
		return this.bagUldWt;
	}

	public void setBagUldWt(float bagUldWt) {
		this.bagUldWt = bagUldWt;
	}

	public float getBulkBagUldPcs() {
		return this.bulkBagUldPcs;
	}

	public void setBulkBagUldPcs(float bulkBagUldPcs) {
		this.bulkBagUldPcs = bulkBagUldPcs;
	}

	public float getBulkBagUldWt() {
		return this.bulkBagUldWt;
	}

	public void setBulkBagUldWt(float bulkBagUldWt) {
		this.bulkBagUldWt = bulkBagUldWt;
	}

	public float getBulkCargoUldPcs() {
		return this.bulkCargoUldPcs;
	}

	public void setBulkCargoUldPcs(float bulkCargoUldPcs) {
		this.bulkCargoUldPcs = bulkCargoUldPcs;
	}

	public float getBulkCargoUldWt() {
		return this.bulkCargoUldWt;
	}

	public void setBulkCargoUldWt(float bulkCargoUldWt) {
		this.bulkCargoUldWt = bulkCargoUldWt;
	}

	public float getBulkPcs() {
		return this.bulkPcs;
	}

	public void setBulkPcs(float bulkPcs) {
		this.bulkPcs = bulkPcs;
	}

	public float getBulkWeight() {
		return this.bulkWeight;
	}

	public void setBulkWeight(float bulkWeight) {
		this.bulkWeight = bulkWeight;
	}

	public float getCargoUldPcs() {
		return this.cargoUldPcs;
	}

	public void setCargoUldPcs(float cargoUldPcs) {
		this.cargoUldPcs = cargoUldPcs;
	}

	public float getCargoUldWt() {
		return this.cargoUldWt;
	}

	public void setCargoUldWt(float cargoUldWt) {
		this.cargoUldWt = cargoUldWt;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public float getDollyPcs() {
		return this.dollyPcs;
	}

	public void setDollyPcs(float dollyPcs) {
		this.dollyPcs = dollyPcs;
	}

	public String getHopFrom() {
		return this.hopFrom;
	}

	public void setHopFrom(String hopFrom) {
		this.hopFrom = hopFrom;
	}

	public String getHopTo() {
		return this.hopTo;
	}

	public void setHopTo(String hopTo) {
		this.hopTo = hopTo;
	}

	public BigDecimal getIdConxFlight() {
		return this.idConxFlight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getInfoType() {
		return this.infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public float getPalletUldPcs() {
		return this.palletUldPcs;
	}

	public void setPalletUldPcs(float palletUldPcs) {
		this.palletUldPcs = palletUldPcs;
	}

	public float getPalletUldWt() {
		return this.palletUldWt;
	}

	public void setPalletUldWt(float palletUldWt) {
		this.palletUldWt = palletUldWt;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public float getUldPcs() {
		return this.uldPcs;
	}

	public void setUldPcs(float uldPcs) {
		this.uldPcs = uldPcs;
	}

	public float getUldWeight() {
		return this.uldWeight;
	}

	public void setUldWeight(float uldWeight) {
		this.uldWeight = uldWeight;
	}
}