package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author btr
 * The persistent class for the MD_ULD_ITEMTYPE database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "EntDbMdUldItemType.getAllData", query = "SELECT a FROM EntDbMdUldItemType a")
})
@Table(name="MD_ULD_ITEMTYPE")
public class EntDbMdUldItemType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Column(name="OPT_LOCK")
	private BigDecimal optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="ULD_ITEM_TYPE_CODE")
	private String uldItemTypeCode;

	@Column(name="ULD_ITEM_TYPE_DESC")
	private String uldItemTypeDesc;

	@Column(name="ULD_TYPE_CODE")
	private String uldTypeCode;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	public EntDbMdUldItemType() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getUldItemTypeCode() {
		return this.uldItemTypeCode;
	}

	public void setUldItemTypeCode(String uldItemTypeCode) {
		this.uldItemTypeCode = uldItemTypeCode;
	}

	public String getUldItemTypeDesc() {
		return this.uldItemTypeDesc;
	}

	public void setUldItemTypeDesc(String uldItemTypeDesc) {
		this.uldItemTypeDesc = uldItemTypeDesc;
	}

	public String getUldTypeCode() {
		return this.uldTypeCode;
	}

	public void setUldTypeCode(String uldTypeCode) {
		this.uldTypeCode = uldTypeCode;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}