package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author btr
 * The persistent class for the MD_ULD_LOC database table.
 * 
 */
@Entity
@Table(name="MD_ULD_LOC")
@NamedQueries({
	@NamedQuery(name = "EntDbMdUldLoc.getAllData", query = "SELECT a FROM EntDbMdUldLoc a")
})
public class EntDbMdUldLoc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Column(name="OPT_LOCK")
	private BigDecimal optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="ULD_PLACE_LOC")
	private String uldPlaceLoc;

	@Column(name="ULD_PLACE_LOC_DESC")
	private String uldPlaceLocDesc;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	public EntDbMdUldLoc() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getUldPlaceLoc() {
		return this.uldPlaceLoc;
	}

	public void setUldPlaceLoc(String uldPlaceLoc) {
		this.uldPlaceLoc = uldPlaceLoc;
	}

	public String getUldPlaceLocDesc() {
		return this.uldPlaceLocDesc;
	}

	public void setUldPlaceLocDesc(String uldPlaceLocDesc) {
		this.uldPlaceLocDesc = uldPlaceLocDesc;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}