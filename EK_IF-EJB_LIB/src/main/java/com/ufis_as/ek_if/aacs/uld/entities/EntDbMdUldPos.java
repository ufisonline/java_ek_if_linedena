package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author btr
 * The persistent class for the MD_ULD_POS database table.
 * 
 */
@Entity
@Table(name="MD_ULD_POS")
@NamedQueries({
	@NamedQuery(name = "EntDbMdUldPos.getAllData", query = "SELECT a FROM EntDbMdUldPos a")
})
public class EntDbMdUldPos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name="AIRCRAFT_TYPE3")
	private String aircraftType3;

	@Column(name="AIRCRAFT_TYPE5")
	private String aircraftType5;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Column(name="OPT_LOCK")
	private BigDecimal optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="ULD_POSITION")
	private String uldPosition;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	public EntDbMdUldPos() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAircraftType3() {
		return this.aircraftType3;
	}

	public void setAircraftType3(String aircraftType3) {
		this.aircraftType3 = aircraftType3;
	}

	public String getAircraftType5() {
		return this.aircraftType5;
	}

	public void setAircraftType5(String aircraftType5) {
		this.aircraftType5 = aircraftType5;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getUldPosition() {
		return this.uldPosition;
	}

	public void setUldPosition(String uldPosition) {
		this.uldPosition = uldPosition;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}