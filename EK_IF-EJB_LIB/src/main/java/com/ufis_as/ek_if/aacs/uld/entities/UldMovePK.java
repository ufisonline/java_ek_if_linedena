package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The primary key class for the LOAD_ULD_MOVE database table.
 * 
 */

@Embeddable
public class UldMovePK  implements Serializable{
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ULD_NUMBER")
	private String uldNumber;

	@Column(name="FLIGHT_DATE")
	private Date flightDate;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	public UldMovePK() {
	}

	public String getUldFltno() {
		return this.flightNumber;
	}

	public void setUldFltno(String uldFltno) {
		this.flightNumber = uldFltno;
	}

	public String getUldNumber() {
		return this.uldNumber;
	}

	public void setUldNumber(String uldNumber) {
		this.uldNumber = uldNumber;
	}

	public Date getFltDate() {
		return this.flightDate;
	}

	public void setFltDate(Date fltDate) {
		this.flightDate = fltDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UldMovePK)) {
			return false;
		}
		UldMovePK castOther = (UldMovePK)other;
		return 
			this.flightNumber.equals(castOther.flightNumber)
			&& this.uldNumber.equals(castOther.uldNumber)
			&& this.flightDate.equals(castOther.flightDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.flightNumber.hashCode();
		hash = hash * prime + this.uldNumber.hashCode();
		hash = hash * prime + this.flightDate.hashCode();
		
		return hash;
	}
}