package com.ufis_as.ek_if.aacs.uld.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * @author btr
 * The primary key class for the LOAD_ULDS database table.
 * 
 */

@Embeddable
public class UldPK  implements Serializable{
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ULD_CURR_FLNO")
	private String uldCurrFltno;
	
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="ULD_NUMBER")
	private String uldNumber;
	
	
	public UldPK() {
	}
	
	public UldPK(Date fltDate, String uldCurrFltno, String uldNumber){
		this.uldCurrFltno = uldCurrFltno;
		this.fltDate = fltDate;
		this.uldNumber = uldNumber;
	}

	public String getUldCurrFltno() {
		return this.uldCurrFltno;
	}

	public void setUldCurrFltno(String uldCurrFltno) {
		this.uldCurrFltno = uldCurrFltno;
	}

	public String getUldNumber() {
		return this.uldNumber;
	}

	public void setUldNumber(String uldNumber) {
		this.uldNumber = uldNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}
}