package com.ufis_as.ek_if.acts_ods.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the FLT_JOB_ASSIGN database table.
 * 
 */
@Entity
@Table(name="FLT_JOB_ASSIGN")
@NamedQueries({
	@NamedQuery(name = "EntDbFltJobAssign.findByFlIdAndStaffNum", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFlight = :idFlight AND a.staffNumber= :staffNumber AND a.fltDate=:fltDate AND a.dataSource=:dataSource AND a.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbFltJobAssign.findAssignedCrewByFlidAndStaffNum", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFlight = :idFlight AND a.staffNumber= :staffNumber AND a.dataSource=:dataSource AND a.recStatus <> :recStatus"),
	@NamedQuery(name="EntDbFltJobAssign.findByFltIdDetails",query="SELECT e FROM EntDbFltJobAssign e WHERE e.flightNumber = :fltNum AND e.fltDate=:fltDate AND e.fltOrigin3=:org3 AND e.fltDest3=:des3 AND e.idFlight <> :idFlight AND e.dataSource=:dataSource AND e.recStatus <> :recStatus"),
	@NamedQuery(name="EntDbFltJobAssign.findByFltId",query="SELECT e FROM EntDbFltJobAssign e WHERE e.idFlight = :idFlight AND e.dataSource=:dataSource AND e.recStatus <> :recStatus")
})
public class EntDbFltJobAssign extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final Logger LOG = LoggerFactory.getLogger(EntDbFltJobAssign.class);
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)	
//	private String id;

	@Column(name="CHANGE_REASON")
	private String changeReason;

	@Column(name="CHANGE_REASON_CODE")
	private String changeReasonCode;

//	@Temporal(TemporalType.DATE)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRANSACT_DATE")
	private Date transactDate;
		

	@Column(name="TRANSACT_USER")
	private String transactUser;
	
	@Column(name="DEP_NUM")
	private String depNum;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="FLT_DEST3")
	private String fltDest3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_EST_ARR_DATE")
	private Date fltEstArrDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_EST_DEP_DATE")
	private Date fltEstDepDate;

	@Column(name="FLT_ORIGIN3")
	private String fltOrigin3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_SCHE_ARR_DATE")
	private Date fltScheArrDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_SCHE_DEP_DATE")
	private Date fltScheDepDate;

	//private String id;

	@Column(name="ID_FLIGHT")
	private long idFlight;

	//@Column(name="ID_HOPO")
	//private String idHopo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="JOB_ASSIGN_DATE")
	private Date jobAssignDate;

	@Column(name="LEG_NUM")
	private String legNum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

//	@Column(name="OPT_LOCK")
//	private long optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_FIRST_NAME")
	private String staffFirstName;

	@Column(name="STAFF_LAST_NAME")
	private String staffLastName;

	@Column(name="STAFF_NUMBER")
	private String staffNumber;

	@Column(name="STAFF_OP_GRADE")
	private String staffOpGrade;

	@Column(name="STAFF_TYPE")
	private String staffType;

	@Column(name="STAFF_WORK_TYPE")
	private String staffWorkType;

	@Column(name="TRIP_END")
	private String tripEnd;

	@Column(name="TRIP_START")
	private String tripStart;

//	@Temporal(TemporalType.DATE)
//	@Column(name="UPDATED_DATE")
//	private Date updatedDate;
//
//	@Column(name="UPDATED_USER")
//	private String updatedUser;

	public EntDbFltJobAssign() {
	}

	public String getChangeReason() {
		return this.changeReason;
	}

	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}

	public String getChangeReasonCode() {
		return this.changeReasonCode;
	}

	public void setChangeReasonCode(String changeReasonCode) {
		this.changeReasonCode = changeReasonCode;
	}

//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedUser() {
//		return this.createdUser;
//	}
//
//	public void setCreatedUser(String createdUser) {
//		this.createdUser = createdUser;
//	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.ACTS_DATA_SOURCE;
		this.dataSource = HpEKConstants.ACTS_DATA_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			 if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			 }
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.ACTS_DATA_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbFltJobAssign other =  (EntDbFltJobAssign) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }
		
	
	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDepNum() {
		return this.depNum;
	}

	public void setDepNum(String depNum) {
		this.depNum = depNum;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public Date getFltEstArrDate() {
		return this.fltEstArrDate;
	}

	public void setFltEstArrDate(Date fltEstArrDate) {
		this.fltEstArrDate = fltEstArrDate;
	}

	public Date getFltEstDepDate() {
		return this.fltEstDepDate;
	}

	public void setFltEstDepDate(Date fltEstDepDate) {
		this.fltEstDepDate = fltEstDepDate;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public Date getFltScheArrDate() {
		return this.fltScheArrDate;
	}

	public void setFltScheArrDate(Date fltScheArrDate) {
		this.fltScheArrDate = fltScheArrDate;
	}

	public Date getFltScheDepDate() {
		return this.fltScheDepDate;
	}

	public void setFltScheDepDate(Date fltScheDepDate) {
		this.fltScheDepDate = fltScheDepDate;
	}

//	public String getId() {
//		return this.id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}

	public long getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(long idFlight) {
		this.idFlight = idFlight;
	}
//
//	public String getIdHopo() {
//		return this.idHopo;
//	}
//
//	public void setIdHopo(String idHopo) {
//		this.idHopo = idHopo;
//	}

	public Date getJobAssignDate() {
		return this.jobAssignDate;
	}

	public void setJobAssignDate(Date jobAssignDate) {
		this.jobAssignDate = jobAssignDate;
	}

	public String getLegNum() {
		return this.legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

//	public long getOptLock() {
//		return this.optLock;
//	}
//
//	public void setOptLock(long optLock) {
//		this.optLock = optLock;
//	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffFirstName() {
		return this.staffFirstName;
	}

	public void setStaffFirstName(String staffFirstName) {
		this.staffFirstName = staffFirstName;
	}

	public String getStaffLastName() {
		return this.staffLastName;
	}

	public void setStaffLastName(String staffLastName) {
		this.staffLastName = staffLastName;
	}

	public String getStaffNumber() {
		return this.staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffOpGrade() {
		return this.staffOpGrade;
	}

	public void setStaffOpGrade(String staffOpGrade) {
		this.staffOpGrade = staffOpGrade;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStaffWorkType() {
		return this.staffWorkType;
	}

	public void setStaffWorkType(String staffWorkType) {
		this.staffWorkType = staffWorkType;
	}

	public String getTripEnd() {
		return this.tripEnd;
	}

	public void setTripEnd(String tripEnd) {
		this.tripEnd = tripEnd;
	}

	public String getTripStart() {
		return this.tripStart;
	}

	public void setTripStart(String tripStart) {
		this.tripStart = tripStart;
	}

	public Date getTransactDate() {
		return transactDate;
	}

	public void setTransactDate(Date transactDate) {
		this.transactDate = transactDate;
	}

	public String getTransactUser() {
		return transactUser;
	}

	public void setTransactUser(String transactUser) {
		this.transactUser = transactUser;
	}
	
//	public Date getUpdatedDate() {
//		return this.updatedDate;
//	}
//
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}
//
//	public String getUpdatedUser() {
//		return this.updatedUser;
//	}
//
//	public void setUpdatedUser(String updatedUser) {
//		this.updatedUser = updatedUser;
//	}

}