package com.ufis_as.ek_if.acts_ods.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * The persistent class for the FLT_JOB_SUMMARY database table.
 * 
 */
@Entity
@Table(name = "FLT_JOB_SUMMARY")
@NamedQueries({
		@NamedQuery(name = "EntDbFltJobSummary.findByFlId", query = "SELECT e FROM EntDbFltJobSummary e WHERE e.idFlight=:fltId AND UPPER(e.resourceType)='CREW'"),
		@NamedQuery(name = "EntDbFltJobSummary.findByFlIdCrewType", query = "SELECT e FROM EntDbFltJobSummary e WHERE e.idFlight=:fltId AND e.resourceTypeCode=:resourceTypeCode AND UPPER(e.resourceType)='CREW'"),
		@NamedQuery(name = "EntDbFltJobSummary.findCrewSumByFlIdCrewType", query = "SELECT e FROM EntDbFltJobSummary e WHERE e.idFlight=:fltId AND e.resourceTypeCode IS NULL AND e.staffWorkType IS NULL AND UPPER(e.resourceType)='CREW'"), })
public class EntDbFltJobSummary extends EntHopoAssociated implements
		Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	public static final Logger LOG = LoggerFactory
			.getLogger(EntDbFltJobSummary.class);

	// @Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	// private String id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGN_DATE")
	private Date assignDate;
	//
	// @Temporal(TemporalType.DATE)
	// @Column(name="CREATED_DATE")
	// private Date createdDate;
	//
	// @Column(name="CREATED_USER")
	// private String createdUser;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	// private String id;

	@Column(name = "ID_FLIGHT")
	private long idFlight;

	// @Column(name="ID_HOPO")
	// private String idHopo;
	//
	// @Column(name="OPT_LOCK")
	// private BigDecimal optLock;

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Column(name = "RESOURCE_SUBTYPE")
	private String resourceSubtype;

	@Column(name = "RESOURCE_TYPE")
	private String resourceType;

	@Column(name = "RESOURCE_TYPE_CODE")
	private String resourceTypeCode;

	@Column(name = "STAFF_OP_GRADE")
	private String staffOpGrade;

	@Column(name = "STAFF_WORK_TYPE")
	private String staffWorkType;

	@Column(name = "TOTAL_ASSIGNED")
	private long totalAssigned;

	// @Temporal(TemporalType.DATE)
	// @Column(name="UPDATED_DATE")
	// private Date updatedDate;
	//
	// @Column(name="UPDATED_USER")
	// private String updatedUser;

	public EntDbFltJobSummary() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.ACTS_DATA_SOURCE;
		this.dataSource = HpEKConstants.ACTS_DATA_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
		// try {
		if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		}
		// } catch (ParseException e) {
		// LOG.error("ERROR when getting current UTC time : {}", e.toString());
		// }
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.ACTS_DATA_SOURCE);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbFltJobSummary other = (EntDbFltJobSummary) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public Date getAssignDate() {
		return this.assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	// public Date getCreatedDate() {
	// return this.createdDate;
	// }
	//
	// public void setCreatedDate(Date createdDate) {
	// this.createdDate = createdDate;
	// }
	//
	// public String getCreatedUser() {
	// return this.createdUser;
	// }
	//
	// public void setCreatedUser(String createdUser) {
	// this.createdUser = createdUser;
	// }
	//
	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(long idFlight) {
		this.idFlight = idFlight;
	}

	// public String getIdHopo() {
	// return this.idHopo;
	// }
	//
	// public void setIdHopo(String idHopo) {
	// this.idHopo = idHopo;
	// }
	//
	// public BigDecimal getOptLock() {
	// return this.optLock;
	// }
	//
	// public void setOptLock(BigDecimal optLock) {
	// this.optLock = optLock;
	// }

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getResourceSubtype() {
		return this.resourceSubtype;
	}

	public void setResourceSubtype(String resourceSubtype) {
		this.resourceSubtype = resourceSubtype;
	}

	public String getResourceType() {
		return this.resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceTypeCode() {
		return this.resourceTypeCode;
	}

	public void setResourceTypeCode(String resourceTypeCode) {
		this.resourceTypeCode = resourceTypeCode;
	}

	public String getStaffOpGrade() {
		return this.staffOpGrade;
	}

	public void setStaffOpGrade(String staffOpGrade) {
		this.staffOpGrade = staffOpGrade;
	}

	public String getStaffWorkType() {
		return this.staffWorkType;
	}

	public void setStaffWorkType(String staffWorkType) {
		this.staffWorkType = staffWorkType;
	}

	public long getTotalAssigned() {
		return this.totalAssigned;
	}

	public void setTotalAssigned(long totalAssigned) {
		this.totalAssigned = totalAssigned;
	}

	// public Date getUpdatedDate() {
	// return this.updatedDate;
	// }
	//
	// public void setUpdatedDate(Date updatedDate) {
	// this.updatedDate = updatedDate;
	// }
	//
	// public String getUpdatedUser() {
	// return this.updatedUser;
	// }
	//
	// public void setUpdatedUser(String updatedUser) {
	// this.updatedUser = updatedUser;
	// }
	public EntDbFltJobSummary getClone() throws Exception {

		if (this.clone() != null) {
			return (EntDbFltJobSummary) this.clone();
		}
		return null;
	}

}