package com.ufis_as.ek_if.acts_ods.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the MD_STAFF_TYPE database table.
 * 
 */
@Entity
@Table(name="MD_STAFF_TYPE")
public class EntDbMdStaffType extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final Logger LOG = LoggerFactory.getLogger(EntDbMdStaffType.class);


	@Column(name="DATA_SOURCE")
	private String dataSource;	

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_SUBTYPE")
	private String staffSubtype;

	@Column(name="STAFF_TYPE")
	private String staffType;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	@Column(name="STAFF_TYPE_DESC")
	private String staffTypeDesc;

	

	public EntDbMdStaffType() {
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.ACTS_DATA_SOURCE;
		this.dataSource = HpEKConstants.ACTS_DATA_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.ACTS_DATA_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbMdStaffType other =  (EntDbMdStaffType) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	



	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffSubtype() {
		return this.staffSubtype;
	}

	public void setStaffSubtype(String staffSubtype) {
		this.staffSubtype = staffSubtype;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getStaffTypeDesc() {
		return this.staffTypeDesc;
	}

	public void setStaffTypeDesc(String staffTypeDesc) {
		this.staffTypeDesc = staffTypeDesc;
	}

	
}