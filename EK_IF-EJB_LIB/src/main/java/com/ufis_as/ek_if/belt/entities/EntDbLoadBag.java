package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * The persistent class for the LOAD_BAG database table.
 */
@Entity
@Table(name = "LOAD_BAG")
@NamedQueries({
		// @NamedQuery(name = "EntDbLoadBag.findByFlIdPax", query =
		// "SELECT a FROM EntDbLoadBag a WHERE a.idFlight = :idFlight AND a.idLoadPax=:idLoadPax"),
		@NamedQuery(name = "EntDbLoadBag.findByBagPaxDeptFltInfo", query = "SELECT a FROM EntDbLoadBag a WHERE a.bagTag = :bagTag AND a.paxRefNum = :paxRefNum AND a.bagFlno=:fltNumber AND a.flt_Date=:fltDate AND a.flt_Dest3=:fltDest3 AND a.recStatus IN (' ','R')"),
		@NamedQuery(name = "EntDbLoadBag.findByFlIdBagTag", query = "SELECT a FROM EntDbLoadBag a WHERE a.id_Flight = :idFlight AND a.bagTag=:bagTag AND a.recStatus IN (' ','R')"),
		@NamedQuery(name = "EntDbLoadBag.findByValidFlIdBagTag", query = "SELECT a FROM EntDbLoadBag a WHERE a.id_Flight = :idFlight AND a.bagTag=:bagTag AND a.recStatus = ' '"),
		@NamedQuery(name = "EntDbLoadBag.findByBagTagFliIdPaxRef", query = "SELECT a FROM EntDbLoadBag a WHERE a.id_Flight = :idFlight AND a.bagTag=:bagTag AND a.paxRefNum = :paxRefNum AND a.recStatus IN (' ','R')"),
		@NamedQuery(name = "EntDbLoadBag.findByFliIdPaxRef", query = "SELECT a FROM EntDbLoadBag a WHERE ( a.id_Flight = :idFlight OR a.idConxFlight =:idFlight ) AND a.paxRefNum = :paxRefNum AND a.recStatus IN (' ','R')")})
public class EntDbLoadBag extends EntHopoAssociated implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;

	public static final Logger LOG = LoggerFactory.getLogger(EntDbLoadBag.class);

	@Column(name = "ARR_FLIGHT_NUMBER")
	private String arrFlightNumber;

	@Column(name = "ARR_FLT_DATE")
	private String arrFltDate;

	@Column(name = "ARR_FLT_DEST3")
	private String arrFltDest3;

	@Column(name = "ARR_FLT_ORIGIN3")
	private String arrFltOrigin3;

	@Column(name = "BAG_CLASS")
	private String bagClass;

	@Column(name = "BAG_CLASSIFY")
	private String bagClassify;

	@Column(name = "BAG_TAG")
	private String bagTag;

	@Column(name = "BAG_TYPE")
	private String bagType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHANGE_DATE")
	private Date changeDate;

	// @Temporal(TemporalType.TIMESTAMP)
	// @Column(name="CREATED_DATE")
	// private Date createdDate;
	//
	// @Column(name="CREATED_USER")
	// private String createdUser;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Column(name = "DEP_FLIGHT_NUMBER")
	private String flightNumber;

	@Column(name = "DEP_FLT_DATE")
	private String fltDate;

	@Column(name = "DEP_FLT_DEST3")
	private String fltDest3;

	@Column(name = "DEP_FLT_ORIGIN3")
	private String fltOrigin3;

	// private String id;

	@Column(name = "ID_ARR_FLIGHT")
	private long idArrFlight;

	@Column(name = "ID_DEP_FLIGHT")
	private long idFlight;
	//
	// @Column(name="ID_HOPO")
	// private String idHopo;

	@Column(name = "ID_LOAD_PAX")
	private String idLoadPax;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MSG_SEND_DATE")
	private Date msgSendDate;

	// @Column(name="OPT_LOCK")
	// private long optLock;

	@Column(name = "PAX_NAME")
	private String paxName;

	@Column(name = "PAX_REF_NUM")
	private String paxRefNum;

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Column(name = "BAG_TAG_STR")
	private String bagTagStr;
	
	@Column(name = "BAGWEIGHT")
	private BigDecimal bagWeight;
	
	@Column(name = "ID_FLIGHT")
	private BigDecimal id_Flight;
	
	@Column(name = "ID_CONX_FLIGHT")
	private BigDecimal idConxFlight;
	
	@Column(name = "BAG_FLNO")
	private String bagFlno;
	
	@Column(name = "BAG_CONX_FLNO")
	private String bagConxFlno;
	
	@Column(name = "FLT_DATE")
	private String flt_Date;
	
	@Column(name = "CONX_FLT_DATE")
	private String conxFltDate;
	
	@Column(name = "FLT_ORIGIN3")
	private String flt_Origin3;
	
	@Column(name = "CONX_FLT_ORIGIN3")
	private String conxFltOrigin3;
	
	@Column(name = "FLT_DEST3")
	private String flt_Dest3;
	
	@Column(name = "CONX_FLT_DEST3")
	private String conxFltDes3;

	// @Column(name="SCAN_ACTION")
	// private String scanAction;
	//
	// @Column(name="SCAN_LOC")
	// private String scanLoc;

	// @Temporal(TemporalType.TIMESTAMP)
	// @Column(name="UPDATED_DATE")
	// private Date updatedDate;
	//
	// @Column(name="UPDATED_USER")
	// private String updatedUser;

	public EntDbLoadBag() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.BELT_SOURCE;
		this.dataSource = HpEKConstants.BELT_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			if (this.getCreatedDate() == null) {
				this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.BELT_SOURCE);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbLoadBag other = (EntDbLoadBag) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	/*public String getArrFlightNumber() {
		return this.arrFlightNumber;
	}

	public void setArrFlightNumber(String arrFlightNumber) {
		this.arrFlightNumber = arrFlightNumber;
	}

	public String getArrFltDate() {
		return this.arrFltDate;
	}

	public void setArrFltDate(String arrFltDate) {
		this.arrFltDate = arrFltDate;
	}

	public String getArrFltDest3() {
		return this.arrFltDest3;
	}

	public void setArrFltDest3(String arrFltDest3) {
		this.arrFltDest3 = arrFltDest3;
	}

	public String getArrFltOrigin3() {
		return this.arrFltOrigin3;
	}

	public void setArrFltOrigin3(String arrFltOrigin3) {
		this.arrFltOrigin3 = arrFltOrigin3;
	}*/

	public String getBagClass() {
		return this.bagClass;
	}

	public void setBagClass(String bagClass) {
		this.bagClass = bagClass;
	}

	public String getBagClassify() {
		return this.bagClassify;
	}

	public void setBagClassify(String bagClassify) {
		this.bagClassify = bagClassify;
	}

	public String getBagTag() {
		return this.bagTag;
	}

	public void setBagTag(String bagTag) {
		this.bagTag = bagTag;
	}

	public String getBagType() {
		return this.bagType;
	}

	public void setBagType(String bagType) {
		this.bagType = bagType;
	}

	public Date getChangeDate() {
		return this.changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	// public Date getCreatedDate() {
	// return this.createdDate;
	// }
	//
	// public void setCreatedDate(Date createdDate) {
	// this.createdDate = createdDate;
	// }
	//
	// public String getCreatedUser() {
	// return this.createdUser;
	// }
	//
	// public void setCreatedUser(String createdUser) {
	// this.createdUser = createdUser;
	// }

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	/*public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(String fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}
*/
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/*public long getIdArrFlight() {
		return this.idArrFlight;
	}

	public void setIdArrFlight(long idArrFlight) {
		this.idArrFlight = idArrFlight;
	}

	public long getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(long idFlight) {
		this.idFlight = idFlight;
	}*/

	// public String getIdHopo() {
	// return this.idHopo;
	// }
	//
	// public void setIdHopo(String idHopo) {
	// this.idHopo = idHopo;
	// }

	public String getIdLoadPax() {
		return this.idLoadPax;
	}

	public void setIdLoadPax(String idLoadPax) {
		this.idLoadPax = idLoadPax;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	// public long getOptLock() {
	// return this.optLock;
	// }
	//
	// public void setOptLock(long optLock) {
	// this.optLock = optLock;
	// }

	public String getPaxName() {
		return this.paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPaxRefNum() {
		return this.paxRefNum;
	}

	public void setPaxRefNum(String paxRefNum) {
		this.paxRefNum = paxRefNum;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getBagTagStr() {
		return bagTagStr;
	}

	public void setBagTagStr(String bagTagStr) {
		this.bagTagStr = bagTagStr;
	}

	public BigDecimal getBagWeight() {
		return bagWeight;
	}

	public void setBagWeight(BigDecimal bagWeight) {
		this.bagWeight = bagWeight;
	}

	public BigDecimal getId_Flight() {
		return id_Flight;
	}

	public BigDecimal getIdConxFlight() {
		return idConxFlight;
	}

	public String getBagFlno() {
		return bagFlno;
	}

	public String getBagConxFlno() {
		return bagConxFlno;
	}

	public String getFlt_Date() {
		return flt_Date;
	}

	public String getConxFltDate() {
		return conxFltDate;
	}

	public String getFlt_Origin3() {
		return flt_Origin3;
	}

	public String getConxFltOrigin3() {
		return conxFltOrigin3;
	}

	public String getFlt_Dest3() {
		return flt_Dest3;
	}

	public String getConxFltDes3() {
		return conxFltDes3;
	}

	public void setId_Flight(BigDecimal id_Flight) {
		this.id_Flight = id_Flight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public void setBagFlno(String bagFlno) {
		this.bagFlno = bagFlno;
	}

	public void setBagConxFlno(String bagConxFlno) {
		this.bagConxFlno = bagConxFlno;
	}

	public void setFlt_Date(String flt_Date) {
		this.flt_Date = flt_Date;
	}

	public void setConxFltDate(String conxFltDate) {
		this.conxFltDate = conxFltDate;
	}

	public void setFlt_Origin3(String flt_Origin3) {
		this.flt_Origin3 = flt_Origin3;
	}

	public void setConxFltOrigin3(String conxFltOrigin3) {
		this.conxFltOrigin3 = conxFltOrigin3;
	}

	public void setFlt_Dest3(String flt_Dest3) {
		this.flt_Dest3 = flt_Dest3;
	}

	public void setConxFltDes3(String conxFltDes3) {
		this.conxFltDes3 = conxFltDes3;
	}

	public long getIdArrFlight() {
		return idArrFlight;
	}

	public void setIdArrFlight(long idArrFlight) {
		this.idArrFlight = idArrFlight;
	}

	public long getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(long idFlight) {
		this.idFlight = idFlight;
	}

	public String getArrFlightNumber() {
		return arrFlightNumber;
	}

	public void setArrFlightNumber(String arrFlightNumber) {
		this.arrFlightNumber = arrFlightNumber;
	}

	public String getArrFltDate() {
		return arrFltDate;
	}

	public void setArrFltDate(String arrFltDate) {
		this.arrFltDate = arrFltDate;
	}

	public String getArrFltDest3() {
		return arrFltDest3;
	}

	public void setArrFltDest3(String arrFltDest3) {
		this.arrFltDest3 = arrFltDest3;
	}

	public String getArrFltOrigin3() {
		return arrFltOrigin3;
	}

	public void setArrFltOrigin3(String arrFltOrigin3) {
		this.arrFltOrigin3 = arrFltOrigin3;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFltDate() {
		return fltDate;
	}

	public void setFltDate(String fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}
	
	

	// public String getScanAction() {
	// return this.scanAction;
	// }
	//
	// public void setScanAction(String scanAction) {
	// this.scanAction = scanAction;
	// }
	//
	// public String getScanLoc() {
	// return this.scanLoc;
	// }
	//
	// public void setScanLoc(String scanLoc) {
	// this.scanLoc = scanLoc;
	// }

	// public Date getUpdatedDate() {
	// return this.updatedDate;
	// }
	//
	// public void setUpdatedDate(Date updatedDate) {
	// this.updatedDate = updatedDate;
	// }
	//
	// public String getUpdatedUser() {
	// return this.updatedUser;
	// }
	//
	// public void setUpdatedUser(String updatedUser) {
	// this.updatedUser = updatedUser;
	// }
	public EntDbLoadBag getClone() throws Exception {

		if (this.clone() != null) {
			return (EntDbLoadBag) this.clone();
		}
		return null;
	}
}