package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the LOAD_BAG_MOVE database table.
 * 26DEC2013 --Added three fields(loadDesc,node_info,bimLocCode) to LOAD_BAG_MOVE 
 */
@Entity
@Table(name="LOAD_BAG_MOVE")
@NamedQueries({	
	@NamedQuery(name = "EntDbLoadBagMove.findBagMovesByIdFltBagTagPax", query = "SELECT a FROM EntDbLoadBagMove a WHERE a.idFlight = :idFlight AND a.bagTag=:bagTag AND a.paxRefNum=:paxRefNum AND a.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbLoadBagMove.findBagMovesByIdFltBagTag", query = "SELECT a FROM EntDbLoadBagMove a WHERE a.idFlight = :idFlight AND a.bagTag=:bagTag AND a.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbLoadBagMove.findBagMovesByIdFltIdBagTag", query = "SELECT a FROM EntDbLoadBagMove a WHERE a.idFlight = :idFlight AND a.bagTag=:bagTag AND (a.idLoadBag IS NULL OR (a.idLoadBag IS NOT NULL AND a.idLoadBag <> :idLoadBag)) AND a.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbLoadBagMove.findByidLoadBag", query = "SELECT a FROM EntDbLoadBagMove a WHERE a.idLoadBag=:idLoadBag AND a.recStatus =' '")	
})
public class EntDbLoadBagMove extends EntHopoAssociated implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;

	public static final Logger LOG = LoggerFactory.getLogger(EntDbLoadBagMove.class);
	
	@Column(name="BAG_CLASS")
	private String bagClass;

	@Column(name="BAG_TAG")
	private String bagTag;

	@Column(name="BAG_TYPE")
	private String bagType;	

	@Column(name="LOC_TYPE")
	private String locType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CHANGE_DATE")
	private Date changeDate;

	@Column(name="CONTAINER_DEST3")
	private String containerDest3;

	@Column(name="CONTAINER_ID")
	private String containerId;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Column(name="FLT_DATE")
	private String fltDate;

	@Column(name="FLT_DEST3")
	private String fltDest3;

	@Column(name="FLT_ORIGIN3")
	private String fltOrigin3;

	//private String id;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

//	@Column(name="ID_HOPO")
//	private String idHopo;

	@Column(name="ID_LOAD_BAG")
	private String idLoadBag;

	@Column(name="ID_LOAD_ULD")
	private String idLoadUld;

	@Column(name="ID_MD_BAG_HANDLE_LOC")
	private String idMdBagHandleLoc;

	@Column(name="ID_MD_BAG_RECONCILE_LOC")
	private String idMdBagReconcileLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;
//
//	@Column(name="OPT_LOCK")
//	private long optLock;

	@Column(name="PAX_NAME")
	private String paxName;

	@Column(name="PAX_REF_NUM")
	private String paxRefNum;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="SCAN_ACTION")
	private String scanAction;

	@Column(name="SCAN_LOC")
	private String scanLoc;

	@Column(name="LOC_DESC")
	private String locDesc;
		
	@Column(name="NODE_INFO")
	private String nodeInfo;
	
	@Column(name="BIM_LOC_CODE")
	private String bimLocCode;
	
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="UPDATED_DATE")
//	private Date updatedDate;
//
//	@Column(name="UPDATED_USER")
//	private String updatedUser;

	public EntDbLoadBagMove() {
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.BELT_SOURCE;
		this.dataSource = HpEKConstants.BELT_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			 if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			 }
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.BELT_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbLoadBagMove other =  (EntDbLoadBagMove) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }
	
	public String getBagClass() {
		return this.bagClass;
	}

	public void setBagClass(String bagClass) {
		this.bagClass = bagClass;
	}

	public String getBagTag() {
		return this.bagTag;
	}

	public void setBagTag(String bagTag) {
		this.bagTag = bagTag;
	}

	public String getBagType() {
		return this.bagType;
	}

	public void setBagType(String bagType) {
		this.bagType = bagType;
	}

	public Date getChangeDate() {
		return this.changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getContainerDest3() {
		return this.containerDest3;
	}

	public void setContainerDest3(String containerDest3) {
		this.containerDest3 = containerDest3;
	}

	public String getContainerId() {
		return this.containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedUser() {
//		return this.createdUser;
//	}
//
//	public void setCreatedUser(String createdUser) {
//		this.createdUser = createdUser;
//	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(String fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

//	public String getIdHopo() {
//		return this.idHopo;
//	}
//
//	public void setIdHopo(String idHopo) {
//		this.idHopo = idHopo;
//	}

	public String getIdLoadBag() {
		return this.idLoadBag;
	}

	public void setIdLoadBag(String idLoadBag) {
		this.idLoadBag = idLoadBag;
	}

	public String getIdLoadUld() {
		return this.idLoadUld;
	}

	public void setIdLoadUld(String idLoadUld) {
		this.idLoadUld = idLoadUld;
	}

	public String getIdMdBagHandleLoc() {
		return this.idMdBagHandleLoc;
	}

	public void setIdMdBagHandleLoc(String idMdBagHandleLoc) {
		this.idMdBagHandleLoc = idMdBagHandleLoc;
	}

	public String getIdMdBagReconcileLoc() {
		return this.idMdBagReconcileLoc;
	}

	public void setIdMdBagReconcileLoc(String idMdBagReconcileLoc) {
		this.idMdBagReconcileLoc = idMdBagReconcileLoc;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

//	public long getOptLock() {
//		return this.optLock;
//	}
//
//	public void setOptLock(long optLock) {
//		this.optLock = optLock;
//	}

	public String getPaxName() {
		return this.paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPaxRefNum() {
		return this.paxRefNum;
	}

	public void setPaxRefNum(String paxRefNum) {
		this.paxRefNum = paxRefNum;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getScanAction() {
		return this.scanAction;
	}

	public void setScanAction(String scanAction) {
		this.scanAction = scanAction;
	}

	public String getScanLoc() {
		return this.scanLoc;
	}

	public void setScanLoc(String scanLoc) {
		this.scanLoc = scanLoc;
	}
	
	public String getLocType() {
		return locType;
	}

	public void setLocType(String locType) {
		this.locType = locType;
	}

	public String getLocDesc() {
		return locDesc;
	}

	public String getNodeInfo() {
		return nodeInfo;
	}

	public String getBimLocCode() {
		return bimLocCode;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	public void setNodeInfo(String nodeInfo) {
		this.nodeInfo = nodeInfo;
	}

	public void setBimLocCode(String bimLocCode) {
		this.bimLocCode = bimLocCode;
	}

	
//	public Date getUpdatedDate() {
//		return this.updatedDate;
//	}
//
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}
//
//	public String getUpdatedUser() {
//		return this.updatedUser;
//	}
//
//	public void setUpdatedUser(String updatedUser) {
//		this.updatedUser = updatedUser;
//	}
	public EntDbLoadBagMove getClone() throws Exception {

		if (this.clone() != null) {
			return (EntDbLoadBagMove) this.clone();
		}
		return null;
	}
}