package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the LOAD_BAG_SUMMARY database table.
 * 
 */
@Entity
@Table(name="LOAD_BAG_SUMMARY")
@NamedQueries({ @NamedQuery(name = "EntDbLoadBagSummary.findByFlightId", query = "SELECT a FROM EntDbLoadBagSummary a WHERE a.idFlight = :idFlight AND a.recStatus = :recStatus"),
	@NamedQuery(name = "EntDbLoadBagSummary.findByFlightIdInfoType", query = "SELECT a FROM EntDbLoadBagSummary a WHERE a.idFlight = :idFlight AND a.idConxFlight=:idConxFlight AND a.infoType=:infoType AND a.recStatus = :recStatus"),
	@NamedQuery(name = "EntDbLoadBagSummary.findIdFlightByRange", query = "SELECT DISTINCT a.idFlight FROM EntDbLoadBagSummary a WHERE ((a._createdDate BETWEEN :starDate AND :endDate) OR (a._updatedDate BETWEEN :starDate AND :endDate)) AND a.infoType = :infoType AND a.idFlight is not null "),
	@NamedQuery(name = "EntDbLoadBagSummary.findConxFlightById", query = "SELECT a FROM EntDbLoadBagSummary a WHERE a.idFlight =:idFlight AND a.infoType = :infoType AND a.recStatus <> 'X' "),
	@NamedQuery(name = "EntDbLoadBagSummary.findByInfoType", query = "SELECT a FROM EntDbLoadBagSummary a WHERE a.idFlight = :idFlight AND a.infoType=:infoType AND a.idConxFlight IS NULL AND a.recStatus = :recStatus")
})
public class EntDbLoadBagSummary extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String id;*/

	@Column(name="ALL_ECON_BAG_PCS")
	private BigDecimal allEconBagPcs;

	@Column(name="ALL_ECON_BAG_WEIGHT")
	private BigDecimal allEconBagWeight;

	@Column(name="BAG_PCS")
	private BigDecimal bagPcs;

	@Column(name="BAG_WEIGHT")
	private BigDecimal bagWeight;

	@Column(name="BULK_PCS")
	private BigDecimal bulkPcs;

	@Column(name="BULK_WEIGHT")
	private BigDecimal bulkWeight;

	@Column(name="BUSINESS_BAG_PCS")
	private BigDecimal businessBagPcs;

	@Column(name="BUSINESS_BAG_WEIGHT")
	private BigDecimal businessBagWeight;

	@Column(name="CARGO_PCS")
	private BigDecimal cargoPcs;

	@Column(name="CARGO_WEIGHT")
	private BigDecimal cargoWeight;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="CREW_BAG_PCS")
	private BigDecimal crewBagPcs;

	@Column(name="CREW_BAG_WEIGHT")
	private BigDecimal crewBagWeight;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DEAD_LOAD_PCS")
	private BigDecimal deadLoadPcs;

	@Column(name="DEAD_LOAD_WEIGHT")
	private BigDecimal deadLoadWeight;

	@Column(name="ECON_BAG_PCS")
	private BigDecimal econBagPcs;

	@Column(name="ECON_BAG_WEIGHT")
	private BigDecimal econBagWeight;

	@Column(name="ECON2_BAG_PCS")
	private BigDecimal econ2BagPcs;

	@Column(name="ECON2_BAG_WEIGHT")
	private BigDecimal econ2BagWeight;

	@Column(name="FIRST_BAG_PCS")
	private BigDecimal firstBagPcs;

	@Column(name="FIRST_BAG_WEIGHT")
	private BigDecimal firstBagWeight;

	@Column(name="GATE_BAG_PCS")
	private BigDecimal gateBagPcs;

	@Column(name="GATE_BAG_WEIGHT")
	private BigDecimal gateBagWeight;

	@Column(name="HOP_FROM")
	private String hopFrom;

	@Column(name="HOP_TO")
	private String hopTo;

	@Column(name="ID_CONX_FLIGHT")
	private BigDecimal idConxFlight;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

//	@Column(name="ID_HOPO")
//	private String idHopo;

	@Column(name="INFO_TYPE")
	private String infoType;

	@Column(name="MAIL_PCS")
	private BigDecimal mailPcs;

	@Column(name="MAIL_WEIGHT")
	private BigDecimal mailWeight;

//	@Column(name="OPT_LOCK")
//	private BigDecimal optLock;

	@Column(name="OTHER_PCS")
	private BigDecimal otherPcs;

	@Column(name="OTHER_WEIGHT")
	private BigDecimal otherWeight;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="RUSH_BAG_PCS")
	private BigDecimal rushBagPcs;

	@Column(name="RUSH_BAG_WEIGHT")
	private BigDecimal rushBagWeight;

	@Column(name="UNDER_LOAD_PCS")
	private BigDecimal underLoadPcs;

	@Column(name="UNDER_LOAD_WEIGHT")
	private BigDecimal underLoadWeight;

	/*@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;*/

	public EntDbLoadBagSummary() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = "LSUM";
		this.dataSource = "SYS";
		this.setHopo("DXB");

		if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		}
		if (this.getCreatedUser() == null) {
			this.setCreatedUser("LSUM");
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbLoadBagSummary other = (EntDbLoadBagSummary) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getAllEconBagPcs() {
		return this.allEconBagPcs;
	}

	public void setAllEconBagPcs(BigDecimal allEconBagPcs) {
		this.allEconBagPcs = allEconBagPcs;
	}

	public BigDecimal getAllEconBagWeight() {
		return this.allEconBagWeight;
	}

	public void setAllEconBagWeight(BigDecimal allEconBagWeight) {
		this.allEconBagWeight = allEconBagWeight;
	}

	public BigDecimal getBagPcs() {
		return this.bagPcs;
	}

	public void setBagPcs(BigDecimal bagPcs) {
		this.bagPcs = bagPcs;
	}

	public BigDecimal getBagWeight() {
		return this.bagWeight;
	}

	public void setBagWeight(BigDecimal bagWeight) {
		this.bagWeight = bagWeight;
	}

	public BigDecimal getBulkPcs() {
		return this.bulkPcs;
	}

	public void setBulkPcs(BigDecimal bulkPcs) {
		this.bulkPcs = bulkPcs;
	}

	public BigDecimal getBulkWeight() {
		return this.bulkWeight;
	}

	public void setBulkWeight(BigDecimal bulkWeight) {
		this.bulkWeight = bulkWeight;
	}

	public BigDecimal getBusinessBagPcs() {
		return this.businessBagPcs;
	}

	public void setBusinessBagPcs(BigDecimal businessBagPcs) {
		this.businessBagPcs = businessBagPcs;
	}

	public BigDecimal getBusinessBagWeight() {
		return this.businessBagWeight;
	}

	public void setBusinessBagWeight(BigDecimal businessBagWeight) {
		this.businessBagWeight = businessBagWeight;
	}

	public BigDecimal getCargoPcs() {
		return this.cargoPcs;
	}

	public void setCargoPcs(BigDecimal cargoPcs) {
		this.cargoPcs = cargoPcs;
	}

	public BigDecimal getCargoWeight() {
		return this.cargoWeight;
	}

	public void setCargoWeight(BigDecimal cargoWeight) {
		this.cargoWeight = cargoWeight;
	}

	/*public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}*/

	public BigDecimal getCrewBagPcs() {
		return this.crewBagPcs;
	}

	public void setCrewBagPcs(BigDecimal crewBagPcs) {
		this.crewBagPcs = crewBagPcs;
	}

	public BigDecimal getCrewBagWeight() {
		return this.crewBagWeight;
	}

	public void setCrewBagWeight(BigDecimal crewBagWeight) {
		this.crewBagWeight = crewBagWeight;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getDeadLoadPcs() {
		return this.deadLoadPcs;
	}

	public void setDeadLoadPcs(BigDecimal deadLoadPcs) {
		this.deadLoadPcs = deadLoadPcs;
	}

	public BigDecimal getDeadLoadWeight() {
		return this.deadLoadWeight;
	}

	public void setDeadLoadWeight(BigDecimal deadLoadWeight) {
		this.deadLoadWeight = deadLoadWeight;
	}

	public BigDecimal getEconBagPcs() {
		return this.econBagPcs;
	}

	public void setEconBagPcs(BigDecimal econBagPcs) {
		this.econBagPcs = econBagPcs;
	}

	public BigDecimal getEconBagWeight() {
		return this.econBagWeight;
	}

	public void setEconBagWeight(BigDecimal econBagWeight) {
		this.econBagWeight = econBagWeight;
	}

	public BigDecimal getEcon2BagPcs() {
		return this.econ2BagPcs;
	}

	public void setEcon2BagPcs(BigDecimal econ2BagPcs) {
		this.econ2BagPcs = econ2BagPcs;
	}

	public BigDecimal getEcon2BagWeight() {
		return this.econ2BagWeight;
	}

	public void setEcon2BagWeight(BigDecimal econ2BagWeight) {
		this.econ2BagWeight = econ2BagWeight;
	}

	public BigDecimal getFirstBagPcs() {
		return this.firstBagPcs;
	}

	public void setFirstBagPcs(BigDecimal firstBagPcs) {
		this.firstBagPcs = firstBagPcs;
	}

	public BigDecimal getFirstBagWeight() {
		return this.firstBagWeight;
	}

	public void setFirstBagWeight(BigDecimal firstBagWeight) {
		this.firstBagWeight = firstBagWeight;
	}

	public BigDecimal getGateBagPcs() {
		return this.gateBagPcs;
	}

	public void setGateBagPcs(BigDecimal gateBagPcs) {
		this.gateBagPcs = gateBagPcs;
	}

	public BigDecimal getGateBagWeight() {
		return this.gateBagWeight;
	}

	public void setGateBagWeight(BigDecimal gateBagWeight) {
		this.gateBagWeight = gateBagWeight;
	}

	public String getHopFrom() {
		return this.hopFrom;
	}

	public void setHopFrom(String hopFrom) {
		this.hopFrom = hopFrom;
	}

	public String getHopTo() {
		return this.hopTo;
	}

	public void setHopTo(String hopTo) {
		this.hopTo = hopTo;
	}

	public BigDecimal getIdConxFlight() {
		return this.idConxFlight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	/*public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}*/

	public String getInfoType() {
		return this.infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public BigDecimal getMailPcs() {
		return this.mailPcs;
	}

	public void setMailPcs(BigDecimal mailPcs) {
		this.mailPcs = mailPcs;
	}

	public BigDecimal getMailWeight() {
		return this.mailWeight;
	}

	public void setMailWeight(BigDecimal mailWeight) {
		this.mailWeight = mailWeight;
	}

	/*public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}*/

	public BigDecimal getOtherPcs() {
		return this.otherPcs;
	}

	public void setOtherPcs(BigDecimal otherPcs) {
		this.otherPcs = otherPcs;
	}

	public BigDecimal getOtherWeight() {
		return this.otherWeight;
	}

	public void setOtherWeight(BigDecimal otherWeight) {
		this.otherWeight = otherWeight;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public BigDecimal getRushBagPcs() {
		return this.rushBagPcs;
	}

	public void setRushBagPcs(BigDecimal rushBagPcs) {
		this.rushBagPcs = rushBagPcs;
	}

	public BigDecimal getRushBagWeight() {
		return this.rushBagWeight;
	}

	public void setRushBagWeight(BigDecimal rushBagWeight) {
		this.rushBagWeight = rushBagWeight;
	}

	public BigDecimal getUnderLoadPcs() {
		return this.underLoadPcs;
	}

	public void setUnderLoadPcs(BigDecimal underLoadPcs) {
		this.underLoadPcs = underLoadPcs;
	}

	public BigDecimal getUnderLoadWeight() {
		return this.underLoadWeight;
	}

	public void setUnderLoadWeight(BigDecimal underLoadWeight) {
		this.underLoadWeight = underLoadWeight;
	}

	/*public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}*/

}