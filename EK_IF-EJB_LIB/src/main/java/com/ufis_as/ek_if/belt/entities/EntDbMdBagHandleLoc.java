package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the MD_BAG_HANDLE_LOC database table.
 * 
 */
@Entity
@Table(name="MD_BAG_HANDLE_LOC")
@NamedQueries({
	@NamedQuery(name = "EntDbMdBagHandleLoc.findAllBagHandleLoc", query = "SELECT a FROM EntDbMdBagHandleLoc a where a.recStatus <> :recStatus")	
})
public class EntDbMdBagHandleLoc extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final Logger LOG = LoggerFactory.getLogger(EntDbMdBagHandleLoc.class);

	@Column(name="BIM_LOC_CODE")
	private String bimLocCode;

	@Column(name="BT_DESC")
	private String btDesc;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

//	private String id;
//
//	@Column(name="ID_HOPO")
//	private String idHopo;

	@Column(name="LOC_CODE")
	private String locCode;

	@Column(name="LOC_DESC")
	private String locDesc;

	@Column(name="NODE_CODE")
	private String nodeCode;

	@Column(name="NODE_INFO")
	private String nodeInfo;

	@Column(name="NODE_POS")
	private String nodePos;

	@Column(name="NODE_STATE")
	private String nodeState;

//	@Column(name="OPT_LOCK")
//	private long optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="UPDATED_DATE")
//	private Date updatedDate;
//
//	@Column(name="UPDATED_USER")
//	private String updatedUser;

	@Column(name="XRAY_LOC_FLAG")
	private String xrayLocFlag;

	public EntDbMdBagHandleLoc() {
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.BELT_SOURCE;
		this.dataSource = HpEKConstants.BELT_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			 if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			 }
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.BELT_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbMdBagHandleLoc other =  (EntDbMdBagHandleLoc) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }
	
	public String getBimLocCode() {
		return this.bimLocCode;
	}

	public void setBimLocCode(String bimLocCode) {
		this.bimLocCode = bimLocCode;
	}

	public String getBtDesc() {
		return this.btDesc;
	}

	public void setBtDesc(String btDesc) {
		this.btDesc = btDesc;
	}

//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedUser() {
//		return this.createdUser;
//	}
//
//	public void setCreatedUser(String createdUser) {
//		this.createdUser = createdUser;
//	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public String getIdHopo() {
//		return this.idHopo;
//	}
//
//	public void setIdHopo(String idHopo) {
//		this.idHopo = idHopo;
//	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getLocDesc() {
		return this.locDesc;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	public String getNodeCode() {
		return this.nodeCode;
	}

	public void setNodeCode(String nodeCode) {
		this.nodeCode = nodeCode;
	}

	public String getNodeInfo() {
		return this.nodeInfo;
	}

	public void setNodeInfo(String nodeInfo) {
		this.nodeInfo = nodeInfo;
	}

	public String getNodePos() {
		return this.nodePos;
	}

	public void setNodePos(String nodePos) {
		this.nodePos = nodePos;
	}

	public String getNodeState() {
		return this.nodeState;
	}

	public void setNodeState(String nodeState) {
		this.nodeState = nodeState;
	}

//	public long getOptLock() {
//		return this.optLock;
//	}
//
//	public void setOptLock(long optLock) {
//		this.optLock = optLock;
//	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	// public Date getUpdatedDate() {
	// return this.updatedDate;
	// }
	//
	// public void setUpdatedDate(Date updatedDate) {
	// this.updatedDate = updatedDate;
	// }
	//
	// public String getUpdatedUser() {
	// return this.updatedUser;
	// }
	//
	// public void setUpdatedUser(String updatedUser) {
	// this.updatedUser = updatedUser;
	// }

	public String getXrayLocFlag() {
		return this.xrayLocFlag;
	}

	public void setXrayLocFlag(String xrayLocFlag) {
		this.xrayLocFlag = xrayLocFlag;
	}

}