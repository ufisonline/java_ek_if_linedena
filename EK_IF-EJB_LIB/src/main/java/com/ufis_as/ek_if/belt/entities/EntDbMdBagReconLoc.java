package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the MD_BAG_RECON_LOC database table.
 * 
 */
@Entity
@Table(name="MD_BAG_RECON_LOC")
@NamedQueries({
	@NamedQuery(name = "EntDbMdBagReconLoc.findAllBagReconLoc", query = "SELECT a FROM EntDbMdBagReconLoc a where a.recStatus <> :recStatus")	
})
public class EntDbMdBagReconLoc extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final Logger LOG=LoggerFactory.getLogger(EntDbMdBagReconLoc.class);

	@Column(name="ALERT_FLAG")
	private String alertFlag;

	private String area;

	@Column(name="CLEAR_SECURITY_FLAG")
	private String clearSecurityFlag;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	//private String id;

//	@Column(name="ID_HOPO")
//	private String idHopo;

	@Column(name="LOC_CODE")
	private String locCode;

	@Column(name="LOC_DESC")
	private String locDesc;

//	@Column(name="OPT_LOCK")
//	private long optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="SEQ_NUM")
	private long seqNum;

	private String source;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SRC_CREATE_DATE")
	private Date srcCreateDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SRC_UPD_DATE")
	private Date srcUpdDate;

	private String station;

	private String terminal;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="UPDATED_DATE")
//	private Date updatedDate;
//
//	@Column(name="UPDATED_USER")
//	private String updatedUser;

	public EntDbMdBagReconLoc() {
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.BELT_SOURCE;
		this.dataSource = HpEKConstants.BELT_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
			 if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			 }
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.BELT_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbMdBagReconLoc other =  (EntDbMdBagReconLoc) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }
	
	public String getAlertFlag() {
		return this.alertFlag;
	}

	public void setAlertFlag(String alertFlag) {
		this.alertFlag = alertFlag;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getClearSecurityFlag() {
		return this.clearSecurityFlag;
	}

	public void setClearSecurityFlag(String clearSecurityFlag) {
		this.clearSecurityFlag = clearSecurityFlag;
	}

//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedUser() {
//		return this.createdUser;
//	}
//
//	public void setCreatedUser(String createdUser) {
//		this.createdUser = createdUser;
//	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public String getIdHopo() {
//		return this.idHopo;
//	}
//
//	public void setIdHopo(String idHopo) {
//		this.idHopo = idHopo;
//	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getLocDesc() {
		return this.locDesc;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

//	public long getOptLock() {
//		return this.optLock;
//	}
//
//	public void setOptLock(long optLock) {
//		this.optLock = optLock;
//	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public long getSeqNum() {
		return this.seqNum;
	}

	public void setSeqNum(long seqNum) {
		this.seqNum = seqNum;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getSrcCreateDate() {
		return this.srcCreateDate;
	}

	public void setSrcCreateDate(Date srcCreateDate) {
		this.srcCreateDate = srcCreateDate;
	}

	public Date getSrcUpdDate() {
		return this.srcUpdDate;
	}

	public void setSrcUpdDate(Date srcUpdDate) {
		this.srcUpdDate = srcUpdDate;
	}

	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getTerminal() {
		return this.terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

//	public Date getUpdatedDate() {
//		return this.updatedDate;
//	}
//
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}
//
//	public String getUpdatedUser() {
//		return this.updatedUser;
//	}
//
//	public void setUpdatedUser(String updatedUser) {
//		this.updatedUser = updatedUser;
//	}

}