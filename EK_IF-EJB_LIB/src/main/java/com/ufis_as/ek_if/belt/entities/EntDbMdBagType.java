package com.ufis_as.ek_if.belt.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the MD_BAG_TYPE database table.
 * 
 */
@Entity
@Table(name="MD_BAG_TYPE")
@NamedQueries({
	@NamedQuery(name = "EntDbMdBagType.findallBagTypes", query = "SELECT a FROM EntDbMdBagType a"),	
	@NamedQuery(name = "EntDbMdBagType.findBagTypes", query = "SELECT a.bagTypeCode FROM EntDbMdBagType a where a.recStatus <> :recStatus")	
})
public class EntDbMdBagType extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final Logger LOG=LoggerFactory.getLogger(EntDbMdBagType.class);

	@Column(name="BAG_TYPE_CODE")
	private String bagTypeCode;

	@Column(name="BAG_TYPE_DESC")
	private String bagTypeDesc;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="CREATED_DATE")
//	private Date createdDate;
//
//	@Column(name="CREATED_USER")
//	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	//private String id;
//
//	@Column(name="ID_HOPO")
//	private String idHopo;
//
//	@Column(name="OPT_LOCK")
//	private long optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="UPDATED_DATE")
//	private Date updatedDate;
//
//	@Column(name="UPDATED_USER")
//	private String updatedUser;

	public EntDbMdBagType() {
	}

	@PrePersist
    void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.BELT_SOURCE;
		this.dataSource = HpEKConstants.BELT_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			 if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			 }
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
       if (this.getCreatedUser() == null) {
           this.setCreatedUser(HpEKConstants.BELT_SOURCE);
       }
    }
	
	
	 @Override
	    public int hashCode() {
	        int hash = 0;
	        hash += (id != null ? id.hashCode() : 0);
	        return hash;
	    }
	   
	    @Override
	    public boolean equals(Object object) {
	        if (object == null) {
	            return false;
	        }
	        if (!(object.getClass() == getClass())) {
	            return false;
	        }
	        EntDbMdBagType other =  (EntDbMdBagType) object;
	        if (id != other.id && (id == null || !id.equals(other.id))) {
	            return false;
	        }
	        return true;
	    }
	
	public String getBagTypeCode() {
		return this.bagTypeCode;
	}

	public void setBagTypeCode(String bagTypeCode) {
		this.bagTypeCode = bagTypeCode;
	}

	public String getBagTypeDesc() {
		return this.bagTypeDesc;
	}

	public void setBagTypeDesc(String bagTypeDesc) {
		this.bagTypeDesc = bagTypeDesc;
	}

//	public Date getCreatedDate() {
//		return this.createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedUser() {
//		return this.createdUser;
//	}
//
//	public void setCreatedUser(String createdUser) {
//		this.createdUser = createdUser;
//	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public String getIdHopo() {
//		return this.idHopo;
//	}
//
//	public void setIdHopo(String idHopo) {
//		this.idHopo = idHopo;
//	}
//
//	public long getOptLock() {
//		return this.optLock;
//	}
//
//	public void setOptLock(long optLock) {
//		this.optLock = optLock;
//	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

//	public Date getUpdatedDate() {
//		return this.updatedDate;
//	}
//
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}
//
//	public String getUpdatedUser() {
//		return this.updatedUser;
//	}
//
//	public void setUpdatedUser(String updatedUser) {
//		this.updatedUser = updatedUser;
//	}

}