package com.ufis_as.ek_if.bms.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * @author btr
 * The persistent class for the CREW_CHECKIN database table.
 * 
 */
@Entity
@Table(name="CREW_CHECKIN")
@NamedQueries({
	@NamedQuery(name = "EntDbCrewCheckin.findByKey", query = "SELECT e FROM EntDbCrewCheckin e WHERE e.flightNumber = :flNum AND e.fltDate = :flightDate AND e.depNum = :depNum AND e.legNum = :legNum AND e.staffNumber = :staffNum"),
	@NamedQuery(name = "EntDbCrewCheckin.findByIdFlightAndStaffNum", query = "SELECT e FROM EntDbCrewCheckin e WHERE e.idFlight = :idFlight AND e.staffNumber = :staffNumber AND e.staffType = :staffType AND e.idFltJobAssign <> :idFltJobAssign AND e.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbCrewCheckin.findByIdFlightAndIdFltJobAssign", query = "SELECT e FROM EntDbCrewCheckin e WHERE e.idFlight = :idFlight AND e.idFltJobAssign = :idFltJobAssign AND e.recStatus <> :recStatus")
	
})
public class EntDbCrewCheckin extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CHECKIN_DATE")
	private Date checkinDate;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DEP_NUM")
	private String depNum;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="FLT_DEST3")
	private String fltDest3;

	@Column(name="FLT_ORIGIN3")
	private String fltOrigin3;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_FLT_JOB_ASSIGN")
	private String idFltJobAssign;

	@Column(name="LEG_NUM")
	private String legNum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="SELF_CHECKIN_FLAG")
	private String selfCheckinFlag;

	@Column(name="STAFF_NAME")
	private String staffName;

	@Column(name="STAFF_NUMBER")
	private String staffNumber;

	@Column(name="STAFF_OP_GRADE")
	private String staffOpGrade;

	@Column(name="STAFF_TYPE")
	private String staffType;

	public EntDbCrewCheckin() {
	}

	public EntDbCrewCheckin(EntDbCrewCheckin obj) {
		this.idFlight = obj.idFlight;
		this.idFltJobAssign = obj.idFltJobAssign;
		this.staffNumber = obj.staffNumber;
		this.staffType = obj.staffType;
		this.checkinDate = obj.checkinDate;
		this.selfCheckinFlag = obj.selfCheckinFlag;
	}
	
	public Date getCheckinDate() {
		return this.checkinDate;
	}

	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDepNum() {
		return this.depNum;
	}

	public void setDepNum(String depNum) {
		this.depNum = depNum;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdFltJobAssign() {
		return this.idFltJobAssign;
	}

	public void setIdFltJobAssign(String idFltJobAssign) {
		this.idFltJobAssign = idFltJobAssign;
	}

	public String getLegNum() {
		return this.legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getSelfCheckinFlag() {
		return this.selfCheckinFlag;
	}

	public void setSelfCheckinFlag(String selfCheckinFlag) {
		this.selfCheckinFlag = selfCheckinFlag;
	}

	public String getStaffName() {
		return this.staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffNumber() {
		return this.staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffOpGrade() {
		return this.staffOpGrade;
	}

	public void setStaffOpGrade(String staffOpGrade) {
		this.staffOpGrade = staffOpGrade;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
}