package com.ufis_as.ek_if.connection;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;

/**
 * @author btr 
 * The persistent class for the FLT_CONNECT_SUMMARY database table.
 */

@Entity
@Table(name="FLT_CONNECT_SUMMARY")
@NamedQueries({
	@NamedQuery(name="EntDbFltConnectSummary.findExisting", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idArrFlight = :idArrFlight AND a.idDepFlight = :idDepFlight AND a.recStatus <> 'X'"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingForAllFlights", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.recStatus <> 'X' AND ((a.idArrFlight = :idArrFlight AND a.idDepFlight = :idDepFlight) OR (a.idDepFlight = :idArrFlight AND a.idArrFlight = :idDepFlight))"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingWithNoConxFlightsForULD", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idArrFlight in (:idArrFlight,:idDepFlight) AND a.idDepFlight in (:idArrFlight,:idDepFlight) AND (conxStatUld IS NULL OR conxStatUld = ' ' OR conxStatUld = '' OR conxStatUld = 'X')"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingWithNoConxFlightsForBag", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idArrFlight in (:idArrFlight,:idDepFlight) AND a.idDepFlight in (:idArrFlight,:idDepFlight) AND (conxStatBag IS NULL OR conxStatBag = ' ' OR conxStatBag = '' OR conxStatBag = 'X')"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingWithNoConxFlightsForPax", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idArrFlight in (:idArrFlight,:idDepFlight) AND a.idDepFlight in (:idArrFlight,:idDepFlight) AND (conxStatPax IS NULL OR conxStatPax = ' ' OR conxStatPax = '' OR conxStatPax = 'X')"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingForFlights", query="SELECT a FROM EntDbFltConnectSummary a WHERE (a.idArrFlight = :idFlight OR a.idDepFlight = :idFlight) AND recStatus <> 'X'"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingForArrival", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idArrFlight = :idFlight AND recStatus = ' '"),
	@NamedQuery(name="EntDbFltConnectSummary.findExistingForDeparture", query="SELECT a FROM EntDbFltConnectSummary a WHERE a.idDepFlight = :idFlight AND recStatus = ' '")
})
public class EntDbFltConnectSummary extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CONX_DEC_BAG")
	private String conxDecBag;

	@Column(name="CONX_DEC_PAX")
	private String conxDecPax;

	@Column(name="CONX_DEC_ULD")
	private String conxDecUld;

	@Column(name="CONX_STAT_BAG")
	private String conxStatBag;

	@Column(name="CONX_STAT_PAX")
	private String conxStatPax;

	@Column(name="CONX_STAT_ULD")
	private String conxStatUld;

	@Column(name="CONX_TIME")
	private BigDecimal conxTime;

	@Column(name="ID_ARR_FLIGHT")
	private BigDecimal idArrFlight;

	@Column(name="ID_DEP_FLIGHT")
	private BigDecimal idDepFlight;
	
	@Column(name="PAX_JOURNEY_TIME")
	private BigDecimal paxJourneyTime;

	@Column(name="REC_STATUS")
	private String recStatus;
	
	@Column(name = "TIFA")
	@Temporal(value = TemporalType.TIMESTAMP)
    private Date tifa;
	
    @Column(name = "TIFD")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date tifd;

	public EntDbFltConnectSummary() {
	}
	
	private EntDbFltConnectSummary(EntDbFltConnectSummary entity) {
		this.id = entity.id;
		this._createdDate = entity._createdDate;
		this._createdUser = entity._createdUser;
		this._updatedDate = entity._updatedDate;
		this._updatedUser = entity._updatedUser;
		this.version = entity.version;
		this.recStatus = entity.recStatus;
		//this.dataSource = entity.dataSource;
		
		this.conxDecBag = entity.conxDecBag;
		this.conxDecPax = entity.conxDecPax;
		this.conxDecUld = entity.conxDecUld;
		this.conxStatBag = entity.conxStatBag;
		this.conxStatPax = entity.conxStatPax;
		this.conxStatUld = entity.conxStatUld;
		this.conxTime = entity.conxTime;
		this.idArrFlight = entity.idArrFlight;
		this.idDepFlight = entity.idDepFlight;
		this.tifa = entity.tifa;
		this.tifd = entity.tifd;
	}
	
	public static EntDbFltConnectSummary valueOf(EntDbFltConnectSummary entity) {
		return new EntDbFltConnectSummary(entity);
	}

	public String getConxDecBag() {
		return this.conxDecBag;
	}

	public void setConxDecBag(String conxDecBag) {
		this.conxDecBag = conxDecBag;
	}

	public String getConxDecPax() {
		return this.conxDecPax;
	}

	public void setConxDecPax(String conxDecPax) {
		this.conxDecPax = conxDecPax;
	}

	public String getConxDecUld() {
		return this.conxDecUld;
	}

	public void setConxDecUld(String conxDecUld) {
		this.conxDecUld = conxDecUld;
	}

	public String getConxStatBag() {
		return this.conxStatBag;
	}

	public void setConxStatBag(String conxStatBag) {
		this.conxStatBag = conxStatBag;
	}

	public String getConxStatPax() {
		return this.conxStatPax;
	}

	public void setConxStatPax(String conxStatPax) {
		this.conxStatPax = conxStatPax;
	}

	public String getConxStatUld() {
		return this.conxStatUld;
	}

	public void setConxStatUld(String conxStatUld) {
		this.conxStatUld = conxStatUld;
	}

	public BigDecimal getConxTime() {
		return this.conxTime;
	}

	public void setConxTime(BigDecimal conxTime) {
		this.conxTime = conxTime;
	}

	public BigDecimal getIdArrFlight() {
		return this.idArrFlight;
	}

	public void setIdArrFlight(BigDecimal idArrFlight) {
		this.idArrFlight = idArrFlight;
	}

	public BigDecimal getIdDepFlight() {
		return this.idDepFlight;
	}

	public void setIdDepFlight(BigDecimal idDepFlight) {
		this.idDepFlight = idDepFlight;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}
	
	public BigDecimal getPaxJourneyTime() {
		return paxJourneyTime;
	}
	
	public void setPaxJourneyTime(BigDecimal paxJourneyTime) {
		this.paxJourneyTime = paxJourneyTime;
	}
	
	public Date getTifa() {
		return tifa;
	}
	
	public void setTifa(Date tifa) {
		this.tifa = tifa;
	}
	
	public Date getTifd() {
		return tifd;
	}
	
	public void setTifd(Date tifd) {
		this.tifd = tifd;
	}
}