package com.ufis_as.ek_if.connection;

import java.math.BigDecimal;

public class FltConnectionDTO {

	private BigDecimal idFlight;
	private BigDecimal idConxFlight;
	
	private int bagPcs = 0;
	private int totalPax = 0;
	private int uldPcs = 0;
	
	private int criticalCount = 0;
	
	public FltConnectionDTO() {
		
	}
	
	public FltConnectionDTO(BigDecimal totalpax, BigDecimal idConxflight) {
		this.totalPax = totalpax.intValue();
		this.idConxFlight = idConxflight;
	}

	public BigDecimal getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public BigDecimal getIdConxFlight() {
		return idConxFlight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public int getBagPcs() {
		return bagPcs;
	}

	public void setBagPcs(int bagPcs) {
		this.bagPcs = bagPcs;
	}

	public int getTotalPax() {
		return totalPax;
	}

	public void setTotalPax(int totalPax) {
		this.totalPax = totalPax;
	}

	public int getUldPcs() {
		return uldPcs;
	}

	public void setUldPcs(int uldPcs) {
		this.uldPcs = uldPcs;
	}

	public int getCriticalCount() {
		return criticalCount;
	}

	public void setCriticalCount(int criticalCount) {
		this.criticalCount = criticalCount;
	}
	
	
}
