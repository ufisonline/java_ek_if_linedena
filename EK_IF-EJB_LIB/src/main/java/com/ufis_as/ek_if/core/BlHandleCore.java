/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.core;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.josql.QueryExecutionException;
import org.josql.QueryParseException;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.handle.BlHandle;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJDCFTAB;
import com.ufis_as.exco.INFOJDCFTABLIST;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.core.flightevent.FlightEvent;
import ek.core.flightevent.FlightEvent.FlightId;

/**
 * @author $Author$
 * @version $Revision$
 */
public class BlHandleCore extends BlHandle {

    /**
     * Logger
     */
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BlHandleCore.class);
    protected static final String MSGIF = "EK-CORE";
    protected static final String EK_PKG = "select * from ek.core.flightevent.";
    //private static final String FIND_FLIGHT = "FlightEvent where flightId.CxCd = :CxCd and  flightId.FltNum = :FltNum and flightId.FltSuffix = :FltSuffix and flightId.DepNum = :DepNum  and flightId.FltDate = :FltDate and flightId.DepStn = :DepStn and flightId.ArrStn = :ArrStn";
    private static final String FIND_FLIGHT = "FlightEvent where flightId.CxCd = :CxCd and  flightId.FltNum = :FltNum and flightId.DepNum = :DepNum  and flightId.FltDate.value = :FltDate and flightId.DepStn = :DepStn and flightId.ArrStn = :ArrStn";
    private static final String FIND_SORT_FLIGHT = "FlightEvent where flightId.CxCd = :CxCd and  flightId.FltNum = :FltNum and flightId.DepNum = :DepNum  and flightId.FltDate.value = :FltDate order by flightId.legNum ";
    //private static final String FIND_FLIGHT_VIAS = "FlightEvent where flightId.CxCd = :CxCd and  flightId.FltNum = :FltNum and flightId.FltSuffix = :FltSuffix and flightId.DepNum = :DepNum  and flightId.FltDate.value = :FltDate and  flightId.DepStn != :DepStn and flightId.ArrStn != :ArrStn order by flightId.LegNum";
    private static final String FIND_FLIGHT_VIAS = "FlightEvent where flightId.CxCd = :CxCd and  flightId.FltNum = :FltNum  and flightId.DepNum = :DepNum  and flightId.FltDate.value = :FltDate and  flightId.DepStn != :DepStn and flightId.ArrStn != :ArrStn order by flightId.legNum ";
    private List<FlightEvent> _globalFlightEvent;
    private FlightEvent flightData;

    public BlHandleCore(
            Marshaller marshaller, Unmarshaller um,
            InterfaceConfig interfaceConfig) {
        super(marshaller, um, interfaceConfig);

    }

    @Override
    public boolean unMarshal() {
        try {
            flightData = (FlightEvent) _um.unmarshal(new StreamSource(new StringReader(_flightXml)));
//            if (readEvent(flightData)) {
//                LOG.debug("FlightEvent {}", flightData.getFlightId().getFltNum());
            return true;
//            } else {
//                return false;
//            }
        } catch (JAXBException ex) {
            LOG.error("Flight Event JAXB exception {}", ex.toString());
            return false;
        }
    }

    public ACTIONTYPE readEvent(FlightEvent flightEvent) {
        if (flightEvent != null) {
            //Check the Message type
            _messageType = flightEvent.getMessageType();
            _actionType = flightEvent.getActionType();
            if (_actionType.equalsIgnoreCase("INSERT")) {
                _msgActionType = ACTIONTYPE.I;
            } else if (_actionType.equalsIgnoreCase("UPDATE")) {
                _msgActionType = ACTIONTYPE.U;
            } else {
                _msgActionType = ACTIONTYPE.D;
            }
            //Check If flight exists in AFTTAB
            /*
             CxCd
             FltNum
             FltSuffix
             DepNum
             FltDate
             DepStn
             ArrStn
             */
            FlightEvent rflightEvent = findFlightEvent(flightEvent);
            if (rflightEvent != null) {
                // LOG.info("FlightEvent Found {}", rflightEvent.getFlightId().getFltNum());
            } else {
                _globalFlightEvent.add(flightEvent);
            }
//            return tranformFlight(flightEvent);
        } else {
            LOG.warn("Flight Event Null detected");
//            return false;
        }
        return _msgActionType;
    }

    public boolean tranformFlight(FlightEvent flightEvent, EntDbAfttab aftFlight) {
        HpUfisCalendar ufisCalendar = new HpUfisCalendar();
        INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
        INFOJXAFTAB infojxaftab = new INFOJXAFTAB();
        INFOJDCFTABLIST infojdcftablist = new INFOJDCFTABLIST();
        //INFOJFEVTAB infojfevtab = new INFOJFEVTAB();
        //INFOJFEVTABLIST infojfevtablist = new INFOJFEVTABLIST();
        FlightEvent.FlightId flightId = flightEvent.getFlightId();
        if (flightId != null) {
            //Event Actuall Flight Data
            if (!flightEvent.getEvents().isEmpty() && flightEvent.getEvents().size() == 1) {
                FlightEvent.Events event = flightEvent.getEvents().get(0);
                //Service Type
                infobjFlight.setSTYP(event.getServiceType());
                //AC Type
                //IATA aircraft subtype as per SSIM
                infobjFlight.setACT3(event.getAcSubType());
                //afttab.setAct5(event.getAcVersion());
                // Registration
                //Action event.getTailNo().getActionType() ??
                //ActionType : Assigned
                // 2013-05-14 updated by JGO - if 4 digits regn, ignored
                if (event.getTailNo() != null && event.getTailNo().getValue() != null) {
                	if (event.getTailNo().getValue().length() == 4) {
                		infobjFlight.setREGN("");
                	} else {
                		infobjFlight.setREGN(event.getTailNo().getValue());
                	}
                }
                //A/C Owner
                infojxaftab.setACOW(event.getAcOwner());
                //A/C Version
                infojxaftab.setACEX(event.getAcVersion());
                //
                //TaxiOut / IN
                if (event.getTaxiOut() != null) {
                    infobjFlight.setEXOT(event.getTaxiOut().getValue().toString());
                }
                if (event.getTaxiIn() != null) {
                    infobjFlight.setEXIT(event.getTaxiIn().getValue().toString());
                }
                //Flight Status
                /*
                 *
                 CODE STATUS_DESCRIPTION
                 1 RTNG RETURNED, IN FLIGHT
                 2 DVTG DIVERTED, IN FLIGHT
                 3 PDEP NOT YET DEPARTED
                 4 OFBL TAXIING FOR TAKEOFF
                 5 ENRT IN FLIGHT
                 6 LAND LANDED AND TAXIING
                 7 ARVD ARRIVED
                 8 CNLD CANCELLED
                 9 DVTD ARRIVED DIVERSION AIRPORT
                 10 DNLD TAXIING DIVERSION AIRPORT
                 11 RROF REROUTED, TAXIING
                 12 RRTG REROUTED, IN FLIGHT
                 13 RRTD REROUTED, LANDED
                 14 RTDR RETURNED TO STAND
                 15 RTND RETURNED DEPARTURE AIRPORT
                 16 DLND DIVERTED
                 17 RRLD REROUTED AND LANDING
                 */
                String flightStatus = event.getFltStatus();
                //TODO how will identify Schedule from Operational
                infobjFlight.setFTYP("S");
                switch (flightStatus) {
                    case "RTNG":
                    case "PRF":
                    case "RRTG":
                    case "RRTD":
                    case "RTND":
                    case "RRLD":
                        infobjFlight.setADID(ADID.B);
                        infobjFlight.setFTYP("Z");
                        break;
                    case "CNLD":
                        infobjFlight.setFTYP("X");
                        break;
                    case "DVTG":
                    case "DVTD":
                    case "DNLD":
                        infobjFlight.setFTYP("D");
                        break;
                }
                //Airline
                if (flightId.getCxCd().length() == 2) {
                    infobjFlight.setALC2(flightId.getCxCd());
                } else {
                    infobjFlight.setALC3(flightId.getCxCd());
                }
                infobjFlight.setFLTN(HpUfisUtils.formatCedaFltn(flightId.getFltNum()));
                //Suffix
                if (HpUfisUtils.isNotEmptyStr(flightId.getFltSuffix())) {
                    infobjFlight.setFLNS(flightId.getFltSuffix());
                }
                //Arrival , Departure , Orig, Des
                if (flightId.getDepStn().equalsIgnoreCase(HOPO) && flightId.getArrStn().equalsIgnoreCase(HOPO)) {
                    //Return Flight /TAxi
                    infobjFlight.setADID(ADID.B);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getDepStn());
                } else if (flightId.getArrStn().equalsIgnoreCase(HOPO)) {
                    //Arival
                    infobjFlight.setADID(ADID.A);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getArrStn());
                } else if (flightId.getDepStn().equalsIgnoreCase(HOPO)) {
                    //Departure
                    infobjFlight.setADID(ADID.D);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getArrStn());
                } else {
                    // Check if this is VIA  or RETURN ?
                    //In case of a Return we would have set ADID just before
                    //if (infobjFlight.getADID() == null) {
                        //LOG.info("Flight which a part of a VIA detected {}", flightStatus);
                	if (aftFlight != null) {
                		infobjFlight.setADID(ADID.fromValue(aftFlight.getAdid().toString()));
                	} else {
                        infobjFlight.setADID(ADID.D);
                	}
                  	    //return true;
                    //}
                }
                //Schedule Date / time
                ufisCalendar.setTime(event.getSchdDateTime().get(0).getValue());
                if (infobjFlight.getADID() != null) {
	                if (ADID.A.equals(infobjFlight.getADID())) {
	                    infobjFlight.setSTOA(ufisCalendar.getCedaString());
	                }
	                if (ADID.D.equals(infobjFlight.getADID())) {
	                    infobjFlight.setSTOD(ufisCalendar.getCedaString());
	                }
	                /* TODO What we should for Return Flight ? */
	                if (ADID.B.equals(infobjFlight.getADID())) {
	                    infobjFlight.setSTOD(ufisCalendar.getCedaString());
	                    infobjFlight.setSTOA(ufisCalendar.getCedaString());
	                }
                }
                ufisCalendar.setTime(event.getFltDate().getValue());
                infobjFlight.setFLDA(ufisCalendar.getCedaDateString());
                
                //Get The Vias from cache
                List<FlightEvent> vias = findFlightEventVia(flightEvent, infobjFlight.getADID());
                //TODO in UFIS we Store ORG /  DES  so
                //So we must get the First Leg from the Arrival VIA's in order to find original ORG
                //and the Last Leg from the Destination in order to find the Final DES
                //This should be for the times also
                //Do we need really this feature for this Application ?
                //In case of a DIV flight we should (CXX the middle VIA)
                //Being an EK flight, it would still have the final destination being DXB. Airlines
                //do not get traffic rights between none home airports,
                //unless the flight has ORG/DES ad the home airport.
                if (vias.size() > 0 && !infobjFlight.getFTYP().equals("D")) {
                	// no matter msg action type is insert or update for via, will always be update
                	_msgActionType = ACTIONTYPE.U;
                    StringBuilder vial = new StringBuilder();
                    for (Iterator<FlightEvent> it = vias.iterator(); it.hasNext();) {
                        FlightEvent viaFlightEvent = it.next();
                        vial.append(createVia(viaFlightEvent, infobjFlight.getADID(), ufisCalendar));
                    }
                    //TODO check if the following is correct
                    //Check the VIAN from LegNum (is this correct ?)
                    //infobjFlight.setVIAN(flightId.getLegNum().toString());
                    infobjFlight.setVIAN(Integer.toString(vias.size()));
                    infobjFlight.setVIAL(vial.toString());
                    if (ADID.A.equals(infobjFlight.getADID())) {
                        LOG.info("VIA Found  {} {} {} {}", new Object[]{infobjFlight.getADID(), vial.toString(), flightEvent.getFlightId().getDepStn(), flightEvent.getFlightId().getArrStn()});
                    } else {
                        LOG.info("VIA Found  {} {} {} {}", new Object[]{infobjFlight.getADID(), flightEvent.getFlightId().getDepStn(), flightEvent.getFlightId().getArrStn(), vial.toString()});
                    }
                }
                //infobjFlight.setREMP(flightStatus);
                infobjFlight.setREM1(flightStatus);
                infobjFlight.setDEPN(event.getDepNum().toString());
                List<FlightEvent.Events.SiRemark> siRemarks = event.getSiRemark();
                StringBuilder siRemarkBuild = new StringBuilder();
                for (Iterator<FlightEvent.Events.SiRemark> it = siRemarks.iterator(); it.hasNext();) {
                    FlightEvent.Events.SiRemark siRemark = it.next();
                    //TODO Check about if the Type is needed
                    siRemarkBuild.append(siRemark.getValue());
                }
                infobjFlight.setREM2(siRemarkBuild.toString());
                
                // 2013-04-18 updated by JGO - change fevtab to fevtab list
                // TODO check where to put the flag and remark
                //Door Flag
                //infojfevtab.setSTFL(event.getDorFlag());
                //infojfevtab.setSTRM(event.getDorRemark());
                //Cabin /Cargo doors times
                /*if (event.getCABINDOORCLOSURE() != null) {
                	INFOJFEVTAB cabinDoor = new INFOJFEVTAB();
                    ufisCalendar.setTime(event.getCABINDOORCLOSURE().getValue());
                    cabinDoor.setSTNM("CABIN DOOR CLOSURE");
                    cabinDoor.setSTTM(ufisCalendar.getCedaString());
                    cabinDoor.setSTFL("");
                    cabinDoor.setSTRM("");
                    infojfevtablist.getINFOJFEVTAB().add(cabinDoor);
                }
                if (event.getCARGODOORCLOSURE() != null) {
                	INFOJFEVTAB cargoDoor = new INFOJFEVTAB();
                    ufisCalendar.setTime(event.getCARGODOORCLOSURE().getValue());
                    cargoDoor.setSTNM("CARGO DOOR CLOSURE");
                    cargoDoor.setSTTM(ufisCalendar.getCedaString());
                    cargoDoor.setSTFL("");
                    cargoDoor.setSTRM("");
                    infojfevtablist.getINFOJFEVTAB().add(cargoDoor);
                }*/
                //Flight Duration
                infobjFlight.setFDUR(event.getFltTime().getValue().toString());
                //Schedule/Actual Block Time (min)
                infobjFlight.setSOOB(event.getBlkTimeOrig().getValue().toString());
                infobjFlight.setAOOB(event.getBlkTimeAct().getValue().toString());
                // Units to measurement for the fuel
                List<FlightEvent.Events.Fuel> fuels = event.getFuel();
                //                if (fuels.size() > 1) {
                //                    LOG.info("FlightEvent with Fuel size > 1");
                //                }
                //TODO for the moment CEDA can Habdle only one Fuel Record
                for (Iterator<FlightEvent.Events.Fuel> it = fuels.iterator(); it.hasNext();) {
                    FlightEvent.Events.Fuel fuel = it.next();
                    infojxaftab.setFUEU(fuel.getUnits());
                    infojxaftab.setFUET(fuel.getType());
                }
                //Get Delays
                List<FlightEvent.Delays.Delay> delays = flightEvent.getDelays().getDelay();
                for (Iterator<FlightEvent.Delays.Delay> it = delays.iterator(); it.hasNext();) {
                    FlightEvent.Delays.Delay delay = it.next();
                    INFOJDCFTAB infojdcftab = new INFOJDCFTAB();
                    ufisCalendar.setTime(delay.getPostedOn());
                    infojdcftab.setCDAT(ufisCalendar.getCedaString());
                    infojdcftab.setDECA(delay.getCode());
                    //TODO Check why Type is not needed
                    //infojdcftab.setDURA(delay.getType());
                    //Format to 0000
                    infojdcftab.setDURA(String.format("%04d", delay.getAmount()));
                    infojdcftablist.getINFOJDCFTAB().add(infojdcftab);
                }
                //Get Operational Times
                List<FlightEvent.Events.MvtDateTime> mvtDateTimes = event.getMvtDateTime();
                /*
                 Actual Take Off Time (Airline)		ATON ,AirborneAct – Actual Aiborne time
                 Estimated Landing Time (Airline)        ELDN,LandedEst – Estimated landing time
                 Actual Landing Time (Airline)		ALDN,LandedAct – Actual landing time
                 Estimated In-block Time (Airline)		EIBN,OnBlocksAct – Actual on block time
                 Actual In-block Time (Airline)		AIBN,OnBlocksEst – Estimated on block time
                 Estimated Off-block Time (Airline)		EOBN,OffBlocksEst – Estimated off block time
                 Actual Off-block Time (Airline)		AOBN,OffBlocksAct – Actual off block time
                 Estimated Take Off Time (Airline)		ETON,AirborneEst – Estimated Airborne time
                 Actual Start Of Taxi Time (Airline)		ASTN,StartTaxiAct – Actual time of start taxing
                 NXTI , NextInformation – Time at which next information for the flight shall be made available
                 *
                 */
                for (Iterator<FlightEvent.Events.MvtDateTime> it = mvtDateTimes.iterator(); it.hasNext();) {
                    FlightEvent.Events.MvtDateTime mvtDateTime = it.next();
                    String mvtType = mvtDateTime.getType();
                    ufisCalendar.setTime(mvtDateTime.getValue());
                    String cedaTime = ufisCalendar.getCedaString();
                    //LOG.info("MvtType {} ceda time {}", new Object[]{mvtType, afttab.getADID().toString()}, ufisCalendar.getCedaString());
                    switch (mvtType) {
                        case "AirborneAct":
                            infobjFlight.setATON(cedaTime);
                            break;
                        case "LandedEst":
                            infobjFlight.setELDN(cedaTime);
                            break;
                        case "LandedAct":
                            infobjFlight.setALDN(cedaTime);
                            break;
                        case "OnBlocksAct":
                            infobjFlight.setEIBN(cedaTime);
                            break;
                        case "OnBlocksEst":
                            infobjFlight.setAIBN(cedaTime);
                            break;
                        case "OffBlocksEst":
                            infobjFlight.setEOBN(cedaTime);
                            break;
                        case "OffBlocksAct":
                            infobjFlight.setAOBN(cedaTime);
                            break;
                        case "AirborneEst":
                            infobjFlight.setETON(cedaTime);
                            break;
                        case "StartTaxiAct":
                            infobjFlight.setASTN(cedaTime);
                            break;
                        case "NextInformation":
                            infobjFlight.setNXTI(cedaTime);
                            infojxaftab.setNEXI(cedaTime);
                            break;
                    }
                }
            } else {
                LOG.info("FlightEven (event) size empty or > 1");
            }
            
            /* TODO 
             * if delete msg doesnt contain hopo, change action type to update and update via(maybe org and dest)
             * if delete msg contains hopo, consider as flight delete?
             * 
             */
            // Delete set infojflight
            if (ACTIONTYPE.D == _msgActionType && aftFlight != null) {
            	
            	//Airline
                if (flightId.getCxCd().length() == 2) {
                    infobjFlight.setALC2(flightId.getCxCd());
                } else {
                    infobjFlight.setALC3(flightId.getCxCd());
                }
                infobjFlight.setFLTN(HpUfisUtils.formatCedaFltn(flightId.getFltNum()));
                //Suffix
                if (HpUfisUtils.isNotEmptyStr(flightId.getFltSuffix())) {
                    infobjFlight.setFLNS(flightId.getFltSuffix());
                }
                // depNum
                infobjFlight.setDEPN(String.valueOf(flightId.getDepNum()));
                // adid
                infobjFlight.setADID(ADID.fromValue(aftFlight.getAdid().toString()));
                //Schedule Date / time
                if (ADID.A.equals(infobjFlight.getADID())) {
                    infobjFlight.setSTOA(aftFlight.getStoa());
                }
                if (ADID.D.equals(infobjFlight.getADID())) {
                    infobjFlight.setSTOD(aftFlight.getStod());
                }
                /* TODO What we should for Return Flight ? */
                if (ADID.B.equals(infobjFlight.getADID())) {
                    infobjFlight.setSTOD(aftFlight.getStoa());
                    infobjFlight.setSTOA(aftFlight.getStod());
                }
                infobjFlight.setFLDA(aftFlight.getFlda());
                
                // if delete msg contains hopo, flight delete, otherwise update via
                if (HOPO.equalsIgnoreCase(flightId.getDepStn()) && HOPO.equalsIgnoreCase(flightId.getArrStn())) {
                    //Return Flight /TAxi
                    infobjFlight.setADID(ADID.B);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getDepStn());
                } else if (HOPO.equalsIgnoreCase(flightId.getArrStn())) {
                    //Arival
                    infobjFlight.setADID(ADID.A);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getArrStn());
                } else if (HOPO.equalsIgnoreCase(flightId.getDepStn())) {
                    //Departure
                    infobjFlight.setADID(ADID.D);
                    infobjFlight.setORG3(flightId.getDepStn());
                    infobjFlight.setDES3(flightId.getArrStn());
                } else {
                	// via delete message (change action type to update)
                	_msgActionType = ACTIONTYPE.U;
                	
                	// remove from cache 
                	removeFlight(flightId);
                	
                	// get all flighs msg relate to this flight from cache
                    List<FlightEvent> flights = findFlight(flightEvent);
                    if (flights != null && flights.size() > 0) {
                    	// no matter msg action type is insert or update for via, will always be update
                    	_msgActionType = ACTIONTYPE.U;
                        StringBuilder vial = new StringBuilder();
                        Iterator<FlightEvent> it = flights.iterator();
                        boolean isFirst = true;
                        while (it.hasNext()) {
                        	FlightEvent flight = it.next();
                        	// first record
                        	if (isFirst) {
                        		infobjFlight.setORG3(flight.getFlightId().getDepStn());
                        		isFirst = false;
                        	}
                        	// last record
                        	if (!it.hasNext()) {
                        		infobjFlight.setDES3(flight.getFlightId().getArrStn());
                        		break;
                        	}
                        	// update vias
                            vial.append(createVia(flight, infobjFlight.getADID(), ufisCalendar));
                        }
                        //TODO check if the following is correct
                        //Check the VIAN from LegNum (is this correct ?)
                        //infobjFlight.setVIAN(flightId.getLegNum().toString());
                    	infobjFlight.setVIAN(Integer.toString(flights.size() -1));
                        infobjFlight.setVIAL(vial.toString());
                        if (infobjFlight.getADID().equals(ADID.A)) {
                            LOG.info("VIA Found  {} {} {} {}", new Object[]{infobjFlight.getADID(), vial.toString(), flightEvent.getFlightId().getDepStn(), flightEvent.getFlightId().getArrStn()});
                        } else {
                            LOG.info("VIA Found  {} {} {} {}", new Object[]{infobjFlight.getADID(), flightEvent.getFlightId().getDepStn(), flightEvent.getFlightId().getArrStn(), vial.toString()});
                        }
                    }
                }
            }
            
            MSGOBJECTS msgobjects = new MSGOBJECTS();
            INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
            /* Set the fields that uniquely identify a flight
             * the rest are done in the UfisMarshal
             */
            infobjgeneric.setDEPN(infobjFlight.getDEPN());
            msgobjects.setINFOBJFLIGHT(infobjFlight);
            //msgobjects.setINFOJFEVTAB(infojfevtab);
            //msgobjects.setINFOJFEVTABLIST(infojfevtablist);
            msgobjects.setINFOJXAFTAB(infojxaftab);
            msgobjects.setINFOJDCFTABLIST(infojdcftablist);
            UfisMarshal ufisMarshal = new UfisMarshal(_ms, _msgActionType, MSGIF, _interfaceConfig);
            _returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);
            return true;
            //marshalFlightEvent(infobjFlight, msgobjects);
        }
        return false;
    }

    public FlightEvent findFlightEvent(FlightEvent flightEvent) {
        // Create a new Query.
        org.josql.Query q = new org.josql.Query();
        List returnCache = new ArrayList();
        FlightEvent rflightEvent = null;
        int countCache = 0;
        try {
            /*
             CxCd
             FltNum
             FltSuffix
             DepNum
             FltDate
             DepStn
             ArrStn
             */
            // Parse the SQL you are going to use.
            q.parse(EK_PKG + FIND_FLIGHT);
            q.setVariable("CxCd", flightEvent.getFlightId().getCxCd());
            q.setVariable("FltNum", flightEvent.getFlightId().getFltNum());
            //  q.setVariable("FltSuffix", flightEvent.getFlightId().getFltSuffix());
            q.setVariable("DepNum", flightEvent.getFlightId().getDepNum());
            q.setVariable("FltDate", flightEvent.getFlightId().getFltDate().getValue());
            q.setVariable("DepStn", flightEvent.getFlightId().getDepStn());
            q.setVariable("ArrStn", flightEvent.getFlightId().getArrStn());

//            LOG.info("Flight {} {} {} {} {} {} {}", new Object[]{
//                        flightEvent.getFlightId().getCxCd(),
//                        flightEvent.getFlightId().getFltNum(),
//                        flightEvent.getFlightId().getFltSuffix(),
//                        flightEvent.getFlightId().getDepNum(),
//                        flightEvent.getFlightId().getFltDate().getValue(),
//                        flightEvent.getFlightId().getDepStn(),
//                        flightEvent.getFlightId().getArrStn()});
        } catch (QueryParseException ex) {
            LOG.info("JoSQL QueryParseException " + ex.toString());
        }

        // Execute the query.
        org.josql.QueryResults qr;
        try {
            //LOG.info(q.getWhereClause().toString());
            qr = q.execute(_globalFlightEvent);
            // Get the query results.
            returnCache = qr.getResults();
            if (returnCache.size() > 0) {
                rflightEvent = (FlightEvent) returnCache.get(0);
            }
            countCache = returnCache.size();
        } catch (QueryExecutionException ex) {
            LOG.info("JoSQL QueryExecutionException " + ex.toString());
        }

        return rflightEvent;
    }
    
    public FlightEvent findFlightWithHopo(FlightEvent flightEvent) {
        // Create a new Query.
        org.josql.Query q = new org.josql.Query();
        List<FlightEvent> returnCache = new ArrayList<>();
        FlightEvent rflightEvent = null;
        int countCache = 0;
        try {
            /*
             CxCd
             FltNum
             FltSuffix
             DepNum
             FltDate
             DepStn
             ArrStn
             */
            // Parse the SQL you are going to use.
            q.parse(EK_PKG + FIND_FLIGHT);
            q.setVariable("CxCd", flightEvent.getFlightId().getCxCd());
            q.setVariable("FltNum", flightEvent.getFlightId().getFltNum());
            //  q.setVariable("FltSuffix", flightEvent.getFlightId().getFltSuffix());
            q.setVariable("DepNum", flightEvent.getFlightId().getDepNum());
            q.setVariable("FltDate", flightEvent.getFlightId().getFltDate().getValue());
            q.setVariable("DepStn", HOPO);
            q.setVariable("ArrStn", HOPO);

        } catch (QueryParseException ex) {
            LOG.info("JoSQL QueryParseException " + ex.toString());
        }

        // Execute the query.
        org.josql.QueryResults qr;
        try {
            //LOG.info(q.getWhereClause().toString());
            qr = q.execute(_globalFlightEvent);
            // Get the query results.
            returnCache = qr.getResults();
            if (returnCache.size() > 0) {
                rflightEvent = (FlightEvent) returnCache.get(0);
            }
            countCache = returnCache.size();
        } catch (QueryExecutionException ex) {
            LOG.info("JoSQL QueryExecutionException " + ex.toString());
        }

        return rflightEvent;
    }
    
    public List<FlightEvent> findFlight(FlightEvent flightEvent) {
        // Create a new Query.
        org.josql.Query q = new org.josql.Query();
        List<FlightEvent> returnCache = new ArrayList<>();
        try {
            /*
             CxCd
             FltNum
             FltSuffix
             DepNum
             FltDate
             DepStn
             ArrStn
             */
            // Parse the SQL you are going to use.
            q.parse(EK_PKG + FIND_SORT_FLIGHT);
            q.setVariable("CxCd", flightEvent.getFlightId().getCxCd());
            q.setVariable("FltNum", flightEvent.getFlightId().getFltNum());
            //  q.setVariable("FltSuffix", flightEvent.getFlightId().getFltSuffix());
            q.setVariable("DepNum", flightEvent.getFlightId().getDepNum());
            q.setVariable("FltDate", flightEvent.getFlightId().getFltDate().getValue());
        } catch (QueryParseException ex) {
            LOG.info("JoSQL QueryParseException " + ex.toString());
        }

        // Execute the query.
        org.josql.QueryResults qr;
        try {
            //LOG.info(q.getWhereClause().toString());
            qr = q.execute(_globalFlightEvent);
            // Get the query results.
            returnCache = qr.getResults();
        } catch (QueryExecutionException ex) {
            LOG.info("JoSQL QueryExecutionException " + ex.toString());
        }

        return returnCache;
    }

    public List<FlightEvent> findFlightEventVia(FlightEvent flightEvent, ADID flightAdid) {
        // Create a new Query.
        org.josql.Query q = new org.josql.Query();

        List<FlightEvent> rflightEvent = new ArrayList<>();
        int countCache = 0;
        try {
            /*
             CxCd
             FltNum
             FltSuffix
             DepNum
             FltDate
             DepStn
             ArrStn
             */
            // Parse the SQL you are going to use.
//            if (flightAdid.equals(ADID.A)) {
            q.parse(EK_PKG + FIND_FLIGHT_VIAS);
//            } else {
//                q.parse(EK_PKG + FIND_FLIGHT_VIAS);
//            }

            q.setVariable("CxCd", flightEvent.getFlightId().getCxCd());
            q.setVariable("FltNum", flightEvent.getFlightId().getFltNum());
            //  q.setVariable("FltSuffix", flightEvent.getFlightId().getFltSuffix());
            q.setVariable("DepNum", flightEvent.getFlightId().getDepNum());
            q.setVariable("FltDate", flightEvent.getFlightId().getFltDate().getValue());
            q.setVariable("DepStn", flightEvent.getFlightId().getDepStn());
            q.setVariable("ArrStn", flightEvent.getFlightId().getArrStn());

//            LOG.debug("Flight {} {} {} {} {} {} {}", new Object[]{
//                        flightEvent.getFlightId().getCxCd(),
//                        flightEvent.getFlightId().getFltNum(),
//                        flightEvent.getFlightId().getFltSuffix(),
//                        flightEvent.getFlightId().getDepNum(),
//                        flightEvent.getFlightId().getFltDate().getValue(),
//                        flightEvent.getFlightId().getDepStn(),
//                        flightEvent.getFlightId().getArrStn()});
        } catch (QueryParseException ex) {
            LOG.info("JoSQL QueryParseException " + ex.toString());
        }

        // Execute the query.
        org.josql.QueryResults qr;
        try {
            //LOG.info(q.getWhereClause().toString());
            qr = q.execute(_globalFlightEvent);
            // Get the query results.
            rflightEvent = qr.getResults();

            countCache = rflightEvent.size();

        } catch (QueryExecutionException ex) {
            LOG.info("JoSQL QueryExecutionException " + ex.toString());
        }

        return rflightEvent;
    }

    //TODO
    //format the VIA to a CEDA VIA Format
    /*EXPLODE_DETAILS = VIAS
     EXPLODE_VIAS = VIAL,8,120,VIA
     VIA_1 =  FLG,Display Flag,1,CHAR, ,N, ,
     VIA_2 =  AP3,APC3 of Via,3,CHAR, ,N, ,
     VIA_3 =  AP4,APC4 of Via,4,CHAR, ,N, ,
     VIA_4 =  STA,STOA at Via,14,DATE, ,N, ,
     VIA_5 =  ETA,ETOA at Via,14,DATE, ,N, ,
     VIA_6 =  LND,LAND at Via,14,DATE, ,N, ,
     VIA_7 =  ONB,ONBL at Via,14,DATE, ,N, ,
     VIA_8 =  STD,STOD at Via,14,DATE, ,N, ,
     VIA_9 =  ETD,ETOD at Via,14,DATE, ,N, ,
     VIA_10 = OFB,OFBL at Via,14,DATE, ,N, ,
     VIA_11 = AIR,AIRB at Via,14,DATE, ,N, ,
     */
    private String createVia(FlightEvent viaFlightEvent, ADID adid, HpUfisCalendar ufisCalendar) {
        StringBuilder vial = new StringBuilder();
        FlightEvent.Events event = viaFlightEvent.getEvents().get(0);

        if (ADID.A.equals(adid)) {
            //VIA_1 FLG
            vial.append(String.format("%-" + 1 + "s", ""));
            //VIA_2 AP3
            vial.append(String.format("%-" + 3 + "s", viaFlightEvent.getFlightId().getDepStn()));
            //VIA_3 AP4
            vial.append(String.format("%-" + 4 + "s", ""));
            //VIA_4 STA
            ufisCalendar.setTime(event.getSchdDateTime().get(0).getValue());
            vial.append(String.format("%-" + 14 + "s", ufisCalendar.getCedaString()));
            //VIA_5 ETA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_6 LND
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_7 ONB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_8 STD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_9 ETD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_10 OFB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_11 AIR
            vial.append(String.format("%-" + 14 + "s", ""));
        } else {
            //VIA_1 FLG
            vial.append(String.format("%-" + 1 + "s", ""));
            //VIA_2 AP3
            vial.append(String.format("%-" + 3 + "s", viaFlightEvent.getFlightId().getArrStn()));
            //VIA_3 AP4
            vial.append(String.format("%-" + 4 + "s", ""));
            //VIA_4 STA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_5 ETA
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_6 LND
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_7 ONB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_8 STD
            ufisCalendar.setTime(event.getSchdDateTime().get(0).getValue());
            vial.append(String.format("%-" + 14 + "s", ufisCalendar.getCedaString()));
            //VIA_9 ETD
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_10 OFB
            vial.append(String.format("%-" + 14 + "s", ""));
            //VIA_11 AIR
            vial.append(String.format("%-" + 14 + "s", ""));
        }
        return vial.toString();
    }
    
    public void removeFlight(FlightId flightId) {
    	if (_globalFlightEvent != null) {
    		Iterator<FlightEvent> cacheIter = _globalFlightEvent.iterator();
    		while (cacheIter.hasNext()) {
    			FlightEvent event = cacheIter.next();
    			FlightId id = event.getFlightId();
    			if (id.getCxCd().equals(flightId.getCxCd()) 
    					&& id.getFltNum().equals(flightId.getFltNum())
    					&& id.getFltDate().getValue().equals(flightId.getFltDate().getValue())
    					&& id.getDepStn().equals(flightId.getDepStn())
    					&& id.getArrStn().equals(flightId.getArrStn())) {
    				if (flightId.getFltSuffix() != null 
        					&& flightId.getFltSuffix().equals(id.getFltSuffix())) {
    					cacheIter.remove();
    				} else if (flightId.getFltSuffix() == id.getFltSuffix()) {
    					cacheIter.remove();
    				}
    			}
    		}
    		// FlightEvent need to override the hashcode and equal method using flightId
    		//_globalFlightEvent.remove(flightId);
    	}
    }

    public List<FlightEvent> getGlobalFlightEvent() {
        return _globalFlightEvent;
    }

    public void setGlobalFlightEvent(List<FlightEvent> _globalFlightEvent) {
        this._globalFlightEvent = _globalFlightEvent;
    }

	public FlightEvent getFlightData() {
		return flightData;
	}

	public void setFlightData(FlightEvent flightData) {
		this.flightData = flightData;
	}
    
}
