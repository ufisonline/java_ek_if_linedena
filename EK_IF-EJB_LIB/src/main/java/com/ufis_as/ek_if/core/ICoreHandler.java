package com.ufis_as.ek_if.core;

import ek.core.flightevent.FlightEvent;

public interface ICoreHandler {
	
	public FlightEvent unMarshal(String message, long irmtabRef);
	
	public boolean tranformFlight(FlightEvent flightEvent);
	
	public String getReturnXml();

}
