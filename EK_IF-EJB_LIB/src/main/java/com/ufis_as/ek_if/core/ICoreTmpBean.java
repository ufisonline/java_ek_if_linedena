package com.ufis_as.ek_if.core;

import java.util.List;

import com.ufis_as.ek_if.core.entities.EntDbCoreTmp;
import com.ufis_as.ufisapp.server.eao.generic.IAbstractBeanRemote;

import ek.core.flightevent.FlightEvent.FlightId;

public interface ICoreTmpBean extends IAbstractBeanRemote<EntDbCoreTmp> {

	// Single result
	
	
	// Collection result
	public List<EntDbCoreTmp> getMsgById(FlightId id);
	public List<EntDbCoreTmp> getMsgChainById(FlightId id);

	// void
	public void updMsgStatus(FlightId criteria, boolean isChainUpd, Character status);

}
