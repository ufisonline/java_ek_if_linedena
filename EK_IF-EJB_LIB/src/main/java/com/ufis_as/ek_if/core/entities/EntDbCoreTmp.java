package com.ufis_as.ek_if.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntUfisID;

@Entity
@Table(name = "IF_CORE_TMP")
public class EntDbCoreTmp extends EntUfisID implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "HEADER_CXCD")
	private String cxcd;

	@Column(name = "HEADER_FLT_NUM")
	private String fltNum;

	@Column(name = "HEADER_FLT_SUFFIX")
	private String fltSuffix;

	@Column(name = "HEADER_FLT_DATE")
	private String fltDate;

	@Column(name = "HEADER_ARR_STN")
	private String arrStn;

	@Column(name = "HEADER_DEP_STN")
	private String depStn;

	@Column(name = "HEADER_DEP_NUM")
	private int depNum;

	@Column(name = "HEADER_LEG_NUM")
	private int legNum;
	
	@Column(name = "SCHEDULE_ARR")
	private String schArr;
	
	@Column(name = "SCHEDULE_DEP")
	private String schDep;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE")
	private Date created_date;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE")
	private Date updated_date;
	
	@Column(name = "REC_STATUS")
	private Character rec_status;
	
    @PrePersist
    void onPersistMaintenance() {
       this.setCreatedDate(new Date());
    }
    
    @PreUpdate
    void onUpdateMaintenance() {
       this.setUpdatedDate(new Date());
    }

	public String getCxcd() {
		return cxcd;
	}

	public void setCxcd(String cxcd) {
		this.cxcd = cxcd;
	}

	public String getFltNum() {
		return fltNum;
	}

	public void setFltNum(String fltNum) {
		this.fltNum = fltNum;
	}

	public String getFltSuffix() {
		return fltSuffix;
	}

	public void setFltSuffix(String fltSuffix) {
		this.fltSuffix = fltSuffix;
	}

	public String getFltDate() {
		return fltDate;
	}

	public void setFltDate(String fltDate) {
		this.fltDate = fltDate;
	}

	public String getArrStn() {
		return arrStn;
	}

	public void setArrStn(String arrStn) {
		this.arrStn = arrStn;
	}

	public String getDepStn() {
		return depStn;
	}

	public void setDepStn(String depStn) {
		this.depStn = depStn;
	}

	public int getDepNum() {
		return depNum;
	}

	public void setDepNum(int depNum) {
		this.depNum = depNum;
	}

	public int getLegNum() {
		return legNum;
	}

	public void setLegNum(int legNum) {
		this.legNum = legNum;
	}

	public String getSchArr() {
		return schArr;
	}

	public void setSchArr(String schArr) {
		this.schArr = schArr;
	}

	public String getSchDep() {
		return schDep;
	}

	public void setSchDep(String schDep) {
		this.schDep = schDep;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getUpdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	public Character getRecStatus() {
		return rec_status;
	}

	public void setRecStatus(Character rec_status) {
		this.rec_status = rec_status;
	}
	
}
