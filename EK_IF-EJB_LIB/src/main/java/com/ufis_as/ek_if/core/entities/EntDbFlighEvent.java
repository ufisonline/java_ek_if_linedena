/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.core.entities;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;

/**
 * @author $Author$
 * @version $Revision$
 */
@Entity
@Table(name = "FlightEvent")
public class EntDbFlighEvent extends EntHopoAssociated {

    /**
     * Logger
     */
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EntDbFlighEvent.class);
    private String cxCd;
    private String fltNum;
    private String fltSuffix;
    private BigInteger depNum;
    @Column(name = "ScheduledFlightDatetime")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date fltDate;
    private String depStn;
    private String arrStn;
    private BigInteger legNum;
    private String flightEvent;

    public EntDbFlighEvent() {
    }

    public String getCxCd() {
        return cxCd;
    }

    public void setCxCd(String cxCd) {
        this.cxCd = cxCd;
    }

    public String getFltNum() {
        return fltNum;
    }

    public void setFltNum(String fltNum) {
        this.fltNum = fltNum;
    }

    public String getFltSuffix() {
        return fltSuffix;
    }

    public void setFltSuffix(String fltSuffix) {
        this.fltSuffix = fltSuffix;
    }

    public BigInteger getDepNum() {
        return depNum;
    }

    public void setDepNum(BigInteger depNum) {
        this.depNum = depNum;
    }

    public Date getFltDate() {
        return fltDate;
    }

    public void setFltDate(Date fltDate) {
        this.fltDate = fltDate;
    }

    public String getDepStn() {
        return depStn;
    }

    public void setDepStn(String depStn) {
        this.depStn = depStn;
    }

    public String getArrStn() {
        return arrStn;
    }

    public void setArrStn(String arrStn) {
        this.arrStn = arrStn;
    }

    public BigInteger getLegNum() {
        return legNum;
    }

    public void setLegNum(BigInteger legNum) {
        this.legNum = legNum;
    }

    public String getFlightEvent() {
        return flightEvent;
    }

    public void setFlightEvent(String flightEvent) {
        this.flightEvent = flightEvent;
    }
    
}
