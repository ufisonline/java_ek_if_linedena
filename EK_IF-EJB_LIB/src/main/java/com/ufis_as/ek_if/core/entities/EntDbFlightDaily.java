package com.ufis_as.ek_if.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;

/**
 * The persistent class for the FLT_DAILY database table.
 * 
 */
@Entity
@Table(name = "FLT_DAILY")
@NamedQueries({
	@NamedQuery(name = "EntDbFlightDaily.findIdsByFlightInfo", query = "SELECT a.id FROM EntDbFlightDaily a WHERE a.fltNumber=:flno AND a.fltOrigin3=:org3 AND a.fltDest3=:des3 AND a.flut=:flut"),
	@NamedQuery(name = "EntDbFlightDaily.findByFlightInfo", query = "SELECT a FROM EntDbFlightDaily a WHERE a.fltNumber=:flno AND a.fltOrigin3=:org3 AND a.fltDest3=:des3 AND a.legNum=:legNum AND a.flut=:flut"),
	@NamedQuery(name = "EntDbFlightDaily.findIdByFltInfoSobtLoc", query = "SELECT a.idFlight FROM EntDbFlightDaily a WHERE a.fltNumber=:flno AND a.fltOrigin3=:org3 AND a.fltDest3=:des3 AND trunc(a.sobtLoc)= to_date(:sobtLoc,'YYYY-MM-DD')")	
})
public class EntDbFlightDaily extends EntHopoAssociated implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "ID_FLIGHT")
	private BigDecimal idFlight;
	
	@Column(name = "FLIGHT_NUMBER")
	private String fltNumber;

	@Column(name = "FLT_ORIGIN3")
	private String fltOrigin3;
	
	@Column(name = "FLT_DEST3")
	private String fltDest3;
	
	@Column(name = "DEP_NUM")
	private String depNum;
	
	@Column(name = "LEG_NUM")
	private String legNum;
	
	@Column(name = "AIRCRAFT_TYPE3")
	private String act3;

	@Column(name = "AIRCRAFT_TYPE5")
	private String act5;
	
	@Column(name = "FLT_DATE_LOC")
	private String flda;

	@Column(name = "FLT_DATE_UTC")
	private String flut;
	
	@Column(name = "FLT_DURATION")
	private BigDecimal fltDuration;
	
	@Column(name = "FLT_SERVICE_TYPE")
	private String fltServiceType;
	
	@Column(name = "SCHE_OFF_ONBLOCK_DURATION")
	private BigDecimal scheOffOnblockDuration;
	
	@Column(name = "ACTUAL_OFF_ONBLOCK_DURATION")
	private BigDecimal actualOffOnblockDuration;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AIBT")
	private Date aibt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AIBT_LOC")
	private Date aibtLoc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ALDT")
	private Date aldt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ALDT_LOC")
	private Date aldtLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AOBT")
	private Date aobt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "AOBT_LOC")
	private Date aobtLoc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASTT")
	private Date astt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASTT_LOC")
	private Date asttLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ATOT")
	private Date atot;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ATOT_LOC")
	private Date atotLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EIBT")
	private Date eibt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EIBT_LOC")
	private Date eibtLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELDT")
	private Date eldt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELDT_LOC")
	private Date eldtLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EOBT")
	private Date eobt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EOBT_LOC")
	private Date eobtLoc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETOT")
	private Date etot;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETOT_LOC")
	private Date etotLoc;
	
	@Column(name = "EXIT")
	private BigDecimal exit;

	@Column(name = "EXOT")
	private BigDecimal exot;

	@Column(name = "FLT_REGN")
	private String fltRegn;

	@Column(name = "FLT_REMARKS")
	private String fltRemarks;

	@Column(name = "FLT_SUPPLEMENT_REMARKS")
	private String fltSupplementRemarks;

	@Column(name = "ISTA")
	private String ista;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SIBT")
	private Date sibt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SIBT_LOC")
	private Date sibtLoc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SOBT")
	private Date sobt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SOBT_LOC")
	private Date sobtLoc;
	
	@Column(name = "REC_STATUS")
	private String recStatus;

	public EntDbFlightDaily() {
	}

	public BigDecimal getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getFltNumber() {
		return fltNumber;
	}

	public void setFltNumber(String fltNumber) {
		this.fltNumber = fltNumber;
	}

	public String getFltOrigin3() {
		return fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public String getFltDest3() {
		return fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getDepNum() {
		return depNum;
	}

	public void setDepNum(String depNum) {
		this.depNum = depNum;
	}

	public String getLegNum() {
		return legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public String getAct3() {
		return act3;
	}

	public void setAct3(String act3) {
		this.act3 = act3;
	}

	public String getAct5() {
		return act5;
	}

	public void setAct5(String act5) {
		this.act5 = act5;
	}

	public String getFlda() {
		return flda;
	}

	public void setFlda(String flda) {
		this.flda = flda;
	}

	public String getFlut() {
		return flut;
	}

	public void setFlut(String flut) {
		this.flut = flut;
	}

	public BigDecimal getFltDuration() {
		return fltDuration;
	}

	public void setFltDuration(BigDecimal fltDuration) {
		this.fltDuration = fltDuration;
	}

	public String getFltServiceType() {
		return fltServiceType;
	}

	public void setFltServiceType(String fltServiceType) {
		this.fltServiceType = fltServiceType;
	}

	public BigDecimal getScheOffOnblockDuration() {
		return scheOffOnblockDuration;
	}

	public void setScheOffOnblockDuration(BigDecimal scheOffOnblockDuration) {
		this.scheOffOnblockDuration = scheOffOnblockDuration;
	}

	public BigDecimal getActualOffOnblockDuration() {
		return actualOffOnblockDuration;
	}

	public void setActualOffOnblockDuration(BigDecimal actualOffOnblockDuration) {
		this.actualOffOnblockDuration = actualOffOnblockDuration;
	}

	public Date getAibt() {
		return aibt;
	}

	public void setAibt(Date aibt) {
		this.aibt = aibt;
	}

	public Date getAldt() {
		return aldt;
	}

	public void setAldt(Date aldt) {
		this.aldt = aldt;
	}

	public Date getAobt() {
		return aobt;
	}

	public void setAobt(Date aobt) {
		this.aobt = aobt;
	}

	public Date getAstt() {
		return astt;
	}

	public void setAstt(Date astt) {
		this.astt = astt;
	}

	public Date getAtot() {
		return atot;
	}

	public void setAtot(Date atot) {
		this.atot = atot;
	}

	public Date getEibt() {
		return eibt;
	}

	public void setEibt(Date eibt) {
		this.eibt = eibt;
	}

	public Date getEldt() {
		return eldt;
	}

	public void setEldt(Date eldt) {
		this.eldt = eldt;
	}

	public Date getEobt() {
		return eobt;
	}

	public void setEobt(Date eobt) {
		this.eobt = eobt;
	}

	public Date getEtot() {
		return etot;
	}

	public void setEtot(Date etot) {
		this.etot = etot;
	}

	public BigDecimal getExit() {
		return exit;
	}

	public void setExit(BigDecimal exit) {
		this.exit = exit;
	}

	public BigDecimal getExot() {
		return exot;
	}

	public void setExot(BigDecimal exot) {
		this.exot = exot;
	}

	public String getFltRegn() {
		return fltRegn;
	}

	public void setFltRegn(String fltRegn) {
		this.fltRegn = fltRegn;
	}

	public String getFltRemarks() {
		return fltRemarks;
	}

	public void setFltRemarks(String fltRemarks) {
		this.fltRemarks = fltRemarks;
	}

	public String getFltSupplementRemarks() {
		return fltSupplementRemarks;
	}

	public void setFltSupplementRemarks(String fltSupplementRemarks) {
		this.fltSupplementRemarks = fltSupplementRemarks;
	}

	public String getIsta() {
		return ista;
	}

	public void setIsta(String ista) {
		this.ista = ista;
	}

	public Date getSibt() {
		return sibt;
	}

	public void setSibt(Date sibt) {
		this.sibt = sibt;
	}

	public Date getSibtLoc() {
		return sibtLoc;
	}

	public void setSibtLoc(Date sibtLoc) {
		this.sibtLoc = sibtLoc;
	}

	public Date getSobt() {
		return sobt;
	}

	public void setSobt(Date sobt) {
		this.sobt = sobt;
	}

	public Date getSobtLoc() {
		return sobtLoc;
	}

	public void setSobtLoc(Date sobtLoc) {
		this.sobtLoc = sobtLoc;
	}

	public Date getAibtLoc() {
		return aibtLoc;
	}

	public void setAibtLoc(Date aibtLoc) {
		this.aibtLoc = aibtLoc;
	}

	public Date getAldtLoc() {
		return aldtLoc;
	}

	public void setAldtLoc(Date aldtLoc) {
		this.aldtLoc = aldtLoc;
	}

	public Date getAobtLoc() {
		return aobtLoc;
	}

	public void setAobtLoc(Date aobtLoc) {
		this.aobtLoc = aobtLoc;
	}

	public Date getAsttLoc() {
		return asttLoc;
	}

	public void setAsttLoc(Date asttLoc) {
		this.asttLoc = asttLoc;
	}

	public Date getAtotLoc() {
		return atotLoc;
	}

	public void setAtotLoc(Date atotLoc) {
		this.atotLoc = atotLoc;
	}

	public Date getEibtLoc() {
		return eibtLoc;
	}

	public void setEibtLoc(Date eibtLoc) {
		this.eibtLoc = eibtLoc;
	}

	public Date getEldtLoc() {
		return eldtLoc;
	}

	public void setEldtLoc(Date eldtLoc) {
		this.eldtLoc = eldtLoc;
	}

	public Date getEobtLoc() {
		return eobtLoc;
	}

	public void setEobtLoc(Date eobtLoc) {
		this.eobtLoc = eobtLoc;
	}

	public Date getEtotLoc() {
		return etotLoc;
	}

	public void setEtotLoc(Date etotLoc) {
		this.etotLoc = etotLoc;
	}

	public String getRecStatus() {
		return recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

}