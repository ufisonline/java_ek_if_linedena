package com.ufis_as.ek_if.core.entities;


public class EntFlightDailyDTO {

	private String flightNumber;
	private String fltOrg3;
	private String fltDes3;
	private String legNum;
	private String flut;
	private String flda;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFltOrg3() {
		return fltOrg3;
	}

	public void setFltOrg3(String fltOrg3) {
		this.fltOrg3 = fltOrg3;
	}

	public String getFltDes3() {
		return fltDes3;
	}

	public void setFltDes3(String fltDes3) {
		this.fltDes3 = fltDes3;
	}

	public String getLegNum() {
		return legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public String getFlut() {
		return flut;
	}

	public void setFlut(String flut) {
		this.flut = flut;
	}

	public String getFlda() {
		return flda;
	}

	public void setFlda(String flda) {
		this.flda = flda;
	}

	@Override
	public String toString() {
		return "EntFlightDailyDTO [flightNumber=" + flightNumber + ", fltOrg3="
				+ fltOrg3 + ", fltDes3=" + fltDes3 + ", legNum=" + legNum
				+ ", flut=" + flut + "]";
	}

}
