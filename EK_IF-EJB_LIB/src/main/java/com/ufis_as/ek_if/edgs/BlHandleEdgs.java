/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.edgs;

import java.io.StringReader;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.handle.BlHandle;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJFUELSTATUS;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJFEVTAB;
import com.ufis_as.exco.INFOJFEVTABLIST;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

import ek.egds.EGDSMSG;

/**
 * @author $Author$
 * @version $Revision$
 */
public class BlHandleEdgs extends BlHandle {

	/**
	 * Logger
	 */
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BlHandleEdgs.class);
	protected static final String MSGIF = "EK-EDGS";
	private EGDSMSG flightData = null;
	
	public BlHandleEdgs(Marshaller marshaller, Unmarshaller um,
			InterfaceConfig interfaceConfig) {
		super(marshaller, um, interfaceConfig);
	}

	@Override
	public boolean unMarshal() {
		try {
			flightData = (EGDSMSG) _um.unmarshal(new StreamSource(
					new StringReader(_flightXml)));
//			if (readEvent(flightData)) {
//				LOG.debug("FlightEvent {}", flightData.getEGDSBODY().getFNR());
				return true;
//			} else {
//				return false;
//			}
		} catch (JAXBException ex) {
			LOG.error("Flight Event JAXB exception {}", ex.toString());
			return false;
		}
	}

//	protected boolean readEvent(EGDSMSG flightEvent) {
//		if (flightEvent != null) {
//			return tranformFlight(flightEvent);
//		} else {
//			LOG.warn("Flight Event Null detected");
//			return false;
//		}
//	}

	public boolean tranformFlight(EGDSMSG flightEvent, EntDbAfttab aftFlight) {
		boolean isTransferred = false;
		try {
			HpUfisCalendar ufisCalendar = new HpUfisCalendar();
			ufisCalendar.setCustomFormat(HpEKConstants.EDGS_TIME_FORMAT);
	
			INFOBJFLIGHT infobjFlight = new INFOBJFLIGHT();
			MSGOBJECTS msgobjects = new MSGOBJECTS();
			INFOBJGENERIC infobjgeneric = new INFOBJGENERIC();
			INFOJXAFTAB infojxaftab = new INFOJXAFTAB();
			INFOBJFUELSTATUS infofuelstatus = new INFOBJFUELSTATUS();
			INFOJFEVTABLIST	infojfevtablist = new INFOJFEVTABLIST();
			Boolean xafFound = false;
			Boolean fevFound = false;
			
			/*
			 * REGN Java program has to trim the hyphen char '-'
			 */
			infobjFlight.setREGN(aftFlight.getRegn());
			
			/*
			 * Change to number for both in order to compare when use to search
			 * flight
			 */
			//infobjFlight.setFLTN(flightEvent.getEGDSBODY().getFNR());
			infobjFlight.setALC2(aftFlight.getAlc2());
			infobjFlight.setALC3(aftFlight.getAlc3());
			infobjFlight.setFLDA(aftFlight.getFlda());
	
			infobjFlight.setFLTN(aftFlight.getFltn());
			infobjFlight.setORG3(flightEvent.getEGDSBODY().getDEP());
			if ("DXB".equals(flightEvent.getEGDSBODY().getDEP()) && "DXB".equals(flightEvent.getEGDSBODY().getARR())) {
				infobjFlight.setADID(ADID.B);
			} else if ("DXB".equals(flightEvent.getEGDSBODY().getDEP())) {
				infobjFlight.setADID(ADID.D);
			} else {
				infobjFlight.setADID(ADID.fromValue(aftFlight.getAdid().toString()));
			}
	
			// according to type settle down return message
			if (flightEvent.getEGDSBODY().getTYPE() != null) {
				switch (flightEvent.getEGDSBODY().getTYPE()) {
				case MVT:
					/*  
					 * OUT -> AOBN
					 * OFF -> ATON
					 * ON  -> ALDN
					 * IN  -> AIBN
					 */
					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getTRIGEVT())) {
						String trigevt = flightEvent.getEGDSBODY().getTRIGEVT();
						if (trigevt.contains("OUT")) {
							infobjFlight.setAOBN(flightEvent.getEGDSBODY().getATA());
						} else if (trigevt.contains("OFF")) {
							infobjFlight.setATON(flightEvent.getEGDSBODY().getATA());
						} else if (trigevt.contains("ON")) {
							infobjFlight.setALDN(flightEvent.getEGDSBODY().getATA());
						} else if (trigevt.contains("IN")) {
							infobjFlight.setAIBN(flightEvent.getEGDSBODY().getATA());
						}
					}
					
					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getFOB())) {
						infojxaftab.setFUOB(flightEvent.getEGDSBODY().getFOB());
						xafFound = true;
					}
					
					// Cargo Door closed and Cabin Door closed 
					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getCARD())) {
						ufisCalendar.setTime(flightEvent.getEGDSBODY().getCARD(), ufisCalendar.getCustomFormat());
						INFOJFEVTAB cargoDoor = new INFOJFEVTAB();
						cargoDoor.setSTNM("CARGO DOOR CLOSED");
						cargoDoor.setSTTM(ufisCalendar.getCedaString());
						infojfevtablist.getINFOJFEVTAB().add(cargoDoor);
						fevFound = true;
					}
					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getCABD())) {
						ufisCalendar.setTime(flightEvent.getEGDSBODY().getCABD(), ufisCalendar.getCustomFormat());
						INFOJFEVTAB cabinDoor = new INFOJFEVTAB();
						cabinDoor.setSTNM("CABIN DOOR CLOSED");
						cabinDoor.setSTTM(ufisCalendar.getCedaString());
						infojfevtablist.getINFOJFEVTAB().add(cabinDoor);
						fevFound = true;
					}
					break;
					
				case PROGRESSREPORT:
//					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getETA())) {
//						infobjFlight.setELDN(flightEvent.getEGDSBODY().getETA());
//					}
					infobjFlight.setEIBN(flightEvent.getEGDSBODY().getETA());
					break;
					
				case LOADACC:
					infojxaftab.setLSHT(flightEvent.getEGDSBODY().getLOADSHEET());
//					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getZFW())) {
//						infojxaftab.setZFWT(flightEvent.getEGDSBODY().getZFW());
//						xafFound = true;
//					}
//					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSBODY().getTOW())) {
//						infojxaftab.setTOWT(flightEvent.getEGDSBODY().getTOW());
//						xafFound = true;
//					}
					break;
					
				case ROUTE:
					// confirm message from pilot that crew is on board
					// due to if NON-EK need to ignore whole message
					infobjgeneric.setALC2("EK");
					if (HpUfisUtils.isNotEmptyStr(flightEvent.getEGDSHEADER().getTIMESTAMP())) {
						ufisCalendar.setTime(flightEvent.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
						INFOJFEVTAB route = new INFOJFEVTAB();
						route.setSTNM("Route Request");
						route.setSTRM("<ACR>");
						route.setSTTM(ufisCalendar.getCedaString());
						infojfevtablist.getINFOJFEVTAB().add(route);
						fevFound = true;
					}
					break;
					
				case FUEL:
					// message send datetime
					ufisCalendar.setTime(flightEvent.getEGDSHEADER().getTIMESTAMP(), ufisCalendar.getCustomFormat());
					infofuelstatus.setMSGSENDDATE(ufisCalendar.getCedaString());
					// regn
					infofuelstatus.setFLTREGN(aftFlight.getRegn());
					// alc2
					infofuelstatus.setAIRLINECODE2(aftFlight.getAlc2());
					// org3
					infofuelstatus.setFLTORIGIN3(aftFlight.getOrg3());
					// des3
					infofuelstatus.setFLTDEST3(aftFlight.getDes3());
					// trip_fuel
					infofuelstatus.setTRIPFUEL(flightEvent.getEGDSBODY().getTripFuel());
					// taxi_fuel
					infofuelstatus.setTAXIFUEL(flightEvent.getEGDSBODY().getTaxiFuel());
					// ramp_fuel
					infofuelstatus.setRAMPFUEL(flightEvent.getEGDSBODY().getRampFuel());
					// trm
					infofuelstatus.setTRM(flightEvent.getEGDSBODY().getTRM());
					// den
					infofuelstatus.setDEN(flightEvent.getEGDSBODY().getDEN());
					// rtw
					infofuelstatus.setRTW(flightEvent.getEGDSBODY().getRTW());
					break;
				}
			}
	
			/*
			 * Set the fields that uniquely identify a flight the rest are done in
			 * the UfisMarshal
			 */
			infobjgeneric.setDEPN(infobjFlight.getDEPN());
			infobjgeneric.setREGN(infobjFlight.getREGN());
			msgobjects.setINFOBJFLIGHT(infobjFlight);
			if (xafFound) {
				msgobjects.setINFOJXAFTAB(infojxaftab);
			}
			if (fevFound) {
				//msgobjects.setINFOJFEVTAB(infojfevtab);
				msgobjects.setINFOJFEVTABLIST(infojfevtablist);
			}
	
			UfisMarshal ufisMarshal = new UfisMarshal(_ms, _msgActionType, MSGIF, _interfaceConfig);
			_returnXML = ufisMarshal.marshalFlightEvent(infobjgeneric, msgobjects);
			isTransferred = true;
		} catch (Exception e) {
			LOG.error("Cannot transform egds message: {}", e.getMessage());
		}
		return isTransferred;
	}

	public EGDSMSG getFlightData() {
		return flightData;
	}

	public void setFlightData(EGDSMSG flightData) {
		this.flightData = flightData;
	}
	
}
