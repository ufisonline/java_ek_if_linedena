package com.ufis_as.ek_if.egds.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


@Entity
@Table(name = "FLT_FUEL_STATUS")
@NamedQueries({ 
	@NamedQuery(name = "EntDbFlightFuelStatus.findByIdFlight", query = "SELECT a FROM EntDbFlightFuelStatus a WHERE a.idFlight = :idFlight"),
	@NamedQuery(name = "EntDbFlightFuelStatus.findByIdRFlight", query = "SELECT a FROM EntDbFlightFuelStatus a WHERE a.idRFlight = :idRFlight")
})
public class EntDbFlightFuelStatus extends EntHopoAssociated implements
		Serializable {

	//private static final long serialVersionUID = 1L;

	@Column(name = "id_flight")
	private BigDecimal idFlight;

	@Column(name = "ID_RFLIGHT")
	private BigDecimal idRFlight;
	
	@Column(name = "msg_send_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date msgSendDate;

	@Column(name = "flt_regn")
	private String fltRegn;

	@Column(name = "airline_code2")
	private String airlineCode2;

	@Column(name = "flight_number")
	private String flightNumber;
	
//	@Column(name = "flt_suffix")
//	private Character fltSuffix;
	
	@Column(name = "flt_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fltDate;

	@Column(name = "flt_origin3")
	private String fltOrigin3;

	@Column(name = "flt_dest3")
	private String fltDest3;

	@Column(name = "trip_fuel", precision = 15, scale = 8)
	private float tripFuel;

	@Column(name = "taxi_fuel", precision = 15, scale = 8)
	private float taxiFuel;

	@Column(name = "ramp_fuel", precision = 15, scale = 8)
	private float rampFuel;

	@Column(name = "onboard_fuel", precision = 15, scale = 8)
	private float onboardFuel;

	@Column(name = "takeoff_fuel", precision = 15, scale = 8)
	private float takeoffFuel;

	@Column(name = "arrival_fuel", precision = 15, scale = 8)
	private float arrivalFuel;

	@Column(name = "required_fuel", precision = 15, scale = 8)
	private float requiredFuel;

	@Column(name = "fuel_rate", precision = 15, scale = 8)
	private float fuelRate;

	@Column(name = "fuel_time")
	private float fuelTime;

	@Column(name = "TRM")
	private String trm;

	@Column(name = "DEN")
	private String den;

	@Column(name = "RTW")
	private String rtw;
	
	@Column(name = "ID_FLT_DAILY")
	private String idFltDaily;

	@Column(name = "rec_status")
	private Character recStatus;

	@Column(name = "data_source")
	private String dataSource;
	
	public EntDbFlightFuelStatus() {
		
	}
	
	private EntDbFlightFuelStatus(EntDbFlightFuelStatus value) {
		this.id = value.id;
		this._createdDate = value._createdDate;
		this._createdUser = value._createdUser;
		this._updatedDate = value._updatedDate;
		this._updatedUser = value._updatedUser;
		this.version = value.version;
		this.idFlight = value.idFlight;
		this.idRFlight = value.idRFlight;
		this.msgSendDate = value.msgSendDate;
		this.fltRegn = value.fltRegn;
		this.airlineCode2 = value.airlineCode2;
		this.flightNumber = value.flightNumber;
		this.fltDate = value.fltDate;
		this.fltOrigin3 = value.fltOrigin3;
		this.fltDest3 = value.fltDest3;
		this.tripFuel = value.tripFuel;
		this.taxiFuel = value.taxiFuel;
		this.rampFuel = value.rampFuel;
		this.onboardFuel = value.onboardFuel;
		this.takeoffFuel = value.takeoffFuel;
		this.arrivalFuel = value.arrivalFuel;
		this.requiredFuel = value.requiredFuel;
		this.fuelRate = value.fuelRate;
		this.fuelTime = value.fuelTime;
		this.trm = value.trm;
		this.den = value.den;
		this.rtw = value.rtw;
		this.idFltDaily = value.idFltDaily;
		this.recStatus = value.recStatus;
		this.dataSource = value.dataSource;
	}
	
	public static EntDbFlightFuelStatus valueOf(EntDbFlightFuelStatus value) {
		return new EntDbFlightFuelStatus(value);
	}
	
	public BigDecimal getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public BigDecimal getIdRFlight() {
		return idRFlight;
	}

	public void setIdRFlight(BigDecimal idRFlight) {
		this.idRFlight = idRFlight;
	}

	public Date getMsgSendDate() {
		return msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getFltRegn() {
		return fltRegn;
	}

	public void setFltRegn(String fltRegn) {
		this.fltRegn = fltRegn;
	}

	public String getAirlineCode2() {
		return airlineCode2;
	}

	public void setAirlineCode2(String airlineCode2) {
		this.airlineCode2 = airlineCode2;
	}

	public Date getFltDate() {
		return fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

//	public Character getFltSuffix() {
//		return fltSuffix;
//	}
//
//	public void setFltSuffix(Character fltSuffix) {
//		this.fltSuffix = fltSuffix;
//	}

	public String getFltOrigin3() {
		return fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public String getFltDest3() {
		return fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public float getTripFuel() {
		return tripFuel;
	}

	public void setTripFuel(float tripFuel) {
		this.tripFuel = tripFuel;
	}

	public float getTaxiFuel() {
		return taxiFuel;
	}

	public void setTaxiFuel(float taxiFuel) {
		this.taxiFuel = taxiFuel;
	}

	public float getRampFuel() {
		return rampFuel;
	}

	public void setRampFuel(float rampFuel) {
		this.rampFuel = rampFuel;
	}

	public String getTrm() {
		return trm;
	}

	public void setTrm(String trm) {
		this.trm = trm;
	}

	public String getDen() {
		return den;
	}

	public void setDen(String den) {
		this.den = den;
	}

	public String getRtw() {
		return rtw;
	}

	public void setRtw(String rtw) {
		this.rtw = rtw;
	}

	public Character getRecStatus() {
		return recStatus;
	}

	public void setRecStatus(Character recStatus) {
		this.recStatus = recStatus;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public float getOnboardFuel() {
		return onboardFuel;
	}

	public void setOnboardFuel(float onboardFuel) {
		this.onboardFuel = onboardFuel;
	}

	public float getTakeoffFuel() {
		return takeoffFuel;
	}

	public void setTakeoffFuel(float takeoffFuel) {
		this.takeoffFuel = takeoffFuel;
	}

	public float getArrivalFuel() {
		return arrivalFuel;
	}

	public void setArrivalFuel(float arrivalFuel) {
		this.arrivalFuel = arrivalFuel;
	}

	public float getRequiredFuel() {
		return requiredFuel;
	}

	public void setRequiredFuel(float requiredFuel) {
		this.requiredFuel = requiredFuel;
	}

	public float getFuelRate() {
		return fuelRate;
	}

	public void setFuelRate(float fuelRate) {
		this.fuelRate = fuelRate;
	}

	public float getFuelTime() {
		return fuelTime;
	}

	public void setFuelTime(float fuelTime) {
		this.fuelTime = fuelTime;
	}

	public String getIdFltDaily() {
		return idFltDaily;
	}

	public void setIdFltDaily(String idFltDaily) {
		this.idFltDaily = idFltDaily;
	}

}
