/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.enumeration;


/**
 * @author  $Author$
 * @version $Revision$
 */

public enum EnumCOREEventTimeType {
AirborneEst,// – Estimated Airborne time
AirborneAct,// – Actual Aiborne time
OffBlocksEst,// – Estimated off block time
OffBlocksAct,// – Actual off block time
OnBlocksAct,// – Actual on block time
OnBlocksEst,// – Estimated on block time
StartTaxiAct,// – Actual time of start taxing
LandedEst,// – Estimated landing time
LandedAct,// – Actual landing time
NextInformation //– Time at which next information for the flight shall be made available

}
