package com.ufis_as.ek_if.enumeration;

/**
 * @author JGO
 *
 */
public enum EnumExceptionCodes {

	ENOFL("Error - No Flight Found"), 
	EMAND("Error - Mandatory Data Not Found"), 
	EENUM("Error - Enumerated Data Not Matching"), 
	EXSDF("Error - Wrong XSD Format"), 
	EWEVT("Error - Wrong Event Type"), 
	EWALC("Error - Wrong Airline"), 
	ENDEP("Error - Not a Departure Flight"), 
	ENOBG("Error - No BAG Found"), 
	ENOPX("Error - No PAX Found"), 
	ENOUD("Error - No ULD Found"), 
	EWSRC("Error - Wrong Source"), 
	ENREC("Error - No Record Found"), 
	ENOMD("Error - Master Data Not Found"), 
	EWFMT("Error - Wrong Field Format"),
	EWVAL("Error - Wrong Value"), 
	EWACT("Error - Unexpected Data Manipulation Action"),
	EWPTY("Error - Wrong Pax Type C with infant"),
	WNCOG("Warning - Value Not In Configuration List"), 
	WNOPX("Warning - No PAX Found"), 
	WNOBG("Warning - No BAG Found"), 
	WNOUD("Warning - No ULD Found"), 
	WNOMD("Warning - Master Data Does Not Match"), 
	WENUM("Warning - Enumerated Data Not Matching"),
	WBGCL("Warning - Unexpected Bag Class"), 
	WNOFL("Warning - No Flight Found"),
	WPXPD("Warning - PAX with PD status");
	
	private String desc;
	
	private EnumExceptionCodes(String desc) {
		this.desc = desc;
	}
	
	@Override
	public String toString() {
		return String.valueOf(desc);
	}

}
