/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.enumeration;


/**
 * @author  $Author$
 * @version $Revision$
 */

public enum EnumMACSFlightStatus {
FE , //  Flight Edit
FS , // Flight Suspended
FT , // Flight open for TCI
FO , // Flight Open
FU , // Flight Update
FZ , // Boarding Completed
FH , // Flight Held
FG , // Flight Gated
FC , // Flight Closed
FF , // Flight Finalised
PD  // Post Departure


}
