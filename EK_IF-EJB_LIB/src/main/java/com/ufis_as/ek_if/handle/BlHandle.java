/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.handle;

import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.ek_if.ufis.UfisMarshal;
import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJFLIGHT;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.INFOJDCFTAB;
import com.ufis_as.exco.INFOJDCFTABLIST;
import com.ufis_as.exco.INFOJFEVTAB;
import com.ufis_as.exco.INFOJXAFTAB;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import ek.core.flightevent.FlightEvent;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author fou
 */
public abstract class BlHandle implements IHandle {

    protected static final String HOPO = "DXB";

    protected String _actionType = "";
    protected String _flightXml = "";
    protected InterfaceConfig _interfaceConfig;
    protected String _messageType = "";
    protected Marshaller _ms;
    protected ACTIONTYPE _msgActionType;
    protected String _returnXML = "";
    protected Unmarshaller _um;

    public BlHandle(
            Marshaller marshaller,Unmarshaller um,
            InterfaceConfig interfaceConfig) {
        _um = um;
        _ms = marshaller;
        _interfaceConfig = interfaceConfig;

    }

    @Override
    public String getFlightXml() {
        return _flightXml;
    }

    @Override
    public String getReturnXML() {
        return _returnXML;
    }



    @Override
    public void setFlightXml(String _flightXml) {
        this._flightXml = _flightXml;
    }

    @Override
    public void setReturnXML(String _returnXML) {
        this._returnXML = _returnXML;
    }

}
