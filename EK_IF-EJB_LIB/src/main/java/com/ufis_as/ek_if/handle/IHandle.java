/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.handle;

/**
 *
 * @author fou
 */
public interface IHandle {

    String getFlightXml();

    String getReturnXML();

    void setFlightXml(String _flightXml);

    void setReturnXML(String _returnXML);

    boolean unMarshal();
    
}
