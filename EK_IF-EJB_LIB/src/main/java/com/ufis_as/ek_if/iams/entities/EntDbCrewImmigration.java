package com.ufis_as.ek_if.iams.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the CREW_IMMIGRATION database table.
 * @author btr
 */
@Entity
@Table(name="CREW_IMMIGRATION")
@NamedQueries({
	@NamedQuery(name = "EntDbCrewImmigration.findByPk", query = "SELECT e FROM EntDbCrewImmigration e WHERE e.flightNumber = :flNum AND e.fltDate = :flightDate AND e.depNum = :depNum AND e.legNum = :legNum AND e.staffNumber = :staffNum AND e.gateType = :gateType AND e.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbCrewImmigration.findByIdFlightAndStaffNum", query = "SELECT e FROM EntDbCrewImmigration e WHERE e.idFlight = :idFlight AND e.staffNumber = :staffNumber AND e.crewTypeCode = :crewTypeCode AND e.idFltJobAssign <> :idFltJobAssign AND e.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbCrewImmigration.findByIdFlightAndIdFltJobAssign", query = "SELECT e FROM EntDbCrewImmigration e WHERE e.idFlight = :idFlight AND e.idFltJobAssign = :idFltJobAssign AND e.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbCrewImmigration.updateIdFltJobAssignDeleted", query = "UPDATE EntDbCrewImmigration e SET  e.recStatus = :recStatus WHERE e.idFltJobAssign = :idFltJobAssign")
})
public class EntDbCrewImmigration extends EntHopoAssociated {//implements Cloneable{
	private static final long serialVersionUID = 1L;

	@Column(name="CREW_ACTIVITY_FLAG")
	private String crewActivityFlag;

	@Column(name="CREW_ACTIVITY_STAT")
	private String crewActivityStat;

	@Column(name="CREW_TYPE_CODE")
	private String crewTypeCode;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DEP_NUM")
	private String depNum;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="FLT_DEST3")
	private String fltDest3;

	@Column(name="FLT_ORIGIN3")
	private String fltOrigin3;

	@Column(name="GATE_LOCATION")
	private String gateLocation;

	@Column(name="GATE_NUMBER")
	private String gateNumber;

	@Column(name="GATE_TYPE")
	private String gateType;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_FLT_JOB_ASSIGN")
	private String idFltJobAssign;

	@Column(name="ID_MD_GATE_TYPE")
	private String idMdGateType;

	@Column(name="LEG_NUM")
	private String legNum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_NUMBER")
	private String staffNumber;

	@Column(name="STATUS_REMARKS")
	private String statusRemarks;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="STATUSCHANGE_DATE")
	private Date statusChangeDate; 

	public EntDbCrewImmigration() {
	}

/*	@Override
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}*/
	
	public EntDbCrewImmigration(EntDbCrewImmigration crew){
		this.idFlight = crew.idFlight;
		this.idFltJobAssign = crew.idFltJobAssign;
		this.staffNumber = crew.staffNumber;
		this.crewTypeCode = crew.crewTypeCode;
		this.gateNumber = crew.gateNumber;
		this.gateType = crew.gateType;
	}
	
	public String getCrewActivityFlag() {
		return this.crewActivityFlag;
	}

	public void setCrewActivityFlag(String crewActivityFlag) {
		this.crewActivityFlag = crewActivityFlag;
	}

	public String getCrewActivityStat() {
		return this.crewActivityStat;
	}

	public void setCrewActivityStat(String crewActivityStat) {
		this.crewActivityStat = crewActivityStat;
	}

	public String getCrewTypeCode() {
		return this.crewTypeCode;
	}

	public void setCrewTypeCode(String crewTypeCode) {
		this.crewTypeCode = crewTypeCode;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDepNum() {
		return this.depNum;
	}

	public void setDepNum(String depNum) {
		this.depNum = depNum;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public String getGateLocation() {
		return this.gateLocation;
	}

	public void setGateLocation(String gateLocation) {
		this.gateLocation = gateLocation;
	}

	public String getGateNumber() {
		return this.gateNumber;
	}

	public void setGateNumber(String gateNumber) {
		this.gateNumber = gateNumber;
	}

	public String getGateType() {
		return this.gateType;
	}

	public void setGateType(String gateType) {
		this.gateType = gateType;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdFltJobAssign() {
		return this.idFltJobAssign;
	}

	public void setIdFltJobAssign(String idFltJobAssign) {
		this.idFltJobAssign = idFltJobAssign;
	}

	public String getIdMdGateType() {
		return this.idMdGateType;
	}

	public void setIdMdGateType(String idMdGateType) {
		this.idMdGateType = idMdGateType;
	}

	public String getLegNum() {
		return this.legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffNumber() {
		return this.staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStatusRemarks() {
		return this.statusRemarks;
	}

	public void setStatusRemarks(String statusRemarks) {
		this.statusRemarks = statusRemarks;
	}

	public Date getStatusChangeDate() {
		return statusChangeDate;
	}

	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}
}