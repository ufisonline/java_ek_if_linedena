package com.ufis_as.ek_if.iams.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_GATE_TYPE database table.
 * @author btr
 */
@Entity
@Table(name="MD_GATE_TYPE")
public class EntDbMdGateType extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="GATE_TYPE")
	private String gateType;

	@Column(name="GATE_TYPE_CODE")
	private String gateTypeCode;

	@Column(name="GATE_TYPE_DESC")
	private String gateTypeDesc;

	@Column(name="REC_STATUS")
	private String recStatus;

	public EntDbMdGateType() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getGateType() {
		return this.gateType;
	}

	public void setGateType(String gateType) {
		this.gateType = gateType;
	}

	public String getGateTypeCode() {
		return this.gateTypeCode;
	}

	public void setGateTypeCode(String gateTypeCode) {
		this.gateTypeCode = gateTypeCode;
	}

	public String getGateTypeDesc() {
		return this.gateTypeDesc;
	}

	public void setGateTypeDesc(String gateTypeDesc) {
		this.gateTypeDesc = gateTypeDesc;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}
}