package com.ufis_as.ek_if.iams.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the STAFF_ACTIVITY database table.
 * @author btr
 */
@Entity
@Table(name="STAFF_ACTIVITY")
public class EntDbStaffActivity extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FIRST_START_DATE")
	private Date firstStartDate;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_FLT_JOB_ASSIGN")
	private String idFltJobAssign;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_ACTIVITY_DATE")
	private Date lastActivityDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_END_DATE")
	private Date lastEndDate;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="RESOURCE_SUBTYPE")
	private String resourceSubtype;

	@Column(name="RESOURCE_TYPE")
	private String resourceType;

	@Column(name="RESOURCE_TYPE_CODE")
	private String resourceTypeCode;

	@Column(name="STAFF_ACTIVITY")
	private String staffActivity;

	@Column(name="TOTAL_FAIL")
	private BigDecimal totalFail;

	@Column(name="TOTAL_FINISH")
	private BigDecimal totalFinish;

	@Column(name="TOTAL_START")
	private BigDecimal totalStart;

	@Column(name="TOTAL_SUCCESSFUL")
	private BigDecimal totalSuccessful;

	public EntDbStaffActivity() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public Date getFirstStartDate() {
		return this.firstStartDate;
	}

	public void setFirstStartDate(Date firstStartDate) {
		this.firstStartDate = firstStartDate;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdFltJobAssign() {
		return this.idFltJobAssign;
	}

	public void setIdFltJobAssign(String idFltJobAssign) {
		this.idFltJobAssign = idFltJobAssign;
	}

	public Date getLastActivityDate() {
		return this.lastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}

	public Date getLastEndDate() {
		return this.lastEndDate;
	}

	public void setLastEndDate(Date lastEndDate) {
		this.lastEndDate = lastEndDate;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getResourceSubtype() {
		return this.resourceSubtype;
	}

	public void setResourceSubtype(String resourceSubtype) {
		this.resourceSubtype = resourceSubtype;
	}

	public String getResourceType() {
		return this.resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceTypeCode() {
		return this.resourceTypeCode;
	}

	public void setResourceTypeCode(String resourceTypeCode) {
		this.resourceTypeCode = resourceTypeCode;
	}

	public String getStaffActivity() {
		return this.staffActivity;
	}

	public void setStaffActivity(String staffActivity) {
		this.staffActivity = staffActivity;
	}

	public BigDecimal getTotalFail() {
		return this.totalFail;
	}

	public void setTotalFail(BigDecimal totalFail) {
		this.totalFail = totalFail;
	}

	public BigDecimal getTotalFinish() {
		return this.totalFinish;
	}

	public void setTotalFinish(BigDecimal totalFinish) {
		this.totalFinish = totalFinish;
	}

	public BigDecimal getTotalStart() {
		return this.totalStart;
	}

	public void setTotalStart(BigDecimal totalStart) {
		this.totalStart = totalStart;
	}

	public BigDecimal getTotalSuccessful() {
		return this.totalSuccessful;
	}

	public void setTotalSuccessful(BigDecimal totalSuccessful) {
		this.totalSuccessful = totalSuccessful;
	}
}