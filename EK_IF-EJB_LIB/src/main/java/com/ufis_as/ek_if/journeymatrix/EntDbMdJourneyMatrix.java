package com.ufis_as.ek_if.journeymatrix;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_JOURNEY_MATRIX database table.
 * 
 */
@Entity
@Table(name="MD_JOURNEY_MATRIX")
@NamedQueries({
	@NamedQuery(name="EntDbMdJourneyMatrix.findMatrixBy", query="SELECT a FROM EntDbMdJourneyMatrix a WHERE trim(a.opType) = trim(:opType) AND trim(a.fromLocType) = trim(:fromLocType) AND trim(a.toLocType) = trim(:toLocType) AND ((trim(a.fromLoc) = trim(:fromLoc) AND trim(a.toLoc) = trim(:toLoc)) OR (trim(a.fromLoc) = '*' AND trim(a.toLoc) = trim(:toLoc)) OR (trim(a.fromLoc) = trim(:fromLoc) AND trim(a.toLoc) = '*') OR (trim(a.fromLoc) = '*' AND trim(a.toLoc) = '*')) AND trim(a.moveObjType) = trim(:moveObjType)")
})
public class EntDbMdJourneyMatrix extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	private BigDecimal duration;

	@Column(name="FROM_LOC")
	private String fromLoc;

	@Column(name="FROM_LOC_TYPE")
	private String fromLocType;

	@Column(name="MOVE_OBJ_CATEGORY1")
	private String moveObjCategory1;

	@Column(name="MOVE_OBJ_CATEGORY2")
	private String moveObjCategory2;

	@Column(name="MOVE_OBJ_TYPE")
	private String moveObjType;

	@Column(name="OP_TYPE")
	private String opType;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="TO_LOC")
	private String toLoc;

	@Column(name="TO_LOC_TYPE")
	private String toLocType;

	public EntDbMdJourneyMatrix() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getDuration() {
		return this.duration;
	}

	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}

	public String getFromLoc() {
		return this.fromLoc;
	}

	public void setFromLoc(String fromLoc) {
		this.fromLoc = fromLoc;
	}

	public String getFromLocType() {
		return this.fromLocType;
	}

	public void setFromLocType(String fromLocType) {
		this.fromLocType = fromLocType;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMoveObjCategory1() {
		return this.moveObjCategory1;
	}

	public void setMoveObjCategory1(String moveObjCategory1) {
		this.moveObjCategory1 = moveObjCategory1;
	}

	public String getMoveObjCategory2() {
		return this.moveObjCategory2;
	}

	public void setMoveObjCategory2(String moveObjCategory2) {
		this.moveObjCategory2 = moveObjCategory2;
	}

	public String getMoveObjType() {
		return this.moveObjType;
	}

	public void setMoveObjType(String moveObjType) {
		this.moveObjType = moveObjType;
	}

	public String getOpType() {
		return this.opType;
	}

	public void setOpType(String opType) {
		this.opType = opType;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getToLoc() {
		return this.toLoc;
	}

	public void setToLoc(String toLoc) {
		this.toLoc = toLoc;
	}

	public String getToLocType() {
		return this.toLocType;
	}

	public void setToLocType(String toLocType) {
		this.toLocType = toLocType;
	}

}