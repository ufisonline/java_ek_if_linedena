package com.ufis_as.ek_if.lido.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the AIRPORT_WEATHER database table.
 * @author btr
 */
@Entity
@Table(name="AIRPORT_WEATHER")
@NamedQueries({
	@NamedQuery(name = "EntDbAirportWeather.findByKey", query = "SELECT e FROM EntDbAirportWeather e WHERE e.airportCodeIata = :iata AND e.airportCodeIcao = :icao ORDER BY e.msgSendDate DESC"),
	@NamedQuery(name = "EntDbAirportWeather.findByIcaoWeatherIndent", query = "SELECT e FROM EntDbAirportWeather e WHERE e.airportCodeIcao = :icao AND e.weatherIndent = :weatherIndent ORDER BY e.msgSendDate DESC")
			})
public class EntDbAirportWeather extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	@Column(name="AIRPORT_CODE_IATA")
	private String airportCodeIata;

	@Column(name="AIRPORT_CODE_ICAO")
	private String airportCodeIcao;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="OBSERVE_TIME")
	private String observeTime;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="VALID_END")
	private String validEnd;

	@Column(name="VALID_START")
	private String validStart;

	@Column(name="WEATHER_IDENTIFIER")
	private String weatherIndent;

	@Column(name="WEATHER_REMARK")
	private String weatherRemark;

	@Column(name="WEATHER_TEXT")
	private String weatherText;

	public EntDbAirportWeather() {
	}
	
	private EntDbAirportWeather(EntDbAirportWeather entity) {
		this.id = entity.id;
		this._createdDate = entity._createdDate;
		this._createdUser = entity._createdUser;
		this._updatedDate = entity._updatedDate;
		this._updatedUser = entity._updatedUser;
		this.version = entity.version;
		this.recStatus = entity.recStatus;
		this.dataSource = entity.dataSource;
		
		this.airportCodeIata = entity.airportCodeIata;
		this.airportCodeIcao = entity.airportCodeIcao;
		this.dataSource = entity.dataSource;
		this.msgSendDate = entity.msgSendDate;
		this.observeTime = entity.observeTime;
		this.validEnd = entity.validEnd;
		this.validStart = entity.validStart;
		this.weatherIndent = entity.weatherIndent;
		this.weatherRemark = entity.weatherRemark;
		this.weatherText = entity.weatherText;
	}
	
	public static EntDbAirportWeather valueOf(EntDbAirportWeather entity) {
		return new EntDbAirportWeather(entity);
	}

	public String getAirportCodeIata() {
		return this.airportCodeIata;
	}

	public void setAirportCodeIata(String airportCodeIata) {
		this.airportCodeIata = airportCodeIata;
	}

	public String getAirportCodeIcao() {
		return this.airportCodeIcao;
	}

	public void setAirportCodeIcao(String airportCodeIcao) {
		this.airportCodeIcao = airportCodeIcao;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	
	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getObserveTime() {
		return this.observeTime;
	}

	public void setObserveTime(String observeTime) {
		this.observeTime = observeTime;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getValidEnd() {
		return this.validEnd;
	}

	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}

	public String getValidStart() {
		return this.validStart;
	}

	public void setValidStart(String validStart) {
		this.validStart = validStart;
	}

	public String getWeatherIndent() {
		return this.weatherIndent;
	}

	public void setWeatherIndent(String weatherIndent) {
		this.weatherIndent = weatherIndent;
	}

	public String getWeatherRemark() {
		return this.weatherRemark;
	}

	public void setWeatherRemark(String weatherRemark) {
		this.weatherRemark = weatherRemark;
	}

	public String getWeatherText() {
		return this.weatherText;
	}

	public void setWeatherText(String weatherText) {
		this.weatherText = weatherText;
	}

}
