package com.ufis_as.ek_if.lido.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the APTTAB database table.
 * 
 */
@Entity
@Table(name="APTTAB")
public class EntDbApttab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long urno;

	private String apc3;

	private String apc4;

	private String apfn;

	private String apn2;

	private String apn3;

	private String apn4;

	private String apsn;

	private String aptt;

	private String cdat;

	private String crif;

	private String etof;

	private String home;

	private String hopo;

	private String land;

	private String lstu;

	private String prfl;

	private String tdi1;

	private String tdi2;

	private String tdis;

	private String tdiw;

	private String tich;

	private String tzon;

	private BigDecimal ucit;

	private String usec;

	private String useu;

	private String vafr;

	private String vato;

	public EntDbApttab() {
	}

	public long getUrno() {
		return this.urno;
	}

	public void setUrno(long urno) {
		this.urno = urno;
	}

	public String getApc3() {
		return this.apc3;
	}

	public void setApc3(String apc3) {
		this.apc3 = apc3;
	}

	public String getApc4() {
		return this.apc4;
	}

	public void setApc4(String apc4) {
		this.apc4 = apc4;
	}

	public String getApfn() {
		return this.apfn;
	}

	public void setApfn(String apfn) {
		this.apfn = apfn;
	}

	public String getApn2() {
		return this.apn2;
	}

	public void setApn2(String apn2) {
		this.apn2 = apn2;
	}

	public String getApn3() {
		return this.apn3;
	}

	public void setApn3(String apn3) {
		this.apn3 = apn3;
	}

	public String getApn4() {
		return this.apn4;
	}

	public void setApn4(String apn4) {
		this.apn4 = apn4;
	}

	public String getApsn() {
		return this.apsn;
	}

	public void setApsn(String apsn) {
		this.apsn = apsn;
	}

	public String getAptt() {
		return this.aptt;
	}

	public void setAptt(String aptt) {
		this.aptt = aptt;
	}

	public String getCdat() {
		return this.cdat;
	}

	public void setCdat(String cdat) {
		this.cdat = cdat;
	}

	public String getCrif() {
		return this.crif;
	}

	public void setCrif(String crif) {
		this.crif = crif;
	}

	public String getEtof() {
		return this.etof;
	}

	public void setEtof(String etof) {
		this.etof = etof;
	}

	public String getHome() {
		return this.home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getHopo() {
		return this.hopo;
	}

	public void setHopo(String hopo) {
		this.hopo = hopo;
	}

	public String getLand() {
		return this.land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public String getLstu() {
		return this.lstu;
	}

	public void setLstu(String lstu) {
		this.lstu = lstu;
	}

	public String getPrfl() {
		return this.prfl;
	}

	public void setPrfl(String prfl) {
		this.prfl = prfl;
	}

	public String getTdi1() {
		return this.tdi1;
	}

	public void setTdi1(String tdi1) {
		this.tdi1 = tdi1;
	}

	public String getTdi2() {
		return this.tdi2;
	}

	public void setTdi2(String tdi2) {
		this.tdi2 = tdi2;
	}

	public String getTdis() {
		return this.tdis;
	}

	public void setTdis(String tdis) {
		this.tdis = tdis;
	}

	public String getTdiw() {
		return this.tdiw;
	}

	public void setTdiw(String tdiw) {
		this.tdiw = tdiw;
	}

	public String getTich() {
		return this.tich;
	}

	public void setTich(String tich) {
		this.tich = tich;
	}

	public String getTzon() {
		return this.tzon;
	}

	public void setTzon(String tzon) {
		this.tzon = tzon;
	}

	public BigDecimal getUcit() {
		return this.ucit;
	}

	public void setUcit(BigDecimal ucit) {
		this.ucit = ucit;
	}

	public String getUsec() {
		return this.usec;
	}

	public void setUsec(String usec) {
		this.usec = usec;
	}

	public String getUseu() {
		return this.useu;
	}

	public void setUseu(String useu) {
		this.useu = useu;
	}

	public String getVafr() {
		return this.vafr;
	}

	public void setVafr(String vafr) {
		this.vafr = vafr;
	}

	public String getVato() {
		return this.vato;
	}

	public void setVato(String vato) {
		this.vato = vato;
	}

}