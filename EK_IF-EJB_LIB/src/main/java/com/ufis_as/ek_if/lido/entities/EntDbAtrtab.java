package com.ufis_as.ek_if.lido.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;


/**
 * The persistent class for the ATRTAB database table.
 * 
 */
@Entity
@Table(name="ATRTAB")
@NamedQueries({
	@NamedQuery(name = "EntDbAtrtab.findByRestAndMsid", query = "SELECT a FROM EntDbAtrtab a WHERE trim(a.rest) = :rest AND trim(a.msid) = :msid ORDER BY a.cdat DESC")
})
public class EntDbAtrtab implements Serializable {
	private static final long serialVersionUID = 1L;

	private String apc3;

	private String apc4;

	private String cdat;

	private String freq;

	private String hopo;

	private String lstu;

	private String msid;

	@Lob
	private String rema;

	private String rest;

	private String tifr;

	private String tito;

	@Id
	private BigDecimal urno;

	private String usec;

	private String useu;

	private String vafr;

	private String vato;

	public EntDbAtrtab() {
	}

	public String getApc3() {
		return this.apc3;
	}

	public void setApc3(String apc3) {
		this.apc3 = apc3;
	}

	public String getApc4() {
		return this.apc4;
	}

	public void setApc4(String apc4) {
		this.apc4 = apc4;
	}

	public String getCdat() {
		return this.cdat;
	}

	public void setCdat(String cdat) {
		this.cdat = cdat;
	}

	public String getFreq() {
		return this.freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getHopo() {
		return this.hopo;
	}

	public void setHopo(String hopo) {
		this.hopo = hopo;
	}

	public String getLstu() {
		return this.lstu;
	}

	public void setLstu(String lstu) {
		this.lstu = lstu;
	}

	public String getMsid() {
		return this.msid;
	}

	public void setMsid(String msid) {
		this.msid = msid;
	}

	public String getRema() {
		return this.rema;
	}

	public void setRema(String rema) {
		this.rema = rema;
	}

	public String getRest() {
		return this.rest;
	}

	public void setRest(String rest) {
		this.rest = rest;
	}

	public String getTifr() {
		return this.tifr;
	}

	public void setTifr(String tifr) {
		this.tifr = tifr;
	}

	public String getTito() {
		return this.tito;
	}

	public void setTito(String tito) {
		this.tito = tito;
	}

	public BigDecimal getUrno() {
		return this.urno;
	}

	public void setUrno(BigDecimal urno) {
		this.urno = urno;
	}

	public String getUsec() {
		return this.usec;
	}

	public void setUsec(String usec) {
		this.usec = usec;
	}

	public String getUseu() {
		return this.useu;
	}

	public void setUseu(String useu) {
		this.useu = useu;
	}

	public String getVafr() {
		return this.vafr;
	}

	public void setVafr(String vafr) {
		this.vafr = vafr;
	}

	public String getVato() {
		return this.vato;
	}

	public void setVato(String vato) {
		this.vato = vato;
	}

}