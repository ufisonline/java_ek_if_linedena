package com.ufis_as.ek_if.linedena.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * The persistent class for the AIRCRAFT_OPS_MSG database table.
 * 
 */
@Entity
@Table(name="AIRCRAFT_OPS_MSG")
@NamedQueries({
@NamedQuery(name = "EntDbAircraftOpsMsg.findRecord", query = "SELECT a FROM EntDbAircraftOpsMsg a WHERE a.idFlight = :idFlight AND  a.msgSeqnNum = :msgSeqnNum AND closeMsgFlag = 'N' AND a.recStatus NOT IN ('X') ")})
public class EntDbAircraftOpsMsg implements Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(EntDbAircraftOpsMsg.class);
	private static final long serialVersionUID = 1L;

	@Column(name="ACTION_REQD")
	private String actionReqd;

	@Column(name="ADD_TIME_MINS")
	private BigDecimal addTimeMins;

	@Column(name="ADD_TIME_REQD")
	private String addTimeReqd;

	@Column(name="ARR_DEP_FLAG")
	private String arrDepFlag;

	@Column(name="ASSIST_REQD")
	private String assistReqd;

	@Column(name="CLOSE_MSG_FLAG")
	private String closeMsgFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	protected Date createdDate;

	@Column(name="CREATED_USER")
	protected String createdUser;

	@Column(name="DATA_SOURCE")
	protected String dataSource;

	@Column(name="DEFECT_DETAILS")
	private String defectDetails;

	@Column(name="DEPT_NAME")
	private String deptName;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="FLT_REGN")
	private String fltRegn;

	@Id
	@Column(name="ID" , nullable = false)
	public String id;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_POST_DATE")
	private Date msgPostDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="MSG_SEQN_NUM")
	private BigDecimal msgSeqnNum;

	@Column(name="MSG_SUBJECT")
	private String msgSubject;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NEXT_UPD_DATE")
	private Date nextUpdDate;

	@Column(name="OPS_REASON")
	private String opsReason;

	@Column(name="OPT_LOCK")
	protected BigDecimal optLock;

	@Column(name="PUBLISH_TO")
	private String publishTo;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	protected Date updatedDate;

	@Column(name="UPDATED_USER")
	protected String updatedUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ALDT")
	private Date aldt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ATOT")
	private Date atot;
	
	@Column(name="ID_RFLIGHT")
	private BigDecimal idRflight;
	
	

	public EntDbAircraftOpsMsg() {
	}

	public String getActionReqd() {
		return this.actionReqd;
	}

	public void setActionReqd(String actionReqd) {
		this.actionReqd = actionReqd;
	}

	public BigDecimal getAddTimeMins() {
		return this.addTimeMins;
	}

	public void setAddTimeMins(BigDecimal addTimeMins) {
		this.addTimeMins = addTimeMins;
	}

	public String getAddTimeReqd() {
		return this.addTimeReqd;
	}

	public void setAddTimeReqd(String addTimeReqd) {
		this.addTimeReqd = addTimeReqd;
	}

	public String getArrDepFlag() {
		return this.arrDepFlag;
	}

	public void setArrDepFlag(String arrDepFlag) {
		this.arrDepFlag = arrDepFlag;
	}

	public String getAssistReqd() {
		return this.assistReqd;
	}

	public void setAssistReqd(String assistReqd) {
		this.assistReqd = assistReqd;
	}

	public String getCloseMsgFlag() {
		return this.closeMsgFlag;
	}

	public void setCloseMsgFlag(String closeMsgFlag) {
		this.closeMsgFlag = closeMsgFlag;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDefectDetails() {
		return this.defectDetails;
	}

	public void setDefectDetails(String defectDetails) {
		this.defectDetails = defectDetails;
	}

	public String getDeptName() {
		return this.deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltRegn() {
		return fltRegn;
	}

	public void setFltRegn(String fltRegn) {
		this.fltRegn = fltRegn;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public Date getMsgPostDate() {
		return this.msgPostDate;
	}

	public void setMsgPostDate(Date msgPostDate) {
		this.msgPostDate = msgPostDate;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public BigDecimal getMsgSeqnNum() {
		return this.msgSeqnNum;
	}

	public void setMsgSeqnNum(BigDecimal msgSeqnNum) {
		this.msgSeqnNum = msgSeqnNum;
	}

	public String getMsgSubject() {
		return this.msgSubject;
	}

	public void setMsgSubject(String msgSubject) {
		this.msgSubject = msgSubject;
	}

	public Date getNextUpdDate() {
		return this.nextUpdDate;
	}

	public void setNextUpdDate(Date nextUpdDate) {
		this.nextUpdDate = nextUpdDate;
	}

	public String getOpsReason() {
		return this.opsReason;
	}

	public void setOpsReason(String opsReason) {
		this.opsReason = opsReason;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getPublishTo() {
		return this.publishTo;
	}

	public void setPublishTo(String publishTo) {
		this.publishTo = publishTo;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	
	
	 public Date getAldt() {
		return aldt;
	}

	public void setAldt(Date aldt) {
		this.aldt = aldt;
	}

	public Date getAtot() {
		return atot;
	}

	public void setAtot(Date atot) {
		this.atot = atot;
	}

	public BigDecimal getIdRflight() {
		return idRflight;
	}

	public void setIdRflight(BigDecimal idRflight) {
		this.idRflight = idRflight;
	}

	@PrePersist
	    void onPersistEntLineDeNA() {
	       if (this.getIdHopo()== null) {
	          this.setIdHopo("DXB");	// "DBS" should read from the configuration later
	       }
	       
	       if (this.createdUser == null){
	    	   this.setCreatedUser(HpEKConstants.DeNA_DATA_SOURCE);
	       }
	       
	       if (this.getCreatedDate() == null){
//	       try {
	   		this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
//	       	} catch (ParseException e) {
//	   		LOG.error("ERROR when getting current UTC time : {}", e.toString());
//	   		}
	       }
	       
	       if (this.getDataSource()== null) {
		          this.setDataSource("DeNA");	
		       }

	       if (this.getRecStatus() == null){
	    	   this.setRecStatus(" ");
	       }

	    }

	    @PreUpdate
	    void onUpdateEntLineDeNAr(){
	       if (this.getIdHopo()== null) {
	          this.setIdHopo("DXB");	// "DBS" should read from the configuration later
	       }
	       
	       if (this.createdUser == null){
	    	   this.setUpdatedUser(HpEKConstants.DeNA_DATA_SOURCE);
	       }
	       
	       if (this.getCreatedDate() == null){
//	       try {
	   		this.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
//	       	} catch (ParseException e) {
//	   		LOG.error("ERROR when getting current UTC time : {}", e.toString());
//	   		}
	       }
	       
	       if (this.getDataSource()== null) {
		          this.setDataSource(HpEKConstants.DeNA_DATA_SOURCE);	
		       }
	       
	       if (this.getRecStatus() == null){
	    	   this.setRecStatus(" ");
	       }
	    }

}