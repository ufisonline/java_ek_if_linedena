package com.ufis_as.ek_if.lms.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 * The persistent class for the MSG_TELEX database table.
 * 
 */
@Entity
@Table(name = "MSG_TELEX")
public class EntDbMsgTelex extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy=GenerationType.IDENTITY) private String id;
	 */

	@Column(name = "ARR_DEP_FLAG")
	private String arrDepFlag;
	
	/*
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @Column(name="CREATED_DATE") private Date createdDate;
	 * 
	 * @Column(name="CREATED_USER") private String createdUser;
	 */

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Column(name = "ID_FLIGHT")
	private BigDecimal idFlight;

	/*
	 * @Column(name="ID_HOPO") private String idHopo;
	 */

	@Column(name = "LOADING_PT")
	private String loadingPt;

	@Column(name = "MSG_FLT_DAY")
	private String msgFltDay;

	@Column(name = "MSG_FLTNO")
	private String msgFltno;

	@Lob
	@Column(name = "MSG_INFO")
	private String msgInfo;

	@Column(name = "MSG_SEND_DAY")
	private String msgSendDay;

	@Column(name = "MSG_SEND_TIME")
	private String msgSendTime;

	@Column(name = "MSG_SENDER")
	private String msgSender;

	@Column(name = "MSG_SUBTYPE")
	private String msgSubtype;

	@Column(name = "MSG_TYPE")
	private String msgType;

	/*
	 * @Column(name="OPT_LOCK") private BigDecimal optLock;
	 */

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Column(name = "SEND_RECV_FLAG")
	private String sendRecvFlag;
	
	@Column(name = "MSG_SEND_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date msgSendDate;

	/*
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @Column(name="UPDATED_DATE") private Date updatedDate;
	 * 
	 * @Column(name="UPDATED_USER") private String updatedUser;
	 */

	public EntDbMsgTelex() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpUfisUtils.isNullOrEmptyStr(_createdUser) ? HpEKConstants.LOAD_CONTROL_MSG : _createdUser;
		this.dataSource = HpUfisUtils.isNullOrEmptyStr(_createdUser) ? HpEKConstants.LOAD_CONTROL_MSG : dataSource;
		this.setHopo(HpEKConstants.EK_HOPO);
		// try {
		if (this.getCreatedDate() == null) {
			this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
		}
		// } catch (ParseException e) {
		// LOG.error("ERROR when getting current UTC time : {}", e.toString());
		// }
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.LOAD_CONTROL_MSG);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbMsgTelex other = (EntDbMsgTelex) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArrDepFlag() {
		return this.arrDepFlag;
	}

	public void setArrDepFlag(String arrDepFlag) {
		this.arrDepFlag = arrDepFlag;
	}

	/*
	 * public Date getCreatedDate() { return this.createdDate; }
	 * 
	 * public void setCreatedDate(Date createdDate) { this.createdDate =
	 * createdDate; }
	 * 
	 * public String getCreatedUser() { return this.createdUser; }
	 * 
	 * public void setCreatedUser(String createdUser) { this.createdUser =
	 * createdUser; }
	 */

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	/*
	 * public String getIdHopo() { return this.idHopo; }
	 * 
	 * public void setIdHopo(String idHopo) { this.idHopo = idHopo; }
	 */

	public String getLoadingPt() {
		return this.loadingPt;
	}

	public void setLoadingPt(String loadingPt) {
		this.loadingPt = loadingPt;
	}

	public String getMsgFltDay() {
		return this.msgFltDay;
	}

	public void setMsgFltDay(String msgFltDay) {
		this.msgFltDay = msgFltDay;
	}

	public String getMsgFltno() {
		return this.msgFltno;
	}

	public void setMsgFltno(String msgFltno) {
		this.msgFltno = msgFltno;
	}

	public String getMsgInfo() {
		return this.msgInfo;
	}

	public void setMsgInfo(String msgInfo) {
		this.msgInfo = msgInfo;
	}

	public String getMsgSendDay() {
		return this.msgSendDay;
	}

	public void setMsgSendDay(String msgSendDay) {
		this.msgSendDay = msgSendDay;
	}

	public String getMsgSendTime() {
		return this.msgSendTime;
	}

	public void setMsgSendTime(String msgSendTime) {
		this.msgSendTime = msgSendTime;
	}

	public String getMsgSender() {
		return this.msgSender;
	}

	public void setMsgSender(String msgSender) {
		this.msgSender = msgSender;
	}

	public String getMsgSubtype() {
		return this.msgSubtype;
	}

	public void setMsgSubtype(String msgSubtype) {
		this.msgSubtype = msgSubtype;
	}

	public String getMsgType() {
		return this.msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/*
	 * public BigDecimal getOptLock() { return this.optLock; }
	 * 
	 * public void setOptLock(BigDecimal optLock) { this.optLock = optLock; }
	 */
	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getSendRecvFlag() {
		return this.sendRecvFlag;
	}

	public void setSendRecvFlag(String sendRecvFlag) {
		this.sendRecvFlag = sendRecvFlag;
	}

	public Date getMsgSendDate() {
		return msgSendDate;
	}
	
	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}
	
	
	/*
	 * public Date getUpdatedDate() { return this.updatedDate; }
	 * 
	 * public void setUpdatedDate(Date updatedDate) { this.updatedDate =
	 * updatedDate; }
	 * 
	 * public String getUpdatedUser() { return this.updatedUser; }
	 * 
	 * public void setUpdatedUser(String updatedUser) { this.updatedUser =
	 * updatedUser; }
	 */

}