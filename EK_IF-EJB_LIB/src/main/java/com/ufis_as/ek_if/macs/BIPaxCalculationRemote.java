package com.ufis_as.ek_if.macs;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import com.ufis_as.ek_if.macs.entities.EntDbFlightIdMapping;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxConn;
/**
 * 
 * @author SCH
 *
 */
@Remote
public interface BIPaxCalculationRemote {
//	public boolean SummarizePaxForMainFlight(String intFlid);
	public boolean SummarizePaxForConnFlight(String intFlid, String airlineCode, String flightNumber, String flightNumberSuffice, Date scheduledFlightDatetime );
	public boolean SummarizePaxForConnFlight(EntDbFlightIdMapping entDbFlightIdMapping, List<EntDbLoadPaxConn> rawPaxConnList);
	public boolean SummarizePaxForMainFlight(EntDbFlightIdMapping entDbFlightIdMapping) ;

}
