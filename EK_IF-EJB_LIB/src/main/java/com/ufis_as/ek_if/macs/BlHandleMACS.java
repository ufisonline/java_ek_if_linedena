/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.macs;

import java.io.StringReader;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.handle.BlHandle;
import com.ufis_as.ek_if.ufis.InterfaceConfig;
import com.ufis_as.exco.ACTIONTYPE;

import ek.macs.flightdetails.FlightDetails;

/**
 * @author $Author$
 * @version $Revision$
 */
public class BlHandleMACS extends BlHandle {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(BlHandleMACS.class);
    private static final String MSGIF = "EK-MACS";
    private FlightDetails _flightData;
    
    public BlHandleMACS(Marshaller marshaller, Unmarshaller um,
            InterfaceConfig interfaceConfig) {
        super(marshaller, um, interfaceConfig);
        //In General it is an Update msg
        //unless flightStatus is X
        _msgActionType = ACTIONTYPE.U;
    }

    @Override
    public boolean unMarshal() {
        try {
            if (_flightXml.contains("FlightDetails")) {
                _flightData = (FlightDetails) _um.unmarshal(new StreamSource(new StringReader(_flightXml)));
                if (readEvent(_flightData)) {
                    LOG.debug("FlightEvent {}", _flightData.getFlightNumber());
                }
                return false;
            }
        } catch (JAXBException ex) {
            LOG.error("Flight Event JAXB exception {}", ex.toString());
            LOG.error("Dropped Message details: \n{}", _flightXml);
            return false;
        }
        return false;
    }

    public boolean readEvent(FlightDetails flightDetails) {

        /*
         *
         The FO (Flight Open) should trigger setting of the MFL_ID for the flight in afttab, and the actual seat configuration
         (they destinguis between the physical seat configuration and the available configuration).
         That will be new fields in afttab, including the Flight status. So a total of 5 new fields.
         The PD (Post Departure) should only set the flight status, but we will use it as trigger on the CEDA server
         to build the load information (PAX and ULD for LOATAB).
         MFL_ID, CLASS_CONFIGURATION, FLIGHT_STATUS.
         The CLASS_CONFIGURATION is a string that should be split into 3 fields. Here is the format: F012J042Y183
         MFID = MFL_ID,
         MAFS = FLIGHT_STATUS
         ACTF = F class seats
         ACTJ = J class seats
         ACTY = Y class seats
         */

/*        if (flightDetails != null) {

            if (flightDetails.getFlightStatus().equals(EnumMACSFlightStatus.FO.toString()))  {

            }
            if (flightDetails.getFlightStatus().equals(EnumMACSFlightStatus.PD.toString()))  {

            }
            if (flightDetails.getFlightStatus().equals(EnumMACSFlightStatus.FC.toString()))  {
                //Read all related MAXS PAX and fwd to CEDA (LOADTAB)
             Get the data and store it, and delete it when we get a FO for the next leg (and change the MFL_ID in the afttab record?)
                 * Elements that Uniquely identify a flight
                 FLIGHT_NUMBER
                 BOARD_POINT                (ORG )
                 OPERATION_DATE             (OPERATION_DATE_GMT should be used)
                 FLIGHT_NUMBER_EXP
                 AIRLINE_DESIGNATOR
                 AIRLINE_DESIGNATOR_EXP
                 
                List<PaxDetails> paxDetails = new ArrayList<>();
                List<FctDetails> fctDetails = new ArrayList<>();
                List<InbDetails> inbDetails = new ArrayList<>();
                List<OnwDetails> ownDetails = new ArrayList<>();
                //Find based on the MAXCS ID
                return true;
            }
            if (flightDetails != null) {
                // Store the data ?
                //Find base on the MAXCS ID
                LOG.info("Find the MAXCS Records based on the ID {}", flightDetails.getMflId());
                return true;
            }
        } else {
            LOG.warn("Flight Event Null detected");

        }*/
        return false;
    }

    public FlightDetails getFlightData() {
        return _flightData;
    }

    public void setFlightData(FlightDetails _flightData) {
        this._flightData = _flightData;
    }

}
