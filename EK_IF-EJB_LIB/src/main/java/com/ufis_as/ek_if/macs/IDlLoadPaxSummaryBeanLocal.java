package com.ufis_as.ek_if.macs;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.connection.FltConnectionDTO;
import com.ufis_as.ek_if.macs.entities.EntDbLoadPaxSummary;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.eao.generic.IAbstractBeanRemote;
@Local
public interface IDlLoadPaxSummaryBeanLocal extends
		IAbstractBeanRemote<EntDbLoadPaxSummary> {

	public EntDbLoadPaxSummary getLoadPaxSummaryByMfid(BigDecimal mfid, String dataSource);
	public List<EntDbLoadPaxSummary> findByArrFlId(BigDecimal arrFlId);
	public List<EntDbLoadPaxSummary> findByDepFlId(BigDecimal depFlId);
	public void persist(EntDbLoadPaxSummary entity);
	public void update(EntDbLoadPaxSummary entity);
	public EntDbLoadPaxSummary findByPk(String id);
	public List<EntDbLoadPaxSummary> findByTimeRange();
	//public List<EntDbLoadPaxSummary> findByTimeRange(HpUfisCalendar from, HpUfisCalendar to);
	public List<Object> findByTimeRange(HpUfisCalendar from, HpUfisCalendar to);
	public List<FltConnectionDTO> findConxFlightById(BigDecimal idFlight, Character adid);
	public EntDbLoadPaxSummary findByintIdNtype(EntDbLoadPaxSummary loadPaxSum);
	public List<FltConnectionDTO> findConxFlightById(BigDecimal idFlight);
}
