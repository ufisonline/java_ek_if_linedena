package com.ufis_as.ek_if.macs;

import javax.ejb.Remote;

@Remote
public interface IPaxDoSummaryRemote {
	public void DoSummary();
}
