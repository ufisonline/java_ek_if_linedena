package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntMaintenance;

@Entity
@Table(name = "FLT_ID_MAPPING")
@NamedQueries({
	@NamedQuery(name = "EntDbFlightIdMapping.findByFlid", query = "SELECT a FROM EntDbFlightIdMapping a WHERE a.intFltId = :intFlId "),
	@NamedQuery(name = "EntDbFlightIdMapping.findByAftCriteria", query = "SELECT a FROM EntDbFlightIdMapping a WHERE a.arrDepFlag = :adid AND a.fltNumber = :flno AND a.fltDate = :schDate "),
	@NamedQuery(name = "EntDbFlightIdMapping.getMappingByFlightId", query = "SELECT a.intFltId FROM EntDbFlightIdMapping a WHERE a.idFlight = :idFlight "),
	@NamedQuery(name = "EntDbFlightIdMapping.getFlightIdByIntFlId", query = "SELECT a.idFlight FROM EntDbFlightIdMapping a WHERE a.intFltId = :intFlId "),
	//@NamedQuery(name = "EntDbFlightIdMapping.getFlightIdByOtherInfo", query = "SELECT a.flightId FROM EntDbFlightIdMapping a WHERE a.airlineCode2 = :airlineCode2 AND a.flightNumber = :flightNumber AND a.flightNumberSuffix = :flightNumberSuffix AND a.scheduledflightdatetime = :scheduledflightdatetime")
})
public class EntDbFlightIdMapping extends EntMaintenance implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "INTERFACE_FLTID")
	private String intFltId;
	
	@Column(name = "ID_FLIGHT")
	private BigDecimal idFlight;
	
	@Column(name = "TMP_FLTID")
	private String tmpFlightId;
	
	@Column(name = "FLIGHT_NUMBER")
	private String fltNumber;
	
	@Column(name = "ARR_DEP_FLAG")
	private Character arrDepFlag;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FLT_DATE")
	private Date fltDate;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FLT_DATE_LOCAL")
	private Date fltDateLocal;	
	
	@Column(name = "REC_STATUS")
	private Character recStatus;
	
	@Column(name = "DATA_SOURCE")
	private String dataSource;
	
	public String getIntFltId() {
		return intFltId;
	}

	public void setIntFltId(String intFltId) {
		this.intFltId = intFltId;
	}

	public BigDecimal getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getTmpFlightId() {
		return tmpFlightId;
	}

	public void setTmpFlightId(String tmpFlightId) {
		this.tmpFlightId = tmpFlightId;
	}

	public String getFltNumber() {
		return fltNumber;
	}

	public void setFltNumber(String fltNumber) {
		this.fltNumber = fltNumber;
	}

	public Character getArrDepFlag() {
		return arrDepFlag;
	}

	public void setArrDepFlag(Character arrDepFlag) {
		this.arrDepFlag = arrDepFlag;
	}

	public Date getFltDate() {
		return fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public Date getFltDateLocal() {
		return fltDateLocal;
	}

	public void setFltDateLocal(Date fltDateLocal) {
		this.fltDateLocal = fltDateLocal;
	}

	public Character getRecStatus() {
		return recStatus;
	}

	public void setRecStatus(Character recStatus) {
		this.recStatus = recStatus;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	@Column(name = "INTSYSTEM")
	private String intSystem;
	
	//@Id
	@Column(name = "INTFLID")
	private String intFlId;
	
	@Column(name = "FLIGHTID")
	private String flightId;
	
	@Column(name = "TEMPFLIGHTID")
	private String tempFlightId;
	
	@Column(name = "AIRLINECODE2")
	private String airlineCode2;
	
	@Column(name = "AIRLINECODE3")
	private String airlineCode3;
	
	@Column(name = "ARRDEPID")
	private String arrDepId;
	
	@Column(name = "FLIGHTNUMBER")
	private String flightNumber;
	
	@Column(name = "FLIGHTNUMBERSUFFIX")
	private String flightNumberSuffix;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SCHEDULEDFLIGHTDATETIME")
	private Date scheduledflightdatetime;

	public String getIntSystem() {
		return intSystem;
	}

	public void setIntSystem(String intSystem) {
		this.intSystem = intSystem;
	}

	public String getIntFlId() {
		return intFlId;
	}

	public void setIntFlId(String intFlId) {
		this.intFlId = intFlId;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getTempFlightId() {
		return tempFlightId;
	}

	public void setTempFlightId(String tempFlightId) {
		this.tempFlightId = tempFlightId;
	}

	public String getAirlineCode2() {
		return airlineCode2;
	}

	public void setAirlineCode2(String airlineCode2) {
		this.airlineCode2 = airlineCode2;
	}

	public String getAirlineCode3() {
		return airlineCode3;
	}

	public void setAirlineCode3(String airlineCode3) {
		this.airlineCode3 = airlineCode3;
	}

	public String getArrDepId() {
		return arrDepId;
	}

	public void setArrDepId(String arrDepId) {
		this.arrDepId = arrDepId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightNumberSuffix() {
		return flightNumberSuffix;
	}

	public void setFlightNumberSuffix(String flightNumberSuffix) {
		this.flightNumberSuffix = flightNumberSuffix;
	}

	public Date getScheduledFlightDatetime() {
		return scheduledflightdatetime;
	}

	public void setScheduledFlightDatetime(Date scheduledflightdatetime) {
		this.scheduledflightdatetime = scheduledflightdatetime;
	}
	*/
	
}
