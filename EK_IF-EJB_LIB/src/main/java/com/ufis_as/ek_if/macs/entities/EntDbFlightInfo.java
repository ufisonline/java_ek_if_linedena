/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.macs.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.LoggerFactory;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;

/**
 * @author $Author$
 * @version $Revision$
 */
@Entity
@Table(name = "FlightInfo")
@NamedQueries({
    @NamedQuery(name = "EntDbFlightInfo.findAll", query = "SELECT a FROM EntDbFlightInfo a"),
    @NamedQuery(name = "EntDbFlightInfo.findByFlid", query = "SELECT a FROM EntDbFlightInfo a WHERE a.intFlId = :intFlId "),
    @NamedQuery(name = "EntDbFlightInfo.findByFligthid", query = "SELECT a FROM EntDbFlightInfo a WHERE a.flightId = :flightId "),
    @NamedQuery(name = "EntDbFlightInfo.findByFlidFligthid", query = "SELECT a FROM EntDbFlightInfo a WHERE a.intFlId = :intFlId and a.flightId = :flightId ")
})
public class EntDbFlightInfo extends EntHopoAssociated {

    /**
     * Logger
     */
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EntDbFlightInfo.class);
    protected String intSystem = "";
    protected String intFlId = "";
    /*UFIS Flight Id */
    protected String flightId = "";
    protected String airlineCode2 = "";
    protected String airlineCode3 = "";
    protected String arrDepId = "";
    protected String flightNumber = "";
    protected String flightNumberSuffix = "";
    @Column(name = "ScheduledFlightDatetime")
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date scheduledFlightDatetime;
    protected String origin3 = "";
    protected String origin4 = "";
    protected String destination3 = "";
    protected String destination4 = "";
    protected String flightStatus = "";

    public EntDbFlightInfo(){}

    public String getIntSystem() {
        return intSystem;
    }

    public void setIntSystem(String intSystem) {
        this.intSystem = intSystem;
    }

    public String getIntFlId() {
        return intFlId;
    }

    public void setIntFlId(String intFlId) {
        this.intFlId = intFlId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirlineCode2() {
        return airlineCode2;
    }

    public void setAirlineCode2(String airlineCode2) {
        this.airlineCode2 = airlineCode2;
    }

    public String getAirlineCode3() {
        return airlineCode3;
    }

    public void setAirlineCode3(String airlineCode3) {
        this.airlineCode3 = airlineCode3;
    }

    public String getArrDepId() {
        return arrDepId;
    }

    public void setArrDepId(String arrDepId) {
        this.arrDepId = arrDepId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightNumberSuffix() {
        return flightNumberSuffix;
    }

    public void setFlightNumberSuffix(String flightNumberSuffix) {
        this.flightNumberSuffix = flightNumberSuffix;
    }

    public Date getScheduledFlightDatetime() {
        return scheduledFlightDatetime;
    }

    public void setScheduledFlightDatetime(Date scheduledFlightDatetime) {
        this.scheduledFlightDatetime = scheduledFlightDatetime;
    }

    public String getOrigin3() {
        return origin3;
    }

    public void setOrigin3(String origin3) {
        this.origin3 = origin3;
    }

    public String getOrigin4() {
        return origin4;
    }

    public void setOrigin4(String origin4) {
        this.origin4 = origin4;
    }

    public String getDestination3() {
        return destination3;
    }

    public void setDestination3(String destination3) {
        this.destination3 = destination3;
    }

    public String getDestination4() {
        return destination4;
    }

    public void setDestination4(String destination4) {
        this.destination4 = destination4;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }


}
