package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author SCH
 */

@Entity
@Table(name = "LOAD_PAX")
@NamedQueries({
		@NamedQuery(name = "EntDbLoadPax.findAll", query = "SELECT a FROM EntDbLoadPax a"),
		@NamedQuery(name = "EntDbLoadPax.findByIdFlight", query = "SELECT a.pKId.intFlId FROM EntDbLoadPax a WHERE a.idFlight = :idFlight"),
		@NamedQuery(name = "EntDbLoadPax.findByFlightAndName", query = "SELECT a FROM EntDbLoadPax a WHERE a.idFlight = :idFlight AND a.paxName = :paxName AND a._recStatus <> 'X' "),
		@NamedQuery(name = "EntDbLoadPax.findByPk", query = "SELECT a FROM EntDbLoadPax a WHERE a.pKId = :pKId"),
		@NamedQuery(name = "EntDbLoadPax.findByPk2", query = "SELECT a.uuid FROM EntDbLoadPax a WHERE a.pKId = :pKId"),
		@NamedQuery(name = "EntDbLoadPax.findByid", query = "SELECT a FROM EntDbLoadPax a WHERE a.intId = :intId "),
		@NamedQuery(name = "EntDbLoadPax.findByFlid", query = "SELECT a FROM EntDbLoadPax a WHERE a.pKId.intFlId = :intFlId "),
		@NamedQuery(name = "EntDbLoadPax.findByPaxIdFlight", query = "SELECT a FROM EntDbLoadPax a WHERE trim(a.pKId.intRefNumber) = :paxRefNum AND a.idFlight = :idFlight"),
		@NamedQuery(name = "EntDbLoadPax.findForPaxLounge", query = "SELECT a FROM EntDbLoadPax a WHERE trim(a.pKId.intRefNumber) = :paxRefNum AND a.pKId.intFlId = :intFlId")})
public class EntDbLoadPax extends EntDbLoadPaxSuper implements Serializable {
	private static final long serialVersionUID = 1L;

	// change to public since HpUfisNotifyFormatter (can not access a member of class com.ufis_as.ek_if.macs.entities.EntDbLoadPax with modifiers "private")
	@EmbeddedId
	public LoadPaxPK pKId;
	
	@Column(name = "DATA_SOURCE")
	private String intSystem;

	@Column(name = "ID_FLIGHT")
	private int idFlight;
	
	// added by 2013.09.30
	@Column(name = "ID_LOAD_PAX_CONNECT")
	private String idLoadPaxConnect;
	
	@Column(name = "BAGNOOFPIECES")
	private BigDecimal bagNoOfPieces;

	@Column(name = "BAGTAGPRINT")
	private String bagTagPrint;

	@Column(name = "BAGWEIGHT")
	private BigDecimal bagWeight;

	@Column(name = "BOARDINGPASSPRINT")
	private String boardingPassprint;

	@Column(name = "BOARDINGSTATUS")
	private String boardingStatus;

	@Column(name = "BOOKEDCLASS")
	private String bookedClass;

	@Column(name = "CABINCLASS")
	private String cabinClass;
	
	// added by 2013.09.30
	@Column(name = "UPGRADE_INDICATOR")
	private String upgradeIndicator;
	
	// added by 2013.09.30
	@Column(name = "TRANSIT_INDICATOR")
	private String transitIndicator;

	@Column(name = "CANCELLED")
	private String cancelled;
	
	// added by 2013.09.30
	@Column(name = "JTOP_PAX")
	private String jtopPax;
	
	// added by 2013.09.30
	@Column(name = "TRANSIT_BAG_INDICATOR")
	private String transitBagIndicator;
	
	// added by 2013.09.30
	@Column(name = "E_TICKET_ID")
	private String eTickedId;
	
	// added by 2013.09.30
	@Column(name = "TICKET_NUMBER")
	private String ticketNumber;
	
//	// added by 2013.09.30
//	@Column(name = "CHK_DIGIT")
//	private String chkDigit;
	
	// added by 2013.09.30
	@Column(name = "COUPON_NUMBER")
	private String couponNumber;
	
	// added by 2013.09.30
		@Column(name = "APD_TYPE")
		private String apdType;
		
		// added by 2013.09.30
		@Column(name = "DOCUMENT_TYPE")
		private String documentType;
		
		// added by 2013.09.30
		@Column(name = "DOCUMENT_NUMBER")
		private String documentNumber;
		
		// added by 2013.09.30
		@Column(name = "DOCUMENT_EXPIRY_DATE")
		private Date documentExpiryDate;
				
		// added by 2013.09.30
		@Column(name = "DOCUMENT_ISSUED_DATE")
		private Date documentIssuedDate;
		
		// added by 2013.09.30
		@Column(name = "DOCUMENT_ISSUED_COUNTRY")
		private String documentIssuedCountry;
		
		// added by 2013.09.30
		@Column(name = "COUNTRY_OF_BIRTH")
		private String countryOfBirth;
		
		// added by 2013.09.30
		@Column(name = "COUNTRY_OF_RESIDENCE")
		private String countryOfResidence;
		
		// added by 2013.09.30
		@Column(name = "ITN_EMBARKATION")
		private String itnEmbarkation;
		
		// added by 2013.09.30
		@Column(name = "ITN_DISEMBARKATION")
		private String itnDisembarkation;
		
//		// added by 2013.09.30
//		@Column(name = "SCAN_STATION")
//		private String scanStation;
		
//		// added by 2013.09.30
//		@Column(name = "SCAN_TERMINAL")
//		private String scanTerminal;
		
//		// added by 2013.09.30
//		@Column(name = "SCAN_TRANSFER_AREA")
//		private String scanTransferArea;
		
//		// added by 2013.09.30
//		@Column(name = "SCANNER_ID")
//		private String scannerId;
		
		
		// added by 2013.09.30
		@Column(name = "PAX_STATUS")
		private String paxStatus;
		
	

	@Column(name = "CHECKINDATETIME")
	private Date checkInDateTime;

	@Column(name = "DESTINATION")
	private String destination;

	@Temporal(TemporalType.DATE)
	private Date dob;

	@Column(name = "ETKTYPE")
	private String etkType;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "HANDICAPPED")
	private String handicapped;

	@Column(name = "INFANTINDICATOR")
	private String infantIndicator;

	@Column(name = "INTID")
	private String intId;

	@Column(name = "NATIONALITY")
	private String nationality;

	@Column(name = "OFFLOADEDPAX")
	private String offLoadedPax;

	@Column(name = "PAXBOOKINGSTATUS")
	private String paxBookingStatus;

	@Column(name = "PAXGROUPCODE")
	private String paxGroupCode;

	@Column(name = "PAXNAME")
	private String paxName;

	@Column(name = "PAXTYPE")
	private String paxType;

	@Column(name = "PRIORITYPAX")
	private String priorityPax;

	@Column(name = "STATUSONBOARD")
	private String statusOnboard;

	@Column(name = "TRAVELLEDCLASS")
	private String travelledClass;
	
	// added by 2013.09.30
	@Column(name = "SEAT_NUMBER")
	private String seatNumber;

	@Column(name = "UNACCOMPANIEDMINOR")
	private String unAccompaniedMinor;

	@Column(name = "BAGTAGINFO")
	private String bagTagInfo;

	@Column(name = "SCANLOCATION")
	private String scanLocation;

	@Column(name = "SCANDATETIME")
	private Date scanDateTime;

	@Column(name = "CHECKINAGENTCODE")
	private String checkInAgentCode;

	@Column(name = "CHECKINHANDLINGAGENT")
	private String checkInHandlingAgent;

	@Column(name = "CHECKINSEQUENCE")
	private String checkInSequence;

	@Column(name = "CHECKINCITY")
	private String checkInCity;

	@Column(name = "BOARDINGDATETIME")
	private Date boardingDateTime;

	@Column(name = "BOARDINGAGENTCODE")
	private String boardingAgentCode;

	@Column(name = "BOARDINGHANDLINGAGENT")
	private String boardingHandlingAgent;

	// bi-directional many-to-one association to PaxConn
	@OneToMany(mappedBy = "pax")
	private List<EntDbLoadPaxConn> entDbPaxConns;

	public List<EntDbLoadPaxConn> getEntDbPaxConns() {
		return entDbPaxConns;
	}

	public void setEntDbPaxConns(List<EntDbLoadPaxConn> entDbPaxConns) {
		this.entDbPaxConns = entDbPaxConns;
	}

	public EntDbLoadPax() {
	}

	public BigDecimal getBagNoOfPieces() {
		return bagNoOfPieces;
	}

	public void setBagNoOfPieces(BigDecimal bagNoOfPieces) {
		this.bagNoOfPieces = bagNoOfPieces;
	}

	public String getBagTagPrint() {
		return bagTagPrint;
	}

	public void setBagTagPrint(String bagTagPrint) {
		this.bagTagPrint = bagTagPrint;
	}

	public BigDecimal getBagWeight() {
		return bagWeight;
	}

	public void setBagWeight(BigDecimal bagWeight) {
		this.bagWeight = bagWeight;
	}

	public String getBoardingPassprint() {
		return boardingPassprint;
	}

	public void setBoardingPassprint(String boardingPassprint) {
		this.boardingPassprint = boardingPassprint;
	}

	public String getBoardingStatus() {
		return boardingStatus;
	}

	public void setBoardingStatus(String boardingStatus) {
		this.boardingStatus = boardingStatus;
	}

	public String getBookedClass() {
		return bookedClass;
	}

	public void setBookedClass(String bookedClass) {
		this.bookedClass = bookedClass;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getCancelled() {
		return cancelled;
	}

	public void setCancelled(String cancelled) {
		this.cancelled = cancelled;
	}

	public Date getCheckInDateTime() {
		return checkInDateTime;
	}

	public void setCheckInDateTime(Date checkInDateTime) {
		this.checkInDateTime = checkInDateTime;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEtkType() {
		return etkType;
	}

	public void setEtkType(String etkType) {
		this.etkType = etkType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHandicapped() {
		return handicapped;
	}

	public void setHandicapped(String handicapped) {
		this.handicapped = handicapped;
	}

	public String getInfantIndicator() {
		return infantIndicator;
	}

	public void setInfantIndicator(String infantIndicator) {
		this.infantIndicator = infantIndicator;
	}

	public LoadPaxPK getPKId() {
	//public LoadPaxPK getpKId() {
		return this.pKId;
	}

	public void setPKId(LoadPaxPK pKId) {
	//public void setpKId(LoadPaxPK pKId) {
		this.pKId = pKId;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOffLoadedPax() {
		return offLoadedPax;
	}

	public void setOffLoadedPax(String offLoadedPax) {
		this.offLoadedPax = offLoadedPax;
	}

	public String getPaxBookingStatus() {
		return paxBookingStatus;
	}

	public void setPaxBookingStatus(String paxBookingStatus) {
		this.paxBookingStatus = paxBookingStatus;
	}

	public String getPaxGroupCode() {
		return paxGroupCode;
	}

	public void setPaxGroupCode(String paxGroupCode) {
		this.paxGroupCode = paxGroupCode;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPriorityPax() {
		return priorityPax;
	}

	public void setPriorityPax(String priorityPax) {
		this.priorityPax = priorityPax;
	}

	public String getStatusOnboard() {
		return statusOnboard;
	}

	public void setStatusOnboard(String statusOnboard) {
		this.statusOnboard = statusOnboard;
	}

	public String getTravelledClass() {
		return travelledClass;
	}

	public void setTravelledClass(String travelledClass) {
		this.travelledClass = travelledClass;
	}

	public String getUnAccompaniedMinor() {
		return unAccompaniedMinor;
	}

	public void setUnAccompaniedMinor(String unAccompaniedMinor) {
		this.unAccompaniedMinor = unAccompaniedMinor;
	}

	public String getBagTagInfo() {
		return bagTagInfo;
	}

	public void setBagTagInfo(String bagTagInfo) {
		this.bagTagInfo = bagTagInfo;
	}

	public String getScanLocation() {
		return scanLocation;
	}

	public void setScanLocation(String scanLocation) {
		this.scanLocation = scanLocation;
	}

	public Date getScanDateTime() {
		return scanDateTime;
	}

	public void setScanDateTime(Date scanDateTime) {
		this.scanDateTime = scanDateTime;
	}

	public String getIntId() {
		return intId;
	}

	public void setIntId(String intId) {
		this.intId = intId;
	}

	public String getCheckInHandlingAgent() {
		return checkInHandlingAgent;
	}

	public void setCheckInHandlingAgent(String checkInHandlingAgent) {
		this.checkInHandlingAgent = checkInHandlingAgent;
	}

	public String getCheckInSequence() {
		return checkInSequence;
	}

	public void setCheckInSequence(String checkInSequence) {
		this.checkInSequence = checkInSequence;
	}

	public String getCheckInCity() {
		return checkInCity;
	}

	public void setCheckInCity(String checkInCity) {
		this.checkInCity = checkInCity;
	}

	public Date getBoardingDateTime() {
		return boardingDateTime;
	}

	public void setBoardingDateTime(Date boardingDateTime) {
		this.boardingDateTime = boardingDateTime;
	}

	public String getBoardingAgentCode() {
		return boardingAgentCode;
	}

	public void setBoardingAgentCode(String boardingAgentCode) {
		this.boardingAgentCode = boardingAgentCode;
	}

	public String getBoardingHandlingAgent() {
		return boardingHandlingAgent;
	}

	public void setBoardingHandlingAgent(String boardingHandlingAgent) {
		this.boardingHandlingAgent = boardingHandlingAgent;
	}

	public String getCheckInAgentCode() {
		return checkInAgentCode;
	}

	public void setCheckInAgentCode(String checkInAgentCode) {
		this.checkInAgentCode = checkInAgentCode;
	}

	public int getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}

	public LoadPaxPK getpKId() {
		return pKId;
	}

	public void setpKId(LoadPaxPK pKId) {
		this.pKId = pKId;
	}

	public String getIdLoadPaxConnect() {
		return idLoadPaxConnect;
	}

	public void setIdLoadPaxConnect(String idLoadPaxConnect) {
		this.idLoadPaxConnect = idLoadPaxConnect;
	}

	public String getUpgradeIndicator() {
		return upgradeIndicator;
	}

	public void setUpgradeIndicator(String upgradeIndicator) {
		this.upgradeIndicator = upgradeIndicator;
	}

	public String getTransitIndicator() {
		return transitIndicator;
	}

	public void setTransitIndicator(String transitIndicator) {
		this.transitIndicator = transitIndicator;
	}

	public String getJtopPax() {
		return jtopPax;
	}

	public void setJtopPax(String jtopPax) {
		this.jtopPax = jtopPax;
	}

	public String getTransitBagIndicator() {
		return transitBagIndicator;
	}

	public void setTransitBagIndicator(String transitBagIndicator) {
		this.transitBagIndicator = transitBagIndicator;
	}

	public String getETickedId() {
		return eTickedId;
	}

	public void setETickedId(String eTickedId) {
		this.eTickedId = eTickedId;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

//	public String getChkDigit() {
//		return chkDigit;
//	}
//
//	public void setChkDigit(String chkDigit) {
//		this.chkDigit = chkDigit;
//	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getApdType() {
		return apdType;
	}

	public void setApdType(String apdType) {
		this.apdType = apdType;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Date getDocumentExpiryDate() {
		return documentExpiryDate;
	}

	public void setDocumentExpiryDate(Date documentExpiryDate) {
		this.documentExpiryDate = documentExpiryDate;
	}

	public Date getDocumentIssuedDate() {
		return documentIssuedDate;
	}

	public void setDocumentIssuedDate(Date documentIssuedDate) {
		this.documentIssuedDate = documentIssuedDate;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public String getItnEmbarkation() {
		return itnEmbarkation;
	}

	public void setItnEmbarkation(String itnEmbarkation) {
		this.itnEmbarkation = itnEmbarkation;
	}

	public String getItnDisembarkation() {
		return itnDisembarkation;
	}

	public void setItnDisembarkation(String itnDisembarkation) {
		this.itnDisembarkation = itnDisembarkation;
	}

//	public String getScanStation() {
//		return scanStation;
//	}
//
//	public void setScanStation(String scanStation) {
//		this.scanStation = scanStation;
//	}

//	public String getScanTerminal() {
//		return scanTerminal;
//	}
//
//	public void setScanTerminal(String scanTerminal) {
//		this.scanTerminal = scanTerminal;
//	}
//
//	public String getScanTransferArea() {
//		return scanTransferArea;
//	}
//
//	public void setScanTransferArea(String scanTransferArea) {
//		this.scanTransferArea = scanTransferArea;
//	}

//	public String getScannerId() {
//		return scannerId;
//	}
//
//	public void setScannerId(String scannerId) {
//		this.scannerId = scannerId;
//	}

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getDocumentIssuedCountry() {
		return documentIssuedCountry;
	}

	public void setDocumentIssuedCountry(String documentIssuedCountry) {
		this.documentIssuedCountry = documentIssuedCountry;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getIntSystem() {
		return intSystem;
	}

	public void setIntSystem(String intSystem) {
		this.intSystem = intSystem;
	}


	public String geteTickedId() {
		return eTickedId;
	}

	public void seteTickedId(String eTickedId) {
		this.eTickedId = eTickedId;
	}


	
	

}