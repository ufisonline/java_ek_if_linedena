package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author SCH
 * 
 */

@Entity
@Table(name = "LOAD_PAX_CONNECT")
@NamedQueries({
		@NamedQuery(name = "EntDbLoadPaxConn.findAll", query = "SELECT a FROM EntDbLoadPaxConn a"),
		@NamedQuery(name = "EntDbLoadPaxConn.findByPk", query = "SELECT a FROM EntDbLoadPaxConn a WHERE a.intId = :intId and a.intSystem = :intSystem"),
		@NamedQuery(name = "EntDbLoadPaxConn.findByPk2", query = "SELECT a.uuid FROM EntDbLoadPaxConn a WHERE a.intId = :intId and a.intSystem = :intSystem"),
		@NamedQuery(name = "EntDbLoadPaxConn.findByFlid", query = "SELECT a FROM EntDbLoadPaxConn a WHERE a.paxConnPK.interfaceFltid = :intFlId and a.intSystem = :intSystem "),
		@NamedQuery(name = "EntDbLoadPaxConn.findByIdAndType", query = "SELECT a FROM EntDbLoadPaxConn a WHERE a.intId = :intId and a.connType = :connType "),
		@NamedQuery(name = "EntDbLoadPaxConn.findByIntFlid", query = "SELECT a FROM EntDbLoadPaxConn a WHERE a.paxConnPK.interfaceFltid = :intFlId") })
public class EntDbLoadPaxConn extends EntDbLoadPaxSuper implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	public LoadPaxConnPK paxConnPK;

	@Column(name = "ID_FLIGHT")
	private int idFlight;

	// added by 2013.09.30
	@Column(name = "ID_LOAD_PAX")
	private String idLoadPax;

	// removed by 2013.09.30
	// @Column(name = "AIRLINECODE")
	// private String airlineCode;

	@Column(name = "BOARDPOINT")
	private String boardPoint;

	@Column(name = "BOOKEDCLASS")
	private String bookedClass;

	@Column(name = "CONNSTATUS")
	private String connStatus;

	@Column(name = "CONNTYPE")
	private String connType;

	// removed by 2013.09.30
	// @Column(name = "FLIGHTNUMBER")
	// private String flightNumber;

	// removed by 2013.09.30
	// @Column(name = "FLIGHTNUMBERSUFFICE")
	// private String flightNumberSuffice;

	// @Column(name = "INTFLID")
	// private String intFlId;

//	@Id
	@Column(name = "INTID")
	private String intId;

	// commented on 2013 12 04
//	// added by 2013.09.30
//	@Column(name = "INTERFACE_FLTID")
//	private String interfaceFltid;

	// added by 2013.09.30
	@Column(name = "INTERFACE_CONX_FLTID")
	private String interfaceConxFltid;

	// added by 2013.09.30
	@Column(name = "PAX_CONX_FLNO")
	private String paxConxFlno;

	// added by 2013.09.30
	@Column(name = "CONX_FLT_DATE")
	private Date conxFltDate;

	// added by 2013.09.30
	@Column(name = "ID_CONX_FLIGHT")
	private int idConxFlight;

	// commented on 2013 12 04
//	@Column(name = "INTREFNUMBER")
//	private String intRefNumber;

	@Column(name = "DATA_SOURCE")
	private String intSystem;

	@Column(name = "OFFPOINT")
	private String offPoint;

	// removed by 2013.09.30
	// @Column(name = "SCHEDULEDFLIGHTDATETIME")
	// @Temporal(value = TemporalType.TIMESTAMP)
	// private Date scheduledFlightDateTime;

	@Transient
	private String downloadDate;

	@Transient
	private String msgTimeStamp;

	@Transient
	private String variableData;

	// bi-directional many-to-one association to Pax
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "INTERFACE_FLTID", referencedColumnName = "INTFLID", insertable = false, updatable = false),
			@JoinColumn(name = "INTREFNUMBER", referencedColumnName = "INTREFNUMBER", insertable = false, updatable = false) })
//			@JoinColumn(name = "DATA_SOURCE", referencedColumnName = "DATA_SOURCE", insertable = false, updatable = false) })
	private EntDbLoadPax pax;

	public String getVariableData() {
		return variableData;
	}

	public void setVariableData(String variableData) {
		this.variableData = variableData;
	}

	public String getMsgTimeStamp() {
		return msgTimeStamp;
	}

	public void setMsgTimeStamp(String msgTimeStamp) {
		this.msgTimeStamp = msgTimeStamp;
	}

	public String getDownloadDate() {
		return downloadDate;
	}

	public void setDownloadDate(String downloadDate) {
		this.downloadDate = downloadDate;
	}

	public EntDbLoadPaxConn() {
	}

	// public String getAirlineCode() {
	// return airlineCode;
	// }
	//
	// public void setAirlineCode(String airlineCode) {
	// this.airlineCode = airlineCode;
	// }

	public String getBoardPoint() {
		return boardPoint;
	}

	public void setBoardPoint(String boardPoint) {
		this.boardPoint = boardPoint;
	}

	public String getBookedClass() {
		return bookedClass;
	}

	public void setBookedClass(String bookedClass) {
		this.bookedClass = bookedClass;
	}

	public String getConnStatus() {
		return connStatus;
	}

	public void setConnStatus(String connStatus) {
		this.connStatus = connStatus;
	}

	public String getConnType() {
		return connType;
	}

	public void setConnType(String connType) {
		this.connType = connType;
	}


	public String getInterfaceConxFltid() {
		return interfaceConxFltid;
	}

	public void setInterfaceConxFltid(String interfaceConxFltid) {
		this.interfaceConxFltid = interfaceConxFltid;
	}

	public int getIdConxFlight() {
		return idConxFlight;
	}

	public void setIdConxFlight(int idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

	public String getIntId() {
		return intId;
	}

	public void setIntId(String intId) {
		this.intId = intId;
	}

	// commented on 2013 12 04
//	public String getIntRefNumber() {
//		return intRefNumber;
//	}
//
//	public void setIntRefNumber(String intRefNumber) {
//		this.intRefNumber = intRefNumber;
//	}

	public String getIntSystem() {
		return intSystem;
	}

	public void setIntSystem(String intSystem) {
		this.intSystem = intSystem;
	}

	public String getOffPoint() {
		return offPoint;
	}

	public void setOffPoint(String offPoint) {
		this.offPoint = offPoint;
	}

	// public Date getScheduledFlightDateTime() {
	// return scheduledFlightDateTime;
	// }
	//
	// public void setScheduledFlightDateTime(Date scheduledFlightDateTime) {
	// this.scheduledFlightDateTime = scheduledFlightDateTime;
	// }

	public EntDbLoadPax getPax() {
		return pax;
	}

	public void setPax(EntDbLoadPax pax) {
		this.pax = pax;
	}

	public int getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdLoadPax() {
		return idLoadPax;
	}

	public void setIdLoadPax(String idLoadPax) {
		this.idLoadPax = idLoadPax;
	}
	
	

	// commented on 2013 12 04
//	public String getInterfaceFltid() {
//		return interfaceFltid;
//	}
//
//	public void setInterfaceFltid(String interfaceFltid) {
//		this.interfaceFltid = interfaceFltid;
//	}

	public LoadPaxConnPK getPaxConnPK() {
		return paxConnPK;
	}

	public void setPaxConnPK(LoadPaxConnPK paxConnPK) {
		this.paxConnPK = paxConnPK;
	}

	public String getPaxConxFlno() {
		return paxConxFlno;
	}

	public void setPaxConxFlno(String paxConxFlno) {
		this.paxConxFlno = paxConxFlno;
	}

	public Date getConxFltDate() {
		return conxFltDate;
	}

	public void setConxFltDate(Date conxFltDate) {
		this.conxFltDate = conxFltDate;
	}


}