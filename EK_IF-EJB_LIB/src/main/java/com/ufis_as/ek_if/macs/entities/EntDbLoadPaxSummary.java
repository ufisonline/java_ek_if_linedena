package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;

@Entity
@Table(name = "LOAD_PAX_SUMMARY")
@NamedQueries({
	@NamedQuery(name= "EntDbLoadPaxSummary.findByMfid", query = "SELECT e FROM EntDbLoadPaxSummary e WHERE e.interfaceFltId =:mfid AND e.infoType =:infoType"),
	@NamedQuery(name = "EntDbLoadPaxSummary.findByPk", query = "SELECT a FROM EntDbLoadPaxSummary a WHERE a.id = :id"),
	//@NamedQuery(name = "EntDbLoadPaxSummary.findByTimeRange", query = "SELECT a FROM EntDbLoadPaxSummary a WHERE ((a._createdDate BETWEEN :starDate AND :endDate) OR (a._updatedDate BETWEEN :starDate AND :endDate)) AND a.infoType = :infoType AND a.totalPax > 0 AND a.idArrFlight is not null"),
	//@NamedQuery(name = "EntDbLoadPaxSummary.findIdArrFlightByTimeRange", query = "SELECT DISTINCT a.idArrFlight FROM EntDbLoadPaxSummary a WHERE ((a._createdDate BETWEEN :starDate AND :endDate) OR (a._updatedDate BETWEEN :starDate AND :endDate)) AND a.infoType = :infoType AND a.idArrFlight is not null"),
	@NamedQuery(name = "EntDbLoadPaxSummary.findIdArrFlightByTimeRange", query = "SELECT DISTINCT a.idFlight FROM EntDbLoadPaxSummary a WHERE ((a._createdDate BETWEEN :starDate AND :endDate) OR (a._updatedDate BETWEEN :starDate AND :endDate)) AND a.infoType = :infoType AND a.idFlight is not null"),
	@NamedQuery(name = "EntDbLoadPaxSummary.findByArrFlId", query = "SELECT a FROM EntDbLoadPaxSummary a WHERE a.idArrFlight = :idArrFlight AND a.infoType = :infoType AND a.totalPax > 0"),
	@NamedQuery(name = "EntDbLoadPaxSummary.findByintIdNtype", query = "SELECT a FROM EntDbLoadPaxSummary a WHERE a.interfaceFltId = :interfaceFltId AND a.infoType = :infoType"),
	@NamedQuery(name = "EntDbLoadPaxSummary.findByDepFlId", query = "SELECT a FROM EntDbLoadPaxSummary a WHERE a.idDepFlight = :idDepFlight AND a.infoType = :infoType AND a.totalPax > 0")
})
public class EntDbLoadPaxSummary extends EntHopoAssociated implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "ADULT_PAX")
	private BigDecimal adultPax;

	@Column(name = "ALL_ECON_ADULT")
	private BigDecimal allEconAdult;

	@Column(name = "ALL_ECON_CHILD")
	private BigDecimal allEconChild;

	@Column(name = "ALL_ECON_FEMALE_PAX")
	private BigDecimal allEconFemalePax;

	@Column(name = "ALL_ECON_INFANT")
	private BigDecimal allEconInfant;

	@Column(name = "ALL_ECON_MALE_PAX")
	private BigDecimal allEconMalePax;

	@Column(name = "ALL_ECON_PAX")
	private BigDecimal allEconPax;

	@Column(name = "BUSINESS_ADULT")
	private BigDecimal businessAdult;

	@Column(name = "BUSINESS_CHILD")
	private BigDecimal businessChild;

	@Column(name = "BUSINESS_FEMALE_PAX")
	private BigDecimal businessFemalePax;

	@Column(name = "BUSINESS_INFANT")
	private BigDecimal businessInfant;

	@Column(name = "BUSINESS_MALE_PAX")
	private BigDecimal businessMalePax;

	@Column(name = "BUSINESS_PAX")
	private BigDecimal businessPax;

	@Column(name = "CABIN_CREW")
	private BigDecimal cabinCrew;

	@Column(name = "CHILD_PAX")
	private BigDecimal childPax;

	@Column(name = "CONX_TYPE")
	private String conxType;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Column(name = "DEAD_HEAD_CREW")
	private BigDecimal deadHeadCrew;

	@Column(name = "ECON_ADULT")
	private BigDecimal econAdult;

	@Column(name = "ECON_CHILD")
	private BigDecimal econChild;

	@Column(name = "ECON_FEMALE_PAX")
	private BigDecimal econFemalePax;

	@Column(name = "ECON_INFANT")
	private BigDecimal econInfant;

	@Column(name = "ECON_MALE_PAX")
	private BigDecimal econMalePax;

	@Column(name = "ECON_PAX")
	private BigDecimal econPax;

	@Column(name = "ECON2_ADULT")
	private BigDecimal econ2Adult;

	@Column(name = "ECON2_CHILD")
	private BigDecimal econ2Child;

	@Column(name = "ECON2_FEMALE_PAX")
	private BigDecimal econ2FemalePax;

	@Column(name = "ECON2_INFANT")
	private BigDecimal econ2Infant;

	@Column(name = "ECON2_MALE_PAX")
	private BigDecimal econ2MalePax;

	@Column(name = "ECON2_PAX")
	private BigDecimal econ2Pax;

	@Column(name = "FEMALE_PAX")
	private BigDecimal femalePax;

	@Column(name = "FIRST_ADULT")
	private BigDecimal firstAdult;

	@Column(name = "FIRST_CHILD")
	private BigDecimal firstChild;

	@Column(name = "FIRST_FEMALE_PAX")
	private BigDecimal firstFemalePax;

	@Column(name = "FIRST_INFANT")
	private BigDecimal firstInfant;

	@Column(name = "FIRST_MALE_PAX")
	private BigDecimal firstMalePax;

	@Column(name = "FIRST_PAX")
	private BigDecimal firstPax;

	@Temporal(TemporalType.DATE)
	@Column(name = "FLT_DATE")
	private Date fltDate;

	@Column(name = "HOP_FROM")
	private String hopFrom;

	@Column(name = "HOP_TO")
	private String hopTo;

	@Column(name = "ID_ARR_FLIGHT")
	private BigDecimal idArrFlight;

	@Column(name = "ID_DEP_FLIGHT")
	private BigDecimal idDepFlight;
	
	@Column(name = "ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_CONX_FLIGHT")
	private BigDecimal idConxFlight;
	
	@Column(name = "INFANT_PAX")
	private BigDecimal infantPax;

	@Column(name = "INFO_TYPE")
	private String infoType;

	@Column(name = "INTERFACE_FLT_ID")
	private BigDecimal interfaceFltId;

	@Column(name = "JUMP_PAX")
	private BigDecimal jumpPax;

	@Column(name = "MALE_PAX")
	private BigDecimal malePax;

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Column(name = "TECH_CREW")
	private BigDecimal techCrew;

	@Column(name = "TOTAL_CREW")
	private BigDecimal totalCrew;

	@Column(name = "TOTAL_PAX")
	private BigDecimal totalPax;

	public BigDecimal getAdultPax() {
		return adultPax;
	}
	
	public void setAdultPax(BigDecimal adultPax) {
		this.adultPax = adultPax;
	}

	public BigDecimal getAllEconAdult() {
		return allEconAdult;
	}

	public void setAllEconAdult(BigDecimal allEconAdult) {
		this.allEconAdult = allEconAdult;
	}

	public BigDecimal getAllEconChild() {
		return allEconChild;
	}

	public void setAllEconChild(BigDecimal allEconChild) {
		this.allEconChild = allEconChild;
	}

	public BigDecimal getAllEconFemalePax() {
		return allEconFemalePax;
	}

	public void setAllEconFemalePax(BigDecimal allEconFemalePax) {
		this.allEconFemalePax = allEconFemalePax;
	}

	public BigDecimal getAllEconInfant() {
		return allEconInfant;
	}

	public void setAllEconInfant(BigDecimal allEconInfant) {
		this.allEconInfant = allEconInfant;
	}

	public BigDecimal getAllEconMalePax() {
		return allEconMalePax;
	}

	public void setAllEconMalePax(BigDecimal allEconMalePax) {
		this.allEconMalePax = allEconMalePax;
	}

	public BigDecimal getAllEconPax() {
		return allEconPax;
	}

	public void setAllEconPax(BigDecimal allEconPax) {
		this.allEconPax = allEconPax;
	}

	public BigDecimal getBusinessAdult() {
		return businessAdult;
	}

	public void setBusinessAdult(BigDecimal businessAdult) {
		this.businessAdult = businessAdult;
	}

	public BigDecimal getBusinessChild() {
		return businessChild;
	}

	public void setBusinessChild(BigDecimal businessChild) {
		this.businessChild = businessChild;
	}

	public BigDecimal getBusinessFemalePax() {
		return businessFemalePax;
	}

	public void setBusinessFemalePax(BigDecimal businessFemalePax) {
		this.businessFemalePax = businessFemalePax;
	}

	public BigDecimal getBusinessInfant() {
		return businessInfant;
	}

	public void setBusinessInfant(BigDecimal businessInfant) {
		this.businessInfant = businessInfant;
	}

	public BigDecimal getBusinessMalePax() {
		return businessMalePax;
	}

	public void setBusinessMalePax(BigDecimal businessMalePax) {
		this.businessMalePax = businessMalePax;
	}

	public BigDecimal getBusinessPax() {
		return businessPax;
	}

	public void setBusinessPax(BigDecimal businessPax) {
		this.businessPax = businessPax;
	}

	public BigDecimal getCabinCrew() {
		return cabinCrew;
	}

	public void setCabinCrew(BigDecimal cabinCrew) {
		this.cabinCrew = cabinCrew;
	}

	public BigDecimal getChildPax() {
		return childPax;
	}

	public void setChildPax(BigDecimal childPax) {
		this.childPax = childPax;
	}

	public String getConxType() {
		return conxType;
	}

	public void setConxType(String conxType) {
		this.conxType = conxType;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getDeadHeadCrew() {
		return deadHeadCrew;
	}

	public void setDeadHeadCrew(BigDecimal deadHeadCrew) {
		this.deadHeadCrew = deadHeadCrew;
	}

	public BigDecimal getEconAdult() {
		return econAdult;
	}

	public void setEconAdult(BigDecimal econAdult) {
		this.econAdult = econAdult;
	}

	public BigDecimal getEconChild() {
		return econChild;
	}

	public void setEconChild(BigDecimal econChild) {
		this.econChild = econChild;
	}

	public BigDecimal getEconFemalePax() {
		return econFemalePax;
	}

	public void setEconFemalePax(BigDecimal econFemalePax) {
		this.econFemalePax = econFemalePax;
	}

	public BigDecimal getEconInfant() {
		return econInfant;
	}

	public void setEconInfant(BigDecimal econInfant) {
		this.econInfant = econInfant;
	}

	public BigDecimal getEconMalePax() {
		return econMalePax;
	}

	public void setEconMalePax(BigDecimal econMalePax) {
		this.econMalePax = econMalePax;
	}

	public BigDecimal getEconPax() {
		return econPax;
	}

	public void setEconPax(BigDecimal econPax) {
		this.econPax = econPax;
	}

	public BigDecimal getEcon2Adult() {
		return econ2Adult;
	}

	public void setEcon2Adult(BigDecimal econ2Adult) {
		this.econ2Adult = econ2Adult;
	}

	public BigDecimal getEcon2Child() {
		return econ2Child;
	}

	public void setEcon2Child(BigDecimal econ2Child) {
		this.econ2Child = econ2Child;
	}

	public BigDecimal getEcon2FemalePax() {
		return econ2FemalePax;
	}

	public void setEcon2FemalePax(BigDecimal econ2FemalePax) {
		this.econ2FemalePax = econ2FemalePax;
	}

	public BigDecimal getEcon2Infant() {
		return econ2Infant;
	}

	public void setEcon2Infant(BigDecimal econ2Infant) {
		this.econ2Infant = econ2Infant;
	}

	public BigDecimal getEcon2MalePax() {
		return econ2MalePax;
	}

	public void setEcon2MalePax(BigDecimal econ2MalePax) {
		this.econ2MalePax = econ2MalePax;
	}

	public BigDecimal getEcon2Pax() {
		return econ2Pax;
	}

	public void setEcon2Pax(BigDecimal econ2Pax) {
		this.econ2Pax = econ2Pax;
	}

	public BigDecimal getFemalePax() {
		return femalePax;
	}

	public void setFemalePax(BigDecimal femalePax) {
		this.femalePax = femalePax;
	}

	public BigDecimal getFirstAdult() {
		return firstAdult;
	}

	public void setFirstAdult(BigDecimal firstAdult) {
		this.firstAdult = firstAdult;
	}

	public BigDecimal getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(BigDecimal firstChild) {
		this.firstChild = firstChild;
	}

	public BigDecimal getFirstFemalePax() {
		return firstFemalePax;
	}

	public void setFirstFemalePax(BigDecimal firstFemalePax) {
		this.firstFemalePax = firstFemalePax;
	}

	public BigDecimal getFirstInfant() {
		return firstInfant;
	}

	public void setFirstInfant(BigDecimal firstInfant) {
		this.firstInfant = firstInfant;
	}

	public BigDecimal getFirstMalePax() {
		return firstMalePax;
	}

	public void setFirstMalePax(BigDecimal firstMalePax) {
		this.firstMalePax = firstMalePax;
	}

	public BigDecimal getFirstPax() {
		return firstPax;
	}

	public void setFirstPax(BigDecimal firstPax) {
		this.firstPax = firstPax;
	}

	public Date getFltDate() {
		return fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getHopFrom() {
		return hopFrom;
	}

	public void setHopFrom(String hopFrom) {
		this.hopFrom = hopFrom;
	}

	public String getHopTo() {
		return hopTo;
	}

	public void setHopTo(String hopTo) {
		this.hopTo = hopTo;
	}

	public BigDecimal getIdArrFlight() {
		return idArrFlight;
	}

	public void setIdArrFlight(BigDecimal idArrFlight) {
		this.idArrFlight = idArrFlight;
	}

	public BigDecimal getIdDepFlight() {
		return idDepFlight;
	}

	public void setIdDepFlight(BigDecimal idDepFlight) {
		this.idDepFlight = idDepFlight;
	}

	public BigDecimal getInfantPax() {
		return infantPax;
	}

	public void setInfantPax(BigDecimal infantPax) {
		this.infantPax = infantPax;
	}

	public String getInfoType() {
		return infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public BigDecimal getInterfaceFltId() {
		return interfaceFltId;
	}

	public void setInterfaceFltId(BigDecimal interfaceFltId) {
		this.interfaceFltId = interfaceFltId;
	}

	public BigDecimal getJumpPax() {
		return jumpPax;
	}

	public void setJumpPax(BigDecimal jumpPax) {
		this.jumpPax = jumpPax;
	}

	public BigDecimal getMalePax() {
		return malePax;
	}

	public void setMalePax(BigDecimal malePax) {
		this.malePax = malePax;
	}

	public String getRecStatus() {
		return recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public BigDecimal getTechCrew() {
		return techCrew;
	}

	public void setTechCrew(BigDecimal techCrew) {
		this.techCrew = techCrew;
	}

	public BigDecimal getTotalCrew() {
		return totalCrew;
	}

	public void setTotalCrew(BigDecimal totalCrew) {
		this.totalCrew = totalCrew;
	}

	public BigDecimal getTotalPax() {
		return totalPax;
	}

	public void setTotalPax(BigDecimal totalPax) {
		this.totalPax = totalPax;
	}

	public BigDecimal getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public BigDecimal getIdConxFlight() {
		return idConxFlight;
	}

	public void setIdConxFlight(BigDecimal idConxFlight) {
		this.idConxFlight = idConxFlight;
	}

}
