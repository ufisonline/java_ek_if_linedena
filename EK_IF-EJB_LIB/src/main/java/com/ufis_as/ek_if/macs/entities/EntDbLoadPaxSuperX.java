package com.ufis_as.ek_if.macs.entities;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * 
 * @author SCH
 *
 */

@MappedSuperclass
public class EntDbLoadPaxSuperX {
	
	@Column(name = "id_HOPO", nullable = false)
	protected String _idHopo;
	
    @Column(name = "Created_Date", nullable = false, updatable=false)
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date _createdDate;
    
    @Column(name = "Updated_Date")
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date _updatedDate;
    
    @Column(name = "Updated_User")
    protected String _updateUser;
    
    @Column(name = "Created_User")
    protected String _createdUser;
    
    @Column(name = "Rec_Status")
    protected String _recStatus;
    
    @Id
	@Column(name = "ID", nullable = false, updatable=false)
	protected String uuid;
    
    
    @PrePersist
    void onPersistEntPaxSuper() {
       if (this.get_idHopo()== null) {
          this.set_idHopo("DXB");	// "DBS" should read from the configuration later
       }
       
       this.set_createdDate(HpUfisCalendar.getCurrentUTCTime());
       
       if (this.get_createdUser() == null){
    	   this.set_createdUser(HpEKConstants.MACS_PAX_DATA_SOURCE); // read from configuration later   	   
       }
       
       if (this.get_updateUser() == null){
    	   this.set_updateUser(""); 
       }
       
       if (this.get_recStatus() == null){
    	   this.set_recStatus(" ");
       }
       
       this.uuid = UUID.randomUUID().toString();

    }

    @PreUpdate
    void onUpdateEntPaxSuper(){
       if (this.get_idHopo()== null) {
          this.set_idHopo("DXB");	// "DBS" should read from the configuration later
       }
       
//       this.set_updatedDate(HpUfisCalendar.getCurrentUTCTime());
       
       if (this.get_createdUser() == null){
    	   this.set_createdUser(HpEKConstants.MACS_PAX_DATA_SOURCE); // read from configuration later
       }
       
       if (this.get_updateUser() == null){
    	   this.set_updateUser(HpEKConstants.MACS_PAX_DATA_SOURCE); 
       }
       
       if (this.get_recStatus() == null){
    	   this.set_recStatus(" ");
       }
    }
    

	public String get_idHopo() {
		return _idHopo;
	}

	public void set_idHopo(String _idHopo) {
		if (_idHopo != null){
			this._idHopo = _idHopo;
		}else{
			this._idHopo = "DXB";  // "DBS" should read from the configuration later
		}	
	}

	public Date get_createdDate() {
		return _createdDate;
	}

	public void set_createdDate(Date _createdDate) {
		if ( _createdDate != null){
		this._createdDate = _createdDate;
		}
	}

	public Date get_updatedDate() {
		return _updatedDate;
	}

	public void set_updatedDate(Date _updatedDate) {
		this._updatedDate = _updatedDate;
	}

	public String get_updateUser() {
		return _updateUser;
	}

	public void set_updateUser(String _updateUser) {
		if (_updateUser != null){
			this._updateUser = _updateUser;
		}else{
			this._updateUser = "";  
		}	
		
	}

	public String get_createdUser() {
		return _createdUser;
	}

	public void set_createdUser(String _createdUser) {
		if (_createdUser != null){
			this._createdUser = _createdUser;
		}else{
			this._createdUser = "";  
		}	

	}

	public String get_recStatus() {
		return _recStatus;
	}

	public void set_recStatus(String _recStatus) {
		if (_recStatus != null){
			this._recStatus = _recStatus;
		}else{
			this._recStatus = " ";  
		}	

	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

    

}
