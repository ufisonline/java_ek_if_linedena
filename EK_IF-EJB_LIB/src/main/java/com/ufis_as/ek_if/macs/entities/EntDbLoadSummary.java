//package com.ufis_as.ek_if.macs.entities;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.EmbeddedId;
//import javax.persistence.Entity;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//
///**
// * 
// * @author SCH
// *
// */
//@Entity
//@Table(name = "LOAD_SUMMARY")
//@NamedQueries({
//	@NamedQuery(name = "EntDbLoadSummary.findByPkey", query = "SELECT a FROM EntDbLoadSummary a WHERE a.lPk.intFlId = :intFlId AND a.lPk.infoId = :infoId AND a.lPk.airlineCode = :airlineCode AND a.lPk.flightNumber = :flightNumber AND a.lPk.flightNumberSuffix = :flightNumberSuffix AND a.lPk.scheduledFlightDateTime = :scheduledFlightDateTime"),
//	@NamedQuery(name = "EntDbLoadSummary.findByPk", query = "SELECT a FROM EntDbLoadSummary a WHERE a.lPk = :lPk"),
//})
//public class EntDbLoadSummary extends EntDbLoadPaxSuper implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	@EmbeddedId
//	private LoadSummaryPK lPk;
//
////	@Column(name="AIRLINECODE")
////	private String airlineCode;
//
//	@Column(name="BUSINESSCLASS")
//	private int businessClass;
//
//	@Column(name="ECONOMYCLASS")
//	private int economyClass;
//
//	@Column(name="ECONOMYCLASS2")
//	private int economyClass2;
//
//	@Column(name="FIRSTCLASS")
//	private int firstClass;
//
//	@Column(name="DEPFLID")
//	private String depFlId;
//
////	@Column(name="FLIGHTNUMBER")
////	private BigDecimal flightNumber;
////
////	@Column(name="FLIGHTNUMBERSUFFIX")
////	private String flightNumberSuffix;
//
////	@Column(name="INFOID")
////	private String infoId;
//
////	@Column(name="INTFLID")
////	private BigDecimal intFlId;
//
//	@Column(name="INTSYSTEM")
//	private String intSystem;
//
//	@Column(name="ARRFLID")
//	private String arrFlId;
//
////	@Temporal(TemporalType.DATE)
////	@Column(name="SCHEDULEDFLIGHTDATETIME")
////	private Date scheduledFlightDateTime;
//
//	@Column(name="TOTALVALUE")
//	private int totalValue;
//	
//	@Column(name="CONNTYPE")
//	private String connType;
//
//	public EntDbLoadSummary() {
//	}
//
////	public String getAirlineCode() {
////		return airlineCode;
////	}
////
////	public void setAirlineCode(String airlineCode) {
////		this.airlineCode = airlineCode;
////	}
//
//	public int getBusinessClass() {
//		return businessClass;
//	}
//
//	public void setBusinessClass(int businessClass) {
//		this.businessClass = businessClass;
//	}
//
//	public int getEconomyClass() {
//		return economyClass;
//	}
//
//	public void setEconomyClass(int economyClass) {
//		this.economyClass = economyClass;
//	}
//
//	public int getEconomyClass2() {
//		return economyClass2;
//	}
//
//	public void setEconomyClass2(int economyClass2) {
//		this.economyClass2 = economyClass2;
//	}
//
//	public int getFirstClass() {
//		return firstClass;
//	}
//
//	public void setFirstClass(int firstClass) {
//		this.firstClass = firstClass;
//	}
//
////	public String getFlightId() {
////		return flightId;
////	}
////
////	public void setFlightId(String flightId) {
////		this.flightId = flightId;
////	}
//
////	public BigDecimal getFlightNumber() {
////		return flightNumber;
////	}
////
////	public void setFlightNumber(BigDecimal flightNumber) {
////		this.flightNumber = flightNumber;
////	}
////
////	public String getFlightNumberSuffix() {
////		return flightNumberSuffix;
////	}
////
////	public void setFlightNumberSuffix(String flightNumberSuffix) {
////		this.flightNumberSuffix = flightNumberSuffix;
////	}
//
////	public String getInfoId() {
////		return infoId;
////	}
////
////	public void setInfoId(String infoId) {
////		this.infoId = infoId;
////	}
//
////	public BigDecimal getIntFlId() {
////		return intFlId;
////	}
////
////	public void setIntFlId(BigDecimal intFlId) {
////		this.intFlId = intFlId;
////	}
//
//	public String getIntSystem() {
//		return intSystem;
//	}
//
//	public void setIntSystem(String intSystem) {
//		this.intSystem = intSystem;
//	}
//
////	public String getLinkFlightId() {
////		return linkFlightId;
////	}
////
////	public void setLinkFlightId(String linkFlightId) {
////		this.linkFlightId = linkFlightId;
////	}
//
////	public Date getScheduledFlightDateTime() {
////		return scheduledFlightDateTime;
////	}
////
////	public void setScheduledFlightDateTime(Date scheduledFlightDateTime) {
////		this.scheduledFlightDateTime = scheduledFlightDateTime;
////	}
//
//	public int getTotalValue() {
//		return totalValue;
//	}
//
//	public void setTotalValue(int totalValue) {
//		this.totalValue = totalValue;
//	}
//
//	public LoadSummaryPK getlPk() {
//		return lPk;
//	}
//
//	public void setlPk(LoadSummaryPK lPk) {
//		this.lPk = lPk;
//	}
//
//	public String getDepFlId() {
//		return depFlId;
//	}
//
//	public void setDepFlId(String depFlId) {
//		this.depFlId = depFlId;
//	}
//
//
//
//	public String getArrFlId() {
//		return arrFlId;
//	}
//
//	public void setArrFlId(String arrFlId) {
//		this.arrFlId = arrFlId;
//	}
//
//	public String getConnType() {
//		return connType;
//	}
//
//	public void setConnType(String connType) {
//		this.connType = connType;
//	}
//
//
//	
//	
//
////	public String getFlightId() {
////		return flightId;
////	}
////
////	public void setFlightId(String flightId) {
////		this.flightId = flightId;
////	}
//
//}