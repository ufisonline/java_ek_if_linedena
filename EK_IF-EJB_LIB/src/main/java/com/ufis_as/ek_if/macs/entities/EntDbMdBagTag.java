package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the MD_BAG_TAG database table.
 * 
 */
@Entity
@Table(name = "MD_BAG_TAG")
public class EntDbMdBagTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "AIRLINE_CODE2")
	private String airlineCode2;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "CREATED_USER")
	private String createdUser;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Id
	private String id;

	@Column(name = "ID_HOPO")
	private String idHopo;

	@Column(name = "OPT_LOCK")
	private BigDecimal optLock;

	@Column(name = "PREFIX_CODE")
	private String prefixCode;

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

	@Column(name = "UPDATED_USER")
	private String updatedUser;

	public EntDbMdBagTag() {
	}

	public String getAirlineCode2() {
		return this.airlineCode2;
	}

	public void setAirlineCode2(String airlineCode2) {
		this.airlineCode2 = airlineCode2;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getPrefixCode() {
		return this.prefixCode;
	}

	public void setPrefixCode(String prefixCode) {
		this.prefixCode = prefixCode;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}