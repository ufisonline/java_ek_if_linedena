//package com.ufis_as.ek_if.macs.entities;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.EmbeddedId;
//import javax.persistence.Entity;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
///**
// * 
// * @author SCH
// * 
// */
//
//@Entity
//@Table(name = "LOAD_PAX")
//@NamedQueries({
//		@NamedQuery(name = "EntDbPax.findAll", query = "SELECT a FROM EntDbPax a"),
//		@NamedQuery(name = "EntDbPax.findByid", query = "SELECT a FROM EntDbPax a WHERE a.intId = :intId "),
//		@NamedQuery(name = "EntDbPax.findByFlid", query = "SELECT a FROM EntDbPax a WHERE a.pKId.intFlId = :intFlId "),
//		@NamedQuery(name = "EntDbPax.findForPaxLounge", query = "SELECT a FROM EntDbPax a WHERE trim(a.pKId.intRefNumber) = :paxRefNum")
//		})
//public class EntDbPax extends EntPaxSuper implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	// @Id
//	@EmbeddedId
//	private PaxPK pKId;
//
//	@Column(name = "BAGNOOFPIECES")
//	private BigDecimal bagNoOfPieces;
//
//	@Column(name = "BAGTAGPRINT")
//	private String bagTagPrint;
//
//	@Column(name = "BAGWEIGHT")
//	private BigDecimal bagWeight;
//
//	@Column(name = "BOARDINGPASSPRINT")
//	private String boardingPassprint;
//
//	@Column(name = "BOARDINGSTATUS")
//	private String boardingStatus;
//
//	@Column(name = "BOOKEDCLASS")
//	private String bookedClass;
//
//	@Column(name = "CABINCLASS")
//	private String cabinClass;
//
//	@Column(name = "CANCELLED")
//	private String cancelled;
//
//	@Column(name = "CHECKINDATETIME")
//	private Date checkInDateTime;
//
//	@Column(name = "DESTINATION")
//	private String destination;
//
//	@Temporal(TemporalType.DATE)
//	private Date dob;
//
//	@Column(name = "ETKTYPE")
//	private String etkType;
//
//	@Column(name = "GENDER")
//	private String gender;
//
//	@Column(name = "HANDICAPPED")
//	private String handicapped;
//
//	@Column(name = "INFANTINDICATOR")
//	private String infantIndicator;
//
//	@Column(name = "INTID")
//	private String intId;
//
//	@Column(name = "NATIONALITY")
//	private String nationality;
//
//	@Column(name = "OFFLOADEDPAX")
//	private String offLoadedPax;
//
//	@Column(name = "PAXBOOKINGSTATUS")
//	private String paxBookingStatus;
//
//	@Column(name = "PAXGROUPCODE")
//	private String paxGroupCode;
//
//	@Column(name = "PAXNAME")
//	private String paxName;
//
//	@Column(name = "PAXTYPE")
//	private String paxType;
//
//	@Column(name = "PRIORITYPAX")
//	private String priorityPax;
//
//	@Column(name = "STATUSONBOARD")
//	private String statusOnboard;
//
//	@Column(name = "TRAVELLEDCLASS")
//	private String travelledClass;
//
//	@Column(name = "UNACCOMPANIEDMINOR")
//	private String unAccompaniedMinor;
//
//	@Column(name = "BAGTAGINFO")
//	private String bagTagInfo;
//
//	@Column(name = "SCANLOCATION")
//	private String scanLocation;
//
//	@Column(name = "SCANDATETIME")
//	private Date scanDateTime;
//
//	@Column(name = "CHECKINAGENTCODE")
//	private String checkInAgentCode;
//
//	@Column(name = "CHECKINHANDLINGAGENT")
//	private String checkInHandlingAgent;
//
//	@Column(name = "CHECKINSEQUENCE")
//	private String checkInSequence;
//
//	@Column(name = "CHECKINCITY")
//	private String checkInCity;
//
//	@Column(name = "BOARDINGDATETIME")
//	private Date boardingDateTime;
//
//	@Column(name = "BOARDINGAGENTCODE")
//	private String boardingAgentCode;
//
//	@Column(name = "BOARDINGHANDLINGAGENT")
//	private String boardingHandlingAgent;
//
//	// bi-directional many-to-one association to PaxConn
//	@OneToMany(mappedBy = "pax")
//	private List<EntDbPaxConn> entDbPaxConns;
//
//	public List<EntDbPaxConn> getEntDbPaxConns() {
//		return entDbPaxConns;
//	}
//
//	public void setEntDbPaxConns(List<EntDbPaxConn> entDbPaxConns) {
//		this.entDbPaxConns = entDbPaxConns;
//	}
//
//	public EntDbPax() {
//	}
//
//	public BigDecimal getBagNoOfPieces() {
//		return bagNoOfPieces;
//	}
//
//	public void setBagNoOfPieces(BigDecimal bagNoOfPieces) {
//		this.bagNoOfPieces = bagNoOfPieces;
//	}
//
//	public String getBagTagPrint() {
//		return bagTagPrint;
//	}
//
//	public void setBagTagPrint(String bagTagPrint) {
//		this.bagTagPrint = bagTagPrint;
//	}
//
//	public BigDecimal getBagWeight() {
//		return bagWeight;
//	}
//
//	public void setBagWeight(BigDecimal bagWeight) {
//		this.bagWeight = bagWeight;
//	}
//
//	public String getBoardingPassprint() {
//		return boardingPassprint;
//	}
//
//	public void setBoardingPassprint(String boardingPassprint) {
//		this.boardingPassprint = boardingPassprint;
//	}
//
//	public String getBoardingStatus() {
//		return boardingStatus;
//	}
//
//	public void setBoardingStatus(String boardingStatus) {
//		this.boardingStatus = boardingStatus;
//	}
//
//	public String getBookedClass() {
//		return bookedClass;
//	}
//
//	public void setBookedClass(String bookedClass) {
//		this.bookedClass = bookedClass;
//	}
//
//	public String getCabinClass() {
//		return cabinClass;
//	}
//
//	public void setCabinClass(String cabinClass) {
//		this.cabinClass = cabinClass;
//	}
//
//	public String getCancelled() {
//		return cancelled;
//	}
//
//	public void setCancelled(String cancelled) {
//		this.cancelled = cancelled;
//	}
//
//	public Date getCheckInDateTime() {
//		return checkInDateTime;
//	}
//
//	public void setCheckInDateTime(Date checkInDateTime) {
//		this.checkInDateTime = checkInDateTime;
//	}
//
//	public String getDestination() {
//		return destination;
//	}
//
//	public void setDestination(String destination) {
//		this.destination = destination;
//	}
//
//	public Date getDob() {
//		return dob;
//	}
//
//	public void setDob(Date dob) {
//		this.dob = dob;
//	}
//
//	public String getEtkType() {
//		return etkType;
//	}
//
//	public void setEtkType(String etkType) {
//		this.etkType = etkType;
//	}
//
//	public String getGender() {
//		return gender;
//	}
//
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//
//	public String getHandicapped() {
//		return handicapped;
//	}
//
//	public void setHandicapped(String handicapped) {
//		this.handicapped = handicapped;
//	}
//
//	public String getInfantIndicator() {
//		return infantIndicator;
//	}
//
//	public void setInfantIndicator(String infantIndicator) {
//		this.infantIndicator = infantIndicator;
//	}
//
//	public PaxPK getpKId() {
//		return this.pKId;
//	}
//
//	public void setpKId(PaxPK pKId) {
//		this.pKId = pKId;
//	}
//
//	public String getNationality() {
//		return nationality;
//	}
//
//	public void setNationality(String nationality) {
//		this.nationality = nationality;
//	}
//
//	public String getOffLoadedPax() {
//		return offLoadedPax;
//	}
//
//	public void setOffLoadedPax(String offLoadedPax) {
//		this.offLoadedPax = offLoadedPax;
//	}
//
//	public String getPaxBookingStatus() {
//		return paxBookingStatus;
//	}
//
//	public void setPaxBookingStatus(String paxBookingStatus) {
//		this.paxBookingStatus = paxBookingStatus;
//	}
//
//	public String getPaxGroupCode() {
//		return paxGroupCode;
//	}
//
//	public void setPaxGroupCode(String paxGroupCode) {
//		this.paxGroupCode = paxGroupCode;
//	}
//
//	public String getPaxName() {
//		return paxName;
//	}
//
//	public void setPaxName(String paxName) {
//		this.paxName = paxName;
//	}
//
//	public String getPaxType() {
//		return paxType;
//	}
//
//	public void setPaxType(String paxType) {
//		this.paxType = paxType;
//	}
//
//	public String getPriorityPax() {
//		return priorityPax;
//	}
//
//	public void setPriorityPax(String priorityPax) {
//		this.priorityPax = priorityPax;
//	}
//
//	public String getStatusOnboard() {
//		return statusOnboard;
//	}
//
//	public void setStatusOnboard(String statusOnboard) {
//		this.statusOnboard = statusOnboard;
//	}
//
//	public String getTravelledClass() {
//		return travelledClass;
//	}
//
//	public void setTravelledClass(String travelledClass) {
//		this.travelledClass = travelledClass;
//	}
//
//	public String getUnAccompaniedMinor() {
//		return unAccompaniedMinor;
//	}
//
//	public void setUnAccompaniedMinor(String unAccompaniedMinor) {
//		this.unAccompaniedMinor = unAccompaniedMinor;
//	}
//
//	public String getBagTagInfo() {
//		return bagTagInfo;
//	}
//
//	public void setBagTagInfo(String bagTagInfo) {
//		this.bagTagInfo = bagTagInfo;
//	}
//
//	public String getScanLocation() {
//		return scanLocation;
//	}
//
//	public void setScanLocation(String scanLocation) {
//		this.scanLocation = scanLocation;
//	}
//
//	public Date getScanDateTime() {
//		return scanDateTime;
//	}
//
//	public void setScanDateTime(Date scanDateTime) {
//		this.scanDateTime = scanDateTime;
//	}
//
//	public String getIntId() {
//		return intId;
//	}
//
//	public void setIntId(String intId) {
//		this.intId = intId;
//	}
//
//	public String getCheckInHandlingAgent() {
//		return checkInHandlingAgent;
//	}
//
//	public void setCheckInHandlingAgent(String checkInHandlingAgent) {
//		this.checkInHandlingAgent = checkInHandlingAgent;
//	}
//
//	public String getCheckInSequence() {
//		return checkInSequence;
//	}
//
//	public void setCheckInSequence(String checkInSequence) {
//		this.checkInSequence = checkInSequence;
//	}
//
//	public String getCheckInCity() {
//		return checkInCity;
//	}
//
//	public void setCheckInCity(String checkInCity) {
//		this.checkInCity = checkInCity;
//	}
//
//	public Date getBoardingDateTime() {
//		return boardingDateTime;
//	}
//
//	public void setBoardingDateTime(Date boardingDateTime) {
//		this.boardingDateTime = boardingDateTime;
//	}
//
//	public String getBoardingAgentCode() {
//		return boardingAgentCode;
//	}
//
//	public void setBoardingAgentCode(String boardingAgentCode) {
//		this.boardingAgentCode = boardingAgentCode;
//	}
//
//	public String getBoardingHandlingAgent() {
//		return boardingHandlingAgent;
//	}
//
//	public void setBoardingHandlingAgent(String boardingHandlingAgent) {
//		this.boardingHandlingAgent = boardingHandlingAgent;
//	}
//
//	public String getCheckInAgentCode() {
//		return checkInAgentCode;
//	}
//
//	public void setCheckInAgentCode(String checkInAgentCode) {
//		this.checkInAgentCode = checkInAgentCode;
//	}
//
//}