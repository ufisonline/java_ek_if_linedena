//package com.ufis_as.ek_if.macs.entities;
//
//import java.io.Serializable;
//import javax.persistence.*;
//
//import java.util.Date;
//import java.util.Objects;
//
///**
// * 
// * @author SCH
// * 
// */
//
//@Entity
//@Table(name = "LOAD_PAX_CONNECT")
//@NamedQueries({
//		@NamedQuery(name = "PaxConn.findAll", query = "SELECT a FROM EntDbPaxConn a"),
//		@NamedQuery(name = "PaxConn.findByid", query = "SELECT a FROM EntDbPaxConn a WHERE a.intId = :intId and a.intSystem = :intSystem"),
//		@NamedQuery(name = "PaxConn.findByFlid", query = "SELECT a FROM EntDbPaxConn a WHERE a.intFlId = :intFlId and a.intSystem = :intSystem "),
//		@NamedQuery(name = "PaxConn.findByIdAndType", query = "SELECT a FROM EntDbPaxConn a WHERE a.intId = :intId and a.connType = :connType "),
//		@NamedQuery(name = "PaxConn.findByIntflidAndOtherInfo", query = "SELECT a FROM EntDbPaxConn a WHERE a.intFlId = :intFlId and a.airlineCode = :airlineCode and flightNumber = :flightNumber and a.flightNumberSuffice = :flightNumberSuffice and a.scheduledFlightDateTime = :scheduledFlightDateTime"),
//		@NamedQuery(name = "PaxConn.findByIntFlid", query = "SELECT a FROM EntDbPaxConn a WHERE a.intFlId = :intFlId") })
//public class EntDbPaxConn extends EntPaxSuper implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	@Column(name = "AIRLINECODE")
//	private String airlineCode;
//
//	@Column(name = "BOARDPOINT")
//	private String boardPoint;
//
//	@Column(name = "BOOKEDCLASS")
//	private String bookedClass;
//
//	@Column(name = "CONNSTATUS")
//	private String connStatus;
//
//	@Column(name = "CONNTYPE")
//	private String connType;
//
//	@Column(name = "FLIGHTNUMBER")
//	private String flightNumber;
//
//	@Column(name = "FLIGHTNUMBERSUFFICE")
//	private String flightNumberSuffice;
//
//	@Column(name = "INTFLID")
//	private String intFlId;
//
//	@Id
//	@Column(name = "INTID")
//	private String intId;
//
//	@Column(name = "INTREFNUMBER")
//	private String intRefNumber;
//
//	@Column(name = "DATA_SOURCE")
//	private String intSystem;
//
//	@Column(name = "OFFPOINT")
//	private String offPoint;
//
//	@Column(name = "SCHEDULEDFLIGHTDATETIME")
//	@Temporal(value = TemporalType.TIMESTAMP)
//	private Date scheduledFlightDateTime;
//
//	@Transient
//	private String downloadDate;
//
//	@Transient
//	private String msg_timeStamp;
//
//	@Transient
//	private String variableData;
//
//	// bi-directional many-to-one association to Pax
//	@ManyToOne
//	@JoinColumns({
//			@JoinColumn(name = "INTFLID", referencedColumnName = "INTFLID", insertable = false, updatable = false),
//			@JoinColumn(name = "INTREFNUMBER", referencedColumnName = "INTREFNUMBER", insertable = false, updatable = false),
//			@JoinColumn(name = "DATA_SOURCE", referencedColumnName = "DATA_SOURCE", insertable = false, updatable = false) })
//	private EntDbPax pax;
//
//	public String getVariableData() {
//		return variableData;
//	}
//
//	public void setVariableData(String variableData) {
//		this.variableData = variableData;
//	}
//
//	public String getMsg_timeStamp() {
//		return msg_timeStamp;
//	}
//
//	public void setMsg_timeStamp(String msg_timeStamp) {
//		this.msg_timeStamp = msg_timeStamp;
//	}
//
//	public String getDownloadDate() {
//		return downloadDate;
//	}
//
//	public void setDownloadDate(String downloadDate) {
//		this.downloadDate = downloadDate;
//	}
//
//	public EntDbPaxConn() {
//	}
//
//	public String getAirlineCode() {
//		return airlineCode;
//	}
//
//	public void setAirlineCode(String airlineCode) {
//		this.airlineCode = airlineCode;
//	}
//
//	public String getBoardPoint() {
//		return boardPoint;
//	}
//
//	public void setBoardPoint(String boardPoint) {
//		this.boardPoint = boardPoint;
//	}
//
//	public String getBookedClass() {
//		return bookedClass;
//	}
//
//	public void setBookedClass(String bookedClass) {
//		this.bookedClass = bookedClass;
//	}
//
//	public String getConnStatus() {
//		return connStatus;
//	}
//
//	public void setConnStatus(String connStatus) {
//		this.connStatus = connStatus;
//	}
//
//	public String getConnType() {
//		return connType;
//	}
//
//	public void setConnType(String connType) {
//		this.connType = connType;
//	}
//
//	public String getFlightNumber() {
//		return flightNumber;
//	}
//
//	public void setFlightNumber(String flightNumber) {
//		this.flightNumber = flightNumber;
//	}
//
//	public String getFlightNumberSuffice() {
//		return flightNumberSuffice;
//	}
//
//	public void setFlightNumberSuffice(String flightNumberSuffice) {
//		this.flightNumberSuffice = flightNumberSuffice;
//	}
//
//	public String getIntFlId() {
//		return intFlId;
//	}
//
//	public void setIntFlId(String intFlId) {
//		this.intFlId = intFlId;
//	}
//
//	public String getIntId() {
//		return intId;
//	}
//
//	public void setIntId(String intId) {
//		this.intId = intId;
//	}
//
//	public String getIntRefNumber() {
//		return intRefNumber;
//	}
//
//	public void setIntRefNumber(String intRefNumber) {
//		this.intRefNumber = intRefNumber;
//	}
//
//	public String getIntSystem() {
//		return intSystem;
//	}
//
//	public void setIntSystem(String intSystem) {
//		this.intSystem = intSystem;
//	}
//
//	public String getOffPoint() {
//		return offPoint;
//	}
//
//	public void setOffPoint(String offPoint) {
//		this.offPoint = offPoint;
//	}
//
//	public Date getScheduledFlightDateTime() {
//		return scheduledFlightDateTime;
//	}
//
//	public void setScheduledFlightDateTime(Date scheduledFlightDateTime) {
//		this.scheduledFlightDateTime = scheduledFlightDateTime;
//	}
//
//	public EntDbPax getPax() {
//		return pax;
//	}
//
//	public void setPax(EntDbPax pax) {
//		this.pax = pax;
//	}
//
//	@Override
//	public int hashCode() {
//		int hash = 7;
//		hash = 29 * hash + Objects.hashCode(this.intId);
//		hash = 29 * hash + Objects.hashCode(this.intFlId);
//		return hash;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (obj == null) {
//			return false;
//		}
//		if (getClass() != obj.getClass()) {
//			return false;
//		}
//		final EntDbPaxServiceRequest other = (EntDbPaxServiceRequest) obj;
//		if (!Objects.equals(this.intId, other.intId)) {
//			return false;
//		}
//		return true;
//	}
//
//}