///*
// * $Id$
// *
// * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
// * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
// */
//package com.ufis_as.ek_if.macs.entities;
//
//import java.io.Serializable;
//import java.util.Objects;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import org.slf4j.LoggerFactory;
//
///**
// * @author $Author$
// * @version $Revision$
// */
//@Entity
//@Table(name="SERVICE_REQUEST")
//public class EntDbPaxServiceRequest extends EntPaxSuper implements Serializable{
//
//    /**
//     * Logger
//     */
//    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EntDbPaxServiceRequest.class);
//    @Column(name = "DATA_SOURCE")
//    protected String intSystem = "";
//	@Id
//    @Column(name = "INTID")
//    protected String intId = "";
//    @Column(name = "INTFLID")
//    protected String intFlId = "";
//    @Column(name = "INTREFNUMBER")
//    protected String intRefNumber = "";
//    @Column(name = "REQUESTTYPE")
//    protected String requestType="";
//    @Column(name = "SERVICECODE")
//    protected String serviceCode="";
//    @Column(name = "SERVICETYPE")
//    protected String serviceType="";
//    @Column(name = "EXTINFO")
//    protected String extInfo="";
//
//    public EntDbPaxServiceRequest(){}
//
//    public String getIntSystem() {
//        return intSystem;
//    }
//
//    public void setIntSystem(String intSystem) {
//        this.intSystem = intSystem;
//    }
//
//    public String getIntId() {
//        return intId;
//    }
//
//    public void setIntId(String intId) {
//        this.intId = intId;
//    }
//
//    public String getIntFlId() {
//        return intFlId;
//    }
//
//    public void setIntFlId(String intFlId) {
//        this.intFlId = intFlId;
//    }
//
//    public String getIntRefNumber() {
//        return intRefNumber;
//    }
//
//    public void setIntRefNumber(String intRefNumber) {
//        this.intRefNumber = intRefNumber;
//    }
//
//    public String getRequestType() {
//        return requestType;
//    }
//
//    public void setRequestType(String requestType) {
//        this.requestType = requestType;
//    }
//
//    public String getServiceCode() {
//        return serviceCode;
//    }
//
//    public void setServiceCode(String serviceCode) {
//        this.serviceCode = serviceCode;
//    }
//
//    public String getServiceType() {
//        return serviceType;
//    }
//
//    public void setServiceType(String serviceType) {
//        this.serviceType = serviceType;
//    }
//
//    public String getExtInfo() {
//        return extInfo;
//    }
//
//    public void setExtInfo(String extInfo) {
//        this.extInfo = extInfo;
//    }
//    
//	@Override
//	public int hashCode() {
//		int hash = 7;
//		hash = 29 * hash + Objects.hashCode(this.intId);
//		hash = 29 * hash + Objects.hashCode(this.intFlId);
//		return hash;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (obj == null) {
//			return false;
//		}
//		if (getClass() != obj.getClass()) {
//			return false;
//		}
//		final EntDbPaxServiceRequest other = (EntDbPaxServiceRequest) obj;
//		if (!Objects.equals(this.intId, other.intId)) {
//			return false;
//		}
//		return true;
//	}
//
//
//
//}
