/*
 * $Id: EntDbServiceRequest.java 9066 2013-09-24 09:03:18Z sch $
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author $Author: sch $
 * @version $Revision: 9066 $
 */
@Entity
@Table(name="SERVICE_REQUEST")
public class EntDbServiceRequestX extends EntDbLoadPaxSuperX implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

//	@EmbeddedId
//	public ServiceRequestPK serviceRequestPK;
    @Column(name = "INTFLID")
    protected String intFlId = "";

    @Column(name = "INTREFNUMBER")
    protected String intRefNumber = "";
    
	@Column(name = "ID_FLIGHT")
	private int idFlight;
    @Column(name = "DATA_SOURCE")
    protected String intSystem = "";
    @Column(name = "INTID")
    protected String intId = "";
    @Column(name = "REQUESTTYPE")
    protected String requestType="";
    @Column(name = "SERVICECODE")
    protected String serviceCode="";
    @Column(name = "SERVICETYPE")
    protected String serviceType="";
    @Column(name = "EXTINFO")
    protected String extInfo="";
    @Column(name = "ID_LOAD_PAX")
    protected String idLoadPax="";

    public EntDbServiceRequestX(){}

    public String getIntSystem() {
        return intSystem;
    }

    public void setIntSystem(String intSystem) {
        this.intSystem = intSystem;
    }

    public String getIntId() {
        return intId;
    }

    public void setIntId(String intId) {
        this.intId = intId;
    }


    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }
    
	public int getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}
	
	

	public String getIdLoadPax() {
		return idLoadPax;
	}

	public void setIdLoadPax(String idLoadPax) {
		this.idLoadPax = idLoadPax;
	}

	public String getIntFlId() {
		return intFlId;
	}

	public void setIntFlId(String intFlId) {
		this.intFlId = intFlId;
	}

	public String getIntRefNumber() {
		return intRefNumber;
	}

	public void setIntRefNumber(String intRefNumber) {
		this.intRefNumber = intRefNumber;
	}

//	public ServiceRequestPK getServiceRequestPK() {
//		return serviceRequestPK;
//	}
//
//	public void setServiceRequestPK(ServiceRequestPK serviceRequestPK) {
//		this.serviceRequestPK = serviceRequestPK;
//	}
	
	
	



}
