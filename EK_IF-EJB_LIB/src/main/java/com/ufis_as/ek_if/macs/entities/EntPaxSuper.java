package com.ufis_as.ek_if.macs.entities;

import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 * 
 * @author SCH
 *
 */

@MappedSuperclass
public class EntPaxSuper {
	private static final Logger LOG = LoggerFactory.getLogger(EntPaxSuper.class);
	
	@Column(name = "id_HOPO", nullable = false)
	private String _idHopo;
	
    @Column(name = "Created_Date", nullable = false, updatable=false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date _createdDate;
    
    @Column(name = "Updated_Date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date _updatedDate;
    
    @Column(name = "Updated_User")
    private String _updateUser;
    
    @Column(name = "Created_User")
    private String _createdUser;
    
    @Column(name = "Rec_Status")
    private String _recStatus;
    
    
    @PrePersist
    void onPersistEntPaxSuper() {
       if (this.get_idHopo()== null) {
          this.set_idHopo("DXB");	// "DBS" should read from the configuration later
       }
       
//       try {
		this.set_createdDate(HpUfisCalendar.getCurrentUTCTime());
//	} catch (ParseException e) {
//		LOG.error("ERROR when getting current UTC time : {}", e.toString());
//	}
       
       if (this.get_createdUser() == null){
    	   this.set_createdUser("MACS_PAX"); // read from configuration later
       }
       
       if (this.get_updateUser() == null){
    	   this.set_updateUser(""); 
       }
       
       if (this.get_recStatus() == null){
    	   this.set_recStatus(" ");
       }

    }

    @PreUpdate
    void onUpdateEntPaxSuper(){
       if (this.get_idHopo()== null) {
          this.set_idHopo("DXB");	// "DBS" should read from the configuration later
       }
       
//       try {
		this.set_updatedDate(HpUfisCalendar.getCurrentUTCTime());
//	} catch (ParseException e) {
//		LOG.error("ERROR when getting current UTC time : {}", e.toString());
//	}
       
       if (this.get_createdUser() == null){
    	   this.set_createdUser("MACS_PAX"); // read from configuration later
       }
       
       if (this.get_updateUser() == null){
    	   this.set_updateUser("MACS_PAX"); 
       }
       
       if (this.get_recStatus() == null){
    	   this.set_recStatus(" ");
       }
    }
    

	public String get_idHopo() {
		return _idHopo;
	}

	public void set_idHopo(String _idHopo) {
		if (_idHopo != null){
			this._idHopo = _idHopo;
		}else{
			this._idHopo = "DXB";  // "DBS" should read from the configuration later
		}	
	}

	public Date get_createdDate() {
		return _createdDate;
	}

	public void set_createdDate(Date _createdDate) {
		if ( _createdDate != null){
		this._createdDate = _createdDate;
		}
	}

	public Date get_updatedDate() {
		return _updatedDate;
	}

	public void set_updatedDate(Date _updatedDate) {
		this._updatedDate = _updatedDate;
	}

	public String get_updateUser() {
		return _updateUser;
	}

	public void set_updateUser(String _updateUser) {
		if (_updateUser != null){
			this._updateUser = _updateUser;
		}else{
			this._updateUser = "";  
		}	
		
	}

	public String get_createdUser() {
		return _createdUser;
	}

	public void set_createdUser(String _createdUser) {
		if (_createdUser != null){
			this._createdUser = _createdUser;
		}else{
			this._createdUser = "";  
		}	

	}

	public String get_recStatus() {
		return _recStatus;
	}

	public void set_recStatus(String _recStatus) {
		if (_recStatus != null){
			this._recStatus = _recStatus;
		}else{
			this._recStatus = " ";  
		}	

	}

    

}
