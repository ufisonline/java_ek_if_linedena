/*
 * $Id$
 *
 * Copyright 2012 UFIS Airport Solutions, Inc. All rights reserved.
 * UFIS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ufis_as.ek_if.macs.entities;

/**
 * @author $Author$
 * @version $Revision$
 */
public enum EnumPaxConnType {
//    INBDETAILS, //INBDETAILS
//    OWNDETAILS  //OWNDETAILS
	I,
	O
}
