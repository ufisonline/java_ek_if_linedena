package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the PAX database table.
 * 
 */

@Embeddable
public class LoadPaxConnPK  implements Serializable{
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	// added by 2013.09.30
	@Column(name = "INTERFACE_FLTID")
	private String interfaceFltid;

	@Column(name = "INTREFNUMBER")
	private String intRefNumber;

	public String getInterfaceFltid() {
		return interfaceFltid;
	}

	public void setInterfaceFltid(String interfaceFltid) {
		this.interfaceFltid = interfaceFltid;
	}

	public String getIntRefNumber() {
		return intRefNumber;
	}

	public void setIntRefNumber(String intRefNumber) {
		this.intRefNumber = intRefNumber;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LoadPaxConnPK)) {
			return false;
		}
		LoadPaxConnPK castOther = (LoadPaxConnPK)other;
		return 
			this.interfaceFltid.equals(castOther.interfaceFltid)
			&& this.intRefNumber.equals(castOther.intRefNumber);
////			&& this.intId.equals(castOther.intId);
//			&& this.intSystem.equals(castOther.intSystem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.interfaceFltid.hashCode();
		hash = hash * prime + this.intRefNumber.hashCode();
//		hash = hash * prime + this.intId.hashCode();
//		hash = hash * prime + this.intSystem.hashCode();
		
		return hash;
	}
}