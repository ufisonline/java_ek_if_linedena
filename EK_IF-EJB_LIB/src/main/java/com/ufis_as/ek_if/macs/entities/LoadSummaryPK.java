//package com.ufis_as.ek_if.macs.entities;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
///**
// * 
// * @author SCH
// *
// */
//
//@Embeddable
//public class LoadSummaryPK implements Serializable {
//		//default serial version id, required for serializable classes.
//		private static final long serialVersionUID = 1L;
//		
//		@Column(name="INTFLID")
//		private BigDecimal intFlId;
//		
//		@Column(name="INFOID")
//		private String infoId;
//		
//		@Column(name="AIRLINECODE")
//		private String airlineCode;
//		
//		@Column(name="FLIGHTNUMBER")
//		private String flightNumber;
//
//		@Column(name="FLIGHTNUMBERSUFFIX")
//		private String flightNumberSuffix;
//		
//		@Temporal(TemporalType.DATE)
//		@Column(name="SCHEDULEDFLIGHTDATETIME")
//		private Date scheduledFlightDateTime;
//
//
//		public BigDecimal getIntFlId() {
//			return intFlId;
//		}
//
//		public void setIntFlId(BigDecimal intFlId) {
//			this.intFlId = intFlId;
//		}
//
//		public String getInfoId() {
//			return infoId;
//		}
//
//		public void setInfoId(String infoId) {
//			this.infoId = infoId;
//		}
//		
//		public String getAirlineCode() {
//			return airlineCode;
//		}
//
//		public void setAirlineCode(String airlineCode) {
//			this.airlineCode = airlineCode;
//		}
//
//		public String getFlightNumber() {
//			return flightNumber;
//		}
//
//		public void setFlightNumber(String flightNumber) {
//			this.flightNumber = flightNumber;
//		}
//
//		public String getFlightNumberSuffix() {
//			return flightNumberSuffix;
//		}
//
//		public void setFlightNumberSuffix(String flightNumberSuffix) {
//			this.flightNumberSuffix = flightNumberSuffix;
//		}
//
//		public Date getScheduledFlightDateTime() {
//			return scheduledFlightDateTime;
//		}
//
//		public void setScheduledFlightDateTime(Date scheduledFlightDateTime) {
//			this.scheduledFlightDateTime = scheduledFlightDateTime;
//		}
//
//		@Override
//		public int hashCode() {
//			final int prime = 31;
//			int result = 1;
//			result = prime * result
//					+ ((airlineCode == null) ? 0 : airlineCode.hashCode());
//			result = prime * result
//					+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
//			result = prime
//					* result
//					+ ((flightNumberSuffix == null) ? 0 : flightNumberSuffix
//							.hashCode());
//			result = prime * result
//					+ ((infoId == null) ? 0 : infoId.hashCode());
//			result = prime * result
//					+ ((intFlId == null) ? 0 : intFlId.hashCode());
//			result = prime
//					* result
//					+ ((scheduledFlightDateTime == null) ? 0
//							: scheduledFlightDateTime.hashCode());
//			return result;
//		}
//
//		@Override
//		public boolean equals(Object obj) {
//			if (this == obj)
//				return true;
//			if (obj == null)
//				return false;
//			if (getClass() != obj.getClass())
//				return false;
//			LoadSummaryPK other = (LoadSummaryPK) obj;
//			if (airlineCode == null) {
//				if (other.airlineCode != null)
//					return false;
//			} else if (!airlineCode.equals(other.airlineCode))
//				return false;
//			if (flightNumber == null) {
//				if (other.flightNumber != null)
//					return false;
//			} else if (!flightNumber.equals(other.flightNumber))
//				return false;
//			if (flightNumberSuffix == null) {
//				if (other.flightNumberSuffix != null)
//					return false;
//			} else if (!flightNumberSuffix.equals(other.flightNumberSuffix))
//				return false;
//			if (infoId == null) {
//				if (other.infoId != null)
//					return false;
//			} else if (!infoId.equals(other.infoId))
//				return false;
//			if (intFlId == null) {
//				if (other.intFlId != null)
//					return false;
//			} else if (!intFlId.equals(other.intFlId))
//				return false;
//			if (scheduledFlightDateTime == null) {
//				if (other.scheduledFlightDateTime != null)
//					return false;
//			} else if (!scheduledFlightDateTime
//					.equals(other.scheduledFlightDateTime))
//				return false;
//			return true;
//		}
//
////		@Override
////		public String toString() {
////		return "IntFlid: " + intFlId.toString() 
////				+ " InfoId: " + infoId
////				+ " AirlineCode: " + airlineCode 
////				+ " flightNumber: "	+ flightNumber 
////				+ " flightSuffix: " + flightNumberSuffix
////				+ " Schedule time: " + scheduledFlightDateTime.toString();
////		}
//		
//		
//
//}
