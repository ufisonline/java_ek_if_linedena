package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PAX database table.
 * 
 */

@Embeddable
public class PaxPK  implements Serializable{
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "INTFLID")
	private String intFlId;

	@Column(name = "INTREFNUMBER")
	private String intRefNumber;
	
	@Column(name = "DATA_SOURCE")
	private String intSystem;
	
//	@Column(name = "INTID")
//	private String intId;

	public PaxPK() {
	}
	public String getIntflid() {
		return this.intFlId;
	}
	public void setIntflid(String intflid) {
		this.intFlId = intflid;
	}
	public String getIntrefnumber() {
		return this.intRefNumber;
	}
	public void setIntrefnumber(String intrefnumber) {
		this.intRefNumber = intrefnumber;
	}
	public String getIntSystem() {
		return intSystem;
	}
	public void setIntSystem(String intSystem) {
		this.intSystem = intSystem;
	}
//	public String getIntId() {
//		return intId;
//	}
//	public void setIntId(String intId) {
//		this.intId = intId;
//	}
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaxPK)) {
			return false;
		}
		PaxPK castOther = (PaxPK)other;
		return 
			this.intFlId.equals(castOther.intFlId)
			&& this.intRefNumber.equals(castOther.intRefNumber)
//			&& this.intId.equals(castOther.intId);
			&& this.intSystem.equals(castOther.intSystem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.intFlId.hashCode();
		hash = hash * prime + this.intRefNumber.hashCode();
//		hash = hash * prime + this.intId.hashCode();
		hash = hash * prime + this.intSystem.hashCode();
		
		return hash;
	}
}