package com.ufis_as.ek_if.macs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the PAX database table.
 * 
 */

@Embeddable
public class ServiceRequestPK  implements Serializable{
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

    @Column(name = "INTFLID")
    protected String intFlId = "";

    @Column(name = "INTREFNUMBER")
    protected String intRefNumber = "";



	public String getIntFlId() {
		return intFlId;
	}

	public void setIntFlId(String intFlId) {
		this.intFlId = intFlId;
	}

	public String getIntRefNumber() {
		return intRefNumber;
	}

	public void setIntRefNumber(String intRefNumber) {
		this.intRefNumber = intRefNumber;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ServiceRequestPK)) {
			return false;
		}
		ServiceRequestPK castOther = (ServiceRequestPK)other;
		return 
			this.intFlId.equals(castOther.intFlId)
			&& this.intRefNumber.equals(castOther.intRefNumber);
////			&& this.intId.equals(castOther.intId);
//			&& this.intSystem.equals(castOther.intSystem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.intFlId.hashCode();
		hash = hash * prime + this.intRefNumber.hashCode();
//		hash = hash * prime + this.intId.hashCode();
//		hash = hash * prime + this.intSystem.hashCode();
		
		return hash;
	}
}