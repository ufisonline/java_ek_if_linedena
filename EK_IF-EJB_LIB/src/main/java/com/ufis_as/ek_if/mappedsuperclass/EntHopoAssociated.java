/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ufis_as.ek_if.mappedsuperclass;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.ufis_as.configuration.HpEKConstants;

/**
 * @author btr
 */

@MappedSuperclass
public class EntHopoAssociated  extends EntMaintenance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "id_hopo", nullable = false)
    private String _idHopo = null;

    @PrePersist
    void onPersistHopo() {
       if (this.getHopo()== null) {
          this.setHopo(HpEKConstants.EK_HOPO);
       }
    }

    @PreUpdate
    void onUpdateHopo() {
       if (this.getHopo()== null) {
          this.setHopo(HpEKConstants.EK_HOPO);
       }
    }

    public String getHopo() {
        return _idHopo;
    }

    public void setHopo(String hopo) {
        if (hopo != null) {
            this._idHopo = hopo;
        } else {
            this._idHopo = HpEKConstants.EK_HOPO;
        }
    }

}
