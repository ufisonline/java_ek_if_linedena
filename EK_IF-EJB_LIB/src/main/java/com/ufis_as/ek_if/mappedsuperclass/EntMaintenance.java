package com.ufis_as.ek_if.mappedsuperclass;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.ufis_as.ufisapp.configuration.HpUfisAppConstants;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;


/**
 *
 * @author usc
 */

@MappedSuperclass
public abstract class EntMaintenance extends EntUfisID implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "created_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date _createdDate;
    @Column(name = "updated_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date _updatedDate;

    @Version
    @Column(name = "opt_lock")
    protected long version;

    @Column(name = "created_user")
    protected String _createdUser;

    @Column(name = "updated_user")
    protected String _updatedUser;

    /**
     * this flag can be used to mark an entity that is inside a collection as an "orphan". The eao can remove
     * such entities from the database and the collection before merging the parent object.
     */
    @Transient
    private boolean isOrphan = false;

    
    /* Keep the validation Errors */
    @Transient
    private HashMap<String,String> validationErrors = new HashMap<String,String>();

    @PrePersist
    void onPersistMaintenance() {
    	if (this.getCreatedDate() == null) {
    		this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
    	}
    	if (this.getCreatedUser() == null) {
            this.setCreatedUser(HpUfisAppConstants.SYSTEM);
    	}
    }
    
    @PreUpdate
    void onUpdateMaintenance() {
    	//this.setUpdatedDate(new Date());
    	if (this.getUpdatedDate() == null) {
    		this.setUpdatedDate(HpUfisCalendar.getCurrentUTCTime());
    	}
    	if (this.getUpdatedUser() == null) {
    		this.setUpdatedUser(HpUfisAppConstants.SYSTEM);
    	}
    }
    
    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this._createdDate = createdDate;
    }

    public String getCreatedUser() {
        return _createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this._createdUser = createdUser;
    }

    public String getUpdatedUser() {
        return _updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this._updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return _updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this._updatedDate = updatedDate;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    
    /**
     * Returns a hash code value for the object.  This implementation computes
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Actualleg.  The result is
     * <code>true</code> if and only if the argument is not null and is a Actualleg object that
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (object == null) {
            return false;
        }
        if (!(object.getClass() == getClass())) {
            return false;
        }
        EntMaintenance other =  (EntMaintenance) object;
        if (id != other.id && (id == null || !id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     * checks if this entity object was marked as an orphan that can must be deleted from the database when it is
     * contained in a collection.
     *
     * Note: this deletion will not be done automatically but must be done manually when a parent object writes a
     * collection to the database.
     *
     * @return true if this object can be deleted, false if not
     *
     * @see #setOrphan(boolean)
     */
    public boolean isOrphan() {
        return isOrphan;
    }

    /**
     * marks an entity as an orphan that can be deleted from the database. Note that the deletion will not happen
     * automatically but must be implemented explicitely.
     *
     * @param orphan orphan flag
     */
    public void setOrphan(boolean orphan) {
        isOrphan = orphan;
    }
    
}
