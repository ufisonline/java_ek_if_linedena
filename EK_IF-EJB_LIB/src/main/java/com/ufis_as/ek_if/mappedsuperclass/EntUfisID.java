/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.mappedsuperclass;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

/**
 * @author fou
 */
@MappedSuperclass
public abstract class EntUfisID implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "ID", nullable = false)
    protected String id;

    @PrePersist
	void onPersistUfisID() {
		if (this.id == null) {
			this.id = UUID.randomUUID().toString();
		}
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
