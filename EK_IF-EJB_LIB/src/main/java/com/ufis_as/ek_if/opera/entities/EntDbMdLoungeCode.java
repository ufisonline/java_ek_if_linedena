package com.ufis_as.ek_if.opera.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MD_LOUNGE_CODE database table.
 * 
 */
@Entity
@Table(name="MD_LOUNGE_CODE")
@NamedQueries({
	@NamedQuery(name = "EntDbMdLoungeCode.getAllData", query = "SELECT a FROM EntDbMdLoungeCode a")
})
public class EntDbMdLoungeCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Column(name="LOUNGE_CODE")
	private String loungeCode;

	@Column(name="LOUNGE_NAME")
	private String loungeName;

	@Column(name="LOUNGE_STATION")
	private String loungeStation;

	@Column(name="LOUNGE_TERMINAL")
	private String loungeTerminal;

	@Column(name="OPT_LOCK")
	private BigDecimal optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	public EntDbMdLoungeCode() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public String getLoungeCode() {
		return this.loungeCode;
	}

	public void setLoungeCode(String loungeCode) {
		this.loungeCode = loungeCode;
	}

	public String getLoungeName() {
		return this.loungeName;
	}

	public void setLoungeName(String loungeName) {
		this.loungeName = loungeName;
	}

	public String getLoungeStation() {
		return this.loungeStation;
	}

	public void setLoungeStation(String loungeStation) {
		this.loungeStation = loungeStation;
	}

	public String getLoungeTerminal() {
		return this.loungeTerminal;
	}

	public void setLoungeTerminal(String loungeTerminal) {
		this.loungeTerminal = loungeTerminal;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}