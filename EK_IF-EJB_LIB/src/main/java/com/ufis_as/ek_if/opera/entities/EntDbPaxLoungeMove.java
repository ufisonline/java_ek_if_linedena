package com.ufis_as.ek_if.opera.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * @author btr
 * The persistent class for the PAX_LOUNGE_MOVE database table.
 * 
 */
@Entity
@Table(name="PAX_LOUNGE_MOVE")
@NamedQueries({
	@NamedQuery(name = "EntDbPaxLoungeMove.getByPaxRegnKey", query = "SELECT a FROM EntDbPaxLoungeMove a WHERE a.paxRegnKey = :paxRegnKey AND a.idFlight = :id_Flight")
})
public class EntDbPaxLoungeMove extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DEP_NUM")
	private String depNum;

	@Column(name="ENTER_EXIT_FLAG")
	private String enterExitFlag;

	@Column(name="FLIGHT_NUMBER")
	private String flightNumber;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="FLT_DATE")
	private Date fltDate;

	@Column(name="FLT_DEST3")
	private String fltDest3;

	@Column(name="FLT_ORIGIN3")
	private String fltOrigin3;

	@Column(name="GUEST_FLAG")
	private String guestFlag;

	@Column(name="GUEST_MAPKEY")
	private String guestMapkey;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_LOAD_PAX")
	private String idLoadPax;
	
	@Column(name="ID_MD_LOUNGE_CODE")
	private String idMdLoungeCode;

	@Column(name="LEG_NUM")
	private String legNum;

	@Column(name="LOUNGE_CLASS")
	private String loungeClass;

	@Column(name="LOUNGE_CODE")
	private String loungeCode;

	@Column(name="MSG_ID")
	private BigDecimal msgId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="MSG_SENT_DATE")
	private Date msgSentDate;

	@Column(name="PAX_FREQ_FLYER_ID")
	private String paxFreqFlyerId;

	@Column(name="PAX_FREQ_FLYER_TIER")
	private String paxFreqFlyerTier;

	@Column(name="PAX_NAME")
	private String paxName;

	@Column(name="PAX_NAME_REC")
	private String paxNameRec;

	@Column(name="PAX_REF_NUM")
	private String paxRefNum;

	@Column(name="PAX_REGN_KEY")
	private String paxRegnKey;

	@Column(name="REC_STATUS")
	private String recStatus;

	/*@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="SCAN_LOCAL_DATE")
	private Date scanLocalDate;*/

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="SCAN_DATE")
	private Date scanLoungeDate;

	@Column(name="SCAN_USER")
	private String scanUser;

	public EntDbPaxLoungeMove() {
	}

	public EntDbPaxLoungeMove(EntDbPaxLoungeMove ent) {
		this.idFlight = ent.idFlight;
		this.paxRegnKey = ent.paxRegnKey;
		this.paxRefNum = ent.paxRefNum;
		this.loungeCode = ent.loungeCode;
		this.guestFlag = ent.guestFlag;
		this.scanUser = ent.scanUser;
		this.scanLoungeDate = ent.scanLoungeDate;
	}
	
	@PrePersist
    void onPersistMaintenance() {
		this.dataSource = HpEKConstants.OPERA_SOURCE;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDepNum() {
		return this.depNum;
	}

	public void setDepNum(String depNum) {
		this.depNum = depNum;
	}

	public String getEnterExitFlag() {
		return this.enterExitFlag;
	}

	public void setEnterExitFlag(String enterExitFlag) {
		this.enterExitFlag = enterExitFlag;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFltDate() {
		return this.fltDate;
	}

	public void setFltDate(Date fltDate) {
		this.fltDate = fltDate;
	}

	public String getFltDest3() {
		return this.fltDest3;
	}

	public void setFltDest3(String fltDest3) {
		this.fltDest3 = fltDest3;
	}

	public String getFltOrigin3() {
		return this.fltOrigin3;
	}

	public void setFltOrigin3(String fltOrigin3) {
		this.fltOrigin3 = fltOrigin3;
	}

	public String getGuestFlag() {
		return this.guestFlag;
	}

	public void setGuestFlag(String guestFlag) {
		this.guestFlag = guestFlag;
	}

	public String getGuestMapkey() {
		return this.guestMapkey;
	}

	public void setGuestMapkey(String guestMapkey) {
		this.guestMapkey = guestMapkey;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdLoadPax() {
		return this.idLoadPax;
	}

	public void setIdLoadPax(String idLoadPax) {
		this.idLoadPax = idLoadPax;
	}

	public String getLegNum() {
		return this.legNum;
	}

	public void setLegNum(String legNum) {
		this.legNum = legNum;
	}

	public String getLoungeClass() {
		return this.loungeClass;
	}

	public void setLoungeClass(String loungeClass) {
		this.loungeClass = loungeClass;
	}

	public String getIdMdLoungeCode() {
		return idMdLoungeCode;
	}

	public void setIdMdLoungeCode(String idMdLoungeCode) {
		this.idMdLoungeCode = idMdLoungeCode;
	}

	public String getLoungeCode() {
		return this.loungeCode;
	}

	public void setLoungeCode(String loungeCode) {
		this.loungeCode = loungeCode;
	}

	public BigDecimal getMsgId() {
		return this.msgId;
	}

	public void setMsgId(BigDecimal msgId) {
		this.msgId = msgId;
	}

	public Date getMsgSentDate() {
		return this.msgSentDate;
	}

	public void setMsgSentDate(Date msgSentDate) {
		this.msgSentDate = msgSentDate;
	}

	public String getPaxFreqFlyerId() {
		return this.paxFreqFlyerId;
	}

	public void setPaxFreqFlyerId(String paxFreqFlyerId) {
		this.paxFreqFlyerId = paxFreqFlyerId;
	}

	public String getPaxFreqFlyerTier() {
		return this.paxFreqFlyerTier;
	}

	public void setPaxFreqFlyerTier(String paxFreqFlyerTier) {
		this.paxFreqFlyerTier = paxFreqFlyerTier;
	}

	public String getPaxName() {
		return this.paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPaxNameRec() {
		return this.paxNameRec;
	}

	public void setPaxNameRec(String paxNameRec) {
		this.paxNameRec = paxNameRec;
	}

	public String getPaxRefNum() {
		return this.paxRefNum;
	}

	public void setPaxRefNum(String paxRefNum) {
		this.paxRefNum = paxRefNum;
	}

	public String getPaxRegnKey() {
		return this.paxRegnKey;
	}

	public void setPaxRegnKey(String paxRegnKey) {
		this.paxRegnKey = paxRegnKey;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}
/*
	public Date getScanLocalDate() {
		return this.scanLocalDate;
	}

	public void setScanLocalDate(Date scanLocalDate) {
		this.scanLocalDate = scanLocalDate;
	}
*/
	public Date getScanLoungeDate() {
		return this.scanLoungeDate;
	}

	public void setScanLoungeDate(Date scanLoungeDate) {
		this.scanLoungeDate = scanLoungeDate;
	}

	public String getScanUser() {
		return this.scanUser;
	}

	public void setScanUser(String scanUser) {
		this.scanUser = scanUser;
	}
}