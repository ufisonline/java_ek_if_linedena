package com.ufis_as.ek_if.proveo.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * The persistent class for the EQUIP_POS database table.
 */
@Entity
@Table(name = "EQUIP_POS")
@NamedQueries({@NamedQuery(name = "EntDbEquipPos.findall", query = "SELECT a FROM EntDbEquipPos a where a.recStatus <> 'R'")})
public class EntDbEquipPos extends EntHopoAssociated implements Serializable {

	public static final Logger LOG = LoggerFactory.getLogger(EntDbEquipPos.class);

	//private static final long serialVersionUID = 1L;

	// @Temporal(TemporalType.DATE)
	// @Column(name = "CREATED_DATE")
	// private Date createdDate;
	//
	// @Column(name = "CREATED_USER")
	// private String createdUser;

	@Column(name = "CURR_ARR_FLTNUM")
	private String currArrFltnum;

	@Column(name = "CURR_DEP_FLTNUM")
	private String currDepFltnum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CURR_STATUS_DATE")
	private Date currStatusDate;

	@Column(name = "CURR_UNIT_AREA")
	private String currUnitArea;

	@Column(name = "CURR_VALUE")
	private String currValue;

	@Column(name = "CURR_VALUE_CODE")
	private String currValueCode;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	// private String id;

	@Column(name = "ID_ARR_FLIGHT")
	private BigDecimal idArrFlight;

	@Column(name = "ID_DEP_FLIGHT")
	private BigDecimal idDepFlight;

	// @Column(name = "ID_HOPO")
	// private String idHopo;

	@Column(name = "INFO_TYPE")
	private String infoType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "INFO_UPD_TIME")
	private Date infoUpdTime;

	@Column(name = "INFO_VALUE")
	private String infoValue;

	@Column(name = "LATITUDE")
	private Double latitude;

	@Column(name = "LONGITUTDE")
	private Double longitutde;

	// @Column(name = "OPT_LOCK")
	// private long optLock;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PREV_STATUS_DATE")
	private Date prevStatusDate;

	@Column(name = "PREV_UNIT_AREA")
	private String prevUnitArea;

	@Column(name = "PREV_VALUE")
	private String prevValue;

	@Column(name = "PREV_VALUE_CODE")
	private String prevValueCode;

	@Column(name = "REC_STATUS")
	private String recStatus;

	@Column(name = "TRACK_NUM")
	private String trackNum;

	@Column(name = "UNIT_MODEL")
	private String unitModel;

	@Column(name = "UNIT_NAME")
	private String unitName;

	// @Temporal(TemporalType.DATE)
	// @Column(name = "UPDATED_DATE")
	// private Date updatedDate;
	//
	// @Column(name = "UPDATED_USER")
	// private String updatedUser;

	@Column(name = "USER_DEFINE_AREA")
	private String userDefineArea;

	@Column(name = "X")
	private Double x;

	@Column(name = "Y")
	private Double y;

	public EntDbEquipPos() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.PROVEO_SOURCE;
		this.dataSource = HpEKConstants.PROVEO_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			if (this.getCreatedDate() == null) {
				this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.PROVEO_SOURCE);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbEquipPos other = (EntDbEquipPos) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	// public Date getCreatedDate() {
	// return this.createdDate;
	// }
	//
	// public void setCreatedDate(Date createdDate) {
	// this.createdDate = createdDate;
	// }
	//
	// public String getCreatedUser() {
	// return this.createdUser;
	// }
	//
	// public void setCreatedUser(String createdUser) {
	// this.createdUser = createdUser;
	// }

	public String getCurrArrFltnum() {
		return this.currArrFltnum;
	}

	public void setCurrArrFltnum(String currArrFltnum) {
		this.currArrFltnum = currArrFltnum;
	}

	public String getCurrDepFltnum() {
		return this.currDepFltnum;
	}

	public void setCurrDepFltnum(String currDepFltnum) {
		this.currDepFltnum = currDepFltnum;
	}

	public Date getCurrStatusDate() {
		return this.currStatusDate;
	}

	public void setCurrStatusDate(Date currStatusDate) {
		this.currStatusDate = currStatusDate;
	}

	public String getCurrUnitArea() {
		return this.currUnitArea;
	}

	public void setCurrUnitArea(String currUnitArea) {
		this.currUnitArea = currUnitArea;
	}

	public String getCurrValue() {
		return this.currValue;
	}

	public void setCurrValue(String currValue) {
		this.currValue = currValue;
	}

	public String getCurrValueCode() {
		return this.currValueCode;
	}

	public void setCurrValueCode(String currValueCode) {
		this.currValueCode = currValueCode;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getIdArrFlight() {
		return this.idArrFlight;
	}

	public void setIdArrFlight(BigDecimal idArrFlight) {
		this.idArrFlight = idArrFlight;
	}

	public BigDecimal getIdDepFlight() {
		return this.idDepFlight;
	}

	public void setIdDepFlight(BigDecimal idDepFlight) {
		this.idDepFlight = idDepFlight;
	}

	// public String getIdHopo() {
	// return this.idHopo;
	// }
	//
	// public void setIdHopo(String idHopo) {
	// this.idHopo = idHopo;
	// }

	public String getInfoType() {
		return this.infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public Date getInfoUpdTime() {
		return this.infoUpdTime;
	}

	public void setInfoUpdTime(Date infoUpdTime) {
		this.infoUpdTime = infoUpdTime;
	}

	public String getInfoValue() {
		return this.infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitutde() {
		return this.longitutde;
	}

	public void setLongitutde(Double longitutde) {
		this.longitutde = longitutde;
	}

	// public long getOptLock() {
	// return this.optLock;
	// }
	//
	// public void setOptLock(long optLock) {
	// this.optLock = optLock;
	// }

	public Date getPrevStatusDate() {
		return this.prevStatusDate;
	}

	public void setPrevStatusDate(Date prevStatusDate) {
		this.prevStatusDate = prevStatusDate;
	}

	public String getPrevUnitArea() {
		return this.prevUnitArea;
	}

	public void setPrevUnitArea(String prevUnitArea) {
		this.prevUnitArea = prevUnitArea;
	}

	public String getPrevValue() {
		return this.prevValue;
	}

	public void setPrevValue(String prevValue) {
		this.prevValue = prevValue;
	}

	public String getPrevValueCode() {
		return this.prevValueCode;
	}

	public void setPrevValueCode(String prevValueCode) {
		this.prevValueCode = prevValueCode;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getTrackNum() {
		return this.trackNum;
	}

	public void setTrackNum(String trackNum) {
		this.trackNum = trackNum;
	}

	public String getUnitModel() {
		return this.unitModel;
	}

	public void setUnitModel(String unitModel) {
		this.unitModel = unitModel;
	}

	public String getUnitName() {
		return this.unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	// public Date getUpdatedDate() {
	// return this.updatedDate;
	// }
	//
	// public void setUpdatedDate(Date updatedDate) {
	// this.updatedDate = updatedDate;
	// }
	//
	// public String getUpdatedUser() {
	// return this.updatedUser;
	// }
	//
	// public void setUpdatedUser(String updatedUser) {
	// this.updatedUser = updatedUser;
	// }

	public String getUserDefineArea() {
		return this.userDefineArea;
	}

	public void setUserDefineArea(String userDefineArea) {
		this.userDefineArea = userDefineArea;
	}

	public Double getX() {
		return this.x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return this.y;
	}

	public void setY(Double y) {
		this.y = y;
	}

}