package com.ufis_as.ek_if.proveo.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * The persistent class for the MD_EQUIP database table.
 */
@Entity
@Table(name = "MD_EQUIP")
public class EntDbMdEquip extends EntHopoAssociated implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final Logger LOG = LoggerFactory.getLogger(EntDbMdEquip.class);

	// @Temporal(TemporalType.DATE)
	// @Column(name = "CREATED_DATE")
	// private Date createdDate;
	//
	// @Column(name = "CREATED_USER")
	// private String createdUser;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Column(name = "EQUIP_COST_CTR")
	private String equipCostCtr;

	@Column(name = "EQUIP_FLEET_NUM")
	private String equipFleetNum;

	@Column(name = "EQUIP_GRP_NAME")
	private String equipGrpName;

	@Column(name = "EQUIP_NUM")
	private String equipNum;

	@Column(name = "EQUIP_OWNR_DEPT")
	private String equipOwnrDept;

	@Column(name = "EQUIP_OWNR_SECT")
	private String equipOwnrSect;

	@Column(name = "EQUIP_REMARKS")
	private String equipRemarks;

	@Column(name = "EQUIP_TERMINAL")
	private String equipTerminal;

	@Column(name = "EQUIP_TYPE")
	private String equipType;

	// private String id;

	// @Column(name = "ID_HOPO")
	// private String idHopo;

	@Column(name = "INFOMAN_ID")
	private String infomanId;

	@Column(name = "INFOMAN_NUM")
	private String infomanNum;

	// @Column(name = "OPT_LOCK")
	// private BigDecimal optLock;

	@Column(name = "REC_STATUS")
	private String recStatus;

	// @Temporal(TemporalType.DATE)
	// @Column(name = "UPDATED_DATE")
	// private Date updatedDate;
	//
	// @Column(name = "UPDATED_USER")
	// private String updatedUser;

	private String workgrp;

	public EntDbMdEquip() {
	}

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.PROVEO_SOURCE;
		this.dataSource = HpEKConstants.PROVEO_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			if (this.getCreatedDate() == null) {
				this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.PROVEO_SOURCE);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbMdEquip other = (EntDbMdEquip) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String getEquipCostCtr() {
		return this.equipCostCtr;
	}

	public void setEquipCostCtr(String equipCostCtr) {
		this.equipCostCtr = equipCostCtr;
	}

	public String getEquipFleetNum() {
		return this.equipFleetNum;
	}

	public void setEquipFleetNum(String equipFleetNum) {
		this.equipFleetNum = equipFleetNum;
	}

	public String getEquipGrpName() {
		return this.equipGrpName;
	}

	public void setEquipGrpName(String equipGrpName) {
		this.equipGrpName = equipGrpName;
	}

	public String getEquipNum() {
		return this.equipNum;
	}

	public void setEquipNum(String equipNum) {
		this.equipNum = equipNum;
	}

	public String getEquipOwnrDept() {
		return this.equipOwnrDept;
	}

	public void setEquipOwnrDept(String equipOwnrDept) {
		this.equipOwnrDept = equipOwnrDept;
	}

	public String getEquipOwnrSect() {
		return this.equipOwnrSect;
	}

	public void setEquipOwnrSect(String equipOwnrSect) {
		this.equipOwnrSect = equipOwnrSect;
	}

	public String getEquipRemarks() {
		return this.equipRemarks;
	}

	public void setEquipRemarks(String equipRemarks) {
		this.equipRemarks = equipRemarks;
	}

	public String getEquipTerminal() {
		return this.equipTerminal;
	}

	public void setEquipTerminal(String equipTerminal) {
		this.equipTerminal = equipTerminal;
	}

	public String getEquipType() {
		return this.equipType;
	}

	public void setEquipType(String equipType) {
		this.equipType = equipType;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInfomanId() {
		return this.infomanId;
	}

	public void setInfomanId(String infomanId) {
		this.infomanId = infomanId;
	}

	public String getInfomanNum() {
		return this.infomanNum;
	}

	public void setInfomanNum(String infomanNum) {
		this.infomanNum = infomanNum;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getWorkgrp() {
		return this.workgrp;
	}

	public void setWorkgrp(String workgrp) {
		this.workgrp = workgrp;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

}