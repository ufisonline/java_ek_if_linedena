package com.ufis_as.ek_if.proveo.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ufis_as.configuration.HpEKConstants;
import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;

/**
 * The persistent class for the MD_EQUIP_LOC database table.
 */
@Entity
@Table(name = "MD_EQUIP_LOC")
@NamedQueries({@NamedQuery(name = "EntDbMdEquipLoc.findall", query = "SELECT a FROM EntDbMdEquipLoc a where a.recStatus <> 'R'")})
public class EntDbMdEquipLoc extends EntHopoAssociated implements Serializable {

	public static final Logger LOG = LoggerFactory.getLogger(EntDbMdEquipLoc.class);

	private static final long serialVersionUID = 1L;

	// @Temporal(TemporalType.DATE)
	// @Column(name = "CREATED_DATE")
	// private Date createdDate;
	//
	// @Column(name = "CREATED_USER")
	// private String createdUser;

	@Column(name = "DATA_SOURCE")
	private String dataSource;

	@Column(name = "EQUIP_AREA")
	private String equipArea;

	@Column(name = "EQUIP_AREA_DESC")
	private String equipAreaDesc;

	// private String id;
	//
	// @Column(name = "ID_HOPO")
	// private String idHopo;
	//
	// @Column(name = "OPT_LOCK")
	// private BigDecimal optLock;

	@Column(name = "REC_STATUS")
	private String recStatus;

	// @Temporal(TemporalType.DATE)
	// @Column(name = "UPDATED_DATE")
	// private Date updatedDate;
	//
	// @Column(name = "UPDATED_USER")
	// private String updatedUser;

	@PrePersist
	void onPersistMaintenance() {
		this.id = UUID.randomUUID().toString();
		this._createdUser = HpEKConstants.PROVEO_SOURCE;
		this.dataSource = HpEKConstants.PROVEO_SOURCE;
		this.setHopo(HpEKConstants.EK_HOPO);
//		try {
			if (this.getCreatedDate() == null) {
				this.setCreatedDate(HpUfisCalendar.getCurrentUTCTime());
			}
//		} catch (ParseException e) {
//			LOG.error("ERROR when getting current UTC time : {}", e.toString());
//		}
		if (this.getCreatedUser() == null) {
			this.setCreatedUser(HpEKConstants.PROVEO_SOURCE);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (!(object.getClass() == getClass())) {
			return false;
		}
		EntDbMdEquipLoc other = (EntDbMdEquipLoc) object;
		if (id != other.id && (id == null || !id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public EntDbMdEquipLoc() {
	}

	// public Date getCreatedDate() {
	// return this.createdDate;
	// }
	//
	// public void setCreatedDate(Date createdDate) {
	// this.createdDate = createdDate;
	// }
	//
	// public String getCreatedUser() {
	// return this.createdUser;
	// }
	//
	// public void setCreatedUser(String createdUser) {
	// this.createdUser = createdUser;
	// }

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getEquipArea() {
		return this.equipArea;
	}

	public void setEquipArea(String equipArea) {
		this.equipArea = equipArea;
	}

	public String getEquipAreaDesc() {
		return this.equipAreaDesc;
	}

	public void setEquipAreaDesc(String equipAreaDesc) {
		this.equipAreaDesc = equipAreaDesc;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// public String getIdHopo() {
	// return this.idHopo;
	// }
	//
	// public void setIdHopo(String idHopo) {
	// this.idHopo = idHopo;
	// }
	//
	// public BigDecimal getOptLock() {
	// return this.optLock;
	// }
	//
	// public void setOptLock(BigDecimal optLock) {
	// this.optLock = optLock;
	// }

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	// public Date getUpdatedDate() {
	// return this.updatedDate;
	// }
	//
	// public void setUpdatedDate(Date updatedDate) {
	// this.updatedDate = updatedDate;
	// }
	//
	// public String getUpdatedUser() {
	// return this.updatedUser;
	// }
	//
	// public void setUpdatedUser(String updatedUser) {
	// this.updatedUser = updatedUser;
	// }

}