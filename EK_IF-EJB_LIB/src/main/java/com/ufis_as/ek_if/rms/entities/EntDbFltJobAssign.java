package com.ufis_as.ek_if.rms.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the FLT_JOB_ASSIGN database table.
 * 
 */
@Entity
@Table(name="FLT_JOB_ASSIGN")
@NamedQueries({
	@NamedQuery(name = "EntDbFltJobAssign.findByIdFltJobTask", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFltJobTask = :idFltJobTask AND a.recStatus = :recStatus"),
	@NamedQuery(name = "EntDbFltJobAssign.findAssignedCrewByFlidAndStaffNum", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFlight = :idFlight AND a.staffNumber= :staffNumber AND a.staffTypeCode = :staffTypeCode AND a.staffType = :staffType AND a.dataSource=:dataSource AND a.recStatus <> :recStatus"),
	@NamedQuery(name = "EntDbFltJobAssign.findRecord", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFltJobTask = :idFltJobTask AND a.idStaffShift = :idStaffShift AND a.recStatus NOT IN ('X') "),
	//@NamedQuery(name = "EntDbFltJobAssign.findByFlIdAndStaffNum", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFlight = :idFlight AND a.staffNumber= :staffNumber AND a.fltDate=:fltDate AND a.dataSource=:dataSource AND a.recStatus <> :recStatus"),	
	@NamedQuery(name="EntDbFltJobAssign.findByFltId",query="SELECT e FROM EntDbFltJobAssign e WHERE e.idFlight = :idFlight AND e.dataSource=:dataSource AND e.recStatus <> :recStatus"),
	@NamedQuery(name="EntDbFltJobAssign.findByFltIdCrewType",query="SELECT e FROM EntDbFltJobAssign e WHERE e.idFlight = :idFlight AND e.staffType=:staffType AND e.dataSource=:dataSource AND e.recStatus <> :recStatus")
//	@NamedQuery(name = "EntDbFltJobAssign.findRecordByIdFltJobTaskAndShiftId", query = "SELECT a FROM EntDbFltJobAssign a WHERE a.idFltJobTask = :idFltJobTask AND a.shiftId = :shiftId AND a.recStatus NOT IN ('X') ")	
})
public class EntDbFltJobAssign extends EntHopoAssociated implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;

	@Column(name="CHANGE_REASON")
	private String changeReason;

	@Column(name="CHANGE_REASON_CODE")
	private String changeReasonCode;

	@Column(name="CHARGE_NOTE")
	private String chargeNote;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="EQUIP_DESC")
	private String equipDesc;

	@Column(name="EQUIP_ID")
	private String equipId;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_FLT_JOB_TASK")
	private String idFltJobTask;

//	@Column(name="ID_SERVICE_REQUEST")
//	private String idServiceRequest;

	@Column(name="ID_STAFF_SHIFT")
	private String idStaffShift;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="JOB_ASSIGN_DATE")
	private Date jobAssignDate;

	@Column(name="JOB_DEPT_NAME")
	private String jobDeptName;

	@Column(name="JOB_WORK_AREA")
	private String jobWorkArea;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="PAX_REF_NUM")
	private String paxRefNum;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="SPECIAL_SERVICE")
	private String specialService;

	@Column(name="STAFF_FIRST_NAME")
	private String staffFirstName;

	@Column(name="STAFF_LAST_NAME")
	private String staffLastName;

	@Column(name="STAFF_NAME")
	private String staffName;

	@Column(name="STAFF_NUMBER")
	private String staffNumber;

	@Column(name="STAFF_OP_GRADE")
	private String staffOpGrade;

	@Column(name="STAFF_TYPE")
	private String staffType;

	@Column(name="STAFF_WORK_TYPE")
	private String staffWorkType;
	
	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRANSACT_DATE")
	private Date transactDate;

	@Column(name="TRANSACT_USER")
	private String transactUser;

	@Column(name="TRIP_END")
	private String tripEnd;

	@Column(name="TRIP_START")
	private String tripStart;
	
	@Column(name="SHIFT_ID")
	private BigDecimal shiftId;
	
	@Column(name = "ID_LOAD_PAX")
	private String idLoadPax;
	
	@Column(name = "ID_SERVICE_REQUEST")
	private String idServiceReq;
	
	@Column(name = "PAX_NAME")
	private String paxName;

	public EntDbFltJobAssign() {
	}

	public String getChangeReason() {
		return this.changeReason;
	}

	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}

	public String getChangeReasonCode() {
		return this.changeReasonCode;
	}

	public void setChangeReasonCode(String changeReasonCode) {
		this.changeReasonCode = changeReasonCode;
	}

	public String getChargeNote() {
		return this.chargeNote;
	}

	public void setChargeNote(String chargeNote) {
		this.chargeNote = chargeNote;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getEquipDesc() {
		return this.equipDesc;
	}

	public void setEquipDesc(String equipDesc) {
		this.equipDesc = equipDesc;
	}

	public String getEquipId() {
		return this.equipId;
	}

	public void setEquipId(String equipId) {
		this.equipId = equipId;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdFltJobTask() {
		return this.idFltJobTask;
	}

	public void setIdFltJobTask(String idFltJobTask) {
		this.idFltJobTask = idFltJobTask;
	}

//	public String getIdServiceRequest() {
//		return this.idServiceRequest;
//	}
//
//	public void setIdServiceRequest(String idServiceRequest) {
//		this.idServiceRequest = idServiceRequest;
//	}

	public String getIdStaffShift() {
		return this.idStaffShift;
	}

	public void setIdStaffShift(String idStaffShift) {
		this.idStaffShift = idStaffShift;
	}

	public Date getJobAssignDate() {
		return this.jobAssignDate;
	}

	public void setJobAssignDate(Date jobAssignDate) {
		this.jobAssignDate = jobAssignDate;
	}

	public String getJobDeptName() {
		return this.jobDeptName;
	}

	public void setJobDeptName(String jobDeptName) {
		this.jobDeptName = jobDeptName;
	}

	public String getJobWorkArea() {
		return this.jobWorkArea;
	}

	public void setJobWorkArea(String jobWorkArea) {
		this.jobWorkArea = jobWorkArea;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getPaxRefNum() {
		return this.paxRefNum;
	}

	public void setPaxRefNum(String paxRefNum) {
		this.paxRefNum = paxRefNum;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getSpecialService() {
		return this.specialService;
	}

	public void setSpecialService(String specialService) {
		this.specialService = specialService;
	}

	public String getStaffFirstName() {
		return this.staffFirstName;
	}

	public void setStaffFirstName(String staffFirstName) {
		this.staffFirstName = staffFirstName;
	}

	public String getStaffLastName() {
		return this.staffLastName;
	}

	public void setStaffLastName(String staffLastName) {
		this.staffLastName = staffLastName;
	}

	public String getStaffName() {
		return this.staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffNumber() {
		return this.staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffOpGrade() {
		return this.staffOpGrade;
	}

	public void setStaffOpGrade(String staffOpGrade) {
		this.staffOpGrade = staffOpGrade;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStaffWorkType() {
		return this.staffWorkType;
	}

	public void setStaffWorkType(String staffWorkType) {
		this.staffWorkType = staffWorkType;
	}

	public Date getTransactDate() {
		return this.transactDate;
	}

	public void setTransactDate(Date transactDate) {
		this.transactDate = transactDate;
	}

	public String getTransactUser() {
		return this.transactUser;
	}

	public void setTransactUser(String transactUser) {
		this.transactUser = transactUser;
	}

	public String getTripEnd() {
		return this.tripEnd;
	}

	public void setTripEnd(String tripEnd) {
		this.tripEnd = tripEnd;
	}

	public String getTripStart() {
		return this.tripStart;
	}

	public void setTripStart(String tripStart) {
		this.tripStart = tripStart;
	}

	public BigDecimal getShiftId() {
		return shiftId;
	}

	public void setShiftId(BigDecimal shiftId) {
		this.shiftId = shiftId;
	}

	public String getIdLoadPax() {
		return idLoadPax;
	}

	public void setIdLoadPax(String idLoadPax) {
		this.idLoadPax = idLoadPax;
	}

	public String getIdServiceReq() {
		return idServiceReq;
	}

	public void setIdServiceReq(String idServiceReq) {
		this.idServiceReq = idServiceReq;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getStaffTypeCode() {
		return staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public EntDbFltJobAssign getClone() throws Exception {

		if (this.clone() != null) {
			return (EntDbFltJobAssign) this.clone();
		}
		return null;
	}
}