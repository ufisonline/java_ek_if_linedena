package com.ufis_as.ek_if.rms.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the FLT_JOB_TASK database table.
 * @author btr
 */
@Entity
@Table(name="FLT_JOB_TASK")
@NamedQueries({
	@NamedQuery(name = "EntDbFltJobTask.findUniqueFltJobTask", query = "SELECT a FROM EntDbFltJobTask a WHERE a.idFlight = :idFlight AND a.staffType = :staffType AND a.taskId = :taskId  AND a.recStatus = :recStatus")
})
public class EntDbFltJobTask extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	/**
	 * - (id_flight, task_id, staff_type) are defined as Unique Flt_Job_Task record.
	 */
	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="MAIN_TASK_NUM")
	private BigDecimal mainTaskNum;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE")
	private String staffType;

	@Column(name="SUBTASK_STATUS")
	private String subtaskStatus;

	@Column(name="TASK_ACTUAL_DURATION")
	private BigDecimal taskActualDuration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_STATUS_CHNG_TIME")
	private Date taskStatusChngTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_ACTUAL_END_DATE")
	private Date taskActualEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_ACTUAL_START_DATE")
	private Date taskActualStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_ASSIGN_END_DATE")
	private Date taskAssignEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_ASSIGN_START_DATE")
	private Date taskAssignStartDate;

	@Column(name="TASK_CANNED_MSG")
	private String taskCannedMsg;

	@Column(name="TASK_CATEGORY")
	private String taskCategory;

	@Column(name="TASK_COLOR_CODE")
	private String taskColorCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_CREATED_DATE")
	private Date taskCreatedDate;

	@Column(name="TASK_CRITICAL_FLAG")
	private String taskCriticalFlag;

	@Column(name="TASK_DEPT_ID")
	private String taskDeptId;

	@Column(name="TASK_DEPT_NAME")
	private String taskDeptName;

	@Column(name="TASK_DURATION")
	private BigDecimal taskDuration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_EARLY_START_DATE")
	private Date taskEarlyStartDate;

	@Column(name="TASK_END_LOC")
	private String taskEndLoc;

	@Column(name="TASK_GRP_TYPE")
	private String taskGrpType;

	@Column(name="TASK_HANDLING_STATE")
	private String taskHandlingState;

	@Column(name="TASK_ID")
	private BigDecimal taskId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_LATEST_END_DATE")
	private Date taskLatestEndDate;

	@Column(name="TASK_NUM")
	private BigDecimal taskNum;

	@Column(name="TASK_PLAN_DURATION")
	private BigDecimal taskPlanDuration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_PLAN_END_DATE")
	private Date taskPlanEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TASK_PLAN_START_DATE")
	private Date taskPlanStartDate;

	@Column(name="TASK_PRIORITY")
	private BigDecimal taskPriority;

	@Column(name="TASK_REMARK")
	private String taskRemark;

	@Column(name="TASK_REQUIREMENT_ID")
	private String taskRequirementId;

	@Column(name="TASK_REQUIREMENT_NAME")
	private String taskRequirementName;

	@Column(name="TASK_RULE_GRP")
	private String taskRuleGrp;

	@Column(name="TASK_START_LOC")
	private String taskStartLoc;

	@Column(name="TASK_START_LOC_TYPE")
	private String taskStartLocType;

	@Column(name="TASK_STATUS")
	private String taskStatus;

	@Column(name="TASK_SUBWORK_AREA")
	private String taskSubworkArea;

	@Column(name="TASK_TYPE")
	private String taskType;

	@Column(name="TASK_TYPE_DESC")
	private String taskTypeDesc;

	@Column(name="TASK_WORK_AREA")
	private String taskWorkArea;

	@Column(name="TASK_WORKLOAD_FACTOR")
	private BigDecimal taskWorkloadFactor;

	public EntDbFltJobTask() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public BigDecimal getMainTaskNum() {
		return this.mainTaskNum;
	}

	public void setMainTaskNum(BigDecimal mainTaskNum) {
		this.mainTaskNum = mainTaskNum;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getSubtaskStatus() {
		return this.subtaskStatus;
	}

	public void setSubtaskStatus(String subtaskStatus) {
		this.subtaskStatus = subtaskStatus;
	}

	public BigDecimal getTaskActualDuration() {
		return this.taskActualDuration;
	}

	public void setTaskActualDuration(BigDecimal taskActualDuration) {
		this.taskActualDuration = taskActualDuration;
	}

	public Date getTaskActualEndDate() {
		return this.taskActualEndDate;
	}

	public void setTaskActualEndDate(Date taskActualEndDate) {
		this.taskActualEndDate = taskActualEndDate;
	}

	public Date getTaskActualStartDate() {
		return this.taskActualStartDate;
	}

	public void setTaskActualStartDate(Date taskActualStartDate) {
		this.taskActualStartDate = taskActualStartDate;
	}

	public Date getTaskAssignEndDate() {
		return this.taskAssignEndDate;
	}

	public void setTaskAssignEndDate(Date taskAssignEndDate) {
		this.taskAssignEndDate = taskAssignEndDate;
	}

	public Date getTaskAssignStartDate() {
		return this.taskAssignStartDate;
	}

	public void setTaskAssignStartDate(Date taskAssignStartDate) {
		this.taskAssignStartDate = taskAssignStartDate;
	}

	public String getTaskCannedMsg() {
		return this.taskCannedMsg;
	}

	public void setTaskCannedMsg(String taskCannedMsg) {
		this.taskCannedMsg = taskCannedMsg;
	}

	public String getTaskCategory() {
		return this.taskCategory;
	}

	public void setTaskCategory(String taskCategory) {
		this.taskCategory = taskCategory;
	}

	public String getTaskColorCode() {
		return this.taskColorCode;
	}

	public void setTaskColorCode(String taskColorCode) {
		this.taskColorCode = taskColorCode;
	}

	public Date getTaskCreatedDate() {
		return this.taskCreatedDate;
	}

	public void setTaskCreatedDate(Date taskCreatedDate) {
		this.taskCreatedDate = taskCreatedDate;
	}

	public String getTaskCriticalFlag() {
		return this.taskCriticalFlag;
	}

	public void setTaskCriticalFlag(String taskCriticalFlag) {
		this.taskCriticalFlag = taskCriticalFlag;
	}

	public String getTaskDeptId() {
		return this.taskDeptId;
	}

	public void setTaskDeptId(String taskDeptId) {
		this.taskDeptId = taskDeptId;
	}

	public String getTaskDeptName() {
		return this.taskDeptName;
	}

	public void setTaskDeptName(String taskDeptName) {
		this.taskDeptName = taskDeptName;
	}

	public BigDecimal getTaskDuration() {
		return this.taskDuration;
	}

	public void setTaskDuration(BigDecimal taskDuration) {
		this.taskDuration = taskDuration;
	}

	public Date getTaskEarlyStartDate() {
		return this.taskEarlyStartDate;
	}

	public void setTaskEarlyStartDate(Date taskEarlyStartDate) {
		this.taskEarlyStartDate = taskEarlyStartDate;
	}

	public String getTaskEndLoc() {
		return this.taskEndLoc;
	}

	public void setTaskEndLoc(String taskEndLoc) {
		this.taskEndLoc = taskEndLoc;
	}

	public String getTaskGrpType() {
		return this.taskGrpType;
	}

	public void setTaskGrpType(String taskGrpType) {
		this.taskGrpType = taskGrpType;
	}

	public String getTaskHandlingState() {
		return this.taskHandlingState;
	}

	public void setTaskHandlingState(String taskHandlingState) {
		this.taskHandlingState = taskHandlingState;
	}

	public BigDecimal getTaskId() {
		return this.taskId;
	}

	public void setTaskId(BigDecimal taskId) {
		this.taskId = taskId;
	}

	public Date getTaskLatestEndDate() {
		return this.taskLatestEndDate;
	}

	public void setTaskLatestEndDate(Date taskLatestEndDate) {
		this.taskLatestEndDate = taskLatestEndDate;
	}

	public BigDecimal getTaskNum() {
		return this.taskNum;
	}

	public void setTaskNum(BigDecimal taskNum) {
		this.taskNum = taskNum;
	}

	public BigDecimal getTaskPlanDuration() {
		return this.taskPlanDuration;
	}

	public void setTaskPlanDuration(BigDecimal taskPlanDuration) {
		this.taskPlanDuration = taskPlanDuration;
	}

	public Date getTaskPlanEndDate() {
		return this.taskPlanEndDate;
	}

	public void setTaskPlanEndDate(Date taskPlanEndDate) {
		this.taskPlanEndDate = taskPlanEndDate;
	}

	public Date getTaskPlanStartDate() {
		return this.taskPlanStartDate;
	}

	public void setTaskPlanStartDate(Date taskPlanStartDate) {
		this.taskPlanStartDate = taskPlanStartDate;
	}

	public BigDecimal getTaskPriority() {
		return this.taskPriority;
	}

	public void setTaskPriority(BigDecimal taskPriority) {
		this.taskPriority = taskPriority;
	}

	public String getTaskRemark() {
		return this.taskRemark;
	}

	public void setTaskRemark(String taskRemark) {
		this.taskRemark = taskRemark;
	}

	public String getTaskRequirementId() {
		return this.taskRequirementId;
	}

	public void setTaskRequirementId(String taskRequirementId) {
		this.taskRequirementId = taskRequirementId;
	}

	public String getTaskRequirementName() {
		return this.taskRequirementName;
	}

	public void setTaskRequirementName(String taskRequirementName) {
		this.taskRequirementName = taskRequirementName;
	}

	public String getTaskRuleGrp() {
		return this.taskRuleGrp;
	}

	public void setTaskRuleGrp(String taskRuleGrp) {
		this.taskRuleGrp = taskRuleGrp;
	}

	public String getTaskStartLoc() {
		return this.taskStartLoc;
	}

	public void setTaskStartLoc(String taskStartLoc) {
		this.taskStartLoc = taskStartLoc;
	}

	public String getTaskStartLocType() {
		return this.taskStartLocType;
	}

	public void setTaskStartLocType(String taskStartLocType) {
		this.taskStartLocType = taskStartLocType;
	}

	public String getTaskStatus() {
		return this.taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskSubworkArea() {
		return this.taskSubworkArea;
	}

	public void setTaskSubworkArea(String taskSubworkArea) {
		this.taskSubworkArea = taskSubworkArea;
	}

	public String getTaskType() {
		return this.taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskTypeDesc() {
		return this.taskTypeDesc;
	}

	public void setTaskTypeDesc(String taskTypeDesc) {
		this.taskTypeDesc = taskTypeDesc;
	}

	public String getTaskWorkArea() {
		return this.taskWorkArea;
	}

	public void setTaskWorkArea(String taskWorkArea) {
		this.taskWorkArea = taskWorkArea;
	}

	public BigDecimal getTaskWorkloadFactor() {
		return this.taskWorkloadFactor;
	}

	public void setTaskWorkloadFactor(BigDecimal taskWorkloadFactor) {
		this.taskWorkloadFactor = taskWorkloadFactor;
	}

	public Date getTaskStatusChngTime() {
		return taskStatusChngTime;
	}

	public void setTaskStatusChngTime(Date taskStatusChngTime) {
		this.taskStatusChngTime = taskStatusChngTime;
	}
}