package com.ufis_as.ek_if.rms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_RMS_FLT_STAT database table.
 * @author btr
 */
@Entity
@Table(name="MD_RMS_FLT_STAT")
public class EntDbMdRmsFltStat  extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="FLT_STATUS_ID")
	private String fltStatusId;

	@Column(name="FLT_STATUS_NAME")
	private String fltStatusName;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	public EntDbMdRmsFltStat() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getFltStatusId() {
		return this.fltStatusId;
	}

	public void setFltStatusId(String fltStatusId) {
		this.fltStatusId = fltStatusId;
	}

	public String getFltStatusName() {
		return this.fltStatusName;
	}

	public void setFltStatusName(String fltStatusName) {
		this.fltStatusName = fltStatusName;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
}