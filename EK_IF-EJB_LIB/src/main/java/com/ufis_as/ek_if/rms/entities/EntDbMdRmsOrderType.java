package com.ufis_as.ek_if.rms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_RMS_ORDER_TYPE database table.
 * @author btr
 */
@Entity
@Table(name="MD_RMS_ORDER_TYPE")
public class EntDbMdRmsOrderType extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ORDER_TYPE")
	private String orderType;

	@Column(name="ORDER_TYPE_DESC")
	private String orderTypeDesc;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	public EntDbMdRmsOrderType() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getOrderType() {
		return this.orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderTypeDesc() {
		return this.orderTypeDesc;
	}

	public void setOrderTypeDesc(String orderTypeDesc) {
		this.orderTypeDesc = orderTypeDesc;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
}