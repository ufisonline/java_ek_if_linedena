package com.ufis_as.ek_if.rms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_RMS_SERV_CODE database table.
 * @author btr
 */
@Entity
@Table(name="MD_RMS_SERV_CODE")
public class EntDbMdRmsServCode extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="SERVICE_ID")
	private String serviceId;

	@Column(name="SERVICE_NAME")
	private String serviceName;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	public EntDbMdRmsServCode() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
}