package com.ufis_as.ek_if.rms.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the MD_RMS_SUBTASK_STATUS database table.
 * @author SCH
 */
@Entity
@Table(name="MD_RMS_SUBTASK_STATUS")
public class EntDbMdRmsSubtaskStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_USER")
	private String createdUser;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Id
	@Column(name="ID")
	private String id;

	@Column(name="ID_HOPO")
	private String idHopo;

	@Column(name="OPT_LOCK")
	private BigDecimal optLock;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	@Column(name="SUBTASK_STATUS_DESC")
	private String subtaskStatusDesc;

	@Column(name="SUBTASK_STATUS_ID")
	private String subtaskStatusId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="UPDATED_USER")
	private String updatedUser;

	public EntDbMdRmsSubtaskStatus() {
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdHopo() {
		return this.idHopo;
	}

	public void setIdHopo(String idHopo) {
		this.idHopo = idHopo;
	}

	public BigDecimal getOptLock() {
		return this.optLock;
	}

	public void setOptLock(BigDecimal optLock) {
		this.optLock = optLock;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getSubtaskStatusDesc() {
		return this.subtaskStatusDesc;
	}

	public void setSubtaskStatusDesc(String subtaskStatusDesc) {
		this.subtaskStatusDesc = subtaskStatusDesc;
	}

	public String getSubtaskStatusId() {
		return this.subtaskStatusId;
	}

	public void setSubtaskStatusId(String subtaskStatusId) {
		this.subtaskStatusId = subtaskStatusId;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}