package com.ufis_as.ek_if.rms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_RMS_TASK_STATUS database table.
 * @author btr
 */
@Entity
@Table(name="MD_RMS_TASK_STATUS")
public class EntDbMdRmsTaskStatus extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	@Column(name="TASK_STATUS_ID")
	private String taskStatusId;

	@Column(name="TASK_STATUS_NAME")
	private String taskStatusName;

	public EntDbMdRmsTaskStatus() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getTaskStatusId() {
		return this.taskStatusId;
	}

	public void setTaskStatusId(String taskStatusId) {
		this.taskStatusId = taskStatusId;
	}

	public String getTaskStatusName() {
		return this.taskStatusName;
	}

	public void setTaskStatusName(String taskStatusName) {
		this.taskStatusName = taskStatusName;
	}
}