package com.ufis_as.ek_if.rms.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the MD_RMS_WORK_AREA database table.
 * @author btr
 */
@Entity
@Table(name="MD_RMS_WORK_AREA")
public class EntDbMdRmsWorkArea extends EntHopoAssociated {
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="DEPT_ID")
	private String deptId;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="STAFF_TYPE_CODE")
	private String staffTypeCode;

	@Column(name="WORK_AREA")
	private String workArea;

	@Column(name="WORK_AREA_DESC")
	private String workAreaDesc;

	public EntDbMdRmsWorkArea() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDeptId() {
		return this.deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getStaffTypeCode() {
		return this.staffTypeCode;
	}

	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}

	public String getWorkArea() {
		return this.workArea;
	}

	public void setWorkArea(String workArea) {
		this.workArea = workArea;
	}

	public String getWorkAreaDesc() {
		return this.workAreaDesc;
	}

	public void setWorkAreaDesc(String workAreaDesc) {
		this.workAreaDesc = workAreaDesc;
	}

}