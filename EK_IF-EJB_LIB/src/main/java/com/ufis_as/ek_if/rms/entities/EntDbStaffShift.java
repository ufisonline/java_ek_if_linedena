package com.ufis_as.ek_if.rms.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ufis_as.ek_if.mappedsuperclass.EntHopoAssociated;


/**
 * The persistent class for the STAFF_SHIFT database table.
 * @author btr
 */
@Entity
@Table(name="STAFF_SHIFT")
@NamedQueries({
	@NamedQuery(name = "EntDbStaffShift.findById", query = "SELECT a FROM EntDbStaffShift a WHERE a.id = :id AND a.recStatus = :recStatus AND trim(a.staffType) IN ('ROP', 'SPHL')")
})
public class EntDbStaffShift  extends EntHopoAssociated{
	private static final long serialVersionUID = 1L;

	@Column(name="DATA_SOURCE")
	private String dataSource;

	@Column(name="ID_FLIGHT")
	private BigDecimal idFlight;

	@Column(name="ID_FLT_JOB_ASSIGN")
	private String idFltJobAssign;

	@Column(name="ID_FLT_JOB_TASK")
	private String idFltJobTask;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MSG_SEND_DATE")
	private Date msgSendDate;

	@Column(name="MUTATE_CODE")
	private String mutateCode;

	@Column(name="PDA_RANGE")
	private String pdaRange;

	@Column(name="REC_STATUS")
	private String recStatus;

	@Column(name="RESOURCE_TYPE")
	private String resourceType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_ACTUAL_END_DATE")
	private Date shiftActualEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_ACTUAL_START_DATE")
	private Date shiftActualStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_EST_END_DATE")
	private Date shiftEstEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_EST_START_DATE")
	private Date shiftEstStartDate;

	@Column(name="SHIFT_FUNCTION")
	private String shiftFunction;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_FUNCTION_DATE")
	private Date shiftFunctionDate;

	@Column(name="SHIFT_ID")
	private BigDecimal shiftId;

	@Column(name="SHIFT_LOC")
	private String shiftLoc;

	@Column(name="SHIFT_REMARKS")
	private String shiftRemarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_SCHE_END_DATE")
	private Date shiftScheEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHIFT_SCHE_START_DATE")
	private Date shiftScheStartDate;

	@Column(name="STAFF_DEPT")
	private String staffDept;

	@Column(name="STAFF_EQUIP_ID")
	private String staffEquipId;

	@Column(name="STAFF_FIRST_NAME")
	private String staffFirstName;

	@Column(name="STAFF_HHD_NO")
	private String staffHhdNo;

	@Column(name="STAFF_MOBILE_NO")
	private String staffMobileNo;

	@Column(name="STAFF_NAME")
	private String staffName;

	@Column(name="STAFF_NUMBER")
	private String staffNumber;

	@Column(name="STAFF_QUALIFICATION")
	private String staffQualification;

	@Column(name="STAFF_TYPE")
	private String staffType;

	@Column(name="STAFF_WALKIE_NO")
	private String staffWalkieNo;

	@Column(name="STAFF_WORK_AREA")
	private String staffWorkArea;

	public EntDbStaffShift() {
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public BigDecimal getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(BigDecimal idFlight) {
		this.idFlight = idFlight;
	}

	public String getIdFltJobAssign() {
		return this.idFltJobAssign;
	}

	public void setIdFltJobAssign(String idFltJobAssign) {
		this.idFltJobAssign = idFltJobAssign;
	}

	public String getIdFltJobTask() {
		return this.idFltJobTask;
	}

	public void setIdFltJobTask(String idFltJobTask) {
		this.idFltJobTask = idFltJobTask;
	}

	public Date getMsgSendDate() {
		return this.msgSendDate;
	}

	public void setMsgSendDate(Date msgSendDate) {
		this.msgSendDate = msgSendDate;
	}

	public String getMutateCode() {
		return this.mutateCode;
	}

	public void setMutateCode(String mutateCode) {
		this.mutateCode = mutateCode;
	}

	public String getPdaRange() {
		return this.pdaRange;
	}

	public void setPdaRange(String pdaRange) {
		this.pdaRange = pdaRange;
	}

	public String getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public String getResourceType() {
		return this.resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public Date getShiftActualEndDate() {
		return this.shiftActualEndDate;
	}

	public void setShiftActualEndDate(Date shiftActualEndDate) {
		this.shiftActualEndDate = shiftActualEndDate;
	}

	public Date getShiftActualStartDate() {
		return this.shiftActualStartDate;
	}

	public void setShiftActualStartDate(Date shiftActualStartDate) {
		this.shiftActualStartDate = shiftActualStartDate;
	}

	public Date getShiftEstEndDate() {
		return this.shiftEstEndDate;
	}

	public void setShiftEstEndDate(Date shiftEstEndDate) {
		this.shiftEstEndDate = shiftEstEndDate;
	}

	public Date getShiftEstStartDate() {
		return this.shiftEstStartDate;
	}

	public void setShiftEstStartDate(Date shiftEstStartDate) {
		this.shiftEstStartDate = shiftEstStartDate;
	}

	public String getShiftFunction() {
		return this.shiftFunction;
	}

	public void setShiftFunction(String shiftFunction) {
		this.shiftFunction = shiftFunction;
	}

	public Date getShiftFunctionDate() {
		return this.shiftFunctionDate;
	}

	public void setShiftFunctionDate(Date shiftFunctionDate) {
		this.shiftFunctionDate = shiftFunctionDate;
	}

	public BigDecimal getShiftId() {
		return this.shiftId;
	}

	public void setShiftId(BigDecimal shiftId) {
		this.shiftId = shiftId;
	}

	public String getShiftLoc() {
		return this.shiftLoc;
	}

	public void setShiftLoc(String shiftLoc) {
		this.shiftLoc = shiftLoc;
	}

	public String getShiftRemarks() {
		return this.shiftRemarks;
	}

	public void setShiftRemarks(String shiftRemarks) {
		this.shiftRemarks = shiftRemarks;
	}

	public Date getShiftScheEndDate() {
		return this.shiftScheEndDate;
	}

	public void setShiftScheEndDate(Date shiftScheEndDate) {
		this.shiftScheEndDate = shiftScheEndDate;
	}

	public Date getShiftScheStartDate() {
		return this.shiftScheStartDate;
	}

	public void setShiftScheStartDate(Date shiftScheStartDate) {
		this.shiftScheStartDate = shiftScheStartDate;
	}

	public String getStaffDept() {
		return this.staffDept;
	}

	public void setStaffDept(String staffDept) {
		this.staffDept = staffDept;
	}

	public String getStaffEquipId() {
		return this.staffEquipId;
	}

	public void setStaffEquipId(String staffEquipId) {
		this.staffEquipId = staffEquipId;
	}

	public String getStaffFirstName() {
		return this.staffFirstName;
	}

	public void setStaffFirstName(String staffFirstName) {
		this.staffFirstName = staffFirstName;
	}

	public String getStaffHhdNo() {
		return this.staffHhdNo;
	}

	public void setStaffHhdNo(String staffHhdNo) {
		this.staffHhdNo = staffHhdNo;
	}

	public String getStaffMobileNo() {
		return this.staffMobileNo;
	}

	public void setStaffMobileNo(String staffMobileNo) {
		this.staffMobileNo = staffMobileNo;
	}

	public String getStaffName() {
		return this.staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffNumber() {
		return this.staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String getStaffQualification() {
		return this.staffQualification;
	}

	public void setStaffQualification(String staffQualification) {
		this.staffQualification = staffQualification;
	}

	public String getStaffType() {
		return this.staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getStaffWalkieNo() {
		return this.staffWalkieNo;
	}

	public void setStaffWalkieNo(String staffWalkieNo) {
		this.staffWalkieNo = staffWalkieNo;
	}

	public String getStaffWorkArea() {
		return this.staffWorkArea;
	}

	public void setStaffWorkArea(String staffWorkArea) {
		this.staffWorkArea = staffWorkArea;
	}

}