package com.ufis_as.ek_if.rms.entities;

public class EntRTCFlightSearchObject {
	
	private String flno;
	private String stod;
	private String stoa;
	private String adid;
	private String ori;
	private String org3;
	private String des3;
	private String regn;
	public String getFlno() {
		return flno;
	}
	public void setFlno(String flno) {
		this.flno = flno;
	}
	public String getStod() {
		return stod;
	}
	public void setStod(String stod) {
		this.stod = stod;
	}
	public String getStoa() {
		return stoa;
	}
	public void setStoa(String stoa) {
		this.stoa = stoa;
	}
	public String getAdid() {
		return adid;
	}
	public void setAdid(String adid) {
		this.adid = adid;
	}
	public String getOri() {
		return ori;
	}
	public void setOri(String ori) {
		this.ori = ori;
	}
	public String getOrg3() {
		return org3;
	}
	public void setOrg3(String org3) {
		this.org3 = org3;
	}
	public String getDes3() {
		return des3;
	}
	public void setDes3(String des3) {
		this.des3 = des3;
	}
	public String getRegn() {
		return regn;
	}
	public void setRegn(String regn) {
		this.regn = regn;
	}
	
	


}
