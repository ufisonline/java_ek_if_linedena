package com.ufis_as.ek_if.ufis;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.Tuple;

import com.ufis_as.ek_if.rms.entities.EntRTCFlightSearchObject;
import com.ufis_as.exco.ADID;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.server.oldflightdata.entities.EntDbAfttab;

import ek.egds.MSGTYPE;

@Local
public interface IAfttabBean {

	public Tuple getUrnoByCriteriaQuery(EntDbAfttab params,
			MSGTYPE egdsMsgType, ADID adid, int fromOffset, int toOffset);

	public BigDecimal getUrnoByFilterQuery(EntDbAfttab params, ADID adid, short fromOffset, short toOffset);
	
	// EK-RTC
	public BigDecimal getUrnoForEKRTC(EntRTCFlightSearchObject entRTCFlightSearchObject);
	//for EK pax alert
	public List<EntDbAfttab> findAllFlightByTimeRange();
	public List<EntDbAfttab> findAllFlightByTimeRange(HpUfisCalendar from, HpUfisCalendar to);
	public EntDbAfttab findFlightForConx(BigDecimal urno);
}
