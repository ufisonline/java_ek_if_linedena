/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.ufis;

/**
 *
 * @author fou
 */
public class InterfaceConfig {

    
    private String _name = "";
    private String _xsd = "";
    private String _regexp = "";
    private String _className = "";
    private InterfaceMqConfig _fromMq =  new InterfaceMqConfig();
    private InterfaceMqConfig _toMq =  new InterfaceMqConfig();
    
    public InterfaceConfig () {        
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public String getXsd() {
        return _xsd;
    }

    public void setXsd(String _xsd) {
        this._xsd = _xsd;
    }

    public String getRegexp() {
        return _regexp;
    }

    public void setRegexp(String _regexp) {
        this._regexp = _regexp;
    }

    public String getClassName() {
        return _className;
    }

    public void setClassName(String _className) {
        this._className = _className;
    }
    
    public InterfaceMqConfig getFromMq() {
        return _fromMq;
    }

    public void setFromMq(InterfaceMqConfig _fromMq) {
        this._fromMq = _fromMq;
    }

    public InterfaceMqConfig getToMq() {
        return _toMq;
    }

    public void setToMq(InterfaceMqConfig _toMq) {
        this._toMq = _toMq;
    }
    
    
}
