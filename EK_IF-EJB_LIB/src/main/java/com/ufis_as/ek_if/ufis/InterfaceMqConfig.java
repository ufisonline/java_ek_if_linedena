/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.ufis;


/**
 *
 * @author fou
 */
public class InterfaceMqConfig {
    
    private String _url = "";
    private String _userName = "";
    private String _password = "";
    private String _queueName = null;
    private String _enabled = "FALSE";

    
    public InterfaceMqConfig() {}

    public String getUrl() {
        return _url;
    }

    public void setUrl(String _url) {
        this._url = _url;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String _userName) {
        this._userName = _userName;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String _password) {
        this._password = _password;
    }

    public String getQueueName() {
        return _queueName;
    }

    public void setQueueName(String _queueName) {
        this._queueName = _queueName;
    }
    
/*    public String getQueueName2() {
        return _queueName2;
    }

    public void setQueueName2(String _queueName) {
        this._queueName2 = _queueName;
    }*/
    
    public String getEnabled() {
        return _enabled;
    }

    public void setEnabled(String _enabled) {
        this._enabled = _enabled;
    }


   
    
    
    
}
