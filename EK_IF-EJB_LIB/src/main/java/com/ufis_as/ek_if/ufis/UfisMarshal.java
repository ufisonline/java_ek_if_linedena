/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufis_as.ek_if.ufis;

import java.io.StringWriter;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.LoggerFactory;

import com.ufis_as.exco.ACTIONTYPE;
import com.ufis_as.exco.ADID;
import com.ufis_as.exco.INFOBJGENERIC;
import com.ufis_as.exco.MSG;
import com.ufis_as.exco.MSGOBJECTS;
import com.ufis_as.exco.TIMEID;
import com.ufis_as.ufisapp.lib.time.HpUfisCalendar;
import com.ufis_as.ufisapp.utils.HpUfisUtils;

/**
 *
 * @author fou
 */
public class UfisMarshal {

    /**
     * Logger
     */
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UfisMarshal.class);
    private Marshaller _ms;
    private ACTIONTYPE _msgActionType;
    private String _msgOrigin = "";
    private String _msgOriginType = "";
    private InterfaceConfig _interfaceConfig;

    public UfisMarshal(Marshaller ms, ACTIONTYPE msgActionType, String msgOrigin, InterfaceConfig interfaceConfig) {
        this._ms = ms;
        _msgActionType = msgActionType;
        _msgOrigin = msgOrigin;
        _msgOriginType = _msgOrigin + "-Event";
        _interfaceConfig = interfaceConfig;
    }

    public String marshalFlightEvent(INFOBJGENERIC infGeneric, MSGOBJECTS msgobjects) {
        try {
            StringWriter writer = new StringWriter();

            _ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            HpUfisCalendar ufisCalendar = new HpUfisCalendar(TimeZone.getTimeZone("UTC"));

            MSG msg = new MSG();
            MSG.MSGSTREAMIN msgstream_in = new MSG.MSGSTREAMIN();

            INFOBJGENERIC infobjgeneric = infGeneric;
            if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getURNO()) 
            		&& msgobjects.getINFOBJFLIGHT() != null) {
            	if (infobjgeneric.getADID() == null) {
            		infobjgeneric.setADID(msgobjects.getINFOBJFLIGHT().getADID());
            	}
            	if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getFLNO())) {
            		infobjgeneric.setFLNO(
            				msgobjects.getINFOBJFLIGHT().getALC2() + " " + msgobjects.getINFOBJFLIGHT().getFLTN());
            	}
            	if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getFLNS())) {
            		infobjgeneric.setFLNS(msgobjects.getINFOBJFLIGHT().getFLNS());
            	}
//            	if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getFLDA())) {
//            		infobjgeneric.setFLDA(msgobjects.getINFOBJFLIGHT().getFLDA());
//            	}
                
                if (msgobjects.getINFOBJFLIGHT().getADID() != null) {
                	if (ADID.A.equals(msgobjects.getINFOBJFLIGHT().getADID())) {
                		//infobjgeneric.setSTDT(msgobjects.getINFOBJFLIGHT().getSTOD());
                    	if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getDES3())) {
                    		infobjgeneric.setDES3(msgobjects.getINFOBJFLIGHT().getDES3());
                    	}
                    	if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getDES4())) {
                    		infobjgeneric.setDES4(msgobjects.getINFOBJFLIGHT().getDES4());
                    	}
                    } else {
                		//infobjgeneric.setSTDT(msgobjects.getINFOBJFLIGHT().getSTOA());
                		if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getORG3())) {
                			infobjgeneric.setORG3(msgobjects.getINFOBJFLIGHT().getORG3());	
                		}
                		if (HpUfisUtils.isNullOrEmptyStr(infobjgeneric.getORG4())) {
                			infobjgeneric.setORG4(msgobjects.getINFOBJFLIGHT().getORG4());	
                		}
                    }
                }
            }
            infobjgeneric.setACTIONTYPE(_msgActionType);
            infobjgeneric.setMESSAGEORIGIN(_msgOrigin);
            infobjgeneric.setMESSAGETYPE(_msgOriginType);
            infobjgeneric.setTIMEID(TIMEID.UTC);
            infobjgeneric.setTIMESTAMP(ufisCalendar.getCedaString());
            msgstream_in.setMSGOBJECTS(msgobjects);
            msgstream_in.setINFOBJGENERIC(infobjgeneric);
            msg.setMSGSTREAMIN(msgstream_in);

            _ms.marshal(msg, writer);

            LOG.info("{}", writer.toString());
            return writer.toString();

        } catch (JAXBException ex) {
            LOG.error("Flight Event JAXB exception {}", ex.toString());

            return "";
        }
        
    }
}
