package com.ufis_as.ek_if.ufis;

/**
 * @author btr
 */
public class UfisUldSummaryConfig {

	private float uld_weight;
	private float cargo_weight;
	private float bag_weight;
	
	private float uld_pcs;
	private float cargo_pcs;
	private float bag_pcs;
	
	private float bulk_weight;
	private float bulkCargo_weight;
	private float bulkBag_weight;
	
	private float bulk_pcs;
	private float bulkCargo_pcs;
	private float bulkBag_pcs;
	
	private float pallet_weight;
	private float palletCargo_weight;
	private float palletBag_weight;
	
	private float pallet_pcs;
	private float palletCargo_pcs;
	private float palletBag_pcs;
	
	public float getUld_weight() {
		return uld_weight;
	}
	public void setUld_weight(float uld_weight) {
		this.uld_weight += uld_weight;
	}
	public float getCargo_weight() {
		return cargo_weight;
	}
	public void setCargo_weight(float cargo_weight) {
		this.cargo_weight += cargo_weight;
	}
	public float getBag_weight() {
		return bag_weight;
	}
	public void setBag_weight(float bag_weight) {
		this.bag_weight += bag_weight;
	}
	public float getUld_pcs() {
		return uld_pcs;
	}
	public void setUld_pcs(float uld_pcs) {
		this.uld_pcs += uld_pcs;
	}
	public float getCargo_pcs() {
		return cargo_pcs;
	}
	public void setCargo_pcs(float cargo_pcs) {
		this.cargo_pcs += cargo_pcs;
	}
	public float getBag_pcs() {
		return bag_pcs;
	}
	public void setBag_pcs(float bag_pcs) {
		this.bag_pcs += bag_pcs;
	}
	public float getBulk_weight() {
		return bulk_weight;
	}
	public void setBulk_weight(float bulk_weight) {
		this.bulk_weight += bulk_weight;
	}
	public float getBulkCargo_weight() {
		return bulkCargo_weight;
	}
	public void setBulkCargo_weight(float bulkCargo_weight) {
		this.bulkCargo_weight += bulkCargo_weight;
	}
	public float getBulkBag_weight() {
		return bulkBag_weight;
	}
	public void setBulkBag_weight(float bulkBag_weight) {
		this.bulkBag_weight += bulkBag_weight;
	}
	public float getBulk_pcs() {
		return bulk_pcs;
	}
	public void setBulk_pcs(float bulk_pcs) {
		this.bulk_pcs += bulk_pcs;
	}
	public float getBulkCargo_pcs() {
		return bulkCargo_pcs;
	}
	public void setBulkCargo_pcs(float bulkCargo_pcs) {
		this.bulkCargo_pcs += bulkCargo_pcs;
	}
	public float getBulkBag_pcs() {
		return bulkBag_pcs;
	}
	public void setBulkBag_pcs(float bulkBag_pcs) {
		this.bulkBag_pcs += bulkBag_pcs;
	}
	public float getPallet_weight() {
		return pallet_weight;
	}
	public void setPallet_weight(float pallet_weight) {
		this.pallet_weight += pallet_weight;
	}
	public float getPalletCargo_weight() {
		return palletCargo_weight;
	}
	public void setPalletCargo_weight(float palletCargo_weight) {
		this.palletCargo_weight += palletCargo_weight;
	}
	public float getPalletBag_weight() {
		return palletBag_weight;
	}
	public void setPalletBag_weight(float palletBag_weight) {
		this.palletBag_weight += palletBag_weight;
	}
	public float getPallet_pcs() {
		return pallet_pcs;
	}
	public void setPallet_pcs(float pallet_pcs) {
		this.pallet_pcs += pallet_pcs;
	}
	public float getPalletCargo_pcs() {
		return palletCargo_pcs;
	}
	public void setPalletCargo_pcs(float palletCargo_pcs) {
		this.palletCargo_pcs += palletCargo_pcs;
	}
	public float getPalletBag_pcs() {
		return palletBag_pcs;
	}
	public void setPalletBag_pcs(float palletBag_pcs) {
		this.palletBag_pcs += palletBag_pcs;
	}
}
