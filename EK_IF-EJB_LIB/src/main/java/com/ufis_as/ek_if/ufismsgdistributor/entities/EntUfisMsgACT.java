/*package com.ufis_as.ek_if.ufismsgdistributor.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class EntUfisMsgACT implements Serializable {
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	@JsonProperty("CMD")
	private String cmd;
	@JsonProperty("TAB")
	private String tab;
	@JsonProperty("FLD")
	private List<String> fld;
	@JsonProperty("DATA")
	private List<String> data;
	@JsonProperty("ID")
	private List<String> id;
	@JsonProperty("SEL")
	private String sel;

	public EntUfisMsgACT() {
		fld = new ArrayList<>();
		data = new ArrayList<>();
		id = new ArrayList<>();
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getTab() {
		return tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	public List<String> getFld() {
		return fld;
	}

	public void setFld(List<String> fld) {
		this.fld = fld;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public List<String> getId() {
		return id;
	}

	public void setId(List<String> id) {
		this.id = id;
	}

	public String getSel() {
		return sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

}
*/