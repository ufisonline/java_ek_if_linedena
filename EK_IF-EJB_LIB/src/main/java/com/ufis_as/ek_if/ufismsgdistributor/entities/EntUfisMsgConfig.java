//package com.ufis_as.ek_if.ufismsgdistributor.entities;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 
// * @author SCH
// * 
// */
//public class EntUfisMsgConfig implements Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private String tableName;
//	private List<EntUfisMsgConfigBody> configBody;
//	
//	public EntUfisMsgConfig() {
//		configBody = new ArrayList<>();
//	}
//
//	public String getTableName() {
//		return tableName;
//	}
//
//	public void setTableName(String tableName) {
//		this.tableName = tableName;
//	}
//
//	public List<EntUfisMsgConfigBody> getConfigBody() {
//		return configBody;
//	}
//
//	public void setConfigBody(List<EntUfisMsgConfigBody> configBody) {
//		this.configBody = configBody;
//	}
//
//}
