//package com.ufis_as.ek_if.ufismsgdistributor.entities;
//
//import java.io.Serializable;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import ek.UfisMsgDistributor.UfisMsgDistributorConfig.MsgWrite.Tables.Table.NotifyTo.Processes.Sect.FilterIn;
//import ek.UfisMsgDistributor.UfisMsgDistributorConfig.MsgWrite.Tables.Table.NotifyTo.Processes.Sect.FilterIn.Cond;
//import ek.UfisMsgDistributor.UfisMsgDistributorConfig.MsgWrite.Tables.Table.NotifyTo.Processes.Sect.FilterOff;
///**
// * 
// * @author SCH
// *
// */
//public class EntUfisMsgConfigBody implements Serializable{
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private String[] filds;
//	private List<FilterIn.Cond> filterInconds;
//	private List<FilterOff.Cond> filterOutconds;
//	private List<EntUfisMsgConfigBodyProc> procList;
//	private Map<String,String> cmdReceSend = new HashMap<String,String>();
//	public String[] getFilds() {
//		return filds;
//	}
//	public void setFilds(String[] filds) {
//		this.filds = filds;
//	}
//
//	public List<Cond> getFilterInconds() {
//		return filterInconds;
//	}
//	public void setFilterInconds(List<Cond> filterInconds) {
//		this.filterInconds = filterInconds;
//	}
//
//	public List<FilterOff.Cond> getFilterOutconds() {
//		return filterOutconds;
//	}
//	public void setFilterOutconds(List<FilterOff.Cond> filterOutconds) {
//		this.filterOutconds = filterOutconds;
//	}
//	public List<EntUfisMsgConfigBodyProc> getProcList() {
//		return procList;
//	}
//	public void setProcList(List<EntUfisMsgConfigBodyProc> procList) {
//		this.procList = procList;
//	}
//	public Map<String, String> getCmdReceSend() {
//		return cmdReceSend;
//	}
//	public void setCmdReceSend(Map<String, String> cmdReceSend) {
//		this.cmdReceSend = cmdReceSend;
//	}
//	
//
//}
