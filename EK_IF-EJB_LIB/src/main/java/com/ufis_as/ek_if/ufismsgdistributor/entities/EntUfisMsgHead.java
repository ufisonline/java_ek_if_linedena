/*package com.ufis_as.ek_if.ufismsgdistributor.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class EntUfisMsgHead implements Serializable {
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	@JsonProperty("APP")
	private String app;
	@JsonProperty("VER")
	private String ver;
	@JsonProperty("IP")
	private String ip;
	@JsonProperty("WKS")
	private String wks;
	@JsonProperty("USR")
	private String usr;
	@JsonProperty("HOP")
	private String hop;
	@JsonProperty("SCENE")
	private String scene;
	@JsonProperty("REQT")
	private String reqt;
	@JsonProperty("ORIG")
	private String orig;
	@JsonProperty("DEST")
	private String dest;
	@JsonProperty("BCHOST")
	private String bchost;
	@JsonProperty("BCNUM")
	private int bcnum;
	@JsonProperty("ID_FLIGHT")
	private List<String> idFlight;
	
	public EntUfisMsgHead() {
		idFlight = new ArrayList<>();
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getWks() {
		return wks;
	}

	public void setWks(String wks) {
		this.wks = wks;
	}

	public String getUsr() {
		return usr;
	}

	public void setUsr(String usr) {
		this.usr = usr;
	}

	public String getHop() {
		return hop;
	}

	public void setHop(String hop) {
		this.hop = hop;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getReqt() {
		return reqt;
	}

	public void setReqt(String reqt) {
		this.reqt = reqt;
	}

	public String getOrig() {
		return orig;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getBchost() {
		return bchost;
	}

	public void setBchost(String bchost) {
		this.bchost = bchost;
	}

	public int getBcnum() {
		return bcnum;
	}

	public void setBcnum(int bcnum) {
		this.bcnum = bcnum;
	}

	public List<String> getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(List<String> idFlight) {
		this.idFlight = idFlight;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
*/