package com.ufis_as.ufisapp.ek.eao;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobAssign;


@Local
public interface IBlFltJobAssignBean {
	
	public void storeCrewFltAssign(EntDbFltJobAssign fltJobAssign);
	
	public void updateCrewFltAssign(EntDbFltJobAssign fltJobAssign);
	
	public void deleteCrewFltAssign(EntDbFltJobAssign fltJobAssign);
	
	public List<EntDbFltJobAssign> retrieveCrews(String fltNum, long fltId,Date fltDate);
	
	public List<EntDbFltJobAssign> retrieveCrewsByFltId(long flightId);	

	public List<EntDbFltJobAssign> retrieveCrewsByFltIdDepOrg(String flightNumber,
			Date fltDate, String depStn, String arrStn);
	
	//public String getMaxId();

}
