package com.ufis_as.ufisapp.ek.eao;

import javax.ejb.Local;
import javax.jms.MessageListener;

@Local
public interface IBlUfisTibcoMDB extends MessageListener {

	public void init(String destination);
	public void destroy();//to close existing session
}
