package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.rms.entities.EntDbFltJobAssign;

@Local
public interface IDlFlighttJobAssignLocal {

	public EntDbFltJobAssign merge(EntDbFltJobAssign fltJobTask);
	public List<EntDbFltJobAssign> findByIdFltJobTask(String idFltJobTask);
	public EntDbFltJobAssign getAssignedCrewIdByFlight(BigDecimal idFlight, String staffNum, String crewType);
	public List<EntDbFltJobAssign> getAssignedCrewsByFltId(long fltId);
	public List<EntDbFltJobAssign> getAssignedCrewsByFltIdCrewType(long fltId,String crewType);	
}
