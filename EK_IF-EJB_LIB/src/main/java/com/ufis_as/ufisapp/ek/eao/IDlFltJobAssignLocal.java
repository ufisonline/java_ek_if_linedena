package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobAssign;

@Local
public interface IDlFltJobAssignLocal {

	boolean saveCrewFlightAssign(EntDbFltJobAssign fltJobAssign);

	public void updateCrewFlightAssign(EntDbFltJobAssign fltJobAssign);

	public List<EntDbFltJobAssign> getAssignedCrews(String fltNum, long fltId,
			Date fltDate);

	public EntDbFltJobAssign findCrewFlight(String fltNum, long fltId,
			Date date, String staffNum);

	public EntDbFltJobAssign getAssignedCrewIdByFlight(BigDecimal idFlight, String staffNum);
	
	public List<EntDbFltJobAssign> getAssignedCrewsByFltId(long fltId);
	// public String getMaxId();

	public List<EntDbFltJobAssign> getAssignedCrewsByFltIdDesOrg(String flightNumber,
			Date fltDate, String org3, String des3);

}
