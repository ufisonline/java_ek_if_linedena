package com.ufis_as.ufisapp.ek.eao;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.acts_ods.entities.EntDbFltJobSummary;

@Local
public interface IDlFltJobSummaryLocal {
	

	public EntDbFltJobSummary saveFltJobSummary(EntDbFltJobSummary fltJobSummary);

	public EntDbFltJobSummary updateFlightJobSummary(EntDbFltJobSummary fltJobSummary);
	
	public List<EntDbFltJobSummary> getFltJobSummary(long id_flight);
	
	public List<EntDbFltJobSummary> getFltJobSummaryByCrewType(long id_flight,String crewType);

	public boolean deleteFltJobSummaryByCrewType(long fltId,String crewType);
	
	public boolean deleteFltJobSummary(long fltId);

	public EntDbFltJobSummary getFltJobSummaryRecByCrewType(long id_flight);

}
