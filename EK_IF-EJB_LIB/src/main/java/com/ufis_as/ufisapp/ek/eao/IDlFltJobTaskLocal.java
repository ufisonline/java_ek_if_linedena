package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;

import javax.ejb.Local;

import com.ufis_as.ek_if.rms.entities.EntDbFltJobTask;

@Local
public interface IDlFltJobTaskLocal {

	public EntDbFltJobTask merge(EntDbFltJobTask fltJobTask);
	public EntDbFltJobTask findExistingFltJobTask(BigDecimal idFlight, BigDecimal taskId, String staffType);
	
}
