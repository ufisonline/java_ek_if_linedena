package com.ufis_as.ufisapp.ek.eao;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbLoadBagMove;

@Local
public interface IDlLoadBagMoveUpdateLocal {

	public List<EntDbLoadBagMove> getBagMoveDetailsByPax(long idFlight,
			String loadBagTag, String paxRefNum);
	
	public List<EntDbLoadBagMove> getBagMoveDetails(long idFlight,
			String loadBagTag);

	EntDbLoadBagMove updateLoadedBagMove(EntDbLoadBagMove loadBagMove);

	boolean saveLoadBagMove(EntDbLoadBagMove loadBagMove);

	public List<EntDbLoadBagMove> getBagMoveDetailsById(BigDecimal bigDecimal,
			String loadBagTag, String id);

	public List<EntDbLoadBagMove> getBagMoveByIdLoadBag(String idLoadBag);

}
