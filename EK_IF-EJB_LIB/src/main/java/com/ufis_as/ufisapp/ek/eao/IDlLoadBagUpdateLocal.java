package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbLoadBag;

@Local
public interface IDlLoadBagUpdateLocal {

	//public List<EntDbLoadBag> getBagMoveDetails(long idFlight, String idLoadPax);

	public boolean saveLoadBagMove(EntDbLoadBag loadBag);

	public EntDbLoadBag updateLoadedBag(EntDbLoadBag loadBag);

	public List<EntDbLoadBag> getBagMoveDetails(String bagTag, String paxRefNum,
			String fltNumber, String fltDate, String fltDest3);

	public List<EntDbLoadBag> getBagMoveDetailsByBagTag(long idFlight, String bagTag);
	
	public List<EntDbLoadBag> getBagMoveDetailsByBagtagPax(long idFlight, String bagTagStr, String paxRefNum);
	
	public EntDbLoadBag getValidBagMoveDetailsByBagTag(long idFlight,String bagTag);
	
	public List<EntDbLoadBag> getBagMoveDetailsByPax(long idFlight,
			String paxRefNum);
	
	public List<EntDbLoadBag> getBagMoveDetailsByPaxX(String idLoadPax);
	
	public List<EntDbLoadBag> getBagMoveDetailsByIdLoadPax(String idLoadPax);

	
	public List<EntDbLoadBag> getBagMoveDetailsByBagtagPaxX(long idFlight,
			String bagTagStr, String paxRefNum);

}
