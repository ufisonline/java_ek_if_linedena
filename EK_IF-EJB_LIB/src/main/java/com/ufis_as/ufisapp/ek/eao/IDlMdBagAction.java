package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbMdBagAction;

@Local
public interface IDlMdBagAction {

	public List<String> getBagActions();

	public List<EntDbMdBagAction> getAllBagActions();

}
