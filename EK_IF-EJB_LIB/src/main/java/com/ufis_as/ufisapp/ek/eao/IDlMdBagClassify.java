package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbMdBagClassify;

@Local
public interface IDlMdBagClassify {

	public List<String> getBagClassifies();

	public List<EntDbMdBagClassify> getAllBagClassifies();

}
