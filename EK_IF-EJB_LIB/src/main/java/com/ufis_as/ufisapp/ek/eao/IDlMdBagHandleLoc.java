package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbMdBagHandleLoc;

@Local
public interface IDlMdBagHandleLoc {

	public List<EntDbMdBagHandleLoc> getAllBagHandleLocs();

}
