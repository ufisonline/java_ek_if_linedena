package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.belt.entities.EntDbMdBagType;

@Local
public interface IDlMdBagType {

	public List<String> getBagTypes();

	public List<EntDbMdBagType> getAllBagTypes();

}
