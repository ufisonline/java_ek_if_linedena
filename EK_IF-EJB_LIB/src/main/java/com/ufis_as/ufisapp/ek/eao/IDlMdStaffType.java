package com.ufis_as.ufisapp.ek.eao;

import java.util.List;

import javax.ejb.Local;

import com.ufis_as.ek_if.acts_ods.entities.EntDbMdStaffType;

@Local
public interface IDlMdStaffType {
	
	public List<EntDbMdStaffType> getStaffTypeDetails();
	
	
}
