package com.ufis_as.ufisapp.ek.eao;

import javax.ejb.Local;

import com.ufis_as.ek_if.rms.entities.EntDbStaffShift;

@Local
public interface IDlStaffShiftLocal {

	public EntDbStaffShift merge(EntDbStaffShift staffShift);
	public EntDbStaffShift findById(String idStaffShift);
	
}
