package com.ufis_as.ufisapp.ek.eao;

import com.ufis_as.ek_if.proveo.entities.EntDbEquipPos;

public interface IDlUpdateEquipPosLocal {

	public EntDbEquipPos saveEqiupPosUpdate(EntDbEquipPos euipPosUpdate);

	public void updateEqiupPos(EntDbEquipPos euipPosUpdate);

}
