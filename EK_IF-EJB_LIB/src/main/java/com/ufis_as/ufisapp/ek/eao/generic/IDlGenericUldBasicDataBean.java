package com.ufis_as.ufisapp.ek.eao.generic;

import java.util.List;

public interface IDlGenericUldBasicDataBean<T> {

	public List<T> getAllBasicData(String queryString);
}
