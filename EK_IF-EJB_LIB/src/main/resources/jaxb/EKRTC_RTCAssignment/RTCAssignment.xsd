<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.emirates.com/AAO/RTC/2013/05" xmlns="http://www.emirates.com/AAO/RTC/2013/05" elementFormDefault="qualified">
    <xs:complexType name="ResourceAssignmentType">
        <xs:sequence>
            <xs:element name="Meta" type="MetaType" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Identifies a message</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="TSK" type="TSKType" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Defines the shift information of the resource</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:element name="RTC" type="ResourceAssignmentType">
        <xs:annotation>
            <xs:documentation>Used to push shift information of a resource</xs:documentation>
        </xs:annotation>
    </xs:element>
    <xs:complexType name="MetaType">
        <xs:sequence>
            <xs:element name="MessageTime" type="xs:dateTime" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Time at which message was sent (in local timezone)</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="MessageID" type="xs:int" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Unique identifier for the message</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="MessageType" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Message type. Select from the applicable values.</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="REA" />
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="MessageSubtype" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>This identifies what action to be performed</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="INS"/>
                        <xs:enumeration value="UPD"/>
                        <xs:enumeration value="DEL"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="MessageSource" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Source system for this message</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="EKRTC"/>
                        <xs:enumeration value="GXRTC"/>
                        <xs:enumeration value="CSRTC"/>
                        <xs:enumeration value="ENGRTC"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="TSKType">
        <xs:annotation>
            <xs:documentation>Contains shift information</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="AID" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Unique identifier for the message</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="TRG" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Rule Group. Refer task rule group master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="TRQ" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Requirement based on which resource is assigned to the task. Refer task requirement Id master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="TRD" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Requirement Description. Refer task requirement Id master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="DEP" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Department ID. Refer task department master</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="PS"/>
                        <xs:enumeration value="BS"/>
                        <xs:enumeration value="GD"/>
                        <xs:enumeration value="SPLSVC"/>
                        <xs:enumeration value="GXAPT"/>
                        <xs:enumeration value="GXCAPU"/>
                        <xs:enumeration value="CB"/>
                        <xs:enumeration value="LM"/>
                        <xs:enumeration value="CM"/>
                        <xs:enumeration value="IFE"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="TYP" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Type Task. Refer task type master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ORI" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Group Type. Refer task group type master. 0-Non flight based, 1-Arrival, 2-Departure, 3-Ground time</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="0"/>
                        <xs:enumeration value="1"/>
                        <xs:enumeration value="2"/>
                        <xs:enumeration value="3"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="SNO" minOccurs="1" maxOccurs="1" type="xs:int">
                <xs:annotation>
                    <xs:documentation>Task Number</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="MNO" minOccurs="1" maxOccurs="1" type="xs:int">
                <xs:annotation>
                    <xs:documentation>Main Task Number</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="RMK" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Remark</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="STI" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Actual Task Start Time (local time zone) format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ETI" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Actual Task End Time (local time zone) format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="DUR" minOccurs="0" maxOccurs="1" type="xs:int">
                <xs:annotation>
                    <xs:documentation>Task Duration</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="EST" type="xs:string" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Planned Task Start Time (local time zone) format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="LET" type="xs:string" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Planned Task End Time (local time zone) format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SLO" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task start location</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ELO" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task end location</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ARK" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Auto Remark. Used by Engineering RTC</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="HST" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Handling state. refer handling state master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="MWA" minOccurs="1" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>WorkArea. Refer work area master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SWA" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Sub WorkArea. Refer work area master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="WLD" minOccurs="0" maxOccurs="1" type="xs:int">
                <xs:annotation>
                    <xs:documentation>Workload factor</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PRI" minOccurs="0" maxOccurs="1" type="xs:int">
                <xs:annotation>
                    <xs:documentation>Task priority indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="NRO" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Category. If the task type starts with NR, then it’s a non-routine task, If It’s not available in RTC, this will be filled in by ESB based on the above task type logic</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="Routine"/>
                        <xs:enumeration value="Non Routine"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="AFT" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Aircraft type</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="STS" minOccurs="1" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Status. Refer status master</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="FREE"/>
                        <xs:enumeration value="AUTO"/>
                        <xs:enumeration value="MANUAL"/>
                        <xs:enumeration value="CANCELLED"/>
                        <xs:enumeration value="ASSIGN"/>
                        <xs:enumeration value="CONFIRMED"/>
                        <xs:enumeration value="CONFRM"/>
                        <xs:enumeration value="STARTED"/>
                        <xs:enumeration value="STARTD"/>
                        <xs:enumeration value="FINISH"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="STT" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Sub Task Status. Refer sub status master. Mandatory for EK RTC</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="FREE"/>
                        <xs:enumeration value="AUTO"/>
                        <xs:enumeration value="MANUAL"/>
                        <xs:enumeration value="CANCELLED"/>
                        <xs:enumeration value="ASSIGN"/>
                        <xs:enumeration value="CONFIRMED"/>
                        <xs:enumeration value="STARTED"/>
                        <xs:enumeration value="DEPARTED"/>
                        <xs:enumeration value="ARRIVED"/>
                        <xs:enumeration value="FINISH"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="RAG" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task RAG. Engineering RTC</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="red"/>
                        <xs:enumeration value="amber"/>
                        <xs:enumeration value="green"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="CRT" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Criticality information. 0 - Non Critical, 1 - Critical</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:token">
                        <xs:enumeration value="0"/>
                        <xs:enumeration value="1"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="DNO" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Departure Flight No (numerical). Mandatory for departure flight. E.g.: 0405</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="DFD" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Scheduled departure date in local time. Mandatory for departure flight. format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="REG" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Registration. Mandatory for Engineering RTC</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="DSN" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Departure Station</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ASN" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Arrival Station</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="ANO" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Arrival Flight No (numerical). Mandatory for arrival flight. E.g.: 0405</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SID" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Shift Unique Identifier. Reference to the staff shift information</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PRF" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>PAX Reference Number. Application for the special services</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PNR" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>PAX PNR number</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PNM" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>PAX Name</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PGE" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>PAX Gender</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="PCC" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>PAX cabin class</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SLT" minOccurs="0" maxOccurs="1" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Task Start Location. Refer location master</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="STA" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Scheduled Time Arrival in local time. Mandatory for arrival flight.format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="AST" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Assigned Start Time. format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="AET" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Assigned End Time. format DD.MM.YYYY HH24:MI:SS</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="AAL" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>2 Letter IATA Airline Code</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SLN" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Start Location Name</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="STN" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Status Name</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="DPN" type="xs:string" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>Task Department Name</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
